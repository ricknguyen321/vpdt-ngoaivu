<?php 

	if(! function_exists('pr'))

	{

		function pr($data) {

			echo "<meta charset='UTF-8' />";

			echo "<pre>";

				print_r ($data);

			echo "</pre>";



			exit();

		}

	}

	// đếm ký tự unicode

	function catKyTu($string)

	{

		$tong = mb_strlen($string, 'UTF-8');

		if($tong>200)

		{

			$ketqua = mb_substr($string,0,197).'...';

		}

		else{

			$ketqua = $string;

		}

		return $ketqua;

	}

	// đếm ký tự unicode

	function catKyTu1($string)

	{

		$tong = mb_strlen($string, 'UTF-8');

		if($tong>50)

		{

			$ketqua = mb_substr($string,0,50).'...';

		}

		else{

			$ketqua = $string;

		}

		return $ketqua;

	}

	function kiemtrangay($ngay)

	{

		$date=  explode('/', $ngay);

		if($date[1]>12)

		{

			$thoigian= $date[1].'/'.$date[0].'/'.$date[2];

		}

		else{

			$thoigian =$ngay;

		}

		return $thoigian;

	}

	// lấy đuôi file

	function layDuoiFile($file)

	{

		return substr($file,-3);

	}

	// Đếm số bản ghi trong 1 bảng

	function banghi($table)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		return $CI->Mdanhmuc->banghi($table);

	}

	function banghi2($table,$trangthai)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		return $CI->Mdanhmuc->banghi2($table,$trangthai);

	}

	//hàm cộng số ngày trừ thứ bảy và chủ nhật

	function dateFromBusinessDays($days, $dateTime=null) {

	  	$dateTime = is_null($dateTime) ? time() : strtotime(str_replace('/', '-', $dateTime));

	  	$_day = 0;

	  	$_direction = $days == 0 ? 0 : intval($days/abs($days));

	  	$_day_value = (60 * 60 * 24);



	  	while($_day !== $days) {

	    	$dateTime += $_direction * $_day_value;



	    	$_day_w = date("w", $dateTime);

	    	if ($_day_w > 0 && $_day_w < 6) {

	      		$_day += $_direction * 1; 

	    	}

	  	}

	  	return date('Y-m-d',$dateTime);

	}

	// chuyển số sang số la mã

	function intToRoman($integer)

	{

	   	// Convert the integer into an integer (just to make sure)

	   	$integer = intval($integer);

	 	$result = '';

	 

		// Create a lookup array that contains all of the Roman numerals.

		$lookup = array('M' => 1000,

		'CM' => 900,

		'D' => 500,

		'CD' => 400,

		'C' => 100,

		'XC' => 90,

		'L' => 50,

		'XL' => 40,

		'X' => 10,

		'IX' => 9,

		'V' => 5,

		'IV' => 4,

		'I' => 1);

		 

		foreach($lookup as $roman => $value){

			// Determine the number of matches

		    $matches = intval($integer/$value);

		 

		  	// Add the same number of characters to the string

		  	$result .= str_repeat($roman,$matches);

		 

		  	// Set the integer to be the remainder of the integer and the value

		  	$integer = $integer % $value;

		}

		 

		// The Roman numeral should be built, return it

		return $result;

	}

	function limit_words($string, $word_limit) {

		$string = strip_tags($string);

		$words = explode(' ', strip_tags($string));

		$return = trim(implode(' ', array_slice($words, 0, $word_limit)));

		if(strlen($return) < strlen($string)){

		$return .= '...';

		}

		return $return;

	}

	// tìm kiếm chuỗi 

	function timkiem($string,$key)

	{

		$kq = strpos($string,$key);

		if($kq===false)

		{

			return 'sai';

		}

		else{

			return 'dung';

		}

	}

	// kiểm tra quyền thêm sửa xóa duyệt đọc

	function kiemtra($macb,$ma)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mnhomchucnang');

		return $CI->Mnhomchucnang->kiemtraQuyen($macb,$ma);

	}

	// kiểm tra quyền

	function kiemtraQuyen($url,$ma)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		$mangma= explode('-',$ma);

		return  $CI->Mdanhmuc->kiemtraQuyen($url,'PK_iMaCN',$mangma,'tbl_chucnang');

	}

	function getURLCurrent()

	{

		$CI =& get_instance();

		return $CI->uri->segment(1);

	}

	function _post($name)

	{

		$CI =& get_instance();

		$ketqua=$CI->input->post($name);

		return $ketqua;

	}

	function _get($name)

	{

		$CI =& get_instance();

		$ketqua=$CI->input->get($name);

		return $ketqua;

	}

	// lấy nhóm chức năng theo mã tài khoản

	function layNhomChucNang($ma)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mnhomchucnang');

		$mangma= explode('-',$ma);

		return  $CI->Mnhomchucnang->layNhomChucNang($mangma);

	}

	// khoảng cách giữa 2 ngày
	function count_ngay($data1,$data2)
	{
	$a=1;
	$first_date = strtotime($data1);
	$second_date = strtotime($data2);
	$datediff = abs($first_date - $second_date);
	if($first_date < $second_date)$a=-1;
	$songay = floor($datediff / (60*60*24));
	return $a*$songay;
	}

	// lấy chức năng teo tài khoản và group nhóm lại

	function layChucNangGR($ma)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mnhomchucnang');

		$mangma= explode('-',$ma);

		return  $CI->Mnhomchucnang->layDuLieu('PK_iMaCN',$mangma,'tbl_chucnang');

	}

	// lấy chức năng theo mã tài khoản

	function layChucNang($ma)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		$mangma= explode('-',$ma);

		return  $CI->Mdanhmuc->layDuLieuMang('PK_iMaCN',$mangma,'tbl_chucnang');

	}

	// lấy menu

	function layMeNu()

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		return $CI->Mdanhmuc->sapxepDuLieu('iDoUuTien','dm_menu','asc');

	}

	function tinThongBao()

	{

		$CI =& get_instance();

		$CI->load->model('tintuc/Mtintuc');

		return $CI->Mtintuc->layTinThongBao();

	}

	// lấy ra những câu hỏi chưa dk trả lời

	function CauHoi()

	{

		$CI =& get_instance();

		$CI->load->model('hoidaplienhe/Mcauhoi');

		return $CI->Mcauhoi->layCauHoiChuaTraLoi();

	}

	// lấy tin tức chưa được duyệt

	function tinChuaDuyet()

	{

		$CI =& get_instance();

		$CI->load->model('tintuc/Mtintuc');

		return $CI->Mtintuc->tinChuaDuyet();

	}

	// lấy 6 ảnh mới nhất

	function layAnh()

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mhinhanh');

		return $CI->Mhinhanh->layAnhMoiNhat();

	}

	// lấy các thể loại

	function layTheLoai()

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		return $CI->Mdanhmuc->sapxepDuLieu('iDoUuTien','dm_theloai','asc');

	}

	// lấy các loại tin bên trái

	function layLoaiTin()

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mloaitin');

		return $CI->Mloaitin->layLoaiTin('left');

	}

	// lấy ra các chuyên mục

	function layChuyenMuc($vitri)

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		return $CI->Mdanhmuc->timkiemDuLieu('sViTriHienThi',$vitri,'tbl_chuyenmuc');

	}

	// lấy liên kết

	function layLienKet()

	{

		$CI =& get_instance();

		$CI->load->model('danhmuc/Mdanhmuc');

		return $CI->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_lienkettrang');

	}

	// lấy mã bài viết hoặc loại tin trên url

	function getmaURL($url)

	{

		$ma = explode('_',$url);

		return $ma[0];

	}

	function clear($str){

	    if(!$str) return false;

	    $unicode = array(

	        'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),

	        'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),

	        'd'=>array('đ'),

	        'D'=>array('Đ'),

	        'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),

	        'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),

	        'i'=>array('í','ì','ỉ','ĩ','ị'),

	        'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),

	        'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),

	        'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),

	        'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),

	        'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),

	        'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),

	        'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),

	        '-'=>array(' ','&quot;','-–-')

	    );

        foreach($unicode as $nonUnicode=>$uni){

            foreach($uni as $value)

            $str = @str_replace($value,$nonUnicode,$str);

            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);

            $str = preg_replace("/-+-/","-",$str);

            $str = preg_replace("/^\-+|\-+$/","",$str);

        }

        return strtolower($str);

    	}



	function post($name)

	{

		$CI =& get_instance();

		return $CI->input->post($name);

	}

	

	// kiểm tra đường dẫn anh

	function kiemtraduongdan($ma)

	{

		if (file_exists("anhcanbo/".$ma.".jpg")) {

		    return 'dung';

		} else {

		    return '';

		}

	}

	// id tự tăng

	function created_id($prefix, $number_id) {

		$number_id += 1;

		

		switch(strlen($number_id)) {

			case 1:

			$result = $prefix."_0".$number_id;

			break;

		

			case 2:

			$result = $prefix."_".$number_id;

			break;

			

			default:

			$result = $prefix."_".$number_id;

			break;

		}

		

		return $result;

	}

	/* ------------------------------------ Thời gian dạng giây từ năm 1970 ------------------------------- */

	function unix_time($date) {

		$date = strtotime(str_replace('/', '-', $date));

		return $date;

	}

	// thông báo showmessage

	function messagebox($content = '',$class = '') {

		return array(

			'content' => $content,

			'class' => $class

		);

	}

	// định dạng ngày tháng năm để insert chuyển từ dd/mm/yyyy => yyyy-mm-dd

	function date_insert($data)

	{

	$ketqua = date('Y-m-d', strtotime(str_replace('/', '-', $data)));

	return $ketqua;

	}

	// định dạng ngày tháng năm để select chuyển từ yyyy-mm-dd => dd/mm/yyyy

	function date_select($data)

	{

	$ketqua = date('d/m/Y', strtotime($data));

	return $ketqua;

	}

	function date_time($data)

	{

	$ketqua = date('d/m/Y H:i:s', strtotime($data));

	return $ketqua;

	}

	function date_time2($data)

	{

	$ketqua = date('d/m H:i:s', strtotime($data));

	return $ketqua;

	}

	// lấy năm hoặc tháng hoặc ngày của thời gian truyền vào

	function gettime($dinhdang,$date)

	{

		return date($dinhdang, strtotime($date));

	}

	//cộng tháng theo thời gian dd-mm-yyyy + n tháng

	function format_date($thoigian,$sothang)

	{

		if(!empty($thoigian))

		{

		$nam         = (int)($sothang/12);

		$thang       = $sothang%12;

		$thoigian    = explode('-',$thoigian);

		$thangcap    = (($thoigian[1]+$thang)>12)?(($thoigian[1]+$thang)-12):($thoigian[1]+$thang);

		$namcap      = (($thoigian[1]+$thang)>12)?($nam+$thoigian[2]+1):$nam+$thoigian[2];

		$thoigiancap = $thoigian[0].'-'.$thangcap.'-'.$namcap;

		return strtotime($thoigiancap);

		}

		else

		{

			return 0;

		}

	}

	// format tiền

	function formattien($tien)

	{

		return number_format($tien,0,",",".");

	}

    // lấy dữ liệu

    function layDuLieu($Colums=NULL,$Value=NULL,$table=NULL)

    {

        $CI =& get_instance();

        $CI->load->model('danhmuc/Mdanhmuc');

        return $CI->Mdanhmuc->layDuLieu($Colums,$Value,$table);

    }

    function unserialize_session_data( $serialized_string )

    {

        $variables = array();

        $a = preg_split( "/(\w+)\|/", $serialized_string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE );



        for( $i = 0; $i<count($a); $i = $i+2 )

        {

            if(isset($a[$i+1]))

            {

                $variables[$a[$i]] = unserialize( $a[$i+1] );

            }

        }

        return( $variables );

    }



    // lấy dữ liệu

    function laySoDen($Colums=NULL,$Value=NULL,$table=NULL)

    {

        $CI =& get_instance();

        $CI->load->model('danhmuc/Mdanhmuc');

        return $CI->Mdanhmuc->laySoDen($Colums,$Value,$table);

    }
	 // lấy dữ liệu
    function layDV_Ngoai($donvi)
    {
        $CI =& get_instance();
        $CI->load->model('vanbandi/Mvanbandi');
		$donvi          =  explode(',', $donvi);
		$thongtin       = $CI->Mvanbandi->layTT_DVNgoai($donvi);
		$thongtin_      = array_column($thongtin, 'sTenDV');
		$thongtin_donvi = implode(',', $thongtin_);
        return $thongtin_donvi;
    }

	function layQTChuyenDi($mavaban)
    {
        $CI =& get_instance();
        $CI->load->model('vanbandi/Mtruyennhan');
		
		return $CI->Mtruyennhan->layQTChuyen($mavaban);
    }	
	
	function layKQChuyenDi($mavaban)
    {
        $CI =& get_instance();
        $CI->load->model('vanbandi/Mtruyennhan');
		
		return $CI->Mtruyennhan->layKQChuyen($mavaban);
    }	
	
	function layTTCB($maCB)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layTTCB($maCB);
	}
	
	function layDoKhan($maDK)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDoKhan($maDK);
	}

	function layDoMat($maDM)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDoMat($maDM);
	}
	
	function laysoluongdaduyet()
	{
		$CI =& get_instance();
        $CI->load->model('vanbandi/Mvanbandi');
		return $CI->Mvanbandi->laysoluongdaduyet();
	}
	function layvbdidaduyet()
	{
		$CI =& get_instance();
        $CI->load->model('vanbandi/Mvanbandi');
		return $CI->Mvanbandi->layvbdidaduyet();
	}
	function layTTGD()
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layTTGD();
	}
	
	function layDSFile($mavbdi)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavbdi,'tbl_files_vbdi');
	}
	
	function layTLHop($mavbdi)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavbdi,'tbl_files_tlhop');
	}
	
	function layTLHop2($mavbden)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDuLieu('FK_iMaVBDen',$mavbden,'tbl_files_vbden');
	}
	
	function layCBDuyet($mavbden)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDuLieu('FK_iMaVBDen',$mavbden,'tbl_luuvet_giahan');
	}
	
	function layThamMuuCVP($mavbden)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mvanbanchoxuly');
		return $CI->Mvanbanchoxuly->layThamMuuCVP($mavbden);
	}
	
	function layDSFileDi($mavbdi)
	{
		$CI =& get_instance();
        $CI->load->model('vanbandi/Mvanbandi');
		return $CI->Mvanbandi->layDSFileDi($mavbdi);
	}
	
	function layemaildonvi()
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_emaildonvi');
	}
	
	function demSoVB($ma)
	{		
		$CI =& get_instance();
        $CI->load->model('hoso/Mhoso');
		return $CI->Mhoso->demSoVB($ma);
	}
	
	function demSoVBDen($ma)
	{		
		$CI =& get_instance();
        $CI->load->model('hoso/Mhoso');
		return $CI->Mhoso->demSoVBDen($ma);
	}
	
	function limit_word($text,$word)
    {
        $mang = explode(' ',$text);
        $tong = count($mang);
        $ketqua = '';
        if($tong>=$word)
        {
            for($i=0;$i<$word;$i++)
            {
                $ketqua = $ketqua.$mang[$i].' ';
            }
            return $ketqua.' ...';
        }
        else{
            return $text;
        }
    }
	
	function laychidao($idAppointment)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_chidao');
	}
	
	function laydsfilettnb($mavanban)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layDuLieu('FK_iMaVB',$mavanban,'tbl_files_vb');
	}
	
	function layluuvetcuoivbden($mavanban)
	{
		$CI =& get_instance();
        $CI->load->model('danhmuc/Mdanhmuc');
		return $CI->Mdanhmuc->layluuvetcuoivbden($mavanban);
	}
	function layGopY($mavanban)
	{
		$CI =& get_instance();
        $CI->load->model('thongtinnoibo/Msoanthaothongtin');
		return $CI->Msoanthaothongtin->layGopY($mavanban);
	}
	
	function demVanBanMoiNhan2($idcb)
	{
		$CI =& get_instance();
        $CI->load->model('thongtinnoibo/Msoanthaothongtin');
		return $CI->Msoanthaothongtin->demVanBanMoiNhan2($idcb,0);
	}

	function timkiemkytu($str){
		$pos = strpos($str, "(HOÃN");
 
		if ($pos === false) {
		    return 0;
		} else {
		    return 1;
		}
	}
		
?>