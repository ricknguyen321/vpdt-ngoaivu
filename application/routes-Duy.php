<?php
/**
 * Created by PhpStorm.
 * User: MinhDuy
 * Date: 6/15/2017
 * Time: 10:18 AM
 */
$route['chuyenvienxuly'] = 'vanbanden/Cchuyenvienxuly';
$route['vanbanchoxuly'] = 'vanbanden/Cvanbanchoxuly';
$route['phongphoihop'] = 'vanbanden/Cphongphoihop';

// 15/6/2017
$route['vanbanden/[0-9]+']        = 'vanbanden/Cvanbanden';
$route['quatrinhxuly/[0-9]+']     = 'vanbanden/Cquatrinhxuly';
$route['vanbanchoxuly_gd']     = 'vanbanden/Cvanbanchoxuly_gd';
$route['vanbanchoxuly_pgd']     = 'vanbanden/Cvanbanchoxuly_pgd';
$route['vanbanchoxuly_tp']     = 'vanbanden/Cvanbanchoxuly_tp';
$route['vanbanchoxuly_pp']     = 'vanbanden/Cvanbanchoxuly_pp';
$route['vanbanchoxuly_cv']     = 'vanbanden/Cvanbanchoxuly_cv';
$route['vanbanchoxuly_cvph']     = 'vanbanden/Cvanbanchoxuly_cvph';