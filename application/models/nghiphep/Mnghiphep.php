<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnghiphep extends CI_Model {
	
	public function layDSNghiPhep($limit=NULL,$offset=NULL,$tonghop=NULL,$canbo=NULL)
	{		
		if(!empty($tonghop))
		{
			$this->db->like('ly_do',$tonghop);
			$this->db->or_like('ho_ten',$tonghop);
			$this->db->or_like('loai_phep',$tonghop);
			$this->db->or_like('loaiphep',$tonghop);		
		}
		if(!empty($canbo))
		{
			$this->db->where('FK_iMaCB',$canbo);
		}
		$this->db->select('np.*');
		$this->db->from('tbl_nghi_phep as np');		
		$this->db->order_by('ngay_tu','desc');
		$this->db->order_by('id','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function demDSNghiPhep($tonghop=NULL,$canbo=NULL)
	{
		if(!empty($tonghop))
		{
			$this->db->like('ly_do',$tonghop);
			$this->db->or_like('ho_ten',$tonghop);
			$this->db->or_like('loai_phep',$tonghop);
			$this->db->or_like('loaiphep',$tonghop);		
		}		
		if(!empty($canbo))
		{
			$this->db->where('FK_iMaCB',$canbo);
		}
		$this->db->select('np.*');
		$this->db->from('tbl_nghi_phep as np');
		$this->db->order_by('ngay_tu','desc');
		return $this->db->get()->num_rows();
	}
	
	public function demNghiPhepMoi()
	{
		$query = $this->db->query("SELECT * FROM tbl_nghi_phep where ngay_den >= date(now());");
        return $query->num_rows();
	}
	
	public function demPhepChoDuyet($idcb=NULL,$phongban=NULL,$quyen=NULL)
	{
		if ($quyen == 3 || $quyen == 6) {
			$query = $this->db->query('SELECT * FROM tbl_nghi_phep where FK_iMaCB != '.$idcb.' && FK_iMaPhongHD = '.$phongban.' && tp_duyet = 0 && bgd_duyet = 0;');
		} 
		if ($quyen == 4 || $quyen == 5) {
			$query = $this->db->query('SELECT np.*, tbl_canbo.iQuyenHan_DHNB FROM tbl_nghi_phep np join tbl_canbo on np.FK_iMaCB=tbl_canbo.PK_iMaCB where (tp_duyet = 1 && bgd_duyet = 0) || (tp_duyet = 0 && bgd_duyet = 0 && (iQuyenHan_DHNB = 3 || iQuyenHan_DHNB = 6));');
		} 
        return $query->num_rows();
	}
	
	public function layldduyetnghi($phong){
		$query = $this->db->query('SELECT * FROM tbl_canbo where FK_iMaPhongHD= '.$phong.' and (iQuyenHan_DHNB = 6 || iQuyenHan_DHNB = 3 || iQuyenHan_DHNB = 7) && itrangthai = 0 order by  iQuyenHan_DHNB asc, PK_iMaCB asc;');
		return $query->result_array();
    }
	
	public function layldvp(){
		$query = $this->db->query('SELECT * FROM tbl_canbo where FK_iMaPhongHD= 11 and (iQuyenHan_DHNB = 3 || iQuyenHan_DHNB = 7) && itrangthai = 0 order by  iQuyenHan_DHNB asc, PK_iMaCB asc;');
		return $query->result_array();
    }
	public function laygd(){
		$query = $this->db->query('SELECT * FROM tbl_canbo where iQuyenHan_DHNB = 4 && itrangthai = 0 order by PK_iMaCB asc;');
		return $query->result_array();
    }
	
	public function laydschoduyet($canbo=NULL)
	{		
		$this->db->where('cb_giu',$canbo);
		$this->db->where('bgd_duyet',0);
		$this->db->select('np.*');
		$this->db->from('tbl_nghi_phep as np');
		return $this->db->get()->result_array();
	}
	
}