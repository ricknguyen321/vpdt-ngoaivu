<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdonvicaphai extends CI_Model {

	// lấy danh sách phòng ban
	public function layDSPB()
	{
		$this->db->where('sepxep>',0);
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaPB,sTenPB');
		return $this->db->get('tbl_phongban')->result_array();
	}
	public function layDSPB_CH()
	{
		$this->db->where_in('PK_iMaPB',array(12,68));
		$this->db->where('sepxep>',0);
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaPB,sTenPB');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// lấy cán bộ theo phòng ban
	public function layDSCB($phongban)
	{
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->where('iTrangThai',0);
		$this->db->where('iQuyenHan_DHNB >',1);
		$this->db->select('PK_iMaCB,sHoTen,quyenhan_caphai,phong_caphai,tendinhdanh');
		$this->db->order_by('phong_caphai','asc');
		$this->db->order_by('quyenhan_caphai','asc');
		return $this->db->get('tbl_canbo')->result_array();
	}

}

/* End of file Mdonvicaphai.php */
/* Location: ./application/models/quantricapcao/Mdonvicaphai.php */