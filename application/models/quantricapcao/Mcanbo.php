<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcanbo extends CI_Model {

	// lấy thông tin có sắp xếp và where theo trạng thái
	public function layDL($primary_key=NULL,$order=NULL,$table)
	{
		$this->db->where('iTrangThai',0);
		if(!empty($primary_key))
		{
			$this->db->order_by($primary_key,$order);
		}
		return $this->db->get($table)->result_array();
	}
	public function layDSCB($phongban=NULL,$chucvu=NULL,$quyen=NULL,$hoten=NULL,$limit=NULL,$offset=NULL)
	{
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPhongHD',$phongban);
		}
		if(!empty($chucvu))
		{
			$this->db->where('FK_iMaCV',$chucvu);
		}
		if(!empty($quyen))
		{
			$this->db->where('iQuyenHan_DHNB',$quyen);
		}
		if(!empty($hoten))
		{
			$this->db->like('sHoTen',$hoten);
		}
		$this->db->where('iTrangThai',0);
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get('tbl_canbo')->result_array();
	}
	public function demDSCB($phongban=NULL,$chucvu=NULL,$quyen=NULL,$hoten=NULL)
	{
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPhongHD',$phongban);
		}
		if(!empty($chucvu))
		{
			$this->db->where('FK_iMaCV',$chucvu);
		}
		if(!empty($quyen))
		{
			$this->db->where('iQuyenHan_DHNB',$quyen);
		}
		if(!empty($hoten))
		{
			$this->db->like('sHoTen',$hoten);
		}
		$this->db->where('iTrangThai',0);
		return $this->db->get('tbl_canbo')->num_rows();
	}
}

/* End of file Mcanbo.php */
/* Location: ./application/models/quantricapcao/Mcanbo.php */