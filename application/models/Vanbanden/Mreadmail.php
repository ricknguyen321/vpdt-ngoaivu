<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mreadmail extends CI_Model {


	// bắt trùng mail lấy về
	public function battrungMail($mail_subject,$mail_from,$mail_date){
		$this->db->where('mail_subject',$mail_subject);
		$this->db->where('mail_from',$mail_from);
		$this->db->where('mail_date',$mail_date);
		return $this->db->get('tbl_mail')->num_rows();
	}
	// lấy danh sách mail
	public function layMail($chude=NULL,$trangthai,$limit=NULL,$offset=NULL)
	{
		$this->db->where('mail_active',1);
		
		if(!empty($chude))
		{
			$this->db->like('mail_subject',$chude);
			$this->db->or_like('mail_from',$chude);
		} 
		else {
			$this->db->where('mail_status',$trangthai);
		}
		
		
		$this->db->order_by('mail_date','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get('tbl_mail')->result_array();
	}
	// đếm danh sách email
	public function demMail($chude=NULL,$trangthai)
	{
		$this->db->where('mail_active',1);
		$this->db->where('mail_status',$trangthai);
		if(!empty($chude))
		{
			$this->db->like('mail_subject',$chude);
			$this->db->or_like('mail_from',$chude);
		}
		return $this->db->get('tbl_mail')->num_rows();
	}
}

/* End of file Mreadmail.php */
/* Location: ./application/models/Vanbanden/Mreadmail.php */