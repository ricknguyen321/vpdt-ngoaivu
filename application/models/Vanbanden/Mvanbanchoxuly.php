<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/20/2017
 * Time: 2:58 PM
 */
class Mvanbanchoxuly extends CI_Model {
    public function getAccount($trangthai=NULL,$QuyenHan_DHNB=NULL,$FK_iMaPhongHD=NULL,$QuyenHan_DHNBPP=NULL)
    {
        $this->db->where('iTrangThai',0);
        if(!empty($QuyenHan_DHNB)){
            $this->db->where('iQuyenHan_DHNB',$QuyenHan_DHNB);
        }
        if(!empty($FK_iMaPhongHD)){
            $this->db->where('FK_iMaPhongHD',$FK_iMaPhongHD);
        }
        if(!empty($QuyenHan_DHNBPP)){
            $this->db->where_in('iQuyenHan_DHNB',$QuyenHan_DHNBPP);
        }
        $this->db->select('tbl_canbo.PK_iMaCB,tbl_canbo.sHoTen');
        $this->db->from('tbl_canbo');
        return $this->db->get()->result_array();
    }
	public function getAccountPH($trangthai=NULL,$QuyenHan_DHNB=NULL,$FK_iMaPhongHD=NULL,$QuyenHan_DHNBPP=NULL)
    {
        $this->db->where('iTrangThai',0);
        if(!empty($QuyenHan_DHNB)){
            $this->db->where_in('iQuyenHan_DHNB',$QuyenHan_DHNB);
        }
        if(!empty($FK_iMaPhongHD)){
            $this->db->where('FK_iMaPhongHD',$FK_iMaPhongHD);
        }
        if(!empty($QuyenHan_DHNBPP)){
            $this->db->where_in('iQuyenHan_DHNB',$QuyenHan_DHNBPP);
        }
        $this->db->select('tbl_canbo.PK_iMaCB,tbl_canbo.sHoTen');
        $this->db->from('tbl_canbo');
        return $this->db->get()->result_array();
    }
    public function getDepartment($trangthai=NULL)
    {
        if(isset($trangthai)){
            $this->db->where('tbl_phongban.iTrangThai',$trangthai);
        }
        $this->db->where('tbl_phongban.iLoaiPhanMem !=',2);
        $this->db->where_in('iQuyenHan_DHNB',array(3,4,6));
        $this->db->select('tbl_phongban.PK_iMaPB,tbl_phongban.sTenPB,tbl_canbo.PK_iMaCB');
        $this->db->from('tbl_phongban');
        $this->db->join('tbl_canbo','tbl_canbo.FK_iMaPhongHD = tbl_phongban.PK_iMaPB','left');
        $this->db->group_by('tbl_phongban.PK_iMaPB');
        return $this->db->get()->result_array();
    }
    // danh sách văn bản chờ xử lý
    public function getDocAwait($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL,$tocongtac=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai',0);
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        if(!empty($idDepartment1) || !empty($Chuyenvien)){
            $this->db->select('tbl_vanbanden.sCVTuChoi,tbl_vanbanden.sTenLV,tbl_vanbanden.iDeadline,tbl_vanbanden.FK_iMaDK,tbl_vanbanden.FK_iMaDM,tbl_chuyennhanvanban.sMoTa as smotald,tbl_chuyennhanvanban.PK_iMaCBChuyen,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,tbl_vanbanden.vbdaura_tp,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,iSTCChuTri,iVanBanTBKL,iToCongTac,iTrangThai_ThemHan,sLyDoTuChoiKetQua,sLyDoTuChoiVanBanDen,iSTCPhoiHop');
        }else{
            $this->db->select('tbl_vanbanden.sCVTuChoi,tbl_vanbanden.sTenLV,tbl_vanbanden.iDeadline,tbl_vanbanden.FK_iMaDK,tbl_vanbanden.FK_iMaDM,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,tbl_vanbanden.vbdaura_tp,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,iSTCChuTri,iVanBanTBKL,iToCongTac,iTrangThai_ThemHan,sLyDoTuChoiKetQua,sLyDoTuChoiVanBanDen,iSTCPhoiHop');
        }
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
		$this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=',3);
//            $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
             $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=',3);
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
             $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=',3);
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
             $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=',3);
			$this->db->where('tbl_vanbanden.iToCongTac !=','1');
        }
        if(!empty($tocongtac)){
            $this->db->where('tbl_vanbanden.iToCongTac !=',1);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sHanThongKe','asc');
//        $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
    // danh sách văn bản theo dõi
    public function getDocFollow($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$PGDThamMuu=NULL,$PPThamMuu=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            //$this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        if($PGDThamMuu){
            $this->db->where('tbl_vanbanden.iPgdThamMuu',$PGDThamMuu);
        }
        if($PPThamMuu){
            $this->db->where('tbl_vanbanden.iPpThamMuu',$PPThamMuu);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sHanThongKe','asc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
	
	
	public function demDocFollow($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$PGDThamMuu=NULL,$PPThamMuu=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            //$this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        if($PGDThamMuu){
            $this->db->where('tbl_vanbanden.iPgdThamMuu',$PGDThamMuu);
        }
        if($PPThamMuu){
            $this->db->where('tbl_vanbanden.iPpThamMuu',$PPThamMuu);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.sMoTa');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
		return $this->db->get()->num_rows();
    }
	
	
	
    // danh sách chờ của chi cục
    public function getDocConcacAwait($trangthai_truyennhan = NULL, $tcdn_active = NULL,$department_id=NULL,$ccp_id=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai',0);
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.*,count(tbl_files_vbden.FK_iMaVBDen) as dem');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($tcdn_active)){
            $this->db->where('tbl_chicuc.tcdn_active',$tcdn_active);
        }
        if(!empty($department_id)){
            $this->db->where('tbl_chicuc.department_id',$department_id);
        }
        if(!empty($ccp_id)){
            $this->db->where('tbl_chicuc.tp_id',$ccp_id);
        }
        if(!empty($Chuyenvien)){
            $this->db->where('tbl_chicuc.ccp_id',$Chuyenvien);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
    // văn bản chờ phê duyệt
    public function getDocPheDuyet($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$pheduyet=NULL,$pheduyetCV=NULL,$Truongphong=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        if($pheduyet == 2){
            $this->db->where('tbl_vanbanden.iTrangThai',1);
        }
		if($pheduyet == 1){
            $this->db->where('tbl_vanbanden.iTrangThai',2);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.sHanThongKe,tbl_vanbanden.sMoTa,tbl_vanbanden.PK_iMaCBDuyet,tbl_vanbanden.lydochammuon,tbl_vanbanden.iToCongTac,tbl_vanbanden.PK_iMaCBHoanThanh,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_files_vbden.sDuongDan,tbl_files_vbden.sDuongDan,tbl_vanbanden.iCoSangTao,tbl_vanbanden.sLuuVaoKho,tbl_vanbanden.vbdaura_tp,tbl_vanbanden.vbdaura_cv,count(tbl_files_vbden.FK_iMaVBDen) as dem,count(tbl_chuyennhanvanban.PK_iMaCVCT) as demchuyennhan,tbl_chuyennhanvanban.PK_iMaCBChuyen,tbl_chuyennhanvanban.PK_iMaPhongCT');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_chuyennhanvanban','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');
        if($pheduyetCV){
            $this->db->where_in('tbl_vanbanden.iTrangThai',array('1','2'));
        }
        if(!empty($idDepartment)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
        }
        if(!empty($idDepartment1)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        }
        if(!empty($Chuyenvien)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
        }
        
        if(!empty($Truongphong)){
            $this->db->where('tbl_vanbanden.PK_iMaCBDuyet',$Truongphong);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
		
		if(!empty($Chuyenvien)){
            $this->db->order_by('tbl_vanbanden.iTrangThai','desc');
        } else {
			$this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
		}
		
//        return $this->db->last_query();
//       pr($this->db->get()->result_array());
        return $this->db->get()->result_array();
    }
    // danh sách chỉ đạo nhưng chưa xử lý ( các phòng )
    public function getDocTPCDAwait($idDepartment1=NULL,$idDepartment=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$stcphoihop=NULL){

        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($stcphoihop)){
            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);
        }else{
            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);
        }
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        if($idDepartment1){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);
            $this->db->where_in('tbl_chuyennhanvanban.CapGiaiQuyet',array('7','8'));
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.vbdaura_tp,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.iPpThamMuu,tbl_vanbanden.sNgayNhap,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_files_vbden.sDuongDan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//        if(!empty($idDepartment)){
//            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//
//        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->result_array();
    }
    // danh sách hoàn thành của chi cục
    // danh sách chỉ đạo nhưng chưa xử lý ( các phòng )
    public function getConCac($idDepartment1=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$stcphoihop=NULL){
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($idDepartment1)){
            $this->db->where('tbl_chicuc.department_id',$idDepartment1);
        }
        if(!empty($stcphoihop)){
            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);
        }else{
            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);
        }
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_chicuc.tcdn_active >=',2);
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sHanThongKe,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_files_vbden.sDuongDan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->result_array();
    }
    // van ban cho xu ly pho chu chi
    public function getPConCac($idDepartment1=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$stcphoihop=NULL){

        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($idDepartment1)){
            $this->db->where('tbl_chicuc.ccp_id',$idDepartment1);
        }
        if(!empty($stcphoihop)){
            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);
        }else{
            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);
        }
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where_in('tbl_chicuc.tcdn_active ',array(3,4));
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.vbdaura_tp,tbl_vanbanden.sHanThongKe,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_files_vbden.sDuongDan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->result_array();
    }
    // danh sách văn bản chờ xử lý
    public function getDocComplete($idDepartment=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($idDepartment)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.iTrangThai','1');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_files_vbden.sDuongDan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->result_array();
    }
    // danh sách văn bản phân loại nhưng lãnh đạo chưa xử lý
    public function getDocDirAwait($loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL){
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($idDepartment)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment);
        }
        $this->db->where_in('tbl_vanbanden.iTrangThai_TruyenNhan',array('3', '4'));
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->select('tbl_chuyennhanvanban.sVBQT,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_files_vbden.sDuongDan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->result_array();
    }
    // văn bản bị từ chối
    public function getDocReject($loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL){
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($idDepartment)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
//        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        //$this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        $this->db->select('PK_iMaCBTuChoi,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanbanden.sHanThongKe');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->where('tbl_vanban_chuyenlai.iTrangThai','1');
        $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->result_array();
    }
    public function getDocAwaitPPH($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        if($Quyen == 6 || $Quyen == 3){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');
			
			//ricknguyen321 Truong phong co the xu ly van ban phoi hop
			$this->db->where(' NOT EXISTS ( SELECT * FROM tbl_file_ketqua WHERE PK_iMaPB = '.$idDepartment1.' AND tbl_chuyennhanvanban.PK_iMaVBDen = tbl_file_ketqua.PK_iMaVBDen)');
            //$this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
            // $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',5);
        }
        if($Quyen == 7){
			$this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
            $this->db->where('tbl_phongphoihop.iTrangThai',2);
			$this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);
            //$this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);
            //$this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
            //$this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'" AND (iTrangThai = 1 OR iTrangThai = 3))');
        }
        if($Quyen == 11){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
            $this->db->where('tbl_phongphoihop.iTrangThai',11);
            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);
            // $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');
        }
        $this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('tbl_vanbanden.iTrangThai !=',1);
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi',2);
        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
        $this->db->select('tbl_phongphoihop.PK_iMaPhongPH,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sTenLVB,tbl_vanbanden.iSoTrang,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sNgayKy,COUNT(tbl_phongphoihop.PK_iMaVBDen) as dem,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan,tbl_phongphoihop.PK_iMaCB');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->result_array();
    }
    public function getDocAwaitCVPPH($trangthai_truyennhan = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        if($idUser){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
        }
        if($iTrangThaiPH){
            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if($Status){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$Status);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
		$this->db->where('tbl_vanbanden.iTrangThai !=','1');
		$this->db->where('tbl_phongphoihop.iTrangThai','1');
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        //$this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d',time()));
        $this->db->select('tbl_vanbanden.*,COUNT(tbl_phongphoihop.PK_iMaVBDen) as dem,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan ');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment1)){
            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        }
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        if(!empty($dem)){
            if($dem == 0){
                $this->db->having('dem = '.$dem, NULL, FALSE);
            }elseif($dem == 1){
                $this->db->having('dem > 0', NULL, FALSE);
            }
        }
        return $this->db->get()->result_array();
    }
    // danh sách phối hợp chủ trì
    public function getDocAwaitDSCTPPH($trangthai_truyennhan = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        if($idUser){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
        }
        if($iTrangThaiPH){
            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if($Status){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$Status);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->select('tbl_vanbanden.*,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan ');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment1)){
            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        }
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }

    // danh sách phối hợp chủ trì
    public function getDocAwaitDSCTPPH1($trangthai_truyennhan = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL,$PK_iMaVBDen=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        if($idUser){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
        }
        if($iTrangThaiPH){
            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if($Status){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$Status);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->select('tbl_vanbanden.*,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan ');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment1)){
            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        }
        if(!empty($PK_iMaVBDen)){
            $this->db->where('tbl_vanbanden.PK_iMaVBDen in('.$PK_iMaVBDen.')');
        }
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }

     // danh sách phối hợp chủ trì
    public function getDocAwaitDSCTPPH2($PK_iMaVBDen=NULL)
    {
        $this->db->select('tbl_vanbanden.*,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan ');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');            
        if(!empty($PK_iMaVBDen)){
            $this->db->where('tbl_vanbanden.PK_iMaVBDen in('.$PK_iMaVBDen.')');
        }
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        return $this->db->get('')->result_array();
    }
    // danh sach cho xu ly cua chuyen vien phoi hop
    public function getDocAwaitCVPH($Chuyenvien=NULL)
    {
        if($Chuyenvien){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$Chuyenvien);
        }
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_phongphoihop.iTrangThai','1');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        return $this->db->get()->result_array();
    }
    /**
     * insert_bacth
     */
    public function themNhieuDuLieu($table,$data)
    {
        $this->db->insert_batch($table,$data);
        return $this->db->affected_rows();
    }
    /**
     * Xoa
     */
    public function xoaDuLieu($primary_key=NULL,$id=NULL,$table,$phophong=NULL){
        if(!empty($primary_key))
        {
            $this->db->where($primary_key,$id);
        }
        if(!empty($phophong)){
            $this->db->where('PK_iMaCBNhan !=',$phophong);
        }
        $this->db->where('PK_iMaCBNhan !=', '519');
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
    /**
     * Quá trình xử lý văn bản
     */
    // qua trinh truyen nhan van ban
    public function getDocProcess($idDoc = NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        }
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=',3);
        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan !=',"");
        $this->db->select('tbl_chuyennhanvanban.*,tbl_canbo.sHoTen');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_chuyennhanvanban.PK_iMaCBNhan','left');
        return $this->db->get()->result_array();
    }
    // trinh tụ xuống chuyên viên
    public function getDocProcesscv($idDoc = NULL,$idDepartment=NULL,$lever=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        }
        if(!empty($idDepartment)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment);
        }
        if(!empty($lever)){
            $this->db->where('tbl_chuyennhanvanban.CapGiaiQuyet',$lever);
        }
        $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT !=',"");
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        $this->db->select('tbl_chuyennhanvanban.*,tbl_canbo.sHoTen');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_chuyennhanvanban.PK_iMaCVCT','left');
        return $this->db->get()->result_array();
    }
    // trinh tu chuyen vien phoi hop
    public function getDocProcesscvph($Chuyenvien = NULL,$idDoc=NULL)
    {
        if($Chuyenvien){
            $this->db->where('tbl_chuyennhanvanban.*,tbl_phongphoihop.PK_iMaCB',$Chuyenvien);
        }
        if(!empty($idDoc)){
            $this->db->where('tbl_phongphoihop.PK_iMaVBDen',$idDoc);
        }
//        $this->db->where('tbl_phongphoihop.iGiayMoi','2');
        $this->db->select('tbl_canbo.sHoTen');
        $this->db->from('tbl_phongphoihop');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_phongphoihop.PK_iMaCB','left');
//        $this->db->group_by('tbl_phongphoihop.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
    // trinh tư phòng ban
    public function getDocProcessPB($idDoc = NULL,$idDepartment=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        }
        if(!empty($idDepartment)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment);
        }
        // $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT !=',"");
        $this->db->select('tbl_chuyennhanvanban.*,tbl_phongban.sTenPB as sHoTen');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->join('tbl_phongban','tbl_phongban.PK_iMaPB = tbl_chuyennhanvanban.PK_iMaPhongCT','left');
        return $this->db->get()->result_array();
    }
    public function layDuLieu($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->like($primary_key,$id);
        }
        return $this->db->get($table)->result_array();
    }
    public function layFileLast($ma)
    {
        $this->db->where('FK_iMaVBDi',$ma);
        $this->db->select('sDuongDan');
        $this->db->from('tbl_files_vbdi');
        $this->db->order_by('PK_iMaFileDi','desc');
        $this->db->limit(1);
        return $this->db->get()->row_array();
    }
    // trinh tư chuyen vien phong phoi hop
    public function getDocProcessCVPPH($idDoc = NULL,$idPhong=NULL,$idCBChuyen=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_phongphoihop.PK_iMaVBDen',$idDoc);
        }
        if(!empty($idPhong)){
            $this->db->where('tbl_phongphoihop.PK_iMaPhong',$idPhong);
        }
        if(!empty($idCBChuyen)){
            $this->db->where('tbl_phongphoihop.input_per',$idCBChuyen);
        }
        $this->db->select('tbl_canbo.sHoTen,tbl_phongphoihop.sMoTa,tbl_phongphoihop.input_per,tbl_phongphoihop.sThoiGian');
        $this->db->from('tbl_phongphoihop');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_phongphoihop.PK_iMaCB','left');
        return $this->db->get()->result_array();
    }
    public function getDocCVPPH($idDoc = NULL,$idPhong=NULL,$iTrangThai=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_phongphoihop.PK_iMaVBDen',$idDoc);
        }
        if(!empty($idPhong)){
            $this->db->where('tbl_phongphoihop.PK_iMaPhong',$idPhong);
        }
        if(!empty($iTrangThai)){
            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThai);
        }
        $this->db->select('tbl_phongphoihop.PK_iMaCB');
        $this->db->from('tbl_phongphoihop');
        return $this->db->get()->result_array();
    }
    // lấy mã trưởng phòng
    public function getidCB($idDepartment = NULL)
    {
        $this->db->where('iQuyenHan_DHNB',6);
        $this->db->where('iTrangThai',0);
        $this->db->where('FK_iMaPhongHD',$idDepartment);
        $this->db->select('PK_iMaCB,sHoTen');
        $this->db->from('tbl_canbo');
        return $this->db->get()->result_array();
    }
    public function getidCCCB($idDepartment = NULL)
    {
        $this->db->where('iQuyenHan_DHNB',6);
        $this->db->where('FK_iMaCV',15);
        $this->db->where('iTrangThai',0);
        $this->db->where('FK_iMaPhongHD',$idDepartment);
        $this->db->select('PK_iMaCB,sHoTen');
        $this->db->from('tbl_canbo');
        return $this->db->get()->result_array();
    }
    public function getidCBVP($idDepartment = NULL)
    {
        $this->db->where('iQuyenHan_DHNB',3);
        $this->db->where('iTrangThai',0);
        $this->db->where('FK_iMaPhongHD',$idDepartment);
        $this->db->select('PK_iMaCB,sHoTen');
        $this->db->from('tbl_canbo');
        $this->db->order_by('tbl_canbo.PK_iMaCB','desc');
        return $this->db->get()->result_array();
    }
    // danh sách kết quả giải quyết phòng phối hợp

    public function getResultPPH($idDoc = NULL,$idDepartment=NULL,$idCVCT=NULL)

    {
        if(!empty($idDoc)){
            $this->db->where('tbl_file_ketqua.PK_iMaVBDen',$idDoc);
        }
        if($idDepartment){
            $this->db->where('tbl_file_ketqua.PK_iMaPB',$idDepartment);
        }
        $this->db->select('tbl_file_ketqua.*,tbl_canbo.sHoTen,tbl_phongban.sTenPB');
        $this->db->from('tbl_file_ketqua');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_file_ketqua.PK_iMaCB','left');
        $this->db->join('tbl_phongban','tbl_phongban.PK_iMaPB = tbl_file_ketqua.PK_iMaPB','left');
        if($idCVCT){
            $this->db->where('tbl_file_ketqua.PK_iMaPB !=',$idCVCT);
        }
//        $this->db->group_by('tbl_file_ketqua.PK_iMaVBDen');
        return $this->db->get()->result_array();

    }

    // ket qua phong thu ly
    public function getResultPTL($idDoc = NULL,$idDepartment=NULL,$idCVCT=NULL)

    {
        if($idDoc){
            $this->db->where('tbl_file_ketqua.PK_iMaVBDen',$idDoc);
        }
        if($idDepartment){
            $this->db->where('tbl_file_ketqua.PK_iMaPB',$idDepartment);
        }
        if($idCVCT){
            $this->db->where('tbl_file_ketqua.PK_iMaPB',$idCVCT);
        }
        $this->db->select('tbl_file_ketqua.*,tbl_canbo.sHoTen,tbl_phongban.sTenPB');
        $this->db->from('tbl_file_ketqua');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_file_ketqua.PK_iMaCB');
        $this->db->join('tbl_phongban','tbl_phongban.PK_iMaPB = tbl_file_ketqua.PK_iMaPB');
//        $this->db->group_by('tbl_file_ketqua.PK_iMaVBDen');
//        return $this->db->last_query($this->db->get());
        return $this->db->get()->result_array();

    }
    // chuyen vien chu tri
    public function getCVCT($idDoc = NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        }
        $this->db->select('tbl_chuyennhanvanban.PK_iMaCVCT,PK_iMaPhongCT');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT !=',' ');
        return $this->db->get()->result_array();
    }

    public function updatePPH($idDocGo,$idUser,$data=array()){
        $this->db->where('PK_iMaVBDen',$idDocGo);
        $this->db->where('PK_iMaCB',$idUser);
        $this->db->update('tbl_phongphoihop',$data);
        return $this->db->affected_rows();
    }
    // update ket qua hoan thanh trong bang chuyenvienphoihop
    public function updateCVPH($idDocGo,$idUser,$data=array()){
        $this->db->where('PK_iMaVBDen',$idDocGo);
        $this->db->where('PK_iMaCVPH',$idUser);
        $this->db->update('tbl_chuyenvienphoihop',$data);
        return $this->db->affected_rows();
    }
    // cập nhật bảng chuyền nhận văn bản
    public function updateCBCT($idDocGo,$idCBCT,$data=array()){
        $this->db->where('PK_iMaVBDen',$idDocGo);
        $this->db->where('PK_iMaCN',$idCBCT);
        $this->db->update('tbl_chuyennhanvanban',$data);
        return $this->db->affected_rows();
    }
    /**
     * Xóa dữ liệu 2 dieu kien
     */
    public function xoaDuLieu2Where($primary_key=NULL,$id=NULL,$val=NULL,$value=NULL,$table)
    {
        if(!empty($primary_key))
        {
            $this->db->where($primary_key,$id);
        }
        if(!empty($val))
        {
            $this->db->where($val,$value);
        }
        else{
            $this->db->where(1,1);
        }
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
    // kiếm tra lv của cán bộ
    public function TestCB($cb){
        $this->db->where('PK_iMaCB',$cb);
        $this->db->select('iQuyenHan_DHNB');
        $this->db->from('tbl_canbo');
        return $this->db->get()->row_array();
    }
    public function getCN($idDoc=NULL,$idPhong=NULL){
        if(!empty($idDoc))
        {
            $this->db->where('PK_iMaVBDen',$idDoc);
        }
        if(!empty($idPhong))
        {
            $this->db->where('PK_iMaPhongCT',$idPhong);
        }
        $this->db->select('sThoiGianHetHan');
        $this->db->from('tbl_chuyennhanvanban');
        return $this->db->get()->row_array();
    }
    /** danh sách chủ trì */
    public function getDocCT($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
		$this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai!=','3');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,iTrangThai_ThemHan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }

    /** danh sách chủ trì */
    public function getDocCT1($PK_iMaVBDen=NULL,$limit=NULL,$offset=NULL)
    {
       
        //$this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai!=','3');
        $this->db->select('tbl_vanbanden.*,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,iTrangThai_ThemHan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        
        if(!empty($PK_iMaVBDen)){
            $this->db->where('tbl_vanbanden.PK_iMaVBDen in ('.$PK_iMaVBDen.')');
        }

        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }

	public function getDocPPCT($idDoc = NULL,$User=NULL,$PK_iMaPPPH=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_phongphoihop.PK_iMaVBDen',$idDoc);
        }
        if(!empty($User)){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$User);
        }
        if(!empty($PK_iMaPPPH)){
            $this->db->where('tbl_phongphoihop.PK_iMaPPPH',$PK_iMaPPPH);
        }
        $this->db->select('tbl_phongphoihop.sMoTa as smotanoidung');
        $this->db->from('tbl_phongphoihop');
        return $this->db->get()->result_array();
    }
	public function laylanhdaosaucung($idDoc = NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        }
        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan !=',"");
        //$this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        $this->db->select('tbl_canbo.PK_iMaCB,tbl_canbo.sHoTen,tbl_canbo.iQuyenHan_DHNB');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_chuyennhanvanban.PK_iMaCBNhan','left');
        $this->db->order_by('tbl_chuyennhanvanban.PK_iMaCN','desc');
        return $this->db->get()->row_array();
    }
    // van ban cho xu ly cong tác đảng
    public function getDocAwaitCTD($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','3');
        if(!empty($idDepartment1) || !empty($Chuyenvien)){
            $this->db->select('tbl_chuyennhanvanban.sMoTa as smotald,tbl_chuyennhanvanban.PK_iMaCBChuyen,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu');
        }else{
            $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu');
        }
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
//            $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
//        $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
	public function getPhongBan($idUser){
        $this->db->where('PK_iMaCB',$idUser);
        $this->db->select('sTenPB');
        $this->db->from('tbl_phongban');
        $this->db->join('tbl_canbo','tbl_canbo.FK_iMaPhongHD = tbl_phongban.PK_iMaPB');
        return $this->db->get()->row_array();
    }
	 public function getDocAwaitPPPH($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        if($Quyen == 7){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
            $this->db->where('tbl_phongphoihop.iTrangThai',2);
        }
        if($giaymoi == 1){
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }else{
            $this->db->where('tbl_vanbanden.iGiayMoi',2);
        }
        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
        $this->db->where('tbl_phongphoihop.sThoiGian >=','2017-10-11');
        $this->db->select('tbl_phongphoihop.PK_iMaPhongPH,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sTenLVB,tbl_vanbanden.iSoTrang,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sNgayKy,COUNT(tbl_phongphoihop.PK_iMaVBDen) as dem,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan,tbl_phongphoihop.PK_iMaCB');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        return $this->db->get()->result_array();
    }
    public function xoaDuLieu3Where($primary_key=NULL,$id=NULL,$val=NULL,$value=NULL,$val1=NULL,$value1=NULL,$table)
    {
        if(!empty($primary_key))
        {
            $this->db->where($primary_key,$id);
        }
        if(!empty($val))
        {
            $this->db->where($val,$value);
        }
        if($value1 == 1)
        {
            $this->db->where($val1,$value1);
        }else{
            $this->db->where($val1,$value1);
        }
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
	public function capnhatkehoach($primary_key=NULL,$id=NULL,$val=NULL,$value=NULL,$val1=NULL,$value1=NULL,$data=NULL){
	    if(!empty($primary_key))
        {
            $this->db->where($primary_key,$id);
        }
        if(!empty($val))
        {
            $this->db->where($val,$value);
        }
        if(!empty($value1))
        {
            $this->db->where($val1,$value1);
        }
        $this->db->update('kehoach',$data);
        return $this->db->affected_rows();
    }
    public function getDocAwaitCTC($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL,$tocongtac=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        if(!empty($idDepartment1) || !empty($Chuyenvien)){
            $this->db->select('tbl_chuyennhanvanban.sMoTa as smotald,tbl_chuyennhanvanban.PK_iMaCBChuyen,tbl_vanbanden.sMoTa,tbl_vanbanden.iTrangThai_ThemHan,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,tbl_vanban_chuyenlai.sTenCB,iVanBanQPPL,iSTCChuTri,iVanBanTBKL,iToCongTac');
        }else{
            $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,iSTCChuTri,iVanBanTBKL,iToCongTac');
        }
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
//            $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($tocongtac)){
            $this->db->where('tbl_vanbanden.iToCongTac',$tocongtac);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sHanThongKe','asc');
//        $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
	public function getDonThu($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
		$this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
        }
        if(!empty($idDepartment1)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        }
        if(!empty($idDepartmentCT)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
        }
        if(!empty($Chuyenvien)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if($cacloaivanban == '2'){
            $this->db->where('tbl_vanbanden.iTrangThai',1);
        }else{
            $this->db->where('tbl_vanbanden.iTrangThai',0);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
	public function getListCVPH($trangthai_truyennhan = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        if($idUser){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
        }
        if($iTrangThaiPH){
            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if($Status){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$Status);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->select('tbl_vanbanden.*,COUNT(tbl_phongphoihop.PK_iMaVBDen) as dem,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan ');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment1)){
            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        }
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
	// danh sách văn bản chờ xử lý
    public function getListCTDang($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL,$tocongtac=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','3');
        if(!empty($idDepartment1) || !empty($Chuyenvien)){
            $this->db->select('tbl_chuyennhanvanban.sMoTa as smotald,tbl_chuyennhanvanban.PK_iMaCBChuyen,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,iSTCChuTri,iVanBanTBKL,iToCongTac');
        }else{
            $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,iSTCChuTri,iVanBanTBKL,iToCongTac');
        }
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
//            $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');
        }
        if(!empty($tocongtac)){
            $this->db->where('tbl_vanbanden.iToCongTac !=',1);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
//        $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
    // danh sách chờ của chi cục
    public function getDocCCDangAwait($trangthai_truyennhan = NULL, $tcdn_active = NULL,$department_id=NULL,$ccp_id=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','3');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($tcdn_active)){
            $this->db->where('tbl_chicuc.tcdn_active',$tcdn_active);
        }
        if(!empty($department_id)){
            $this->db->where('tbl_chicuc.department_id',$department_id);
        }
        if(!empty($ccp_id)){
            $this->db->where('tbl_chicuc.tp_id',$ccp_id);
        }
        if(!empty($Chuyenvien)){
            $this->db->where('tbl_chicuc.ccp_id',$Chuyenvien);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
	// phó phòng phối hợp từ chối
    public function PPPHTuChoi($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->where('tbl_vanbanden.iGiayMoi',2);
        $this->db->where('tbl_phongphoihop.iTRangThai_TuChoi',1);
        $this->db->where('tbl_phongphoihop.PK_iMaPhong',$idDepartment1);
        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
        $this->db->select('tbl_phongphoihop.PK_iMaPhongPH,tbl_phongphoihop.sNoiDung_TuChoi,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sTenLVB,tbl_vanbanden.iSoTrang,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sNgayKy,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan,tbl_phongphoihop.PK_iMaCB');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        return $this->db->get()->result_array();
    }
    public function countDocGDAwait($idDepartment1=NULL,$giamdoc=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$stcphoihop=NULL){
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($stcphoihop)){
            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);
        }else{
            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);
        }
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.iDuHop,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNoiDung,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_files_vbden.sDuongDan,tbl_vanbanden.sHanThongKe');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($giamdoc)){
            if($giamdoc == 1){
                if($idDepartment1){
                    $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
                    $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);
                    $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >','3');
                    $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
                }
            }else{
                if($idDepartment1){
                    $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
                    $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);
                    $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >','5');
                    $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
                }
            }
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.PK_iMaVBDen','desc');
        return $this->db->get()->num_rows();
    }
    // giám đốc đã xử lý
    public function getDocGDAwait($idDepartment1=NULL,$giamdoc=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$stcphoihop = NULL){
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        $this->db->where('tbl_vanbanden.iTrangThai','0');
        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        
        //$this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.iDuHop,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNoiDung,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_files_vbden.sDuongDan,tbl_vanbanden.sHanThongKe,tbl_vanbanden.iPgdThamMuu');
		$this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.iDuHop,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanThongKe,tbl_vanbanden.iPgdThamMuu');
        $this->db->from('tbl_vanbanden');
        //$this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($giamdoc)){
            if($giamdoc == 1){
                if($idDepartment1){
                    $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
                    $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);
                    $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >','3');
                    $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
                }
            }else{
                if($idDepartment1){
                    $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
                    $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);
                    $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >','3');
                    $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
                }
            }
        }
        if(!empty($stcphoihop)){
            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);
        }else{
            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.PK_iMaVBDen','desc');
        return $this->db->get()->result_array();
    }
	/** danh sách hạn đã đề xuất */
    public function layVBGiaHanTuChoi($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
		$this->db->where('tbl_vanbanden.iTrangThai != ',1);
        $this->db->where_in('tbl_vanbanden.iTrangThai_ThemHan',array(1,2,3));
//        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.iLanhDao,tbl_vanbanden.sMoTa,sLyDoTuChoiHan,iTrangThai_ThemHan,sHanChoDuyet,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,iTrangThai_ThemHan, tbl_vanbanden.sMoTa,sNoiDungGiaHan,');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
        }
        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.PK_iMaVBDen','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
    public function getDocDaPPH($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$iTrangThaiPH=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        if($Quyen == 6 || $Quyen == 3){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'" AND (iTrangThai = 1 OR iTrangThai = 2))');
            //$this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        }
        if($Quyen == 7){
           $this->db->where('tbl_phongphoihop.input_per',$idUser);
           $this->db->where('tbl_phongphoihop.iTrangThai',1);
            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);
            //$this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
            // $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE input_per = "'.$idUser.'" AND (iTrangThai = 1))');
        }
        if($Quyen == 11){
            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);
            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);
            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);
//            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');
        }
        $this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('tbl_vanbanden.iTrangThai !=',1);
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_phongphoihop.PK_iMaPhongPH,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sTenLVB,tbl_vanbanden.iSoTrang,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sNgayKy,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan,tbl_phongphoihop.PK_iMaCB');
        $this->db->from('tbl_chuyennhanvanban');
        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.PK_iMaVBDen','desc');
        return $this->db->get()->result_array();
    }
    // can bo phoi hop
    public function getDocProcessCBPH($idDoc = NULL,$idPhong=NULL,$idCBChuyen=NULL,$iTrangThai=NULL,$iTrangThaiPPH=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_phongphoihop.PK_iMaVBDen',$idDoc);
        }
        if(!empty($idPhong)){
            $this->db->where('tbl_phongphoihop.PK_iMaPhong',$idPhong);
        }
        if(!empty($idCBChuyen)){
            $this->db->where('tbl_phongphoihop.input_per',$idCBChuyen);
        }
        if(!empty($iTrangThai)){
            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThai);
        }
        if(!empty($iTrangThaiPPH)){
            $this->db->where('tbl_phongphoihop.PK_iMaPPPH',$iTrangThaiPPH);
        }
        $this->db->select('tbl_canbo.sHoTen,tbl_phongphoihop.sMoTa,tbl_phongphoihop.PK_iMaCB');
        $this->db->from('tbl_phongphoihop');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_phongphoihop.PK_iMaCB','left');
        return $this->db->get()->result_array();
    }
    public function getDocAwaitBGD($idDepartment1=NULL,$User=NULL)
    {

        $this->db->where('Date(tbl_vanbanden.sNgayNhap) >="2018-01-01"');
        $this->db->where('tbl_vanbanden.iGiayMoi',2);
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi',2);
		$this->db->where('tbl_vanbanden.iTrangThai !=',1);
        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        $this->db->not_like('tbl_vanbanden.sSeenBGD',$User);
        if($User == 679)$this->db->where('tbl_vanbanden.sNgayNhap >=',date('2019-10-11 12:00:00'));
        
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sSeenBGD,tbl_chuyennhanvanban.sMoTa as mieuta,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,tbl_vanbanden.iSTCChuTri,tbl_vanbanden.iVanBanTBKL,tbl_vanbanden.iToCongTac,tbl_vanbanden.iTrangThai_ThemHan,tbl_vanbanden.sLyDoTuChoiKetQua,tbl_vanbanden.sLyDoTuChoiVanBanDen,tbl_vanbanden.iSTCPhoiHop');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
//        return $this->db->last_query($this->db->get());
        return $this->db->get()->result_array();
    }
    /** Danh sách văn bản đã xem ở phòng Ban giám đốc */
    public function getDocSeenBGD($idDepartment1=NULL,$User=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->where('tbl_vanbanden.iGiayMoi',2);
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi',2);
        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);
        $this->db->like('tbl_vanbanden.sSeenBGD',$User);
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sSeenBGD,tbl_chuyennhanvanban.sMoTa as mieuta,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,tbl_vanbanden.iSTCChuTri,tbl_vanbanden.iVanBanTBKL,tbl_vanbanden.iToCongTac,tbl_vanbanden.iTrangThai_ThemHan,tbl_vanbanden.sLyDoTuChoiKetQua,tbl_vanbanden.sLyDoTuChoiVanBanDen,tbl_vanbanden.iSTCPhoiHop');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
//        return $this->db->last_query($this->db->get());
        return $this->db->get()->result_array();
    }

public function getDataFomWhere($where,$table,$sapxep=NULL)
    {
        $this->db->where($where);
        $this->db->order_by($sapxep,"asc");
        return $this->db->get($table)->result_array();
    }

public function getDataFomWhere1($where,$table,$sapxep=NULL)
    {
        $this->db->where($where);
        $this->db->order_by($sapxep,"desc");
        return $this->db->get($table)->result_array();
    }    
public function vanbanphongchicuc($quyen,$tp_id){
        if($quyen==11) $this->db->where('tp_id',$tp_id);
        else $this->db->where('ccp_id',$tp_id);
        $this->db->select('doc_id');
        $this->db->from('tbl_chicuc');
        return $this->db->get()->result_array();
    }
	
public function layDSChiDao($idcb,$tencb,$limit,$offset){
	
		$query = $this->db->query('SELECT DISTINCT tbl_canbo.sHoTen, cd.*, vb.sMoTa, vb.iSoDen, vb.sHanThongKe, vb.iGiayMoi, cn.FK_iMaCB_Nhan, (select tbl_chidao_cb.iDaXem from tbl_chidao_cb where id_chidao = cd.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC limit 1) as iDaXem FROM tbl_chidao cd JOIN tbl_vanbanden vb ON cd.FK_iMaVBDen = vb.PK_iMaVBDen JOIN tbl_luuvet_chuyennhan cn ON cd.FK_iMaVBDen = cn.FK_iMaVBDen JOIN tbl_canbo ON cd.FK_iMaCB = tbl_canbo.PK_iMaCB WHERE vb.iGiayMoi != 1 && vb.iTrangThai != 1 && cd.FK_iMaCB != cn.FK_iMaCB_Nhan && (cn.FK_iMaCB_Nhan = '.$idcb.' || cd.sNoiDung LIKE "%@'.$tencb.'%") GROUP BY cd.sThoiGian ORDER BY cd.sThoiGian DESC;');
        
		 if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        return $query->result_array();
    }

public function demChiDaoDaXem($idcb,$limit,$offset){
	
		$query = $this->db->query('SELECT distinct tbl_chidao.sNoiDung, tbl_chidao_cb.id_chidao, tbl_chidao_cb.FK_iMaVBDen FROM tbl_chidao_cb join tbl_chidao on id_chidao = tbl_chidao.id join tbl_vanbanden on tbl_chidao.fk_imavbden = pk_imavbden where tbl_vanbanden.iGiayMoi != 1 && itrangthai != 1 && id_cb = '.$idcb);
		
        return $query->num_rows();
    }
	
public function demDSChiDao($idcb,$tencb,$limit,$offset){
	
		$query = $this->db->query('SELECT distinct tbl_canbo.sHoTen, cd.FK_iMaCB, cd.FK_iMaVBDen, cd.sNoiDung, cd.sThoiGian, vb.sMoTa, vb.iSoDen, vb.sHanThongKe, cn.FK_iMaCB_Nhan FROM tbl_chidao AS cd  JOIN tbl_vanbanden vb ON cd.FK_iMaVBDen = vb.PK_iMaVBDen  JOIN tbl_luuvet_chuyennhan AS cn ON cd.FK_iMaVBDen = cn.FK_iMaVBDen  JOIN tbl_canbo ON cd.FK_iMaCB = tbl_canbo.PK_iMaCB WHERE vb.iGiayMoi != 1 && vb.iTrangThai != 1 && tbl_canbo.PK_iMaCB!=cn.FK_iMaCB_Nhan  && (cn.FK_iMaCB_Nhan = '.$idcb.' || cd.sNoiDung LIKE "%@'.$tencb.'%") GROUP BY cd.sThoiGian order by cd.sThoiGian');
		
        return $query->num_rows();
    }
	
public function layDSYKien($idcb,$tencb,$limit,$offset){
	
		$query = $this->db->query('SELECT DISTINCT tbl_canbo.sHoTen, yk.*, vb.sMoTa, vb.iSoDen, vb.sHanThongKe, vb.iGiayMoi, cn.FK_iMaCB_Nhan, (select tbl_ykien_cb.iDaXem from tbl_ykien_cb where id_ykien = yk.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC limit 1) as iDaXem FROM tbl_ykien yk JOIN tbl_vanbanden vb ON yk.FK_iMaVBDen = vb.PK_iMaVBDen JOIN tbl_luuvet_chuyennhan cn ON yk.FK_iMaVBDen = cn.FK_iMaVBDen JOIN tbl_canbo ON yk.FK_iMaCB = tbl_canbo.PK_iMaCB WHERE vb.iGiayMoi != 1 && vb.iTrangThai != 1 && yk.FK_iMaCB != cn.FK_iMaCB_Nhan && (cn.FK_iMaCB_Nhan = '.$idcb.' || yk.sNoiDung LIKE "%@'.$tencb.'%") GROUP BY yk.sThoiGian ORDER BY yk.sThoiGian DESC;');
        
		 if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        return $query->result_array();
    }
	
public function demYKienDaXem($idcb,$limit,$offset){
	
		$query = $this->db->query('SELECT 1 FROM tbl_ykien_cb join tbl_ykien on id_ykien = tbl_ykien.id join tbl_vanbanden on tbl_ykien.fk_imavbden = pk_imavbden where tbl_vanbanden.iGiayMoi != 1 && itrangthai != 1 && id_cb = '.$idcb);
		
        return $query->num_rows();
    }

public function demDSYKien($idcb,$tencb,$limit,$offset){
	
		$query = $this->db->query('SELECT distinct tbl_canbo.sHoTen, yk.FK_iMaCB, yk.FK_iMaVBDen, yk.sNoiDung, yk.sThoiGian, vb.sMoTa, vb.iSoDen, vb.sHanThongKe, cn.FK_iMaCB_Nhan FROM tbl_ykien AS yk  JOIN tbl_vanbanden vb ON yk.FK_iMaVBDen = vb.PK_iMaVBDen  JOIN tbl_luuvet_chuyennhan AS cn ON yk.FK_iMaVBDen = cn.FK_iMaVBDen  JOIN tbl_canbo ON yk.FK_iMaCB = tbl_canbo.PK_iMaCB WHERE vb.iGiayMoi != 1 && vb.iTrangThai != 1 && yk.FK_iMaCB!= cn.FK_iMaCB_Nhan && (cn.FK_iMaCB_Nhan = '.$idcb.' || yk.sNoiDung LIKE "%@'.$tencb.'%") GROUP BY yk.sThoiGian order by yk.sThoiGian');
		
        return $query->num_rows();
    }
	
public function layThamMuuCVP($mavbden){
    $query = $this->db->query('SELECT distinct FK_iMaCB_Nhan, sNoiDung, sThoiGian, sHoTen FROM tbl_luuvet_chuyennhan join tbl_canbo on FK_iMaCB_Nhan = PK_iMaCB where FK_iMaVBDen= '.$mavbden.' and FK_iMaCB_Chuyen=696 and iQuyenHan_DHNB <7 order by sThoiGian asc;');
    return $query->result_array();
    }
public function layldduyet($phong){
		$query = $this->db->query('SELECT * FROM tbl_canbo where FK_iMaPhongHD= '.$phong.' and (iQuyenHan_DHNB = 6 || iQuyenHan_DHNB = 3) && itrangthai = 0 order by  iQuyenHan_DHNB asc, PK_iMaCB asc;');
		return $query->result_array();
    }	

}