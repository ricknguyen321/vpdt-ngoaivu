<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvanbanden extends CI_Model {
    /**
     * @author:	Duy
     * Time:
     * Description: Model văn bản đến
     * Process: quá trình
     */
    // lấy file cuối cùng
    public function layFileLast($ma)
    {
        $this->db->where('FK_iMaVBDen',$ma);
        $this->db->select('sDuongDan');
        $this->db->from('tbl_files_vbden');
        $this->db->order_by('PK_iMaFileDen','desc');
        $this->db->limit(1);
        return $this->db->get()->row_array();
    }
    public function getDL(){
        $query = $this->db->query("SELECT PK_iMaVBDen,PK_iMaCVPH,PK_iMaCVCT,sMoTa,input_per,PK_iMaPhongCT,sThoiGianHetHan,sThoiGian,PK_iMaCBChuyen,tbl_canbo.FK_iMaPhongHD FROM tbl_chuyennhanvanban LEFT JOIN tbl_canbo ON tbl_canbo.PK_iMaCB = tbl_chuyennhanvanban.`input_per` WHERE PK_iMaCVPH != 0 AND PK_iMaCVPH NOT IN(262,534,435,486)  ORDER BY tbl_chuyennhanvanban.PK_iMaVBDen asc LIMIT 500 OFFSET 1210");
        return $query->result_array();
    }
    public function getDocGo($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$limit=NULL,$offset=NULL,$quyenhan=NULL,$sPhongChuTri=NULL,$tonghop=NULL,$ideadline=NULL,$domat=NULL,$ihoanthanh=NULL)
    {
        //$this->db->where('Date(tbl_vanbanden.sNgayNhap) >="2018-01-01"');
		if(!empty($ideadline)){
			$this->db->like('tbl_vanbanden.iDeadLine',1);
        }
		if(!empty($domat)){
			$this->db->like('tbl_vanbanden.FK_iMaDM',$domat);
        }
		if($ihoanthanh == 1){
			$this->db->where('tbl_vanbanden.iTrangThai',1);
        }
		if($ihoanthanh == 2){
			$this->db->where('tbl_vanbanden.iTrangThai !=',1);
        }
		if(!empty($tonghop)){
            $this->db->like('tbl_vanbanden.sKyHieu',$tonghop);
			$this->db->or_like('tbl_vanbanden.iSoDen',$tonghop);
			$this->db->or_like('tbl_vanbanden.sMoTa',$tonghop);
			$this->db->or_like('tbl_vanbanden.sTenDV',$tonghop);
			$this->db->or_like('tbl_vanbanden.sTenNguoiKy',$tonghop);
			$this->db->or_like('tbl_vanbanden.sTenLVB',$tonghop);
			$this->db->or_like('tbl_vanbanden.sTenLV',$tonghop);
        }
        if(!empty($loaivanban)){
			if ($loaivanban == 'Giấy mời'){
				$this->db->where('tbl_vanbanden.iGiayMoi',1);
			} else {
				$this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
			}
        }
        if(!empty($sokyhieu)){
            //$this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
			$this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
//            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
            }
            elseif($cacloaivanban == '2'){
                $this->db->where('tbl_vanbanden.iTrangThai','1');
            }
            elseif($cacloaivanban == '3'){
                $this->db->where('tbl_vanbanden.iSTCChuTri','1');
            }
            elseif($cacloaivanban == '4'){
                $this->db->like('tbl_vanbanden.sTenDV','Thành ủy Hà Nội');
            }
            elseif($cacloaivanban == '5'){
                $this->db->like('tbl_vanbanden.sTenDV','UBND Thành phố Hà Nội');
            }
            elseif($cacloaivanban == '6'){
                $this->db->where('tbl_vanbanden.iVanBanTBKL','1');
            }
            elseif($cacloaivanban == '7'){
                $this->db->like('tbl_vanbanden.sTenDV','HĐND Thành Phố HN');
            }
            elseif($cacloaivanban == '8'){
                $this->db->like('tbl_vanbanden.iVanBanQPPL',1);
            }
			elseif($cacloaivanban == '9'){
                $this->db->where('tbl_vanbanden.iToCongTac',1);
            }
			elseif($cacloaivanban == '10'){
                $this->db->where('tbl_vanbanden.iSTCPhoiHop',1);
            }
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        $this->db->where('tbl_vanbanden.iGiayMoi !=',3);
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('iMail,sTenLVB,iSoTrang,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.*,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);
//            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }

        if(!empty($sPhongChuTri)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$sPhongChuTri);
        }

        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        //$this->db->order_by('tbl_vanbanden.PK_iMaVBDen','desc');
        return $this->db->get()->result_array();
    }



public function getDocGo1($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$limit=NULL,$offset=NULL,$quyenhan=NULL,$sPhongChuTri=NULL,$canbothuly=NULL)
    {
        //$this->db->where('Date(tbl_vanbanden.sNgayNhap) >="2018-01-01"');
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($canbothuly)){
            $this->db->where('tbl_vanbanden.PK_iMaCBHoanThanh',$canbothuly);
            $this->db->where('tbl_vanbanden.vbdaura_tp',1);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
//            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
            }
            elseif($cacloaivanban == '2'){
                $this->db->where('tbl_vanbanden.iTrangThai','1');
            }
            elseif($cacloaivanban == '3'){
                $this->db->where('tbl_vanbanden.iSTCChuTri','1');
            }
            elseif($cacloaivanban == '4'){
                $this->db->like('tbl_vanbanden.sTenDV','Thành ủy Hà Nội');
            }
            elseif($cacloaivanban == '5'){
                $this->db->like('tbl_vanbanden.sTenDV','UBND Thành phố Hà Nội');
            }
            elseif($cacloaivanban == '6'){
                $this->db->where('tbl_vanbanden.iVanBanTBKL','1');
            }
            elseif($cacloaivanban == '7'){
                $this->db->like('tbl_vanbanden.sTenDV','HĐND Thành Phố HN');
            }
            elseif($cacloaivanban == '8'){
                $this->db->like('tbl_vanbanden.sTenLVB','Khác');
            }
            elseif($cacloaivanban == '9'){
                $this->db->where('tbl_vanbanden.iToCongTac',1);
            }
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        if($quyenhan == 2){
            $this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));
        }
        $this->db->where('tbl_vanbanden.iGiayMoi !=',3);
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sNgayGiaiQuyet,tbl_vanbanden.sHanGiaiQuyet,iMail,sTenLVB,iSoTrang,tbl_vanbanden.sKyHieu,tbl_vanbanden.sNoiDung,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNgayNhap,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);
//            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }

        if(!empty($sPhongChuTri)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$sPhongChuTri);
        }

        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        //$this->db->order_by('tbl_vanbanden.iSoDen','desc');
        $this->db->order_by('tbl_vanbanden.PK_iMaVBDen','desc');
        return $this->db->get()->result_array();
    }



    /// danh sách vb đến bằng ajax
    public function getDocAjax($limit=NULL,$offset=NULL)
    {
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('tbl_vanbanden.*,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        return $this->db->get()->result_array();
    }
    // văn bản quan trong
    public function getVBQT($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$limit=NULL,$offset=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if($cacloaivanban == '2'){
            $this->db->where('tbl_vanbanden.iTrangThai',1);
        }else{
            $this->db->where('tbl_vanbanden.iTrangThai',0);
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('tbl_vanbanden.sMoTa,sTenLVB,iSoTrang,tbl_chuyennhanvanban.PK_iMaCN,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNgayNhap,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idUser);
            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        return $this->db->get()->result_array();
    }
    // dem vanbanden
    public function countDocGo($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$quyenhan=NULL,$sPhongChuTri=NULL,$tonghop=NULL,$ideadline=NULL,$domat=NULL,$ihoanthanh=NULL)
    {
        //$this->db->where('Date(tbl_vanbanden.sNgayNhap) >="2018-01-01"');
		if(!empty($ideadline)){
			$this->db->like('tbl_vanbanden.iDeadLine',1);
        }
		if(!empty($domat)){
			$this->db->like('tbl_vanbanden.FK_iMaDM',$domat);
        }
		if($ihoanthanh == 1){
			$this->db->where('tbl_vanbanden.iTrangThai',1);
        }
		if($ihoanthanh == 2){
			$this->db->where('tbl_vanbanden.iTrangThai !=',1);
        }
		if(!empty($tonghop)){
            $this->db->like('tbl_vanbanden.sKyHieu',$tonghop);
			$this->db->or_like('tbl_vanbanden.iSoDen',$tonghop);
			$this->db->or_like('tbl_vanbanden.sMoTa',$tonghop);
			$this->db->or_like('tbl_vanbanden.sTenDV',$tonghop);
			$this->db->or_like('tbl_vanbanden.sTenNguoiKy',$tonghop);
			$this->db->or_like('tbl_vanbanden.sTenLVB',$tonghop);
        }
         if(!empty($loaivanban)){
			if ($loaivanban == 'Giấy mời'){
				$this->db->where('tbl_vanbanden.iGiayMoi',1);
			} else {
				$this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
			}
        }
        if(!empty($sokyhieu)){
            //$this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
			$this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
            }
            elseif($cacloaivanban == '2'){
                $this->db->where('tbl_vanbanden.iTrangThai','1');
            }
            elseif($cacloaivanban == '3'){
                $this->db->where('tbl_vanbanden.iSTCChuTri','1');
            }
            elseif($cacloaivanban == '4'){
                $this->db->like('tbl_vanbanden.sTenDV','Thành ủy Hà Nội');
            }
            elseif($cacloaivanban == '5'){
                $this->db->like('tbl_vanbanden.sTenDV','UBND Thành phố Hà Nội');
            }
            elseif($cacloaivanban == '6'){
                $this->db->where('tbl_vanbanden.iVanBanTBKL','1');
            }
            elseif($cacloaivanban == '7'){
                $this->db->like('tbl_vanbanden.sTenDV','HĐND Thành Phố HN');
            }
            elseif($cacloaivanban == '8'){
                $this->db->like('tbl_vanbanden.iVanBanQPPL',1);
            }
			elseif($cacloaivanban == '9'){
                $this->db->where('tbl_vanbanden.iToCongTac',1);
            }
			elseif($cacloaivanban == '10'){
                $this->db->where('tbl_vanbanden.iSTCPhoiHop',1);
            }
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        
        $this->db->where('tbl_vanbanden.iGiayMoi !=',3);
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('tbl_vanbanden.PK_iMaVBDen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);
//            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }

        if(!empty($sPhongChuTri)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$sPhongChuTri);
        }

        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->num_rows();
    }



     // dem vanbanden
    public function countDocGo1($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$quyenhan=NULL,$sPhongChuTri=NULL,$canbothuly)
    {
        //$this->db->where('Date(tbl_vanbanden.sNgayNhap) >="2018-01-01"');
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($canbothuly)){
            $this->db->where('tbl_vanbanden.PK_iMaCBHoanThanh',$canbothuly);
            $this->db->where('tbl_vanbanden.vbdaura_tp',1);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
            }
            elseif($cacloaivanban == '2'){
                $this->db->where('tbl_vanbanden.iTrangThai','1');
            }
            elseif($cacloaivanban == '3'){
                $this->db->where('tbl_vanbanden.iSTCChuTri','1');
            }
            elseif($cacloaivanban == '4'){
                $this->db->like('tbl_vanbanden.sTenDV','Thành ủy Hà Nội');
            }
            elseif($cacloaivanban == '5'){
                $this->db->like('tbl_vanbanden.sTenDV','UBND Thành phố Hà Nội');
            }
            elseif($cacloaivanban == '6'){
                $this->db->where('tbl_vanbanden.iVanBanTBKL','1');
            }
            elseif($cacloaivanban == '7'){
                $this->db->like('tbl_vanbanden.sTenDV','HĐND Thành Phố HN');
            }
            elseif($cacloaivanban == '8'){
                $this->db->like('tbl_vanbanden.sTenLVB','Khác');
            }
            elseif($cacloaivanban == '9'){
                $this->db->where('tbl_vanbanden.iToCongTac',1);
            }
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        if($quyenhan == 2){
            $this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));
        }
        $this->db->where('tbl_vanbanden.iGiayMoi !=',3);
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('tbl_vanbanden.PK_iMaVBDen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);
//            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }

        if(!empty($sPhongChuTri)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$sPhongChuTri);
        }

        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');
        return $this->db->get()->num_rows();
    }



    // qua trinh truyen nhan van ban
    public function getDocProcess($idDoc = NULL,$idDepartment1=NULL,$dir=NULL,$lever=NULL,$order=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        }
        if(!empty($idDepartment1)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        }
        if(!empty($dir)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$dir);
        }
        if(!empty($lever)){
            $this->db->where('tbl_chuyennhanvanban.CapGiaiQuyet',$lever);
        }
        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
        $this->db->select('PK_iMaCN,PK_iMaVBDen,PK_iMaCBChuyen,PK_iMaCVPH,PK_iMaCBNhan,sMoTa,sThoiGianHetHan,PK_iMaPhongPH,PK_iMaCVCT,PK_iMaPhongCT,sVBQT');
        $this->db->from('tbl_chuyennhanvanban');
//        $this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');
        if(!empty($order)){
            $this->db->order_by('tbl_chuyennhanvanban.PK_iMaCN','desc');
            $this->db->limit(1);
        }
        return $this->db->get()->result_array();
    }
    /**
     * lấy danh sách hoặc 1 dự liệu
     */
    public function layDuLieu($primary_key=NULL,$id=NULL)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        $this->db->select('tbl_vanbanden.*,tbl_canbo.sHoTen, tbl_files_vbden.*, tbl_dokhan.sTenDoKhan, tbl_domat.sTenDoMat');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_dokhan','FK_iMaDK = PK_iMaDK');
		$this->db->join('tbl_domat','FK_iMaDM = PK_iMaDM');
		$this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
		$this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        return $this->db->get()->result_array();
    }
    public function layDuLieuEnd($primary_key=NULL,$id=NULL,$year=NULL)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap >=',$year.'-01-01');
        $this->db->select('tbl_vanbanden.iSoDen');
        $this->db->from('tbl_vanbanden');
        $this->db->order_by('iSoDen','desc');
        return $this->db->get()->row_array();
    }
    public function getSoDen($primary_key=NULL,$id=NULL,$year=NULL)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        $this->db->where('tbl_vanbanden.sNgayNhap >=',$year.'-01-01');
        $this->db->select('tbl_vanbanden.iSoDen,sMoTa');
        $this->db->from('tbl_vanbanden');
        $this->db->order_by('iSoDen','desc');
        return $this->db->get()->result_array();
    }
    /** Danh sách giấy mới */
    public function getAppointment($idUser=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$limit=NULL,$offset=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
            }
            else{
                $this->db->where('tbl_vanbanden.iTrangThai','1');
            }
        }
        $this->db->where('tbl_vanbanden.iGiayMoi','1');
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('tbl_vanbanden.sMoTa,sTenLVB,iSoTrang,tbl_vanbanden.sNoiDung,iMail,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNgayNhap,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idUser);
            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('tbl_vanbanden.PK_iMaVBDen','desc');
        return $this->db->get()->result_array();
    }
    public function getDocConcac($idDoc = NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chicuc.doc_id',$idDoc);
        }
        $this->db->where('tbl_chicuc.doc_giaymoi','2');
        $this->db->select('tbl_chicuc.*');
        $this->db->from('tbl_chicuc');
        return $this->db->get()->result_array();
    }
    // giấy mời chi cục
    public function getDocConcac1($idDoc = NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chicuc.doc_id',$idDoc);
        }
        $this->db->where('tbl_chicuc.doc_giaymoi','1');
        $this->db->select('tbl_chicuc.*');
        $this->db->from('tbl_chicuc');
        return $this->db->get()->result_array();
    }
    public function getStatusDir($idDoc = NULL){
        if(!empty($idDoc)){
            $this->db->where('tbl_vanbanden.PK_iMaVBDen',$idDoc);
        }
        $this->db->select('tbl_vanbanden.iTrangThai_GD');
        $this->db->from('tbl_vanbanden');
        return $this->db->get()->row_array();
    }
    public function updateDuLieu($idDoc,$idCB,$data=array()){
        $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idCB);
        $this->db->update('tbl_chuyennhanvanban',$data);
    }
    public function updateDuLieuPH($idDoc,$idCB,$data=array()){
        $this->db->where('tbl_phongphoihop.PK_iMaVBDen',$idDoc);
        $this->db->where('tbl_phongphoihop.PK_iMaCB',$idCB);
        $this->db->update('tbl_phongphoihop',$data);
    }
    // danh sách cán bộ ở phần thống kê
    public function getReportMembers($truongphong=NULL,$idUser=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$sangtao=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$Deparment=NULL,$session=NULL,$idLanhDao=NULL,$limit=NULL,$offset=NULL)
    {
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        if(isset($dagiaiquyet)){
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            elseif($dagiaiquyet == 0){
                $this->db->where('tbl_vanbanden.sHanThongKe >','2017-04-01');
                $this->db->where('tbl_vanbanden.sHanThongKe < tbl_vanbanden.sNgayGiaiQuyet',NULL);
            }
			$this->db->where('tbl_vanbanden.iTrangThai',1);
        }
        if(isset($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            elseif($chuagiaquyet == 0){
                $this->db->where('tbl_vanbanden.sHanThongKe <',date('Y-m-d'));
				$this->db->where('tbl_vanbanden.sHanThongKe >','2017-04-01');
            }
			if ($idUser != $truongphong) {
				$this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);		
				$this->db->where('tbl_vanbanden.PK_iMaCBDuyet !=',$idUser);
			} else {
				$this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',0);	
				$this->db->where('lvcn.FK_iMaCB_Nhan',$idUser);
				$this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',5);	
				
			}
			$this->db->where_in('tbl_vanbanden.iTrangThai !=',1);
        }

        if($sangtao==1)// 1 có sáng tạo
        {
            $this->db->where('iCoSangTao',1);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
            }
            elseif($cacloaivanban == '2'){
                $this->db->where('tbl_vanbanden.iTrangThai','1');
            }
            elseif($cacloaivanban == '3'){
                $this->db->where('tbl_vanbanden.iSTCChuTri','1');
            }
            elseif($cacloaivanban == '4'){
                $this->db->like('tbl_vanbanden.sTenDV','Thành ủy Hà Nội');
            }
            elseif($cacloaivanban == '5'){
                $this->db->like('tbl_vanbanden.sTenDV','UBND Thành phố Hà Nội');
            }
            elseif($cacloaivanban == '6'){
                $this->db->where('tbl_vanbanden.iVanBanTBKL','1');
            }
            elseif($cacloaivanban == '7'){
                $this->db->like('tbl_vanbanden.sTenDV','HĐND Thành Phố HN');
            }
            elseif($cacloaivanban == '8'){
                $this->db->like('tbl_vanbanden.sTenLVB','Khác');
            }
        }
        if(!empty($idLanhDao)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idLanhDao);
        }
        //$this->db->where('tbl_vanbanden.iTrangThai !=','3');
		//$this->db->where('tbl_vanbanden.sLuuVaoKho !=','1');
        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
        $this->db->select('tbl_vanbanden.*,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
//            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
//            $this->db->group_by('tbl_chuyennhanvanban.capgiaiquyet');
//            $this->db->having('capgiaiquyet','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);
            $this->db->order_by('tbl_chuyennhanvanban.PK_iMaCVCT','desc');
            //$this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');
        }
		if (!empty($Deparment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
			if(!empty($session) && $session == 12) {
                $this->db->join('tbl_chicuc', 'tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen', 'left');
                $this->db->where('tbl_chicuc.tcdn_active >=','2');
            }
            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
//            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
//            $this->db->group_by('tbl_chuyennhanvanban.capgiaiquyet');
//            $this->db->having('capgiaiquyet','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$Deparment);
            //$this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
//        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        return $this->db->get()->result_array();
    }
	
	
	public function getReportV2($maCB,$trangthai,$sangtao=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$param1=NULL,$param2=NULL,$idLanhDao=NULL,$truongphong=NULL,$ngaynhap=NULL,$ngayden=NULL,$quyen=NULL,$limit=NULL,$offset=NULL)// trạng thái 0: chưa giải quyết 1,2 đã giải quyết
    {		
	
		$this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        $this->db->where('cn.iGiayMoi',2);
        if(isset($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
            }
            $this->db->where('vb.iTrangThai !=',1);
			if ($maCB != $truongphong) {
				$this->db->where('cn.PK_iMaCVCT',$maCB);		
				$this->db->where('vb.PK_iMaCBDuyet !=',$maCB);
			} else {
				$this->db->where('cn.PK_iMaCVCT',0);	
				$this->db->where('lvcn.FK_iMaCB_Nhan',$maCB);
				$this->db->where('vb.iTrangThai_TruyenNhan',5);	
				
			}
			
			if(!empty($ngaynhap)){
				$this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
			}
			if(!empty($ngayden)){
				$this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
			}
			if(!empty($idLanhDao)){
				$this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
			}
			if(!empty($quyen)){
				$this->db->where('vb.iTrangThai_TruyenNhan',$quyen - 1);
			}
			if($sangtao==1)// 1 có sáng tạo
			{
				$this->db->where('iCoSangTao',1);
				$this->db->where('vb.iTrangThai',$trangthai);
			}		
			$this->db->select('vb.*');
			$this->db->from('tbl_vanbanden as vb');
			$this->db->join('tbl_luuvet_chuyennhan as lvcn','lvcn.FK_iMaVBDen=vb.PK_iMaVBDen');
			$this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
			if(!empty($limit))
			{
				$this->db->limit($limit,$offset);
			}
			$this->db->order_by('lvcn.sThoiGian','desc');
			$this->db->group_by('vb.PK_iMaVBDen');
        } else {
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
//                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
            }
			$this->db->where('vb.PK_iMaCBHoanThanh',$maCB);		
            $this->db->where('vb.iTrangThai',$trangthai);
			
			if(!empty($ngaynhap)){
				$this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
			}
			if(!empty($ngayden)){
				$this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
			}
			if(!empty($idLanhDao)){
				$this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
			}
			if(!empty($quyen)){
				$this->db->where('vb.iTrangThai_TruyenNhan',$quyen - 1);
			}
			if($sangtao==1)// 1 có sáng tạo
			{
				$this->db->where('iCoSangTao',1);
				$this->db->where('vb.iTrangThai',$trangthai);
			}	
			$this->db->select('vb.*');
			$this->db->from('tbl_vanbanden as vb');
			$this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
			if(!empty($limit))
			{
				$this->db->limit($limit,$offset);
			}
			$this->db->group_by('vb.PK_iMaVBDen');
			$this->db->order_by('vb.iSoDen','desc');

        }
        
        return $this->db->get()->result_array();

    }
	
	public function countReportV2($maCB,$trangthai,$sangtao=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$param1=NULL,$param2=NULL,$idLanhDao=NULL,$truongphong=NULL,$ngaynhap=NULL,$ngayden=NULL,$quyen=NULL,$limit=NULL,$offset=NULL)// trạng thái 0: chưa giải quyết 1,2 đã giải quyết
    {		
	
		$this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        $this->db->where('cn.iGiayMoi',2);
        if(isset($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
            }
            $this->db->where('vb.iTrangThai !=',1);
			if ($maCB != $truongphong) {
				$this->db->where('cn.PK_iMaCVCT',$maCB);		
				$this->db->where('vb.PK_iMaCBDuyet !=',$maCB);
			} else {
				$this->db->where('cn.PK_iMaCVCT',0);	
				$this->db->where('lvcn.FK_iMaCB_Nhan',$maCB);
				$this->db->where('vb.iTrangThai_TruyenNhan',5);	
				
			}
			if(!empty($ngaynhap)){
				$this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
			}
			if(!empty($ngayden)){
				$this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
			}
			if(!empty($idLanhDao)){
				$this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
			}
			if(!empty($quyen)){
				$this->db->where('vb.iTrangThai_TruyenNhan',$quyen - 1);
			}
			if($sangtao==1)// 1 có sáng tạo
			{
				$this->db->where('iCoSangTao',1);
				$this->db->where('vb.iTrangThai',$trangthai);
			}	
			$this->db->select('vb.*');
			$this->db->from('tbl_vanbanden as vb');
			$this->db->join('tbl_luuvet_chuyennhan as lvcn','lvcn.FK_iMaVBDen=vb.PK_iMaVBDen');
			$this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
			$this->db->order_by('lvcn.sThoiGian','desc');
			$this->db->group_by('vb.PK_iMaVBDen');
        } else {
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
//                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
            }
			$this->db->where('vb.PK_iMaCBHoanThanh',$maCB);		
            $this->db->where('vb.iTrangThai',$trangthai);
			if(!empty($ngaynhap)){
				$this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
			}
			if(!empty($ngayden)){
				$this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
			}
			if(!empty($idLanhDao)){
				$this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
			}
			if(!empty($quyen)){
				$this->db->where('vb.iTrangThai_TruyenNhan',$quyen - 1);
			}
			if($sangtao==1)// 1 có sáng tạo
			{
				$this->db->where('iCoSangTao',1);
				$this->db->where('vb.iTrangThai',$trangthai);
			}		
			$this->db->select('vb.*');
			$this->db->from('tbl_vanbanden as vb');
			$this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
			$this->db->group_by('vb.PK_iMaVBDen');

        }
        
        return $this->db->get()->num_rows();

    }
	
	public function getReportPhong($pb,$trangthai,$sangtao=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$param1=NULL,$param2=NULL,$idLanhDao=NULL,$truongphong=NULL,$ngaynhap=NULL,$ngayden=NULL,$quyen=NULL,$limit=NULL,$offset=NULL)// trạng thái 0: chưa giải quyết 1,2 đã giải quyết
    {		
		$this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        if(isset($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
            }
            $this->db->where('vb.iTrangThai !=',1);

        }
        if(isset($dagiaiquyet)){
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
//                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
            }	
            $this->db->where('vb.iTrangThai',1);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
        }
        if($sangtao==1)// 1 có sáng tạo
        {
            $this->db->where('iCoSangTao',1);
        }
		$this->db->where('cn.PK_iMaPhongCT',$pb);
		$this->db->where('vb.iTrangThai_TruyenNhan > ',4);	
        $this->db->select('vb.*, (select sHoTen from tbl_canbo where PK_iMaCB = (select PK_iMaCVCT from tbl_chuyennhanvanban where tbl_chuyennhanvanban.PK_iMavbden = vb.PK_iMavbden order by sThoigian desc limit 1)) as sHoTen');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
		if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
		
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();

    }
	
	public function countReportPhong($pb,$trangthai,$sangtao=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$param1=NULL,$param2=NULL,$idLanhDao=NULL,$truongphong=NULL,$ngaynhap=NULL,$ngayden=NULL,$quyen=NULL,$limit=NULL,$offset=NULL)// trạng thái 0: chưa giải quyết 1,2 đã giải quyết
    {		
	
		$this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
		$this->db->where('cn.iGiayMoi',2);
        if(isset($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
            }
            $this->db->where('vb.iTrangThai !=',1);

        }
        if(isset($dagiaiquyet)){
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
//                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
            }	
            $this->db->where('vb.iTrangThai',1);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
        }
        if($sangtao==1)// 1 có sáng tạo
        {
            $this->db->where('iCoSangTao',1);
        }
		
		$this->db->where('cn.PK_iMaPhongCT',$pb);
		$this->db->where('vb.iTrangThai_TruyenNhan > ',4);
        $this->db->select('vb.*');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        $this->db->group_by('vb.PK_iMaVBDen');
         return $this->db->get()->num_rows();
    }
	
    public function getDocCTDang($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$limit=NULL,$offset=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
//            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');
            }
            elseif($cacloaivanban == '2'){
                $this->db->where('tbl_vanbanden.iTrangThai','1');
            }
            elseif($cacloaivanban == '3'){
                $this->db->where('tbl_vanbanden.iSTCChuTri','1');
            }
            elseif($cacloaivanban == '4'){
                $this->db->like('tbl_vanbanden.sTenDV','Thành ủy Hà Nội');
            }
            elseif($cacloaivanban == '5'){
                $this->db->like('tbl_vanbanden.sTenDV','UBND Thành phố Hà Nội');
            }
            elseif($cacloaivanban == '6'){
                $this->db->where('tbl_vanbanden.iVanBanTBKL','1');
            }
            elseif($cacloaivanban == '7'){
                $this->db->like('tbl_vanbanden.sTenDV','HĐND Thành Phố HN');
            }
            elseif($cacloaivanban == '8'){
                $this->db->like('tbl_vanbanden.sTenLVB','Khác');
            }
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        $this->db->where('tbl_vanbanden.iSTCPhoiHop',1);
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->select('tbl_vanbanden.sMoTa,sTenLVB,iSoTrang,tbl_vanbanden.sKyHieu,tbl_vanbanden.sNoiDung,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNgayNhap,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//            $this->db->where('tbl_chuyennhanvanban.sVBQT','1');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);
//            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        return $this->db->get()->result_array();
    }
    // văn bản quan trong
    public function getToCongTac($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$limit=NULL,$offset=NULL,$chuyenvien=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if($cacloaivanban == '2'){
            $this->db->where('tbl_vanbanden.iTrangThai',1);
        }else{
            $this->db->where('tbl_vanbanden.iTrangThai',0);
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        $this->db->where('tbl_vanbanden.iTrangThai !=','3');
        $this->db->where('tbl_vanbanden.iToCongTac',1);
        $this->db->select('tbl_vanbanden.sHanThongKe,tbl_vanbanden.sMoTa,sTenLVB,iSoTrang,tbl_chuyennhanvanban.PK_iMaCN,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNgayNhap,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idUser);
            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }
		if(!empty($chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$chuyenvien);
            $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
        return $this->db->get()->result_array();
    }
	public function layDuLieuCN($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        $this->db->order_by('PK_iMaChuyenLai','desc');
        return $this->db->get($table)->row_array();
    }
	// lấy cán bộ phòng ban
    public function layCBPhong($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        return $this->db->get($table)->row_array();
    }
	public function dsGiaHan($idUser=NULL,$status=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$ngaymoi=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$cacloaivanban=NULL,$limit=NULL,$offset=NULL,$chuyenvien=NULL)
    {
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->where('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->where('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($ngaymoi)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) >=',$ngaymoi);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->where('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->where('Date(tbl_vanbanden.sGiayMoiNgay) <=',$denngay);
            $this->db->where('tbl_vanbanden.iGiayMoi',1);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($status)){
            $this->db->where('tbl_vanbanden.iGiayMoi',$status);
        }
        $this->db->where('tbl_vanbanden.iTrangThai !=',1);
        $this->db->where('tbl_vanbanden.iTrangThai_ThemHan',1);
        $this->db->select('tbl_vanbanden.sHanThongKe,tbl_vanbanden.sMoTa,sTenLVB,iSoTrang,tbl_vanbanden.sKyHieu,tbl_vanbanden.sHanChoDuyet,tbl_vanbanden.sNoiDungGiaHan,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sNgayNhap,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        if ($idUser){
			if ($idUser == 735){
				$this->db->where_in('tbl_vanbanden.iLanhDao',array(735,694,695));
			}
			else {
				$this->db->where('tbl_vanbanden.iLanhDao',$idUser);
			}
        }
        if(!empty($chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$chuyenvien);
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
		$this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }

    public function return_vb($sql)
        {
            $query = $this->db->query($sql);
            return 1;
        }
    public function layGiaHanGiamDoc($mavb){
        $query = $this->db->query("SELECT PK_iMaVBDen FROM tbl_chuyennhanvanban WHERE PK_iMaVBDen= ".$mavb." AND PK_iMaCBChuyen = 673 ");
        return $query->result_array();
    }

    public function laySoDenLonNhat($year)
    {
        $this->db->select('MAX(tbl_vanbanden.iSoDen) AS soden');
        $this->db->where('tbl_vanbanden.sNgayNhap >=',$year.'-01-01');
        $this->db->from('tbl_vanbanden');
        return $this->db->get()->row_array();
    }
	
	public function layLichHopPhongCT($phongban,$quyen)
	{
		 $query = $this->db->query('SELECT * FROM tbl_lichhopso WHERE sNgayMoi >= CURDATE() && (sPhongCT = "'.$phongban.'" || sPhongPH LIKE "%'.$phongban.'%") && sMoTa != "Chào cờ đầu tuần" ORDER BY sNgayMoi, sGioMoi ASC');
        return $query->result_array();
	}

}
