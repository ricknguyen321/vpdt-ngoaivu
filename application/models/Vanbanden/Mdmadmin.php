<?php

/**

 * Created by PhpStorm.

 * User: Minh Duy

 * Date: 8/3/2017

 * Time: 9:37 AM

 */

class Mdmadmin extends CI_Model {

    public function getDocAwait($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$GiayMoi = NULL,$PGDThamMuu=NULL,$PPThamMuu=NULL,$tocongtac=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        if($PGDThamMuu){

            $this->db->where('tbl_vanbanden.iPgdThamMuu',$PGDThamMuu);

        }

        if($PPThamMuu){

            $this->db->where('tbl_vanbanden.iPpThamMuu',$PPThamMuu);

        }

        if(!empty($tocongtac)){

            $this->db->where('tbl_vanbanden.iToCongTac !=',$tocongtac);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

//        $this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d',time()));

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        if(!empty($idDepartment)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        if(!empty($idDepartment1)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

//        if(!empty($idDepartmentCT)){

//            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

//            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);

//            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

//        }

        if(!empty($Chuyenvien)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

			if(!empty($GiayMoi) && $GiayMoi !=1){

                $this->db->where('tbl_vanbanden.iToCongTac !=','1');

            }

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }

    public function getDocAwait1($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$GiayMoi = NULL,$PGDThamMuu=NULL,$PPThamMuu=NULL,$tocongtac=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan <=',$trangthai_truyennhan);

        }

        if($PGDThamMuu){

            $this->db->where('tbl_vanbanden.iPgdThamMuu',$PGDThamMuu);

        }

        if($PPThamMuu){

            $this->db->where('tbl_vanbanden.iPpThamMuu',$PPThamMuu);

        }

        if(!empty($tocongtac)){

            $this->db->where('tbl_vanbanden.iToCongTac !=',$tocongtac);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

//        $this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d',time()));

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        if(!empty($idDepartment)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        if(!empty($idDepartment1)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

//        if(!empty($idDepartmentCT)){

//            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

//            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);

//            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

//        }

        if(!empty($Chuyenvien)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

            if(!empty($GiayMoi) && $GiayMoi !=1){

                $this->db->where('tbl_vanbanden.iToCongTac !=','1');

            }

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }



    // danh sách văn bản phân loại nhưng lãnh đạo chưa xử lý

    public function getDocDirAwait($GiayMoi = NULL){

        if($GiayMoi == 1){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >',2);

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

            $this->db->where('tbl_vanbanden.iTrangThai','0');

        }

        if($GiayMoi == 2){

            $this->db->where_in('tbl_vanbanden.iTrangThai_TruyenNhan',array('3','4'));

            $this->db->where('tbl_vanbanden.iGiayMoi',2);

            $this->db->where('tbl_vanbanden.iTrangThai','0');

        }



        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    // văn bản bị từ chối

    public function getDocReject(){

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->where('tbl_vanban_chuyenlai.iTrangThai','1');

        $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','2');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    // danh sách chỉ đạo nhưng chưa xử lý ( các phòng )

    public function getDocTPCDAwait($idDepartment1=NULL,$idDepartment=NULL,$GiayMoi = NULL,$stcphoihop=NULL){



        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        if(!empty($stcphoihop)){

            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);

        }else{

            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);

        }

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);

        $this->db->where_in('tbl_chuyennhanvanban.CapGiaiQuyet',array('7','8'));

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');



        return $this->db->get()->num_rows();

    }

    // văn bản quan trong

    public function getVBQT($idUser=NULL,$status=NULL)

    {

        $this->db->where('tbl_chuyennhanvanban.sVBQT','1');

        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idUser);

		$this->db->where('tbl_vanbanden.iTrangThai',0);

        $this->db->select('tbl_chuyennhanvanban.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        $this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	public function getVBQTGQ($idUser=NULL,$status=NULL)

    {

        $this->db->where('tbl_chuyennhanvanban.sVBQT','1');

        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idUser);

		$this->db->where('tbl_vanbanden.iTrangThai',1);

        $this->db->select('tbl_chuyennhanvanban.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        $this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    // giám đốc đã xử lý

    public function getDocGDAwait($idDepartment1=NULL){

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        $this->db->where('tbl_vanbanden.iGiayMoi','1');

        if($idDepartment1){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >','3');

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    // văn bản chờ phê duyệt

    public function getDocPheDuyet($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$pheduyet=NULL,$pheduyetCV=NULL,$Truongphong=NULL,$giaymoi=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        if($pheduyet){

            $this->db->where('tbl_vanbanden.iTrangThai',$pheduyet);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        if($giaymoi == 1){

            $this->db->where('tbl_vanbanden.iGiayMoi',1);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi',2);

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chuyennhanvanban','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        if($pheduyetCV){

            $this->db->where_in('tbl_vanbanden.iTrangThai',array('1','2'));

        }

        if(!empty($idDepartment)){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

        }

        if(!empty($idDepartment1)){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

        }

        if(!empty($Chuyenvien)){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

        }

        if(!empty($Truongphong)){

            $this->db->where('tbl_vanbanden.PK_iMaCBDuyet',$Truongphong);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');



        return $this->db->get()->num_rows();

    }

    // danh sách chỉ đạo nhưng chưa xử lý ( các phòng )

    public function getConCac($idDepartment1=NULL,$GiayMoi = NULL,$stcphoihop=NULL)

    {

        if(!empty($idDepartment1)){

            $this->db->where('tbl_chicuc.department_id',$idDepartment1);

        }

        if(isset($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        if(!empty($stcphoihop)){

            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);

        }else{

            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);

        }

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where('tbl_chicuc.tcdn_active >=',2);

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    public function getDocAwaitCVPPH($trangthai_truyennhan = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$iTrangThaiPH=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        if($idUser){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

        }

        if($iTrangThaiPH){

            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);

        }

        if($Status){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$Status);

        }

        if($giaymoi == 1){

            $this->db->where('tbl_vanbanden.iGiayMoi',$giaymoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi',$giaymoi);

        }

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
		$this->db->where('tbl_vanbanden.iTrangThai != ',1);

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');



        if(!empty($idDepartment1)){

            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        }

        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        if(!empty($dem)){

            if($dem == 0){

                $this->db->having('dem = '.$dem, NULL, FALSE);

            }elseif($dem == 1){

                $this->db->having('dem > 0', NULL, FALSE);

            }

        }

        return $this->db->get()->num_rows();

    }



   // danh sách van ban phòng phối hợp

    public function getDocAwaitPPH_welcome($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$iTrangThaiPH=NULL)

    {



        if($Quyen == 6 || $Quyen == 3){        

            $this->db->where(' NOT EXISTS ( SELECT * FROM tbl_phongphoihop WHERE PK_iMaPhong = '.$idDepartment1.' AND tbl_chuyennhanvanban.PK_iMaVBDen = tbl_phongphoihop.PK_iMaVBDen)');

			//ricknguyen321 Truong phong co the xu ly van ban phoi hop
			$this->db->where(' NOT EXISTS ( SELECT * FROM tbl_file_ketqua WHERE PK_iMaPB = '.$idDepartment1.' AND tbl_chuyennhanvanban.PK_iMaVBDen = tbl_file_ketqua.PK_iMaVBDen)');

            //$this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');
        }

        if($Quyen == 7){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

            $this->db->where('tbl_phongphoihop.iTrangThai',2);

            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);

        }

        if($Quyen == 11){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

            $this->db->where('tbl_phongphoihop.iTrangThai',11);

            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);

            // $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');

        }

        if($giaymoi == 1){

            $this->db->where('tbl_vanbanden.iGiayMoi',1);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi',2);

        }

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
		$this->db->where('tbl_vanbanden.iTrangThai !=',1);

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();



    }



   // danh sách van ban phòng phối hợp

    public function getDocAwaitPPH($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$iTrangThaiPH=NULL)

    {



        if($Quyen == 6 || $Quyen == 3){

            $this->db->where(' NOT EXISTS

( SELECT * FROM tbl_phongphoihop WHERE PK_iMaPhong = '.$idDepartment1.' AND tbl_chuyennhanvanban.PK_iMaVBDen = tbl_phongphoihop.PK_iMaVBDen)');

            //$this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');

            // $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

            // $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',5);

        }

        if($Quyen == 7){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

            $this->db->where('tbl_phongphoihop.iTrangThai',2);

			$this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);

        }

        if($Quyen == 11){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

            $this->db->where('tbl_phongphoihop.iTrangThai',11);

            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);

            // $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');

        }

        if($giaymoi == 1){

            $this->db->where('tbl_vanbanden.iGiayMoi',1);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi',2);

        }

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');

        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sTenLVB,tbl_vanbanden.iSoTrang,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sNgayKy,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan,tbl_phongphoihop.PK_iMaCB');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();



    }



public function getDocAwaitPPHDXL_welcome($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$iTrangThaiPH=NULL)

    {

        $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->where('tbl_vanbanden.iGiayMoi',1);

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');

        $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }



    public function getDocAwaitPPHDXL($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL,$Status=NULL,$dem=NULL,$iTrangThaiPH=NULL)

    {

        $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->where('tbl_vanbanden.iGiayMoi',1);

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');

        $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sTenLVB,tbl_vanbanden.iSoTrang,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sNgayKy,COUNT(tbl_files_vbden.FK_iMaVBDen) as demtin,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan,tbl_phongphoihop.PK_iMaCB');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    // danh sách văn bản hoàn thành

    public function getDocComplete($idDepartment=NULL,$Chuyenvien=NULL)

    {

        if(!empty($idDepartment)){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment);

        }

        if(!empty($Chuyenvien)){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

        }

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi','2');

        $this->db->where('tbl_vanbanden.iGiayMoi','1');

        $this->db->where('tbl_vanbanden.iTrangThai','1');

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

//        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');

        return $this->db->get()->num_rows();

    }

    // danh sách chờ của chi cục

    public function getDocConcacAwait($trangthai_truyennhan = NULL, $tcdn_active = NULL,$department_id=NULL,$ccp_id=NULL,$Chuyenvien=NULL,$GiayMoi=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($tcdn_active)){

            $this->db->where('tbl_chicuc.tcdn_active',$tcdn_active);

        }

        if(!empty($department_id)){

            $this->db->where('tbl_chicuc.department_id',$department_id);

        }

        if(!empty($ccp_id)){

            $this->db->where('tbl_chicuc.tp_id',$ccp_id);

        }

        if(!empty($Chuyenvien)){

            $this->db->where('tbl_chicuc.ccp_id',$Chuyenvien);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }

    // van ban cho xu ly pho chu chi

    public function getPConCac($idDepartment1=NULL,$GiayMoi=NULL,$stcphoihop=NULL){



        if(!empty($idDepartment1)){

            $this->db->where('tbl_chicuc.ccp_id',$idDepartment1);

        }

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        if(!empty($stcphoihop)){

            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);

        }else{

            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where_in('tbl_chicuc.tcdn_active ',array(3,4));

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSODen','desc');

        return $this->db->get()->num_rows();

    }

    /** giấy mới kết luận cuộc họp mới */

    public function getMeeting($idUser=NULL,$iTrangThaiXem=NULL){



        if(!empty($idUser)){

            $this->db->where('tbl_soanbaocao.PK_iMaCB',$idUser);

        }

        if(isset($iTrangThaiXem)){

            $this->db->where('tbl_soanbaocao.iTrangThaiXem',$iTrangThaiXem);

        }

        $this->db->where('tbl_soanbaocao.iTrangThai','0');

        $this->db->select('tbl_soanbaocao.PK_iMaSoanBaoCao');

        $this->db->from('tbl_soanbaocao');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_soanbaocao.PK_iMaVBDen','left');

        return $this->db->get()->num_rows();

    }

    public function getSoanThaoDaGui($idUser=NULL,$iTrangThai=NULL){

        if(!empty($idUser)){

            $this->db->where('tbl_soanbaocao.PK_iMaCBNhap',$idUser);

        }

        if(!empty($iTrangThai)){

            $this->db->where('tbl_soanbaocao.iTrangThai',$iTrangThai);

        }

        // $this->db->where('tbl_soanbaocao.iTrangThai','0');

        $this->db->select('tbl_soanbaocao.PK_iMaNoiDung');

        $this->db->from('tbl_soanbaocao');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_soanbaocao.PK_iMaVBDen','left');

        $this->db->join('tbl_noidungbaocao','tbl_noidungbaocao.PK_iMaNoiDung = tbl_soanbaocao.PK_iMaNoiDung','left');

        $this->db->group_by('tbl_noidungbaocao.PK_iMaNoiDung');

        return $this->db->get()->num_rows();

    }

	// danh sach vb cong tac dang

    public function getDocAwaitCTD($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$GiayMoi = NULL,$PGDThamMuu=NULL,$PPThamMuu=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        if($PGDThamMuu){

            $this->db->where('tbl_vanbanden.iPgdThamMuu',$PGDThamMuu);

        }

        if($PPThamMuu){

            $this->db->where('tbl_vanbanden.iPpThamMuu',$PPThamMuu);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

//        $this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d',time()));

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','3');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        if(!empty($idDepartment)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        if(!empty($idDepartment1)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

//        if(!empty($idDepartmentCT)){

//            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

//            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);

//            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

//        }

        if(!empty($Chuyenvien)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }

	public function getDocAwaitPPPH($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$giaymoi=NULL)

    {

        if($Quyen == 7){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

            $this->db->where('tbl_phongphoihop.iTrangThai',2);

			$this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);

        }

        if($giaymoi == 1){

            $this->db->where('tbl_vanbanden.iGiayMoi',1);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi',2);

        }

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');

        $this->db->where('tbl_phongphoihop.sThoiGian >=','2017-10-11');

        $this->db->select('tbl_phongphoihop.PK_iMaPhongPH');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

        return $this->db->get()->num_rows();

    }

    public function getDocAwaitTCT($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$GiayMoi = NULL,$PGDThamMuu=NULL,$PPThamMuu=NULL,$tocongtac=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        if($PGDThamMuu){

            $this->db->where('tbl_vanbanden.iPgdThamMuu',$PGDThamMuu);

        }

        if($PPThamMuu){

            $this->db->where('tbl_vanbanden.iPpThamMuu',$PPThamMuu);

        }

        if(!empty($tocongtac)){

            $this->db->where('tbl_vanbanden.iToCongTac',$tocongtac);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

//        $this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d',time()));

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        if(!empty($idDepartment)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        if(!empty($idDepartment1)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

//        if(!empty($idDepartmentCT)){

//            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

//            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);

//            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

//        }

        if(!empty($Chuyenvien)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }

	// văn bản quan trong

    public function getToCongTac($idUser=NULL,$status=NULL)

    {

        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idUser);

//        $this->db->where('tbl_vanbanden.iTrangThai',0);

        $this->db->where('tbl_vanbanden.iToCongTac',1);

        $this->db->select('tbl_chuyennhanvanban.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        $this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	public function getDonThu($idUser=NULL,$Phong=NULL,$CV = NULL,$trangthai=NULL,$trangthai_ht=NULL)

    {

        if($idUser){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idUser);

        }

        if($Phong){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$Phong);

        }

        if($CV){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$CV);

        }

		if(!empty($trangthai)){

			$this->db->where('tbl_vanbanden.iTrangThai',0);

		}

		if(!empty($trangthai_ht)){

			$this->db->where('tbl_vanbanden.iTrangThai',1);

		}

        $this->db->where_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

		$this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->select('tbl_chuyennhanvanban.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        $this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	public function getCTD($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$GiayMoi = NULL,$PGDThamMuu=NULL,$PPThamMuu=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        if($PGDThamMuu){

            $this->db->where('tbl_vanbanden.iPgdThamMuu',$PGDThamMuu);

        }

        if($PPThamMuu){

            $this->db->where('tbl_vanbanden.iPpThamMuu',$PPThamMuu);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

//        $this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d',time()));

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','3');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        if(!empty($idDepartment)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        if(!empty($idDepartment1)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

//        if(!empty($idDepartmentCT)){

//            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

//            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);

//            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

//        }

        if(!empty($Chuyenvien)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }

    // danh sách chờ của chi cục

    public function getDocCcDangAwait($trangthai_truyennhan = NULL, $tcdn_active = NULL,$department_id=NULL,$ccp_id=NULL,$Chuyenvien=NULL,$GiayMoi=NULL)

    {

        if($trangthai_truyennhan){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','3');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($tcdn_active)){

            $this->db->where('tbl_chicuc.tcdn_active',$tcdn_active);

        }

        if(!empty($department_id)){

            $this->db->where('tbl_chicuc.department_id',$department_id);

        }

        if(!empty($ccp_id)){

            $this->db->where('tbl_chicuc.tp_id',$ccp_id);

        }

        if(!empty($Chuyenvien)){

            $this->db->where('tbl_chicuc.ccp_id',$Chuyenvien);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSoDen','desc');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }

	// phó phòng phối hợp từ chối



    public function PPPHTuChoi_welcome($idDepartment1=NULL,$giaymoi=NULL)

    {

        if($giaymoi == 1){

            $this->db->where('tbl_vanbanden.iGiayMoi',1);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi',2);

        }

        $this->db->where('tbl_phongphoihop.iTRangThai_TuChoi',1);

        $this->db->where('tbl_phongphoihop.PK_iMaPhong',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    } 

    public function PPPHTuChoi($idDepartment1=NULL,$giaymoi=NULL)

    {

        if($giaymoi == 1){

            $this->db->where('tbl_vanbanden.iGiayMoi',1);

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi',2);

        }

        $this->db->where('tbl_phongphoihop.iTRangThai_TuChoi',1);

        $this->db->where('tbl_phongphoihop.PK_iMaPhong',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');

        $this->db->select('tbl_phongphoihop.PK_iMaPhongPH,tbl_phongphoihop.sNoiDung_TuChoi,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sTenLVB,tbl_vanbanden.iSoTrang,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sNgayKy,tbl_phongphoihop.iTrangThai,tbl_phongphoihop.sMoTa as mieuta,tbl_files_vbden.sDuongDan,tbl_phongphoihop.PK_iMaCB');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        return $this->db->get()->num_rows();

    }

	// giấy mời đã hoàn thành

	public function getDocTPCDHT($idDepartment1=NULL,$idDepartment=NULL,$GiayMoi = NULL){



        //$this->db->where('tbl_vanbanden.iTrangThai','0');

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

			$this->db->where('tbl_vanbanden.iTrangThai',1);

            //$this->db->where('tbl_vanbanden.sGiayMoiNgay <','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);

        //$this->db->where_in('tbl_chuyennhanvanban.CapGiaiQuyet',array('7','8'));

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        $this->db->order_by('tbl_vanbanden.iSODen','desc');

//        return $this->db->last_query($this->db->get());

        return $this->db->get()->num_rows();

    }



    public function getDocCT_welcome($idDepartmentCT=NULL)

    {

        $this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where('tbl_vanbanden.iTrangThai!=','3');

//        $this->db->where('tbl_vanbanden.iTrangThai','0');

        $this->db->where('tbl_vanbanden.iGiayMoi','2');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        if(!empty($idDepartmentCT)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }



	public function getDocCT($idDepartmentCT=NULL)

    {

        $this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where('tbl_vanbanden.iTrangThai!=','3');

//        $this->db->where('tbl_vanbanden.iTrangThai','0');

//        $this->db->where('tbl_vanbanden.iGiayMoi','2');

        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($idDepartmentCT)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

//        return $this->db->last_query();

        return $this->db->get()->num_rows();

    }

    public function getDocCT2($iMaCB=NULL)

    {

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where('tbl_vanbanden.iTrangThai!=','3');

        $this->db->from('tbl_vanbanden');

        if(!empty($iMaCB)){

            $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chicuc.ccp_id',$iMaCB);

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    public function getDocCT1($iMaCB=NULL)

    {

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where('tbl_vanbanden.iTrangThai!=','3');

        $this->db->from('tbl_vanbanden');

        if(!empty($iMaCB)){

            $this->db->join('tbl_chicuc','tbl_chicuc.doc_id = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chicuc.tp_id',$iMaCB);

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	// danh sách tổ công tác hoàn thành - không hoàn thành

    public function getToCongTacHT($idUser=NULL,$cacloaivanban=NULL,$idUser_CV=NULL,$trangthai_truyennhan=NULL)

    {

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        // $this->db->where('tbl_vanbanden.iTrangThai','0');

        $this->db->where('tbl_vanbanden.iGiayMoi','2');

        if($cacloaivanban == '2'){

            $this->db->where('tbl_vanbanden.iTrangThai',1);

        }else{

            $this->db->where('tbl_vanbanden.iTrangThai',0);

        }

		if(!empty($idUser)){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idUser);

        }

        if(!empty($idUser_CV)){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser_CV);

        }
		
		 if(!empty($trangthai_truyennhan)){

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);

        }

//        $this->db->where('tbl_vanbanden.iTrangThai',0);

        $this->db->where('tbl_vanbanden.iToCongTac',1);

        $this->db->select('tbl_chuyennhanvanban.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        $this->db->group_by('tbl_chuyennhanvanban.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	// danh sách chờ duyệt hạn

	public function dsGiaHan($idUser=NULL)

    {

        $this->db->where('tbl_vanbanden.iTrangThai !=','1');

        $this->db->where('tbl_vanbanden.iTrangThai_ThemHan',1);

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');

        if ($idUser){

			if ($idUser == 735){
				$this->db->where_in('tbl_vanbanden.iLanhDao',array(735,694,695));
			}
			else {
				$this->db->where('tbl_vanbanden.iLanhDao',$idUser);
			}

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	public function getDocGoGDAwait($idDepartment1=NULL,$stcphoihop=NULL){

		if(!empty($stcphoihop)){

            $this->db->where('tbl_vanbanden.iSTCPhoiHop',$stcphoihop);

        }else{

            $this->db->where('tbl_vanbanden.iSTCPhoiHop !=',1);

        }

        $this->db->where('tbl_vanbanden.iTrangThai','0');

        $this->db->where('tbl_vanbanden.iGiayMoi','2');

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        if($idDepartment1){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$idDepartment1);

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >','3');

            $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');

        }

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');



        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    public function layVBGiaHanTuChoi_welcome($idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL)

    {

        $this->db->where_in('tbl_vanbanden.iTrangThai_ThemHan',array(1,2,3));
		
		 $this->db->where('tbl_vanbanden.iTrangThai != ',1);

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

       

        if(!empty($idDepartment)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

        }

        if(!empty($idDepartment1)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

        }

        if(!empty($Chuyenvien)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }



	public function layVBGiaHanTuChoi($idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL)

    {

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->where_in('tbl_vanbanden.iTrangThai_ThemHan',array(1,2,3));

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($idDepartment)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);

        }

        if(!empty($idDepartment1)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);

        }

        if(!empty($Chuyenvien)){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	// danh sách phối hợp chủ trì



    public function getDocAwaitDSCTPPH_welcome($idDepartment1=NULL)

    {

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
	$this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        

        if(!empty($idDepartment1)){

            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }



    public function getDocAwaitDSCTPPH($idDepartment1=NULL)

    {

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($idDepartment1)){

            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	public function getDocAwaitDSCTPPH_sua($idDepartment1=NULL)

    {

        /*$this->db->where('PK_iMaPhong',$idDepartment1);

        $this->db->select('PK_iMaVBDen');

        $this->db->from('tbl_phongphoihop');

        $this->db->group_by('PK_iMaVBDen');

        return $this->db->get()->num_rows();*/


        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($idDepartment1)){

            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();


    }

	// người dùng phối hợp xử lý



    public function getListCVPH_welcome($idDepartment1=NULL,$idUser=NULL)

    {

        if($idUser){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        

        if(!empty($idDepartment1)){

            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }



	public function getListCVPH($idDepartment1=NULL,$idUser=NULL)

    {

        if($idUser){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($idDepartment1)){

            $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        }

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	public function getListCVPH_sua($idDepartment1=NULL,$idUser=NULL)

    {

        if($idUser){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

        }

        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

	//người dùng xử lý

    public function countDocGo($idUser=NULL)

    {

        $this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));

        $this->db->where('tbl_vanbanden.iGiayMoi !=',3);

        $this->db->where('tbl_vanbanden.iTrangThai !=','3');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');

        if ($idUser){

            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$idUser);

        }

        return $this->db->get()->num_rows();

    }

    public function getDuyetBaoCao($idUser=NULL,$FK_iMaPhHD=NULL){

        if(!empty($idUser)){

            $this->db->where('tbl_soanbaocao.PK_iMaCBNhap',$idUser);

        }

        if(!empty($FK_iMaPhHD)){

            $this->db->where('tbl_soanbaocao.PK_iMaPhongNhap',$FK_iMaPhHD);

        }

        $this->db->where('tbl_soanbaocao.iTrangThai','1');

        $this->db->select('tbl_noidungbaocao.sNoiDung as smieuta,tbl_noidungbaocao.sNoiDungDeXuat,tbl_soanbaocao.PK_iMaSoanBaoCao,tbl_soanbaocao.sFile,tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,sGiayMoiGio,sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNoiDung');

        $this->db->from('tbl_soanbaocao');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_soanbaocao.PK_iMaVBDen','left');

        $this->db->join('tbl_noidungbaocao','tbl_noidungbaocao.PK_iMaNoiDung = tbl_soanbaocao.PK_iMaNoiDung','left');

        $this->db->group_by('tbl_noidungbaocao.PK_iMaNoiDung');

        $this->db->order_by('tbl_soanbaocao.sNgayNhap','desc');

        return $this->db->get()->num_rows();

    }

    public function getDocDaPPH($Quyen = NULL,$idDepartment1=NULL,$idUser=NULL,$iTrangThaiPH=NULL,$GiayMoi=NULL)

    {

        if($Quyen == 6 || $Quyen == 3){

            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'" AND (iTrangThai = 1 OR iTrangThai = 2))');

            //$this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        }

        if($Quyen == 7){

            $this->db->where('tbl_phongphoihop.input_per',$idUser);

            $this->db->where('tbl_phongphoihop.iTrangThai !=',3);

            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);

            //$this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);

            //$this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

            //$this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'" AND (iTrangThai = 1 OR iTrangThai = 3))');

        }

        if($Quyen == 11){

            $this->db->where('tbl_phongphoihop.PK_iMaCB',$idUser);

            $this->db->where('tbl_phongphoihop.iTrangThai',$iTrangThaiPH);

            $this->db->where('tbl_phongphoihop.iTrangThai_TuChoi !=',1);

//            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen  NOT IN (SELECT PK_iMaVBDen FROM tbl_phongphoihop WHERE PK_iMaPhong = "'.$idDepartment1.'")');

        }

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=','2017-11-06');

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        $this->db->where('tbl_chuyennhanvanban.iGiayMoi !=','3');
		
		$this->db->where('tbl_vanbanden.iTrangThai !=', 1);

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');

        $this->db->select('tbl_vanbanden.PK_iMaVBDen');

        $this->db->from('tbl_chuyennhanvanban');

        $this->db->join('tbl_vanbanden','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    public function getDocAwaitBGD($idDepartment1=NULL, $GiayMoi=NULL,$User=NULL)

    {

        if(!empty($GiayMoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$GiayMoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d'));

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }
		$this->db->where('tbl_vanbanden.iTrangThai !=','1');

        $this->db->not_like('tbl_vanbanden.sSeenBGD',$User);
        
        if($User == 679)$this->db->where('tbl_vanbanden.sNgayNhap >=',date('2019-10-11 12:00:00'));
        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->select('tbl_vanbanden.sMoTa,tbl_chuyennhanvanban.sMoTa as mieuta,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,tbl_vanbanden.iDuHop,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,sGiayMoiDiaDiem,tbl_vanbanden.sHanThongKe,tbl_vanbanden.iPhongDuHop,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,iSTCChuTri,iVanBanTBKL,iToCongTac,iTrangThai_ThemHan,sLyDoTuChoiKetQua,sLyDoTuChoiVanBanDen,iSTCPhoiHop');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_vanbanden.PK_iMaVBDen = tbl_chuyennhanvanban.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        return $this->db->get()->num_rows();

    }

    public function getDocSeenBGD($idDepartment1=NULL,$User=NULL,$Giaymoi=NULL)

    {

        if(!empty($Giaymoi)){

            $this->db->where('tbl_vanbanden.iGiayMoi',$Giaymoi);

            $this->db->where('tbl_vanbanden.sGiayMoiNgay >=',date('Y-m-d'));

        }else{

            $this->db->where('tbl_vanbanden.iGiayMoi','2');

        }

        $this->db->like('tbl_chuyennhanvanban.PK_iMaPhongPH',$idDepartment1);

        $this->db->like('tbl_vanbanden.sSeenBGD',$User);

        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sSeenBGD,tbl_chuyennhanvanban.sMoTa as mieuta,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,tbl_vanban_chuyenlai.sLyDo,tbl_vanban_chuyenlai.sGhiChu,tbl_vanban_chuyenlai.PK_iMaCBTuChoi,iVanBanQPPL,tbl_vanbanden.iSTCChuTri,tbl_vanbanden.iVanBanTBKL,tbl_vanbanden.iToCongTac,tbl_vanbanden.iTrangThai_ThemHan,tbl_vanbanden.sLyDoTuChoiKetQua,tbl_vanbanden.sLyDoTuChoiVanBanDen,tbl_vanbanden.iSTCPhoiHop');

        $this->db->from('tbl_vanbanden');

        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_phongphoihop','tbl_phongphoihop.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->join('tbl_vanban_chuyenlai','tbl_vanban_chuyenlai.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');

        $this->db->order_by('tbl_vanbanden.sNgayNhap','desc');

//        return $this->db->last_query($this->db->get());

        return $this->db->get()->num_rows();

    }

    public function countlanhdaogiao($lanhdao_id)

    {
        
        $this->db->where('FK_iMaCB_LanhDao ',$lanhdao_id);
        $this->db->where('qlvDetails_active <',2);
        $this->db->from('tbl_lanhdaogiao_details');
        return $this->db->get()->num_rows();

    }

    public function countphongthuly($phong_id)

    {
        
        $this->db->where('main_department ',$phong_id);
        $this->db->where('qlvDetails_active',2);
        $this->db->from('tbl_lanhdaogiao_details');
        return $this->db->get()->num_rows();

    }

    public function countphongphoihop($phong_id)

    {
        $this->db->like('department_id',$phong_id);
        $this->db->not_like('phongdaphoihop',$phong_id);
        $this->db->from('tbl_lanhdaogiao_details');
        return $this->db->get()->num_rows();

    }

    public function countccpthuly($cb_id)

    {
        
        $this->db->where('FK_iMaCB_CCP ',$cb_id);
        $this->db->where('qlvDetails_active',3);
        $this->db->from('tbl_lanhdaogiao_details');
        return $this->db->get()->num_rows();

    }

    public function counttpccthuly($cb_id)

    {
        $this->db->where('FK_iMaCB_CC_TP ',$cb_id);
        $this->db->where('qlvDetails_active',4);
        $this->db->from('tbl_lanhdaogiao_details');
        return $this->db->get()->num_rows();

    }

    public function countphophongthuly($cb_id)

    {
        $this->db->where('FK_iMaCB_PP_CT ',$cb_id);
        $this->db->where('qlvDetails_active',5);
        $this->db->from('tbl_lanhdaogiao_details');
        return $this->db->get()->num_rows();

    }

    public function countchuyenvienthuly($cb_id)

    {
        $this->db->where('FK_iMaCB_CV_CT ',$cb_id);
        $this->db->where('qlvDetails_active',6);
        $this->db->from('tbl_lanhdaogiao_details');
        return $this->db->get()->num_rows();

    }

    public function countphoihopthuly($cb_id)

    {
        $this->db->where('FK_iMaCB_Nhan ',$cb_id);
        $this->db->where('iTrangThai',1);
        $this->db->from('tbl_chuyennhan_lanhdaogiao');
        return $this->db->get()->num_rows();

    }

}