<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvanbanphoihop extends CI_Model {
	// xóa bản ghi
	public function xoaChuyenNhan($machuyen,$mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where('PK_iMaCN >=',$machuyen);
		$this->db->delete('tbl_chuyennhanvanban');
	}
	// lấy mã chuyền nhận
	public function layMaChuyenNhan($taikhoan,$mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where('PK_iMaCVCT',$taikhoan);
		return $this->db->get('tbl_chuyennhanvanban')->row_array();
	}
	// lấy quán trình truyền nhận phòng phối hợp
	public function layQT_Phong_PH($mavanban,$maphong)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where_in('PK_iMaPhong',$maphong);
		$this->db->select('PK_iMaCB,sThoiGian,input_per,sMoTa');
		$this->db->order_by('PK_iMaPhong','asc');
		$this->db->order_by('sThoiGian','asc');
		return $this->db->get('tbl_phongphoihop')->result_array();
	}
	// cập nhật kết quả
	public function capnhatKetQua($mavanban,$phongban)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->where('phong_id',$phongban);
		$this->db->set('active',5);
		$this->db->update('kehoach');
		return $this->db->affected_rows();
	}
	// cập nhật kết quả
	public function capnhatKetQua_Hai($mavanban,$phongban,$data)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->where('phong_id',$phongban);
		$this->db->update('kehoach',$data);
		return $this->db->affected_rows();
	}
	// phó phòng lấy tài khoản chuyên viên để trả lại
	public function layTK_CV($mavanban,$taikhoan,$phongphoihop)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->where('FK_iMaPB_PH',$phongphoihop);
		$this->db->where('CH_TL <',3);
		$this->db->select('PK_iMaCN,FK_iMaNguoi_Gui');
		return $this->db->get('tbl_chuyennhan_phoihop')->row_array();
	}
	// lấy phó phòng và chuyền viên
	public function layDS_TL($mavanban,$taikhoan,$phongphoihop,$chicuctruong=NULL)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan !=',$taikhoan);
		if(!empty($chicuctruong))
		{
			$this->db->where('FK_iMaNguoi_Nhan !=',$chicuctruong);
		}
		$this->db->where('FK_iMaPB_PH',$phongphoihop);
		$this->db->select('PK_iMaCN,FK_iMaNguoi_Nhan');
		$this->db->group_by('FK_iMaNguoi_Nhan');
		return $this->db->get('tbl_chuyennhan_phoihop')->result_array();
	}
	// lấy chi cục trưởng
	public function layCCT()
	{
		$this->db->where('FK_iMaPhongHD',12);
		$this->db->where('iTrangThai',0);
		$this->db->where('iQuyenHan_DHNB',6);
		$this->db->select('PK_iMaCB as PK_iMaCBChuyen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy danh sách lãnh đạo cấp trên của phòng phối hợp
	public function layLanhDao_PH($mavanban,$taikhoan,$phongphoihop)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaPB_PH',$phongphoihop);
		$this->db->where('CT_PH',2);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->where('CH_TL <',3);
		$this->db->select('FK_iMaNguoi_Gui');
		return $this->db->get('tbl_chuyennhan_phoihop')->row_array();
	}
	// lấy danh sách phòng (PP vs CV)
	public function layDS_PB($phongban,$canbo,$quyendb=NULL,$quyen=NULL)
	{
		if($phongban==12)
		{
			$this->db->where('FK_iMaCV !=',16);
		}
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->where('iTrangThai',0);
		$this->db->where('PK_iMaCB !=',$canbo);
		if(!empty($quyendb))
		{
			$this->db->where('iQuyenDB',$quyendb);
		}
		if(!empty($quyen))
		{
			$this->db->where('iQuyenHan_DHNB',$quyen);
		}
		$this->db->select('PK_iMaCB,sHoTen,iQuyenHan_DHNB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy chuyên viên xử lý chính của phòng chủ trì
	public function layCV_CT($mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where('iGiayMoi <',3);
		$this->db->select('PK_iMaCBChuyen,PK_iMaCVCT');
		$this->db->order_by('PK_iMaCN','desc');
		return $this->db->get('tbl_chuyennhanvanban')->row_array();
	}
	// lấy người duyệt
	public function laynguoiduyet($taikhoan,$mavanban)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->order_by('PK_iMa','asc');
		return $this->db->get('tbl_luuvet_chuyennhan')->row_array();
	}
	// lấy người duyệt
	// public function laynguoiduyetduyet($taikhoan,$mavanban)
	// {
	// 	$this->db->where('FK_iMaCB_Nhan',$taikhoan);
	// 	$this->db->where('FK_iMaVBDen',$mavanban);
	// 	return $this->db->get('tbl_luuvet_chuyennhan')->row_array();
	// }
	public function laynguoiduyetduyet($taikhoan,$mavanban)
	{
		$this->db->where('PK_iMaCVCT',$taikhoan);
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->order_by('PK_iMaCN','desc');
		return $this->db->get('tbl_chuyennhanvanban')->row_array();
	}
	// lấy phòng phối hợp
	public function layPB_PH($mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where('iGiayMoi <',3);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->select('PK_iMaPhongPH');
		$this->db->order_by('PK_iMaCN','desc');
		return $this->db->get('tbl_chuyennhanvanban')->row_array();
	}
	// lấy danh sách phòng ban
	public function layPB($maphongban)
	{
		$this->db->where('PK_iMaPB >',0);
		$this->db->where('PK_iMaPB <',78);
		$this->db->select('PK_iMaPB,sTenPB');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// lấy phó phòng và trưởng phòng
	public function layPH_TP($mavanban,$taikhoan)
	{
		$this->db->where('PK_iMaCVCT',$taikhoan);
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where('iGiayMoi <',3);
		$this->db->where('CapGiaiQuyet >',0);
		$this->db->select('PK_iMaCBChuyen');
		return $this->db->get('tbl_chuyennhanvanban')->result_array();
	}
	
	//ricknguyen321	
	public function layTPVBden($mavanban)
	{
		$query = $this->db->query("select cn.PK_iMaCBChuyen from tbl_chuyennhanvanban as cn join tbl_canbo as cb on cn.PK_iMaCBChuyen = cb.PK_iMaCB where PK_iMaVBDen= ".$mavanban." and iGiayMoi < 3 and CapGiaiQuyet > 0 and (cb.iQuyenHan_DHNB = 6 or cb.iQuyenHan_DHNB = 3);");
        return $query->result_array();
	}
	
	// lấy thông tin văn bản chờ đề xuất
	public function layTTVB_DeXuat($taikhoan,$trangthai,$ct_ph=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->where('ph.iTrangThai',$trangthai);
		$this->db->where('vb.iTrangThai != ',1);
		if(!empty($ct_ph))
		{
			$this->db->where('CT_PH',$ct_ph);
		}
		$this->db->select('ph.*,vb.PK_iMaVBDen,vb.iSoDen,vb.sMoTa,vb.sHanThongKe');
		$this->db->from('tbl_chuyennhan_phoihop as ph');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=ph.FK_iMaVBDen');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// lấy thông tin văn bản chờ đề xuất
	public function demTTVB_DeXuat($taikhoan,$trangthai,$ct_ph=NULL)
	{
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->where('ph.iTrangThai',$trangthai);
		$this->db->where('vb.iTrangThai != ',1);
		if(!empty($ct_ph))
		{
			$this->db->where('CT_PH',$ct_ph);
		}
		$this->db->select('vb.iSoDen');
		$this->db->from('tbl_chuyennhan_phoihop as ph');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=ph.FK_iMaVBDen');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->num_rows();
	}
	// kiểm tra xem đã xử lý chưa
	public function kiemtraTaiKhoan($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->where('iTrangThai',1);
		return $this->db->get('tbl_chuyennhan_phoihop')->num_rows();
	}
	// cập nhật trang thái  đã xử lý
	public function capnhatTrangThai($mavanban,$id_insert,$phongphoihop)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaPB_PH',$phongphoihop);
		$this->db->where('PK_iMaCN !=',$id_insert);
		$this->db->set('iTrangThai',2);
		$this->db->update('tbl_chuyennhan_phoihop');
	}
	// cập nhật trạng thái 
	public function capnhatTrangThai_PH($mavanban,$taikhoan,$trangthai)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->set('iTrangThai',$trangthai);
		$this->db->update('tbl_chuyennhan_phoihop');
	}
	public function layTrangThai_PH_hai($mavanban,$taikhoan,$phongban)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->where('FK_iMaPB_PH',$phongban);
		$this->db->where('CH_TL',2);
		$this->db->select('PK_iMaCN');
		$this->db->order_by('PK_iMaCN','desc');
		return $this->db->get('tbl_chuyennhan_phoihop')->row_array();
	}
	public function capnhatTrangThai_PH_hai($machuyen)
	{
		$this->db->where('PK_iMaCN',$machuyen);
		$this->db->set('iTrangThai',3);
		$this->db->update('tbl_chuyennhan_phoihop');
	}
	public function capnhatTrangThai_PH_TL($mavanban,$taikhoan,$trangthai)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->set('CH_TL',$trangthai);
		$this->db->update('tbl_chuyennhan_phoihop');
	}

	// lấy mã chuyền nhận cuối cùng của người duyệt
	public function layMaCN_Cuoi($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->select('PK_iMaCN,FK_iMaNguoi_Gui,sThoiGian_Chuyen,sNoiDung');
		$this->db->order_by('PK_iMaCN','desc');
		return $this->db->get('tbl_chuyennhan_phoihop')->row_array();
	}
	// cập nhật trang thái 
	public function capnhatTrangThai_DY($machuyennhan)
	{
		$this->db->where('PK_iMaCN',$machuyennhan);
		$this->db->set('iTrangThai',3);
		$this->db->update('tbl_chuyennhan_phoihop');
		return $this->db->affected_rows();
	}
	// cập nhật trang thái 
	public function capnhatTrangThai_DY_XL($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Gui',$taikhoan);
		$this->db->set('iTrangThai',2);
		$this->db->update('tbl_chuyennhan_phoihop');
	}
	// lấy lãnh đạo sở
	public function layLD_So($mavanban,$phongban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where('PK_iMaPhongPH',$phongban);
		$this->db->select('PK_iMaCBChuyen');
		return $this->db->get('tbl_chuyennhanvanban')->row_array();
	}
	// lấy lãnh đao phòng chuyển
	public function layLD_Phong($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->select('FK_iMaNguoi_Gui,sThoiGian_Chuyen');
		return $this->db->get('tbl_chuyennhan_phoihop')->row_array();
	}
	// xóa văn bản đã giao
	public function xoaVBDaGiao($mavanban,$phongban,$taikhoan=NULL)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->where('phong_id',$phongban);
		if(!empty($taikhoan))
		{
			$this->db->where('lanhdao_id',$taikhoan);
		}
		$this->db->delete('kehoach');
	}
	// kiểm tra đã thêm vào bảng kế hoạch chưa
	public function kiemtrathem_KH($mavanban,$taikhoan)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->where('canbo_id',$taikhoan);
		return $this->db->get('kehoach')->num_rows();
	}
	// cập nhật vào bảng kế hoạch
	public function capnhat_KH($mavanban,$taikhoan,$data)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->where('canbo_id',$taikhoan);
		$this->db->update('kehoach',$data);
		return $this->db->affected_rows();
	}
	// lấy danh sách chuyền nhận yêu cầu phối hợp
	public function layDS_CN($mavanban)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->order_by('FK_iMaPB_PH','asc');
		$this->db->order_by('sThoiGian_Chuyen','asc');
		return $this->db->get('tbl_chuyennhan_phoihop')->result_array();
	}
	// lấy người gửi kết quả
	public function layNguoiGui_KQ($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDen',$mavanban);
		$this->db->where('FK_iMaNguoi_Nhan',$taikhoan);
		$this->db->where('CH_TL',2);
		$this->db->select('FK_iMaNguoi_Gui');
		return $this->db->get('tbl_chuyennhan_phoihop')->row_array();
	}
}

/* End of file Mvanbanphoihop.php */
/* Location: ./application/models/Vanbanden/Mvanbanphoihop.php */
