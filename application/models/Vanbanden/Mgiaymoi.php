<?php

class Mgiaymoi extends CI_Model {
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/12/2017
 * Time: 8:56 AM
 */
	 // lấy danh sách giấy mời đã họp mà chưa hoàn thành
    public function layGiayMoiDaHopChuaHT($taikhoan)
    {
        $this->db->where('cn.PK_IMaCVCT',$taikhoan);
        $this->db->where('vb.iTrangThai',0);
        $this->db->where('sGiayMoiNgay >','1970-01-01');
        $this->db->where('sGiayMoiNgay <',date('Y-m-d'));
        $this->db->select('vb.*');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
    public function demGiayMoiDaHopChuaHT($taikhoan)
    {
        $this->db->where('cn.PK_IMaCVCT',$taikhoan);
        $this->db->where('vb.iTrangThai',0);
        $this->db->where('sGiayMoiNgay >','1970-01-01');
        $this->db->where('sGiayMoiNgay <',date('Y-m-d'));
        $this->db->select('vb.PK_iMaVBDen');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->num_rows();
    }
    // qua trinh truyen nhan van ban
    public function getDocProcess($idDoc = NULL,$idDepartment1=NULL,$dir=NULL,$lever=NULL,$order=NULL)
    {
        if(!empty($idDoc)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaVBDen',$idDoc);
        }
        if(!empty($idDepartment1)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        }
        if(!empty($dir)){
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBChuyen',$dir);
        }
        if(!empty($lever)){
            $this->db->where('tbl_chuyennhanvanban.CapGiaiQuyet',$lever);
        }
//        $this->db->where('tbl_chuyennhanvanban.iGiayMoi','1');
        $this->db->select('PK_iMaCN,PK_iMaVBDen,PK_iMaCBChuyen,PK_iMaCVPH,PK_iMaCBNhan,sMoTa,sThoiGianHetHan,PK_iMaPhongPH,PK_iMaCVCT,PK_iMaPhongCT');
        $this->db->from('tbl_chuyennhanvanban');
        if(!empty($order)){
            $this->db->order_by('tbl_chuyennhanvanban.PK_iMaCN','desc');
            $this->db->limit(1);
        }
//        $this->db->order_by('tbl_chuyennhanvanban.sThoiGian','asc');
        return $this->db->get()->result_array();
    }
    /**
     * lấy danh sách hoặc 1 dự liệu
     */
    public function layDuLieu($primary_key=NULL,$id=NULL)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        $this->db->select('tbl_vanbanden.*,tbl_canbo.sHoTen');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = tbl_vanbanden.FK_iMaNguoiNhap','left');
        return $this->db->get()->result_array();
    }


}
