<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlichcongtac extends CI_Model {

	// lấy lịch công tác tuần
	public function layLichCongTacTuan($ngaybd,$ngaykt)
	{
		$this->db->where('iGiayMoi',1);
		$this->db->where('sGiayMoiNgay >=',$ngaybd);
		$this->db->where('sGiayMoiNgay <=',$ngaykt);
		$this->db->select('PK_iMaVBDen,sMoTa,sGiayMoiNgay,sTenDV,sGiayMoiDiaDiem,sGiayMoiGio');
		$this->db->from('tbl_vanbanden as vbd');
		$this->db->order_by('sGiayMoiNgay','asc');
		$this->db->order_by('sGiayMoiGio','asc');
		return $this->db->get()->result_array();
	}
	// lấy tên viết tắt của phòng giải quyết
	public function layTenVietTat($ma)
	{
		$this->db->where('PK_iMaVBDen',$ma);
		$this->db->select('sVietTat');
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cn.PK_iMaPhongCT');
		return $this->db->get()->row_array();
	}
	// ===========================================================================================
	// lấy mã văn bản đi, dựa trên mã tài khoản và quyền đăng nhập
	public function layMaVB($macb=NULL,$taikhoan,$quyen,$phongban,$thoigianbd,$thoigiankt)
	{
		$this->db->where('sGiayMoiNgay >=',$thoigianbd);
		$this->db->where('sGiayMoiNgay <=',$thoigiankt);

		$this->db->group_start();
		$this->db->where('PK_iMaCBNhan',$taikhoan);
		$this->db->or_where("PK_iMaCBChuyen",$taikhoan);
		$this->db->group_end();

		$this->db->select('ch.PK_iMaVBDen');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=ch.PK_iMaVBDen');
		$this->db->group_by('PK_iMaVBDen');
		return $this->db->get('tbl_chuyennhanvanban as ch')->result_array();
	}
	//  lấy phòng phối hợp
	public function layPhongPH($mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->select('PK_iMaPhongPH');
		$this->db->order_by('PK_iMaPhongPH','desc');
		return $this->db->get('tbl_chuyennhanvanban')->row_array();
	}
	// lấy danh sách phòng ban
	public function layPhongBan()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('sTenPB,PK_iMaPB');
		return $this->db->get('tbl_phongban')->result_array();
	}
	public function layVBDen($ngaybd,$ngaykt,$ma)
	{
		$this->db->where('iGiayMoi',1);
		$this->db->where('sGiayMoiNgay >=',$ngaybd);
		$this->db->where('sGiayMoiNgay <=',$ngaykt);
		//$this->db->where_in('PK_iMaVBDen',$ma);
		$this->db->select('PK_iMaVBDen,sMoTa,sNoiDung,sGiayMoiNgay,sTenDV,sKyHieu,sGiayMoiDiaDiem,sGiayMoiGio,iDuHop,iPGDDuHop,iPhongDuHop');
		$this->db->from('tbl_vanbanden as vbd');
		$this->db->order_by('sGiayMoiNgay','asc');
		$this->db->order_by('sGiayMoiGio','asc');
		return $this->db->get()->result_array();
	}
	// lấy thông tin báo cáo họp: tên người upfiles, phòng ban người upfiles và đường dẫn file
	public function layBaoCaoHop($mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->where('active',1);
		$this->db->select('sHoTen,sTenPB,sDuongDanFile,sTenFiles');
		$this->db->from('tbl_file_ketqua as f');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=f.PK_iMaCB');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=f.PK_iMaPB');
		return $this->db->get()->result_array();
	}
	// lấy ý kiến chỉ đạo
	public function layChiDao($ma)
	{
		$this->db->where('PK_iMaVBDen',$ma);
		$this->db->select('PK_iMaVBDen,sMoTa,PK_iMaCBNhan,sHoTen');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=n.PK_iMaCBNhan','left');
		$this->db->order_by('PK_iMaVBDen','asc');
		$this->db->order_by('PK_iMaCN','asc');
		return $this->db->get('tbl_chuyennhanvanban as n')->result_array();
	}
	
	public function layBGD($mavanban)
	{
		$this->db->where('iQuyenHan_DHNB >',3);
		$this->db->where('iQuyenHan_DHNB <',6);
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaCB, sHoTen, iQuyenHan_DHNB');
		$this->db->order_by('iQuyenHan_DHNB','asc');
		return $this->db->get('tbl_canbo')->result_array();
	}
}

/* End of file Mlichcongtac.php */
/* Location: ./application/models/Vanbanden/Mlichcongtac.php */