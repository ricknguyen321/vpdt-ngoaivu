<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhoso extends CI_Model {

	// lấy danh sách hồ sơ
	public function layDSHS($phongban,$tenhoso=NULL,$limit=NULL,$offset=NULL) {
		$this->db->where('FK_iMaPB',$phongban);
		if(!empty($tenhoso)){
			$this->db->like('sTenHS',$tenhoso);
		}
		$this->db->where('iTrangThai',1);
		$this->db->select('sTenHS,sMoTa,PK_iMaHS');
		$this->db->from('tbl_hoso');
		if(!empty($limit)){
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	public function demDSHS($phongban,$tenhoso=NULL) {
		$this->db->where('FK_iMaPB',$phongban);
		if(!empty($tenhoso)){
			$this->db->like('sTenHS',$tenhoso);
		}
		$this->db->where('iTrangThai',1);
		$this->db->select('sTenHS,sMoTa,PK_iMaHS');
		$this->db->from('tbl_hoso');
		return $this->db->get()->num_rows();
	}
	// lấy danh sách phòng ban
	public function layPB(){
		$this->db->where('iLoaiPhanMem',3);
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaPB,sTenPB');
		return $this->db->get('tbl_phongban')->result_array();
	}

	// lấy hồ sơ theo cá nhân
	public function layHScanhan($macanbo)
	{
		$this->db->where('FK_iMaCB',$macanbo);
		$this->db->order_by('iTrangThai','asc');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get('tbl_hoso')->result_array();
	}
	// lấy hồ sơ hiện thị
	public function layHoSo($macanbo)
	{
		$this->db->where('FK_iMaCB',$macanbo);
		$this->db->select('PK_iMaHS,sTenHS');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get('tbl_hoso')->result_array();
	}
	// lấy số văn bản đã chọn
	public function layHSDaChon($mahoso,$vanban){
		if($vanban=='di'){
			$this->db->where('FK_iMaHS',$mahoso);
			$this->db->where('FK_iMaVBDi >',0);
			$this->db->select('FK_iMaVBDi');
			return $this->db->get('tbl_hosovanban')->result_array();
		}else{
			$this->db->where('FK_iMaHS',$mahoso);
			$this->db->where('FK_iMaVBDen >',0);
			$this->db->select('FK_iMaVBDen');
			return $this->db->get('tbl_hosovanban')->result_array();
		}
	}
	// lấy văn bản theo số
	public function layVBTheoSo($vanban,$sovanban,$trichyeu,$mavanban)
	{

		if($vanban=='di'){
			if(!empty($sovanban)){
				$this->db->where('iSoVBDi',$sovanban);
			}
			if(!empty($trichyeu)){
				$this->db->like('sMoTa',$trichyeu);
			}
			if(!empty($mavanban)){
				$this->db->where_not_in('PK_iMaVBDi',$mavanban);
			}
			$this->db->select('PK_iMaVBDi as mavanban,sKyHieu,sMoTa, iSoVBDi as iso');
			return $this->db->get('tbl_vanbandi')->result_array();
		}
		else{
			if(!empty($sovanban)){
				$this->db->where('iSoDen',$sovanban);
			}
			if(!empty($trichyeu)){
				$this->db->like('sMoTa',$trichyeu);
			}
			if(!empty($mavanban)){
				$this->db->where_not_in('PK_iMaVBDen',$mavanban);
			}
			$this->db->select('PK_iMaVBDen as mavanban,sKyHieu,sMoTa, iSoDen as iso');
			return $this->db->get('tbl_vanbanden')->result_array();
		}
	}
	// kiểm tra trùng dữ liệu
	public function kiemtraTrung($taikhoan,$mahoso,$mavanban,$diden)
	{
		$this->db->where('FK_iMaCB',$taikhoan);
		$this->db->where('FK_iMaHS',$mahoso);
		if($diden=='di')
		{
			$this->db->where('FK_iMaVBDi',$mavanban);
		}
		else{
			$this->db->where('FK_iMaVBDen',$mavanban);
		}
		return $this->db->get('tbl_hosovanban')->num_rows();
	}
	// lấy thông tin văn bản đến
	public function layThongTinVBDen($mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->select('sDuongDan,sMoTa,sKyHieu,sTenLVB,iSoDen as sovanban,tbl_vanbanden.sNgayNhap');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen', 'left');
		$this->db->order_by('PK_iMaFileDen','desc');
		return $this->db->get()->row_array();
	}
	// lấy thông tin văn bản đi
	public function layThongTinVBDi($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('sDuongDan,sMoTa,sKyHieu,sTenLVB,iSoVBDi as sovanban,tbl_vanbandi.sNgayNhap');
		$this->db->from('tbl_vanbandi');
		$this->db->join('tbl_files_vbdi','tbl_files_vbdi.FK_iMaVBDi = tbl_vanbandi.PK_iMaVBDi');
		$this->db->join('tbl_loaivanban','tbl_loaivanban.PK_iMaLVB=tbl_vanbandi.FK_iMaLVB');
		$this->db->order_by('PK_iMaFileDi','desc');
		return $this->db->get()->row_array();
	}
	// lấy văn bản chi tiết của hồ sơ
	public function layChiTietHoSo($macanbo=NULL,$mahoso)
	{
		if(!empty($macanbo))
		{
			$this->db->where('FK_iMaCB',$macanbo);
		}
		$this->db->where('FK_iMaHS',$mahoso);
		$this->db->order_by('tbl_hosovanban.sNgayNhap','asc');
		return $this->db->get('tbl_hosovanban')->result_array();
	}

	public function layPhongBanDuThao()
    {
        $this->db->where('iTrangThai',0);
        $this->db->where('sepxep>',0);
        $this->db->select('sTenPB,PK_iMaPB,sVietTat');
        $this->db->order_by('sepxep','asc');
        return $this->db->get('tbl_phongban')->result_array();
    }
    // lấy danh sách hồ sơ theo phòng ban
    public function layDSHS_PB($phongban)
    {
    	$this->db->where('FK_iMaPB',$phongban);
    	$this->db->where('iTrangThai',1);
    	$this->db->select('*');
    	$this->db->order_by('sThoiGian','desc');
    	return $this->db->get('tbl_hoso')->result_array();
    }
	
	 public function demSoVB($mahs)
    {
    	$this->db->where('FK_iMaHS',$mahs);
    	$this->db->select('1');
		$this->db->from('tbl_hosovanban');
    	return $this->db->get()->num_rows();
    }
	public function demSoVBDen($mahs)
    {
    	$this->db->where('FK_iMaHS',$mahs);
		$this->db->where('FK_iMaVBDen >', 0);
    	$this->db->select('1');
    	$this->db->from('tbl_hosovanban');
    	return $this->db->get()->num_rows();
    }
}

/* End of file Mhoso.php */
/* Location: ./application/models/hoso/Mhoso.php */