<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mabtc extends CI_Model {
	
	
	public function layDSHSABTC($limit=NULL,$offset=NULL,$tonghop=NULL)
	{		
		if(!empty($tonghop))
		{
			$this->db->like('don_vi',$tonghop);
			$this->db->or_like('so_hieu',$tonghop);
			$this->db->or_like('mst',$tonghop);
			$this->db->or_like('nguoi_duoc_cap',$tonghop);
			$this->db->or_like('vb_den',$tonghop);
			$this->db->or_like('vb_ca',$tonghop);
			$this->db->or_like('diachi',$tonghop);
			$this->db->or_like('trang_thai',$tonghop);
			$this->db->or_like('note',$tonghop);
			$this->db->or_like('cb_xu_ly',$tonghop);
			$this->db->or_like('vb_thue',$tonghop);
			$this->db->or_like('vb_bhxh',$tonghop);
			$this->db->or_like('vb_ubnd',$tonghop);
			$this->db->or_like('vb_kq',$tonghop);			
		}
		$this->db->select('abtc.*, cb.sHoTen as cb_nhap');
		$this->db->from('tbl_hoso_abtc as abtc');
		$this->db->join('tbl_canbo as cb','abtc.FK_iMaCBNhap=cb.PK_iMaCB');
		$this->db->order_by('ngay_den','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function demDSHSABTC($tonghop=NULL)
	{
		if(!empty($tonghop))
		{
			$this->db->like('don_vi',$tonghop);
			$this->db->or_like('so_hieu',$tonghop);
			$this->db->or_like('mst',$tonghop);
			$this->db->or_like('nguoi_duoc_cap',$tonghop);
			$this->db->or_like('vb_den',$tonghop);
			$this->db->or_like('vb_ca',$tonghop);
			$this->db->or_like('diachi',$tonghop);
			$this->db->or_like('trang_thai',$tonghop);
			$this->db->or_like('note',$tonghop);
			$this->db->or_like('cb_xu_ly',$tonghop);
			$this->db->or_like('vb_thue',$tonghop);
			$this->db->or_like('vb_bhxh',$tonghop);
			$this->db->or_like('vb_ubnd',$tonghop);
			$this->db->or_like('vb_kq',$tonghop);	
		}		
		$this->db->select('abtc.*');
		$this->db->from('tbl_hoso_abtc as abtc');
		$this->db->order_by('ngay_den','desc');
		return $this->db->get()->num_rows();
	}
	
}

/* End of file Mvanbandi.php */
/* Location: ./application/models/vanbandi/Mvanbandi.php */
