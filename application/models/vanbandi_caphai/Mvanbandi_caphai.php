<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvanbandi_caphai extends CI_Model {

	// lấy mã văn bản đi dựa vào số đi và mã đơn vị
	public function laySoDi($donvi,$sodi)
	{
		$this->db->where('FK_iMaDV',$donvi);
		$this->db->where('iSoVBDi',$sodi);
		$this->db->select('PK_iMaVBDi');
		return $this->db->get('tbl_vanbandi_caphai')->row_array();
	}
	// lấy thông tin chi tiết
	public function layThongTinChiTiet($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi_caphai as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		return $this->db->get()->row_array();
	}
	// lấy file cuối cùng
	public function layFileLast($ma)
	{
		$this->db->where('FK_iMaVBDi',$ma);
		$this->db->select('sDuongDan,sTenFile');
		$this->db->from('tbl_files_vbdi_caphai');
		$this->db->order_by('PK_iMaFile','desc');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
	// lấy số đi mới nhất // kiểm tra phải giấy mời hay k
	public function laySoDiMoi($donvi,$nam)
	{
		$this->db->where('FK_iMaDV',$donvi);
		$this->db->where('iNam',$nam);
		$this->db->select('iSoVBDi');
		$this->db->order_by('iSoVBDi','desc');
		return $this->db->get('tbl_vanbandi_caphai')->row_array();
	}
	// lấy danh sách văn bản chờ số $trangthai là 1:chờ số 2: cấp số
	public function layDSVB_ChoSo($trangthai,$donvi,$nam,$sodi=NULL,$loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$ngayvanban=NULL,$trichyeu=NULL,$nguoiky=NULL,$limit=NULL,$offset=NULL)
	{
		if($trangthai==1) // chờ số
		{
			$this->db->where('iSoVBDi',0);
		}
		else // đã cấp số
		{
			$this->db->where('iSoVBDi >',0);
		}
		$this->db->where('FK_iMaDV',$donvi);
		$this->db->where('iNam',$nam);
		if(!empty($sodi))
		{
			$this->db->where('iSoVBDi',$sodi);
		}
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->like('sKyHieu',$kyhieu);
		}
		if(!empty($ngayvanban))
		{
			$this->db->where('sNgayVBDi',$ngayvanban);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi_caphai as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm danh sách văn bản chờ số $trangthai là 1:chờ số 2: cấp số
	public function demDSVB_ChoSo($trangthai,$donvi,$nam,$sodi=NULL,$loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$ngayvanban=NULL,$trichyeu=NULL,$nguoiky=NULL)
	{
		if($trangthai==1) // chờ số
		{
			$this->db->where('iSoVBDi',0);
		}
		else // đã cấp số
		{
			$this->db->where('iSoVBDi >',0);
		}
		$this->db->where('FK_iMaDV',$donvi);
		$this->db->where('iNam',$nam);
		if(!empty($sodi))
		{
			$this->db->where('iSoVBDi',$sodi);
		}
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->like('sKyHieu',$kyhieu);
		}
		if(!empty($ngayvanban))
		{
			$this->db->where('sNgayVBDi',$ngayvanban);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		$this->db->select('PK_iMaVBDi');
		$this->db->from('tbl_vanbandi_caphai as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->num_rows();
	}

	// lấy tên mà chức vụ người ký đơn vị cấp hai
	public function layNguoiKy_CH($donvi,$macb=NULL)	
	{
		if(!empty($macb))
		{
			$this->db->where('PK_iMaCB',$macb);
		}
		$this->db->where('FK_iMaPhongHD',$donvi);
		$this->db->where_in('quyenhan_caphai',array(1,2));
		$this->db->select('PK_iMaCB,sHoTen,tendinhdanh');
		$this->db->order_by('quyenhan_caphai','asc');
		return $this->db->get('tbl_canbo')->result_array();
	}

}

/* End of file Mvanbandi_caphai.php */
/* Location: ./application/models/vanbandi_caphai/Mvanbandi_caphai.php */