<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mqldv_phongban extends CI_Model {

	// thống kê đầu việc theo phòng ban chủ trì
	public function thongkePhongBan($mangPB)
	{
		$this->db->where_in('main_department',$mangPB);
		$this->db->where('qlv_id_sub >',0);// không tính đầu việc cha
		$this->db->where('qlvDetails_active >',2);// bắt đầu tính khi chuyển xuống phòng
		$this->db->select('main_department,COUNT(qlvDetails_id) as tong');
		$this->db->group_by('main_department');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
	public function layDauViecThongKe($mangPB,$trangthai,$dunghan=NULL,$quahan=NULL)// trangthai 1:đang thực hiên 2: đã hoàn thành dunghan và quahan đều bằng 1
	{
		$this->db->where_in('main_department',$mangPB);
		$this->db->where('qlv_id_sub >',0);// không tính đầu việc cha
		if($trangthai==1)// đang thực hiên
		{
			$this->db->where('qlvDetails_active >',2);// bắt đầu tính khi chuyển xuống phòng
			$this->db->where('qlvDetails_active <',9);
			if(!empty($dunghan))
			{
				$this->db->where('han_thongke >=',date('Y-m-d'));
			}
			if(!empty($quahan))
			{
				$this->db->where('han_thongke <',date('Y-m-d'));
			}
		}
		if($trangthai==2)// đã hoàn thành
		{
			$this->db->where('qlvDetails_active',9);// đã giải quyết
			if(!empty($dunghan))
			{
				$this->db->where('han_thongke >=','DATE(date_traloi)');
			}
			if(!empty($quahan))
			{
				$this->db->where('han_thongke <','DATE(date_traloi)');
			}
		}
		$this->db->select('main_department,COUNT(qlvDetails_id) as tong');
		$this->db->group_by('main_department');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
	// lấy tổng số đầu việc
	public function tongDauViec()
	{
		$this->db->where('qlv_id_sub >',0);// không tính đầu việc cha
		return $this->db->get('tbl_qlv_details')->num_rows();
	}

}

/* End of file Mqldv_phongban.php */
/* Location: ./application/models/qldv/Mqldv_phongban.php */