<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mchuyennhan extends CI_Model {
    // lấy người gửi cho 
    public function layNguoiChuyen($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaVB',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',1);
        $this->db->select('Fk_iMaCB_Chuyen');
        return $this->db->get('tbl_dexuatrahan')->row_array();
    }
    public function capnhat_TrangThai($madexuat,$trangthai)
    {
        $this->db->where('PK_iMaDX',$madexuat);
        $this->db->set('iTrangThai',$trangthai);
        $this->db->update('tbl_dexuatrahan');
    }
    //  lấy đề xuât gia hạn của chuyên viên
    public function layDX_Han($taikhoan)
    {
        $query = "SELECT PK_iMaDX,FK_iMaVB,Fk_iMaCB_Chuyen,FK_iMaCB_Nhan,sThoiGian_Chuyen,sLyDo_Chuyen,sHanDeXuat,qlvDetails_desc,input_per,qlvDetails_date,qlvDetails_limit_time,han_thongke,qlvDetails_id FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where (Fk_iMaCB_Chuyen = $taikhoan) or (FK_iMaCB_Nhan = $taikhoan)  GROUP by FK_iMaVB) and iTrangThai = 1";
        return $this->db->query($query)->result_array();
    }
    // đếm đầu việc gia hạn của chuyên viên chờ duyệt
    public function demDX_Han($taikhoan)
    {
        $query = "SELECT PK_iMaDX FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where (Fk_iMaCB_Chuyen = $taikhoan) or (FK_iMaCB_Nhan = $taikhoan)  GROUP by FK_iMaVB) and iTrangThai = 1";
        return $this->db->query($query)->num_rows();
    }
    // đã duyệt đề xuất của chuyên viên
    public function layDaDX_Han($taikhoan)
    {
        $query = "SELECT PK_iMaDX,FK_iMaVB,Fk_iMaCB_Chuyen,FK_iMaCB_Nhan,sThoiGian_Chuyen,sLyDo_Chuyen,sHanDeXuat,qlvDetails_desc,input_per,qlvDetails_date,qlvDetails_limit_time,han_thongke,qlvDetails_id FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where (Fk_iMaCB_Chuyen = $taikhoan) or (FK_iMaCB_Nhan = $taikhoan) GROUP by FK_iMaVB) and iTrangThai in (2,3)";
        return $this->db->query($query)->result_array();
    }
    // đếm đầu việc đã được duyệt hạn
    public function demDaDX_Han($taikhoan)
    {
        $query = "SELECT PK_iMaDX FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where (Fk_iMaCB_Chuyen = $taikhoan) or (FK_iMaCB_Nhan = $taikhoan) GROUP by FK_iMaVB) and iTrangThai in (2,3)";
        return $this->db->query($query)->num_rows();
    }
    // lấy dữ liệu
    public function layDX_Han_TP($taikhoan)
    {
        $query = "SELECT PK_iMaDX,FK_iMaVB,Fk_iMaCB_Chuyen,FK_iMaCB_Nhan,sThoiGian_Chuyen,sLyDo_Chuyen,sHanDeXuat,qlvDetails_desc,input_per,qlvDetails_date,qlvDetails_limit_time,han_thongke,qlvDetails_id FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where FK_iMaCB_Nhan = $taikhoan GROUP by FK_iMaVB) and iTrangThai = 1";
        return $this->db->query($query)->result_array();
    }
    // đếm đầu việc chờ xử lý của lãnh đạo
    public function demDX_Han_TP($taikhoan)
    {
        $query = "SELECT PK_iMaDX FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where FK_iMaCB_Nhan = $taikhoan GROUP by FK_iMaVB) and iTrangThai = 1";
        return $this->db->query($query)->num_rows();
    }
    // lấy dữ liệu
    public function layDaDX_Han_TP($taikhoan)
    {
        $query = "SELECT PK_iMaDX,FK_iMaVB,Fk_iMaCB_Chuyen,FK_iMaCB_Nhan,sThoiGian_Chuyen,sLyDo_Chuyen,sHanDeXuat,qlvDetails_desc,input_per,qlvDetails_date,qlvDetails_limit_time,han_thongke,qlvDetails_id FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where (Fk_iMaCB_Chuyen = $taikhoan and iTrangThai = 1) or (FK_iMaCB_Nhan = $taikhoan and iTrangThai in (2,3)) GROUP by FK_iMaVB)";
        return $this->db->query($query)->result_array();
    }
    public function demDaDX_Han_TP($taikhoan)
    {
        $query = "SELECT PK_iMaDX FROM tbl_dexuatrahan as dx join tbl_qlv_details as vb on vb.qlvDetails_id=dx.FK_iMaVB where PK_iMaDX in (SELECT MAX(PK_iMaDX) from tbl_dexuatrahan where (Fk_iMaCB_Chuyen = $taikhoan and iTrangThai = 1) or (FK_iMaCB_Nhan = $taikhoan and iTrangThai in (2,3)) GROUP by FK_iMaVB)";
        return $this->db->query($query)->num_rows();
    }

    // ============================================== ĐỀ XUẤT HẠN
    // cập dữ diệu bảng phối hợp
    public function capnhatDLPH($mavanban,$maphong,$data)
    {
        $this->db->where('FK_iMaVB',$mavanban);
        $this->db->where('FK_iMaPhong',$maphong);
        $this->db->update('tbl_phoihop_qldv',$data);
    }

    // lấy mã chuyền nhận
    public function layMaChuyenNhan($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',2);
        $this->db->where('iTraLai',1);
        $this->db->select('PK_CN_QLDV');
        return $this->db->get('tbl_chuyennhan_qldv')->row_array();

    }
    // xóa chuyền nhận những người trực tiếp nhận được //====> có thể chưa dùng
    public function xoaChuyenNhan($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Gui',$taikhoan);
        $this->db->where('iTraLai',1);
        $this->db->delete('tbl_chuyennhan_qldv');
    }
    // xóa chuyền nhận 2
    public function xoaChuyenNhan_Hai($madauviec,$machuyennhan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('PK_CN_QLDV >',$machuyennhan);
        $this->db->where('iTraLai',1);
        $this->db->delete('tbl_chuyennhan_qldv');
    }
    // xóa chuyền nhận 2
    public function xoaChuyenNhanCC($madauviec,$machuyennhan,$maphong)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('PK_CN_QLDV >',$machuyennhan);
        $this->db->where('FK_iMaPB_CapHai',$maphong);
        $this->db->where('iTraLai',1);
        $this->db->delete('tbl_chuyennhan_qldv');
    }
    // lấy người phối hợp để giải quyết
    public function layNguoiPH_GiaiQuyet($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',1);
        $this->db->where('CT_PH >=',2);
        return $this->db->get('tbl_chuyennhan_qldv')->num_rows();
    }
    public function layNguoiCTPH_GiaiQuyet($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',1);
        $this->db->where('phoihop_chutri',2);
        return $this->db->get('tbl_chuyennhan_qldv')->num_rows();
    }
    // lấy thông tin phòng phối hợp hay chủ trì
    public function laythogntinphong($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->select('CT_PH,FK_iMaPB');
        return $this->db->get('tbl_chuyennhan_qldv')->row_array();
    }
    // cập nhật trạng thái hoàn thành
    public function capnhatHoanThanh($madauviec,$phongban){
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaPB',$phongban);
        $this->db->set('hoanthanh_phoihop',1);
        $this->db->update('tbl_chuyennhan_qldv');
    }
    // lấy người cuối
    public function layNguoiCuoiCung($madauviec,$taikhoan,$chutri_phoihop=NULL,$nguoi_chutri_phoihop=NULL)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('iTraLai',1);
        $this->db->where('iTrangThai',1);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        if(!empty($chutri_phoihop))
        {
            $this->db->where_in('CT_PH',$chutri_phoihop);
        }
        if(!empty($nguoi_chutri_phoihop))
        {
            $this->db->where_in('phoihop_chutri',$nguoi_chutri_phoihop);   
        }
        $this->db->select('FK_iMaCB_Nhan');
        $this->db->order_by('PK_CN_QLDV','desc');
        return $this->db->get('tbl_chuyennhan_qldv')->num_rows();
    }
    // lấy người gửi cho 
    public function layNguoiGui($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTraLai',1);
        $this->db->select('FK_iMaCB_Gui');
        return $this->db->get('tbl_chuyennhan_qldv')->row_array();
    }
    // lấy quá trình chuyển nhận
    public function layQuyTrinhXuLy($madauviec,$chutri_phoihop=NULL,$nguoi_chutri_phoihop=NULL)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        // $this->db->where('iTraLai',1);
        if(!empty($chutri_phoihop))
        {
            $this->db->where_in('CT_PH',$chutri_phoihop);
        }
        if(!empty($nguoi_chutri_phoihop))
        {
            $this->db->where_in('phoihop_chutri',$nguoi_chutri_phoihop);   
        }
        $this->db->select('FK_iMaCB_Gui,sNoiDungChuyen,FK_iMaCB_Nhan,sThoiGian,sHanXuLy');
        return $this->db->get('tbl_chuyennhan_qldv')->result_array();
    }
    public function layQuyTrinhXuLyPH($madauviec,$chutri_phoihop,$phong,$tralai,$an)
    {
        if(!empty($an))
        {
            $this->db->where('sNoiDungChuyen !=','');
        }
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where_in('iTraLai',$tralai);
        $this->db->where_in('CT_PH',$chutri_phoihop);
        $this->db->where_in('FK_iMaPB',$phong);
        $this->db->select('FK_iMaCB_Gui,sNoiDungChuyen,FK_iMaCB_Nhan,sThoiGian,sHanXuLy');
        return $this->db->get('tbl_chuyennhan_qldv')->result_array();
    }
    public function layQuyTrinhXuLy_Hai($madauviec,$chutri_phoihop=NULL,$phong=NULL,$phongcaphai=NULL,$nguoi_chutri_phoihop=NULL)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('sNoiDungChuyen !=','');
        if(!empty($chutri_phoihop))
        {
            $this->db->where_in('CT_PH',$chutri_phoihop);
        }
        if(!empty($phong))
        {
            $this->db->where_in('FK_iMaPB',$phong);
        }
        if(!empty($phongcaphai))
        {
            $this->db->where_in('FK_iMaPB_CapHai',$phongcaphai);
        }
        if(!empty($nguoi_chutri_phoihop))
        {
            $this->db->where_in('phoihop_chutri',$nguoi_chutri_phoihop);   
        }
        $this->db->select('FK_iMaCB_Gui,sNoiDungChuyen,FK_iMaCB_Nhan,sThoiGian,sHanXuLy');
        return $this->db->get('tbl_chuyennhan_qldv')->result_array();
    }
    // lấy phó phòng + chuyên viên
    public function layCanBo($phongban,$quyenhan=NULL,$chucvu=NULL,$phongchicuc=NULL)
    {
        $this->db->where('FK_iMaPhongHD',$phongban);
        if(!empty($quyenhan))
        {
            $this->db->where('iQuyenHan_DHNB',$quyenhan);
        }
        if(!empty($chucvu))
        {
            $this->db->where('FK_iMaCV',$chucvu);
        }
        if(!empty($phongchicuc))
        {
            $this->db->where('iQuyenDB',$phongchicuc);
        }
        $this->db->where('iTrangThai',0);
        $this->db->select('PK_iMaCB,sHoTen');
        return $this->db->get('tbl_canbo')->result_array();
    }
    //  lấy phó phòng và chuyên viên
    public function layPPvaCV($phongban)
    {
        $this->db->where('iQuyenDB',$phongban);
        $this->db->where_in('iQuyenHan_DHNB',array(7,8));
        $this->db->where('iTrangThai',0);
        $this->db->select('PK_iMaCB,sHoTen');
        return $this->db->get('tbl_canbo')->result_array();
    }


    // lấy đầu việc ông
    public function layDauViecOng($mangcon)
    {
        $this->db->where_in('tbl_qlv.qlv_id',$mangcon);
        $this->db->select('tbl_qlv.qlv_id,qlv_noidung,qlv_file,qlvFile_path');
        $this->db->from('tbl_qlv');
        $this->db->join('tbl_qlvfiles','tbl_qlvfiles.qlv_id=tbl_qlv.qlv_id','left');
        $this->db->group_by('tbl_qlv.qlv_id');
        $this->db->order_by('tbl_qlv.qlv_id','desc');
        return $this->db->get()->result_array();
    }
    // cập nhât trạng thái người chuyển
    public function capnhatTrangThaiNguoiChuyen($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->set('iTrangThai',2);
        $this->db->update('tbl_chuyennhan_qldv');
    }
    // lấy cán bộ
    public function layCB()
    {
        $this->db->where('iTrangThai',0);
        $this->db->select('PK_iMaCB,sHoTen');
        return $this->db->get('tbl_canbo')->result_array();
    }
    // lấy đầu việc của phòng
    public function layDauViecCuaPhong($phong,$chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('FK_iMaPB',$phong);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->result_array();
    }
    public function layDauViecCuaPhongPH($phong,$chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaPB',$phong);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->result_array();
    }
    public function demDauViecCuaPhong($phong,$chutri_phoihop,$phongchicuc=NULL)
    {
        $this->db->where('FK_iMaPB',$phong);
        $this->db->where('CT_PH',$chutri_phoihop);
        if(!empty($phongchicuc))
        {
            // $this->db->where('FK_iMaPB_CapHai',$phongchicuc);
        }
        $this->db->where('phoihop_chutri',$chutri_phoihop);
        $this->db->where('iTraLai',1);
        $this->db->select('qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->num_rows();
    }
    //===================================================== ĐẦU VIỆC ĐÃ XỬ LÝ GĐ PGĐ
    public function layDauViecDaXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        if($chutri_phoihop==1)
        {
            $this->db->where('thoigian_hoanthanh','0000-00-00');
        }
        else
        {
            $this->db->where('hoanthanh_phoihop',0);
        }
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_PP,sGoiY_CV,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id,FK_iMaPB_CT_CC,FK_iMaCB_CCP,FK_iMaPB_PH_CC,sGoiY_PB_CC,FK_iMaCB_PGD,sGoiY_PGD,FK_iMaCB_CVPH_PH,sGoiY_CVPH');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
    // đã xử lý phòng phối hợp
    public function layDauViecDaXuLyPH($phongphoihop,$taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaPhong',$phongphoihop);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        $this->db->where('hoanthanh_phoihop',0);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_PP,sGoiY_CV,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id,ph.FK_iMaPB_PH_CC,ph.sGoiY_PB_CC,ph.FK_iMaCB_PPPH_CT,ph.FK_iMaCB_PPPH_PH,ph.FK_iMaCB_CVPH_CT,ph.FK_iMaCB_CVPH_PH,ph.sGoiY_PPPH,ph.sGoiY_CVPH,FK_iMaCVPH_PPH,sGoiY_CVPH_PPH,ph.FK_iMaCCP,ph.sGoiY_CCP,FK_iMaCVPM,sGoiYCVPM,FK_iMaCVPH,sGoiYCVPH,FK_iMaCVPB,sGoiYCVPB');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        $this->db->join('tbl_phoihop_qldv as ph','ph.FK_iMaVB=tbl_chuyennhan_qldv.FK_iMaQLDV');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
    public function demDauViecDaXuLyPH($phongphoihop,$taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaPhong',$phongphoihop);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        $this->db->where('hoanthanh_phoihop',0);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_PP,sGoiY_CV,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id,ph.FK_iMaCB_PPPH_CT,ph.FK_iMaCB_PPPH_PH,ph.FK_iMaCB_CVPH_CT,ph.FK_iMaCB_CVPH_PH,ph.sGoiY_PPPH,ph.sGoiY_CVPH');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        $this->db->join('tbl_phoihop_qldv as ph','ph.FK_iMaVB=tbl_chuyennhan_qldv.FK_iMaQLDV');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        return $this->db->get()->num_rows();
    }
    public function demDauViecDaXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        if($chutri_phoihop==1)
        {
            $this->db->where('thoigian_hoanthanh','0000-00-00');
        }
        else
        {
            $this->db->where('hoanthanh_phoihop',0);
        }
        
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        return $this->db->get()->num_rows();
    }
    // ==================================================== ĐẦU VIỆC CHỜ XỬ LÝ
    // lấy đầu việc chờ xử lý
    public function layDauViecChoXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('main_department >',0);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_PP,sGoiY_CV,sTieuDe,sHanXuLy,main_department, department_id,FK_iMaCB_LanhDao,qlvDetails_id,FK_iMaPB_CT_CC,FK_iMaPB_PH_CC,sGoiY_PB_CC,FK_iMaCB_CVPH_PH,sGoiY_CVPH');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
    // đầu việc chờ xử lý phó phòng
    public function layDauViecChoXuLyPH($phongphoihop,$taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaPhong',$phongphoihop);
        $this->db->where('main_department >',0);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_PP,sGoiY_CV,sTieuDe,sHanXuLy,main_department, department_id,FK_iMaCB_LanhDao,qlvDetails_id,ph.FK_iMaCB_PPPH_CT,ph.FK_iMaCB_PPPH_PH,ph.FK_iMaCB_CVPH_CT,ph.FK_iMaCB_CVPH_PH,ph.sGoiY_PPPH,ph.sGoiY_CVPH,ph.FK_iMaPB_PH_CC,ph.sGoiY_PB_CC');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        $this->db->join('tbl_phoihop_qldv as ph','ph.FK_iMaVB=tbl_chuyennhan_qldv.FK_iMaQLDV');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
    // đếm đầu việc chờ xứ lý
    public function demDauViecChoXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL)
    {
        $this->db->where('main_department >',0);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        return $this->db->get()->num_rows();
    }
    // ============================================================ GIÁM ĐỐC
    // lấy văn bản bị trả lại
    public function layDauViecTraLai($taikhoan)
    {
        $this->db->where('nguoibitralai',$taikhoan);
        $this->db->select('qlvDetails_id,qlv_id,qlvDetails_date,input_per,noidungtralai,qlvDetails_desc,sGoiY_PB,qlvDetails_limit_time,FK_iMaCB_LanhDao,main_department,department_id');
        $this->db->order_by('qlvDetails_id','desc');
        return $this->db->get('tbl_qlv_details')->result_array();
    }
    // đếm đầu việc trả lại
    public function demDauViecTraLai($taikhoan)
    {
        $this->db->where('nguoibitralai',$taikhoan);
        $this->db->select('qlvDetails_id');
        $this->db->order_by('qlvDetails_id','desc');
        return $this->db->get('tbl_qlv_details')->num_rows();
    }
    // lấy văn bản chờ duyệt của lãnh đạo phòng
    public function layDauViecChoDuyet($taikhoan)
    {
        $this->db->where('truongphongduyet',$taikhoan);
        $this->db->select('qlvDetails_id,qlv_id,qlvDetails_date,input_per,qlvDetails_desc,sGoiY_PB,qlvDetails_limit_time,FK_iMaCB_LanhDao,main_department,department_id');
        $this->db->order_by('qlvDetails_id','desc');
        return $this->db->get('tbl_qlv_details')->result_array();
    }
    // đếm văn bản chờ duyệt của lãnh đạo phòng
    public function demDauViecChoDuyet($taikhoan)
    {
        $this->db->where('truongphongduyet',$taikhoan);
        $this->db->select('qlvDetails_id');
        $this->db->order_by('qlvDetails_id','desc');
        return $this->db->get('tbl_qlv_details')->num_rows();
    }

}

/* End of file Mchuyennhan.php */
/* Location: ./application/models/qldv/Mchuyennhan.php */