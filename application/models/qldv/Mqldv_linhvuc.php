<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mqldv_linhvuc extends CI_Model {

	// lấy lĩnh vực cha
	public function layLinhVucCha()
	{
		$this->db->where('linhVuc_active',1);
		$this->db->where('id_parent',0);
		$this->db->select('linhVuc_id,	linhVuc_name');
		return $this->db->get('tbl_linhvuc_qldv')->result_array();
	}
	// lấy lĩnh vực con theo lĩnh vực cha
	public function layLinhVucCon($linhvuccha)
	{
		$this->db->where('id_parent',$linhvuccha);
		$this->db->where('linhVuc_active',1);
		$this->db->select('linhVuc_id,	linhVuc_name');
		return $this->db->get('tbl_linhvuc_qldv')->result_array();
	}
	// lấy tất lĩnh vực con
	public function LinhVucCon()
	{
		$this->db->where('linhVuc_active',1);
		$this->db->select('linhVuc_id');
		return $this->db->get('tbl_linhvuc_qldv')->result_array();
	}
	// lấy đầu việc theo lĩnh vực
	public function layDauViecLinhVuc($mangLV)
	{
		$this->db->where_in('linhvuc_sub',$mangLV);
		$this->db->where('qlv_id_sub >',0);// không tính đầu việc cha
		$this->db->select('linhvuc_sub,COUNT(qlvDetails_id) as tong');
		$this->db->group_by('linhvuc_sub');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
	public function layDauViecThongKe($mangLV,$trangthai,$dunghan=NULL,$quahan=NULL)// trangthai 1:đang thực hiên 2: đã hoàn thành dunghan và quahan đều bằng 1
	{
		$this->db->where_in('linhvuc_sub',$mangLV);
		$this->db->where('qlv_id_sub >',0);// không tính đầu việc cha
		if($trangthai==1)// đang thực hiên
		{
			$this->db->where('qlvDetails_active <',9);
			if(!empty($dunghan))
			{
				$this->db->where('han_thongke >=',date('Y-m-d'));
			}
			if(!empty($quahan))
			{
				$this->db->where('han_thongke <',date('Y-m-d'));
			}
		}
		if($trangthai==2)// đã hoàn thành
		{
			$this->db->where('qlvDetails_active',9);// đã giải quyết
			if(!empty($dunghan))
			{
				$this->db->where('han_thongke >=','DATE(date_traloi)');
			}
			if(!empty($quahan))
			{
				$this->db->where('han_thongke <','DATE(date_traloi)');
			}
		}
		$this->db->select('linhvuc_sub,COUNT(qlvDetails_id) as tong');
		$this->db->group_by('linhvuc_sub');
		return $this->db->get('tbl_qlv_details')->result_array();
	}

}

/* End of file Mqldv_linhvuc.php */
/* Location: ./application/models/qldv/Mqldv_linhvuc.php */