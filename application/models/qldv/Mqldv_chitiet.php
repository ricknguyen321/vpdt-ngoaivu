<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mqldv_chitiet extends CI_Model {

	// lấy đầu việc chi tiết
	public function layDauViecChiTiet($mavanban,$trangthai,$dunghan=NULL,$quahan=NULL,$new_date=NULL) // trang thai 1:đang thực hiên 2: đã hoàn thành 3:sắp đến hạn
	{
		if(!empty($mavanban))
		{
			$this->db->where('qlv_id',$mavanban);
		}
		if($trangthai==1)
		{
			$this->db->where('qlvDetails_active <',9);// đang giải quyết
			if(!empty($dunghan))
			{
				$this->db->where('(han_thongke > "'.date('Y-m-d').'" or han_thongke <= "1970-01-01")',NULL);
			}
			if(!empty($quahan))
			{
				$this->db->where('(han_thongke < "'.date('Y-m-d').'" and han_thongke > "1970-01-01")',NULL);
			}
		}
		if($trangthai==2)
		{
			$this->db->where('qlvDetails_active',9);// đã giải quyết
			if(!empty($dunghan))
			{
				// $this->db->where('han_thongke >=','DATE(date_traloi)');
				$this->db->where('(han_thongke >= thoigian_hoanthanh or han_thongke <= "1970-01-01")',NULL);
			}
			if(!empty($quahan))
			{
				// $this->db->where('han_thongke <','DATE(date_traloi)');
				$this->db->where('(han_thongke < thoigian_hoanthanh and han_thongke > "1970-01-01")',NULL);
			}
		}
		if($trangthai==3)
		{
			$this->db->where('qlvDetails_active <',9);// sắp đến hạn
			if(!empty($new_date))
			{
				$this->db->where('han_thongke <=',$new_date);
				$this->db->where('han_thongke >=',date('Y-m-d'));	
			}
		}
		$this->db->where('qlv_id_sub >',0);
		$this->db->select('qlv_id,qlvDetails_id,qlv_id_sub,qlvDetails_desc,han_thongke,main_department,department_id');
		return $this->db->get('tbl_qlv_details')->result_array();
	}

	public function layDauViecChiTiet1($mavanban,$trangthai,$dunghan=NULL,$quahan=NULL,$new_date=NULL) // trang thai 1:đang thực hiên 2: đã hoàn thành 3:sắp đến hạn
	{
		if(!empty($mavanban))
		{
			$this->db->where('qlv_id',$mavanban);
		}
		if($trangthai==1)
		{
			$this->db->where('qlvDetails_active <',9);// đang giải quyết
			if(!empty($dunghan))
			{
				$this->db->where('(han_thongke > "'.date('Y-m-d').'" or han_thongke <= "1970-01-01")',NULL);
			}
			if(!empty($quahan))
			{
				$this->db->where('(han_thongke < "'.date('Y-m-d').'" and han_thongke > "1970-01-01")',NULL);
			}
		}
		if($trangthai==2)
		{
			$this->db->where('qlvDetails_active',9);// đã giải quyết
			if(!empty($dunghan))
			{
				// $this->db->where('han_thongke >=','DATE(date_traloi)');
				$this->db->where('(han_thongke >= thoigian_hoanthanh or han_thongke <= "1970-01-01")',NULL);
			}
			if(!empty($quahan))
			{
				// $this->db->where('han_thongke <','DATE(date_traloi)');
				$this->db->where('(han_thongke < thoigian_hoanthanh and han_thongke > "1970-01-01")',NULL);
			}
		}
		if($trangthai==3)
		{
			$this->db->where('qlvDetails_active <',9);// sắp đến hạn
			if(!empty($new_date))
			{
				$this->db->where('han_thongke <=',$new_date);
				$this->db->where('han_thongke >=',date('Y-m-d'));	
			}
		}
		$this->db->where('qlv_id_sub >',0);
		$this->db->select('qlv_id,qlvDetails_id,qlv_id_sub,qlvDetails_desc,han_thongke,main_department,department_id');
		return $this->db->get('tbl_lanhdaogiao_details')->result_array();
	}

	// lấy đầu việc cha
	public function layDauViecCha($mavanban)
	{
		$this->db->where_in('qlvDetails_id',$mavanban);
		$this->db->select('qlv_id,qlvDetails_id,qlv_id_sub,qlvDetails_desc');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
	// lấy đầu việc ông 
	public function layDauViecOng($mavanban)
	{
		$this->db->where_in('qlv_id',$mavanban);
		$this->db->select('qlv_id,qlv_code,qlv_date,qlv_noidung,thongbao_ketluan');
		return $this->db->get('tbl_qlv')->result_array();
	}

}

/* End of file Mqldv_chitiet.php */
/* Location: ./application/models/qldv/Mqldv_chitiet.php */