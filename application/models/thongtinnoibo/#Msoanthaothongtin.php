<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msoanthaothongtin extends CI_Model {

	/**
	*  ============================ lấy danh sách văn bản đến
	*/
	public function layDSVBDen($mavanbanden,$soden=NULL,$kyhieu=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where_not_in('PK_iMaVBDen',$mavanbanden);
		$this->db->where('iSoDen >',0);
		$this->db->where('iGiayMoi',2);
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		$this->db->select('PK_iMaVBDen,sTenLVB,sKyHieu,iSoDen,sMoTa,sNgayKy,sTenNguoiKy');
		$this->db->order_by('iSoDen','desc');
		$this->db->order_by('PK_iMaVBDen','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get('tbl_vanbanden')->result_array();
	}

	public function demDSVBDen($mavanbanden,$soden=NULL,$kyhieu=NULL)
	{
		$this->db->where_not_in('PK_iMaVBDen',$mavanbanden);
		$this->db->where('iSoDen >',0);
		$this->db->where('iGiayMoi',2);
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		return $this->db->get('tbl_vanbanden')->num_rows();
	}
	// lấy thông tin chi tiết của văn bản đến
	public function layChiTietVBDen($ma)
	{
		$this->db->where('iGiayMoi',2);
		$this->db->where('PK_iMaVBDen',$ma);
		$this->db->select('sTenNguoiKy as sHoTen,sNgayKy as sNgayVBDi,sTenLV,sTenLVB,sKyHieu,sMoTa,FK_iMaDK');
		return $this->db->get('tbl_vanbanden')->result_array();
	}
	/**
	* ============================== lấy danh sách văn bản đi
	*/
	// lấy mã văn bản đến, văn bản đi(1 là đên: NULL là đi)
	public function layMaVBDenDi($trangthai=NULL)
	{
		if(!empty($trangthai))
		{
			$this->db->select('FK_iMaVBDen');
			$this->db->group_by('FK_iMaVBDen');
		}
		else{
			$this->db->select('FK_iMaVBDi');
			$this->db->group_by('FK_iMaVBDi');
		}
		return $this->db->get('tbl_vanban')->result_array();
	}
	public function layDSVBDi($mavanbandi,$loaivanban=NULL,$kyhieu=NULL,$nguoiky=NULL,$ngaythang=NULL,$trichyeu=NULL,$noinhan=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where_not_in('PK_iMaVBDi',$mavanbandi);
		$this->db->where('FK_iMaLVB !=',10);
		$this->db->where('iSoVBDi >',0);
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->where('sMoTa',$trichyeu);
		}
		if(!empty($noinhan))
		{
			$this->db->where('sNoiNhan',$noinhan);
		}
		$this->db->select('sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	public function demDSVBDi($mavanbandi,$loaivanban=NULL,$kyhieu=NULL,$nguoiky=NULL,$ngaythang=NULL,$trichyeu=NULL,$noinhan=NULL)
	{
		$this->db->where_not_in('PK_iMaVBDi',$mavanbandi);
		$this->db->where('FK_iMaLVB !=',10);
		$this->db->where('iSoVBDi >',0);
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->where('sMoTa',$trichyeu);
		}
		if(!empty($noinhan))
		{
			$this->db->where('sNoiNhan',$noinhan);
		}
		$this->db->select('sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->num_rows();
	}
	/**
	* ===========================================================================================
	*/
	
	// lấy các phong ban
	public function layPhongBan($loaiphanmen)
	{
		if($loaiphanmen==2)
		{
			$this->db->where('iLoaiPhanMem',$loaiphanmen);
		}
		else{
			$this->db->where('iLoaiPhanMem !=',2);
		}
		$this->db->where('iTrangThai',0);
		$this->db->order_by('PK_iMaPB','desc');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// lấy người nhận theo phong ban hoặc quận huyện
	public function layNguoiNhanChuaChon($maphongban,$mangnguoinhan)
	{
		if(!empty($mangnguoinhan))
		{
			$this->db->where_not_in('PK_iMaCB',$mangnguoinhan);
		}
		$this->db->where_in('FK_iMaPhongHD',$maphongban);
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy người nhận theo phong ban hoặc quận huyện
	public function layNguoiNhan($maphongban)
	{
		$this->db->where_in('FK_iMaPhongHD',$maphongban);
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy văn bản mới nhận của từng người theo session
	public function layVanBanMoiNhan($taikhoan,$trangthaixem)
	{
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		if($trangthaixem==1)
		{
			$this->db->where('sThoiGianDoc !=',NULL);
		}
		else{
			$this->db->where('sThoiGianDoc',NULL);
		}
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->order_by('sNgayNhap','desc');
		return $this->db->get()->result_array();
	}


	// ======================================== PHÂN TRANG ====================================
	public function layDuLieu($taikhoan,$trangthaixem,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		if($trangthaixem==1)
		{
			$this->db->where('sThoiGianDoc !=',NULL);
		}
		else{
			$this->db->where('sThoiGianDoc',NULL);
		}
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->order_by('sNgayNhap','desc');
		$this->db->limit($limit,$offset);
		return $this->db->get()->result_array();
	}
	// lấy thông tin đã gửi 
	public function demDuLieu($taikhoan,$trangthaixem)
	{
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		if($trangthaixem==1)
		{
			$this->db->where('sThoiGianDoc !=',NULL);
		}
		else{
			$this->db->where('sThoiGianDoc',NULL);
		}
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		return $this->db->get()->num_rows();
	}
	// ========================================================================================
		/**
		* =============================== Phân trang thông tin nội bộ(ds đã xem)
		*/
	public function layDSVB($taikhoan,$trangthaixem)
	{
		# code...
	}
		
	// lấy thông tin chi tiết của một văn bản
	public function layThongTinVanBan($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->select('vb.*,sHoTen');
		$this->db->from('tbl_vanban as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		return $this->db->get()->row_array();
	}
	// cập nhật thời gian xem
	public function capnhatThoiGianXem($mavanban,$macanbo,$time)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('FK_iMaNguoiNhan',$macanbo);
		$this->db->set('sThoiGianDoc',$time);
		$this->db->update('tlb_nguoinhanvanban');
	}
	// lấy phòng ban và người được nhận văn bản
	public function layPB($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->from('tbl_vanban as vb');
		$this->db->select('pb.PK_iMaPB,pb.sTenPB,nn.sGhiChu');
		$this->db->join('tlb_nguoinhanvanban as nn','nn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=nn.FK_iMaNguoiNhan');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cb.FK_iMaPhongHD');
		$this->db->group_by('pb.sTenPB');
		return $this->db->get()->result_array();
	}
	public function layCB($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->from('tbl_vanban as vb');
		$this->db->select('PK_iMaCB,sHoTen,cb.FK_iMaPhongHD,sThoiGianDoc,nn.sGhiChu');
		$this->db->join('tlb_nguoinhanvanban as nn','nn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=nn.FK_iMaNguoiNhan');
		return $this->db->get()->result_array();
	}
	// lấy cán bộ soạn báo cáo giấy mời
    public function layCBBC($mavanban)
    {
        $this->db->where('PK_iMaNoiDung',$mavanban);
        $this->db->from('tbl_soanbaocao');
        $this->db->select('tbl_soanbaocao.*');
        return $this->db->get()->result_array();
    }
	// lấy danh sách góp ý cho văn bản
	public function layGopY($mavanban)
	{
		$this->db->where('gy.FK_iMaVB',$mavanban);
		$this->db->select('sHoTen,pb.sTenPB,gy.sNoiDung,gy.sThoiGian');
		$this->db->from('tbl_gopy as gy');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=gy.FK_iMaCB');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cb.FK_iMaPhongHD');
		return $this->db->get()->result_array();
	}
	// lấy cán bộ chưa được chọn
	public function layCBChuaChon($macanbo=NULL)
	{
		if(!empty($macanbo))
		{
			$this->db->where_not_in('PK_iMaCB',$macanbo);
		}
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy phong ban của những ng chưa chọn
	public function layPBChuaChon()
	{
		$this->db->where('iTrangThai',0);
		$this->db->order_by('PK_iMaPB','asc');
		return $this->db->get('tbl_phongban')->result_array();
	}
}

/* End of file Msoanthaothongtin.php */
/* Location: ./application/models/thongtinnoibo/Msoanthaothongtin.php */