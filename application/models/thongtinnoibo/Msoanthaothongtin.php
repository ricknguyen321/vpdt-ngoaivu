<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msoanthaothongtin extends CI_Model {
	// lấy phòng ban
	public function layPB_STC(){
		$mang = array(1,3);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iLoaiPhanMem',$mang);
		$this->db->select('PK_iMaPB,sTenPB');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// lấy cán bộ phòng
	public function layCB_Phong($phongban){
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaCB,sHoTen');
		$this->db->order_by('iQuyenHan_DHNB','asc');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy văn bản đã xem
	public function layTT_DaXem($taikhoan,$phongban=NULL,$noidung=NULL,$canbo=NULL,$tungay=NULL,$denngay=NULL,$limit=NULL,$offset=NULL){
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		$this->db->where('sThoiGianDoc !=',NULL);
		if(!empty($phongban)){
			$this->db->where('FK_iMaPB',$phongban);
		}
		if(!empty($noidung)){
			$this->db->like('sMoTa',$noidung);
		}
		if(!empty($canbo)){
			$this->db->where('FK_iMaCB_Nhap',$canbo);
		}
		if(!empty($tungay) && $tungay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) >=',date_insert($tungay));
		}
		if(!empty($denngay) && $denngay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) <=',date_insert($denngay));
		}
		$this->db->where('nn.iDel', NULL);
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('vb.*,sNgayNhap,sTenPB,sMoTa,PK_iMaVB, nn.iDel, nn.PK_iMaNguoiNhanVB');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->order_by('sNgayNhap','desc');
		if(!empty($limit)){
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	public function TT_DaXem($taikhoan,$phongban=NULL,$noidung=NULL,$canbo=NULL,$tungay=NULL,$denngay=NULL){
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		$this->db->where('sThoiGianDoc !=',NULL);
		if(!empty($phongban)){
			$this->db->where('FK_iMaPB',$phongban);
		}
		if(!empty($noidung)){
			$this->db->like('sMoTa',$noidung);
		}
		if(!empty($canbo)){
			$this->db->where('FK_iMaCB_Nhap',$canbo);
		}
		if(!empty($tungay) && $tungay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) >=',date_insert($tungay));
		}
		if(!empty($denngay) && $denngay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) <=',date_insert($denngay));
		}
		$this->db->where('nn.iDel', NULL);
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('sNgayNhap,sTenPB,sMoTa,PK_iMaVB, nn.iDel');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		return $this->db->get()->num_rows();
	}
	// lấy thông tin đã gửi
	public function layTT_DaGui($taikhoan,$noidung=NULL,$tungay=NULL,$denngay=NULL,$limit=NULL,$offset=NULL){
		$this->db->where('FK_iMaCB_Nhap',$taikhoan);
		if(!empty($noidung)){
			$this->db->like('sMoTa',$noidung);
		}
		if(!empty($tungay) && $tungay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) >=',date_insert($tungay));
		}
		if(!empty($denngay) && $denngay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) <=',date_insert($denngay));
		}
		$this->db->where('iDel', NULL);
		$this->db->order_by('sNgayNhap','desc');
		if(!empty($limit)){
			$this->db->limit($limit,$offset);
		}
		return $this->db->get('tbl_vanban')->result_array();
	}
	public function demTT_DaGui($taikhoan,$noidung=NULL,$tungay=NULL,$denngay=NULL){
		$this->db->where('FK_iMaCB_Nhap',$taikhoan);
		if(!empty($noidung)){
			$this->db->like('sMoTa',$noidung);
		}
		if(!empty($tungay) && $tungay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) >=',date_insert($tungay));
		}
		if(!empty($denngay) && $denngay !='dd/mm/yyyy'){
			$this->db->where('DATE(sNgayNhap) <=',date_insert($denngay));
		}
		$this->db->where('iDel', NULL);
		return $this->db->get('tbl_vanban')->num_rows();
	}
	// lấy thông tin người nhận
	public function layNguoiNhanVB($mavanban)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->select('FK_iMaNguoiNhan');
		return $this->db->get('tlb_nguoinhanvanban')->result_array();
	}
	/**
	*  ============================ lấy danh sách văn bản đến
	*/
	public function layDSVBDen($mavanbanden,$soden=NULL,$kyhieu=NULL,$limit=NULL,$offset=NULL)
	{
		//$this->db->where_not_in('PK_iMaVBDen',$mavanbanden);
		$this->db->where('iSoDen >',0);
		$this->db->where('iGiayMoi',2);
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		$this->db->select('PK_iMaVBDen,sTenLVB,sKyHieu,iSoDen,sMoTa,sNgayKy,sTenNguoiKy,sTenDV');
		$this->db->order_by('iSoDen','desc');
		$this->db->order_by('PK_iMaVBDen','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get('tbl_vanbanden')->result_array();
	}

	public function demDSVBDen($mavanbanden,$soden=NULL,$kyhieu=NULL)
	{
		$this->db->where_not_in('PK_iMaVBDen',$mavanbanden);
		$this->db->where('iSoDen >',0);
		$this->db->where('iGiayMoi',2);
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		return $this->db->get('tbl_vanbanden')->num_rows();
	}
	// lấy thông tin chi tiết của văn bản đến
	public function layChiTietVBDen($ma)
	{
		$this->db->where('iGiayMoi',2);
		$this->db->where('PK_iMaVBDen',$ma);
		$this->db->select('sTenNguoiKy as sHoTen,sNgayKy as sNgayVBDi,sTenLV,sTenLVB,sKyHieu,sMoTa,FK_iMaDK');
		return $this->db->get('tbl_vanbanden')->result_array();
	}
	/**
	* ============================== lấy danh sách văn bản đi
	*/
	// lấy mã văn bản đến, văn bản đi(1 là đên: NULL là đi)
	public function layMaVBDenDi($trangthai=NULL)
	{
		if(!empty($trangthai))
		{
			$this->db->select('FK_iMaVBDen');
			$this->db->group_by('FK_iMaVBDen');
		}
		else{
			$this->db->select('FK_iMaVBDi');
			$this->db->group_by('FK_iMaVBDi');
		}
		return $this->db->get('tbl_vanban')->result_array();
	}
	public function layDSVBDi($mavanbandi,$loaivanban=NULL,$kyhieu=NULL,$nguoiky=NULL,$ngaythang=NULL,$trichyeu=NULL,$noinhan=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where_not_in('PK_iMaVBDi',$mavanbandi);
		$this->db->where('FK_iMaLVB !=',10);
		$this->db->where('iSoVBDi >',0);
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->where('sMoTa',$trichyeu);
		}
		if(!empty($noinhan))
		{
			$this->db->where('sNoiNhan',$noinhan);
		}
		$this->db->select('sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('sNgayVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	public function demDSVBDi($mavanbandi,$loaivanban=NULL,$kyhieu=NULL,$nguoiky=NULL,$ngaythang=NULL,$trichyeu=NULL,$noinhan=NULL)
	{
		$this->db->where_not_in('PK_iMaVBDi',$mavanbandi);
		$this->db->where('FK_iMaLVB !=',10);
		$this->db->where('iSoVBDi >',0);
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->where('sMoTa',$trichyeu);
		}
		if(!empty($noinhan))
		{
			$this->db->where('sNoiNhan',$noinhan);
		}
		$this->db->select('sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->num_rows();
	}
	/**
	* ===========================================================================================
	*/
	
	// lấy các phong ban
	public function layPhongBan($loaiphanmen)
	{
		if($loaiphanmen==2)
		{
			$this->db->where('iLoaiPhanMem',$loaiphanmen);
		}
		else{
			$this->db->where('iLoaiPhanMem !=',2);
		}
		$this->db->where('iTrangThai',0);
		$this->db->order_by('PK_iMaPB','desc');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// lấy người nhận theo phong ban hoặc quận huyện
	public function layNguoiNhanChuaChon($maphongban,$mangnguoinhan)
	{
		if(!empty($mangnguoinhan))
		{
			$this->db->where_not_in('PK_iMaCB',$mangnguoinhan);
		}
		$this->db->where_in('FK_iMaPhongHD',$maphongban);
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy người nhận theo phong ban hoặc quận huyện
	public function layNguoiNhan($maphongban)
	{
		$this->db->where_in('FK_iMaPhongHD',$maphongban);
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy người nhận của cả phòng
	public function layNguoiNhanPhong($maphongban)
	{
		$this->db->where('FK_iMaPhongHD',$maphongban);
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// cập nhật dự liệu
	public function capnhatNguoiNhan($nguoinhan,$mavanban,$data)
	{
		$this->db->where('FK_iMaNguoiNhan',$nguoinhan);
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->set('sThoiGianDoc',NULL);
		$this->db->set('sThoiGianGui',date('Y-m-d H:i:s'));
		$this->db->update('tlb_nguoinhanvanban');
	}
	// lấy văn bản mới nhận của từng người theo session
	public function layVanBanMoiNhan($taikhoan,$trangthaixem)
	{
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		if($trangthaixem==1)
		{
			$this->db->where('sThoiGianDoc !=',NULL);
		}
		else{
			$this->db->where('sThoiGianDoc',NULL);
		}
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('sNgayNhap,sTenPB,sMoTa,PK_iMaVB,FK_iMaCB_Nhap');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->order_by('sNgayNhap','desc');
		return $this->db->get()->result_array();
	}
	public function layVanBanMoiNhan2($taikhoan,$trangthaixem)
	{
		$query = $this->db->query('SELECT sNgayNhap, sTenPB, sMoTa, PK_iMaVB, FK_iMaCB_Nhap, sNoiDungChiDao FROM tlb_nguoinhanvanban nn JOIN tbl_vanban vb ON vb.PK_iMaVB = nn.FK_iMaVB left JOIN tbl_gopy gy ON nn.FK_iMaVB = gy.FK_iMaVB WHERE (FK_iMaNguoiNhan = '.$taikhoan.') && (nn.sThoiGianDoc IS NULL || nn.sThoiGianDoc < (SELECT sThoiGian FROM tbl_gopy WHERE tbl_gopy.FK_iMaVB = nn.FK_iMaVB && tbl_gopy.FK_iMaCB != '.$taikhoan.' ORDER BY sThoiGian DESC LIMIT 1)) || vb.FK_iMaCB_Nhap = '.$taikhoan.' && (SELECT count(sThoiGian) FROM tbl_gopy WHERE tbl_gopy.FK_iMaVB = nn.FK_iMaVB && tbl_gopy.FK_iMaCB != '.$taikhoan.') > 0 && (select count(1) from tlb_nguoinhanvanban where tlb_nguoinhanvanban.FK_iMaNguoiNhan = '.$taikhoan.' && tlb_nguoinhanvanban.FK_iMaVB = nn.FK_iMaVB) = 0 GROUP BY sNgayNhap , sTenPB , sMoTa , PK_iMaVB ORDER BY sNgayNhap DESC;');
	        return $query->result_array();
	}
	public function demVanBanMoiNhan2($taikhoan,$trangthaixem)
	{
		$query = $this->db->query('SELECT sNgayNhap, sTenPB, sMoTa, PK_iMaVB, FK_iMaCB_Nhap FROM tlb_nguoinhanvanban nn JOIN tbl_vanban vb ON vb.PK_iMaVB = nn.FK_iMaVB left JOIN tbl_gopy gy ON nn.FK_iMaVB = gy.FK_iMaVB WHERE (FK_iMaNguoiNhan = '.$taikhoan.') && (nn.sThoiGianDoc IS NULL || nn.sThoiGianDoc < (SELECT sThoiGian FROM tbl_gopy WHERE tbl_gopy.FK_iMaVB = nn.FK_iMaVB && tbl_gopy.FK_iMaCB != '.$taikhoan.' ORDER BY sThoiGian DESC LIMIT 1)) || vb.FK_iMaCB_Nhap = '.$taikhoan.' && (SELECT count(sThoiGian) FROM tbl_gopy WHERE tbl_gopy.FK_iMaVB = nn.FK_iMaVB && tbl_gopy.FK_iMaCB != '.$taikhoan.') > 0 && (select count(1) from tlb_nguoinhanvanban where tlb_nguoinhanvanban.FK_iMaNguoiNhan = '.$taikhoan.' && tlb_nguoinhanvanban.FK_iMaVB = nn.FK_iMaVB) = 0 GROUP BY sNgayNhap , sTenPB , sMoTa , PK_iMaVB ORDER BY sNgayNhap DESC;');
	        return $query->num_rows();
	}
	public function demVanBanMoiNhan($taikhoan,$trangthaixem)
	{
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		if($trangthaixem==1)
		{
			$this->db->where('sThoiGianDoc !=',NULL);
		}
		else{
			$this->db->where('sThoiGianDoc',NULL);
		}
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('PK_iMaVB');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		return $this->db->get()->num_rows();
	}
	// đếm tin đã gửi
	public function demTinDaGui($macanbo)
	{
		$this->db->where('FK_iMaCB_Nhap',$macanbo);
		$this->db->where('iDel', NULL);
		return $this->db->get('tbl_vanban')->num_rows();
	}


	// ======================================== PHÂN TRANG ====================================
	public function layDuLieu($taikhoan,$trangthaixem,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		if($trangthaixem==1)
		{
			$this->db->where('sThoiGianDoc !=',NULL);
		}
		else{
			$this->db->where('sThoiGianDoc',NULL);
		}
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->order_by('sNgayNhap','desc');
		$this->db->limit($limit,$offset);
		return $this->db->get()->result_array();
	}
	// lấy thông tin đã gửi 
	public function demDuLieu($taikhoan,$trangthaixem)
	{
		$this->db->where('FK_iMaNguoiNhan',$taikhoan);
		if($trangthaixem==1)
		{
			$this->db->where('sThoiGianDoc !=',NULL);
		}
		else{
			$this->db->where('sThoiGianDoc',NULL);
		}
		$this->db->where('nn.iDel', NULL);
		$this->db->from('tlb_nguoinhanvanban as nn');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=nn.FK_iMaVB');
		$this->db->select('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		$this->db->group_by('sNgayNhap,sTenPB,sMoTa,PK_iMaVB');
		return $this->db->get()->num_rows();
	}
	// ========================================================================================
		/**
		* =============================== Phân trang thông tin nội bộ(ds đã xem)
		*/
	public function layDSVB($taikhoan,$trangthaixem)
	{
		# code...
	}
		
	// lấy thông tin chi tiết của một văn bản
	public function layThongTinVanBan($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->select('vb.*,sHoTen');
		$this->db->from('tbl_vanban as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		return $this->db->get()->row_array();
	}
	// cập nhật thời gian xem
	public function capnhatThoiGianXem($mavanban,$macanbo,$time)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('FK_iMaNguoiNhan',$macanbo);
		//$this->db->where('sThoiGianDoc',NULL);
		$this->db->set('sThoiGianDoc',$time);
		$this->db->update('tlb_nguoinhanvanban');
	}
	// lấy phòng ban và người được nhận văn bản
	public function layPB($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->from('tbl_vanban as vb');
		$this->db->select('pb.PK_iMaPB,pb.sTenPB,nn.sGhiChu');
		$this->db->join('tlb_nguoinhanvanban as nn','nn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=nn.FK_iMaNguoiNhan');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cb.FK_iMaPhongHD');
		$this->db->group_by('pb.sTenPB');
		return $this->db->get()->result_array();
	}
	public function layCB($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->where('iTrangThai',0);
		$this->db->from('tbl_vanban as vb');
		$this->db->select('PK_iMaCB,sHoTen,cb.FK_iMaPhongHD,sThoiGianDoc,nn.sGhiChu');
		$this->db->join('tlb_nguoinhanvanban as nn','nn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=nn.FK_iMaNguoiNhan');
		$this->db->order_by('iQuyenHan_DHNB','asc');
		return $this->db->get()->result_array();
	}
	// lấy cán bộ soạn báo cáo giấy mời
    public function layCBBC($mavanban)
    {
        $this->db->where('PK_iMaNoiDung',$mavanban);
        $this->db->from('tbl_soanbaocao');
        $this->db->select('tbl_soanbaocao.*');
        return $this->db->get()->result_array();
    }
	// lấy danh sách góp ý cho văn bản
	public function layGopY($mavanban)
	{
		$this->db->where('gy.FK_iMaVB',$mavanban);
		$this->db->select('sHoTen,pb.sTenPB,gy.*');
		$this->db->from('tbl_gopy as gy');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=gy.FK_iMaCB');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cb.FK_iMaPhongHD');
		return $this->db->get()->result_array();
	}
	// lấy cán bộ chưa được chọn
	public function layCBChuaChon()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy phong ban của những ng chưa chọn
	public function layPBChuaChon()
	{
		$this->db->where('iTrangThai',0);
		$this->db->order_by('sTenPB','asc');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// lấy thông tin gửi đi
	public function layThongTinGuiDi($macanbo,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaCB_Nhap',$macanbo);
		$this->db->order_by('sNgayNhap','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get('tbl_vanban')->result_array();
	}

	// đếm thông tin gửi đi
	public function demThongTinGuiDi($macanbo)
	{
		$this->db->where('FK_iMaCB_Nhap',$macanbo);
		return $this->db->get('tbl_vanban')->num_rows();
	}
	// cập nhật lại mã file văn bản
	public function capnhatMaVB($mavanban,$macanbo)
	{
		$this->db->where('FK_iMaVB',0);
		$this->db->where('FK_iMaCB',$macanbo);
		$this->db->set('FK_iMaVB',$mavanban);
		$this->db->update('tbl_files_vb');
	}
	// lấy danh sách file vừa up
	public function layfilevuatai($macanbo)
	{
		$this->db->where('FK_iMaVB',0);
		$this->db->where('FK_iMaCB',$macanbo);
		return $this->db->get('tbl_files_vb')->result_array();
	}
	// lấy người nhận của TP
    public function layNguoiNhanTP($maphongban,$quyenhan)
    {
        $this->db->where('FK_iMaPhongHD',$maphongban);
        $this->db->where('iQuyenHan_DHNB',$quyenhan);
        $this->db->where('iTrangThai',0);
        $this->db->select('sHoTen,FK_iMaPhongHD,PK_iMaCB');
        return $this->db->get('tbl_canbo')->result_array();
    }
    //từ mã cán bộ lấy tên phòng ban
	public function layTenPhong($macanbo)
	{
		$this->db->where('PK_iMaCB',$macanbo);
		$this->db->select('FK_iMaPhongHD');
		return $this->db->get('tbl_canbo')->row_array()['FK_iMaPhongHD'];		
	}
	
	public function kiemtratontai($id=NULL,$canbo=NULL){
		$this->db->where('FK_iMaNguoiNhan',$canbo);
		$this->db->where('FK_iMaVB',$id);		
		$this->db->from('tlb_nguoinhanvanban');		
		$this->db->select('FK_iMaNguoiNhan');
		return $this->db->get()->num_rows();
	}
	
	public function resetDaXem($mavanban)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->set('sThoiGianDoc',NULL);
		$this->db->update('tlb_nguoinhanvanban');
	}
}

/* End of file Msoanthaothongtin.php */
/* Location: ./application/models/thongtinnoibo/Msoanthaothongtin.php */