<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdauviec_quanhuyen extends CI_Model {

	// lấy đầu việc quận huyện được giao
	public function layDauViec_QuanHuyen($phongban=NULL,$limit=NULL,$offset=NULL)
	{
		// if(!empty($phongban))
		// {
		// 	$this->db->where('pb.PK_iMaPB',$phongban);
		// }
		
		// $this->db->where('sThoiGianDoc',NULL);
		$this->db->where('iLoaiPhanMem',2);
		$this->db->select('cn.sThoiGianGui,sThoiGianDoc,vb.sMoTa,sHoTen,pb.sTenPB');
		$this->db->from('tbl_phongban as pb');
		$this->db->join('tbl_canbo as cb','cb.FK_iMaPhongHD=pb.PK_iMaPB');
		$this->db->join('tlb_nguoinhanvanban as cn','cn.FK_iMaNguoiNhan=cb.PK_iMaCB');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=FK_iMaVB');
		$this->db->group_by('vb.sMoTa');
		$this->db->order_by('PK_iMaPB','asc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm đầu việc quận huyện được giao
	public function demDauViec_QuanHuyen($phongban)
	{
		// if(!empty($phongban))
		// {
		// 	$this->db->where('pb.PK_iMaPB',$phongban);
		// }
		// $this->db->where('sThoiGianDoc',NULL);
		$this->db->where('iLoaiPhanMem',2);
		$this->db->select('cn.sThoiGianGui,sThoiGianDoc,vb.sMoTa,sHoTen,pb.sTenPB');
		$this->db->from('tbl_phongban as pb');
		$this->db->join('tbl_canbo as cb','cb.FK_iMaPhongHD=pb.PK_iMaPB');
		$this->db->join('tlb_nguoinhanvanban as cn','cn.FK_iMaNguoiNhan=cb.PK_iMaCB');
		$this->db->join('tbl_vanban as vb','vb.PK_iMaVB=FK_iMaVB');
		$this->db->group_by('vb.sMoTa');
		// $this->db->order_by('PK_iMaPB','asc');
		return $this->db->get()->num_rows();
	}

}

/* End of file Mdauviec_quanhuyen.php */
/* Location: ./application/models/thongtinnoibo/Mdauviec_quanhuyen.php */