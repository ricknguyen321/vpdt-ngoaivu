<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkiennghicutri extends CI_Model {

	// lấy lĩnh vực con theo lĩnh vực cha
	public function layLinhVucCon($macha)
	{
		$this->db->where('FK_iMaLV_Cha',$macha);
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaLV,sTenLV');
		return $this->db->get('tbl_linhvuccon')->result_array();
	}
	// lấy kiến nghị cử chi cuối cùng
	public function layKienNghiCuoi()
	{
		$this->db->select('PK_iMaKienNghi,sKhoa,sNhiemKy,sKyHop,sCauHoi');
		$this->db->order_by('PK_iMaKienNghi','desc');
		$this->db->limit(1);
		return $this->db->get('tbl_kiennghicutri')->result_array();
	}
	// lấy kiến nghị theo phòng chủ trì
	public function layKienNghitheoPhong($maphong,$ma)
	{
		$this->db->where('FK_iMaPhong_CT',$maphong);
		$this->db->where('PK_iMaKienNghi !=',$ma);
		$this->db->select('PK_iMaKienNghi,sNoiDung');
		return $this->db->get('tbl_kiennghicutri')->result_array();
	}
	// lấy danh sách kiến nghị
	public function layDSKienNghi($khoa=NULL,$nhiemky=NULL,$kyhop=NULL,$trangthai=NULL,$cauhoi=NULL,$cutri=NULL,$noidungcauhoi=NULL,$linhvuccha=NULL,$linhvuccon=NULL,$lanhdao=NULL,$phongban=NULL)
	{
		if(!empty($khoa))
		{
			$this->db->where('sKhoa',$khoa);
		}
		if(!empty($nhiemky))
		{
			$this->db->where('sNhiemKy',$nhiemky);
		}
		if(!empty($kyhop))
		{
			$this->db->where('sKyHop',$kyhop);
		}
		if(!empty($trangthai))
		{
			$this->db->where('iTrangThai',$trangthai);
		}
		if(!empty($cauhoi))
		{
			$this->db->where('sCauHoi',$cauhoi);
		}
		if(!empty($cutri))
		{
			$this->db->where('sTenCuTri',$cutri);
		}
		if(!empty($noidungcauhoi))
		{
			$this->db->like('sNoiDung',$noidungcauhoi);
		}
		if(!empty($linhvuccha))
		{
			$this->db->where('FK_iMaLV_Cha',$linhvuccha);
		}
		if(!empty($linhvuccon))
		{
			$this->db->where('FK_iMaLV_Con',$linhvuccon);
		}
		if(!empty($lanhdao))
		{
			$this->db->where('FK_iMaCB_LD',$lanhdao);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPhong_CT',$phongban);
		}
		$this->db->select('PK_iMaKienNghi,sNoiDungTraLoi,sNoiDung,FK_iMaLV_Con,FK_iMaPhong_CT,FK_iMaPhong_PH,FK_iMaCB_LD,iTrangThai');
		return $this->db->get('tbl_kiennghicutri')->result_array();
	}

}

/* End of file Mkiennghicutri.php */
/* Location: ./application/models/traloikiennghicutri/Mkiennghicutri.php */