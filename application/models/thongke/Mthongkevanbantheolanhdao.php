<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mthongkevanbantheolanhdao extends CI_Model {

	// các thống kê ở bên dưới được tính từ ngày 01-01-2018
	// lấy số văn bản quá hạn đang thực hiện
	public function demVB_ChuaLam_QuaHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		$date = date('Y-m-d');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai !=',1);
		$this->db->where("sHanThongKe>'1970-01-01' and sHanThongKe < '$date'",NULL);
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		return $this->db->get()->num_rows();
	}
	public function layVB_ChuaLam_QuaHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		$date = date('Y-m-d');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai !=',1);
		$this->db->where("sHanThongKe>'1970-01-01' and sHanThongKe < '$date'",NULL);
		$this->db->select('vb.PK_iMaVBDen,vb.iGiayMoi,sTenLVB,sNgayNhap,FK_iMaNguoiNhap,sKyHieu,iSoDen,vb.sMoTa,sTenLV,sTenDV,sNgayGiaiQuyet,sHanThongKe');
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		$this->db->order_by('iSoDen','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// lấy số văn bản trong hạn đang thực hiện
	public function demVB_ChuaLam_TrongHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		$date = date('Y-m-d');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai !=',1);
		$this->db->where("(sHanThongKe <='1970-01-01' or sHanThongKe >= '$date' or sHanThongKe is NULL)",NULL);
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		return $this->db->get()->num_rows();
	}
	public function layVB_ChuaLam_TrongHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		$date = date('Y-m-d');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai !=',1);
		$this->db->where("(sHanThongKe <='1970-01-01' or sHanThongKe >= '$date' or sHanThongKe is NULL)",NULL);
		$this->db->select('vb.PK_iMaVBDen,vb.iGiayMoi,sTenLVB,sNgayNhap,FK_iMaNguoiNhap,sKyHieu,iSoDen,vb.sMoTa,sTenLV,sTenDV,sNgayGiaiQuyet,sHanThongKe');
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		$this->db->order_by('iSoDen','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// lấy số văn bản quá hạn đã thực hiện
	public function demVB_DaLam_QuaHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai',1);
		$this->db->where("sHanThongKe>'1970-01-01' and sHanThongKe < sNgayGiaiQuyet",NULL);
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		return $this->db->get()->num_rows();
	}
	// lấy số văn bản quá hạn đã thực hiện
	public function layVB_DaLam_QuaHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai',1);
		$this->db->where("sHanThongKe>'1970-01-01' and sHanThongKe < sNgayGiaiQuyet",NULL);
		$this->db->select('vb.PK_iMaVBDen,vb.iGiayMoi,sTenLVB,sNgayNhap,FK_iMaNguoiNhap,sKyHieu,iSoDen,vb.sMoTa,sTenLV,sTenDV,sNgayGiaiQuyet,sHanThongKe');
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		$this->db->order_by('iSoDen','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// lấy số văn bản trong hạn đã thực hiện
	public function demVB_DaLam_TrongHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai',1);
		$this->db->where("(sHanThongKe <='1970-01-01' or sHanThongKe >= sNgayGiaiQuyet or sNgayGiaiQuyet is NULL or sHanThongKe is NULL)",NULL);
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		return $this->db->get()->num_rows();
	}
	// lấy số văn bản trong hạn đã thực hiện
	public function layVB_DaLam_TrongHan($phongban=NULL,$phogiamdoc=NULL,$giamdoc=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('sThoiGian>=','2018-01-01');
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		if(!empty($phogiamdoc)&&empty($giamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$phogiamdoc);
		}
		if(!empty($giamdoc)&&empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
		}
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$this->db->where('PK_iMaCBChuyen',$giamdoc);
			$this->db->where('PK_iMaCBNhan',$phogiamdoc);
		}
		$this->db->where('iTrangThai',1);
		$this->db->where("(sHanThongKe <='1970-01-01' or sHanThongKe >= sNgayGiaiQuyet or sNgayGiaiQuyet is NULL or sHanThongKe is NULL)",NULL);
		$this->db->select('vb.PK_iMaVBDen,vb.iGiayMoi,sTenLVB,sNgayNhap,FK_iMaNguoiNhap,sKyHieu,iSoDen,vb.sMoTa,sTenLV,sTenDV,sNgayGiaiQuyet,sHanThongKe');
		$this->db->from('tbl_chuyennhanvanban as cn');
		$this->db->join('tbl_vanbanden as vb','vb.PK_iMaVBDen=cn.PK_iMaVBDen');
		$this->db->order_by('iSoDen','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// lấy lanh đạo sở
	public function layLD_So($quyen)
	{
		$this->db->where('FK_iMaCV !=',5);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iQuyenHan_DHNB',$quyen);
		$this->db->select('sHoTen,PK_iMaCB,iQuyenHan_DHNB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// thống kê văn bản của giám đốc
	public function layVanBan_GD_ChiDao($lanhdao)
	{
		$this->db->where('PK_iMaCBChuyen',$lanhdao);
		$this->db->where('sThoiGian>=','2018-01-01');
		$this->db->where('iGiayMoi <',3);
		$this->db->where('PK_iMaPhongCT',0);
		$this->db->where('PK_iMaCBNhan >',0);
		$this->db->select('PK_iMaCBNhan,sHoTen,count(PK_iMaVBDen) as tongvanban');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.PK_iMaCBNhan');
		$this->db->group_by('PK_iMaCBNhan');
		return $this->db->get('tbl_chuyennhanvanban as vb')->result_array();
	}
	// thống kê văn bản của phó giám đốc
	public function layVanBan_PGD_ChiDao($lanhdao,$phongban)
	{
		$this->db->where('PK_iMaCBChuyen',$lanhdao);
		$this->db->where('sThoiGian>=','2018-01-01');
		$this->db->where('iGiayMoi <',3);
		$this->db->where_in('PK_iMaPhongCT',$phongban);
		$this->db->select('PK_iMaPhongCT,sTenPB,count(PK_iMaVBDen) as tongvanban');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.PK_iMaPhongCT');
		$this->db->group_by('PK_iMaPhongCT');
		return $this->db->get('tbl_chuyennhanvanban as vb')->result_array();
	}

}

/* End of file Mthongkevanbantheolanhdao.php */
/* Location: ./application/models/thongke/Mthongkevanbantheolanhdao.php */
