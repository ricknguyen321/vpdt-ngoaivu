<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtonghop_moi extends CI_Model {
	// xóa bảng kế hoạch
	public function xoabangkehoach($mavanban,$tuan)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->where('tuan',$tuan);
		$this->db->where('active',1);
		$this->db->delete('kehoach');
	}
	public function laythongtinvanban($mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->select('sNgayHoanThanhCu');
		return $this->db->get('tbl_vanbanden')->row_array();
	}
	// lấy thông tin trong bản kế hoạch
	public function laythongtinkehoach($mavanban,$tuan)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->where('tuan',$tuan);
		return $this->db->get('kehoach')->result_array();
	}

	public function laytuanmoinhat($mavanban)
	{
		$this->db->where('vanban_id',$mavanban);
		$this->db->order_by('tuan','desc');
		return $this->db->get('kehoach')->row_array();
	}
	// lấy phòng ban
	public function layPhongBanDuThao()
    {
        $this->db->where('iTrangThai',0);
        $this->db->where('sepxep>',0);
        $this->db->select('sTenPB,PK_iMaPB,sVietTat');
        $this->db->order_by('sepxep','asc');
        return $this->db->get('tbl_phongban')->result_array();
    }
     // lấy nội dung là file kết quả
    public function layFileKetQua($mavanban,$phongban)
    {
    	$this->db->where('PK_iMaPB',$phongban);
    	$this->db->where('PK_iMaVBDen',$mavanban);
    	$this->db->select('sMoTa,sDuongDanFile');
    	$this->db->order_by('PK_iMaKetQua','desc');
    	return $this->db->get('tbl_file_ketqua')->row_array();
    }
    // lấy nội dung là file kết quả
    // public function layFileKetQua($mavanban)
    // {
    // 	$this->db->where('PK_iMaVBDen',$mavanban);
    // 	$this->db->select('sMoTa,sDuongDanFile');
    // 	$this->db->order_by('PK_iMaKetQua','desc');
    // 	return $this->db->get('tbl_file_ketqua')->row_array();
    // }
    // lấy file cuối cùng
    public function layFileCuoi($mavanban)
    {
    	$this->db->where('FK_iMaVBDen',$mavanban);
    	$this->db->select('sDuongDan');
    	$this->db->order_by('PK_iMaFileDen','desc');
    	return $this->db->get('tbl_files_vbden')->row_array();
    }

    //==========================================================================================================================
	// lấy việc đã hoàn thành
	public function thongke_hoanthanh($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('sNgayGiaiQuyet >=',$ngaybd);
		$this->db->where('sNgayGiaiQuyet <=',$ngaykt);
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		$this->db->select('tbl_vanbanden.PK_iMaVBDen,hanvb,PK_iMaPhongPH,sKyHieu,iSoDen,sNgayKy,sGhiChu,tbl_vanbanden.sMoTa,sHanGiaiQuyet,sHanThongKe,tbl_chuyennhanvanban.sThoiGian,sNgayGiaiQuyet,PK_iMaPhongPH,sVietTat,PK_iMaPhongCT,tbl_vanbanden.iTrangThai');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->join('tbl_phongban','tbl_phongban.PK_iMaPB=tbl_chuyennhanvanban.PK_iMaPhongCT');
		$this->db->order_by('sThoiGian','asc');
		return $this->db->get()->result_array();
	}
	// đếm việc đã hoàn thành
	public function dem_thongke_hoanthanh($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('sNgayGiaiQuyet >=',$ngaybd);
		$this->db->where('sNgayGiaiQuyet <=',$ngaykt);
		$this->db->select('PK_iMaPhongCT,COUNT(tbl_vanbanden.PK_iMaVBDen) as tong');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->group_by('PK_iMaPhongCT');
		return $this->db->get()->result_array();
	}
	// đếm việc đã hoàn thành đúng hạn
	public function dem_thongke_hoanthanh_dung($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('sNgayGiaiQuyet >=',$ngaybd);
		$this->db->where('sNgayGiaiQuyet <=',$ngaykt);
		//$this->db->where('(sNgayGiaiQuyet <= sHanGiaiQuyet or sHanGiaiQuyet <= "1970-01-01")',NULL);
		$this->db->where('(sNgayGiaiQuyet <= sHanThongKe or sHanThongKe <= "1970-01-01")',NULL);
		$this->db->select('PK_iMaPhongCT,COUNT(tbl_vanbanden.PK_iMaVBDen) as tong');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->group_by('PK_iMaPhongCT');
		return $this->db->get()->result_array();
	}
	// đếm việc đã hoàn thành quá hạn
	public function dem_thongke_hoanthanh_sai($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('sNgayGiaiQuyet >=',$ngaybd);
		$this->db->where('sNgayGiaiQuyet <=',$ngaykt);
		//$this->db->where('(sNgayGiaiQuyet > sHanGiaiQuyet and sHanGiaiQuyet > "1970-01-01")',NULL);
		$this->db->where('(sNgayGiaiQuyet > sHanThongKe and sHanThongKe > "1970-01-01")',NULL);
		$this->db->select('PK_iMaPhongCT,COUNT(tbl_vanbanden.PK_iMaVBDen) as tong');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->group_by('PK_iMaPhongCT');
		return $this->db->get()->result_array();
	}
//==================================================================================================================================================================================================================================================================
	// lấy việc chưa hoàn thành trong khoảng và trc khoảng
	public function thongke_chuahoanthanh($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		$this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iTrangThai !=',3);
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngaykt);
		$this->db->where('(sNgayGiaiQuyet > "'.$ngaykt.'" or sNgayGiaiQuyet IS NULL)',NULL);
		if(!empty($phongban))
		{
			$this->db->where('PK_iMaPhongCT',$phongban);
		}
		$this->db->select('tbl_vanbanden.PK_iMaVBDen,PK_iMaPhongPH,sKyHieu,iSoDen,sNgayKy,sGhiChu,tbl_vanbanden.sMoTa,sHanGiaiQuyet,tbl_chuyennhanvanban.sThoiGian,sNgayGiaiQuyet,PK_iMaPhongPH,sVietTat,PK_iMaPhongCT,iTraLai_BPTH,sNgayHoanThanhCu,tbl_vanbanden.iTrangThai');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->join('tbl_phongban','tbl_phongban.PK_iMaPB=tbl_chuyennhanvanban.PK_iMaPhongCT');
		$this->db->order_by('sThoiGian','asc');
		return $this->db->get()->result_array();
	}
	// đếm tổng việc đang dang dở
	public function dem_thongke_chuahoanthanh($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		$this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iTrangThai !=',3);
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngaykt);
		$this->db->where('(sNgayGiaiQuyet > "'.$ngaykt.'" or sNgayGiaiQuyet IS NULL)',NULL);
		$this->db->select('PK_iMaPhongCT,COUNT(tbl_vanbanden.PK_iMaVBDen) as tong');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->group_by('PK_iMaPhongCT');
		return $this->db->get()->result_array();
	}
	// đếm tổng việc đang dang dở
	public function dem_thongke_chuahoanthanh_dung($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		$this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iTrangThai !=',3);
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngaykt);
		$this->db->where('(sNgayGiaiQuyet > "'.$ngaykt.'" or sNgayGiaiQuyet IS NULL)',NULL);
		$this->db->where('(sHanGiaiQuyet <= "1970-01-01" or sHanGiaiQuyet IS NULL or sHanGiaiQuyet >= "'.$ngaykt.'")',NULL);
		$this->db->select('PK_iMaPhongCT,COUNT(tbl_vanbanden.PK_iMaVBDen) as tong');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->group_by('PK_iMaPhongCT');
		return $this->db->get()->result_array();
	}
	// đếm tổng việc đang dang dở
	public function dem_thongke_chuahoanthanh_sai($ngaybd,$ngaykt,$phongban=NULL,$vbtt=NULL,$vbstc=NULL,$nam=NULL)
	{
		if(!empty($nam))
		{
			$this->db->where('SUBSTR(sNgayKy,1,4)',$nam);
		}
		$this->db->where('tbl_vanbanden.sNgayNhap >','2017-04-01');
		if(!empty($vbtt) && empty($vbstc))// lấy văn bản thông thường
		{
			$this->db->where('iSTCChuTri !=',1);
		}
		if(empty($vbtt) && !empty($vbstc))// lấy văn bản của stc chủ trì
		{
			$this->db->where('iSTCChuTri',1);
		}
		$this->db->where('tbl_vanbanden.iTrangThai !=',3);
		$this->db->where('tbl_vanbanden.iGiayMoi',2);
		$this->db->where('PK_iMaPhongCT >',0);
		$this->db->where('Date(tbl_vanbanden.sNgayNhap) <=',$ngaykt);
		$this->db->where('(sNgayGiaiQuyet > "'.$ngaykt.'" or sNgayGiaiQuyet IS NULL)',NULL);
		$this->db->where('(sHanGiaiQuyet < "'.$ngaykt.'" and sHanGiaiQuyet > "1970-01-01")',NULL);
		$this->db->select('PK_iMaPhongCT,COUNT(tbl_vanbanden.PK_iMaVBDen) as tong');
		$this->db->from('tbl_vanbanden');
		$this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen=tbl_vanbanden.PK_iMaVBDen');
		$this->db->group_by('PK_iMaPhongCT');
		return $this->db->get()->result_array();
	}

}

/* End of file Mtonghop_moi.php */
/* Location: ./application/models/thongke/Mtonghop_moi.php */