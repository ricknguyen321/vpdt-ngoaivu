<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mthongke extends CI_Model {
    // lấy văn bản đi
    public function layVBDicoTL()
    {
        $query ="select  sDuongDan,iSoDen,iSoVBDi,PK_iMaVBDi from tbl_files_vbdi 
		join tbl_vanbandi on tbl_files_vbdi.FK_iMaVBDi=tbl_vanbandi.PK_iMaVBDi 
		where tbl_files_vbdi.PK_iMaFileDi in (select max(tbl_files_vbdi.PK_iMaFileDi) from tbl_files_vbdi
		group by tbl_files_vbdi.FK_iMaVBDi) and iSoDen>0 AND sNgayNhap > '2017-04-01 00:00:00' group by tbl_files_vbdi.FK_iMaVBDi  
		ORDER BY iSoDen  ASC";
        return $this->db->query($query)->result_array();

    }
     // ======================================================================================================
    public function getDepartment($trangthai=NULL)
    {
        if(isset($trangthai)){
            $this->db->where('tbl_phongban.iTrangThai',$trangthai);
        }
        $this->db->where('tbl_phongban.iLoaiPhanMem !=',2);
        $this->db->where_in('iQuyenHan_DHNB',array(3,6));
        $this->db->select('tbl_phongban.PK_iMaPB,tbl_phongban.sTenPB,tbl_canbo.PK_iMaCB');
        $this->db->from('tbl_phongban');
        $this->db->join('tbl_canbo','tbl_canbo.FK_iMaPhongHD = tbl_phongban.PK_iMaPB','left');
        $this->db->group_by('tbl_phongban.PK_iMaPB');
        $this->db->order_by('tbl_phongban.sepxep','asc');
        return $this->db->get()->result_array();
    }
    // lấy theo từng phòng
    // lấy theo từng phòng
    public function layDauViecTheoPhong($mangMaPB,$trangthai,$sangtao=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$ngaynhap=NULL,$denngay=NULL,$session=NULL,$idLanhDao=NULL)
    {
        $this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        if(!empty($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
            }
            $this->db->where_in('vb.iTrangThai',$trangthai);
        }
        if(!empty($dagiaiquyet)){
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
                //$this->db->where('vb.iCoSangTao !=',1);
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            else{
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
                $this->db->where('sHanThongKe >','2017-04-01');
            }
            $this->db->where('vb.iTrangThai',$trangthai);
        }

        if($sangtao==1)// 1 có sáng tạo
        {
            $this->db->where('vb.iCoSangTao',1);
            $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            $this->db->where('vb.iTrangThai',1);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($denngay)){
            $this->db->where('Date(vb.sNgayNhap) <=',$denngay);
        }
        if(!empty($idLanhDao)){
            $this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
        }
        $this->db->where('cn.iGiayMoi',2);
        //$this->db->where('vb.sLuuVaoKho !=',1);
        //$this->db->where('vb.iTrangThai !=',3);
        $this->db->where('PK_iMaPhongCT',$mangMaPB);
        $this->db->select('PK_iMaPhongCT,COUNT(vb.PK_iMaVBDen) as tong');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        if(!empty($session) && $session == 12) {
            $this->db->join('tbl_chicuc', 'tbl_chicuc.doc_id = vb.PK_iMaVBDen', 'left');
            $this->db->where('tbl_chicuc.tcdn_active >=','2');
        }
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
    public function tongDauViecTheoPhong($mangMaPB,$ngaynhap=NULL,$ngayden=NULL,$session=NULL,$idLanhDao=NULL)
    {
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
        }
        if(!empty($idLanhDao)){
            $this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
        }
        $this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        $this->db->where('vb.iTrangThai !=',3);
        $this->db->where('vb.sLuuVaoKho !=',1);
        $this->db->where('PK_iMaPhongCT',$mangMaPB);
        $this->db->select('PK_iMaPhongCT,COUNT(vb.PK_iMaVBDen) as tong');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        if(!empty($session) && $session == 12) {
            $this->db->join('tbl_chicuc', 'tbl_chicuc.doc_id = vb.PK_iMaVBDen', 'left');
            $this->db->where('tbl_chicuc.tcdn_active >=','2');
        }
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }

    public function layDauViecTheoBGD($mangMaPB,$trangthai,$sangtao=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$ngaynhap=NULL,$denngay=NULL,$session=NULL,$idLanhDao=NULL)
    {
        $this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        if(!empty($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
            }
            $this->db->where_in('vb.iTrangThai',$trangthai);
        }
        if(!empty($dagiaiquyet)){
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
                //$this->db->where('vb.iCoSangTao !=',1);
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            else{
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
                $this->db->where('sHanThongKe >','2017-04-01');
            }
            $this->db->where('vb.iTrangThai',$trangthai);
        }

        if($sangtao==1)// 1 có sáng tạo
        {
            $this->db->where('vb.iCoSangTao',1);
            $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            $this->db->where('vb.iTrangThai',1);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($denngay)){
            $this->db->where('Date(vb.sNgayNhap) <=',$denngay);
        }
        if(!empty($idLanhDao)){
            $this->db->where('vb.PK_iMaCBChuyen',$idLanhDao);
        }
        $this->db->where('cn.iGiayMoi',2);
        $this->db->where('vb.sLuuVaoKho !=',1);
        $this->db->where('vb.iTrangThai !=',3);
        $this->db->where('PK_iMaCBNhan',$mangMaPB);
        $this->db->select('PK_iMaCBNhan,COUNT(vb.PK_iMaVBDen) as tong');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
    public function tongDauViecTheoBGD($mangMaPB,$ngaynhap=NULL,$ngayden=NULL,$session=NULL,$idLanhDao=NULL)
    {
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($ngayden)){
            $this->db->where('Date(vb.sNgayNhap) <=',$ngayden);
        }
        if(!empty($idLanhDao)){
            $this->db->where('vb.PK_iMaCBChuyen',$idLanhDao);
        }
        $this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        $this->db->where('cn.iGiayMoi',2);
        $this->db->where('vb.iTrangThai !=',3);
        $this->db->where('vb.sLuuVaoKho !=',1);
        $this->db->where('PK_iMaCBNhan',$mangMaPB);
        $this->db->select('PK_iMaCBNhan,COUNT(vb.PK_iMaVBDen) as tong');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
    // ======================================================================================================
    // lấy ds nhân viên theo phòng hoạt động
    public function layDSCanBoPhong($phongban)
    {
        $this->db->where('FK_iMaPhongHD',$phongban);
        //$this->db->where('iTrangThai',0);
        $this->db->select('PK_iMaCB,sHoTen,iQuyenHan_DHNB');
        return $this->db->get('tbl_canbo')->result_array();
    }
	public function layMaPhong($id)
    {
        $this->db->where('PK_iMaCB',$id);
        $this->db->where('iTrangThai',0);
        $this->db->select('FK_iMaPhongHD');
        return $this->db->get('tbl_canbo')->result_array();
    }
    public function layTongDauViec($maCB,$ngaynhap=NULL,$denngay=NULL,$idLanhDao=NULL)
    {
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($denngay)){
            $this->db->where('Date(vb.sNgayNhap) <=',$denngay);
        }
        if(!empty($idLanhDao)){
            $this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
        }
        $this->db->where('vb.sNgayNhap >','2017-04-01');
        //$this->db->where('vb.iTrangThai !=',3);
        $this->db->where('vb.iGiayMoi',2);
        $this->db->where('cn.iGiayMoi',2);
        //$this->db->where('vb.sLuuVaoKho !=',1);
        $this->db->where('PK_iMaCVCT',$maCB);
		$this->db->where('vb.PK_iMaCBDuyet !=',$maCB);	
        $this->db->select('PK_iMaCVCT,vb.PK_iMaVBDen');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        //$this->db->order_by('vb.PK_iMaVBDen','asc');
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
    public function layTongDauViecTP($phongban,$ngaynhap=NULL,$denngay=NULL,$session=NULL,$idLanhDao=NULL)
    {
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($denngay)){
            $this->db->where('Date(vb.sNgayNhap) <=',$denngay);
        }
        if(!empty($idLanhDao)){
            $this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
        }
        $this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        $this->db->where('cn.iGiayMoi',2);
        //$this->db->where('vb.sLuuVaoKho !=',1);
        //$this->db->where('vb.iTrangThai !=',3);
        $this->db->where('PK_iMaPhongCT',$phongban);
        $this->db->select('PK_iMaPhongCT,COUNT(vb.PK_iMaVBDen) as tong');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        if(!empty($session) && $session == 12) {
            $this->db->join('tbl_chicuc', 'tbl_chicuc.doc_id = vb.PK_iMaVBDen', 'left');
            $this->db->where('tbl_chicuc.tcdn_active >=','2');
        }
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }

    public function layDauViecGiaiQuyetTP($phongban,$trangthai,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$ngaynhap=NULL,$denngay=NULL,$session=NULL,$idLanhDao=NULL)// trạng thái 0: chưa giải quyết 1,2 đã giải quyết
    {
        $this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        if(!empty($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
                //$this->db->where('vb.iTrangThai_TruyenNhan',5);
            }
            $this->db->where_in('vb.iTrangThai',$trangthai);
        }
        if(!empty($dagiaiquyet)){
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= sNgayGiaiQuyet)',NULL);
            }
            else{
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
                $this->db->where('sHanThongKe >','2017-04-01');
            }
            $this->db->where('vb.iTrangThai',$trangthai);
        }
        if(!empty($ngaynhap)){
            $this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
        }
        if(!empty($denngay)){
            $this->db->where('Date(vb.sNgayNhap) <=',$denngay);
        }
        if(!empty($idLanhDao)){
            $this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
        }
        $this->db->where('cn.iGiayMoi',2);
        //$this->db->where('vb.sLuuVaoKho !=',1);
        //$this->db->where('vb.iTrangThai !=',3);
        $this->db->where('PK_iMaPhongCT',$phongban);
        $this->db->select('PK_iMaPhongCT,COUNT(vb.PK_iMaVBDen) as tong');
        $this->db->from('tbl_vanbanden as vb');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
        if(!empty($session) && $session == 12) {
            $this->db->join('tbl_chicuc', 'tbl_chicuc.doc_id = vb.PK_iMaVBDen', 'left');
            $this->db->where('tbl_chicuc.tcdn_active >=','2');
        }
        $this->db->group_by('vb.PK_iMaVBDen');
        return $this->db->get()->result_array();

    }
    public function layDauViecGiaiQuyet($maCB,$trangthai,$sangtao=NULL,$chuagiaquyet=NULL,$dagiaiquyet=NULL,$ngaynhap=NULL,$denngay=NULL,$idLanhDao=NULL,$truongphong=NULL,$quyen=NULL)// trạng thái 0: chưa giải quyết 1,2 đã giải quyết
    {
		//$this->db->where('vb.sNgayNhap >','2017-04-01');
        $this->db->where('vb.iGiayMoi',2);
        $this->db->where('cn.iGiayMoi',2);
        if(isset($chuagiaquyet))
        {
            if($chuagiaquyet==1){ // 1: trong hạn 2:quá hạn
                $this->db->where('(sHanThongKe <= "1970-01-01" OR sHanThongKe >= "'.date('Y-m-d').'")',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe <',date('Y-m-d'));
            }
            $this->db->where('vb.iTrangThai !=',1);
			if ($maCB != $truongphong) {
				$this->db->where('cn.PK_iMaCVCT',$maCB);		
				$this->db->where('vb.PK_iMaCBDuyet !=',$maCB);
			} else {
				$this->db->where('cn.PK_iMaCVCT',0);	
				$this->db->where('lvcn.FK_iMaCB_Nhan',$maCB);
				$this->db->where('vb.iTrangThai_TruyenNhan',5);			
			}
			
			//chung
			if(!empty($ngaynhap)){
				$this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
			}
			if(!empty($denngay)){
				$this->db->where('Date(vb.sNgayNhap) <=',$denngay);
			}
			if(!empty($idLanhDao)){
				$this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
			}
			if(!empty($quyen)){
				
				if ($quyen == 3) {
					$this->db->where('vb.iTrangThai_TruyenNhan',5);	
				}else {
					$this->db->where('vb.iTrangThai_TruyenNhan',$quyen - 1);
				}
			}
			if($sangtao==1)// 1 có sáng tạo
			{
				$this->db->where('iCoSangTao',1);
				$this->db->where('vb.iTrangThai',$trangthai);
			}
			//$this->db->where('vb.sLuuVaoKho !=',1);
			//$this->db->where('vb.iTrangThai !=',3);
			
			
			//$this->db->where('vb.PK_iMaCBHoanThanh',$maCB);		
			$this->db->select('cn.PK_iMaCVCT,cn.CapGiaiQuyet');
			$this->db->from('tbl_vanbanden as vb');
			$this->db->join('tbl_luuvet_chuyennhan as lvcn','lvcn.FK_iMaVBDen=vb.PK_iMaVBDen','left');
			$this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
			$this->db->order_by('lvcn.sThoiGian','desc');
			$this->db->group_by('vb.PK_iMaVBDen');
        } else {
            if($dagiaiquyet==1){ // 1: đúng hạn 2: quá hạn
//                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe >= sNgayGiaiQuyet',NULL);
            }
            else{
                $this->db->where('vb.sHanThongKe >','2017-04-01');
                $this->db->where('sHanThongKe < sNgayGiaiQuyet',NULL);
            }
			$this->db->where('vb.PK_iMaCBHoanThanh',$maCB);		
            $this->db->where('vb.iTrangThai',1);
			
			//chung
			if(!empty($ngaynhap)){
				$this->db->where('Date(vb.sNgayNhap) >=',$ngaynhap);
			}
			if(!empty($denngay)){
				$this->db->where('Date(vb.sNgayNhap) <=',$denngay);
			}
			if(!empty($idLanhDao)){
				$this->db->where('cn.PK_iMaCBChuyen',$idLanhDao);
			}
			if(!empty($quyen)){
				
				if ($quyen == 3) {
					$this->db->where('vb.iTrangThai_TruyenNhan',5);	
				}else {
					$this->db->where('vb.iTrangThai_TruyenNhan',$quyen - 1);
				}
			}
			if($sangtao==1)// 1 có sáng tạo
			{
				$this->db->where('iCoSangTao',1);
			}
			
			//$this->db->select('cn.PK_iMaCVCT,cn.CapGiaiQuyet');
			$this->db->from('tbl_vanbanden as vb');
			$this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vb.PK_iMaVBDen');
			$this->db->group_by('vb.PK_iMaVBDen');
        }
       
        return $this->db->get()->result_array();

    }
    // ======================================================================================================
    // lấy thông tin văn bản đến
    public function insoluutru_den(
        $soditu      = NULL,
        $sodiden     = NULL,
        $ngaynhaptu  = NULL,
        $ngaynhapden = NULL,
        $noiguiden   = NULL,
        $loaivanban  = NULL,
        $ngaymoitu   = NULL,
        $ngaymoiden  = NULL,
        $phongban    = NULL,
        $lanhdao     = NULL,
        $canbo       = NULL,
        $limit       = NULL,
        $offset      = NULL
    )
    {
        if(empty($canbo))
        {
            $this->db->where('PK_iMaPhongCT >',0);
        }
        if(!empty($soditu))
        {
            $this->db->where('iSoDen >=',$soditu);
        }
        if(!empty($sodiden))
        {
            $this->db->where('iSoDen <=',$sodiden);
        }
        if(!empty($ngaynhaptu))
        {
            $this->db->where('vbd.sNgayNhap >=',date_insert($ngaynhaptu));
        }
        if(!empty($ngaynhapden))
        {
            $this->db->where('vbd.sNgayNhap <=',date_insert($ngaynhapden));
        }
        if(!empty($noiguiden))
        {
            $this->db->like('sTenDV',$noiguiden);
        }
        if(!empty($loaivanban))
        {
            $this->db->like('sTenLVB',$loaivanban);
        }
        if(!empty($ngaymoitu))
        {
            $this->db->where('sGiayMoiNgay >=',date_insert($ngaymoitu));
        }
        if(!empty($ngaymoiden))
        {
            $this->db->where('sGiayMoiNgay <=',date_insert($ngaymoiden));
        }
        if(!empty($phongban) && empty($canbo))
        {
            $this->db->where('PK_iMaPhongCT',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('PK_iMaCBChuyen',$lanhdao);
        }
        if(!empty($canbo))
        {
            $this->db->where('cn.PK_iMaCVCT',$canbo);
        }
		if(empty($canbo))
        {
            $this->db->select('vbd.PK_iMaVBDen,vbd.sNgayNhan,sTenPB,vbd.sHanGiaiQuyet,vbd.iTrangThai,iSoDen,sTenDV,sKyHieu,sNgayKy,sTenLVB,vbd.sMoTa,sTenNguoiKy,sHoTen');
        }else{
			$this->db->select('vbd.PK_iMaVBDen,vbd.sNgayNhan,vbd.sHanGiaiQuyet,vbd.iTrangThai,iSoDen,sTenDV,sKyHieu,sNgayKy,sTenLVB,vbd.sMoTa,sTenNguoiKy,sHoTen');
		}

        $this->db->from('tbl_vanbanden as vbd');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vbd.PK_iMaVBDen');
        if(empty($canbo))
        {
            $this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cn.PK_iMaPhongCT');
        }
        $this->db->join('tbl_canbo as cb','cb.PK_iMaCB=cn.PK_iMaCBChuyen');
        $this->db->order_by('iSoDen','asc');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
    // đếm thông tin văn bản đến
    public function dem_insoluutru_den(
        $soditu      = NULL,
        $sodiden     = NULL,
        $ngaynhaptu  = NULL,
        $ngaynhapden = NULL,
        $noiguiden   = NULL,
        $loaivanban  = NULL,
        $ngaymoitu   = NULL,
        $ngaymoiden  = NULL,
        $phongban    = NULL,
        $lanhdao     = NULL,
        $canbo       = NULL
    )
    {
        if(empty($canbo))
        {
            $this->db->where('PK_iMaPhongCT >',0);
        }
        if(!empty($soditu))
        {
            $this->db->where('iSoDen >=',$soditu);
        }
        if(!empty($sodiden))
        {
            $this->db->where('iSoDen <=',$sodiden);
        }
        if(!empty($ngaynhaptu))
        {
            $this->db->where('vbd.sNgayNhap >=',date_insert($ngaynhaptu));
        }
        if(!empty($ngaynhapden))
        {
            $this->db->where('vbd.sNgayNhap <=',date_insert($ngaynhapden));
        }
        if(!empty($noiguiden))
        {
            $this->db->like('sTenDV',$noiguiden);
        }
        if(!empty($loaivanban))
        {
            $this->db->like('sTenLVB',$loaivanban);
        }
        if(!empty($ngaymoitu))
        {
            $this->db->where('sGiayMoiNgay >=',date_insert($ngaymoitu));
        }
        if(!empty($ngaymoiden))
        {
            $this->db->where('sGiayMoiNgay <=',date_insert($ngaymoiden));
        }
        if(!empty($phongban) && empty($canbo))
        {
            $this->db->where('PK_iMaPhongCT',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('PK_iMaCBChuyen',$lanhdao);
        }
        if(!empty($canbo))
        {
            $this->db->where('cn.PK_iMaCVCT',$canbo);
        }
        $this->db->select('vbd.PK_iMaVBDen');
        $this->db->from('tbl_vanbanden as vbd');
        $this->db->join('tbl_chuyennhanvanban as cn','cn.PK_iMaVBDen=vbd.PK_iMaVBDen');
        if(empty($canbo))
        {
            $this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cn.PK_iMaPhongCT');
        }
        $this->db->join('tbl_canbo as cb','cb.PK_iMaCB=cn.PK_iMaCBChuyen');
        return $this->db->get()->num_rows();
    }
    // ======================================================================================================
    public function insoluutru_di($soditu=NULL,$sodiden=NULL,$ngaytu=NULL,$ngayden=NULL,$loaiso,$giaymoitu=NULL,$giaymoiden=NULL,$phongban=NULL, $limit=NULL,$offset=NULL)
    {
        if(!empty($soditu))
        {
            $this->db->where('iSoVBDi >=',$soditu);
        }
        if(!empty($sodiden))
        {
            $this->db->where('iSoVBDi <=',$sodiden);
        }
        if(!empty($ngaytu))
        {
            $this->db->where('sNgayVBDi >=',date_insert($ngaytu));
        }
        if(!empty($ngayden))
        {
            $this->db->where('sNgayVBDi <=',date_insert($ngayden));
        }
        if($loaiso==1)
        {
            $this->db->where('FK_iMaLVB',10);
        }
        else
        {
            $this->db->where('FK_iMaLVB !=',10);
        }
        if(!empty($giaymoitu))
        {
            $this->db->where('sNgayMoi >=',date_insert($giaymoitu));
        }
        if(!empty($giaymoiden))
        {
            $this->db->where('sNgayMoi <=',date_insert($giaymoiden));
        }
        if(!empty($phongban))
        {
            $this->db->where('FK_iMaPB',$phongban);
        }
        $this->db->select('sTenLVB,sHoTen,sTenPB,vb.sMoTa,sKyHieu,sNgayVBDi,sNoiNhan,FK_iMaCB_Nhap');
        $this->db->from('tbl_vanbandi as vb');

        $this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
        $this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
        $this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB=vb.FK_iMaLVB');
        $this->db->order_by('iSoVBDi','ASC');
        $this->db->limit($limit,$offset);
        return $this->db->get()->result_array();
    }
    public function dem_insoluutru_di($soditu=NULL,$sodiden=NULL,$ngaytu=NULL,$ngayden=NULL,$loaiso,$giaymoitu=NULL,$giaymoiden=NULL,$phongban=NULL)
    {
        if(!empty($soditu))
        {
            $this->db->where('iSoVBDi >=',$soditu);
        }
        if(!empty($sodiden))
        {
            $this->db->where('iSoVBDi <=',$sodiden);
        }
        if(!empty($ngaytu))
        {
            $this->db->where('sNgayVBDi >=',date_insert($ngaytu));
        }
        if(!empty($ngayden))
        {
            $this->db->where('sNgayVBDi <=',date_insert($ngayden));
        }
        if($loaiso==1)
        {
            $this->db->where('FK_iMaLVB',10);
        }
        else
        {
            $this->db->where('FK_iMaLVB !=',10);
        }
        if(!empty($giaymoitu))
        {
            $this->db->where('sNgayMoi >=',date_insert($giaymoitu));
        }
        if(!empty($giaymoiden))
        {
            $this->db->where('sNgayMoi <=',date_insert($giaymoiden));
        }
        if(!empty($phongban))
        {
            $this->db->where('FK_iMaPB',$phongban);
        }
        $this->db->select('sTenLVB,sHoTen,sTenPB,vb.sMoTa,sKyHieu,sNgayVBDi,sNoiNhan,FK_iMaCB_Nhap');
        $this->db->from('tbl_vanbandi as vb');
        $this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
        $this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
        $this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB=vb.FK_iMaLVB');
        $this->db->order_by('iSoVBDi','ASC');
        return $this->db->get()->num_rows();
    }
    //========================================================================================
    // lấy phòng phối hợp
    public function layPhongPhoiHop($ma)
    {
        $this->db->where_in('PK_iMaPB',$ma);
        $this->db->select('sVietTat,sTenPB');
        return $this->db->get('tbl_phongban')->result_array();
    }
    // lấy kết quả theo mã văn bản
    public function layKetQuaGiaiQuyet($ma)
    {
        $this->db->where('PK_iMaVBDen',$ma);
        return $this->db->get('tbl_file_ketqua')->row_array();
    }
    // lấy người giải quyết cuối cùng
    public function layNguoiGiaiQuyetCuoi($mavanban)
    {
        $this->db->where('PK_iMaVBDen',$mavanban);
        $this->db->select('sHoTen');
        $this->db->from('tbl_chuyennhanvanban as cn');
        $this->db->join('tbl_canbo as cb','cb.PK_iMaCB=cn.PK_iMaCVCT');
        $this->db->order_by('PK_iMaCN','desc');
        return $this->db->get()->row_array();
    }
    // lấy tên người giải quyết
    public function nguoiGiaiQuyet($mavanban)
    {
        $this->db->where('PK_iMaVBDen',$mavanban);
        $this->db->where('sDuongDanFile !=','');
        $this->db->select('sHoTen');
        $this->db->from('tbl_file_ketqua as f');
        $this->db->join('tbl_canbo as cb','cb.PK_iMaCB=f.PK_iMaCB');
        return $this->db->get()->row_array();
    }
    // lấy kết quả đạt được
    public function layKetQuaDatDuoc($mavanban)
    {
        $query = "SELECT sTenFile,sDuongDan FROM tbl_files_vbdi WHERE FK_iMaVBDi IN (SELECT PK_iMaVBDi FROM tbl_vanbandi WHERE iSoDen = '".$mavanban."') LIMIT 1";
        return $this->db->query($query)->row_array();
    }
    public function layPhongBanDuThao()
    {
        $this->db->where('iTrangThai',0);
        $this->db->where('sepxep>',0);
        $this->db->select('sTenPB,PK_iMaPB');
        $this->db->order_by('sepxep','asc');
        return $this->db->get('tbl_phongban')->result_array();
    }
    public function baocaotonghop(
        $all          = NULL,
        $vbtt         = NULL,
        $vbstc        = NULL,
        $vbtt_tbkt    = NULL,
        $vbtt_khac    = NULL,
        $vbstc_tbkt   = NULL,
        $vbstc_khac   = NULL,
        $khongthoihan = NULL,
        $cothoihan    = NULL,
        $ngay_ngaybd  = NULL,
        $ngay_ngaykt  = NULL,
        $soden_tu     = NULL,
        $soden_den    = NULL,
        $cgq          = NULL,
        $cgq_cdh      = NULL,
        $cgq_sdh      = NULL,
        $cgq_dqh      = NULL,
        $dgq          = NULL,
        $dgq_dth      = NULL,
        $dgq_qth      = NULL,
        $phongban     = NULL,
        $quyen=NULL)
    {
        $where= ' where 1=1 AND PK_iMaPhongCT !=0 AND sLuuVaoKho !=1 AND cn.iGiayMoi =2 AND vbd.iGiayMoi =2  AND vbd.sNgayNhap > "2017-04-01 00:00:00" ';
        if($all==1)// tất cả văn bản
        {
            if($vbtt==1) // văn bản thông thường
            {
                if($vbtt_tbkt==1 && $vbtt_khac !=1)
                {
                    $where .= " AND (((iSTCChuTri != 1 and iVanBanTBKL = 1)";
                }
                elseif($vbtt_tbkt!=1 && $vbtt_khac ==1)
                {
                    $where .= " AND (((iSTCChuTri != 1 and iVanBanTBKL != 1)";
                }
                else{
                    $where .= " AND ((iSTCChuTri != 1";
                }
                // văn bản sở tài chính
                if($vbstc==1){ // van ban stc chu tri
                    if($vbtt_tbkt==1 && $vbstc_khac!=1)
                    { // tbkl - vbstc
                        $where .= " OR (iSTCChuTri = 1 and iVanBanTBKL = 1))";
                    }
                    elseif($vbstc_khac==1 && $vbtt_tbkt!=1)
                    { // khac - vbstc
                        $where .= " OR (iSTCChuTri = 1 and iVanBanTBKL != 1))";
                    }else
                    { // ca 2 loai
                        $where .= " OR iSTCChuTri = 1)";
                    }
                }else{
                    $where .= " )";
                }
            }
            elseif($vbstc==1) // văn bản sở tài chính chủ trì
            {
                if($vbstc_tbkt==1 && $vbstc_khac !=1)
                {
                    $where .= " AND ((iSTCChuTri = 1 and iVanBanTBKL = 1)";
                }
                elseif($vbtt_tbkt!=1 && $vbtt_khac ==1)
                {
                    $where .= " AND ((iSTCChuTri = 1 and iVanBanTBKL != 1)";
                }
                else{
                    $where .= " AND (iSTCChuTri = 1";
                }
            }
            else{
                $where .= " AND ( 1 =1 ";
            }
        }
        if($khongthoihan==1)
        { // co thoi han (doc_theodoi = 0)
            $where .= " AND (sTenLVB = 'Quyết định' OR sTenLVB = 'quyết định') AND sHanGiaiQuyet-sNgayNhan = 5";
        }

        if($cothoihan==1)
        { // khon thoi han (doc_theodoi = 1)
            $where .= " AND sHanGiaiQuyet-sNgayNhan > 5";
        }
        if (!empty($soden_tu)) $where .= " AND iSoDen >= ".$soden_tu;
        if (!empty($soden_den)) $where .= " AND iSoDen <= ".$soden_den;
        if (!empty($ngay_ngaybd)) $where .= " AND (sNgayNhan >= '".date_insert($ngay_ngaybd)."')";
        if (!empty($ngay_ngaykt)) $where .= " AND(  sNgayNhan <= '".date_insert($ngay_ngaykt)."')";

        if($cgq==1){ // chua gai quyet
            if($cgq_cdh==1){ // chua den han trang thái =0
                $where .= " and vbd.iTrangThai = 0 and sHanGiaiQuyet >= '".date('Y-m-d')."'";
            }elseif($cgq_sdh==1){ // sap den han
                $where .= " and vbd.iTrangThai = 0 and(";
                $where .= " sHanGiaiQuyet <= '".date('Y-m-d', strtotime(date('Y-m-d'))+(3*24*60*60))."'";
                $where .= " and sHanGiaiQuyet >= '".date('Y-m-d')."'";
                $where .= " )";
            }elseif($cgq_dqh==1){ // qua han
                $where .= " and vbd.iTrangThai = 0 and sHanGiaiQuyet < '".date('Y-m-d')."'";
            }else{
                $where .= " and vbd.iTrangThai = 0";
            }
        }
        if($dgq==1){ // da gai quyet
            if($dgq_dth==1){ // dung han
                $where .= " and vbd.iTrangThai = 1 and (sHanGiaiQuyet >= sNgayGiaiQuyet or sHanGiaiQuyet <= '1970-01-01' )";
            }elseif($dgq_qth==1){ // qua han
                $where .= " and vbd.iTrangThai = 1 and sHanGiaiQuyet > '1970-01-01' and sHanGiaiQuyet < sNgayGiaiQuyet";
            }else{
                $where .= " and vbd.iTrangThai = 1";
            }
        }
        if($quyen==2 || $quyen==9 || $quyen==3 || $quyen==4 || $quyen==5 )
        {
            $where_list = $where;
            if($phongban){
                $where_list .= " AND PK_iMaPhongCT = ".$phongban.")";
            }else{
                $where_list .= " )";
            }
        }
        else
        {	$where_list = $where;
            $where_list .= " AND PK_iMaPhongCT = ".$phongban.")";
        }
        $query = 'SELECT vbd.PK_iMaVBDen,vbd.sGhiChu,vbd.iTrangThai,vbd.sKyHieu,vbd.iSoDen,vbd.sNgayKy,vbd.sMoTa,vbd.sHanGiaiQuyet,vbd.sNgayGiaiQuyet,cn.sThoiGian,cn.PK_iMaPhongPH,pb.sVietTat,cn.PK_iMaPhongCT FROM tbl_vanbanden as vbd LEFT JOIN tbl_chuyennhanvanban as cn ON cn.PK_iMaVBDen = vbd.PK_iMaVBDen JOIN tbl_phongban as pb ON pb.PK_iMaPB = cn.PK_iMaPhongCT '.$where_list." GROUP BY vbd.PK_iMaVBDen";
        return $this->db->query($query)->result_array();
    }

}

/* End of file Mthongke.php */
/* Location: ./application/models/thongke/Mthongke.php */