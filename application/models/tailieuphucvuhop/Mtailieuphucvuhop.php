<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtailieuphucvuhop extends CI_Model {


    // lấy thông tin tài liệu 
    public function layTTTaiLieu($khoa=NULL,$nhiemky=NULL,$kyhop=NULL)
    {
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        $this->db->select('PK_iMaTaiLieu,sTenPhuLuc,sNoiDung,stt');
        $this->db->order_by('stt','asc');
        $this->db->order_by('PK_iMaTaiLieu','asc');
        return $this->db->get('tbl_tailieuphucvuhop')->result_array();
    }
    // lấy dữ liệu theo điều kiện truyền vào
    public function layDL($dieukien)
    {
        $this->db->select($dieukien);
        $this->db->group_by($dieukien);
        return $this->db->get('tbl_tailieuphucvuhop')->result_array();
    }
    // lấy thông tin cuối vừa nhập
    public function layTTCuoi()
    {
        $this->db->select('sKhoa,sNhiemKy,sKyHop,iTrangThaiKyHop');
        $this->db->order_by('PK_iMaTaiLieu','desc');
        $this->db->limit(1);
        return $this->db->get('tbl_tailieuphucvuhop')->result_array();
    }
    public function layPhongBanDuThao()
    {
        $this->db->where('iTrangThai',0);
        $this->db->where('sepxep>',0);
        $this->db->select('sTenPB,PK_iMaPB,sVietTat');
        $this->db->order_by('PK_iMaPB','asc');
        return $this->db->get('tbl_phongban')->result_array();
    }
    // lấy file tài liệu theo từng mã
    public function layFile($matailieu)
    {
        $this->db->where('FK_iMaTaiLieu',$matailieu);
        $this->db->where('CT_PH',1);
        $this->db->where('iTrangThai',1);
        $this->db->select('sDuongDan');
        return $this->db->get('tbl_file_tailieu')->result_array();
    }
    // lấy danh sách tài liêu
    public function layDSTaiLieu($khoa=NULL,$nhiemky=NULL,$kyhop=NULL,$dinhkybatthuong=NULL,$tenphuluc=NULL,$trangthaitailieu=NULL,$noidungtailieu=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongchutri=NULL,$loaitailieu=NULL,$limit=NULL,$offset=NULL)
    {
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($dinhkybatthuong))
        {
            $this->db->where('iTrangThaiKyHop',$dinhkybatthuong);
        }
        if(!empty($tenphuluc))
        {
            $this->db->where('sTenPhuLuc',$tenphuluc);
        }
        if(!empty($trangthaitailieu))
        {
            $this->db->where('iTrangThaiTaiLieu',$trangthaitailieu);
        }
        if(!empty($noidungtailieu))
        {
            $this->db->like('sNoiDung',$noidungtailieu);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('FK_iMaLV',$linhvuc);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        if(!empty($phongchutri))
        {
            $this->db->where('FK_iMaPhong_CT',$phongchutri);
        }
        if(!empty($loaitailieu))
        {
            $this->db->where('FK_iMaLTL',$loaitailieu);
        }
        $this->db->order_by('FK_iMaLTL','asc');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->select('PK_iMaTaiLieu,sNoiDung,sTenPhuLuc,FK_iMaPhong_CT,FK_iMaPhong_PH,FK_iMaCB_LanhDao,iFile,FK_iMaLTL,FK_iMaCB_Nhap');
        $this->db->order_by('stt','asc');
        $this->db->order_by('PK_iMaTaiLieu','asc');
        return $this->db->get('tbl_tailieuphucvuhop')->result_array();
    }
    // đếm tổng tài liệu
    public function demDSTaiLieu($khoa=NULL,$nhiemky=NULL,$kyhop=NULL,$dinhkybatthuong=NULL,$tenphuluc=NULL,$trangthaitailieu=NULL,$noidungtailieu=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongchutri=NULL,$loaitailieu=NULL)
    {
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($dinhkybatthuong))
        {
            $this->db->where('iTrangThaiKyHop',$dinhkybatthuong);
        }
        if(!empty($tenphuluc))
        {
            $this->db->where('sTenPhuLuc',$tenphuluc);
        }
        if(!empty($trangthaitailieu))
        {
            $this->db->where('iTrangThaiTaiLieu',$trangthaitailieu);
        }
        if(!empty($noidungtailieu))
        {
            $this->db->like('sNoiDung',$noidungtailieu);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('FK_iMaLV',$linhvuc);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        if(!empty($phongchutri))
        {
            $this->db->where('FK_iMaPhong_CT',$phongchutri);
        }
        if(!empty($loaitailieu))
        {
            $this->db->where('FK_iMaLTL',$loaitailieu);
        }
        $this->db->select('PK_iMaTaiLieu');
        return $this->db->get('tbl_tailieuphucvuhop')->num_rows();
    }


}

/* End of file Mtailieuphucvuhop.php */
/* Location: ./application/models/tailieuphucvuhop/Mtailieuphucvuhop.php */