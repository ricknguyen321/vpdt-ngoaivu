<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvanban_caphai extends CI_Model {
	// Danh sách tổng thể chi cục cấp 2
	// đếm danh sách 
	public function demVBDen_caphai($donvi,$loaivanban=NULL,$noiden=NULL,$kyhieu=NULL,$ngayky=NULL,$ngaynhaptu=NULL,$ngaynhapden=NULL,$trichyeu=NULL,$ngaymoitu=NULL,$ngaymoiden=NULL,$nguoiky=NULL,$chucvu=NULL,$soden=NULL,$nguoinhap=NULL)
	{
		$this->db->where('FK_iMaDV',$donvi);
		if(!empty($loaivanban))
		{
			$this->db->where('sTenLVB',$loaivanban);
		}
		if(!empty($noiden))
		{
			$this->db->where('sNoiGuiDen',$noiden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($ngaynhaptu))
		{
			$this->db->where('sNgayNhap <=',$ngaynhaptu);
		}
		if(!empty($ngaynhapden))
		{
			$this->db->where('sNgayNhap >=',$ngaynhapden);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($ngaymoitu))
		{
			$this->db->where('sNgayMoi <=',$ngaymoitu);
		}
		if(!empty($ngaymoiden))
		{
			$this->db->where('sNgayMoi >=',$ngaymoiden);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('sNguoiKy',$nguoiky);
		}
		if(!empty($chucvu))
		{
			$this->db->where('sChucVu',$chucvu);
		}
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB',$nguoinhap);
		}
		$this->db->select('PK_iMaVB');
		return $this->db->get('tbl_vanbanden_caphai')->num_rows();
	}
	// lấy danh sách 
	public function layVBDen_caphai($donvi,$loaivanban=NULL,$noiden=NULL,$kyhieu=NULL,$ngayky=NULL,$ngaynhaptu=NULL,$ngaynhapden=NULL,$trichyeu=NULL,$ngaymoitu=NULL,$ngaymoiden=NULL,$nguoiky=NULL,$chucvu=NULL,$soden=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaDV',$donvi);
		if(!empty($loaivanban))
		{
			$this->db->where('sTenLVB',$loaivanban);
		}
		if(!empty($noiden))
		{
			$this->db->where('sNoiGuiDen',$noiden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($ngaynhaptu))
		{
			$this->db->where('sNgayNhap <=',$ngaynhaptu);
		}
		if(!empty($ngaynhapden))
		{
			$this->db->where('sNgayNhap >=',$ngaynhapden);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($ngaymoitu))
		{
			$this->db->where('sNgayMoi <=',$ngaymoitu);
		}
		if(!empty($ngaymoiden))
		{
			$this->db->where('sNgayMoi >=',$ngaymoiden);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('sNguoiKy',$nguoiky);
		}
		if(!empty($chucvu))
		{
			$this->db->where('sChucVu',$chucvu);
		}
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB',$nguoinhap);
		}
		$this->db->select('PK_iMaVB,sTenLVB,sNoiGuiDen,sGioMoi,sNgayMoi,sDiaDiem,iSoDen,sMoTa,sKyHieu,sNoiDung,FK_iMaCB,sNgayNhap,FK_iMaPhong_CT,iGiayMoi');
		$this->db->order_by('PK_iMaVB','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get('tbl_vanbanden_caphai')->result_array();
	}
	// ==================================================================================================================
	// lấy danh sách của phòng 
	public function layDSVB_Phong($donvi,$phongban,$ct_ph,$loaivanban=NULL,$noiden=NULL,$kyhieu=NULL,$ngayky=NULL,$ngaynhaptu=NULL,$ngaynhapden=NULL,$trichyeu=NULL,$ngaymoitu=NULL,$ngaymoiden=NULL,$nguoiky=NULL,$chucvu=NULL,$soden=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('vb.FK_iMaDV',$donvi);
		if(!empty($loaivanban))
		{
			$this->db->where('sTenLVB',$loaivanban);
		}
		if(!empty($noiden))
		{
			$this->db->where('sNoiGuiDen',$noiden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($ngaynhaptu))
		{
			$this->db->where('sNgayNhap <=',$ngaynhaptu);
		}
		if(!empty($ngaynhapden))
		{
			$this->db->where('sNgayNhap >=',$ngaynhapden);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($ngaymoitu))
		{
			$this->db->where('sNgayMoi <=',$ngaymoitu);
		}
		if(!empty($ngaymoiden))
		{
			$this->db->where('sNgayMoi >=',$ngaymoiden);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('sNguoiKy',$nguoiky);
		}
		if(!empty($chucvu))
		{
			$this->db->where('sChucVu',$chucvu);
		}
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		$this->db->where('phongban',$phongban);
		$this->db->where('CT_PH',$ct_ph);
		$this->db->where('nguoinhan',0);
		$this->db->where('tralai',1);
		$this->db->select('PK_iMaVB,sTenLVB,sNoiGuiDen,sGioMoi,sNgayMoi,sDiaDiem,iSoDen,sMoTa,sKyHieu,vb.sNoiDung,FK_iMaCB,sNgayNhap,FK_iMaPhong_CT,iGiayMoi');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('PK_iMaVB','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm danh sách của phòng 
	public function demDSVB_Phong($donvi,$phongban,$ct_ph,$loaivanban=NULL,$noiden=NULL,$kyhieu=NULL,$ngayky=NULL,$ngaynhaptu=NULL,$ngaynhapden=NULL,$trichyeu=NULL,$ngaymoitu=NULL,$ngaymoiden=NULL,$nguoiky=NULL,$chucvu=NULL,$soden=NULL)
	{
		$this->db->where('vb.FK_iMaDV',$donvi);
		if(!empty($loaivanban))
		{
			$this->db->where('sTenLVB',$loaivanban);
		}
		if(!empty($noiden))
		{
			$this->db->where('sNoiGuiDen',$noiden);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('sKyHieu',$kyhieu);
		}
		if(!empty($ngaynhaptu))
		{
			$this->db->where('sNgayNhap <=',$ngaynhaptu);
		}
		if(!empty($ngaynhapden))
		{
			$this->db->where('sNgayNhap >=',$ngaynhapden);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($ngaymoitu))
		{
			$this->db->where('sNgayMoi <=',$ngaymoitu);
		}
		if(!empty($ngaymoiden))
		{
			$this->db->where('sNgayMoi >=',$ngaymoiden);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('sNguoiKy',$nguoiky);
		}
		if(!empty($chucvu))
		{
			$this->db->where('sChucVu',$chucvu);
		}
		if(!empty($soden))
		{
			$this->db->where('iSoDen',$soden);
		}
		$this->db->where('phongban',$phongban);
		$this->db->where('CT_PH',$ct_ph);
		$this->db->where('nguoinhan',0);
		$this->db->where('tralai',1);
		$this->db->select('PK_iMaVB,sTenLVB,sNoiGuiDen,sGioMoi,sNgayMoi,sDiaDiem,iSoDen,sMoTa,sKyHieu,vb.sNoiDung,FK_iMaCB,sNgayNhap,FK_iMaPhong_CT,iGiayMoi');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		return $this->db->get()->num_rows();
	}
	// ==========================================================================================================
	// lấy thông tin văn bản dựa trên số đến và đơn vị
	public function layTT_soDen($donvi,$soden)
	{
		$this->db->where('FK_iMaDV',$donvi);
		$this->db->where('iSoDen',$soden);
		$this->db->select('PK_iMaVB');
		return $this->db->get('tbl_vanbanden_caphai')->result_array();
	}
	// cập nhật trạng thái duyệt
	public function capnhatTrangThai_KQ($mavanban,$taikhoan,$ct_ph=NULL,$trangthai=NULL,$data)
	{
		if(!empty($ct_ph))
		{
			$this->db->where('CT_PH',$ct_ph);
		}
		if(!empty($trangthai))
		{
			$this->db->where('iTrangThai',$trangthai);
		}
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('FK_iMaCB_Duyet',$taikhoan);
		$this->db->update('tbl_file_caphai',$data);
		return $this->db->affected_rows();
	}
	// lấy văn bản hoàn thành chờ duyệt
	public function layDSVB_ChoDuyet($taikhoan)
	{
		$this->db->where('FK_iMaCB_Duyet',$taikhoan);
		$this->db->where('CT_PH',2);
		$this->db->where('f.iTrangThai',2);
		$this->db->where('f.iTrangThai_Duyet',1);
		$this->db->select('PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sHan_ThongKe,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,f.FK_iMaCB,sDuongDan as duongdan,sThoiGian,f.sNoiDung');
		$this->db->from('tbl_file_caphai as f');
		$this->db->join('tbl_vanbanden_caphai as vb','vb.PK_iMaVB=f.FK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->result_array();
	}
	// đếm văn bản hoàn thành chờ duyệt
	public function demDSVB_ChoDuyet($taikhoan)
	{ 
		$this->db->where('FK_iMaCB_Duyet',$taikhoan);
		$this->db->where('CT_PH',2);
		$this->db->where('f.iTrangThai',2);
		$this->db->where('f.iTrangThai_Duyet',1);
		$this->db->select('PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sHan_ThongKe,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,f.FK_iMaCB,sDuongDan as duongdan,sThoiGian,f.sNoiDung');
		$this->db->from('tbl_file_caphai as f');
		$this->db->join('tbl_vanbanden_caphai as vb','vb.PK_iMaVB=f.FK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->num_rows();
	}
	// lấy file văn bản cấp hai
	public function layFielVB_caphai($mavanban,$ct_ph=NULL,$trangthai=NULL){
		$this->db->where('FK_iMaVB',$mavanban);
		if(!empty($ct_ph))
		{
			$this->db->where('CT_PH',$ct_ph);
		}
		if(!empty($trangthai))
		{
			$this->db->where('iTrangThai',$trangthai);
		}
		$this->db->select('PK_iMaFile,sTenFile,FK_iMaCB,phong,sDuongDan,sNoiDung,sThoiGian');
		$this->db->order_by('PK_iMaFile','desc');
		return $this->db->get('tbl_file_caphai')->result_array();
	}
	// lấy thông tin chuyền nhận
	public function layTT_ChuyenNhan($mavanban,$nguoinhan)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('FK_iMaCB_Nhan',$nguoinhan);
		// $this->db->where('iTrangThai',1);
		$this->db->select('FK_iMaCB_Gui,iTrangThai,FK_iMaCB_Nhan,CT_PH,phongban,FK_iMaVB,nguoinhan');
		return $this->db->get('tbl_chuyennhan_caphai')->row_array();
	}
	// lấy chi tiết văn bản
	public function layChiTietVB($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->select('PK_iMaVB,sTenLVB,sTenLV,sTenKV,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,sHanGiaiQuyet,sHan_ThongKe,sNgayGiaiQuyet,iTrangThai,sNgayNhap,sNguoiKy,sChucVu,sNgayKy,sNgayNhan,sNguoiChuTri,iSoTrang,iGiayMoi,sGioMoi,sNgayMoi,sDiaDiem');
		return $this->db->get('tbl_vanbanden_caphai')->row_array();
	}
	// lấy cá bộ được chuyển phối hợp
	public function layCB_PH_Chuyen($mavanban,$taikhoan){
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('FK_iMaCB_Gui',$taikhoan);
		$this->db->where('nguoinhan',2);
		$this->db->select('FK_iMaCB_Nhan,sNoiDung');
		return $this->db->get('tbl_chuyennhan_caphai')->result_array();
	}

	// cập nhật dữ liệu văn bản cấp hai phối hợp
	public function capnhatVB_PH($mavanban,$phongban,$data)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('FK_iMaPB',$phongban);
		$this->db->update('tbl_vanbanden_caphai_ph',$data);
	}
	// lấy văn bản đã chuyển phòng chưa giải quyết
	public function layDSVB_Phong_PH_ChuaGiaiQuyet($donvi,$taikhoan,$nguoinhan=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('CT_PH',3);
		$this->db->where('cn.iTrangThai',2);
		$this->db->where('ph.sNgayGiaiQuyet','0000-00-00');
		$this->db->where('tralai',1);
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',1);
		}
		$this->db->where('cn.phongban=ph.FK_iMaPB');
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,ph.FK_iMaCB_PP_CT,ph.FK_iMaCB_PP_PH,ph.sGoiY_PP_CT,ph.sGoiY_PP_PH,ph.FK_iMaCB_CV_CT,ph.FK_iMaCB_CV_PH,ph.sGoiY_CV_CT,ph.sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_vanbanden_caphai_ph as ph','cn.FK_iMaVB=ph.FK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm văn bản đã chuyển phòng chưa giải quyết
	public function demDSVB_Phong_PH_ChuaGiaiQuyet($donvi,$taikhoan,$nguoinhan=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('CT_PH',3);
		$this->db->where('cn.iTrangThai',2);
		$this->db->where('ph.sNgayGiaiQuyet','0000-00-00');
		$this->db->where('tralai',1);
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',1);
		}
		$this->db->where('cn.phongban=ph.FK_iMaPB');
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,ph.FK_iMaCB_PP_CT,ph.FK_iMaCB_PP_PH,ph.sGoiY_PP_CT,ph.sGoiY_PP_PH,ph.FK_iMaCB_CV_CT,ph.FK_iMaCB_CV_PH,ph.sGoiY_CV_CT,ph.sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_vanbanden_caphai_ph as ph','cn.FK_iMaVB=ph.FK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->num_rows();
	}
	// lấy văn bản chờ xử lý phòng phối hợp
	public function layDSVB_Phong_PH($donvi,$taikhoan=NULL,$CT_PH=NULL,$trangthai=NULL,$tralai=NULL,$phongban=NULL,$nguoinhan=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		if(!empty($taikhoan))
		{
			$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		}
		if(!empty($CT_PH))
		{
			$this->db->where('CT_PH',$CT_PH);
		}
		if(!empty($trangthai))
		{
			$this->db->where('cn.iTrangThai',$trangthai);
		}
		if(!empty($tralai))
		{
			$this->db->where('tralai',$tralai);
		}
		if(!empty($phongban))
		{
			$this->db->where('phongban',$phongban);
		}
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',$nguoinhan);
		}
		$this->db->where('cn.phongban=ph.FK_iMaPB');
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,ph.FK_iMaCB_PP_CT,ph.FK_iMaCB_PP_PH,ph.sGoiY_PP_CT,ph.sGoiY_PP_PH,ph.FK_iMaCB_CV_CT,ph.FK_iMaCB_CV_PH,ph.sGoiY_CV_CT,ph.sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_vanbanden_caphai_ph as ph','cn.FK_iMaVB=ph.FK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// lấy văn bản chờ xử lý phòng phối hợp
	public function demDSVB_Phong_PH($donvi,$taikhoan=NULL,$CT_PH=NULL,$trangthai=NULL,$tralai=NULL,$phongban=NULL,$nguoinhan=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		if(!empty($taikhoan))
		{
			$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		}
		if(!empty($CT_PH))
		{
			$this->db->where('CT_PH',$CT_PH);
		}
		if(!empty($trangthai))
		{
			$this->db->where('cn.iTrangThai',$trangthai);
		}
		if(!empty($tralai))
		{
			$this->db->where('tralai',$tralai);
		}
		if(!empty($phongban))
		{
			$this->db->where('phongban',$phongban);
		}
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',$nguoinhan);
		}
		$this->db->where('cn.phongban=ph.FK_iMaPB');
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,ph.FK_iMaCB_PP_CT,ph.FK_iMaCB_PP_PH,ph.sGoiY_PP_CT,ph.sGoiY_PP_PH,ph.FK_iMaCB_CV_CT,ph.FK_iMaCB_CV_PH,ph.sGoiY_CV_CT,ph.sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->join('tbl_vanbanden_caphai_ph as ph','cn.FK_iMaVB=ph.FK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->num_rows();
	}
	//=========================================== END PHÒNG PHỐI HỢP
	// lấy mã chuyền nhận
	public function layMaChuyenNhan($mavanban,$taikhoan)
    {
        $this->db->where('FK_iMaVB',$mavanban);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',2);
        $this->db->where('tralai',1);
        $this->db->select('PK_iMaCN');
        return $this->db->get('tbl_chuyennhan_caphai')->row_array();
    }
    // xóa chuyền nhận
    public function xoaChuyenNhan($mavanban,$machuyennhan,$phongban=NULL,$nguoigui=NULL)
    {
    	$this->db->where('FK_iMaVB',$mavanban);
    	$this->db->where('PK_iMaCN >',$machuyennhan);
    	if(!empty($phongban))
    	{
    		$this->db->where('phongban',$phongban);
    	}
    	if(!empty($nguoigui))
    	{
    		$this->db->where('FK_iMaCB_Gui',$nguoigui);
    	}
    	$this->db->where('tralai',1);
    	$this->db->delete('tbl_chuyennhan_caphai');
    }
    // lấy quá trình chuyền nhận
    public function layQuaTrinhChuyenNhan($mavanban,$ct_ph=NULL,$nguoinhan=NULL,$phongban=NULL)
    {
    	$this->db->where('sNoiDung !=','');
    	$this->db->where('FK_iMaVB',$mavanban);
    	$this->db->where_in('CT_PH',$ct_ph);
		$this->db->where_in('nguoinhan',$nguoinhan);
		if(!empty($phongban))
		{
			$this->db->where_in('phongban',$phongban);
		}
		$this->db->select('FK_iMaCB_Gui,sNoiDung,FK_iMaCB_Nhan,sHanGiaiQuyet,sThoiGian,nguoinhan');
		return $this->db->get('tbl_chuyennhan_caphai')->result_array();
    }
	// lấy chuyền nhận
	public function layChuyenNhan($mavanban,$ct_ph,$nguoinhan,$phong_caphai=NULL)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where_in('CT_PH',$ct_ph);
		$this->db->where_in('nguoinhan',$nguoinhan);
		if(!empty($phong_caphai))
		{
			$this->db->where_in('phongban',$phong_caphai);
		}
		$this->db->select('FK_iMaCB_Nhan');
		return $this->db->get('tbl_chuyennhan_caphai')->result_array();
	}
	// cập nhât trạng thái người chuyển
    public function capnhatTrangThaiNguoiChuyen($mavanban,$taikhoan)
    {
        $this->db->where('FK_iMaVB',$mavanban);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->set('iTrangThai',2);
        $this->db->update('tbl_chuyennhan_caphai');
    }
    // lấy danh sách cán bộ trong phòng khác trưởng phòng
	public function layCB_Phong($taikhoan,$donvi,$quyenhan_caphai=NULL,$phong_caphai=NULL)
	{
		$this->db->where('PK_iMaCB !=',$taikhoan);
		$this->db->where('FK_iMaPhongHD',$donvi);
		$this->db->where('iTrangThai',0);
		$this->db->where('donvi_caphai',1);
		if(!empty($quyenhan_caphai))
		{
			$this->db->where('quyenhan_caphai',$quyenhan_caphai);
		}
		if(!empty($phong_caphai))
		{
			$this->db->where('phong_caphai',$phong_caphai);
		}
		$this->db->select('PK_iMaCB,sHoTen,tendinhdanh');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy lãnh đạo theo đơn vị
	public function layCB($donvi,$quyenhan_caphai=NULL,$phong_caphai=NULL)
	{
		$this->db->where('FK_iMaPhongHD',$donvi);
		$this->db->where('iTrangThai',0);
		$this->db->where('donvi_caphai',1);
		if(!empty($quyenhan_caphai))
		{
			$this->db->where('quyenhan_caphai',$quyenhan_caphai);
		}
		if(!empty($phong_caphai))
		{
			$this->db->where('phong_caphai',$phong_caphai);
		}
		$this->db->select('PK_iMaCB,sHoTen,tendinhdanh');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy văn bản phỏng chưa giải quyết
	public function layDSVB_Phong_ChuaGiaiQuyet($donvi,$taikhoan,$nguoinhan=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('CT_PH',2);
		$this->db->where('cn.iTrangThai',2);
		$this->db->where('sNgayGiaiQuyet','0000-00-00');
		$this->db->where('tralai',1);
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',1);
		}
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,sGoiY_PP_CT,sGoiY_PP_PH,sGoiY_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_CV_CT,sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm văn bản phỏng chưa giải quyết
	public function demDSVB_Phong_ChuaGiaiQuyet($donvi,$taikhoan,$nguoinhan=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('CT_PH',2);
		$this->db->where('cn.iTrangThai',2);
		$this->db->where('sNgayGiaiQuyet','0000-00-00');
		$this->db->where('tralai',1);
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',1);
		}
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,sGoiY_PP_CT,sGoiY_PP_PH,sGoiY_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_CV_CT,sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->num_rows();
	}
	//lấy văn bản đã chuyển phòng chưa  xử lý: Lãnh đạo vào lãnh đạo phó
	public function layDSVB_Phong_ChuaXuLy($donvi,$taikhoan,$limit=NULL,$offset=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('CT_PH',1);
		$this->db->where('cn.iTrangThai',2);
		$this->db->where('FK_iMaCB_PP_CT',0);
		$this->db->where('FK_iMaCB_CV_CT',0);
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,FK_iMaCB_LanhDao_Pho,sGoiY_LanhDao_Pho,FK_iMaPhong_CT,FK_iMaPhong_PH,sGoiY_Phong_CT,sGoiY_Phong_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	//đếm văn bản đã chuyển phòng chưa  xử lý: Lãnh đạo vào lãnh đạo phó
	public function demDSVB_Phong_ChuaXuLy($donvi,$taikhoan)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('CT_PH',1);
		$this->db->where('cn.iTrangThai',2);
		$this->db->where('FK_iMaCB_PP_CT',0);
		$this->db->where('FK_iMaCB_CV_CT',0);
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,FK_iMaCB_LanhDao_Pho,sGoiY_LanhDao_Pho,FK_iMaPhong_CT,FK_iMaPhong_PH,sGoiY_Phong_CT,sGoiY_Phong_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->num_rows();
	}
	// lấy văn bản phòng chủ trì
	public function layDSVB_Phong_CT($donvi,$taikhoan=NULL,$CT_PH=NULL,$trangthai=NULL,$tralai=NULL,$phongban=NULL,$nguoinhan=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		if(!empty($taikhoan))
		{
			$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		}
		if(!empty($CT_PH))
		{
			$this->db->where('CT_PH',$CT_PH);
		}
		if(!empty($trangthai))
		{
			$this->db->where('cn.iTrangThai',$trangthai);
		}
		if(!empty($tralai))
		{
			$this->db->where('tralai',$tralai);
		}
		if(!empty($phongban))
		{
			$this->db->where('phongban',$phongban);
		}
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',$nguoinhan);
		}
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,CT_PH,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,FK_iMaCB_LanhDao_Pho,sGoiY_LanhDao_Pho,FK_iMaPhong_CT,FK_iMaPhong_PH,sGoiY_Phong_CT,sGoiY_Phong_PH,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,sGoiY_PP_CT,sGoiY_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_CV_CT,sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('sNgayNhap','desc');
		$this->db->order_by('sThoiGian','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm văn bản phòng chủ trì
	public function demDSVB_Phong_CT($donvi,$taikhoan=NULL,$CT_PH=NULL,$trangthai=NULL,$tralai=NULL,$phongban=NULL,$nguoinhan=NULL)
	{
		$this->db->where('cn.FK_iMaDV',$donvi);
		if(!empty($taikhoan))
		{
			$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		}
		if(!empty($CT_PH))
		{
			$this->db->where('CT_PH',$CT_PH);
		}
		if(!empty($trangthai))
		{
			$this->db->where('cn.iTrangThai',$trangthai);
		}
		if(!empty($tralai))
		{
			$this->db->where('tralai',$tralai);
		}
		if(!empty($phongban))
		{
			$this->db->where('phongban',$phongban);
		}
		if(!empty($nguoinhan))
		{
			$this->db->where('nguoinhan',$nguoinhan);
		}
		$this->db->select('cn.FK_iMaCB_Gui,cn.sNoiDung,cn.sHanGiaiQuyet,CT_PH,PK_iMaVB,vb.sNoiDung as noidungvanban,sGioMoi,sNgayMoi,sDiaDiem,iGiayMoi,sNgayNhap,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,FK_iMaCB_LanhDao_Pho,sGoiY_LanhDao_Pho,FK_iMaPhong_CT,FK_iMaPhong_PH,sGoiY_Phong_CT,sGoiY_Phong_PH,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,sGoiY_PP_CT,sGoiY_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_CV_CT,sGoiY_CV_PH');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->num_rows();
	}

}

/* End of file Mvanban_caphai.php */
/* Location: ./application/models/vanbanden_caphai/Mvanban_caphai.php */