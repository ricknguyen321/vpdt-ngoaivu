<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvanbanchoxuly extends CI_Model {

	// lấy file cuối 
	public function layFile($mavanban)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('CT_PH',1);
		$this->db->where('iTrangThai',1);
		$this->db->where('sDuongDan !=','');
		$this->db->select('PK_iMaFile,sTenFile,FK_iMaCB,sThoiGian,sDuongDan');
		$this->db->order_by('PK_iMaFile','desc');
		return $this->db->get('tbl_file_caphai')->result_array();
	}
	// kiểm tra đơn vị cấp 2
	public function kiemtraDV($mavanban,$donvi)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->where('FK_iMaDV',$donvi);
		return $this->db->get('tbl_vanbanden_caphai')->num_rows();
	}
	// kiểm tra xem có quyền ghi kết quả hay lý do chậm muộn
	public function kiemtraTaiKhoan($mavanban,$taikoan)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('FK_iMaCB_Nhan',$taikoan);
		$this->db->where('tralai',1);
		$this->db->select('iTrangThai');
		return $this->db->get('tbl_chuyennhan_caphai')->row_array();
	}
	// lấy file văn bản cấp hai chủ trì
	public function layFileVB($mavanban,$chutri_phoihop,$trangthai)
	{
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->where('CT_PH',$chutri_phoihop);
		$this->db->where('iTrangThai',$trangthai);
		$this->db->select('sTenFile,PK_iMaFile,sDuongDan,sThoiGian,FK_iMaCB,sNoiDung');
		return $this->db->get('tbl_file_caphai')->result_array();
	}

	// lấy thông tin văn bản
	public function layTTVB($mavanban)
	{
		$this->db->where('PK_iMaVB',$mavanban);
		$this->db->select('PK_iMaVB,sTenLVB,sTenLV,sTenKV,sNoiGuiDen,iSoDen,sKyHieu,sMoTa,sHanGiaiQuyet,sNgayGiaiQuyet,sNguoiKy,sChucVu,sNgayKy,iSoTrang,sNgayNhan');
		return $this->db->get('tbl_vanbanden_caphai')->row_array();
	}
	// lấy quá trình chuyền nhận
	public function quatrinhchuyennhan($mavanban,$chutri_phoihop)// chutri_phoihop 1: là chủ trì ngược lại là phối hợp
	{
		$this->db->where('FK_iMaVB',$mavanban);
		if($chutri_phoihop==1)
		{ 
			$chutri = array(1,2);
			$this->db->where_in('CT_PH',$chutri);
		}
		else
		{
			$phoihop = array(1,3);
			$this->db->where_in('CT_PH',$phoihop);
		}
		$this->db->where('tralai',1);
		$this->db->select('FK_iMaCB_Nhan');
		return $this->db->get('tbl_chuyennhan_caphai')->result_array();
	}
	public function quatrinhchuyennhan2($mavanban,$chutri_phoihop)// chutri_phoihop 1: là chủ trì ngược lại là phối hợp
	{
		$this->db->where('FK_iMaVB',$mavanban);
		if($chutri_phoihop==1)
		{ 
			$chutri = array(1,2);
			$this->db->where_in('CT_PH',$chutri);
		}
		else
		{
			$phoihop = array(1,3);
			$this->db->where_in('CT_PH',$phoihop);
		}
		$this->db->where('sNoiDung !=','');
		$this->db->where('tralai',1);
		$this->db->select('FK_iMaCB_Nhan,FK_iMaCB_Gui,sNoiDung,sHanGiaiQuyet,sThoiGian');
		return $this->db->get('tbl_chuyennhan_caphai')->result_array();
	}
	// cập nhật bảng chuyền nhận nhiều điều kiện
	public function capnhatTrangThai($taikoan,$mavanban,$data)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikoan);
		$this->db->where('FK_iMaVB',$mavanban);
		$this->db->update('tbl_chuyennhan_caphai',$data);
		return $this->db->affected_rows();
	}
	// lấy ds phó chi cục
	public function layCB($donvi,$quyen=NULL,$chucvu=NULL,$quyendb=NULL)
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('FK_iMaPhongHD',$donvi);
		if(!empty($quyen))
		{
			$this->db->where('iQuyenHan_DHNB',$quyen);
		}
		if(!empty($chucvu))
		{
			$this->db->where('FK_iMaCV',$chucvu);
		}
		if(!empty($quyendb))
		{
			$this->db->where('iQuyenDB',$quyendb);
		}
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// danh sách văn ban chờ xử lý chủ trì
	public function layVBChoXuLy($taikhoan,$chutri_phoihop,$giaymoi,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('cn.iTrangThai',1);
		$this->db->where('cn.tralai',1);// 1:vb thường 2:vb giấy mời
		$this->db->where('iGiayMoi',$giaymoi);
		if($chutri_phoihop==3)// phối hơp
		{
			$this->db->where('CT_PH',3);
		}
		else{//chủ trì
			$this->db->where('CT_PH <',3);
		}
		$this->db->select('PK_iMaVB,sMoTa,sGioMoi,sNgayMoi,sDiaDiem,sNgayNhap,vb.sNoiDung,sKyHieu,iSoDen,sNoiGuiDen,cn.sNoiDung as ykien,cn.sHanGiaiQuyet,FK_iMaPhong_CT,FK_iMaPhong_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_Phong_CT');
		$this->db->from('tbl_vanbanden_caphai as vb');
		$this->db->join('tbl_chuyennhan_caphai as cn','cn.FK_iMaVB=vb.PK_iMaVB');
		$this->db->order_by('cn.sThoiGian','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();

	}
	// lấy người nhập văn bản
	public function layNguoiNhap($donvi)
	{
		$this->db->where('FK_iMaDV',$donvi);
		$this->db->select('FK_iMaCB');
		$this->db->group_by('FK_iMaCB');
		return $this->db->get('tbl_vanbanden_caphai')->result_array();
	}
	// ==================================================================================================================
	
}

/* End of file Mvanbanchoxuly.php */
/* Location: ./application/models/vanbanden_caphai/Mvanbanchoxuly.php */