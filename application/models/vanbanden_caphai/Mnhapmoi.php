<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnhapmoi extends CI_Model {


	// lấy văn thư
	public function layCB($donvi,$quyenhan_caphai=NULL)
	{
		$this->db->where('FK_iMaPhongHD',$donvi);
		if(!empty($quyenhan_caphai))
		{
			$this->db->where('quyenhan_caphai',$quyenhan_caphai);
		}
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy lãnh đạo( chi cục trưởng hoặc giám đốc)
	public function layLanhDao($donvi)
	{
		$this->db->where('FK_iMaPhongHD',$donvi);
		$this->db->where('quyenhan_caphai',1);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->row_array();
	}
	// lấy số đến mới nhất
	public function laySoDen($donvi,$nam)
	{
		$this->db->where('FK_iMaDV',$donvi);
		$this->db->where('iNam',$nam);
		$this->db->select('iSoDen');
		$this->db->order_by('iSoDen','desc');
		return $this->db->get('tbl_vanbanden_caphai')->row_array();
	}

}

/* End of file Mnhapmoi.php */
/* Location: ./application/models/vanbanden_caphai/Mnhapmoi.php */