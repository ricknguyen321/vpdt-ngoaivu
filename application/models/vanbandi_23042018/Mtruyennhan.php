<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtruyennhan extends CI_Model {
	// văn bản đến chời xử lý
	public function demVanBanChoXuLy($taikhoan)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('iSoVBDi',0);
		$this->db->where('trangthai_chuyen <',8);
		$this->db->where('nguoi_nhan',$taikhoan);
		$this->db->or_where('nhan_debiet',$taikhoan);
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		$this->db->group_by('PK_iMaVBDi');
		return $this->db->get()->num_rows();
	}
	// văn bản đến đã xử lý
	public function demVBDiDaXuLy($taikhoan,$trangthai)
	{
		if($trangthai==1)
		{
			$this->db->where('FK_iMaCB_Nhap',$taikhoan);
		}
		else
		{
			$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		}
		$this->db->where('trangthai_chuyen >',$trangthai);
		$this->db->from('tbl_luuvet_vbdi as lv');
		$this->db->join('tbl_vanbandi as vb','vb.PK_iMaVBDi=lv.FK_iMaVBDi');
		$this->db->group_by('FK_iMaVBDi');
		return $this->db->get()->num_rows();
	}
	//======================================== lấy văn bản đã được lãnh đạo duyệt
	public function layvanbandaduyet($phongban)
	{
		$this->db->where('FK_iMaPB',$phongban);
		$this->db->where('trangthai_chuyen',8);
		$this->db->select('PK_iMaVBDi,sMoTa,sKyHieu,ykien_chuyen,iFile,thoigian_chuyen,sHoTen,FK_iMaCB_Nhap,sNoiNhan,nguoiduyet');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		$this->db->group_by('PK_iMaVBDi');
		return $this->db->get()->result_array();
	}
	// đếm văn bản đã được lãnh đạo duyệt theo phòng
	public function demvanbandaduyet($phongban)
	{
		$this->db->where('FK_iMaPB',$phongban);
		$this->db->where('trangthai_chuyen',8);
		return $this->db->get('tbl_vanbandi')->num_rows();
	}
	//======================================== ĐẾM SỐ BẢN GHI
	// lấy văn bản đi chính đã xử lý
	public function layVanBanDaXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem=NULL,$trangthai_xem=NULL,$chude=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where($ten_taikhoan,$taikhoan);// tên tài khoản nhận tùy theo chức vụ
		$this->db->where($ykien,''); // ý kiến khác rỗng là chờ xử lý
		if(!empty($trangthai_xem))// trạng thái xem phục vụ cho lãnh đạo: 1:chưa xem 2:đã xem 3: góp ý
		{
			$this->db->where($ten_trangthai_xem,$trangthai_xem);
		}
		if(!empty($chude))
		{
			$this->db->like('sMoTa',$chude);
		}
		$this->db->select('vb.PK_iMaVBDi,sMoTa,sKyHieu,sNoiNhan,sNgayNhap,iFile,sHoTen');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		$this->db->order_by('sNgayNhap','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}

	// ====================================== VĂN BẢN ĐI PHỐI HỢP
	// lấy văn bản đi phối hợp đã xử lý
	public function layVanBanDaXuLy_phoihop($taikhoan)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('iXuLy',1);
		$this->db->select('PK_iMaPH,PK_iMaVBDi,sFile,ph.sThoiGian,ph.FK_iMaCB_Gui,FK_iMaCB_Nhap,sMoTa,sKyHieu,sYKien,noidung_xuly,thoigian_xuly');
		$this->db->from('tbl_phoihop_vbdi as ph');
		$this->db->join('tbl_vanbandi as vb','vb.PK_iMaVBDi=ph.FK_iMaVBDi');
		return $this->db->get()->result_array();
	}
	// lấy quá trình chuyển đi
	public function layQTChuyen($mavanban){
		$this->db->where('FK_iMaVBDi',$mavanban);
		return $this->db->get('tbl_luuvet_vbdi')->result_array();
	}
	// lấy quá trình chuyển phối hợp
	public function layQTChuyenPH($mavanban){
		$this->db->where('FK_iMaVBDi',$mavanban);
		return $this->db->get('tbl_phoihop_vbdi')->result_array();
	}
	public function layVanBanChoXuLy_phoihop($taikhoan)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('iXuLy',0);
		$this->db->select('PK_iMaPH,PK_iMaVBDi,iFile,ph.sThoiGian,ph.FK_iMaCB_Gui,FK_iMaCB_Nhap,sMoTa,sKyHieu,sYKien');
		$this->db->from('tbl_phoihop_vbdi as ph');
		$this->db->join('tbl_vanbandi as vb','vb.PK_iMaVBDi=ph.FK_iMaVBDi');
		return $this->db->get()->result_array();
	}
	// ============================================================================================
	// lấy danh sách can bộ
	public function layDSCB()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// kiêm tra thuộc phòng nào
	public function kiemtraPhong($taikhoan)
	{
		$this->db->where('PK_iMaCB',$taikhoan);
		$this->db->select('FK_iMaPhongHD');
		return $this->db->get('tbl_canbo')->row_array();
	}
	// kiểm tra là phó phòng hay chuyên viên quyền8
	public function kiemtraQuyen($taikhoan){
		$this->db->where('PK_iMaCB',$taikhoan);
		$this->db->select('iQuyenHan_DHNB');
		return $this->db->get('tbl_canbo')->row_array();
	}
	// lây trưởng phòng
	public function layTruongPhong($taikhoan,$quyen,$chucvu)
	{
		$this->db->where('PK_iMaCB !=',$taikhoan);
		$this->db->where('PK_iMaCB !=',617);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iQuyenHan_DHNB',$quyen);
		$this->db->where_in('FK_iMaCV',$chucvu);
		$this->db->select('PK_iMaCB,sHoTen,FK_iMaPhongHD');
		$this->db->order_by('FK_iMaPhongHD','asc');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lây phó phòng
	public function layPhoPhong($phongbanHD,$quyen,$taikhoan)
	{
		$this->db->where('PK_iMaCB !=',$taikhoan);
		$this->db->where('FK_iMaPhongHD',$phongbanHD);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iQuyenHan_DHNB',$quyen);
		$this->db->select('PK_iMaCB,sHoTen,FK_iMaPhongHD');
		$this->db->order_by('FK_iMaPhongHD','asc');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy thông tin văn bản đi cần xử lý
	public function layThongTinVBDI($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('*');
		return $this->db->get('tbl_vanbandi')->row_array();
	}
	// lấy danh sách văn bản chờ xử lý
	public function layVanBanChoXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem=NULL,$trangthai_xem=NULL,$chude=NULL,$limit=NULL,$offset=NULL)
	{
		if($trangthai_xem==1)
		{
			$this->db->where($ten_trangthai_xem,$trangthai_xem);
			$this->db->where('iSoVBDi',0);
		}
		if($trangthai_xem==2)
		{
			$this->db->where('iSoVBDi >',0);
			$this->db->or_where($ten_trangthai_xem,$trangthai_xem);
		}
		$this->db->where($ten_taikhoan,$taikhoan);// tên tài khoản nhận tùy theo chức vụ
		$this->db->where($ykien.'!=',''); // ý kiến khác rỗng là chờ xử lý
		// if(!empty($trangthai_xem))// trạng thái xem phục vụ cho lãnh đạo: 1:chưa xem 2:đã xem 3: góp ý
		// {
		// 	$this->db->where($ten_trangthai_xem,$trangthai_xem);
		// }
		if(!empty($chude))
		{
			$this->db->like('sMoTa',$chude);
		}
		$this->db->select('vb.*,sHoTen');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		$this->db->order_by('sNgayNhap','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm văn bản cần sử lý
	public function demdemVanBanChoXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem=NULL,$trangthai_xem=NULL,$chude=NULL,$limit=NULL,$offset=NULL)
	{
		if($trangthai_xem==1)
		{
			$this->db->where($ten_trangthai_xem,$trangthai_xem);
			$this->db->where('iSoVBDi',0);
		}
		if($trangthai_xem==2)
		{
			$this->db->where('iSoVBDi >',0);
			$this->db->or_where($ten_trangthai_xem,$trangthai_xem);
		}
		$this->db->where($ten_taikhoan,$taikhoan);// tên tài khoản nhận tùy theo chức vụ
		$this->db->where($ykien.'!=',''); // ý kiến khác rỗng là chờ xử lý
		// if(!empty($trangthai_xem))// trạng thái xem phục vụ cho lãnh đạo: 1:chưa xem 2:đã xem 3: góp ý
		// {
		// 	$this->db->where($ten_trangthai_xem,$trangthai_xem);
		// }
		if(!empty($chude))
		{
			$this->db->like('sMoTa',$chude);
		}
		$this->db->select('sHoTen');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		$this->db->order_by('sNgayNhap','desc');
		return $this->db->get()->num_rows();
	}
	public function demdemVanBanDaXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem=NULL,$trangthai_xem=NULL,$chude=NULL,$limit=NULL,$offset=NULL)
	{
		$this->db->where($ten_taikhoan,$taikhoan);// tên tài khoản nhận tùy theo chức vụ
		$this->db->where($ykien,''); // ý kiến khác rỗng là chờ xử lý
		if(!empty($trangthai_xem))// trạng thái xem phục vụ cho lãnh đạo: 1:chưa xem 2:đã xem 3: góp ý
		{
			$this->db->where($ten_trangthai_xem,$trangthai_xem);
		}
		if(!empty($chude))
		{
			$this->db->like('sMoTa',$chude);
		}
		$this->db->select('sHoTen');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		$this->db->order_by('sNgayNhap','desc');
		return $this->db->get()->num_rows();
	}
	// cập nhật lại
	public function capnhattrangthai($mavanban,$ten_taikhoan,$taikhoan,$ten_trangthai)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->where($ten_taikhoan,$taikhoan);
		$this->db->set($ten_trangthai,2);
		$this->db->update('tbl_vanbandi');
	}
	// lấy người nhận
	public function layNguoiNhan($quyen,$chucvu=NULL,$phongban=NULL,$quyendb=NULL)
	{
		$this->db->where('iTrangThai',0);
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPhongHD',$phongban);
		}
		if(!empty($chucvu))
		{
			$this->db->where('FK_iMaCV',$chucvu);
		}
		if(!empty($quyendb))
		{
			$this->db->where('iQuyenDB',$quyendb);
		}
		$this->db->where('iQuyenHan_DHNB',$quyen);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}

}

/* End of file Mtruyennhan.php */
/* Location: ./application/models/vanbandi/Mtruyennhan.php */