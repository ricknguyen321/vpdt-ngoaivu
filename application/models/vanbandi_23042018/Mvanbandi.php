<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvanbandi extends CI_Model {
	// lấy cán bộ
	public function layCB()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy mail nhóm
	public function laynhomMail($nhom)
	{
		$this->db->where('mail_group',$nhom);
		$this->db->where('mail_cha',0);
		$this->db->where('sSDT !=','');
		$this->db->select('sSDT');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}
	// đếm danh sách văn bản và giấy mời của phòng
	public function demDSVB_Phong($phongban,$loaivanban)
	{
		$this->db->where('FK_iMaPB',$phongban);
		$this->db->where('iSoVBDi > 0');
		if($loaivanban==10)
		{
			$this->db->where('FK_iMaLVB',10);
		}
		else
		{
			$this->db->where('FK_iMaLVB !=',10);
		}
		$this->db->select('PK_iMaVBDi');
		return $this->db->get('tbl_vanbandi')->num_rows();
	}
	public function laynoinhanmail($trangthai){ // 1: quận huyện 2: sở ban ngành
		$this->db->where('iTrangThai',0);
		$this->db->where('mail_group',$trangthai);
		$this->db->select('sEmail,sTenDV');
		$this->db->order_by('mail_cha','desc');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}

	// lấy đơn vị nhận
	public function layDonViNhanMail()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaEmailDV,sTenDV');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}
	// lấy đơn vị nhận mail
	public function MailGuiDi($madonvi)
	{
		$this->db->where_in('PK_iMaEmailDV',$madonvi);
		$this->db->select('sEmail');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}
	// lấy đơn vị nhận mail qua mã văn bản đi
	public function layDVQuaMa($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('FK_iMaNoiNhan');
		return $this->db->get('tbl_vanbandi')->row_array();
	}
	// lấy thông tin văn bản theo mã
	public function layThongTinTheoMaVB($ma)
	{
		$this->db->where('PK_iMaVBDi',$ma);
		$this->db->select('sHoTen,sTenLVB,vbd.sMoTa,sKyHieu,iSoVBDi,sNgayVBDi,vbd.sNoiNhan,cv.sTenCV,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('tbl_vanbandi as vbd');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vbd.FK_iMaCB_Ky');
		$this->db->join('tbl_chucvu as cv','cv.PK_iMaCV=cb.FK_iMaCV');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB=vbd.FK_iMaLVB');
		return $this->db->get()->row_array();
	}
	// lấy văn bản chờ số
	public function layVanBanChoSo()
	{
		$this->db->where('iSoVBDi',0);
		return $this->db->get('tbl_vanbandi')->result_array();
	}
	// lấy mã văn bản theo số đến
	public function layMaVBDen($soden)
	{
		$this->db->where('iGiayMoi',2);
		$this->db->where_in('iSoDen',$soden);
		$this->db->select('iSoDen,PK_iMaVBDen,sKyHieu,sMoTa,sTenDV,sTenNguoiKy');
		return $this->db->get('tbl_vanbanden')->result_array();
	}
	// lấy nơi dự thảo  với loại phần mềm là điều hành nội bộ
	public function layPhongBanDuThao()
	{
		$phanmem = array(1,3);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iLoaiPhanMem',$phanmem);
		$this->db->select('sTenPB,PK_iMaPB');
		$this->db->order_by('sTenPB');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// lấy người ký cho nhật mới văn bản đi với quyền là 3:chánh văn phòng 4:giám đốc 5:phó giám đốc
	public function layNguoiKy()
	{
		$quyen = array(3,4,5);
		$this->db->where('FK_iMaCV !=',5);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iQuyenHan_DHNB',$quyen);
		$this->db->select('sHoTen,PK_iMaCB,iQuyenHan_DHNB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy chức vụ người ký
	public function layChucVu($ma)
	{
		$this->db->where('PK_iMaCB',$ma);
		$this->db->select('PK_iMaCV,sTenCV');
		$this->db->from('tbl_canbo as cb');
		$this->db->join('tbl_chucvu as cv','cv.PK_iMaCV=cb.FK_iMaCV');
		return $this->db->get()->row_array();
	}
		/**
		* =============================== VĂN BẢN CHỜ SỐ =================================
		*/
	// lấy văn bản của phòng: giấy mời: 10
	public function layVBGMPHong($giaymoi,$phongban)
	{
		if($giaymoi==10)
		{
			$this->db->where('FK_iMaLVB',10);
		}
		else{
			$this->db->where('FK_iMaLVB !=',10);
		}
		$this->db->where('FK_iMaPB',$phongban);
		$this->db->where('iSoVBDi >',0);
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->result_array();
	}
	// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời
	public function layVBChoSo($mavanban=NULL,$trangthai,$giaymoi=NULL)
	{
		if(!empty($mavanban))
		{
			$this->db->where('PK_iMaVBDi',$mavanban);
		}
		if($trangthai==0)
		{
			$this->db->where('iSoVBDi',0);
		}
		else{
			$this->db->where('iSoVBDi >',0);
		}
		if(!empty($giaymoi))
		{
			if($giaymoi==10)
			{
				$this->db->where('FK_iMaLVB',10);
			}
			else{
				$this->db->where('FK_iMaLVB !=',10);
			}
		}
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->result_array();
	}
	// lấy tên người nhập
	public function layNguoiNhap($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('sHoTen');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		return $this->db->get()->row_array();
	}

	// lấy số đi mới nhất // kiểm tra phải giấy mời hay k
	public function laySoDiMoi($giaymoi)
	{
		if($giaymoi==10)
		{
			$this->db->where('FK_iMaLVB',10);
		}
		else{
			$this->db->where('FK_iMaLVB !=',10);
		}
		$this->db->select('iSoVBDi');
		$this->db->order_by('iSoVBDi','desc');
		return $this->db->get('tbl_vanbandi')->row_array();
	}
		/**
		*  ================================== Văn bản sở tài chính tham mưu
		*/
	// lấy tên phòng ban và số lượng văn bản
	public function layPBvaSLVB($tungay=NULL,$denngay=NULL,$phongban=NULL,$taikhoan)
	{
		if(!empty($tungay))
		{
			$this->db->where('sNgayVBDi >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('sNgayVBDi <=',$denngay);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		// $this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->where('iDuThao',1);
		$this->db->where('iSoVBDi >',0);
		$this->db->select('count(FK_iMaPB) as tong, FK_iMaPB,sTenPB');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->group_by('FK_iMaPB');
		return $this->db->get()->result_array();
	}
	// lấy thông tin văn bản 
	public function layThongTinVB($tungay=NULL,$denngay=NULL,$phongban=NULL,$taikhoan)
	{
		if(!empty($tungay))
		{
			$this->db->where('sNgayVBDi >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('sNgayVBDi <=',$denngay);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		// $this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->where('iDuThao',1);
		$this->db->where('iSoVBDi >',0);
		$this->db->select('FK_iMaPB,iSoVBDi, sKyHieu,sNgayVBDi,sTenLVB,vb.sMoTa,sHoTen,sTenPB');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB=vb.FK_iMaLVB');
		return $this->db->get()->result_array();
	}
		/**
		*  ================================== Lịch họp do lãnh đạo sở tài chính chủ trì
		*/
	// lấy lịch họp
	public function layLichHop2($ngaybd,$ngaykt,$sangchieu)
	{
		// $this->db->where('iSoDen >',0);
		$this->db->where('sNgayMoi >=',$ngaybd);
		$this->db->where('sNgayMoi <=',$ngaykt);
		$this->db->where('FK_iMaLVB',10);
		if($sangchieu=='sang')
		{
			$this->db->where('sGioMoi <','12:00');
		}
		else{
			$this->db->where('sGioMoi >=','12:00');
		}
		$this->db->select('PK_iMaVBDi,sHoTen,vb.sMoTa,sTenPB,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->order_by('sNgayMoi','asc');
		$this->db->order_by('sGioMoi','asc');
		return $this->db->get()->result_array();
	}
	public function layLichHop($ngaybd,$ngaykt)
	{
		// $this->db->where('iSoDen >',0);
		$this->db->where('sNgayMoi >=',$ngaybd);
		$this->db->where('sNgayMoi <=',$ngaykt);
		$this->db->where('FK_iMaLVB',10);
		$this->db->select('sHoTen,vb.sMoTa,sTenPB,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->order_by('sNgayMoi','asc');
		$this->db->order_by('sGioMoi','asc');
		return $this->db->get()->result_array();
	}// người nhập
	public function layVanThu()
	{
		$ma = array(2,9);
		$this->db->where_in('iQuyenHan_DHNB',$ma);
		$this->db->select('sHoTen,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	/**
	*  ================================== Lấy danh sách van bản với các tiêu chí tìm kiếm khác nhau
	*/
	// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời: default 1 là không phải giấy mời
	// ($mavanban=NULL,$trangthai,$giaymoi=NULL)
	public function layDSVBDi($loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$ngaythang=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$email=NULL,$duthaoubnd=NULL,$trangthai,$giaymoi=NULL,$limit=NULL,$offset=NULL)
	{
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		if(!empty($email))
		{
			if($email==1)
			{
				$this->db->where('iGuiMail',$email);
			}
			else{
				$this->db->where('iGuiMail',0);
			}
			
		}
		if(!empty($duthaoubnd))
		{
			$this->db->where('iDuThao',1);
		}
		if($trangthai==0)
		{
			$this->db->where('iSoVBDi',0);
			$this->db->where('trangthai_chuyen',8);
		}
		else{
			$this->db->where('iSoVBDi >',0);
		}
		if(!empty($giaymoi))
		{
			if($giaymoi==10)
			{
				$this->db->where('FK_iMaLVB',10);
			}
			else{
				$this->db->where('FK_iMaLVB !=',10);
			}
		}
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	public function demDSVBDi($loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$ngaythang=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$email=NULL,$duthaoubnd=NULL,$trangthai,$giaymoi=NULL)
	{
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		if(!empty($email))
		{
			if($email==1)
			{
				$this->db->where('iGuiMail',$email);
			}
			else{
				$this->db->where('iGuiMail',0);
			}
		}
		if(!empty($duthaoubnd))
		{
			$this->db->where('iDuThao',1);
		}
		if($trangthai==0)
		{
			$this->db->where('iSoVBDi',0);
			$this->db->where('trangthai_chuyen',8);
		}
		else{
			$this->db->where('iSoVBDi >',0);
		}
		if(!empty($giaymoi))
		{
			if($giaymoi==10)
			{
				$this->db->where('FK_iMaLVB',10);
			}
			else{
				$this->db->where('FK_iMaLVB !=',10);
			}
		}
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->num_rows();
	}
	// văn bản trình ký
	public function layVBTrinhKy($trangthai=NULL,$canbo,$trichyeu=NULL,$limit=NULL,$offset=NULL)
	{
		if(!empty($trangthai))
		{
			if($trangthai==1)
			{
				$this->db->where('trangthai_xem',$trangthai);
			}
			else{
				$this->db->where('trangthai_xem',2);
			}
		}
		$this->db->where('nguoi_nhan',$canbo);
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		// $this->db->where('iSoVBDi',0);
		$this->db->select('sHoTen,sTenLVB,vb.sKyHieu,vb.sMoTa,vb.sNoiNhan,vb.trangthai_xem,vb.PK_iMaVBDi,vb.iFile');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm văn bản trình ký
	public function demVBTrinhKy($trangthai=NULL,$canbo,$trichyeu=NULL)
	{
		if(!empty($trangthai))
		{
			if($trangthai==1)
			{
				$this->db->where('trangthai_xem',$trangthai);
			}
			else{
				$this->db->where('trangthai_xem',2);
			}
		}
		$this->db->where('nguoi_nhan',$canbo);
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		// $this->db->where('iSoVBDi',0);
		$this->db->select('sHoTen,sTenLVB,vb.sKyHieu,vb.sMoTa,vb.sNoiNhan,vb.trangthai_xem');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->num_rows();
	}
	// lấy file cuối cùng
	public function layFileLast($ma)
	{
		$this->db->where('FK_iMaVBDi',$ma);
		$this->db->select('sDuongDan,sTenFile');
		$this->db->from('tbl_files_vbdi');
		$this->db->order_by('PK_iMaFileDi','desc');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
}

/* End of file Mvanbandi.php */
/* Location: ./application/models/vanbandi/Mvanbandi.php */