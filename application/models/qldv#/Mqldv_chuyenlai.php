<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mqldv_chuyenlai extends CI_Model {

	// lấy người chuyển xuống
	public function layNguoiChuyen($mavanban)
	{
		$this->db->where('qlvDetails_id',$mavanban);
		$this->db->select('qlv_id,qlv_id_sub,coordinate_per,process_per,user_chicuctruong,user_chicucpho');
		return $this->db->get('tbl_qlv_details')->row_array();
	}

}

/* End of file Mqldv_chuyenlai.php */
/* Location: ./application/models/qldv/Mqldv_chuyenlai.php */