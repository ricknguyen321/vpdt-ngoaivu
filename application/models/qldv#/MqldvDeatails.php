<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MqldvDeatails extends CI_Model {

	// lấy đầu việc đã hoàn thành đúng hạn
	public function layDauViec($mavanban,$trangthai,$dunghan=NULL,$quahan=NULL,$new_date=NULL) // trang thai 1:đang thực hiên 2: đã hoàn thành 3:sắp đến hạn
	{
		$this->db->where_in('qlv_id',$mavanban);
		if($trangthai==1)
		{
			$this->db->where('qlvDetails_active <',9);// đang giải quyết
			if(!empty($dunghan))
			{
				$this->db->where('(han_thongke > "'.date('Y-m-d').'" or han_thongke <= "1970-01-01")',NULL);
			}
			if(!empty($quahan))
			{
				$this->db->where('(han_thongke < "'.date('Y-m-d').'" and han_thongke > "1970-01-01")',NULL);
			}
		}
		if($trangthai==2)
		{
			$this->db->where('qlvDetails_active',9);// đã giải quyết
			if(!empty($dunghan))
			{
				// $this->db->where('han_thongke >=','DATE(date_traloi)');
				$this->db->where('(han_thongke >= thoigian_hoanthanh or han_thongke <= "1970-01-01")',NULL);
			}
			if(!empty($quahan))
			{
				// $this->db->where('han_thongke <','DATE(date_traloi)');
				$this->db->where('(han_thongke < thoigian_hoanthanh and han_thongke > "1970-01-01")',NULL);
			}
		}
		if($trangthai==3)
		{
			$this->db->where('qlvDetails_active <',9);// sắp đến hạn
			if(!empty($new_date))
			{
				$this->db->where('han_thongke <=',$new_date);
				$this->db->where('han_thongke >=',date('Y-m-d'));	
			}
		}
		$this->db->where('qlv_id_sub >',0);
		$this->db->select('qlv_id,COUNT(qlv_id_sub) as tong');
		$this->db->group_by('qlv_id');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
	// lấy tổng đầu việc cháu theo đầu văn bản
	public function layTongDauViecChau($mavanban)
	{
		$this->db->where_in('qlv_id',$mavanban);
		$this->db->where('qlv_id_sub >',0);
		$this->db->select('qlv_id,COUNT(qlv_id_sub) as tong');
		$this->db->group_by('qlv_id');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
	// =====================================================================================================
	// lấy đầu việc đã giải quyết
	public function dauviecDaHoanThanh($taikhoan,$trangthai)
	{
		$this->db->where_in('qlvDetails_active',array(8,9));
		switch ($trangthai) {
			case 1: #GD
				$this->db->where('coordinate_per',$taikhoan);
				break;
			case 2: #PGD
				$this->db->where('process_per',$taikhoan);
				break;
			case 3: #CCT
				$this->db->where('user_chicuctruong',$taikhoan);
				break;
			case 4: #CCP
				$this->db->where('user_chicucpho',$taikhoan);
				break;
			case 5: #TP
				$this->db->where('user_truongphong',$taikhoan);
				break;
			case 6: #CV
				$this->db->where('text_photruongphong',$taikhoan);
				break;
			case 7: #CV
				$this->db->where('user_chuyenvien',$taikhoan);
				break;
			default:
				break;
		}
		$this->db->select('vb.qlvDetails_id,vb.qlv_id,qlvFile_path,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,input_per,date_traloi,qlvDetails_user_traloi,qlvDetails_traloi,noidung_danhgia,thoigian_danhgia,user_danhgia');
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_qlvfilesketqua as kq','kq.qlvDetails_id=vb.qlvDetails_id');
		$this->db->order_by('qlv_id','desc');
		$this->db->order_by('date_traloi','asc');
		return $this->db->get()->result_array();
	}
	// lấy đầu việc đã giải quyết
	public function dauviecDaGiaiQuyet($taikhoan)
	{
		$this->db->where('qlvDetails_active',8);
		$this->db->where('user_danhgia',$taikhoan);
		$this->db->select('vb.qlvDetails_id,vb.qlv_id,qlvFile_path,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,input_per,date_traloi,qlvDetails_user_traloi,qlvDetails_traloi');
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_qlvfilesketqua as kq','kq.qlvDetails_id=vb.qlvDetails_id');
		$this->db->order_by('qlv_id','desc');
		$this->db->order_by('date_traloi','asc');
		return $this->db->get()->result_array();
	}
	// lấy hạn thống kê và trạng thái
	public function layHanThongKe($mavanban)
	{
		$this->db->where('qlvDetails_id',$mavanban);
		$this->db->select('han_thongke');
		return $this->db->get('tbl_qlv_details')->row_array();
	}

	// lấy thông tin văn bản đến dựa trên số ký hiệu
	public function layThongTinVBDen_Click($mavanban)
	{
		$this->db->where('PK_iMaVBDen',$mavanban);
		$this->db->select('PK_iMaVBDen,iGiayMoi,vb.sTenLVB,sMoTa,sTenNguoiKy,sGiayMoiChuTri,sNoiDung,DATE_FORMAT(vb.sNgayNhap, "%d/%m/%Y") as ngayvanban');
		return $this->db->get('tbl_vanbanden as vb')->row_array();
	}
	public function layThongTinVBDen($kyhieu)
	{
		$this->db->where('sKyHieu',$kyhieu);
		$this->db->select('PK_iMaVBDen,iGiayMoi,vb.sTenLVB,sMoTa,sTenNguoiKy,sGiayMoiChuTri,sNoiDung,DATE_FORMAT(vb.sNgayNhap, "%d/%m/%Y") as ngayvanban');
		$this->db->limit(10);
		return $this->db->get('tbl_vanbanden as vb')->result_array();
	}
	// lấy ds cán bộ
	public function dsCB()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy chi tiết quá trình chuyển
	public function layQuaTrinh($vanban)
	{
		$this->db->where('lv.qlvDetails_id',$vanban);
		$this->db->select('FK_iMaCB_chuyen,thoigian_chuyen,noidung_chuyen,FK_iMaCB_nhan,qlvDetails_limit_time,han_vanban');
		$this->db->join('tbl_qlv_details dt','dt.qlvDetails_id=lv.qlvDetails_id');
		return $this->db->get('tbl_qlv_luuvet as lv')->result_array();
	}

	// lấy ds phòng ban theo phòng ban của mk truyền vào
	public function layDSCanBoPhong($phongban)
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->select('sHoTen,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	//lấy trưởng phòng của văn phòng sở
	public function layTruongPhongVPS()
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('iQuyenHan_DHNB',3);
		$this->db->where('FK_iMaCV',6);
		$this->db->select('PK_iMaCB');
		return $this->db->get('tbl_canbo')->row_array();
	}
	//lấy chi cục trưởng
	public function layCCT()
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('FK_iMaCV',15);
		$this->db->select('PK_iMaCB');
		return $this->db->get('tbl_canbo')->row_array();
	}
	//lấy chi cục phó
	public function layCCP($phongban)
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->where('FK_iMaCV',16);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy trưởng phòng chi cục
	public function layTruongPhongCC($phongban)
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('iQuyenDB',$phongban);
		$this->db->where('iQuyenHan_DHNB',11);
		$this->db->select('PK_iMaCB');
		return $this->db->get('tbl_canbo')->row_array();
	}
	// lấy tài khoản trưởng phòng
	public function layTruongPhong($phongban)
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->where('iQuyenHan_DHNB',6);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->row_array();
	}
	// lấy phó trưởng phòng hoặc chuyên viên
	public function layPTPorCV($phongban,$quyen) // 7:PTP 8:CV
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->where('iQuyenHan_DHNB',$quyen);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy phó trưởng phòng hoặc chuyên viên chi cục
	public function layPTPorCV_CC($phongban,$quyen) // 7:PTP 8:CV
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('iQuyenDB',$phongban);
		$this->db->where('iQuyenHan_DHNB',$quyen);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy kết quả giải quyết
	public function layKetQuaGiaiQuyet($mavanban,$trangthai)//0:chính 1:phối hợp
	{
		$this->db->where('qlvDetails_id',$mavanban);
		$this->db->where('phoihop',$trangthai);
		$this->db->select('sHoTen,sTenPB,kq.*');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=kq.user_id');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=kq.department_id');
		return $this->db->get('tbl_qlvfilesketqua as kq')->result_array();
	}

	//lấy đầu việc ông 
	public function layVanBanOng($mangcon)
	{
		$this->db->where_in('vb.qlv_id',$mangcon);
		$this->db->select('vb.qlv_id,qlv_noidung,qlv_file,qlvFile_path');
		$this->db->from('tbl_qlv_details as vbdt');
		$this->db->join('tbl_qlv as vb','vb.qlv_id=vbdt.qlv_id');
		$this->db->join('tbl_qlvfiles as f','f.qlv_id=vb.qlv_id','left');
		$this->db->group_by('vbdt.qlv_id');
		$this->db->order_by('vb.qlv_id','desc');
		return $this->db->get()->result_array();
	}
	// lấy văn bản con
	public function layVanBanChau($taikhoan,$trangthai)
	{
		$this->db->where('qlv_id_sub >',0);
		$this->db->where('qlvDetails_active',$trangthai);
		switch ($trangthai) {
			case 1: #GD
				$this->db->where('coordinate_per',$taikhoan);
				$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,qlvDetails_active,noidung_tralai,hoten_tralai,thoigian_tralai,process_per,main_department,department_id,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,sHoTen,noidung_chuyenphongban');
				break;
			case 2: #PGD
				$this->db->where('process_per',$taikhoan);
				$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,qlvDetails_active,noidung_tralai,hoten_tralai,thoigian_tralai,main_department,department_id,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,sHoTen,noidung_chuyenphongban,han_GD');
				break;
			case 3: #CCT
				$this->db->where('user_chicuctruong',$taikhoan);
				$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,qlvDetails_active,noidung_tralai,hoten_tralai,thoigian_tralai,main_department,department_id,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,sHoTen,noidung_chuyenphongban,text_chicucpho,FK_iMaPB_CC_CT,FK_iMaPB_CC_PH,noidung_phoihop_chicuc,han_GD,han_PGD');
				break;
			case 4: #CCP
				$this->db->where('user_chicucpho',$taikhoan);
				$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,qlvDetails_active,noidung_tralai,hoten_tralai,thoigian_tralai,main_department,department_id,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,sHoTen,FK_iMaPB_CC_CT,text_chicucpho,FK_iMaPB_CC_PH,noidung_phoihop_chicuc,Han_CCT');
				break;
			case 5: #TP
				$this->db->where('user_truongphong',$taikhoan);
				$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,qlvDetails_active,noidung_tralai,hoten_tralai,thoigian_tralai,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,sHoTen,text_photruongphong,text_chicuctruong,text_chicucpho,FK_iMaPB_CC_CT,FK_iMaPB_CC_PH,noidung_chuyenvien_phoihop,noidung_phoihop_chicuc,user_chuyenvien,Han_CCT,Han_CCP,han_GD,han_PGD');
				break;
			case 6: #PTP
				$this->db->where('user_photruongphong',$taikhoan);
				$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,qlvDetails_active,noidung_tralai,hoten_tralai,thoigian_tralai,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,sHoTen,text_photruongphong,user_chuyenvien,user_chuyenvien_phoihop,noidung_chuyenvien_phoihop,han_TP');
				break;
			case 7: #CV
				$this->db->where('user_chuyenvien',$taikhoan);
				$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,noidung_tralai,hoten_tralai,thoigian_tralai,qlvDetails_desc,qlvDetails_date,qlvDetails_limit_time,sHoTen,user_truongphong,user_photruongphong,text_truongphong,text_photruongphong,han_TP,han_PTP');
				break;
			default:
				break;
		}
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=input_per');
		$this->db->order_by('qlv_id','desc');
		$this->db->order_by('qlvDetails_date','asc');
		return $this->db->get()->result_array();
	}
	// đếm số việc chờ xử lý
	// lấy văn bản con
	public function demVanBanChau($taikhoan,$trangthai_tk)
	{
		$this->db->where('qlv_id_sub >',0);
		$this->db->where('qlvDetails_active',$trangthai_tk);
		$this->db->select('qlvDetails_id');
		switch ($trangthai_tk) {
			case 1: #GD
				$this->db->where('coordinate_per',$taikhoan);
				break;
			case 2: #PGD
				$this->db->where('process_per',$taikhoan);
				break;
			case 3: #CCT
				$this->db->where('user_chicuctruong',$taikhoan);
				break;
			case 4: #CCP
				$this->db->where('user_chicucpho',$taikhoan);
				break;
			case 5: #TP
				$this->db->where('user_truongphong',$taikhoan);
				break;
			case 6: #PTP
				$this->db->where('user_photruongphong',$taikhoan);
				break;
			case 7: #CV
				$this->db->where('user_chuyenvien',$taikhoan);
				break;
			default:
				break;
		}
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=input_per');
		return $this->db->get()->num_rows();
	}
	//lấy đầu việc ông đã xử lý 
	public function layVanBanOngDaXuLy($mangcon)
	{
		$this->db->where_in('vb.qlv_id',$mangcon);
		$this->db->select('vb.qlv_id,qlv_noidung,qlv_file,qlvFile_path');
		$this->db->from('tbl_qlv_details as vbdt');
		$this->db->join('tbl_qlv as vb','vb.qlv_id=vbdt.qlv_id');
		$this->db->join('tbl_qlvfiles as f','f.qlv_id=vb.qlv_id','left');
		$this->db->group_by('vbdt.qlv_id');
		$this->db->order_by('vb.qlv_id','desc');
		return $this->db->get()->result_array();
	}
	// lấy văn bản đã xử lý
	public function layVanBanDaXuLy($taikhoan,$trangthai)
	{
		$this->db->where('qlv_id_sub >',0);
		switch ($trangthai) {
			case 1: #GD
				$this->db->where('qlvDetails_active >',$trangthai);
				$this->db->where('coordinate_per',$taikhoan);
				break;
			case 2: #PGD
				$this->db->where('qlvDetails_active >',$trangthai);
				$this->db->where('process_per',$taikhoan);
				break;
			case 3: #CCT
				$this->db->where('qlvDetails_active >',$trangthai);
				$this->db->where('user_chicuctruong',$taikhoan);
				break;
			case 4: #CCP
				$this->db->where('qlvDetails_active >',$trangthai);
				$this->db->where('user_chicucpho',$taikhoan);
				break;
			case 5: #TP
				$this->db->where('qlvDetails_active >',$trangthai);
				$this->db->where('user_truongphong',$taikhoan);
				break;
			case 6: #PTP
				$this->db->where('qlvDetails_active >',$trangthai);
				$this->db->where('user_photruongphong',$taikhoan);
				break;
			case 7: #CV
				$this->db->where('qlvDetails_active >',$trangthai);
				$this->db->where('user_chuyenvien',$taikhoan);
				break;
			default:
				break;
		}
		$this->db->select('qlvDetails_id,vb.qlv_id,vb.qlv_id_sub,qlvDetails_desc,qlvDetails_date,qlvDetails_active,qlvDetails_limit_time,sHoTen,coordinate_per_text,process_per_text,text_chicuctruong,text_chicucpho,text_truongphong,text_photruongphong,user_photruongphong');
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=input_per');
		$this->db->order_by('qlv_id','desc');
		$this->db->order_by('qlvDetails_date','asc');
		return $this->db->get()->result_array();
	}
	// đếm đầu việc đã giải quyết
	public function demVanBanDaXuLy($taikhoan,$trangthai_tk)
	{
		$this->db->where('qlv_id_sub >',0);
		switch ($trangthai_tk) {
			case 1: #GD
				$this->db->where('qlvDetails_active >',$trangthai_tk);
				$this->db->where('coordinate_per',$taikhoan);
				break;
			case 2: #PGD
				$this->db->where('qlvDetails_active >',$trangthai_tk);
				$this->db->where('process_per',$taikhoan);
				break;
			case 3: #CCT
				$this->db->where('qlvDetails_active >',$trangthai_tk);
				$this->db->where('user_chicuctruong',$taikhoan);
				break;
			case 4: #CCP
				$this->db->where('qlvDetails_active >',$trangthai_tk);
				$this->db->where('user_chicucpho',$taikhoan);
				break;
			case 5: #TP
				$this->db->where('qlvDetails_active >',$trangthai_tk);
				$this->db->where('user_truongphong',$taikhoan);
				break;
			case 6: #PTP
				$this->db->where('qlvDetails_active >',$trangthai_tk);
				$this->db->where('user_photruongphong',$taikhoan);
				break;
			case 7: #CV
				$this->db->where('qlvDetails_active >',$trangthai_tk);
				$this->db->where('user_chuyenvien',$taikhoan);
				break;
			default:
				break;
		}
		$this->db->select('qlvDetails_id');
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=input_per');
		return $this->db->get()->num_rows();
	}
}

/* End of file Mvanbandi.php */
/* Location: ./application/models/vanbandi/Mvanbandi.php */