<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mqldv_phoihop extends CI_Model {

	// =========================================== NGƯỜI PHỐI HỢP ================================================
	// lấy đầu việc cháu
	public function layVanBanChau($taikhoan)
	{
		$this->db->like('user_chuyenvien_phoihop',$taikhoan);
		$this->db->select('vb.qlvDetails_id,qlv_id,qlvDetails_desc,qlvDetails_date,qlvDetails_active,qlvDetails_limit_time,sHoTen,coordinate_per_text,process_per_text,text_chicuctruong,text_chicucpho,text_truongphong,text_photruongphong,user_photruongphong');
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=input_per');
		$this->db->join('tbl_qlvfilesketqua as kq','kq.qlvDetails_id=vb.qlvDetails_id','left');
		$this->db->order_by('qlv_id','desc');
		$this->db->order_by('qlvDetails_date','asc');
		$this->db->group_by('kq.qlvDetails_id');
		return $this->db->get()->result_array();
	}
	// đếm đầu việc người phối hợp
	public function demVanBanChau($taikhoan)
	{
		$this->db->like('user_chuyenvien_phoihop',$taikhoan);
		$this->db->select('vb.qlvDetails_id');
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=input_per');
		$this->db->join('tbl_qlvfilesketqua as kq','kq.qlvDetails_id=vb.qlvDetails_id','left');
		$this->db->group_by('kq.qlvDetails_id');
		return $this->db->get()->num_rows();
	}
	// kiểm tra phải chuyên viên phối hợp k
	public function kiemtra($taikhoan,$mavanban)
	{
		$this->db->like('user_chuyenvien_phoihop',$taikhoan);
		$this->db->where('qlvDetails_id',$mavanban);
		return $this->db->get('tbl_qlv_details')->num_rows();
	}
	// =========================================== PHÒNG PHỐI HỢP ================================================
	// kiểm tra đầu việc phải được giao cho chuyên viên không
	public function kiemtracanbo($taikhoan)
	{
		$this->db->like('user_nhan',$taikhoan);
		return $this->db->get('tbl_qldv_phongphoihop')->num_rows();
	}
	// lấy đầu việc cháu
	public function layVanBanChau_PB($phongban)
	{
		$this->db->like('department_id',$phongban);
		$this->db->select('vb.qlvDetails_id,qlv_id,qlvDetails_date,qlvDetails_desc,qlvDetails_limit_time,sHoTen,han_giaiquyet,noidung_nhan,user_nhan,user_chuyen');
		$this->db->from('tbl_qlv_details as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=input_per');
		$this->db->join('tbl_qldv_phongphoihop as ph','ph.qlvDetails_id=vb.qlvDetails_id','left');
		$this->db->order_by('qlv_id','desc');
		$this->db->order_by('qlvDetails_date','asc');
		return $this->db->get()->result_array();
	}
	// lấy chuyên viên và trưởng phòng
	public function layPTPandCV($phongban,$quyen) // 7:PTP 8:CV
	{
		$this->db->where('iTrangThai',0);
		$this->db->where('FK_iMaPhongHD',$phongban);
		$this->db->where_in('iQuyenHan_DHNB',$quyen);
		$this->db->select('PK_iMaCB,sHoTen');
		return $this->db->get('tbl_canbo')->result_array();
	}
}

/* End of file Mqldv_phoihop.php */
/* Location: ./application/models/qldv/Mqldv_phoihop.php */