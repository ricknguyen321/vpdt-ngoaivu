<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mketqua extends CI_Model {

     // lấy việc quá hạn của phòng ban
    public function layViecQuaHan($phongban,$limit=NULL,$offset=NULL)
    {
        $date = date('Y-m-d');
        $this->db->where('main_department',$phongban);
        $this->db->where('han_thongke < "'.$date.'" and han_thongke > "1970-01-01" and thoigian_hoanthanh="0000-00-00"',NULL);
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->result_array();
    }
    // đếm việc quá hạn của phòng ban
    public function demViecQuaHan($phongban)
    {
        $date = date('Y-m-d');
        $this->db->where('main_department',$phongban);
        $this->db->where('han_thongke < "'.$date.'" and han_thongke > "1970-01-01" and thoigian_hoanthanh="0000-00-00"',NULL);
        $this->db->select('tbl_qlv_details.qlv_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->num_rows();
    }
    // lấy trưởng phòng duyệt văn bản
    public function layTruongPhong($phongban)
    {
        $this->db->where('FK_iMaPhongHD',$phongban);
        $this->db->where('iQuyenHan_DHNB',6);
        $this->db->where('iTrangThai',0);
        $this->db->select('PK_iMaCB,sHoTen');
        return $this->db->get('tbl_canbo')->row_array();
    }
    public function layPhongBanDuThao()
    {
        $this->db->where('iTrangThai',0);
        $this->db->where('sepxep>',0);
        $this->db->select('sTenPB,PK_iMaPB,sVietTat');
        $this->db->order_by('sepxep','asc');
        return $this->db->get('tbl_phongban')->result_array();
    }
    // kiểm tra người cuối cùng nhận được
    public function laynguoinhancuoi($mavanban)
    {
        $this->db->where('qlvDetails_id',$mavanban);
        $this->db->select('FK_iMaCB_nhan');
        $this->db->order_by('PK_iMaLuuVet','desc');
        return $this->db->get('tbl_qlv_luuvet')->row_array();
    }
    // lấy kết quả bị trả lại
    public function ketquaTraLai($taikhoan)
    {
        $this->db->where('user_id',$taikhoan);
        $this->db->where('iTrangThai',0);
        $this->db->where('qlvFile_path !=','');
        $this->db->select('qlvFile_id ,sNoiDungTraLai,qlvFile_path,qlvDetails_desc,sTieuDe,qlvFile_date');
        $this->db->from('tbl_qlvfilesketqua as f');
        $this->db->join('tbl_qlv_details as det','det.qlvDetails_id = f.qlvDetails_id');
        return $this->db->get()->result_array();
    }
    // lấy kết quả bị trả lại
    public function demketquaTraLai($taikhoan)
    {
        $this->db->where('user_id',$taikhoan);
        $this->db->where('iTrangThai',0);
        $this->db->where('qlvFile_path !=','');
        $this->db->select('qlvFile_id ,sNoiDungTraLai,qlvFile_path,qlvDetails_desc,sTieuDe,qlvFile_date');
        $this->db->from('tbl_qlvfilesketqua as f');
        $this->db->join('tbl_qlv_details as det','det.qlvDetails_id = f.qlvDetails_id');
        return $this->db->get()->num_rows();
    }
     // lấy file đã được duyệt của phòng ban
    public function layFileDaDuyetPhong($mavanban,$trangthai=NULL,$lancuoi=NULL)
    {
        $this->db->where('user_id!=',617);
        $this->db->where('qlvDetails_id',$mavanban);
        
        $this->db->where('qlvFile_path !=','');
        if(!empty($trangthai))
        {
            $this->db->where_in('iTrangThai',$trangthai);
        }
        if(!empty($lancuoi))
        {
            $this->db->where('lancuoi',$lancuoi);
        }
        $this->db->select('qlvFile_path,qlvFile_date,qlvFile_id');
        $this->db->order_by('qlvFile_date','desc');
        return $this->db->get('tbl_qlvfilesketqua')->result_array();
    }
    // lấy file đã được duyệt
    public function layFileDaDuyet($mavanban,$trangthai=NULL,$lancuoi=NULL)
    {
        $this->db->where('qlvDetails_id',$mavanban);
        
        $this->db->where('qlvFile_path !=','');
        if(!empty($trangthai))
        {
            $this->db->where('iTrangThai',$trangthai);
        }
        if(!empty($lancuoi))
        {
            $this->db->where('lancuoi',$lancuoi);
        }
        $this->db->select('qlvFile_path,qlvFile_date,qlvFile_id');
        $this->db->order_by('qlvFile_date','desc');
        return $this->db->get('tbl_qlvfilesketqua')->result_array();
    }

    // lấy danh sách tài liệu
    public function layTaiLieu($loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('main_department >',0);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,sTieuDe,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id');
        $this->db->from('tbl_qlv_details as det');
        $this->db->join('tbl_qlv as q','q.qlv_id=det.qlv_id');
        $this->db->order_by('sapxep','asc');
        $this->db->order_by('qlvDetails_id','asc');
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
     // đếm danh sách tài liệu
    public function demTaiLieu($loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL)
    {
        $this->db->where('main_department >',0);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,sTieuDe,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id');
        $this->db->from('tbl_qlv_details as det');
        $this->db->join('tbl_qlv as q','q.qlv_id=det.qlv_id');
        return $this->db->get()->num_rows();
    }
    // lấy lĩnh vực đầu việc
    public function layLinhVuc()
    {
        $this->db->where('id_parent >',0);
        $this->db->where('linhVuc_active',1);
        $this->db->select('linhVuc_id,linhVuc_name');
        $this->db->order_by('id_parent','desc');
        return $this->db->get('tbl_linhvuc_qldv')->result_array();
    }
	// lấy dữ liệu theo trường cần sử dụng
	public function layDL($dieukien,$table)
    {
        $this->db->select($dieukien);
        $this->db->where("$dieukien !=",'');
        $this->db->group_by($dieukien);
        return $this->db->get($table)->result_array();
    }
    // lấy thông tin đầu việc có kết quả cần duyệt
    public function layTTKT($trangthai=NULL,$phongban=NULL,$taikhoan=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$loaivanban=NULL)
    {
        $this->db->where('phoihop',0);
    	if(!empty($trangthai))
    	{
    		$this->db->where('f.iTrangThai',$trangthai);
    	}
    	if(!empty($phongban))
    	{
    		$this->db->where('main_department',$phongban);
    	}
    	if(!empty($nhiemky))
    	{
    		$this->db->where('sNhiemKy',$nhiemky);
    	}
    	if(!empty($khoa))
    	{
    		$this->db->where('sKhoa',$khoa);
    	}
    	if(!empty($kyhop))
    	{
    		$this->db->where('sKyHop',$kyhop);
    	}
    	if(!empty($loaivanban))
    	{
    		$this->db->where('loaivanban',$loaivanban);
    	}
    	else{
            if($taikhoan!=617)
            {
                $mang = array(0,1,2);
            }
            else
            {
                $mang = array(1,2);
            }
    		$this->db->where_in('loaivanban',$mang);
    	}
        if(!empty($taikhoan))
        {
            if($taikhoan!=617)
            {
                $this->db->where('FK_iMaCB_Duyet',$taikhoan);
            }
        }
    	$this->db->where('qlvFile_path !=','');
    	$this->db->select('qlvFile_id,qlvDetails_desc,sTieuDe,f.qlvFile_date,f.qlvFile_path,f.department_id,loaivanban');
    	$this->db->from('tbl_qlvfilesketqua as f');
    	$this->db->join('tbl_qlv_details as det','det.qlvDetails_id = f.qlvDetails_id');
    	$this->db->join('tbl_qlv as vb','vb.qlv_id=det.qlv_id');
    	return $this->db->get()->result_array();
    }
    // đếm văn bản chờ duyệt
    public function demTTKT($trangthai=NULL,$phongban=NULL,$taikhoan=NULL)
    {
        $this->db->where('phoihop',0);
        if(!empty($trangthai))
        {
            $this->db->where('f.iTrangThai',$trangthai);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($taikhoan))
        {
            if($taikhoan!=617)
            {
                $this->db->where('FK_iMaCB_Duyet',$taikhoan);
            }
            else{
                $mang = array(1,2);
                $this->db->where_in('loaivanban',$mang);
            }
        }
        $this->db->where('qlvFile_path !=','');
        $this->db->select('qlvFile_id');
        $this->db->from('tbl_qlvfilesketqua as f');
        $this->db->join('tbl_qlv_details as det','det.qlvDetails_id = f.qlvDetails_id');
        $this->db->join('tbl_qlv as vb','vb.qlv_id=det.qlv_id');
        return $this->db->get()->num_rows();
    }

}

/* End of file Mketqua.php */
/* Location: ./application/models/qldv/Mketqua.php */