<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mchuyennhan extends CI_Model {


    // lấy mã chuyền nhận
    public function layMaChuyenNhan($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',2);
        $this->db->where('iTraLai',1);
        $this->db->select('PK_CN_QLDV');
        return $this->db->get('tbl_chuyennhan_qldv')->row_array();

    }
    // xóa chuyền nhận những người trực tiếp nhận được //====> có thể chưa dùng
    public function xoaChuyenNhan($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Gui',$taikhoan);
        $this->db->where('iTraLai',1);
        $this->db->delete('tbl_chuyennhan_qldv');
    }
    // xóa chuyền nhận 2
    public function xoaChuyenNhan_Hai($madauviec,$machuyennhan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('PK_CN_QLDV >',$machuyennhan);
        $this->db->where('iTraLai',1);
        $this->db->delete('tbl_chuyennhan_qldv');
    }
    // lấy người phối hợp để giải quyết
    public function layNguoiPH_GiaiQuyet($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',1);
        $this->db->where('phoihop_chutri',2);
        return $this->db->get('tbl_chuyennhan_qldv')->num_rows();
    }
    // lấy người cuối
    public function layNguoiCuoiCung($madauviec,$chutri_phoihop=NULL,$nguoi_chutri_phoihop=NULL)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('iTraLai',1);
        if(!empty($chutri_phoihop))
        {
            $this->db->where_in('CT_PH',$chutri_phoihop);
        }
        if(!empty($nguoi_chutri_phoihop))
        {
            $this->db->where_in('phoihop_chutri',$nguoi_chutri_phoihop);   
        }
        $this->db->select('FK_iMaCB_Nhan');
        $this->db->order_by('PK_CN_QLDV','desc');
        return $this->db->get('tbl_chuyennhan_qldv')->row_array();
    }
    // lấy người gửi cho 
    public function layNguoiGui($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTraLai',1);
        $this->db->where('CT_PH',1);
        $this->db->select('FK_iMaCB_Gui');
        return $this->db->get('tbl_chuyennhan_qldv')->row_array();
    }
    // lấy quá trình chuyển nhận
    public function layQuyTrinhXuLy($madauviec,$chutri_phoihop=NULL,$nguoi_chutri_phoihop=NULL)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        // $this->db->where('iTraLai',1);
        if(!empty($chutri_phoihop))
        {
            $this->db->where_in('CT_PH',$chutri_phoihop);
        }
        if(!empty($nguoi_chutri_phoihop))
        {
            $this->db->where_in('phoihop_chutri',$nguoi_chutri_phoihop);   
        }
        $this->db->select('FK_iMaCB_Gui,sNoiDungChuyen,FK_iMaCB_Nhan,sThoiGian,sHanXuLy');
        return $this->db->get('tbl_chuyennhan_qldv')->result_array();
    }
    public function layQuyTrinhXuLyPH($madauviec,$chutri_phoihop,$phong,$tralai,$an)
    {
        if(!empty($an))
        {
            $this->db->where('sNoiDungChuyen !=','');
        }
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where_in('iTraLai',$tralai);
        $this->db->where_in('CT_PH',$chutri_phoihop);
        $this->db->where_in('FK_iMaPB',$phong);
        $this->db->select('FK_iMaCB_Gui,sNoiDungChuyen,FK_iMaCB_Nhan,sThoiGian,sHanXuLy');
        return $this->db->get('tbl_chuyennhan_qldv')->result_array();
    }
    public function layQuyTrinhXuLy_Hai($madauviec,$chutri_phoihop=NULL,$phong=NULL,$phongcaphai=NULL,$nguoi_chutri_phoihop=NULL)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('sNoiDungChuyen !=','');
        if(!empty($chutri_phoihop))
        {
            $this->db->where_in('CT_PH',$chutri_phoihop);
        }
        if(!empty($phong))
        {
            $this->db->where_in('FK_iMaPB',$phong);
        }
        if(!empty($phongcaphai))
        {
            $this->db->where_in('FK_iMaPB_CapHai',$phongcaphai);
        }
        if(!empty($nguoi_chutri_phoihop))
        {
            $this->db->where_in('phoihop_chutri',$nguoi_chutri_phoihop);   
        }
        $this->db->select('FK_iMaCB_Gui,sNoiDungChuyen,FK_iMaCB_Nhan,sThoiGian,sHanXuLy');
        return $this->db->get('tbl_chuyennhan_qldv')->result_array();
    }
    // lấy phó phòng + chuyên viên
    public function layCanBo($phongban,$quyenhan=NULL,$chucvu=NULL,$phongchicuc=NULL)
    {
        $this->db->where('FK_iMaPhongHD',$phongban);
        if(!empty($quyenhan))
        {
            $this->db->where('iQuyenHan_DHNB',$quyenhan);
        }
        if(!empty($chucvu))
        {
            $this->db->where('FK_iMaCV',$chucvu);
        }
        if(!empty($phongchicuc))
        {
            $this->db->where('iQuyenDB',$phongchicuc);
        }
        $this->db->where('iTrangThai',0);
        $this->db->select('PK_iMaCB,sHoTen');
        return $this->db->get('tbl_canbo')->result_array();
    }

    // lấy đầu việc ông
    public function layDauViecOng($mangcon)
    {
        $this->db->where_in('tbl_qlv.qlv_id',$mangcon);
        $this->db->select('tbl_qlv.qlv_id,qlv_noidung,qlv_file,qlvFile_path');
        $this->db->from('tbl_qlv');
        $this->db->join('tbl_qlvfiles','tbl_qlvfiles.qlv_id=tbl_qlv.qlv_id','left');
        $this->db->group_by('tbl_qlv.qlv_id');
        $this->db->order_by('tbl_qlv.qlv_id','desc');
        return $this->db->get()->result_array();
    }
    // cập nhât trạng thái người chuyển
    public function capnhatTrangThaiNguoiChuyen($madauviec,$taikhoan)
    {
        $this->db->where('FK_iMaQLDV',$madauviec);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->set('iTrangThai',2);
        $this->db->update('tbl_chuyennhan_qldv');
    }
    // lấy cán bộ
    public function layCB()
    {
        $this->db->where('iTrangThai',0);
        $this->db->select('PK_iMaCB,sHoTen');
        return $this->db->get('tbl_canbo')->result_array();
    }
    // lấy đầu việc của phòng
    public function layDauViecCuaPhong($phong,$chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('FK_iMaPB',$phong);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->result_array();
    }
    public function layDauViecCuaPhongPH($phong,$chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaPB',$phong);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->result_array();
    }
    public function demDauViecCuaPhong($phong,$chutri_phoihop,$phongchicuc=NULL)
    {
        $this->db->where('FK_iMaPB',$phong);
        $this->db->where('CT_PH',$chutri_phoihop);
        if(!empty($phongchicuc))
        {
            // $this->db->where('FK_iMaPB_CapHai',$phongchicuc);
        }
        $this->db->where('phoihop_chutri',$chutri_phoihop);
        $this->db->where('iTraLai',1);
        $this->db->select('qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        $this->db->group_by('FK_iMaQLDV');
        return $this->db->get()->num_rows();
    }
    //===================================================== ĐẦU VIỆC ĐÃ XỬ LÝ GĐ PGĐ
    public function layDauViecDaXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        $this->db->where('thoigian_hoanthanh','0000-00-00');
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_PP,sGoiY_CV,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id,FK_iMaPB_CT_CC,FK_iMaCB_CCP,FK_iMaPB_PH_CC,sGoiY_PB_CC,FK_iMaCB_PGD,sGoiY_PGD');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
    public function demDauViecDaXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        $this->db->where('thoigian_hoanthanh','0000-00-00');
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        return $this->db->get()->num_rows();
    }
    // ==================================================== ĐẦU VIỆC CHỜ XỬ LÝ
    // lấy đầu việc chờ xử lý
	public function layDauViecChoXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL,$limit=NULL,$offset=NULL)
    {
        $this->db->where('main_department >',0);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_desc,tbl_qlv_details.qlv_id,FK_iMaCB_Gui,sNoiDungChuyen,sGoiY_PB,tbl_qlv_details.input_per,qlvDetails_date,qlvDetails_limit_time,FK_iMaCB_PP_CT,FK_iMaCB_PP_PH,FK_iMaCB_CV_CT,FK_iMaCB_CV_PH,sGoiY_PP,sGoiY_CV,sTieuDe,sHanXuLy,main_department,department_id,FK_iMaCB_LanhDao,qlvDetails_id,FK_iMaPB_CT_CC,FK_iMaPB_PH_CC,sGoiY_PB_CC');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        if(!empty($limit))
        {
            // $this->db->limit($limit,$offset);
        }
        return $this->db->get()->result_array();
    }
    // đếm đầu việc chờ xứ lý
    public function demDauViecChoXuLy($taikhoan,$trangthai,$chutri_phoihop,$nguoi_chutri_phoihop,$loaivanban=NULL,$nhiemky=NULL,$khoa=NULL,$kyhop=NULL,$tieude=NULL,$noidung=NULL,$linhvuc=NULL,$lanhdao=NULL,$phongban=NULL,$chutri=NULL)
    {
        $this->db->where('main_department >',0);
        $this->db->where('FK_iMaCB_Nhan',$taikhoan);
        $this->db->where('iTrangThai',$trangthai);
        $this->db->where('CT_PH',$chutri_phoihop);
        $this->db->where('phoihop_chutri',$nguoi_chutri_phoihop);
        $this->db->where('iTraLai',1);
        if(!empty($loaivanban))
        {
            $this->db->where('loaivanban',$loaivanban);
        }
        if(!empty($nhiemky))
        {
            $this->db->where('sNhiemKy',$nhiemky);
        }
        if(!empty($khoa))
        {
            $this->db->where('sKhoa',$khoa);
        }
        if(!empty($kyhop))
        {
            $this->db->where('sKyHop',$kyhop);
        }
        if(!empty($tieude))
        {
            $this->db->where('sTieuDe',$tieude);
        }
        if(!empty($noidung))
        {
            $this->db->like('qlvDetails_desc',$noidung);
        }
        if(!empty($linhvuc))
        {
            $this->db->where('linhvuc_sub',$linhvuc);
        }
        if(!empty($phongban))
        {
            $this->db->where('main_department',$phongban);
        }
        if(!empty($lanhdao))
        {
            $this->db->where('FK_iMaCB_LanhDao',$lanhdao);
        }
        $this->db->select('qlvDetails_id');
        $this->db->from('tbl_qlv_details');
        $this->db->join('tbl_qlv','tbl_qlv.qlv_id=tbl_qlv_details.qlv_id');
        $this->db->join('tbl_chuyennhan_qldv','tbl_chuyennhan_qldv.FK_iMaQLDV=tbl_qlv_details.qlvDetails_id');
        return $this->db->get()->num_rows();
    }
	// ============================================================ GIÁM ĐỐC
	// lấy văn bản bị trả lại
	public function layDauViecTraLai($taikhoan)
	{
		$this->db->where('nguoibitralai',$taikhoan);
		$this->db->select('qlvDetails_id,qlv_id,qlvDetails_date,input_per,noidungtralai,qlvDetails_desc,sGoiY_PB,qlvDetails_limit_time,FK_iMaCB_LanhDao,main_department,department_id');
		$this->db->order_by('qlvDetails_id','desc');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
	// đếm đầu việc trả lại
	public function demDauViecTraLai($taikhoan)
	{
		$this->db->where('nguoibitralai',$taikhoan);
		$this->db->select('qlvDetails_id');
		$this->db->order_by('qlvDetails_id','desc');
		return $this->db->get('tbl_qlv_details')->num_rows();
	}
	// lấy văn bản chờ duyệt của lãnh đạo phòng
	public function layDauViecChoDuyet($taikhoan)
	{
		$this->db->where('truongphongduyet',$taikhoan);
		$this->db->select('qlvDetails_id,qlv_id,qlvDetails_date,input_per,qlvDetails_desc,sGoiY_PB,qlvDetails_limit_time,FK_iMaCB_LanhDao,main_department,department_id');
		$this->db->order_by('qlvDetails_id','desc');
		return $this->db->get('tbl_qlv_details')->result_array();
	}
    // đếm văn bản chờ duyệt của lãnh đạo phòng
    public function demDauViecChoDuyet($taikhoan)
    {
        $this->db->where('truongphongduyet',$taikhoan);
        $this->db->select('qlvDetails_id');
        $this->db->order_by('qlvDetails_id','desc');
        return $this->db->get('tbl_qlv_details')->num_rows();
    }

}

/* End of file Mchuyennhan.php */
/* Location: ./application/models/qldv/Mchuyennhan.php */