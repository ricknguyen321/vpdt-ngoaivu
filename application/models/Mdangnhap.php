<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdangnhap extends CI_Model {

	public function kiemtraDangNhap($taikhoan,$matkhau)
    {
		$this->db->where('sTaiKhoan',$taikhoan);
		$this->db->where('sMatKhau',$matkhau);
		$this->db->where('cb.iTrangThai',0);
		if($taikhoan!='admin'){
            $this->db->join('tbl_phongban as pb','pb.PK_iMaPB=cb.FK_iMaPhongHD');
        }
		return $this->db->get('tbl_canbo as cb')->row_array();
	}

	public function checkLogin($taikhoan,$matkhau)
    {
        $this->db->where('sTaiKhoan',$taikhoan);
        $this->db->where('sMatKhau',$matkhau);
        $this->db->where('tbl_canbo.iTrangThai',0);
        $this->db->select('tbl_canbo.iTrangThai,PK_iMaCB,sHoTen,FK_iMaPhongHD,FK_iMaCV,sTaiKhoan,iQuyenHan_DHNB,iPhanMem,iQuyenHan_TNVB,iQuyenDB,iLoaiPhanMem,sodienthoaiky,anhchuky');
        $this->db->from('tbl_canbo');
        $this->db->join('tbl_phongban','tbl_phongban.PK_iMaPB=tbl_canbo.FK_iMaPhongHD');
        return $this->db->get()->row_array();
    }
    public function session_ci($token)
    {
        $this->db->where('sha1(sha1(id))',$token);
        $this->db->select('data');
        $this->db->from('ci_sessions');
        return $this->db->get()->row_array();
    }
}

/* End of file Mdangnhap.php */
/* Location: ./application/models/Mdangnhap.php */