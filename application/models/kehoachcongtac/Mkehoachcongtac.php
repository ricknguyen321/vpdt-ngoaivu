<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkehoachcongtac extends CI_Model {
    /**
     * @author:	admin
     * Time:
     * Description: Model văn bản đến
     * Process: quá trình
     */
     // kiểm tra xem nhận xét chưa
    public function kiemtra($taikhoan,$chuyenvien,$tuan,$id_danhgia)
    {
    	$this->db->where('FK_iMaCB_PP',$taikhoan);
    	$this->db->where('FK_iMaCB_CV',$chuyenvien);
    	$this->db->where('tuan',$tuan);
    	$this->db->where('id_danhgia',$id_danhgia);
    	return $this->db->get('nhanxet_phophong')->num_rows();
    }
    public function kiemtranhanxetthang($taikhoan,$chuyenvien,$thang)
    {
        $this->db->where('FK_iMaCB_PP',$taikhoan);
        $this->db->where('FK_iMaCB_CV',$chuyenvien);
        $this->db->where('thang',$thang);
        return $this->db->get('nhanxet_phophong')->row_array();
    }
    public function capnhatnhanxetthang($taikhoan,$chuyenvien,$thang,$data)
    {
        $this->db->where('FK_iMaCB_PP',$taikhoan);
        $this->db->where('FK_iMaCB_CV',$chuyenvien);
        $this->db->where('thang',$thang);
        $this->db->update('nhanxet_phophong',$data);
        return $this->db->affected_rows();
    }
    // cập nhật

   	public function capnhat($taikhoan,$chuyenvien,$tuan,$id_danhgia,$data)
    {
    	$this->db->where('FK_iMaCB_PP',$taikhoan);
    	$this->db->where('FK_iMaCB_CV',$chuyenvien);
    	$this->db->where('tuan',$tuan);
    	$this->db->where('id_danhgia',$id_danhgia);
    	$this->db->update('nhanxet_phophong',$data);
    	return $this->db->affected_rows();
    }
    public function them($taikhoan,$chuyenvien,$tuan,$id_danhgia,$data)
    {
    	$this->db->where('FK_iMaCB_PP',$taikhoan);
    	$this->db->where('FK_iMaCB_CV',$chuyenvien);
    	$this->db->where('tuan',$tuan);
    	$this->db->where('id_danhgia',$id_danhgia);
    	$this->db->insert('nhanxet_phophong',$data);
    	return $this->db->affected_rows();
    }
    public function lay($taikhoan,$chuyenvien,$tuan,$id_danhgia)
    {
    	$this->db->select('sNhanXet,sThoiGian');
    	$this->db->where('FK_iMaCB_PP',$taikhoan);
    	$this->db->where('FK_iMaCB_CV',$chuyenvien);
    	$this->db->where('tuan',$tuan);
    	$this->db->where('id_danhgia',$id_danhgia);
    	return $this->db->get('nhanxet_phophong')->row_array();
    }

    // lấy nhận xét tháng
    public function laythang($lanhdao,$truongphong,$thang)
    {
        $this->db->select('*');
        $this->db->where('FK_iMaCB_PP',$lanhdao);
        $this->db->where('FK_iMaCB_CV',$truongphong);
        $this->db->where('thang',$thang);
        return $this->db->get('nhanxet_phophong')->row_array();
    }
    // cập nhật dữ liệu tháng
    public function capnhatthang($lanhdao,$truongphong,$thang,$mangcapnhat)
    {
        $this->db->where('FK_iMaCB_PP',$lanhdao);
        $this->db->where('FK_iMaCB_CV',$truongphong);
        $this->db->where('thang',$thang);
        $this->db->update('nhanxet_phophong',$mangcapnhat);
    }

    public function lay_phong_id($kh_id)
    {
        $this->db->select('phong_id');
        $this->db->where('danhgia_id',$kh_id);
        return $this->db->get('danhgia')->row_array();
    }
}
