<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcuoctiep extends CI_Model {
	
	public function layDSCuocTiep($limit=NULL,$offset=NULL,$tonghop=NULL,$cq=NULL,$ldtp=NULL,$tungay=NULL,$denngay=NULL,$qg=NULL)
	{		
		if(!empty($cq))
		{
			$this->db->where('coquanct',$cq);
		}
		if(!empty($qg))
		{
			$this->db->where('quoc_gia',$qg);
		}
		if(!empty($ldtp))
		{
			$this->db->where('ldtiep',$ldtp);
		}
		if(!empty($tungay))
		{
			$this->db->where('ngaytiep >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('ngaytiep <=',$denngay);
		}
		if(!empty($tonghop))
		{
			$this->db->like('tendoan',$tonghop);
			$this->db->or_like('truongdoan',$tonghop);
			$this->db->or_like('ldtiep',$tonghop);
			$this->db->or_like('ldso',$tonghop);
			$this->db->or_like('noidung',$tonghop);
			$this->db->or_like('ngaytiep',date_insert($tonghop));
			$this->db->or_like('cvth',$tonghop);
			$this->db->or_like('tangpham',$tonghop);
			$this->db->or_like('phiendich',$tonghop);
			$this->db->or_like('note',$tonghop);
			$this->db->or_like('quoc_gia',$tonghop);
		}

		$this->db->select('ct.*, ten_ld, chuc_vu, co_quan, chuc_vu_vt, cq_vt');
		$this->db->from('tbl_cuoctiep as ct');
		$this->db->join('tbl_ldtp as ldtp','ct.ldtiep=ldtp.id');
		$this->db->order_by('ngaytiep','desc');
		$this->db->group_by('ct.id');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function demDSCuocTiep($tonghop=NULL,$cq=NULL,$ldtp=NULL,$tungay=NULL,$denngay=NULL)
	{
		if(!empty($cq))
		{
			$this->db->where('coquanct',$cq);
		}
		if(!empty($ldtp))
		{
			$this->db->where('ldtiep',$ldtp);
		}
		if(!empty($tungay))
		{
			$this->db->where('ngaytiep >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('ngaytiep <=',$denngay);
		}
		if(!empty($tonghop))
		{
			$this->db->like('tendoan',$tonghop);
			$this->db->or_like('truongdoan',$tonghop);
			$this->db->or_like('ldtiep',$tonghop);
			$this->db->or_like('ldso',$tonghop);
			$this->db->or_like('noidung',$tonghop);
			$this->db->or_like('cvth',$tonghop);
			$this->db->or_like('tangpham',$tonghop);
			$this->db->or_like('phiendich',$tonghop);
			$this->db->or_like('note',$tonghop);
		}
		$this->db->select('ct.*, ldtp.*');
		$this->db->from('tbl_cuoctiep as ct');
		$this->db->join('tbl_ldtp as ldtp','ct.ldtiep=ldtp.id');
		$this->db->order_by('ngaytiep','desc');
		$this->db->group_by('ct.id');
		return $this->db->get()->num_rows();
	}
	
	public function layDShnht($limit=NULL,$offset=NULL,$tonghop=NULL,$dvtc=NULL,$tungay=NULL,$denngay=NULL,$cq=NULL)
	{				
		if(!empty($tonghop))
		{
			$this->db->like('ten_hnht',$tonghop);
			$this->db->or_like('coquan_th',$tonghop);
			$this->db->or_like('noidung',$tonghop);
			$this->db->or_like('diadiem',$tonghop);
			$this->db->or_like('baocao',$tonghop);
			$this->db->or_like('cq_capphep',$tonghop);
			$this->db->or_like('note',$tonghop);
		}
		if(!empty($tungay))
		{
			$this->db->where('thoi_gian >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('thoi_gian <=',$denngay);
		}
		
		if(!empty($cq))
		{
			$this->db->where('cq_capphep',$cq);
		}
		
		if(!empty($dvtc))
		{
			$this->db->where('coquan_th',$dvtc);
		}

		$this->db->select('hnht.*');
		$this->db->from('tbl_hnht as hnht');
		$this->db->order_by('thoi_gian','desc');
		$this->db->group_by('hnht.id');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function demDShnht($tonghop=NULL,$dvtc=NULL,$tungay=NULL,$denngay=NULL,$cq=NULL)
	{
		
		if(!empty($tonghop))
		{
			$this->db->like('ten_hnht',$tonghop);
			$this->db->or_like('coquan_th',$tonghop);
			$this->db->or_like('noidung',$tonghop);
			$this->db->or_like('diadiem',$tonghop);
			$this->db->or_like('baocao',$tonghop);
			$this->db->or_like('cq_capphep',$tonghop);
			$this->db->or_like('note',$tonghop);
		}
		
		if(!empty($tungay))
		{
			$this->db->where('thoi_gian >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('thoi_gian <=',$denngay);
		}
		
		if(!empty($cq))
		{
			$this->db->where('cq_capphep',$cq);
		}
		
		if(!empty($dvtc))
		{
			$this->db->where('coquan_th',$dvtc);
		}

		$this->db->select('hnht.*');
		$this->db->from('tbl_hnht as hnht');
		$this->db->group_by('hnht.id');
		return $this->db->get()->num_rows();
	}
	
	public function layDSQuocGia()
	{		
		$this->db->select('ct.quoc_gia');
		$this->db->from('tbl_cuoctiep as ct');
		$this->db->order_by('ct.quoc_gia','asc');
		$this->db->group_by('ct.quoc_gia');
		return $this->db->get()->result_array();
	}
	
	public function layDSCQCP()
	{		
		$this->db->select('hnht.cq_capphep');
		$this->db->from('tbl_hnht as hnht');
		$this->db->order_by('hnht.cq_capphep','asc');
		$this->db->group_by('hnht.cq_capphep');
		return $this->db->get()->result_array();
	}
	
	public function layDSDVTC()
	{		
		$this->db->select('hnht.coquan_th');
		$this->db->from('tbl_hnht as hnht');
		$this->db->order_by('hnht.coquan_th','asc');
		$this->db->group_by('hnht.coquan_th');
		return $this->db->get()->result_array();
	}
	
	
}
