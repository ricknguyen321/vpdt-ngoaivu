<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnhomchucnang extends CI_Model {

	// kiểm tra quyền thê sửa xóa
	public function kiemtraQuyen($macb,$quyen)
	{
		$this->db->where('PK_iMaCB',$macb);
		$this->db->like('FK_iMaQuyen',$quyen);
		return $this->db->get('tbl_canbo')->result_array();
	}
	public function layDuLieu($primary_key,$id,$table)
	{
		$this->db->where_in($primary_key,$id);
		$this->db->order_by('iDoUuTien','asc');
		$this->db->group_by('FK_iMaNhomCN');
		return $this->db->get($table)->result_array();
	}
	// lấy nhóm chức năng hệ thống
	public function layNhomChucNang($machucnang)
	{
		$this->db->where_in('PK_iMaCN',$machucnang);
		$this->db->select('PK_iMaNhomCN,sTenNhomCN,iCon,sMaMau');
		$this->db->from('tbl_chucnang as cn');
		$this->db->join('tbl_nhomchucnang as nhom','nhom.PK_iMaNhomCN=cn.FK_iMaNhomCN');
		$this->db->order_by('nhom.PK_iMaNhomCN','asc');
		$this->db->group_by('sTenNhomCN');
		return $this->db->get()->result_array();
	}

}

/* End of file Mnhomchucnang.php */
/* Location: ./application/models/danhmuc/Mnhomchucnang.php */