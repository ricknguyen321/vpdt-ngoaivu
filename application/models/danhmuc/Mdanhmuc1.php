<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdanhmuc extends CI_Model {
    /**
     * @author:	Hải
     * Time:
     * Description: Cập nhật các thông tin trong các bảng đơn
     */

    public function danhgialdso($danhgia_gd,$where,$tbl_danhgia){
        $this->db->select($danhgia_gd);
        $this->db->where($where);
        return $this->db->get($tbl_danhgia)->result_array();
    }
    
    public function layDuLieu_sendMail($sodi,$loai)
    {
        $this->db->select('PK_iMaVBDi');
        $this->db->where('iSoVBDi',$sodi);
        if($loai==10)
        {
            $this->db->where('FK_iMaLVB',10);
        }
        else{
            $this->db->where('FK_iMaLVB !=',10);
        }
        return $this->db->get('tbl_vanbandi')->result_array();
    }
    // lấy những mail đã gửi theo ngày
    public function layMailDaGui_theongay($time)
    {
        $this->db->where('DATE(sThoiGian)',$time);
        $this->db->order_by('PK_iMaLuuVet','desc');
        return $this->db->get('tbl_luuvet_mail')->result_array();
    }
    // lấy danh sách chức năng hệ thống dk viết
    public function layChucNangHeThong()
    {
        $this->db->where('iTrangThai',1);
        $this->db->select('PK_iMaChucNang,sTenChucNang,sHuongDan,iTrangThai,FK_iMaQuyen,sTenQuyen');
        $this->db->from('tbl_chucnang');
        $this->db->join('tbl_quyen','tbl_quyen.PK_iMaQuyen=tbl_chucnang.FK_iMaQuyen');
        $this->db->order_by('FK_iMaQuyen','asc');
        return $this->db->get()->result_array();
    }
     // cập nhật theo mảng
    public function capnhatMang($ma)
    {
        $this->db->where_in('mail_id',$ma);
        $this->db->set('mail_active',2);
        $this->db->update('tbl_mail');
        return $this->db->affected_rows();
    }
     // check trùng văn bản đến
    public function kiemtraTrung($sokyhieu,$loaivanban,$noigui)
    {
        $this->db->where('sTenDV',$noigui);
        $this->db->where('sKyHieu',$sokyhieu);
        $this->db->where('sTenLVB',$loaivanban);
        $this->db->select('sKyHieu,sTenLVB');
        return $this->db->get('tbl_vanbanden')->num_rows();
    }
    public function battrungvanbanden($kyhieu,$ngayky,$nguoiky)
    {
        $this->db->where('sKyHieu',$kyhieu);
        $this->db->where('sNgayKy',$ngayky);
        $this->db->where('sTenNguoiKy',$nguoiky);
        return $this->db->get('tbl_vanbanden')->num_rows();
    }
	public function kiemtraTrung1($sokyhieu,$ngayky,$nguoiky)
    {
        $this->db->where('sKyHieu',$sokyhieu);
        $this->db->where('sNgayKy',$ngayky);
        $this->db->where('sTenNguoiKy',$nguoiky);
        $this->db->where('iTrangThai !=',3);
        $this->db->select('sKyHieu,sTenLVB');
        return $this->db->get('tbl_vanbanden')->num_rows();
    }
	public function kiemtraTrung_VBD($sokyhieu,$ngayky,$nguoiky,$loaivanban)
    {
        $this->db->where('sKyHieu',$sokyhieu);
        $this->db->where('sNgayKy',$ngayky);
        $this->db->where('sTenNguoiKy',$nguoiky);
        $this->db->where('sTenLVB',$loaivanban);
        $this->db->where('iTrangThai !=',3);
        $this->db->select('sKyHieu,sTenLVB');
        return $this->db->get('tbl_vanbanden')->num_rows();
    }

    public function kiemtraTrungVBD($sokyhieu,$loaivanban,$noigui,$vanbanmat)
    {
        $this->db->where('sKyHieu',$sokyhieu);
        $this->db->where('sTenLVB',$loaivanban);
        $this->db->where('sTenDV',$noigui);
        $this->db->where('iVanBanMat',$vanbanmat);
        $this->db->where('iTrangThai !=',3);
        $this->db->select('sKyHieu,sTenLVB');
        return $this->db->get('tbl_vanbanden')->num_rows();
    }
    // lấy số đến cuối
    public function laySodenCuoi($year=NULL)
    {
        $this->db->where('tbl_vanbanden.sNgayNhap >=',$year.'-01-01');
        $this->db->select('iSoDen');
        $this->db->order_by('iSoDen','desc');
        $this->db->limit(1);
        return $this->db->get('tbl_vanbanden')->row_array();
    }
    // lấy lĩnh vực cha trong lĩnh vực quản lý đầu việc
    public function layLinhVucCha()
    {
        $this->db->where('id_parent',0);
        return $this->db->get('tbl_linhvuc_qldv')->result_array();
    }
    // lấy ngày mới nhất nhận mail
    public function layNgayMax()
    {
        $this->db->select('mail_date');
        $this->db->order_by('mail_date','desc');
        return $this->db->get('tbl_mail')->row_array();
    }
    // đếm số lượng bản ghi trong 1 bảng nào đó
    public function banghi($table)
    {
        return $this->db->get($table)->num_rows();
    }
    // đếm số bản ghi đã làm xong hoặc quá hạn
    public function banghi2($table,$trangthai)
    {
        $this->db->where('iTrangThai',$trangthai);
        return $this->db->get($table)->num_rows();
    }
    // kiểm tra quyên
    public function kiemtraQuyen($url,$primary_key,$id,$table)
    {
        $this->db->where('sLink',$url);
        $this->db->where_in($primary_key,$id);
        return $this->db->get($table)->result_array();
    }
    // lấy dữ liệu theo mảng
    public function layDuLieuMang($primary_key,$id,$table)
    {
        $this->db->where_in($primary_key,$id);
        $this->db->order_by('iDoUuTien','asc');
        return $this->db->get($table)->result_array();
    }
    // lấy dữ liệu theo tiêu chí tìm kiếm
    public function timkiemDuLieu($primary_key,$id,$table)
    {
        $this->db->like($primary_key,$id);
        return $this->db->get($table)->result_array();
    }
    // sét dữ liệu
    public function setDuLieu($primary_key,$id,$table,$val,$value)
    {
        $this->db->where($primary_key,$id);
        $this->db->set($val,$value);
        $this->db->update($table);
        return $this->db->affected_rows();
    }
    // bắt trùng dữ liệu
    public function battrungDuLieu($khoa,$dulieu,$table)
    {
        $this->db->where($khoa,$dulieu);
        return $this->db->get($table)->num_rows();

    }
    /**
     * thêm dữ liệu
     */
    public function themDuLieu($table,$data=array())
    {
        $this->db->insert($table,$data);
        return $this->db->affected_rows();
    }

    /**
     * cập nhật dữ liệu
     */
    public function capnhatDuLieu($primary_key,$id,$table,$data=array())
    {
        $this->db->where($primary_key,$id);
        $this->db->update($table,$data);
        return $this->db->affected_rows();
    }

    /**
     * cập nhật dữ liệu nhiêu
     */
    public function capnhatnhieuDuLieu($table,$data,$id)
    {
        $this->db->update_batch($table,$data,$id);
        return $this->db->affected_rows();
    }
    /**
     * lấy danh sách hoặc 1 dự liệu
     */
    public function layDuLieu($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        return $this->db->get($table)->result_array();
    }
    public function layDuLieudistinct($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        $this->db->distinct();
        $this->db->group_by('PK_iMaPB');
        return $this->db->get($table)->result_array();
    }
    /**
     * lấy danh sách hoặc 1 dự liệu với 2 điều kiện where
     */
    public function layDuLieu2($primary_key=NULL,$id=NULL,$primary_key1=NULL,$id1=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        if(!empty($id1))
        {
            $this->db->where($primary_key1,$id1);
        }
        return $this->db->get($table)->result_array();
    }    

    public function layDuLieu2_2($primary_key=NULL,$id=NULL,$primary_key1=NULL,$id1=NULL,$primary_key2=NULL,$id2=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        if(!empty($id1))
        {
            $this->db->where($primary_key1,$id1);
        }
        if(!empty($id2))
        {
            $this->db->where($primary_key2,$id2);
        }
        return $this->db->get($table)->result_array();
    }  
    
    /**
     * lấy danh sách hoặc 1 dự liệu với 2 điều kiện where
     */
     public function getData_kehoach($primary_key=NULL,$id=NULL,$primary_key1=NULL,$id1=NULL,$primary_key2=NULL,$id2=NULL,$primary_key3=NULL,$id3=NULL,$table,$sapxep=NULL)
    {
        $where ="($primary_key = $id and $primary_key1 < $id1 and $primary_key3 =$id3) or ($primary_key2 = $id2 and $primary_key = $id and $primary_key3 = $id3) ";
        $this->db->where($where); //pr($where);
        $this->db->order_by($sapxep,"asc");
        return $this->db->get($table)->result_array();
    }
    
     public function getData_kehoach_new($primary_key=NULL,$id=NULL,$primary_key1=NULL,$id1=NULL,$primary_key2=NULL,$id2=NULL,$primary_key3=NULL,$id3=NULL,$table,$sapxep=NULL)
    {
        $where ="(($primary_key = $id and $primary_key1 < $id1) or ($primary_key2 = $id2 and $primary_key = $id)) AND vanban_id not in(SELECT PK_iMaVBDen FROM tbl_vanbanden WHERE iTrangThai = 1 AND sNgayNhan >='2017-11-01')";
        $this->db->where($where);//pr($where);
        $this->db->order_by($sapxep,"asc");
        return $this->db->get($table)->result_array();
    }
     /**
     * lấy danh sách hoặc 1 dự liệu với 2 điều kiện where
     */
    public function get_kehoach3($where,$table,$sapxep=NULL)
    {
        $this->db->where($where);//echo $where."<br>";
        $this->db->order_by($sapxep,"asc");
        return $this->db->get($table)->result_array();
    }

    public function get_kehoach3a($where,$table,$sapxep=NULL,$sapxep1=NULL)
    {
        $this->db->where($where);//echo $where."<br>";
        $this->db->order_by($sapxep,"asc");
        $this->db->order_by($sapxep1,"DESC");
        return $this->db->get($table)->result_array();
    }

    public function get_kehoach3aa($where,$table,$sapxep=NULL,$sapxep1=NULL)
    {
        $this->db->where($where);//echo $where."<br>";
        $this->db->from('danhgiathang');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = danhgiathang.id_cb');
        $this->db->order_by('tbl_canbo.sapxep',"asc");
        return $this->db->get()->result_array();
    }

    public function get_kehoach3aaa($where,$table,$sapxep=NULL,$sapxep1=NULL)
    {
        $this->db->where($where);//echo $where."<br>";
        $this->db->from('danhgialanhdao');
        $this->db->join('tbl_canbo','tbl_canbo.PK_iMaCB = danhgialanhdao.canbo_id');
        $this->db->order_by('tbl_canbo.sapxep',"asc");
        return $this->db->get()->result_array();
    }

    
    // lấy dữ liệu 
    public function get_kehoach33($where,$table,$sapxep=NULL)
    {
        //$this->db->distinct();
        $this->db->where($where);//echo $where."<br>";
        $this->db->group_by('PK_iMaVBDen');
        $this->db->order_by($sapxep,"asc");
        return $this->db->get($table)->result_array();
    }


    /**
     * lấy danh sách hoặc 1 dự liệu với 2 điều kiện where or
     */
    public function layDuLieu3($primary_key=NULL,$id=NULL,$primary_key1=NULL,$id1=NULL,$table)
    {
        $where ="($primary_key = $id or $primary_key1 =$id1) and iTrangThai =0";
        $this->db->where($where);
        return $this->db->get($table)->result_array();
    }
    /**
     * lấy danh sách sắp xếp dự liệu
     */
    public function sapxepDuLieu($primary_key,$table,$order_by)
    {

        $this->db->order_by($primary_key,$order_by);
        return $this->db->get($table)->result_array();
    }
    /**
     *  lấy mã mới nhất của bảng
     */
    public function layMaMoiNhat($table,$primary_key)
    {
        $this->db->select("MAX(SUBSTRING($primary_key, 4)*1.0) AS max_id", FALSE);
        $this->db->from($table);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            return $query->row_array();
        }else {
            return 0;
        }
    }
    /**
     * insert_bacth
     */
    public function themNhieuDuLieu($table,$data)
    {
        $this->db->insert_batch($table,$data);
        return $this->db->affected_rows();
    }
    /**
     * sắp xếp có where
     */
    public function sapxepDuLieucoDK($primary_key,$id,$table,$id_order,$order_by)
    {
        $this->db->where($primary_key,$id);
        $this->db->order_by($id_order,$order_by);
        return $this->db->get($table)->result_array();
    }
    /**
     * Xóa dữ liệu
     */
    public function xoaDuLieu($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($primary_key))
        {
            $this->db->where($primary_key,$id);
        }
        else{
            $this->db->where(1,1);
        }
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
    /**
     * thêm dữ liệu
     */
    public function themDuLieu2($table,$data)
    {
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    // qldv
    // lấy dữ liệu theo mảng
    public function getWhereIn($primary_key,$id,$key_desc,$table)
    {
        $this->db->select('PK_iMaCB,sHoTen');
        $this->db->where('iTrangThai',0);
        $this->db->where_in($primary_key,$id);
        $this->db->order_by($key_desc,'desc');
        return $this->db->get($table)->result_array();
    }

    // lấy dữ liệu theo mảng
    public function getLinhvuc($table)
    {
        $this->db->where('linhVuc_active',1);
        $this->db->where('id_parent',0);
        return $this->db->get($table)->result_array();
    }

    public function getLinhvucSub($table)
    {
        $this->db->where('linhVuc_active',1);
        $this->db->where('id_parent >',0);
        return $this->db->get($table)->result_array();
    }
    public function getDepartment($table)
    {
        $this->db->select('PK_iMaPB,sTenPB');
        $this->db->where('iLoaiPhanMem',3);
        $this->db->where('iTrangThai',0);
        $this->db->order_by('PK_iMaPB','asc');
        return $this->db->get($table)->result_array();
    }

    //lay đầu việc con chi tiết
    public function getQldvDetail($primary_key,$id,$table)
    {
        $this->db->where($primary_key,$id);
        $this->db->where('qlv_id_sub',0);
        $this->db->order_by($primary_key,'asc');
        return $this->db->get($table)->result_array();
    }

    public function getQldvDetail1($primary_key,$id,$table)
    {
        $this->db->where($primary_key,$id);
        $this->db->where('qlv_id_sub >',0);
        $this->db->order_by($primary_key,'asc');
        return $this->db->get($table)->result_array();
    }
	public function layDuLieuVBDen($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
       $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',14);
        $this->db->select('tbl_vanbanden.PK_iMaVBDen as ID,tbl_vanbanden.sKyHieu as MA,tbl_vanbanden.sMoTa as TEN,tbl_vanbanden.sNoiDung as NOI_DUNG,tbl_vanbanden.sNgayKy as NGAY_BAN_HANH,tbl_vanbanden.sNgayKy as NGAY_CONG_BO,tbl_vanbanden.sNgayNhap as NGAY_TAO,tbl_vanbanden.sTenDV as CO_QUAN_BAN_HANH,tbl_files_vbden.sDuongDan as DUONGDAN_FILE');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen');
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        return $this->db->get()->result_array();
    }
	public function layDuLieuVBDi($primary_key=NULL,$id=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        $this->db->where('tbl_vanbandi.FK_iMaPB',14);
        $this->db->select('tbl_vanbandi.PK_iMaVBDi as ID,tbl_vanbandi.sKyHieu as MA,tbl_vanbandi.sMoTa as TEN,tbl_vanbandi.sNgayNhap as NGAY_TAO,tbl_phongban.sTenPB as CO_QUAN_BAN_HANH,sDuongDan as DUONGDAN_FILE');
        $this->db->from('tbl_vanbandi');
        $this->db->join('tbl_phongban','tbl_phongban.PK_iMaPB = tbl_vanbandi.FK_iMaPB');
        $this->db->join('tbl_files_vbdi','tbl_files_vbdi.FK_iMaVBDi = tbl_vanbandi.PK_iMaVBDi');
		$this->db->group_by('tbl_vanbandi.PK_iMaVBDi');
        return $this->db->get()->result_array();
    }
	public function capnhatDuLieu2($primary_key,$id,$primary_key1,$id1,$table,$data=array())
    {
        if(!empty($primary_key)){
            $this->db->where($primary_key,$id);
        }
        if(!empty($primary_key1)) {
            $this->db->where($primary_key1, $id1);
        }
        $this->db->update($table,$data);
        return $this->db->affected_rows();
    }
    
    // danh sách sáng tạo tuần
     public function sangtaotuan($where)
    {
        $query = $this->db->query('SELECT vanban_skh,kh_noidung,canbo_id,phong_id,tuan FROM kehoach WHERE  '.$where.' ORDER BY  phong_id,canbo_id ');
        return $query->result_array();
    }

    // danh sách sáng tạo tuần
     public function dsviechoanthanh($where)
    {
        $query = $this->db->query('SELECT vanban_skh,kh_noidung,sangtao,canbo_id,phong_id,tuan,ngay_hoanthanh,ngay_han FROM kehoach WHERE  '.$where.' ORDER BY  tuan ');
        return $query->result_array();
    }
    
    // count kế hoạch công tác tuần
     public function count_kehoach($where)
    {
        $query = $this->db->query('SELECT kh_id FROM kehoach WHERE  '.$where);
        return $query->num_rows();
    }
// count điểm tuần
     public function count_diem($field,$where)
    {
        $query = $this->db->query('SELECT '.$field.' FROM danhgia WHERE  '.$where);
        return $query->row_array();
    } 
// count điểm tháng
    public function count_thang($field,$where)
    {
        $query = $this->db->query('SELECT '.$field.' FROM danhgiathang WHERE  '.$where);
        return $query->row_array();
    } 
// count điểm quý
    public function count_quy($field,$where)
    {
        $query = $this->db->query('SELECT '.$field.' FROM danhgiaquy WHERE  '.$where);
        return $query->row_array();
    } 

// count điểm tuần
    public function countLD_thang($field,$where)
    {//pr($where);
        $query = $this->db->query('SELECT count(distinct('.$field.')) as tong FROM kehoach WHERE  '.$where." ");
        return $query->row_array();
    }    
// count điểm lãnh đạo tháng
    public function countTP_thang($field,$where)
    {
        $query = $this->db->query('SELECT '.$field.' FROM danhgialanhdao WHERE  '.$where);
        return $query->row_array();
    }
// count điểm lãnh đạo tháng
    public function countTP_quy($field,$where)
    {
        $query = $this->db->query('SELECT '.$field.' FROM danhgiaquy WHERE  '.$where);
        return $query->row_array();
    }
	public function layDuLieu3where($primary_key=NULL,$id=NULL,$primary_key1=NULL,$id1=NULL,$primary_key2=NULL,$id2=NULL,$table)
    {
        if(!empty($id))
        {
            $this->db->where($primary_key,$id);
        }
        if(!empty($id1))
        {
            $this->db->where($primary_key1,$id1);
        }
        if(!empty($id2))
        {
            $this->db->where($primary_key2,$id2);
        }
        return $this->db->get($table)->result_array();
    }


// danh sách quá hạn

    public function getDocCT_quahan($idDepartmentCT=NULL,$id_ld=NULL,$quyen=NULL)
    {
        
        if($quyen >= 6 || $quyen == 3)$this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >', 4);
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
            if($quyen ==7 || $quyen ==8){
                $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$id_ld);
            }else{
                $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            }

            $this->db->where("((tbl_vanbanden.sHanGiaiQuyet > '1970-01-01' and tbl_vanbanden.sHanGiaiQuyet < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =1) or (tbl_vanbanden.sHanThongKe  > '1970-01-01' and tbl_vanbanden.sHanThongKe  < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =2))");

            $this->db->where('tbl_vanbanden.iTrangThai !=',1);
            $this->db->where('tbl_vanbanden.iTrangThai !=',3);
            // xóa bản ghi được trả lại
            $this->db->where("tbl_chuyennhanvanban.iGiayMoi !=",3);

        }else{
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);

            $this->db->where("(((tbl_vanbanden.sGiayMoiNgay = '1970-01-01' or tbl_vanbanden.sGiayMoiNgay is NULL) and  tbl_vanbanden.iGiayMoi =2) or  (tbl_vanbanden.sGiayMoiNgay >= '2017-11-06' and tbl_vanbanden.iGiayMoi =1))");
            if($id_ld > 0)$this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$id_ld);

            $this->db->where("((tbl_vanbanden.sHanThongKe < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =1) or (tbl_vanbanden.sHanThongKe  < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =2))");
            $this->db->where("((tbl_vanbanden.sHanGiaiQuyet > '1970-01-01' and tbl_vanbanden.iGiayMoi =1) or (tbl_vanbanden.sHanThongKe  > '1970-01-01' and tbl_vanbanden.iGiayMoi =2))");

            $this->db->where('tbl_vanbanden.iTrangThai !=',1);
            $this->db->where('tbl_vanbanden.iTrangThai !=',3);
             // xóa bản ghi được trả lại
            $this->db->where("tbl_chuyennhanvanban.iGiayMoi !=",3);
        }

        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }



    
     /** danh sách chủ trì */
    public function getDocCT($trangthai_truyennhan = NULL, $idDepartment = NULL,$idDepartment1=NULL,$Chuyenvien=NULL,$loaivanban=NULL,$sokyhieu=NULL,$ngaynhap=NULL,$donvi=NULL,$ngayky=NULL,$ngayden=NULL,$trichyeu=NULL,$nguoiky=NULL,$soden=NULL,$denngay=NULL,$chucvu=NULL,$nguoinhap=NULL,$limit=NULL,$offset=NULL,$idDepartmentCT=NULL,$cacloaivanban=NULL,$id_ld=NULL,$quyen=NULL)
    {
        if($trangthai_truyennhan){
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan',$trangthai_truyennhan);
        }
        // $this->db->where_not_in('tbl_vanbanden.sTenLVB',array('Đơn thư','Đơn tố cáo','Đơn khiếu nại','Đơn Kháng cáo','Đơn Kiến nghị','Đơn kêu cứu','Đơn đề nghị'));
        $this->db->where('tbl_vanbanden.sNgayNhap > ','2017-04-01');
        if($quyen >= 6 || $quyen == 3)$this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan >', 4);
//        $this->db->where('tbl_vanbanden.iGiayMoi','2');
        $this->db->select('tbl_vanbanden.sMoTa,tbl_vanbanden.sKyHieu,tbl_vanbanden.iSoDen,tbl_vanbanden.sTenDV,tbl_vanbanden.sNgayNhan,iFile,tbl_vanbanden.PK_iMaVBDen,tbl_vanbanden.iTrangThai_TruyenNhan,tbl_vanbanden.iTrangThai,tbl_vanbanden.iGiayMoi,tbl_vanbanden.sHanGiaiQuyet,tbl_vanbanden.sNgayNhap,tbl_vanbanden.sNgayKy,tbl_vanbanden.sHanThongKe,tbl_vanbanden.sNoiDung,tbl_vanbanden.sGiayMoiGio,tbl_vanbanden.sGiayMoiNgay,tbl_vanbanden.sGiayMoiDiaDiem,tbl_vanbanden.sHanNoiDung,tbl_files_vbden.sDuongDan,count(tbl_files_vbden.FK_iMaVBDen) as dem,iTrangThai_ThemHan');
        $this->db->from('tbl_vanbanden');
        $this->db->join('tbl_files_vbden','tbl_files_vbden.FK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');

        if(!empty($idDepartment)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$idDepartment);
        }
        if(!empty($idDepartment1)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartment1);
        }
        if(!empty($idDepartmentCT)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
            if($quyen ==7 || $quyen ==8){
                $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$id_ld);
            }else{
            	$this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            }
//giấy mời chưa xử lý tính từ ngày 06/11/2017
          // $this->db->where("(((tbl_vanbanden.sGiayMoiNgay = '1970-01-01' or tbl_vanbanden.sGiayMoiNgay is NULL) and  tbl_vanbanden.iGiayMoi =2) or  (tbl_vanbanden.sGiayMoiNgay >= '2017-11-06' and tbl_vanbanden.iGiayMoi =1))");
            //$this->db->where("tbl_chuyennhanvanban.sThoiGianHetHan < ",date('Y-m-d'));
            //$this->db->where("tbl_chuyennhanvanban.sThoiGianHetHan > '1970-01-01'");
            $this->db->where("((tbl_vanbanden.sHanGiaiQuyet < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =1) or (tbl_vanbanden.sHanThongKe  < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =2))");
        	$this->db->where("((tbl_vanbanden.sHanGiaiQuyet > '1970-01-01' and tbl_vanbanden.iGiayMoi =1) or (tbl_vanbanden.sHanThongKe  > '1970-01-01' and tbl_vanbanden.iGiayMoi =2))");
            $this->db->where('tbl_vanbanden.iTrangThai !=',1);
            $this->db->where('tbl_vanbanden.iTrangThai !=',3);
            // xóa bản ghi được trả lại
            $this->db->where("tbl_chuyennhanvanban.iGiayMoi !=",3);

        }else{
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
//            $this->db->where('tbl_chuyennhanvanban.PK_iMaPhongCT',$idDepartmentCT);
            $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan !=',2);
//giấy mời chưa xử lý tính từ ngày 06/11/2017
            $this->db->where("(((tbl_vanbanden.sGiayMoiNgay = '1970-01-01' or tbl_vanbanden.sGiayMoiNgay is NULL) and  tbl_vanbanden.iGiayMoi =2) or  (tbl_vanbanden.sGiayMoiNgay >= '2017-11-06' and tbl_vanbanden.iGiayMoi =1))");
            if($id_ld > 0)$this->db->where('tbl_chuyennhanvanban.PK_iMaCBNhan',$id_ld);
            //$this->db->where("tbl_chuyennhanvanban.sThoiGianHetHan < ",date('Y-m-d'));
            //$this->db->where("tbl_chuyennhanvanban.sThoiGianHetHan > '1970-01-01'");
            $this->db->where("((tbl_vanbanden.sHanThongKe < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =1) or (tbl_vanbanden.sHanThongKe  < '".date('Y-m-d')."' and tbl_vanbanden.iGiayMoi =2))");
            $this->db->where("((tbl_vanbanden.sHanGiaiQuyet > '1970-01-01' and tbl_vanbanden.iGiayMoi =1) or (tbl_vanbanden.sHanThongKe  > '1970-01-01' and tbl_vanbanden.iGiayMoi =2))");

            $this->db->where('tbl_vanbanden.iTrangThai !=',1);
            $this->db->where('tbl_vanbanden.iTrangThai !=',3);
             // xóa bản ghi được trả lại
            $this->db->where("tbl_chuyennhanvanban.iGiayMoi !=",3);
        }

        if(!empty($Chuyenvien)){
            $this->db->join('tbl_chuyennhanvanban','tbl_chuyennhanvanban.PK_iMaVBDen = tbl_vanbanden.PK_iMaVBDen','left');
            $this->db->where('tbl_chuyennhanvanban.PK_iMaCVCT',$Chuyenvien);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($sokyhieu)){
            $this->db->like('tbl_vanbanden.sKyHieu',$sokyhieu);
        }
        if(!empty($ngaynhap)){
            $this->db->like('tbl_vanbanden.sNgayNhap',$ngaynhap);
        }
        if(!empty($donvi)){
            $this->db->like('tbl_vanbanden.sTenDV',$donvi);
        }
        if(!empty($ngayky)){
            $this->db->like('tbl_vanbanden.sNgayKy',$ngayky);
        }
        if(!empty($ngayden)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$ngayden);
        }
        if(!empty($trichyeu)){
            $this->db->like('tbl_vanbanden.sMoTa',$trichyeu);
        }
        if(!empty($nguoiky)){
            $this->db->like('tbl_vanbanden.sTenNguoiKy',$nguoiky);
        }
        if(!empty($loaivanban)){
            $this->db->like('tbl_vanbanden.sTenLVB',$loaivanban);
        }
        if(!empty($soden)){
            $this->db->like('tbl_vanbanden.iSoDen',$soden);
        }
        if(!empty($denngay)){
            $this->db->like('tbl_vanbanden.sHanGiaiQuyet',$denngay);
        }
        if(!empty($chucvu)){
            $this->db->like('tbl_vanbanden.sChucVu',$chucvu);
        }
        if(!empty($nguoinhap)){
            $this->db->like('tbl_vanbanden.FK_iMaNguoiNhap',$nguoinhap);
        }
        if(!empty($cacloaivanban)){
            if($cacloaivanban == '1'){
                $this->db->where('tbl_vanbanden.iTrangThai_TruyenNhan','6');
                $this->db->or_where('tbl_vanbanden.iTrangThai_TruyenNhan','7');
            }
        }
        if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        $this->db->group_by('tbl_vanbanden.PK_iMaVBDen');
        $this->db->order_by('tbl_vanbanden.iSoDen','desc');
//        return $this->db->last_query();
        return $this->db->get()->result_array();
    }
	// lấy quyền khi có id cán bộ
	public function get_quyen($id)
	    {
	        $query = $this->db->query('SELECT iQuyenHan_DHNB FROM tbl_canbo WHERE  PK_iMaCB='.$id);
	        return $query->row_array();
	    }
	    // lấy phòng khi có id cán bộ
	public function get_phong($id)
	    {
	        $query = $this->db->query('SELECT FK_iMaPhongHD FROM tbl_canbo WHERE  PK_iMaCB='.$id);
	        return $query->row_array();
	    } 
            // lấy phòng khi có id cán bộ
    public function get_ten($id)
        {
            $query = $this->db->query('SELECT sHoTen FROM tbl_canbo WHERE  PK_iMaCB='.$id);
            return $query->row_array();
        } 
	    
    // lấy file cuối trong bang kehoach_file
    public function getfile($kh_id,$table)
    {
        $this->db->select('file_upload');
        $this->db->where('kehoach_id',$kh_id);
        $this->db->where('chutri',1);
        $this->db->order_by('kq_id','desc');
        $this->db->limit(1);
        
        return $this->db->get($table)->row_array();
    }

    public function getfile1($kh_id,$table)
    {
        $this->db->where('kehoach_id',$kh_id);
        $this->db->where('chutri',2);
        $this->db->group_by('canbo_id');        
        return $this->db->get($table)->result_array();
    }
    
    public function getFileLD($where,$table)
    {
        $this->db->where($where);
        return $this->db->get($table)->result_array();
    }

    public function getFileLD_One($where,$table)
    {
        $this->db->where($where);
        $this->db->order_by('kq_id','desc');
        $this->db->limit(1);
        return $this->db->get($table)->result_array();
    }
    
    public function get_kehoach5($where,$table,$sapxep=NULL)
    {
        $this->db->where($where);//echo $where;
        $this->db->order_by($sapxep,"desc");
        return $this->db->get($table)->result_array();
    }
    
     public function get_kehoach7($where,$table,$sapxep=NULL)
    {
        $query = $this->db->query('SELECT kh_id FROM '.$table.' WHERE  '.$where);
        return $query->num_rows();
    }

     /**
     * Xóa dữ liệu
     */
    public function xoa_kehoach_file($where=NULL,$table)
    {
        $query = $this->db->query('DELETE  FROM '.$table.' WHERE '.$where);
        return 1;
    }

    public function capNhatKetQua($id){
        $this->db->query('update kehoach_file set active = 2   WHERE chutri = 2 and kehoach_id = '.$id);
    }
// lay tuan lon nhat da cham diem
     public function getTuanMax($canbo_id)
    {
        $this->db->select('tuan');
        $this->db->where('canbo_id',$canbo_id);
        $this->db->order_by('danhgia_id','desc');
        return $this->db->get('danhgia')->row_array();
    }
// cập nhật quá hạn
    public function quahan($kh_id,$tuan)
    {
        $this->db->where('kh_id',$kh_id);
        $this->db->where('tuan',$tuan);
        $this->db->set('qua_han',2);
        $this->db->update('kehoach');
        return $this->db->affected_rows();
    }

	/**
     * cập nhật dữ liệu kế hoạch công tác
     */
    public function capnhatDuLieuKHCT($primary_key,$id,$table,$data=array())
    {
        $this->db->where($primary_key,$id);
        $this->db->or_where("kh_id_sub",$id);
        $this->db->update($table,$data);
        return $this->db->affected_rows();
    }


    public function laySoDen($Colums,$Value,$table)
        {
            $this->db->where($Colums,$Value);
            return $this->db->get($table)->row_array()['iSoDen'];
        }

    public function layVBQuaHanThang($where)
    {//pr($where);
        $query = $this->db->query('SELECT kh_id FROM kehoach WHERE  '.$where);
        return $query->result_array();
    } 

     /**
     * lấy danh sách hoặc 1 dự liệu với 2 điều kiện where
     */
    public function getvanbandi($where,$table)
    {
        $query = $this->db->query('SELECT * FROM '.$table.' WHERE  '.$where);
        return $query->result_array();
    } 

    //cập nhật kết quả công tác tuần

    public function capnhattuan($sql)
    {
        $query = $this->db->query($sql);
        //return $query->result_array();
    }
        
    
}

/* End of file Mdanhmuc.php */
/* Location: ./application/models/danhmuc/Mdanhmuc.php */