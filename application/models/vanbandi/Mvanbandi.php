<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvanbandi extends CI_Model {
	// lấy thông tin đơn vị ngoài
	public function layTT_DVNgoai($donvi){
		$this->db->where_in('PK_iMaTT',$donvi);
		$this->db->select('*');
		return $this->db->get('tbl_thongtin_donvi')->result_array();
	}
	// lấy thông tin văn bản đến theo số đến
	public function layTT_VBDen($soden){
		$this->db->where_in('iSoDen',$soden);
		$this->db->where('sMaCoQuan !=','');
		$this->db->select('sKy_Hieu,sNgay_Ky,sMaCoQuan,sTenCoQuan');
		$this->db->from('tbl_vanbanden');
		return $this->db->get()->result_array();
	}
	// lấy thông tin đơn vị
	public function layTT_DV($ma){
		$this->db->where_in('PK_iMaTT',$ma);
		return $this->db->get('tbl_thongtin_donvi')->result_array();
	}
	// lấy lãnh đạo sở xem để biết
	public function layLDSo_Xem($taikhoan,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaCB',$taikhoan);
		$this->db->where('iSoVBDi',0);
		$this->db->select('PK_iMaVBDi,FK_iMaCB_Gui,FK_iMaCB_Nhan,sThoiGian,sYKien,sKyHieu,sMoTa,FK_iMaCB_Nhap,iSoVBDi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		$this->db->join('tbl_lanhdaoxem as ld','ld.FK_iMaVB=vb.PK_iMaVBDi');
		$this->db->order_by('PK_iMaVBDi','desc');
		$this->db->group_by('PK_iMaVBDi');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	public function demLDSo_Xem($taikhoan)
	{
		$this->db->where('FK_iMaCB',$taikhoan);
		$this->db->where('iSoVBDi',0);
		$this->db->select('PK_iMaVBDi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_lanhdaoxem as ld','ld.FK_iMaVB=vb.PK_iMaVBDi');
		$this->db->group_by('PK_iMaVBDi');
		return $this->db->get()->num_rows();
	}
	// kiểm tra đã xử lý chưa
	public function kiemtra_XuLy($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('iTrangThai',1);
		return $this->db->get('tbl_luuvet_vbdi')->num_rows();
	}
	// lấy file 
	public function layfilevanbandi($mavanban)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->select('sDuongDan');
		$this->db->order_by('PK_iMaFileDi','desc');
		return $this->db->get('tbl_files_vbdi')->row_array();
	}
	// lấy thong tin văn bản đi
	public function laythongtinvanbandi($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('PK_iMaVBDi,FK_iMaPB,sMoTa,sTenLVB,nk.sHoTen as nguoiky,nh.sHoTen as nguoinhap');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_loaivanban as l','l.PK_iMaLVB=vb.FK_iMaLVB');
		$this->db->join('tbl_canbo as nk','nk.PK_iMaCB = vb.FK_iMaCB_Ky');
		$this->db->join('tbl_canbo as nh','nh.PK_iMaCB = vb.FK_iMaCB_Nhap');
		return $this->db->get()->row_array();
	}
	// lấy cán bộ
	public function layCB()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('sHoTen,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy mail nhóm
	public function laynhomMail($nhom)
	{
		$this->db->where('mail_group',$nhom);
		$this->db->where('mail_cha',0);
		$this->db->where('sSDT !=','');
		$this->db->select('sSDT');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}
	// đếm danh sách văn bản và giấy mời của phòng
	public function demDSVB_Phong($phongban,$loaivanban)
	{
		$this->db->where('FK_iMaPB',$phongban);
		$this->db->where('iSoVBDi > 0');
		if($loaivanban==10)
		{
			$this->db->where('FK_iMaLVB',10);
		}
		else
		{
			$this->db->where('FK_iMaLVB !=',10);
		}
		$this->db->select('PK_iMaVBDi');
		return $this->db->get('tbl_vanbandi')->num_rows();
	}
	public function laynoinhanmail($trangthai){ // 1: quận huyện 2: sở ban ngành
		$this->db->where('iTrangThai',0);
		$this->db->where('mail_group',$trangthai);
		$this->db->select('sEmail,sTenDV');
		$this->db->order_by('mail_cha','desc');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}
	public function laydsemailnhan(){ // 1: quận huyện 2: sở ban ngành
		$this->db->where('iTrangThai',0);
		$this->db->select('sEmail,sTenDV');
		$this->db->order_by('mail_cha','desc');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}

	// lấy đơn vị nhận
	public function layDonViNhanMail()
	{
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaEmailDV,sTenDV');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}
	// lấy đơn vị nhận mail
	public function MailGuiDi($madonvi)
	{
		$this->db->where_in('PK_iMaEmailDV',$madonvi);
		$this->db->select('sEmail');
		return $this->db->get('tbl_emaildonvi')->result_array();
	}
	// lấy đơn vị nhận mail qua mã văn bản đi
	public function layDVQuaMa($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('FK_iMaNoiNhan');
		return $this->db->get('tbl_vanbandi')->row_array();
	}
	// lấy thông tin văn bản theo mã
	public function layThongTinTheoMaVB1($ma)
	{
		$this->db->where('PK_iMaVBDi',$ma);
		$this->db->select('PK_iMaVBDi,sHoTen,sTenLVB,vbd.sMoTa,vbd.iSoTrang,vbd.FK_iMaDV_Ngoai,vbd.iSoDen,sKyHieu,iSoVBDi,sNgayVBDi,vbd.sNoiNhan,cv.sTenCV,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('vanban_'.date("Y").'.tbl_vanbandi as vbd');
		$this->db->join('vanban_'.date("Y").'.tbl_canbo as cb','cb.PK_iMaCB=vbd.FK_iMaCB_Ky');
		$this->db->join('vanban_'.date("Y").'.tbl_chucvu as cv','cv.PK_iMaCV=cb.FK_iMaCV');
		$this->db->join('vanban_'.date("Y").'.tbl_loaivanban as lvb','lvb.PK_iMaLVB=vbd.FK_iMaLVB');
		return $this->db->get()->row_array();
	}

	// lấy thông tin văn bản theo mã
	public function layThongTinTheoMaVB($ma)
	{
		$this->db->where('PK_iMaVBDi',$ma);
		$this->db->select('sHoTen,sTenLVB,vbd.sMoTa,vbd.iSoTrang,vbd.FK_iMaDV_Ngoai,vbd.iSoDen,sKyHieu,iSoVBDi,sNgayVBDi,vbd.sNoiNhan,cv.sTenCV,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('tbl_vanbandi as vbd');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vbd.FK_iMaCB_Ky');
		$this->db->join('tbl_chucvu as cv','cv.PK_iMaCV=cb.FK_iMaCV');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB=vbd.FK_iMaLVB');
		return $this->db->get()->row_array();
	}
	// lấy văn bản chờ số
	public function layVanBanChoSo($loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$ngaythang=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$email=NULL,$duthaoubnd=NULL,$trangthai,$giaymoi=NULL,$limit=NULL,$offset=NULL)
	{
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}	
		$this->db->where('iSoVBDi',0);
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('sNgayVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function layVanBanChoPH($loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$ngaythang=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$email=NULL,$duthaoubnd=NULL,$trangthai,$giaymoi=NULL,$limit=NULL,$offset=NULL)
	{
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		if(!empty($ngaythang))
		{
			$this->db->where('sNgayVBDi',$ngaythang);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}	
		$this->db->where('iSoVBDi',0);
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('sNgayVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	 public function laysoluongdaduyet()
    {
        $query = $this->db->query('SELECT COUNT(*) as count FROM tbl_luuvet_vbdi JOIN tbl_vanbandi ON tbl_vanbandi.PK_iMaVBDi = tbl_luuvet_vbdi.FK_iMaVBDi  WHERE iTrangThai = 3 AND tbl_vanbandi.iSoVBDi = 0;');
        return $query->result_array();
    }
	
	 public function layvbdidaduyet()
    {
        $query = $this->db->query('SELECT tbl_vanbandi.* as count FROM tbl_luuvet_vbdi JOIN tbl_vanbandi ON tbl_vanbandi.PK_iMaVBDi = tbl_luuvet_vbdi.FK_iMaVBDi  WHERE iTrangThai = 3 AND tbl_vanbandi.iSoVBDi = 0;');
        return $query->result_array();
    }
	
	// lấy mã văn bản theo số đến
	public function layMaVBDen($soden)
	{
		$this->db->where('iGiayMoi',2);
		$this->db->where_in('iSoDen',$soden);
		$this->db->select('iSoDen,PK_iMaVBDen,sKyHieu,sMoTa,sTenDV,sTenNguoiKy,sNgayKy');
		return $this->db->get('tbl_vanbanden')->result_array();
	}
	// lấy nơi dự thảo  với loại phần mềm là điều hành nội bộ
	public function layPhongBanDuThao()
	{
		$phanmem = array(1,3);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iLoaiPhanMem',$phanmem);
		$this->db->select('sTenPB,PK_iMaPB');
		$this->db->order_by('sTenPB');
		return $this->db->get('tbl_phongban')->result_array();
	}
	// ====================================== Văn bản đi =================================
	// lấy mã mới nhất để cập nhật
	public function layMaMoiNhat_CN($mavanban,$taikhoan, $year)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->select('PK_iMaNhatKy');
		$this->db->order_by('PK_iMaNhatKy','desc');
		return $this->db->get('songoaivu_'.$year.'.tbl_luuvet_vbdi')->row_array();
	}
	public function layMaMoiNhat_CN2($mavanban, $year)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->select('PK_iMaNhatKy');
		$this->db->order_by('PK_iMaNhatKy','desc');
		return $this->db->get('songoaivu_'.$year.'.tbl_luuvet_vbdi')->row_array();
	}
	public function layMaMoiNhat_Cu($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->select('PK_iMaNhatKy');
		$this->db->order_by('PK_iMaNhatKy','asc');
		return $this->db->get('tbl_luuvet_vbdi')->row_array();
	}
	// lấy người trả lại
	public function laynguoitralai($mavanban,$ma)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('PK_iMaNhatKy <=',$ma);
		$this->db->where('FK_iMaCB_Gui >', 695);
		$this->db->select('FK_iMaCB_Gui');
		$this->db->group_by('FK_iMaCB_Gui');
		return $this->db->get('tbl_luuvet_vbdi')->result_array();
	}
	// sét trạng thá chờ số
	public function capnhatTrangThaiChoSo($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->set('trangthai_chuyen',8);
		$this->db->update('tbl_vanbandi');
	}
	// lấy chuyên viên trả lại
	public function layCV_TraLai($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('phong_lanhdao',1);
		$this->db->where('FK_iMaCB_Gui !=',$taikhoan);
		$this->db->select('FK_iMaCB_Gui');
		return $this->db->get('tbl_luuvet_vbdi')->row_array();
	}
	// cập nhật cái mk gửi
	public function capnhatVBDi_Gui($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('FK_iMaCB_Gui',$taikhoan);
		$this->db->set('iTrangThai',2);
		$this->db->update('tbl_luuvet_vbdi');
	}
	// lấy phó phòng chuyên viên
	public function layCV_PP_TraLai($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('phong_lanhdao',1);
		$this->db->where('FK_iMaCB_Gui !=',$taikhoan);
		$this->db->select('FK_iMaCB_Gui');
		$this->db->group_by('FK_iMaCB_Gui');
		return $this->db->get('tbl_luuvet_vbdi')->result_array();
	}
	// cập nhật trạng thái đã xử lý cho văn bản
	public function capnhatTrangThai_XuLy($mavanban,$id_insert)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('phong_lanhdao',1);
		$this->db->where('PK_iMaNhatKy !=',$id_insert);
		$this->db->set('iTrangThai',2);
		$this->db->update('tbl_luuvet_vbdi');
	}
	// cập nhật trạng thái đã xử lý cho văn bản
	public function capnhatTrangThai_VB($mavanban)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->set('iTrangThai',2);
		$this->db->update('tbl_luuvet_vbdi');
	}
	// cập nhật trạng thái đã xử lý cho văn bản
	public function capnhatTrangThai_XuLy_TL($mavanban,$taikhoan)
	{
		$this->db->where('FK_iMaVBDi',$mavanban);
		$this->db->where('iTrangThai',1);
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->set('iTrangThai',2);
		$this->db->update('tbl_luuvet_vbdi');
	}
	// cập nhật trang thái xử lý
	public function capnhatTrangThai_XuLy_LD($ma, $year)
	{
		$this->db->where('PK_iMaNhatKy',$ma);
		$this->db->set('iTrangThai',3);
		$this->db->update('songoaivu_'.$year.'.tbl_luuvet_vbdi');
		return $this->db->affected_rows();
	}
	// lấy người ký cho nhật mới văn bản đi với quyền là 3:chánh văn phòng 4:giám đốc 5:phó giám đốc
	public function layNguoiKy()
	{
		$quyen = array(3,4,5,6,7);
		$this->db->where('FK_iMaCV !=',5);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iQuyenHan_DHNB',$quyen);
		$this->db->select('sHoTen,PK_iMaCB,iQuyenHan_DHNB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	public function layNguoiKyHai()
	{
		$quyen = array(3,4,5,6,7);
		$taikhoan = array(709,712,721,725);
		
		$this->db->where('FK_iMaCV !=',5);
		$this->db->where('iTrangThai',0);
		$this->db->where_in('iQuyenHan_DHNB',$quyen);
		$this->db->or_where_in('PK_iMaCB',$taikhoan);
		$this->db->select('sHoTen,PK_iMaCB,iQuyenHan_DHNB');
		//$this->db->order_by('iQuyenHan_DHNB','asc');
		//$this->db->order_by('PK_iMaCB','desc');
		//$this->db->order_by('sHoTen','asc');
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy trưởng phòng và phó phòng
	public function layLD_Phong($phongban,$quyen=NULL,$chucvu=NULL,$quyendb=NULL)
	{
		$this->db->where('PK_iMaCB !=','178');
		$this->db->where('FK_iMaPhongHD',$phongban);
		if(!empty($quyen))
		{
			$this->db->where_in('iQuyenHan_DHNB',$quyen);
		}
		if(!empty($chucvu))
		{
			$this->db->where_in('FK_iMaCV',$chucvu);
		}
		if(!empty($quyendb))
		{

			$this->db->where('iQuyenDB',$quyendb);
		}
		$this->db->where('iTrangThai',0);
		$this->db->select('PK_iMaCB,sHoTen,iQuyenHan_DHNB');
		if($phongban==12)
		{
			$this->db->order_by('iQuyenHan_DHNB','desc');
		}
		else{
			$this->db->order_by('iQuyenHan_DHNB','asc');
		}
		return $this->db->get('tbl_canbo')->result_array();
	}
	// lấy văn bản chờ xử lý
	public function layVBDi_ChoXuLy($taikhoan,$trangthai,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('iTrangThai',$trangthai);
		$this->db->where('iGuiMail', 0);
		//$this->db->where('iSoVBDi',0);
		$this->db->select('vb.*,PK_iMaVBDi,FK_iMaCB_Gui,FK_iMaCB_Nhan,sThoiGian,sYKien');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		$this->db->order_by('vb.sNgayNhap','desc');

		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function layVBDi_ChoXuLy2($taikhoan,$trangthai,$limit=NULL,$offset=NULL)
	{
		$query = $this->db->query('SELECT vb.*, FK_iMaCB_Gui, FK_iMaCB_Nhan, sThoiGian, sYKien FROM tbl_vanbandi vb JOIN tbl_luuvet_vbdi AS lv ON lv.FK_iMaVBDi = vb.PK_iMaVBDi WHERE (select FK_iMaCB_Nhan from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by sThoigian desc limit 1) = '.$taikhoan.' && (SELECT iTrangThai FROM tbl_luuvet_vbdi WHERE FK_iMaVBDi = vb.PK_iMaVBDi ORDER BY iTrangThai DESC LIMIT 1) != 3 group by PK_imavbdi order by sthoigian desc ;');
        
		 if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        return $query->result_array();
		
	}
	
	public function layVBDi_daphathanh_chuaduyet($taikhoan,$quyen,$trangthai,$limit=NULL,$offset=NULL)
	{
		if ($quyen == 4) {
			$query = $this->db->query('SELECT *, (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) as iTrangThai FROM tbl_vanbandi where (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) != 3 && iGuiMail = 1 order by sNgayVBDi;');
        } else {
			$query = $this->db->query('SELECT *, (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) as iTrangThai FROM tbl_vanbandi where (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) != 3 && iGuiMail = 1 && FK_iMaCB_Ky = '.$taikhoan.' order by sNgayVBDi;');
		}
		
		 if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
		
        return $query->result_array();
	}
	
	public function demVBDi_daphathanh_chuaduyet($taikhoan,$quyen,$trangthai)
	{
		if ($quyen == 4) {
			$query = $this->db->query('SELECT *, (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) as iTrangThai FROM tbl_vanbandi where (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) != 3 && iGuiMail = 1 order by sNgayVBDi;');
        } else {
			$query = $this->db->query('SELECT *, (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) as iTrangThai FROM tbl_vanbandi where (select max(itrangthai) from tbl_luuvet_vbdi where FK_iMaVBDi = PK_iMaVBDi) != 3 && iGuiMail = 1 && FK_iMaCB_Ky = '.$taikhoan.' order by sNgayVBDi;');
		}
		return $query->num_rows();
	}
	
	// đếm văn bản chờ xử lý
	public function demVBDi_ChoXuLy($taikhoan,$trangthai)
	{
		$this->db->where('FK_iMaCB_Nhan',$taikhoan);
		$this->db->where('iTrangThai',$trangthai);
		$this->db->where('iGuiMail', 0);
		//$this->db->where('iSoVBDi',0);
		$this->db->select('FK_iMaCB_Nhan,sThoiGian,sYKien,PK_iMaVBDi,sKyHieu,sMoTa,iSoVBDi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		return $this->db->get()->num_rows();
	}
	
	public function demVBDi_ChoXuLy2($taikhoan,$trangthai)
	{
		$query = $this->db->query('SELECT vb.*, FK_iMaCB_Gui, FK_iMaCB_Nhan, sThoiGian, sYKien FROM tbl_vanbandi vb JOIN tbl_luuvet_vbdi AS lv ON lv.FK_iMaVBDi = vb.PK_iMaVBDi WHERE (select FK_iMaCB_Nhan from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by sThoigian desc limit 1) = '.$taikhoan.' && (SELECT iTrangThai FROM tbl_luuvet_vbdi WHERE FK_iMaVBDi = vb.PK_iMaVBDi ORDER BY iTrangThai DESC LIMIT 1) != 3 group by PK_imavbdi order by sthoigian desc ;');
        return $query->num_rows();
		
	}

	// lấy văn bản đã trình ký
	public function layVBDi_DaTrinhKy($taikhoan)
	{
		$this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->where('trangthai_chuyen >=',8);
		$this->db->select('PK_iMaVBDi,sKyHieu,sMoTa,FK_iMaCB_Nhap,iSoVBDi,sNoiNhan,sNgayNhap');
		return $this->db->get('tbl_vanbandi')->result_array();
	}
	// đếm văn bản đã trình ký
	public function demVBDi_DaTrinhKy($taikhoan)
	{
		$this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->where('trangthai_chuyen >=',8);
		$this->db->select('PK_iMaVBDi');
		return $this->db->get('tbl_vanbandi')->num_rows();
	}
	// lấy văn bản đã xử lý
	public function layVBDi_DaXuLy($taikhoan,$trangthai,$limit=NULL,$offset=NULL)
	{
		$this->db->where("(FK_iMaCB_Nhan = $taikhoan and (iTrangThai = $trangthai or iSoVBDi >0)) or (FK_iMaCB_Gui = $taikhoan and  iTrangThai = $trangthai) or (iSoVBDi >0 and phogiamdoc = $taikhoan) or (iSoVBDi >0 and giamdoc = $taikhoan)",NULL);
		$this->db->select('PK_iMaVBDi,FK_iMaCB_Gui,FK_iMaCB_Nhan,sThoiGian,sYKien,sKyHieu,sMoTa,FK_iMaCB_Nhap,iSoVBDi,sNoiNhan,sNgayNhap');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		$this->db->group_by('FK_iMaVBDi');
		$this->db->order_by('vb.sNgayNhap','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm văn bản chờ xử lý
	public function demVBDi_DaXuLy($taikhoan,$trangthai)
	{
		$this->db->where("(FK_iMaCB_Nhan = $taikhoan and (iTrangThai = $trangthai or iSoVBDi >0)) or (FK_iMaCB_Gui = $taikhoan and  iTrangThai = $trangthai) or (iSoVBDi >0 and phogiamdoc = $taikhoan) or (iSoVBDi >0 and giamdoc = $taikhoan)",NULL);
		$this->db->select('FK_iMaCB_Nhan,sThoiGian,sYKien,PK_iMaVBDi,sKyHieu,sMoTa,iSoVBDi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		$this->db->group_by('FK_iMaVBDi');
		return $this->db->get()->num_rows();
	}
	// lấy văn bản đã xử lý
	public function layVBDi_UserTao($taikhoan,$trangthai,$limit=NULL,$offset=NULL)
	{
		$this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		$this->db->group_by('FK_iMaVBDi');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function demVBDi_UserTao($taikhoan,$trangthai)
	{
		$this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_luuvet_vbdi as lv','lv.FK_iMaVBDi=vb.PK_iMaVBDi');
		$this->db->group_by('FK_iMaVBDi');
		return $this->db->get()->num_rows();
	}
	
	// ====================================== Văn bản đi =================================
	// lấy chức vụ người ký
	public function layChucVu($ma)
	{
		$this->db->where('PK_iMaCB',$ma);
		$this->db->select('PK_iMaCV,sTenCV');
		$this->db->from('tbl_canbo as cb');
		$this->db->join('tbl_chucvu as cv','cv.PK_iMaCV=cb.FK_iMaCV');
		return $this->db->get()->row_array();
	}
		/**
		* =============================== VĂN BẢN CHỜ SỐ =================================
		*/
	// lấy văn bản của phòng: giấy mời: 10
	public function layVBGMPHong($giaymoi,$phongban)
	{
		if($giaymoi==10)
		{
			$this->db->where('FK_iMaLVB',10);
		}
		else{
			$this->db->where('FK_iMaLVB !=',10);
		}
		$this->db->where('FK_iMaPB',$phongban);
		$this->db->where('iSoVBDi >',0);
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->result_array();
	}
	// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời
	public function layVBChoSo($mavanban=NULL,$trangthai,$giaymoi=NULL)
	{
		if(!empty($mavanban))
		{
			$this->db->where('PK_iMaVBDi',$mavanban);
		}
		if($trangthai==0)
		{
			$this->db->where('iSoVBDi',0);
		}
		else{
			$this->db->where('iSoVBDi >',0);
		}
		if(!empty($giaymoi))
		{
			if($giaymoi==10)
			{
				$this->db->where('FK_iMaLVB',10);
			}
			else{
				$this->db->where('FK_iMaLVB !=',10);
			}
		}
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->result_array();
	}
	// lấy tên người nhập
	public function layNguoiNhap($mavanban)
	{
		$this->db->where('PK_iMaVBDi',$mavanban);
		$this->db->select('sHoTen');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Nhap');
		return $this->db->get()->row_array();
	}

	// lấy số đi mới nhất // kiểm tra phải giấy mời hay k
	public function laySoDiMoi($loai)
	{
		$this->db->where('FK_iMaLVB', $loai);		
		$this->db->select('iSoVBDi');
		$this->db->order_by('iSoVBDi','desc');
		return $this->db->get('tbl_vanbandi')->row_array();
	}

	// lấy số đi mới nhất // kiểm tra phải giấy mời hay k
	public function laySoDiMoi_in_Year($giaymoi)
	{
		if($giaymoi==10)
		{
			$this->db->where('FK_iMaLVB',10);
		}
		else{
			$this->db->where('FK_iMaLVB !=',10);
		}
		$this->db->select('iSoVBDi');
		$this->db->order_by('iSoVBDi','desc');
		return $this->db->get('songoaivu_'.date('Y').'.tbl_vanbandi')->row_array();
	}
		/**
		*  ================================== Văn bản sở tài chính tham mưu
		*/
	// lấy tên phòng ban và số lượng văn bản
	public function layPBvaSLVB($tungay=NULL,$denngay=NULL,$phongban=NULL,$taikhoan)
	{
		if(!empty($tungay))
		{
			$this->db->where('sNgayVBDi >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('sNgayVBDi <=',$denngay);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		// $this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->where('iDuThao',1);
		$this->db->where('iSoVBDi >',0);
		$this->db->select('count(FK_iMaPB) as tong, FK_iMaPB,sTenPB');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->group_by('FK_iMaPB');
		return $this->db->get()->result_array();
	}
	// lấy thông tin văn bản 
	public function layThongTinVB($tungay=NULL,$denngay=NULL,$phongban=NULL,$taikhoan)
	{
		if(!empty($tungay))
		{
			$this->db->where('sNgayVBDi >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('sNgayVBDi <=',$denngay);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		// $this->db->where('FK_iMaCB_Nhap',$taikhoan);
		$this->db->where('iDuThao',1);
		$this->db->where('iSoVBDi >',0);
		$this->db->select('FK_iMaPB,iSoVBDi, sKyHieu,sNgayVBDi,sTenLVB,vb.sMoTa,sHoTen,sTenPB');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB=vb.FK_iMaLVB');
		return $this->db->get()->result_array();
	}
		/**
		*  ================================== Lịch họp do lãnh đạo sở tài chính chủ trì
		*/
	// lấy lịch họp
	public function layLichHop2($ngaybd,$ngaykt,$sangchieu)
	{
		// $this->db->where('iSoDen >',0);
		$this->db->where('sNgayMoi >=',$ngaybd);
		$this->db->where('sNgayMoi <=',$ngaykt);
		$this->db->where('FK_iMaLVB',10);
		if($sangchieu=='sang')
		{
			$this->db->where('sGioMoi <','12:00');
		}
		else{
			$this->db->where('sGioMoi >=','12:00');
		}
		$this->db->select('PK_iMaVBDi,sHoTen,vb.sMoTa,sTenPB,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->order_by('sNgayMoi','asc');
		$this->db->order_by('sGioMoi','asc');
		return $this->db->get()->result_array();
	}
	public function layLichHop2_4($phongban,$quyen,$malandao,$ngaybd,$sGioMoibt,$sGioMoikt)
	{
		// $this->db->where('iSoDen >',0);
		$this->db->where('sNgayMoi =',$ngaybd);
		$this->db->where('FK_iMaLVB',10);
		
		$this->db->where('sGioMoi >=',$sGioMoibt);
		$this->db->where('sGioMoi <=',$sGioMoikt);

		$this->db->select('PK_iMaVBDi,sHoTen,vb.sMoTa,sTenPB,sNgayMoi,sGioMoi,sDiaDiemMoi,vb.FK_iMaCB_Ky');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		if($malandao >0) $this->db->where('cb.PK_iMaCB',$malandao);
		if($quyen == 6 or $quyen == 7 or $quyen == 8)$this->db->where('vb.FK_iMaPB',$phongban);
		$this->db->order_by('sNgayMoi','asc');
		$this->db->order_by('sGioMoi','asc');
		return $this->db->get()->result_array();
	}
	
	public function layLichHopSo($phongban,$quyen,$malandao,$ngaybd,$sGioMoibt,$sGioMoikt)
	{
		// $this->db->where('iSoDen >',0);
		$this->db->where('sNgayMoi =',$ngaybd);

		$this->db->select('PK_iMaVBDi,sHoTen,vb.*,sTenPB,sNgayMoi,sGioMoi,sDiaDiemMoi,FK_iMaCB_Nhap');
		$this->db->from('tbl_lichhopso as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->order_by('sGioMoi','asc');
		
		return $this->db->get()->result_array();
	}
	
	// lấy lịch họp đi l
	public function layLichHop2_2($ngaybd,$ngaykt)
	{
		// $this->db->where('iSoDen >',0);
		$this->db->where('sNgayMoi >=',$ngaybd);
		$this->db->where('sNgayMoi <=',$ngaykt);
		$this->db->where('FK_iMaLVB',10);
		
		$this->db->select('PK_iMaVBDi,sHoTen,vb.sMoTa,sTenPB,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->order_by('sNgayMoi','asc');
		$this->db->order_by('sGioMoi','asc');
		return $this->db->get()->result_array();
	}
	public function layLichHop($ngaybd,$ngaykt)
	{
		// $this->db->where('iSoDen >',0);
		$this->db->where('sNgayMoi >=',$ngaybd);
		$this->db->where('sNgayMoi <=',$ngaykt);
		$this->db->where('FK_iMaLVB',10);
		$this->db->select('sHoTen,vb.sMoTa,sTenPB,sNgayMoi,sGioMoi,sDiaDiemMoi');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_phongban as pb','pb.PK_iMaPB=vb.FK_iMaPB');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->order_by('sNgayMoi','asc');
		$this->db->order_by('sGioMoi','asc');
		return $this->db->get()->result_array();
	}// người nhập
	public function layVanThu()
	{
		$ma = array(2,9);
		$this->db->where_in('iQuyenHan_DHNB',$ma);
		$this->db->select('sHoTen,PK_iMaCB');
		return $this->db->get('tbl_canbo')->result_array();
	}
	/**
	*  ================================== Lấy danh sách van bản với các tiêu chí tìm kiếm khác nhau
	*/
	// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời: default 1 là không phải giấy mời
	// ($mavanban=NULL,$trangthai,$giaymoi=NULL)
	public function layDSVBDi($loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$tungay=NULL,$denngay=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$email=NULL,$duthaoubnd=NULL,$trangthai,$giaymoi=NULL,$limit=NULL,$offset=NULL,$tonghop=NULL,$linhvuc=NULL,$domat=NULL)
	{
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		if(!empty($tungay))
		{
			$this->db->where('sNgayVBDi >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('sNgayVBDi <=',$denngay);
		}
		if(!empty($domat))
		{
			$this->db->where('FK_iMaDM',$domat);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($tonghop))
		{
			$this->db->like('sMoTa',$tonghop);
			$this->db->or_like('iSoVBDi',$tonghop);
			$this->db->or_like('sTenLV',$tonghop);
			$this->db->where('iSoVBDi >','0');
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		if(!empty($linhvuc))
		{
			$this->db->like('sTenLV',$linhvuc);
		}
		if(!empty($email))
		{
			if($email==1)
			{
				$this->db->where('iGuiMail',$email);
			}
			else{
				$this->db->where('iGuiMail',0);
			}
			
		}
		if(!empty($duthaoubnd))
		{
			$this->db->where('iDuThao',1);
		}
		if($trangthai==0)
		{
			$this->db->where('iSoVBDi',0);
			$this->db->where('trangthai_chuyen',8);
		}
		else{
			$this->db->where('iSoVBDi >',0);
		}
		if(!empty($giaymoi))
		{
			if($giaymoi==10)
			{
				//$this->db->where('FK_iMaLVB',10);
			}
			else{
				//$this->db->where('FK_iMaLVB !=',10);
			}
		}
		$this->db->where('iSoVBDi >','0');
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('sNgayVBDi','desc');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function layDSGM($kyhieu=NULL,$tungay=NULL,$denngay=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$limit=NULL,$offset=NULL)
	{

		$this->db->where('FK_iMaLVB',10);

		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		if(!empty($tungay))
		{
			$this->db->where('sNgayVBDi >=',$tungay);
		}
		if(!empty($denngay))
		{
			$this->db->where('sNgayVBDi <=',$denngay);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('sNgayVBDi','desc');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	public function layDSVBDiPhong($loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$tu=NULL,$den=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$email=NULL,$duthaoubnd=NULL,$trangthai,$giaymoi=NULL,$limit=NULL,$offset=NULL)
	{
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		if(!empty($tu))
		{
			 //$this->db->where('Date(tbl_vanbanden.sNgayNhap) >=',$ngaynhap);sNgayVBDi
			 $this->db->where('sNgayVBDi >=',$tu);
		}
		if(!empty($den))
		{
			//
			 $this->db->where('sNgayVBDi <=',$den);
		}
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		/*if(!empty($email))
		{
			if($email==1)
			{
				$this->db->where('iGuiMail',$email);
			}
			else{
				$this->db->where('iGuiMail',0);
			}
			
		}
		if($trangthai==0)
		{
			$this->db->where('iSoVBDi',0);
			$this->db->where('trangthai_chuyen',8);
		}
		else{
			$this->db->where('iSoVBDi >',0);
		}*/
		if(!empty($giaymoi))
		{
			if($giaymoi==10)
			{
				$this->db->where('FK_iMaLVB',10);
			}
			else{
				$this->db->where('FK_iMaLVB !=',10);
			}
		}
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('sNgayVBDi','desc');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	
	
	public function demDSVBDi($loaivanban=NULL,$noiduthao=NULL,$kyhieu=NULL,$tungay=NULL,$denngay=NULL,$trichyeu=NULL,$nguoiky=NULL,$nguoinhap=NULL,$phongban=NULL,$email=NULL,$duthaoubnd=NULL,$trangthai,$giaymoi=NULL,$tonghop=NULL,$linhvuc=NULL,$domat=NULL)
	{
		if(!empty($loaivanban))
		{
			$this->db->where('FK_iMaLVB',$loaivanban);
		}
		if(!empty($noiduthao))
		{
			$this->db->where('FK_iMaPB',$noiduthao);
		}
		if(!empty($kyhieu))
		{
			$this->db->where('iSoVBDi',$kyhieu);
		}
		
		if(!empty($tungay))
		{
			$this->db->where('sNgayVBDi >=',$tungay);
		}
		
		if(!empty($denngay))
		{
			$this->db->where('sNgayVBDi <=',$denngay);
		}
		
		if(!empty($domat))
		{
			$this->db->where('FK_iMaDM',$domat);
		}
		
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		if(!empty($nguoiky))
		{
			$this->db->where('FK_iMaCB_Ky',$nguoiky);
		}
		if(!empty($nguoinhap))
		{
			$this->db->where('FK_iMaCB_Nhap',$nguoinhap);
		}
		if(!empty($phongban))
		{
			$this->db->where('FK_iMaPB',$phongban);
		}
		if(!empty($tonghop))
		{
			$this->db->like('sMoTa',$tonghop);
			$this->db->or_like('iSoVBDi',$tonghop);
			$this->db->or_like('sTenLV',$tonghop);
		}
		if(!empty($linhvuc))
		{
			$this->db->like('FK_iMaPB',$linhvuc);
		}
		if(!empty($email))
		{
			if($email==1)
			{
				$this->db->where('iGuiMail',$email);
			}
			else{
				$this->db->where('iGuiMail',0);
			}
		}
		if(!empty($duthaoubnd))
		{
			$this->db->where('iDuThao',1);
		}
		if($trangthai==0)
		{
			$this->db->where('iSoVBDi',0);
			$this->db->where('trangthai_chuyen',8);
		}
		else{
			$this->db->where('iSoVBDi >',0);
		}
		if(!empty($giaymoi))
		{
			if($giaymoi==10)
			{
				//$this->db->where('FK_iMaLVB',10);
			}
			else{
				//$this->db->where('FK_iMaLVB !=',10);
			}
		}
		$this->db->select('sHoTen,sTenLVB,vb.*');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('iSoVBDi','desc');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->num_rows();
	}
	// văn bản trình ký
	public function layVBTrinhKy($trangthai=NULL,$canbo,$trichyeu=NULL,$limit=NULL,$offset=NULL)
	{
		if(!empty($trangthai))
		{
			if($trangthai==1)
			{
				$this->db->where('trangthai_xem',$trangthai);
			}
			else{
				$this->db->where('trangthai_xem',2);
			}
		}
		$this->db->where('nguoi_nhan',$canbo);
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		// $this->db->where('iSoVBDi',0);
		$this->db->select('sHoTen,sTenLVB,vb.sKyHieu,vb.sMoTa,vb.sNoiNhan,vb.trangthai_xem,vb.PK_iMaVBDi,vb.iFile');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('PK_iMaVBDi','desc');
		if(!empty($limit))
		{
			$this->db->limit($limit,$offset);
		}
		return $this->db->get()->result_array();
	}
	// đếm văn bản trình ký
	public function demVBTrinhKy($trangthai=NULL,$canbo,$trichyeu=NULL)
	{
		if(!empty($trangthai))
		{
			if($trangthai==1)
			{
				$this->db->where('trangthai_xem',$trangthai);
			}
			else{
				$this->db->where('trangthai_xem',2);
			}
		}
		$this->db->where('nguoi_nhan',$canbo);
		if(!empty($trichyeu))
		{
			$this->db->like('sMoTa',$trichyeu);
		}
		// $this->db->where('iSoVBDi',0);
		$this->db->select('sHoTen,sTenLVB,vb.sKyHieu,vb.sMoTa,vb.sNoiNhan,vb.trangthai_xem');
		$this->db->from('tbl_vanbandi as vb');
		$this->db->join('tbl_canbo as cb','cb.PK_iMaCB=vb.FK_iMaCB_Ky');
		$this->db->join('tbl_loaivanban as lvb','lvb.PK_iMaLVB = vb.FK_iMaLVB');
		$this->db->order_by('PK_iMaVBDi','desc');
		return $this->db->get()->num_rows();
	}
	// lấy file cuối cùng
	public function layFileLast1($ma)
	{
		$this->db->where('FK_iMaVBDi',$ma);
		$this->db->select('sDuongDan,sTenFile');
		$this->db->from('songoaivu_'.date('Y').'.tbl_files_vbdi');
		$this->db->order_by('PK_iMaFileDi','desc');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
	public function layFileLast($ma)
	{
		$this->db->where('FK_iMaVBDi',$ma);
		$this->db->select('sDuongDan,sTenFile');
		$this->db->from('tbl_files_vbdi');
		$this->db->order_by('PK_iMaFileDi','desc');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	public function layMaVBDiQuaFile($filename)
	{ 
		$this->db->where('sDuongDan',$filename);
		$this->db->select('FK_iMaVBDi');		
		return $this->db->get('songoaivu_'.date('Y').'.tbl_files_vbdi')->row_array()['FK_iMaVBDi'];
	}

	// lấy đơn vị nhận mail qua mã văn bản đi
	public function layMaLoaiVBQuaMaVBDi($mavbdi)
	{
		$this->db->where('PK_iMaVBDi',$mavbdi);
		$this->db->select('FK_iMaLVB');
		return $this->db->get('songoaivu_'.date('Y').'.tbl_vanbandi')->row_array()['FK_iMaLVB'];
	}

	public function GetMaCBFromSession_ci($token)
    {
        $this->db->where('id',$token);
        $this->db->select('data');
        $this->db->from('ci_sessions');
        return $this->db->get()->row_array()['data'];
    }

    public function LayAnhChuKy_soDT($PK_iMaCB)
    {
        $this->db->where('PK_iMaCB',$PK_iMaCB);
        $this->db->select('sodienthoaiky,anhchuky');
        $this->db->from('songoaivu_'.date('Y').'.tbl_canbo');
        return $this->db->get()->row_array();
    }
	
	public function layDSChiDao($idcb,$quyen,$tencb,$limit,$offset){
	
		if ($quyen != 3){
			$query = $this->db->query('SELECT DISTINCT tbl_canbo.sHoTen, cd.*, vb.sMoTa, vb.iSoVBDi, vb.PK_iMaVBDi, vb.sNgayVBDi, vb.FK_iMaCB_Ky, vb.FK_iMaCB_Nhap, vb.iSoDen, (SELECT tbl_chidao_cb.iDaXem FROM tbl_chidao_cb WHERE id_chidao = cd.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC LIMIT 1) AS iDaXem, (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) as iTrangThaiDuyet FROM tbl_chidao cd JOIN tbl_vanbandi vb ON cd.FK_iMaVBDi = vb.PK_iMaVBDi JOIN tbl_canbo ON cd.FK_iMaCB = tbl_canbo.PK_iMaCB JOIN tbl_luuvet_vbdi ON tbl_luuvet_vbdi.FK_iMaVBDi = PK_iMaVBDi WHERE (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && (FK_iMaCB_Gui = '.$idcb.' || FK_iMaCB_Nhan = '.$idcb.' || FK_iMaCB_Ky = '.$idcb.' || cd.sNoiDung LIKE "%@'.$tencb.'%") && cd.FK_iMaCB != '.$idcb.' GROUP BY sThoiGian ORDER BY cd.sThoiGian DESC;');
        } else {
			$query = $this->db->query('SELECT DISTINCT tbl_canbo.sHoTen, cd.*, vb.sMoTa, vb.iSoVBDi, vb.PK_iMaVBDi, vb.sNgayVBDi, vb.FK_iMaCB_Ky, vb.FK_iMaCB_Nhap, vb.iSoDen, (SELECT tbl_chidao_cb.iDaXem FROM tbl_chidao_cb WHERE id_chidao = cd.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC LIMIT 1) AS iDaXem FROM tbl_chidao cd JOIN tbl_vanbandi vb ON cd.FK_iMaVBDi = vb.PK_iMaVBDi JOIN tbl_canbo ON cd.FK_iMaCB = tbl_canbo.PK_iMaCB JOIN tbl_luuvet_vbdi ON tbl_luuvet_vbdi.FK_iMaVBDi = PK_iMaVBDi WHERE (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && cd.FK_iMaCB != '.$idcb.' ORDER BY cd.sThoiGian DESC;');
			
		}
		 if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        return $query->result_array();
    }
	
	public function demChiDaoDaXem($idcb,$limit,$offset){
	
		$query = $this->db->query('SELECT DISTINCT id_chidao, id_cb, tbl_chidao.FK_iMaVBDen, tbl_chidao.FK_iMaVBDi FROM tbl_chidao_cb join tbl_chidao on id_chidao = tbl_chidao.id join tbl_vanbandi vb ON tbl_chidao.FK_iMaVBDi = vb.PK_iMaVBDi where (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && id_cb = '.$idcb);
		
        return $query->num_rows();
    }
	
	public function demDSChiDao($idcb,$quyen,$tencb,$limit,$offset){
		if ($quyen != 3){
			$query = $this->db->query('SELECT DISTINCT tbl_canbo.sHoTen, cd.*, vb.sMoTa, vb.iSoVBDi, vb.PK_iMaVBDi, vb.sNgayVBDi, vb.FK_iMaCB_Ky, vb.FK_iMaCB_Nhap, vb.iSoDen, (SELECT tbl_chidao_cb.iDaXem FROM tbl_chidao_cb WHERE id_chidao = cd.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC LIMIT 1) AS iDaXem, (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) as iTrangThaiDuyet FROM tbl_chidao cd JOIN tbl_vanbandi vb ON cd.FK_iMaVBDi = vb.PK_iMaVBDi JOIN tbl_canbo ON cd.FK_iMaCB = tbl_canbo.PK_iMaCB JOIN tbl_luuvet_vbdi ON tbl_luuvet_vbdi.FK_iMaVBDi = PK_iMaVBDi WHERE (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && (FK_iMaCB_Gui = '.$idcb.' || FK_iMaCB_Nhan = '.$idcb.' || FK_iMaCB_Ky = '.$idcb.' || cd.sNoiDung LIKE "%@'.$tencb.'%") && cd.FK_iMaCB != '.$idcb.' GROUP BY sThoiGian ORDER BY cd.sThoiGian DESC;');
		} else {
			$query = $this->db->query('SELECT DISTINCT tbl_canbo.sHoTen, cd.*, vb.sMoTa, vb.iSoVBDi, vb.PK_iMaVBDi, vb.sNgayVBDi, vb.FK_iMaCB_Ky, vb.FK_iMaCB_Nhap, vb.iSoDen, (SELECT tbl_chidao_cb.iDaXem FROM tbl_chidao_cb WHERE id_chidao = cd.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC LIMIT 1) AS iDaXem FROM tbl_chidao cd JOIN tbl_vanbandi vb ON cd.FK_iMaVBDi = vb.PK_iMaVBDi JOIN tbl_canbo ON cd.FK_iMaCB = tbl_canbo.PK_iMaCB JOIN tbl_luuvet_vbdi ON tbl_luuvet_vbdi.FK_iMaVBDi = PK_iMaVBDi WHERE (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && cd.FK_iMaCB != '.$idcb.' ORDER BY cd.sThoiGian DESC;');
			
		}
        return $query->num_rows();
    }
	
	public function layDSYKien($idcb,$tencb,$limit,$offset){
	
		$query = $this->db->query('SELECT DISTINCT tbl_canbo.sHoTen, yk.*, vb.sMoTa, vb.iSoVBDi, vb.PK_iMaVBDi, vb.sNgayVBDi, vb.FK_iMaCB_Ky, vb.FK_iMaCB_Nhap, vb.iSoDen, (SELECT tbl_ykien_cb.iDaXem FROM tbl_ykien_cb WHERE id_ykien = yk.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC LIMIT 1) AS iDaXem FROM tbl_ykien yk JOIN tbl_vanbandi vb ON yk.FK_iMaVBDi = vb.PK_iMaVBDi JOIN tbl_luuvet_vbdi ON tbl_luuvet_vbdi.FK_iMaVBDi = PK_iMaVBDi JOIN tbl_canbo ON yk.FK_iMaCB = tbl_canbo.PK_iMaCB WHERE (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && (FK_iMaCB_Gui = '.$idcb.' || FK_iMaCB_Nhan = '.$idcb.' || yk.sNoiDung LIKE "%@'.$tencb.'%") && yk.FK_iMaCB != '.$idcb.' GROUP BY sNoiDung ORDER BY yk.sThoiGian DESC;');
        
		 if(!empty($limit))
        {
            $this->db->limit($limit,$offset);
        }
        return $query->result_array();
    }
	
	public function demYKienDaXem($idcb,$limit,$offset){
	
		$query = $this->db->query('SELECT DISTINCT id_ykien, id_cb, tbl_ykien.FK_iMaVBDen, tbl_ykien.FK_iMaVBDi FROM tbl_ykien_cb join tbl_ykien on id_ykien = tbl_ykien.id join tbl_vanbandi vb ON tbl_ykien.FK_iMaVBDi = vb.PK_iMaVBDi where (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && id_cb = '.$idcb);
		
        return $query->num_rows();
    }

	public function demDSYKien($idcb,$tencb, $limit,$offset){
	
		$query = $this->db->query('SELECT DISTINCT yk.*, vb.sMoTa, vb.iSoVBDi, vb.PK_iMaVBDi, vb.sNgayVBDi, vb.FK_iMaCB_Ky, vb.FK_iMaCB_Nhap, vb.iSoDen, (SELECT tbl_ykien_cb.iDaXem FROM tbl_ykien_cb WHERE id_ykien = yk.id && id_cb = '.$idcb.' ORDER BY iDaXem DESC LIMIT 1) AS iDaXem FROM tbl_ykien yk JOIN tbl_vanbandi vb ON yk.FK_iMaVBDi = vb.PK_iMaVBDi JOIN tbl_luuvet_vbdi ON tbl_luuvet_vbdi.FK_iMaVBDi = PK_iMaVBDi JOIN tbl_canbo ON yk.FK_iMaCB = tbl_canbo.PK_iMaCB WHERE (vb.iGuiMail = 0 || (Select iTrangThai from tbl_luuvet_vbdi where FK_iMaVBDi = vb.PK_iMaVBDi order by iTrangThai desc limit 1) !=3) && (FK_iMaCB_Gui = '.$idcb.' || FK_iMaCB_Nhan = '.$idcb.' || yk.sNoiDung LIKE "%@'.$tencb.'%") && yk.FK_iMaCB != '.$idcb.' GROUP BY sNoiDung ORDER BY yk.sThoiGian DESC;');
		
        return $query->num_rows();
    }
	
	public function layDSFileDi($mavbdi){
		$this->db->where('FK_iMaVBDi',$mavbdi);
		$this->db->select('sTenFile,sDuongDan,sThoiGian');
		$this->db->from('tbl_files_vbdi');
		$this->db->order_by('sThoiGian','desc');
		return $this->db->get()->result_array();
    }
	
}

/* End of file Mvanbandi.php */
/* Location: ./application/models/vanbandi/Mvanbandi.php */
