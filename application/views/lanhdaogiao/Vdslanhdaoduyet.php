<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form  method="post" action="" class="form-horizontal">
    <section class="content-header h1no">
      <h1 class="font">
        <b>Danh sách công việc chi tiết</b>
        <span style="padding-left: 600px"><b>Chọn tất cả</b> &nbsp;&nbsp;&nbsp;<input  class="form-check-input" type="checkbox" name="duyettatca" id="select_all"> 
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" class="btn btn-primary btn-sm" name="duyetcongviec" value="Duyệt công việc"></span>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {($thongtin)?'':"Tất cả kế hoạch đã được duyệt! <br><br>"}

                {($thongtin)?'':'<a href="chitietcvlanhdaogiao"> Xem danh sách công việc </a><br>'}
                <!-- {$jj=1} -->
                <table id="" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width:5%" class="text-center">STT</th>
                            <th width="55%">Nội dung</th>
                            <th width="15%">Phòng chủ trì</th>
                            <th width="15%">Phòng phối hợp</th>                       
                            <th class="text-center" width="10%">Hạn xử lý</th>
                            <th class="text-center" width="10%"> Chọn duyệt</th>
                        </tr>                                  
                    </thead>
                    <tbody>
                    {foreach $thongtin as $tt}
                        <tr style="background-color: white;">
                            <td class="text-center"><b>{intToRoman($jj++)}</b></td>
                            <td colspan="4" class="text-left">
                                {$tt['qlv_code']} &nbsp;&nbsp;
                                {$tt['qlv_noidung']} &nbsp;
                                {foreach $file_dk as $file} 
                                {if $file.qlv_id eq $tt['qlv_id']}  
                                    <a href="{$file.qlvFile_path}">[Xem File]</a>
                                {/if}
                                {/foreach}
                                &nbsp;<i>({date_select($tt['qlv_date'])})</i><br>
                                <b>- Lãnh đạo chỉ đạo: </b>{$tt['qlv_ld_chutri']}<br>
                                <b>- Hạn xử lý: </b>{if $tt['qlv_date_han'] >'2000-10-10'}
                                    {date_select($tt['qlv_date_han'])} <br>
                                {/if}
                            </td>
                           
                            <td class="text-center">
                                <input  class="checkbox1"  type="checkbox" name="chonduyet_qlv" value="{$tt['qlv_id']}_{$tt['qlv_active']}" onclick="concatkehoach()" />
                            </td>
                        </tr>
                    
                        <!-- {$ij=1} -->
                        {foreach $qldvDetail as $dv}                        
                        {if $dv.qlv_id == $tt['qlv_id']}
                                <tr class="success">
                                    <td class="text-center">{$ij++}</td>
                                    <td>{$dv.qlvDetails_desc}</td>
                                    <td>{$mangdsphong[$dv.main_department]}</td>     
                                    <td>{foreach $dv.phoihop as $key => $value}
                                        - {$mangdsphong[$value[$key]]} <br>
                                    {/foreach}</td>
                                    <td colspan="2" class="text-center">
                                        {if $dv.han_thongke >'2000-10-10'}
                                            {date_select($dv.han_thongke)} <br>
                                        {/if}
                                    </td>
                                </tr>
                            {/if}
                            {/foreach}
                            {/foreach}
                    </tbody>
                </table>
                       
                <input type="hidden" id="string_kh_id" name="string_kh_id" value="">
                <input type="hidden" id="string_active" name="string_active" value="">                    
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
function concatkehoach(){ 
        var str_id ='0';
        var str_active ='0';
        $.each($("input[name='chonduyet_qlv']:checked"), function(){
            var string = $(this).val();
            var array = string.split("_");
            str_id = str_id + ","+array[0];
            str_active = str_active +',' + array[1];
        });
        $('input[name=string_kh_id]').val(str_id);
        $('input[name=string_active]').val(str_active);
    }

$("#select_all").change(function(){ 
    var str_id ='0';
    var str_active ='0'; 
    var status = this.checked; 
    $('.checkbox1').each(function(){ 
        this.checked = status; 

        var string = $(this).val();
        var array = string.split("_");
        str_id = str_id + ","+array[0];
        str_active = str_active +',' + array[1];
    });
    if(status == false){
        str_id ='0';
        str_active ='0';
    }
    $('input[name=string_kh_id]').val(str_id);
    $('input[name=string_active]').val(str_active);
});

$('.checkbox1').change(function(){ 
    if(this.checked == false){
        $("#select_all")[0].checked = false; 
    }
    
    if ($('.checkbox1:checked').length == $('.checkbox1').length ){ 
        $("#select_all")[0].checked = true; 
    }
});

</script>
