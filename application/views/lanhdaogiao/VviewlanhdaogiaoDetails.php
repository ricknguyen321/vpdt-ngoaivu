<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách công việc chi tiết
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$thongtin[0]['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($thongtin[0]['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($thongtin[0]['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b> <a href="{($file_dk)?$file_dk[0]['qlvFile_path']:''}">{($file_dk)?'[Xem File]':''}</a></span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$thongtin[0]['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($thongtin[0]['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$thongtin[0]['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$thongtin[0]['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$thongtin[0]['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            {if $thongtin[0]['qlv_active'] lt 2} 
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($thongtin[0]['qlv_date_nhap'])}</span>
                                <span for="" class="col-sm-6">
                                <a href="{$url}addlanhdaogiao/{$thongtin[0]['qlv_id']}" class="btn btn-primary btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Chỉnh sửa</a> 
                                <a href="{$url}addlanhdaogiaoDetails/{$thongtin[0]['qlv_id']}" class="btn btn-primary btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Thêm mới</a>
                                </span>
                            {/if}
                            </div>
                        </div>

                    <table id="" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width:5%" class="text-center">STT</th>
                            <th width="55%">Nội dung</th>
                            <th width="15%">Phòng chủ trì</th>
                            <th width="15%">Phòng phối hợp</th>                       
                            <th class="text-center" width="15%">Hạn xử lý</th>
                        </tr>                                  
                    </thead>
                    <tbody>
                        <!--{$ij=1}-->
                        {foreach $qldvDetail as $dv}                        
                                <tr class="success">
                                    <td class="text-center">{$ij++}</td>
                                    <td>
                                       <a href="{$url}xemchitietdauviec/{$dv.qlvDetails_id}">
                                        {$dv.qlvDetails_desc}
                                       </a>
                                        {if $dv.qlvDetails_active lt 2}<br><br> 
                                        <a href="{$url}editlanhdaogiaoDetail/{$dv.qlvDetails_id}/{$dv.qlv_id}" class="btn btn-primary btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Chỉnh sửa</a>
                                        {/if}
                                    </td>
                                    <td>{$mangdsphong[$dv.main_department]}</td>     
                                    <td>{foreach $dv.phoihop as $key => $value}
                                        - {$mangdsphong[$value[$key]]} <br>
                                    {/foreach}</td>
                                    <td class="text-center">
                                        {if $dv.han_thongke >'2000-10-10'}
                                            {date_select($dv.han_thongke)} <br>
                                        {/if}
                                    </td>
                                </tr>
                        {/foreach}
                    </tbody>
                </table> 
                         
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

