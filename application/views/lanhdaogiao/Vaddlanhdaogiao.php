<!-- <script src="{$url}assets/js/qldv/qldv.js"></script> -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới quản lý việc
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal reset-form" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="col-md-12 giaymoi">
                        
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số kí hiệu</label>
                                <div class="col-sm-8">
                                    <input class="form-control"  type="text" id="qlv_code" name="qlv_code" value="{($thongtin)?$thongtin[0]['qlv_code']:''}"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label" >Loại văn bản *</label>
                                <div class="col-sm-7">
                                    <select name="qlv_loai" id="qlv_loai" required class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn loại văn bản --</option>
                                        {if !empty($dsloaivanban)}
                                            {foreach $dsloaivanban as $l}
                                            <option value="{$l.PK_iMaLVB}" {($thongtin)?($thongtin[0]['qlv_loai']==$l.PK_iMaLVB)?'selected':'':''}>{$l.sTenLVB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Ngày văn bản *</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control datepic datemask"  required id="qlv_date" value="{($thongtin)?date_select($thongtin[0]['qlv_date']):date('d/m/Y')}" name="qlv_date">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                   <input type="radio" name="tailieu" value="3" checked=""> <label for="" class="control-label" style="color:red">Chương trình công tác</label>   
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                   <input type="radio" name="tailieu" value="0" {($thongtin)?($thongtin[0]['loaivanban']==0)?'checked':'':''}> <label for="" class="control-label" style="color:red">Văn bản thông thường</label>   
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                   <input type="radio" name="tailieu" value="1" {($thongtin)?($thongtin[0]['loaivanban']==1)?'checked':'':''}> <label for="" class="control-label" style="color:red">Tài liệu phục vụ họp</label>   
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                   <input type="radio" name="tailieu" value="2" {($thongtin)?($thongtin[0]['loaivanban']==2)?'checked':'':''}> <label for="" class="control-label" style="color:red">Kiến nghị cử tri</label>   
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 tailieuhop {($thongtin)?($thongtin[0]['loaivanban']>0)?'hide':'hide':'hide'}">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Khóa</label>
                                <div class="col-sm-8">
                                    <input class="form-control"  type="text" id="khoa" name="khoa" value="{($thongtin)?$thongtin[0]['sKhoa']:''}"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 tailieuhop {($thongtin)?($thongtin[0]['loaivanban']>0)?'hide':'hide':'hide'}">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Nhiệm kỳ</label>
                                <div class="col-sm-7">
                                    <input class="form-control"  type="text" id="nhiemky" name="nhiemky" value="{($thongtin)?$thongtin[0]['sNhiemKy']:''}"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 tailieuhop {($thongtin)?($thongtin[0]['loaivanban']>0)?'hide':'hide':'hide'}">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Kỳ họp</label>
                                <div class="col-sm-7">
                                    <input class="form-control"  type="text" id="kyhop" name="kyhop" value="{($thongtin)?$thongtin[0]['sKyHop']:''}"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Trích yếu *</label>
                                    <textarea  name="qlv_noidung" id="qlv_noidung" required class="form-control" rows="3">{($thongtin)?$thongtin[0]['qlv_noidung']:''}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 hide">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Thông báo kết luận</label>
                                    <textarea name="thongbao_ketluan" class="form-control" rows="3">{($thongtin)?$thongtin[0]['thongbao_ketluan']:''}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">LĐ chỉ đạo</label>
                                <div class="col-sm-8">
                                    <select name="lanhdao_id" id="lanhdao_id" required class="form-control select2" style="width:100%"><option value="0">-- Chọn lãnh đạo --</option>{if !empty($lanhdao)}{foreach $lanhdao as $ld}<option value="{$ld.PK_iMaCB}"  {($thongtin)?($thongtin[0]['lanhdao_id'] eq $ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>{/foreach}{/if}</select>
                                    <!-- <input type="text" class="form-control" value="{($thongtin)?$thongtin[0]['qlv_ld_chutri']:''}" id="qlv_ld_chutri" name="qlv_ld_chutri"> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người ký</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{($thongtin)?$thongtin[0]['qlv_ld_ky']:''}" id="qlv_ld_ky" name="qlv_ld_ky">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Ngày nhập *</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control datemask" readonly=""  required id="qlv_date_nhap" value="{($thongtin)?date_select($thongtin[0]['qlv_date_nhap']):date('d/m/Y')}" name="qlv_date_nhap">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Hạn xử lý</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control datepic datemask"   required id="qlv_date_han" value="{($thongtin)?date_select($thongtin[0]['qlv_date_han']):date('d/m/Y')}" name="qlv_date_han">
                                </div>
                            </div>
                        </div>
                            
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Tệp tin</label>
                                <div class="col-sm-8">
                                    <input type="file" name="files[]" multiple class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-sm-12">

                                    <input type="hidden" name="qlv_active" value="{($thongtin)?$thongtin[0]['qlv_active']:0}">
                                    <input type="hidden" name="qlv_sort" value="{($thongtin)?$thongtin[0]['qlv_sort']:0}">
                                    <input type="hidden" name="qlv_file" value="{($thongtin)?$thongtin[0]['qlv_file']:0}">


                                    <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Thêm mới'}</button> 
                                        <a href="javascript: history.back(1)" class="btn btn-default">Quay lại >></a> 
                                    <label for="">Người nhập văn bản: {$vanban['sHoTen']}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $(document).on('change','input[name=tailieu]',function(){
            var val = $(this).val();
            if(val==1||val==2)
            {
                $('.tailieuhop').removeClass('hide');
            }
            else{
                $('.tailieuhop').addClass('hide');
                $('input[name=khoa]').val('');
                $('input[name=nhiemky]').val('');
                $('input[name=kyhop]').val('');
            }
        });
    });
</script>
