<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form  method="post" action="" class="form-horizontal">
    <section class="content-header h1no">
      <h1 class="font">
        <b>Giao việc chi tiết cho cán bộ, công chức trong đơn vị</b>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {($thongtin)?'':"Tất cả công việc đã được giao! <br><br>"}

                {($thongtin)?'':'<a href="chitietcvlanhdaogiao"> Xem danh sách công việc </a><br>'}
                <!-- {$jj=1} -->
                 <!-- {$jj=1} -->
                <table id="" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width:5%" class="text-center">STT</th>
                            <th width="55%">Nội dung</th>
                            {if $maphong eq 12}
                            <th width="15%">Chi cục phó-TP</th>
                            {/if}
                            <th width="15%">Lãnh đạo phụ trách</th>                 
                            <th width="15%">Cán bộ xử lý</th>
                            <th class="text-center" width="10%"> Chọn duyệt</th>
                        </tr>                                  
                    </thead>
                    <tbody>
                    {foreach $thongtin as $tt}
                        <tr style="background-color: white;">
                            <td class="text-center"><b>{intToRoman($jj++)}</b></td>
                            <td colspan="5" class="text-left">
                                {$tt['qlv_code']} &nbsp;&nbsp;
                                {$tt['qlv_noidung']} &nbsp;
                                {foreach $file_dk as $file} 
                                {if $file.qlv_id eq $tt['qlv_id']}  
                                    <a href="{$file.qlvFile_path}">[Xem File]</a>
                                {/if}
                                {/foreach}
                                &nbsp;<i>({date_select($tt['qlv_date'])})</i><br>
                                <b>- Lãnh đạo chỉ đạo: </b>{$tt['qlv_ld_chutri']}<br>
                                <b>- Hạn xử lý: </b>{if $tt['qlv_date_han'] >'2000-10-10'}
                                    {date_select($tt['qlv_date_han'])} <br>
                                {/if}
                            </td>
                           
                            
                        </tr>
                    
                        <!-- {$ij=1} -->
                        {foreach $qldvDetail as $dv}                        
                        {if $dv.qlv_id == $tt['qlv_id']}
                                <tr class="success">
                                    <td class="text-center">{$ij++}</td>
                                    <td>
                                        {$dv.qlvDetails_desc}
                                        <br>
                                        <i>
                                            Danh sách phòng phối hợp:
                                            {foreach $dv.phoihop as $key => $value}
                                            <br>- {$mangdsphong[$value[$key]]} 
                                            {/foreach}
                                        </i>
                                        {if $dv.han_thongke >'2000-10-10'}
                                        <br><span style="color: red">Hạn xử lý: {date_select($dv.han_thongke)}</span>     
                                        {/if}
                                    </td>
                                    {if $maphong eq 12}
                                    <td>
                                        <select name="FK_iMaCB_CCP{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn chi cục phó --</option>{foreach $cbphong as $ld1}
                                        {if $ld1.iQuyenHan_DHNB eq 10}
                                        <option value="{$ld1.PK_iMaCB}"} {if $dv.FK_iMaCB_CCP eq $ld1.PK_iMaCB}selected{/if}>{$ld1.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                        </select><br><br>
                                        <select name="FK_iMaCB_CC_TP{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%">
                                        <option value="0">-- Chọn trưởng phòng --</option>{foreach $cbphong as $ld2}
                                        {if $ld2.iQuyenHan_DHNB eq 11}
                                        <option value="{$ld2.PK_iMaCB}"} {if $dv.FK_iMaCB_CC_TP eq $ld2.PK_iMaCB}selected{/if}>{$ld2.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                    </td> 
                                    {/if}    
                                    <td><select name="FK_iMaCB_PP_CT{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%">
                                        <option value="0">-- Chọn lãnh đạo chỉ đạo --</option>{foreach $cbphong as $ld1}
                                        {if $ld1.iQuyenHan_DHNB eq 7}
                                        <option value="{$ld1.PK_iMaCB}"}  {if $dv.FK_iMaCB_PP_CT eq $ld1.PK_iMaCB}selected{/if}>{$ld1.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                        </select>
                                    </td>
                                    <td><select name="FK_iMaCB_CV_CT{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%">
                                        <option value="0">{$dv.FK_iMaCB_CV_CT}-- Chọn cán bộ thực hiện --</option>{foreach $cbphong as $ld2}
                                        {if $ld2.iQuyenHan_DHNB eq 8}
                                        <option value="{$ld2.PK_iMaCB}"  {if $dv.FK_iMaCB_CV_CT eq $ld2.PK_iMaCB}selected{/if}>{$ld2.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                    <br> Chọn cán bộ phối hợp:
                                    <select name="FK_iMaCB_CV_PH{$dv.qlvDetails_id}[]"  multiple="" class="form-control select2" style="width:100%">
                                {foreach $cbphong as $ld}
                                    {if !empty($dv.FK_iMaCB_CV_PH)}
                                        {$mang = explode(',',$dv.FK_iMaCB_CV_PH)}
                                        <option value="{$ld.PK_iMaCB}" {foreach $mang as $m} {($m==$ld.PK_iMaCB)?'selected':''}{/foreach}>{$ld.sHoTen}</option>
                                    {else}
                                        <option value="{$ld.PK_iMaCB}">{$ld.sHoTen}</option>
                                    {/if}
                                {/foreach}
                                        </select>
                                </td>
                                    <td class="text-center">
                                        <input type="hidden" name="han_thongke{$dv.qlvDetails_id}" value="{$dv.han_thongke}">
                                        <input type="hidden" name="qlvDetails_desc{$dv.qlvDetails_id}" value="{$dv.qlvDetails_desc}">
                                        <input type="hidden" name="qlvDetails_date{$dv.qlvDetails_id}" value="{$dv.qlvDetails_date}">
                                        <input type="hidden" name="main_department{$dv.qlvDetails_id}" value="{$dv.main_department}">
                                        <input type="hidden" name="lanhdao_id{$dv.qlvDetails_id}" value="{$tt['lanhdao_id']}">
                                        <button type="submit" name="luulai" value="{$dv.qlvDetails_id}" class="btn btn-primary">Giao việc</button>
                                    
                                    </td>
                                </tr>
                            {/if}
                            {/foreach}
                            {/foreach}
                    </tbody>
                </table>
                
                                          
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
$(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $(document).on('click','button[name=themmoi]',function(){
            var stt = $(this).val();
            var tong = parseInt(stt)+1;
            var noidung1 = '<div class="col-md-12 noidung"><div class="form-group"><label for="" class="col-sm-2 control-label">Nội dung công việc giao cán bộ đơn vị *</label><div class="col-sm-10"><textarea name="noidunghop[]" class="form-control" rows="2" style="border: 1px solid red;"></textarea></div></div></div>';

            var noidung2 = ' <div class="col-md-6 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label" required>Lãnh đạo chỉ đạo *</label><div class="col-sm-8 "><select name="phongchutri[]" id="" required class="form-control select2" style="width:100%"><option value="0">-- Chọn đơn vị --</option>{if !empty($main_depart)}{foreach $main_depart as $p}<option value="{$p.PK_iMaPB}"}>{$p.sTenPB}</option>{/foreach}{/if}</select></div></div></div>';


            var noidung3 = '<div class="col-md-2 noidung"><div class="form-group<div style="margin-bottom: 10px"><input type="hidden" name="phongphoihop[]" id="100_'+stt+'" value=""><button type="button" name="chonphoihop" value="'+stt+'" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Chọn phối hợp</button></div></div></div>';

             var noidung4 = '<div class="col-md-3 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label">Hạn xử lý</label><div class="col-sm-8"><input type="text" class="form-control datepic datemask" id="" value="" name="hanxuly[]"></div></div></div> ';
           
            var noidung = noidung1+noidung2+noidung3+noidung4;
            $('.themsau').before(noidung);
            $(".timepicker").timepicker({
                showInputs: false
            });
            $('button[name=themmoi]').val(tong);
            $('input[name=tong_dv_chitiet]').val(tong);
            $(".select2").select2();
            //datepic dd/mm/yyyy
            $('.datepic').datepicker({
              autoclose: true
            });
            //Datemask dd/mm/yyyy
            $(".datemask").inputmask("dd/mm/yyyy");
            $('.control-label').css('text-align', 'left');
        });
    });

function concatkehoach(){ 
        var str_id ='0';
        var str_active ='0';
        $.each($("input[name='chonduyet_qlv']:checked"), function(){
            var string = $(this).val();
            var array = string.split("_");
            str_id = str_id + ","+array[0];
            str_active = str_active +',' + array[1];
        });
        $('input[name=string_kh_id]').val(str_id);
        $('input[name=string_active]').val(str_active);
    }

$("#select_all").change(function(){ 
    var str_id ='0';
    var str_active ='0'; 
    var status = this.checked; 
    $('.checkbox1').each(function(){ 
        this.checked = status; 

        var string = $(this).val();
        var array = string.split("_");
        str_id = str_id + ","+array[0];
        str_active = str_active +',' + array[1];
    });
    if(status == false){
        str_id ='0';
        str_active ='0';
    }
    $('input[name=string_kh_id]').val(str_id);
    $('input[name=string_active]').val(str_active);
});

$('.checkbox1').change(function(){ 
    if(this.checked == false){
        $("#select_all")[0].checked = false; 
    }
    
    if ($('.checkbox1:checked').length == $('.checkbox1').length ){ 
        $("#select_all")[0].checked = true; 
    }
});

</script>
