<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form  method="post" action="" class="form-horizontal">
    <section class="content-header h1no">
      <h1 class="font">
        <b>Danh sách công việc chi tiết</b>
        <span style="padding-left: 600px"><b>Chọn tất cả</b> &nbsp;&nbsp;&nbsp;<input  class="form-check-input" type="checkbox" name="duyettatca" id="select_all"> 
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" class="btn btn-primary btn-sm" name="duyetcongviec" value="Duyệt công việc"></span>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {($thongtin)?'':"Tất cả kế hoạch đã được duyệt! <br><br>"}

                {($thongtin)?'':'<a href="chitietcvlanhdaogiao"> Xem danh sách công việc </a><br>'}
                <!-- {$jj=1} -->
                {foreach $thongtin as $tt}
                
                {if $jj>1}
                <div class="row">
                    <div class="col-md-12" style="margin-top: -10px;">
                        <hr style="border:1px solid #5d5757; width: 100%">
                    </div>
                </div>
                {/if}
                <div class="row">
                    <div class="col-md-9" {if $jj >1} style="margin-top: -10px;margin-bottom: 10px" {else}  style="margin-top:10px;margin-bottom: 10px" {/if}>
                        <b style="color: red">{intToRoman($jj)}. Văn bản thứ {$jj}: </b>
                        <span style="color: blue; padding-left: 20px">Hạn xử lý: {date_select($tt['qlv_date_han'])}</span>
                    </div>
                    <div class="col-md-3" {if $jj >1} style="margin-top: -10px;margin-bottom: 10px;"{else}style="margin-top:10px;margin-bottom: 10px;"{/if}><span><input  class="checkbox1"  type="checkbox" name="chonduyet_qlv" value="{$tt['qlv_id']}_{$tt['qlv_active']}" onclick="concatkehoach()" /> &nbsp; <b style="color: red">Chọn duyệt</b></span>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$tt['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($tt['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($tt['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b>
                                {foreach $file_dk as $file} 
                                {if $file.qlv_id eq $tt['qlv_id']}  
                                    <a href="{$file.qlvFile_path}">[Xem File]</a>
                                {/if}
                                {/foreach}
                                </span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$tt['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($tt['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$tt['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$tt['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$tt['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($tt['qlv_date_nhap'])}</span>
                                <span for="" class="col-sm-6">
                                <a href="{$url}addlanhdaogiao/{$tt['qlv_id']}" class="btn btn-primary btn-sm {($tt['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Chỉnh sửa</a> 
                                <a href="{$url}addlanhdaogiaoDetails/{$tt['qlv_id']}" class="btn btn-primary btn-sm {($tt['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Thêm mới</a>
                                </span>
                            </div>
                        </div>

                        
                        <!-- {$i=0} -->
                        {foreach $qldvDetail as $dv}
                        
                        {if $dv.qlv_id == $tt['qlv_id']}
                        <!-- {$i++}  -->
                        {if $dv.qlv_id == $tt['qlv_id']}
                        <div class="col-sm-12" style="margin-top: -10px;">
                            <hr style="border: 1px dashed #5d5757">
                        </div>
                        {/if}
                        <div class="col-md-12">
                            <div class="form-group">
                                <span for="" class="col-sm-2 text-primary">{$i}. Nội dung công việc: </span>

                                <div class="col-sm-4">
                                    {$dv.qlvDetails_desc}
                                    <br>
                                    <i>(Hạn xử lý: </i><span style="color: red">{date_select($dv.han_thongke)}</span><i>)</i>
                                </div>
                                <div class="col-sm-3">
                                    Phòng chủ trì: <br><b>{$mangdsphong[$dv.main_department]}</b>
                                </div>
                                <div class="col-sm-3">
                                    Phòng phối hợp: <br>
                                    {foreach $dv.phoihop as $key => $value}
                                        - <b>{$mangdsphong[$value[$key]]}</b> <br>
                                    {/foreach}
                                </div>
                            </div>
                        </div> 
                        
                        <div class="col-md-12">   
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{$url}editlanhdaogiaoDetail/{$dv.qlvDetails_id}/{$dv.qlv_id}" class="btn btn-primary btn-sm {($tt['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Chỉnh sửa</a>
                                </div>
                            </div>
                        </div>
                        {/if}

                            {/foreach}
                        
                        </div>
                        <!-- {$jj++} -->
                        {/foreach}
                        <input type="hidden" id="string_kh_id" name="string_kh_id" value="">
                        <input type="hidden" id="string_active" name="string_active" value="">                    
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
function concatkehoach(){ 
        var str_id ='0';
        var str_active ='0';
        $.each($("input[name='chonduyet_qlv']:checked"), function(){
            var string = $(this).val();
            var array = string.split("_");
            str_id = str_id + ","+array[0];
            str_active = str_active +',' + array[1];
        });
        $('input[name=string_kh_id]').val(str_id);
        $('input[name=string_active]').val(str_active);
    }

$("#select_all").change(function(){ 
    var str_id ='0';
    var str_active ='0'; 
    var status = this.checked; 
    $('.checkbox1').each(function(){ 
        this.checked = status; 

        var string = $(this).val();
        var array = string.split("_");
        str_id = str_id + ","+array[0];
        str_active = str_active +',' + array[1];
    });
    if(status == false){
        str_id ='0';
        str_active ='0';
    }
    $('input[name=string_kh_id]').val(str_id);
    $('input[name=string_active]').val(str_active);
});

$('.checkbox1').change(function(){ 
    if(this.checked == false){
        $("#select_all")[0].checked = false; 
    }
    
    if ($('.checkbox1:checked').length == $('.checkbox1').length ){ 
        $("#select_all")[0].checked = true; 
    }
});

</script>
