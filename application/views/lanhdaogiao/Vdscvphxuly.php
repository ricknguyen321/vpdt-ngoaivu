<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <form  method="post" action="" class="form-horizontal">
    <section class="content-header h1no">
      <h1 class="font">
        <b>Giao việc chi tiết cho cán bộ, công chức trong đơn vị</b>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {($thongtin)?'':"Tất cả công việc đã được giao! <br><br>"}

                {($thongtin)?'':'<a href="chitietcvlanhdaogiao"> Xem danh sách công việc </a><br>'}
                <!-- {$jj=1} -->
                 <!-- {$jj=1} -->
                <table id="" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width:5%" class="text-center">STT</th>
                            <th width="55%">Nội dung</th>
                            {if $maphong eq 12}
                            <th width="15%">Chi cục phó-TP</th>
                            {/if}
                            <th width="15%">Lãnh đạo phụ trách</th>                 
                            <th width="15%">Cán bộ xử lý</th>
                            
                        </tr>                                  
                    </thead>
                    <tbody>
                    {foreach $thongtin as $tt}
                        <tr style="background-color: white;">
                            <td class="text-center"><b>{intToRoman($jj++)}</b></td>
                            <td colspan="5" class="text-left">
                                {$tt['qlv_code']} &nbsp;&nbsp;
                                {$tt['qlv_noidung']} &nbsp;
                                {foreach $file_dk as $file} 
                                {if $file.qlv_id eq $tt['qlv_id']}  
                                    <a href="{$file.qlvFile_path}">[Xem File]</a>
                                {/if}
                                {/foreach}
                                &nbsp;<i>({date_select($tt['qlv_date'])})</i><br>
                                <b>- Lãnh đạo chỉ đạo: </b>{$tt['qlv_ld_chutri']}<br>
                                <b>- Hạn xử lý: </b>{if $tt['qlv_date_han'] >'2000-10-10'}
                                    {date_select($tt['qlv_date_han'])} <br>
                                {/if}
                            </td>
                           
                            
                        </tr>
                    
                        <!-- {$ij=1} -->
                        {foreach $qldvDetail as $dv}                        
                        {if $dv.qlv_id == $tt['qlv_id']}
                                <tr class="success">
                                    <td class="text-center">{$ij++}</td>
                                    <td>
                                    <a href="cvphthuchien/{$dv.qlvDetails_id}">
                                        {$dv.qlvDetails_desc}
                                        <br>
                                        <i>
                                            Danh sách phòng phối hợp:
                                            {foreach $dv.phoihop as $key => $value}
                                            <br>- {$mangdsphong[$value[$key]]} 
                                            {/foreach}
                                        </i>
                                        {if $dv.han_thongke >'2000-10-10'}
                                        <br><span style="color: red">Hạn xử lý: {date_select($dv.han_thongke)}</span>     
                                        {/if}
                                    </a>
                                    </td>
                                    {if $maphong eq 12}
                                    <td>
                                        <select name="FK_iMaCB_CCP{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn chi cục phó --</option>{foreach $cbphong as $ld1}
                                        {if $ld1.iQuyenHan_DHNB eq 10}
                                        <option value="{$ld1.PK_iMaCB}"} {if $dv.FK_iMaCB_CCP eq $ld1.PK_iMaCB}selected{/if}>{$ld1.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                        </select><br><br>
                                        <select name="FK_iMaCB_CC_TP{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn trưởng phòng --</option>{foreach $cbphong as $ld2}
                                        {if $ld2.iQuyenHan_DHNB eq 11}
                                        <option value="{$ld2.PK_iMaCB}"} {if $dv.FK_iMaCB_CC_TP eq $ld2.PK_iMaCB}selected{/if}>{$ld2.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                    </td> 
                                    {/if}    
                                    <td><select name="FK_iMaCB_PP_CT{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn lãnh đạo chỉ đạo --</option>{foreach $cbphong as $ld1}
                                        {if $ld1.iQuyenHan_DHNB eq 7}
                                        <option value="{$ld1.PK_iMaCB}"}  {if $dv.FK_iMaCB_PP_CT eq $ld1.PK_iMaCB}selected{/if}>{$ld1.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                        </select>
                                    </td>
                                    <td><select name="FK_iMaCB_CV_CT{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn cán bộ thực hiện --</option>{foreach $cbphong as $ld2}
                                        {if $ld2.iQuyenHan_DHNB eq 8}
                                        <option value="{$ld2.PK_iMaCB}"  {if $dv.FK_iMaCB_CV_CT eq $ld2.PK_iMaCB}selected{/if}>{$ld2.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                    <br> Chọn cán bộ phối hợp:
                                    <select name="FK_iMaCB_CV_PH{$dv.qlvDetails_id}[]"  multiple="" class="form-control select2" style="width:100%" disabled="disabled">
                                {foreach $cbphong as $ld}
                                    {if !empty($dv.FK_iMaCB_CV_PH)}
                                        {$mang = explode(',',$dv.FK_iMaCB_CV_PH)}
                                        <option value="{$ld.PK_iMaCB}" {foreach $mang as $m} {($m==$ld.PK_iMaCB)?'selected':''}{/foreach}>{$ld.sHoTen}</option>
                                    {else}
                                        <option value="{$ld.PK_iMaCB}">{$ld.sHoTen}</option>
                                    {/if}
                                {/foreach}
                                        </select>
                                </td>
                                </tr>
                            {/if}
                            {/foreach}
                            {/foreach}
                    </tbody>
                </table>
                
                                          
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

