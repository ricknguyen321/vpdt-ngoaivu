<script src="{$url}assets/js/qldv/addqldvDetails.js"></script>
<script src="{$url}assets/js/qldv/qldv.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Chỉnh sửa đầu việc con
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$thongtin[0]['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($thongtin[0]['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($thongtin[0]['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b> <a href="{($file_dk)?$file_dk[0]['qlvFile_path']:''}">{($file_dk)?'[Xem File]':''}</a></span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$thongtin[0]['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($thongtin[0]['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$thongtin[0]['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$thongtin[0]['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$thongtin[0]['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($thongtin[0]['qlv_date_nhap'])}</span>
                            </div>
                        </div>

                        <div class="col-sm-12" style="margin-top: -30px;">
                                <hr style="border: 1px dashed #ddd;">
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Nội dung công việc*</label>

                                <div class="col-sm-10">
                                    <textarea tabindex="1" name="qlvDetails_desc" id="qlvDetails_desc" required class="form-control" rows="2">{$qldvDetail[0]['qlvDetails_desc']}</textarea>
                                </div>
                            </div>
                        </div> 

                         <div class="col-md-6 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label" required>Đơn vị chủ trì *</label><div class="col-sm-8 "><select name="main_department" id="" required class="form-control select2" style="width:100%"><option value="0">-- Chọn đơn vị --</option>{if !empty($main_depart)}{foreach $main_depart as $p}<option value="{$p.PK_iMaPB}"} {if $qldvDetail[0]['main_department'] eq $p.PK_iMaPB} selected="selected" {/if}>{$p.sTenPB}</option>{/foreach}{/if}</select></div></div></div>


                        <!-- <div class="col-md-2 noidung"><div class="form-group<div style="margin-bottom: 10px"><input type="hidden" name="phongphoihop" id="phongphoihop" value="{$qldvDetail[0]['department_id']}"><button type="button" name="chonphoihop" value="'+stt+'" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Chọn phối hợp</button></div></div> -->

                        <div class="col-md-3 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label">Hạn xử lý</label><div class="col-sm-8"><input type="text" class="form-control datepic datemask" id="" value="{date_select($qldvDetail[0]['han_thongke'])}" name="hanxuly"></div></div></div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Đơn vị phối hợp</label>

                                <div class="col-sm-10">
                                    <select name="department_id[]"  multiple="" class="form-control select2" style="width:100%">
                                            {if !empty($main_depart)}
                                                {foreach $main_depart as $p}
                                                    {if !empty($qldvDetail[0]['department_id'])}
                                                        {$mang = explode(',',$qldvDetail[0]['department_id'])}
                                                        <option value="{$p.PK_iMaPB}" {foreach $mang as $m} {($m==$p.PK_iMaPB)?'selected':''}{/foreach}>{$p.sTenPB}</option>
                                                    {else}
                                                        <option value="{$p.PK_iMaPB}">{$p.sTenPB}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                </div>
                            </div>
                        </div> 

                        <div class="col-md-6 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label" required>Lãnh đạo sở phụ trách *</label><div class="col-sm-8 "><select name="lanhdao" id="" required class="form-control select2" style="width:100%"><option value="0">-- Chọn Lãnh Đạo --</option>{foreach $lanhdao as $ld}<option value="{$ld.PK_iMaCB}"} {if $qldvDetail[0]['FK_iMaCB_LanhDao'] eq $ld.PK_iMaCB} selected="selected" {/if}>{$ld.sHoTen}</option>{/foreach}</select></div></div></div>
                        <div class="col-md-6 noidung"><div class="form-group"><label for="" class="col-sm-3 control-label" required>Ghi chú</label><div class="col-sm-9 "><textarea name="ghichu" class="form-control" rows="1" style="border: 1px solid red;">{$qldvDetail[0]['ghichu']}</textarea></div></div></div>

                        <div class="col-sm-12" style="margin-top: -20px;">
                            <hr style="border: 1px dashed #ccc">
                        </div>
                            
                            <div class="col-md-12">
                                <button type="submit" name="luulai" value="luulai" class="btn btn-primary">Cập nhật</button> <a href="javascript: history.back(1)" class="btn btn-default" data-original-title="" title="">Quay lại &gt;&gt;</a> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

<script>
    $(document).ready(function() {
        $(document).on('click','button[name=ghilai]',function(){
            var stt = $(this).val();
            var tong = parseInt(stt)+1;
</script>

