<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        <b>Giao việc chi tiết cho cán bộ, công chức trong đơn vị</b>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <!-- {$jj=1} -->
                {foreach $thongtin as $tt}
                
                {if $jj>1}
                <div class="row">
                    <div class="col-md-12" style="margin-top: -10px;">
                        <hr style="border:1px solid #5d5757; width: 100%">
                    </div>
                </div>
                {/if}
                <div class="row">
                    <div class="col-md-9" {if $jj >1} style="margin-top: -10px;margin-bottom: 10px" {else}  style="margin-top:10px;margin-bottom: 10px" {/if}>
                        <b style="color: red">{intToRoman($jj)}. Văn bản thứ {$jj}: </b>
                        <!-- <span style="color: blue; padding-left: 20px">Hạn xử lý: {date_select($tt['qlv_date_han'])}</span> -->
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$tt['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($tt['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($tt['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b>
                                {foreach $file_dk as $file} 
                                {if $file.qlv_id eq $tt['qlv_id']}  
                                    <a href="{$file.qlvFile_path}">[Xem File]</a>
                                {/if}
                                {/foreach}
                                </span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$tt['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($tt['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$tt['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$tt['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$tt['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($tt['qlv_date_nhap'])}</span>
                                
                            </div>
                        </div>

                        
                        <!-- {$i=0} -->
                        {foreach $qldvDetail as $dv}
                        
                        {if $dv.qlv_id == $tt['qlv_id']}
                        <!-- {$i++}  -->
                        {if $dv.qlv_id == $tt['qlv_id']}
                        <div class="col-sm-12" style="margin-top: -10px;">
                            <hr style="border: 1px dashed #5d5757">
                        </div>
                        {/if}
                        <div class="col-md-12">
                            <div class="form-group">
                                <span for="" class="col-sm-2 text-primary">{$i}. Nội dung lãnh đạo giao: </span>

                                <div class="col-sm-4">
                                    {$dv.qlvDetails_desc}
                                    <br>
                                    <i>(Hạn xử lý: </i><span style="color: red">{date_select($dv.han_thongke)}</span><i>)</i>
                                </div>
                                <div class="col-sm-3">
                                    Phòng chủ trì: <br><b>{$mangdsphong[$dv.main_department]}</b>
                                </div>
                                <div class="col-sm-3">
                                    Phòng phối hợp: <br>
                                    {foreach $dv.phoihop as $key => $value}
                                        - <b>{$mangdsphong[$value[$key]]}</b> <br>
                                    {/foreach}
                                </div>
                            </div>
                        </div> 

                        {if $maphong eq 12}
                        <div class="col-md-6 noidung" style="margin-top: 20px">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label" required>Chi cục phó </label>
                                <div class="col-sm-8 ">
                                    <select name="FK_iMaCB_CCP{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn chi cục phó --</option>{foreach $cbphong as $ld1}
                                        {if $ld1.iQuyenHan_DHNB eq 10}
                                        <option value="{$ld1.PK_iMaCB}"  {if $dv.FK_iMaCB_CCP eq $ld1.PK_iMaCB}selected{/if}>{$ld1.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 noidung" style="margin-top: 20px">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label" required>Trưởng phòng </label>
                                <div class="col-sm-8 ">
                                    <select name="FK_iMaCB_CC_TP{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn trưởng phòng --</option>{foreach $cbphong as $ld2}
                                        {if $ld2.iQuyenHan_DHNB eq 11}
                                        <option value="{$ld2.PK_iMaCB}"  {if $dv.FK_iMaCB_CC_TP eq $ld2.PK_iMaCB}selected{/if}>{$ld2.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>

                        {/if}

                        <div class="col-md-6 noidung" style="margin-top: 20px">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label" required>Lãnh đạo chỉ đạo </label>
                                <div class="col-sm-8 ">
                                    <select name="FK_iMaCB_PP_CT{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%" disabled="disabled">
                                        <option value="0">-- Chọn lãnh đạo chỉ đạo --</option>{foreach $cbphong as $ld1}
                                        {if $ld1.iQuyenHan_DHNB eq 7}
                                        <option value="{$ld1.PK_iMaCB}"} {if $ld1.PK_iMaCB eq $dv.FK_iMaCB_PP_CT}selected{/if}>{$ld1.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 noidung" style="margin-top: 20px">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label" required>Cán bộ thực hiện </label>
                                <div class="col-sm-8 ">
                                    <select name="FK_iMaCB_CV_CT{$dv.qlvDetails_id}" id="" required class="form-control select2" style="width:100%"  disabled="disabled">
                                        <option value="0">-- Chọn cán bộ thực hiện --</option>{foreach $cbphong as $ld2}
                                        {if $ld2.iQuyenHan_DHNB eq 8}
                                        <option value="{$ld2.PK_iMaCB}"}  {if $ld2.PK_iMaCB eq $dv.FK_iMaCB_CV_CT}selected{/if}>{$ld2.sHoTen}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 noidung" style="margin-top: 10px">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label" required>Cán bộ phối hợp </label>
                                <div class="col-sm-10 ">
                                    <select name="FK_iMaCB_CV_PH{$dv.qlvDetails_id}[]"  multiple="" class="form-control select2" style="width:100%" disabled="disabled">
                                {foreach $cbphong as $ld}
                                    {if !empty($dv.FK_iMaCB_CV_PH)}
                                        {$mang = explode(',',$dv.FK_iMaCB_CV_PH)}
                                        <option value="{$ld.PK_iMaCB}" {foreach $mang as $m} {($m==$ld.PK_iMaCB)?'selected':''}{/foreach}>{$ld.sHoTen}</option>
                                    {else}
                                        <option value="{$ld.PK_iMaCB}">{$ld.sHoTen}</option>
                                    {/if}
                                {/foreach}
                                        </select>
                                    </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 noidung">
                            <h5>Trình tự giải quyết:</h5>
                            <table id="" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width:5%" class="text-center">STT</th>
                                        <th width="15%">Thời gian chuyển</th>
                                        <th width="15%">Người chuyển</th>
                                        <th width="15%">Người nhận</th>
                                        <th class="text-center" width="15%">Hạn xử lý</th>
                                    </tr>                                  
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center">{date_time($dv.qlvDetails_time_duyetGD)}</td>
                                        <td >{$tt['qlv_ld_chutri']}</td>
                                        
                                        <td >{$mangdsphong[$dv.main_department]}</td>
                                        <td class="text-center">
                                            {if $dv.han_thongke >'2000-10-10'}
                                                {date_select($dv.han_thongke)} <br>
                                            {/if}
                                        </td>
                                    </tr>
                                    <!-- {$ij=2} -->
                                        {foreach $nhatky as $nk}
                                        {if $nk.id_vanban eq $dv.qlvDetails_id}
                                            <tr class="success">
                                                <td class="text-center">{$ij++}</td>
                                                <td class="text-center">{date_time($nk.thoigian)}</td>
                                                <td >{$nk.nguoi_giao}</td>
                                                
                                                <td >{$nk.nguoi_nhan}</td>
                                                <td class="text-center">
                                                    {if $dv.han_thongke >'2000-10-10'}
                                                        {date_select($dv.han_thongke)} <br>
                                                    {/if}
                                                </td>
                                            </tr>
                                        {/if}
                                        {/foreach}
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12 noidung">
                            <h5>Danh sách cán bộ phối hợp:</h5>
                            <table id="" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width:5%" class="text-center">STT</th>
                                        <th width="12%">Thời gian chuyển</th>
                                        <th width="18%">Người nhận</th>
                                        <th width="15%">Phòng</th>
                                        <th class="text-center" width="10%">Hạn xử lý</th>
                                        <th class="text-left">Kết quả</th>
                                    </tr>                                  
                                </thead>
                                <tbody>                                    
                                    <!-- {$jj=1} -->
                                        {foreach $canbophoihop as $cb}
                                        {if $cb.FK_iMaPB > 0 && $cb.FK_iMaCB_Nhan >0 }
                                            <tr class="success">
                                                <td class="text-center">{$jj++}</td>
                                                <td class="text-center">{date_time($cb.sThoiGian)}</td>
                                                <td >{$manglanhdao[$cb.FK_iMaCB_Nhan]}</td>
                                                <td >{$mangdsphong[$cb.FK_iMaPB]}</td>
                                                <td class="text-center">
                                                    {if $dv.han_thongke >'2000-10-10'}
                                                        {date_select($dv.han_thongke)} <br>
                                                    {/if}
                                                </td>
                                                <td>
                                                    {if $cb.iTrangThai eq 1}
                                                        Đang xử lý
                                                    {/if}
                                                    {if $cb.iTrangThai eq 3}
                                                        Đã chỉ đạo
                                                    {/if}
{if $cb.iTrangThai eq 2}
    {foreach $fileketqua as $kq}
        {if $cb.FK_iMaCB_Nhan == $kq.user_id && $kq.qlvDetails_id == $cb.FK_iMaQLDV}
                {$kq.qlvFile_desc} 
                {if $kq.qlvFile_path != ''}
                &nbsp;&nbsp;<a href="{$kq.qlvFile_path}">[Xem File]</a>
                {/if}
                &nbsp;&nbsp;<i>({date_time($kq.qlvFile_date)})</i><br>
        {/if}
    {/foreach}
{/if}
                                                </td>

                                            </tr>
                                        {/if}
                                        {/foreach}
                                </tbody>
                            </table>
                        </div>
                        
                        {if !empty($fileketqua)}
                        <div class="col-md-12 noidung">
                            <h5>Kết quả giải quyết:</h5>
                            <table id="" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width:5%" class="text-center">STT</th>
                                        <th width="20%" class="text-left">Cán bộ hoàn thành</th>
                                        <th >Nội dung hoàn thành</th>
                                        <th >File văn bản</th>
                                        <th width="15%" class="left">Thời gian hoàn thành</th>
                                    </tr>                                  
                                </thead>
                                <tbody>
                                    <!-- {$ij=1} -->
                                        {foreach $fileketqua as $kq}
                                        {if $kq.phoihop != 2}
                                            <tr class="success">
                                                <td class="text-center">{$ij++}</td>
                                                <td class="text-left">{$manglanhdao[$kq.user_id]}</td>
                                                <td class="text-left">{$kq.qlvFile_desc}</td>
                                                <td class="text-left">
                                                {if $kq.qlvFile_path != ''}
                                                <a href="{$kq.qlvFile_path}">[Xem File]</a>
                                                {/if}
                                                </td>
                                                <td class="text-center">{date_time($kq.qlvFile_date)}</td>
                                            </tr>
                                        {/if}
                                        {/foreach}
                                </tbody>
                            </table>
                        </div>
                         
                        {/if}
                        <form class="form-horizontal" method="post" action=""  enctype="multipart/form-data">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="" class="col-sm-12">Kết quả giải quyết công việc </label>

                                        <div class="col-sm-12">

                                        <textarea name="noidunggiaiquyet" id="" class="form-control" rows="4" required></textarea>

                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">

                                    <div class="form-group">
                                    <label for="" class="col-sm-12">Đính kèm file kết quả</label>
                                   
                                    <div class="col-sm-6">
                                        <input type="file" name="file" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                        
                                    </div>
                                    </div> 
                                </div>
                             </div>                           
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="luulai" value="{$dv.qlvDetails_id}">Công việc đã hoàn thành</button>&nbsp;&nbsp;&nbsp;
                            </div>
                            </form>
                        </div>
                        {/if}

                            {/foreach}
                        
                        </div>
                        <!-- {$jj++} -->
                        {/foreach}
                                          
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
