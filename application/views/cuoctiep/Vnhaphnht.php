<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới thông tin cuộc tiếp của lãnh đạo
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:100%; justify-content: center;">
                        <div class="col-md-12" style="margin-top: 10px; ">

							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tên/Chủ đề HN, HT:</label>

                                    <div class="col-sm-8">
										<textarea name="ten_hnht" id="" class="form-control" placeholder="" rows="3" required>{($thongtin)?$thongtin[0]['ten_hnht']:''}</textarea>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tên đơn vị tổ chức:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="coquan_th" value="{($thongtin)?$thongtin[0]['coquan_th']:''}" class="form-control req" id="" placeholder="" required>
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Nội dung hoạt động</label>

                                    <div class="col-sm-8">
										<textarea name="noidung" id="" class="form-control" placeholder="" rows="5" >{($thongtin)?$thongtin[0]['noidung']:''}</textarea>
                                    </div>
                                </div> 

								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Thời gian tổ chức:</label>
					
                                    <div class="col-sm-8">
										<input type="text" name="thoi_gian" tabindex="8" value="{($thongtin)?($thongtin[0]['thoi_gian']!=0000-00-00)?date_select($thongtin[0]['thoi_gian']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>  								
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Địa điểm tổ chức:</label>
					
                                    <div class="col-sm-8">
										<textarea name="diadiem" id="" class="form-control" placeholder="" rows="3" required>{($thongtin)?$thongtin[0]['diadiem']:''}</textarea>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Cơ quan cấp phép:</label>
					
                                    <div class="col-sm-8">
										<input type="text" name="cq_capphep" value="{($thongtin)?$thongtin[0]['cq_capphep']:''}" class="form-control req" id="" placeholder="" >
                                    </div>
                                </div>
								
                            </div>							
							
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng đại biểu người VN:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="sldb1" value="{($thongtin)?$thongtin[0]['sldb1']:''}" class="form-control req" id="" placeholder="" >
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng đại biểu người nước ngoài ở trong nước:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="sldb2" value="{($thongtin)?$thongtin[0]['sldb2']:''}" class="form-control req" id="" placeholder="" >
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng đại biểu người nước ngoài từ nước ngoài vào:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="sldb3" value="{($thongtin)?$thongtin[0]['sldb3']:''}" class="form-control req" id="" placeholder="" >
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Đại biểu đến từ các nước:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="sldb4" value="{($thongtin)?$thongtin[0]['sldb4']:''}" class="form-control req" id="" placeholder="" >
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Báo cáo số:</label>
					
                                    <div class="col-sm-8">
										<input type="text" name="baocao" value="{($thongtin)?$thongtin[0]['baocao']:''}" class="form-control req" id="" placeholder="" >
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Nguồn kinh phí:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="kinhphi" value="{($thongtin)?$thongtin[0]['kinhphi']:''}" class="form-control req" id="" placeholder="" >
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ghi chú</label>

                                    <div class="col-sm-8">
										<textarea name="note" id="" class="form-control" placeholder="" rows="2">{($thongtin)?$thongtin[0]['note']:''}</textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group noidung">
									<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                    <label for="" class="control-label col-sm-3" style="width:33.333333%; padding-top: 7px;">Tài liệu</label>
									
                                    <div class="col-sm-9" style="width:66.666666666%">
                                        <input type="file" name="files[]" multiple="" tabindex="6" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                    </div>
									</form>
                                </div>
								
								<div class="themsau"></div>
                              
							  
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style=""></label>

                                    <div class="col-sm-8">
                                        <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px; float:right"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
							
								
                            </div>
                            
                        </div>
						
						
						<div class="col-md-12" style="margin-top:10px; margin-bottom:20px; text-align: center;">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm"><b>{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</b></button> &nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dshnht"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">&#8594; Danh sách HN, HT</button> </a>
                        </div>
						
						{if !empty($tailieu)}
							<div class="col-md-12" style="text-align: -webkit-center;">
                            <div class="form-group">
                                <label for="">Tài liệu đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style=""></th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                            <th class="text-center" width="8%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                        {foreach $tailieu as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td ><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></td>

                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
												
                                                <td class="text-center">
														<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Bạn có chắc muốn xóa tệp tin này?');" name="xoa" value="{$f.sDuongDan}"><i class="fa fa-trash"></i></button>
																							
                                                </td>
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
                                </table><br>
                                
                            </div>
                        </div>
							{/if}
							
							
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

