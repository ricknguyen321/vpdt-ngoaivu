<style type="text/css">
    .{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
    }
    .{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
    }
</style>
    <section class="content">
      <!-- Default box -->
        <div class="box" style="font-size: 16px">
            <div class="box-body">
				<table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px" style="border-top: 1px solid #000;border-left: 1px solid #000;">STT</th>
									<th width="8%" style="border-top: 1px solid #000;border-left: 1px solid #000;">Ngày tiếp</th>
									<th width="10%" style="border-top: 1px solid #000;border-left: 1px solid #000;">Quốc gia</th>
                                    <th width="22%" style="border-top: 1px solid #000;border-left: 1px solid #000;">Thông tin về đoàn</th>
                                    <th width="25%" class="text-center" style="border-top: 1px solid #000;border-left: 1px solid #000;">Thông tin lãnh đạo</th>
									<th width=""style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">Thông tin cuộc tiếp</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dscuoctiep)} 
								{$i=1}
								{$i = $i + $page}
                                {foreach $dscuoctiep as $cuoctiep}
                                    <tr>
                                        <td class="text-center" style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dscuoctiep)}border-bottom: 1px solid #000;{/if}">{$i++}</td>
										 <td style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dscuoctiep)+1}border-bottom: 1px solid #000;{/if}">
										 {date_select($cuoctiep.ngaytiep)}
										</td>
										<td style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dscuoctiep)+1}border-bottom: 1px solid #000;{/if}">
										{$cuoctiep.quoc_gia}
										</td>
										
                                        <td style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dscuoctiep)+1}border-bottom: 1px solid #000;{/if}">
										<span>- Tên đoàn: <b style="color:blue">{$cuoctiep.tendoan}</b></span><br>
											- Trưởng đoàn: <b>{$cuoctiep.truongdoan}</b>
										</td>
                                       
                                        <td class="" style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dscuoctiep)+1}border-bottom: 1px solid #000;{/if}">
											<span>- CQ chủ trì: <b>{$cuoctiep.coquanct}</b></span><br>
											<span>- LĐ TP tiếp: <b>{$cuoctiep.chuc_vu_vt} {$cuoctiep.cq_vt} {$cuoctiep.ten_ld}</b></span><br>
											- LĐ Sở dự: <b>{$cuoctiep.ldso}</b> 
										</td>
										
										<td style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;{if $i==count($dscuoctiep)+1}border-bottom: 1px solid #000;{/if}">
											<p>- Nội dung cuộc tiếp: {$cuoctiep.noidung}</p>
											<p>- Tặng phẩm: {$cuoctiep.tangpham}</p>
											<p>- Phiên dịch: {$cuoctiep.phiendich}</p>
												- Cán bộ thực hiện: <b>{$cuoctiep.cvth}</b> 
										</td>										
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
			
            </div>
               
        </div>
      <!-- /.box -->

    </section>