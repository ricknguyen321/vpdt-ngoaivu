<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3" style="width:20%">
                    <h3 class="font">
                        Danh sách hội nghị, hội thảo <b> ({$count})</b>
						<!--{if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs" onclick="myFunction()">Gửi nhiều email cùng lúc</button>{/if}-->
                    </h3>
                </div>
                <div class="col-sm-9" style="width:80%">
					<div class="col-md-5" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
									<input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm kiếm tổng hợp">
                               
                            </div>		
						</form>
					</div>
					<div class="col-md-4" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
								{if $vanban['FK_iMaPhongHD'] == 77 && date('Y') == $year} 
									<a href="nhaphnht"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Nhập mới</button> </a>
								{/if}
								<a href="dshnht?kx=word&tonghop={$tonghop}&cq={$cq}&dvtcth={$dvtc}&tungay={$tungay}&denngay={$denngay}"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Xuất Word</button> </a>
                            </div>		
						</form>
					</div>
					 <div class="col-md-2 abc" >
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a  class="font" name="anhien" href="javascript:void(0);"><i class="fa fa-search"></i> Nâng cao</a></label>
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
			
				<form class="form-horizontal hide anhien" method="get" >
				<div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                               
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Cơ quan cấp phép</label>

                                    <div class="col-sm-8">
                                        <select name="cqcp" id="" data-placeholder="" class="form-control select2" style="width:100%">
											<option value="">-- Chọn Cơ quan cấp phép --</option>
                                            {if !empty($dscqcp)}
                                                {foreach $dscqcp as $cqcp} 
													<option value="{$cqcp.cq_capphep}" {if $cqcp.cq_capphep == $cq}selected{/if}>{$cqcp.cq_capphep}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-6">
                               
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Đơn vị tổ chức</label>

                                    <div class="col-sm-8">
                                        <select name="dvtcth" id="" data-placeholder="" class="form-control select2" style="width:100%">
											<option value="">-- Chọn Đơn vị tổ chức --</option>
                                            {if !empty($dsdvtc)}
                                                {foreach $dsdvtc as $dvtcth} 
													<option value="{$dvtcth.coquan_th}" {if $dvtcth.coquan_th == $dvtc}selected{/if}>{$dvtcth.coquan_th}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                               
                                <div class="form-group" style="display:flex">
                                    <label for="" class="col-sm-4 control-label">Ngày tổ chức</label>

                                    <div class="col-sm-4">
                                        <input type="text" name="tungay" value="{($tungay)?$tungay:''}" class="form-control datepic datemask" id="">
                                    </div> <b>&#8594;</b>
									<div class="col-sm-4">
                                        <input type="text" name="denngay" value="{($denngay)?$denngay:''}" class="form-control datepic datemask" id="">
                                    </div>
                                </div>
                            </div>
                            
                         
                            <div class="col-md-12" style="text-align:center; margin-top: 10px">
                                <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
				</div>
                </form>
				
				
				<div class="pull-right" style="margin: -15px 10px">{$phantrang}</div>
				<i>Nhấn vào tên HN, HT để xem chi tiết.</i>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px">STT</th>		
									<th width="8%">Thời gian</th>									
									<th width="22%">Tên/Chủ đề HN,HT</th>
									<th width="18%">Đơn vị thực hiện</th>
                                    <th width="">Nội dung hoạt động, thông tin</th>							
                                    <th width="6%"></th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dshnht)} 
								{$i=1}
								{$i = $i + $page}
                                {foreach $dshnht as $hnht}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
										<td class="text-center">
											{date_select($hnht.thoi_gian)}
										</td>
										<td ><a href="hnht?id={$hnht.id}" style="color:blue"><b>{$hnht.ten_hnht}</b></a></td>
										<td class=""><b>{$hnht.coquan_th}</b></td>
                                        <td style=""><p>- Nội dung hoạt động: <b>{$hnht.noidung}</b></p>
												<p>- Địa điểm: {$hnht.diadiem}</p>
												<p>- Cơ quan cấp phép: {$hnht.cq_capphep}</p>
										</td>
                                       
                                        <td class="text-center">
											{if ($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaPhongHD'] == 77) || $vanban['sHoTen'] == $hnht.cvth || $vanban['PK_iMaCB'] == $hnht.FK_iMaCBNhap}
												<p><a href="nhaphnht?id={$hnht.id}" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a></p>
												<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Bạn có chắc muốn xóa?');" name="xoa" value="{$hnht.id}"><i class="fa fa-trash"></i></button>
												
											{else}
												-
											{/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'right');
    });
</script>
<script>
function myFunction($ma, $loai, $sodi) {	
	window.open("{$url}upload_files_vbdi?ma=" + $ma + "&loai=" + $loai + "&isodi=" + $sodi,"phathanh","menubar=1,location=no,resizable=0,width=550,height=320, top=180, left=400");
}
</script>