<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới thông tin cuộc tiếp của lãnh đạo
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:100%; justify-content: center;">
                        <div class="col-md-12" style="margin-top: 10px; ">

							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tên đoàn</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="tendoan" value="{($thongtin)?$thongtin[0]['tendoan']:''}" class="form-control req" id="" placeholder="Ví dụ: Đại sứ quán Malaysia" required>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Trưởng đoàn</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="truongdoan" value="{($thongtin)?$thongtin[0]['truongdoan']:''}" class="form-control req" id="" placeholder="Ví dụ: Đại sứ Dato' Shariffah" >
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Quốc gia</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="quoc_gia" value="{($thongtin)?$thongtin[0]['quoc_gia']:''}" class="form-control req" id="" placeholder="Ví dụ: Nhật Bản" >
                                    </div>
                                </div> 

								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Cơ quan chủ trì</label>
					
                                    <div class="col-sm-8">
									<select name="coquanct" id="" required class="form-control select2">
											<option value="">-- Chọn Cơ quan chủ trì --</option>
                                            {if !empty($dscoquan)}
                                                {foreach $dscoquan as $cq}
                                                    <option value="{$cq.co_quan}" {($thongtin)?($thongtin[0]['coquanct']==$cq.co_quan)?'selected':'':''}>{$cq.co_quan}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>  								
								
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Lãnh đạo TP tiếp</label>
					
                                     <div class="col-sm-8">
									 <select name="ldtiep[]" multiple="" id="" data-placeholder="" class="form-control select2" style="width:100%">
										{if $ma > 0}
                                            {if !empty($dsldtpall)}
                                                {foreach $dsldtpall as $ldtp} 
													<option value="{$ldtp.id}" {if !empty($mangldtp)}{if in_array($ldtp.id, $mangldtp)}selected{/if}{/if}>{$ldtp.ten_ld}</option>
                                                {/foreach}
                                            {/if}											
										{else}
											{if !empty($dsldtp)}
                                                {foreach $dsldtp as $ldtp} 
													<option value="{$ldtp.id}" {if !empty($mangldtp)}{if in_array($ldtp.id, $mangldtp)}selected{/if}{/if}>{$ldtp.ten_ld}</option>
                                                {/foreach}
                                            {/if}
										{/if}
											
                                        </select>
                                    </div>
                                </div>   
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Lãnh đạo Sở tham dự</label>

                                    <div class="col-sm-8">
										  <select name="ldso" id="" required class="form-control select2">
											<option value="">-- Chọn lãnh đạo Sở tham dự --</option>
                                            {if !empty($dslds)}
                                                {foreach $dslds as $cb}
                                                    <option value="{$cb.sHoTen}" {($thongtin)?($thongtin[0]['ldso']==$cb.sHoTen)?'selected':'':''}>{$cb.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Cán bộ xử lý</label>

                                    <div class="col-sm-8">
										  <select name="cvth" id="" required class="form-control select2">
											<option value="">-- Chọn cán bộ xử lý --</option>
                                            {if !empty($dscbls)}
                                                {foreach $dscbls as $cb}
                                                    <option value="{$cb.sHoTen}" {($thongtin)?($thongtin[0]['cvth']==$cb.sHoTen)?'selected':'':''}>{$cb.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ghi chú</label>

                                    <div class="col-sm-8">
										<textarea name="note" id="" class="form-control" placeholder="" rows="2">{($thongtin)?$thongtin[0]['note']:''}</textarea>
                                    </div>
                                </div>
								
								
                                
                                <div class="form-group noidung">
									<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                    <label for="" class="control-label col-sm-3" style="width:33.333333%; padding-top: 7px;">Tài liệu</label>
									
                                    <div class="col-sm-9" style="width:66.666666666%">
                                        <input type="file" name="files[]" multiple="" tabindex="6" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                    </div>
									</form>
                                </div>
								
								<div class="themsau"></div>
                              
							  
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style=""></label>

                                    <div class="col-sm-8">
                                        <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px; float:right"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
								
                            </div>							
							
							<div class="col-md-6">
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Nội dung cuộc tiếp</label>

                                    <div class="col-sm-8">
										<textarea name="noidung" id="" class="form-control" placeholder="" rows="4">{($thongtin)?$thongtin[0]['noidung']:''}</textarea>
                                    </div>
                                </div>
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ngày tiếp</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngaytiep" tabindex="8" value="{($thongtin)?($thongtin[0]['ngaytiep']!=0000-00-00)?date_select($thongtin[0]['ngaytiep']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Địa điểm</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="diadiem" tabindex="10" value="{($thongtin)?$thongtin[0]['diadiem']:'Trụ sở HĐND - UBND'}" class="form-control req" id="" placeholder="">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tờ trình</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vbdi" tabindex="10" value="{($thongtin)?$thongtin[0]['vbdi']:''}" class="form-control req" id="" placeholder="">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tặng phẩm</label>

                                    <div class="col-sm-8">
										Sở ngoại vụ chuẩn bị:<br>
										<input type="checkbox" name="tangpham1" value="Khuê Văn Các" {(in_array('Khuê Văn Các', $mangtp) && $thongtin[0]['tangpham_cq'] != 2) ? checked : NULL }> Khuê Văn Các &nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="tangpham2" value="Đầu Rồng" {(in_array('Đầu Rồng', $mangtp) && $thongtin[0]['tangpham_cq'] != 2) ? checked : NULL }> Đầu Rồng &nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="tangpham3" value="Bình trà Hanoia" {(in_array('Bình trà Hanoia', $mangtp) && $thongtin[0]['tangpham_cq'] != 2) ? checked : NULL }> Bình trà Hanoia									
                                        <p><input type="text" name="tangpham" tabindex="10" value="{($thongtin[0]['tangpham_cq'] != 2) ? $tpkhac : ''}" class="form-control req" id="" placeholder="Khác"></p>
										
										Văn phòng UB chuẩn bị:<br>
										<input type="text" name="tangphamub" tabindex="10" value="{($thongtin[0]['tangpham_cq'] == 2) ? $thongtin[0]['tangpham'] : ''}" class="form-control req" id="" placeholder="">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Phiên dịch</label>

                                    <div class="col-sm-8">
                                        <p>Sở Ngoại vụ chuẩn bị:<br>
										<input type="text" name="phiendich" tabindex="10" value="{($thongtin && $thongtin[0]['phiendich'] != 'Phía bạn chuẩn bị')?$thongtin[0]['phiendich']:''}" class="form-control req" id="" placeholder=""></p>
										
										<input type="checkbox" name="phiendichpb" value="Phía bạn chuẩn bị" {($thongtin[0]['phiendich'] == 'Phía bạn chuẩn bị') ? checked : NULL }> Phía bạn chuẩn bị
                                    </div>
                                </div>
								
                            </div>
                            
                        </div>
						
						
						<div class="col-md-12" style="margin-top:10px; margin-bottom:20px; text-align: center;">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm"><b>{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</b></button> &nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dscuoctiep"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">&#8594; Danh sách cuộc tiếp</button> </a>
                        </div>
						
						{if !empty($tailieu)}
							<div class="col-md-12" style="text-align: -webkit-center;">
                            <div class="form-group">
                                <label for="">Tài liệu đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style=""></th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                            <th class="text-center" width="8%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                        {foreach $tailieu as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td ><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></td>

                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
												
                                                <td class="text-center">
														<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Bạn có chắc muốn xóa tệp tin này?');" name="xoa" value="{$f.sDuongDan}"><i class="fa fa-trash"></i></button>
																							
                                                </td>
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
                                </table><br>
                                
                            </div>
                        </div>
							{/if}
							
							
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

