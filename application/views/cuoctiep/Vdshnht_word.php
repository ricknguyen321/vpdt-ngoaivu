<style type="text/css">
    .{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
    }
    .{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
    }
</style>
    <section class="content">
      <!-- Default box -->
        <div class="box" style="font-size: 16px">
            <div class="box-body">
				<table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px" style="border-top: 1px solid #000;border-left: 1px solid #000;">STT</th>
									<th width="8%" style="border-top: 1px solid #000;border-left: 1px solid #000;">Thời gian</th>
									<th width="10%" style="border-top: 1px solid #000;border-left: 1px solid #000;">Tên/Chủ đề HN,HT</th>
                                    <th width="22%" style="border-top: 1px solid #000;border-left: 1px solid #000;">Đơn vị thực hiện</th>
                                   <th width=""style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">Nội dung hoạt động, thông tin</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dshnht)} 
								{$i=1}
								{$i = $i + $page}
                                {foreach $dshnht as $hnht}
                                    <tr>
                                        <td class="text-center" style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dshnht)}border-bottom: 1px solid #000;{/if}">{$i++}</td>
										 <td style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dshnht)+1}border-bottom: 1px solid #000;{/if}">
											{date_select($hnht.thoi_gian)}
										</td>
										<td style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dshnht)+1}border-bottom: 1px solid #000;{/if}">
											{$hnht.ten_hnht}
										</td>
										
                                        <td style="border-top: 1px solid #000;border-left: 1px solid #000;{if $i==count($dshnht)+1}border-bottom: 1px solid #000;{/if}">
											<span>{$hnht.coquan_th}</span>
										</td>
                                       
                                        <td style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;{if $i==count($dshnht)+1}border-bottom: 1px solid #000;{/if}">
											- Nội dung hoạt động: <b>{$hnht.noidung}</b><br>
												- Địa điểm: {$hnht.diadiem}<br>
												- Cơ quan cấp phép: {$hnht.cq_capphep}
										</td>
										
																	
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
			
            </div>
               
        </div>
      <!-- /.box -->

    </section>