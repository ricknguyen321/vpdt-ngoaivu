<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
		Thông tin chi tiết hội nghị, hội thảo
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:100%; justify-content: center;">
                        <div class="col-md-12" style="margin-top: 10px; ">
							{$vbdi = explode(',' , $thongtin[0]['vbdi'])}
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tên/Chủ đề HN, HT:</label>

                                    <div class="col-sm-8">
                                        <b style="color:blue">{($thongtin)?$thongtin[0]['ten_hnht']:''}</b>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tên đơn vị tổ chức:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['coquan_th']:''}</b>
                                    </div>
                                </div>  
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Nội dung hoạt động:</label>

                                    <div class="col-sm-8">
                                        {($thongtin)?$thongtin[0]['noidung']:''}
                                    </div>
                                </div>  
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Thời gian tổ chức:</label>

                                    <div class="col-sm-8">
                                        <b>{date_select(($thongtin)?$thongtin[0]['thoi_gian']:'')}</b>
                                    </div>
                                </div>   
								
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Địa điểm tổ chức:</label>

                                    <div class="col-sm-8">
                                        {($thongtin)?$thongtin[0]['diadiem']:''}
                                    </div> 
                                </div>   
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Báo cáo:</label>
									{$vbdi = explode(',' , $thongtin[0]['baocao'])}
                                    <div class="col-sm-8">
										{foreach $vbdi as $di} 
											{$di = trim($di)}
											{if is_numeric($di)}
												<a href="dsvanban?kyhieu={$di}" target="_blank"><b style="color:red">{$di}</b></a>, 
											{else}
												{$di}, 
											{/if}
										{/foreach}
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Cơ quan cấp phép:</label>

                                    <div class="col-sm-8">
										{($thongtin)?$thongtin[0]['cq_capphep']:''}
                                    </div>
                                </div>
								
                            </div>							
							
							<div class="col-md-6">
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng đại biểu người VN:</label>

                                    <div class="col-sm-8">
                                        {($thongtin)?$thongtin[0]['sldb1']:''}
                                    </div>
                                </div>
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng đại biểu người nước ngoài ở trong nước:</label>

                                    <div class="col-sm-8">
									   {($thongtin)?$thongtin[0]['sldb2']:''}
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng đại biểu người nước ngoài từ nước ngoài vào:</label>
                                    <div class="col-sm-8">
                                        {($thongtin)?$thongtin[0]['sldb3']:''}
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Đại biểu đến từ các nước:</label>

                                    <div class="col-sm-8">
                                        {($thongtin)?$thongtin[0]['sldb4']:''}
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Kinh phí:</label>

                                    <div class="col-sm-8">
                                        {($thongtin)?$thongtin[0]['kinhphi']:''}
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Người nhập thông tin:</label>

                                    <div class="col-sm-8">
									{$cb = layTTCB($thongtin[0]['FK_iMaCBNhap'])}
										{$cb[0].sHoTen}
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Thời gian nhập:</label>

                                    <div class="col-sm-8">
										{date_time2(($thongtin)?$thongtin[0]['ngay_nhap']:'')}
                                    </div>
                                </div>
								
                            </div>
                            
                        </div>
						
						
						<div class="col-md-12" style="margin-top:10px; margin-bottom:20px; text-align: center;">
							{if ($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaPhongHD'] == 77) || $vanban['sHoTen'] == $thongtin[0]['cvth'] || $vanban['PK_iMaCB'] == $thongtin[0]['FK_iMaCBNhap']}
                                <a href="nhaphnht?id={$thongtin[0]['id']}"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Sửa thông tin</button> </a>
							{/if}								&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dshnht"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">&#8594; Danh sách HN, HT</button> </a>
                        </div>
						
						{if !empty($tailieu)}
							<div class="col-md-12" style="text-align: -webkit-center;">
                            <div class="form-group">
                                <label for="">Tài liệu đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style=""></th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                        {foreach $tailieu as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td ><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></td>
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
                                </table><br>
                                
                            </div>
                        </div>
							{/if}
							
							
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');
		$('.control-label').css('font-weight', '200');
		$('.control-label').css('padding-top', '0px');
		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>