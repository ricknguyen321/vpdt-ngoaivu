<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3" style="width:20%">
                    <h3 class="font">
                        Danh sách các cuộc tiếp <b> ({$count})</b>
						<!--{if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs" onclick="myFunction()">Gửi nhiều email cùng lúc</button>{/if}-->
                    </h3>
                </div>
                <div class="col-sm-9" style="width:80%">
					<div class="col-md-5" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
									<input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm kiếm tổng hợp">
                               
                            </div>		
						</form>
					</div>
					<div class="col-md-4" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
								{if $vanban['FK_iMaPhongHD'] == 77 && date('Y') == $year} 
									<a href="nhapcuoctiep"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Tạo cuộc tiếp</button> </a>
								{/if}
								<a href="dscuoctiep?kx=word&tonghop={$tonghop}&cq={$cq}&qg={$quocgia}&ldtp={$ldtptiep}&tungay={$tungay}&denngay={$denngay}"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Xuất Word</button> </a>
                            </div>		
						</form>
					</div>
					 <div class="col-md-2 abc" >
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a  class="font" name="anhien" href="javascript:void(0);"><i class="fa fa-search"></i> Nâng cao</a></label>
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
			
				<form class="form-horizontal hide anhien" method="get" >
				<div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Cơ quan chủ trì</label>
                                    <div class="col-sm-8">
                                        <select name="cq" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn cơ quan chủ trì --</option>
                                           
                                                <option value="Thành ủy" {($cq=='Thành ủy')?'selected':''}>Thành ủy</option>
												<option value="HĐND Thành phố" {($cq=='HĐND Thành phố')?'selected':''}>HĐND Thành phố</option>
												<option value="UBND Thành phố" {($cq=='UBND Thành phố')?'selected':''}>UBND Thành phố</option>
                                               
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Quốc gia</label>

                                    <div class="col-sm-8">
                                        <select name="qg" id="" data-placeholder="" class="form-control select2" style="width:100%">
											<option value="">-- Chọn Quốc gia --</option>
                                            {if !empty($dsquocgia)}
                                                {foreach $dsquocgia as $qg} 
													<option value="{$qg.quoc_gia}" {if $qg.quoc_gia == $quocgia}selected{/if}>{$qg.quoc_gia}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lãnh đạo TP</label>

                                    <div class="col-sm-8">
                                        <select name="ldtp" id="" data-placeholder="" class="form-control select2" style="width:100%">
											<option value="">-- Chọn Lãnh đạo --</option>
                                            {if !empty($dsldtp)}
                                                {foreach $dsldtp as $ld} 
													<option value="{$ld.id}" {if $ld.id == $ldtptiep}selected{/if}>{$ld.ten_ld}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" style="display:flex">
                                    <label for="" class="col-sm-4 control-label">Ngày tiếp</label>

                                    <div class="col-sm-4">
                                        <input type="text" name="tungay" value="{($tungay)?$tungay:''}" class="form-control datepic datemask" id="">
                                    </div> <b>&#8594;</b>
									<div class="col-sm-4">
                                        <input type="text" name="denngay" value="{($denngay)?$denngay:''}" class="form-control datepic datemask" id="">
                                    </div>
                                </div>
                            </div>
                            
                         
                            <div class="col-md-12" style="text-align:center; margin-top: 10px">
                                <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
				</div>
                </form>
				
				
				<div class="pull-right" style="margin: -15px 10px">{$phantrang}</div>
				<i>Nhấn vào tên đoàn để xem chi tiết cuộc tiếp.</i>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px">STT</th>
									
									<th width="8%">Ngày tiếp</th>
									<th width="10%">Quốc gia</th>
                                    <th width="22%">Thông tin về đoàn</th>
                                    <th width="25%" class="text-center" >Thông tin lãnh đạo</th>
									<th width="">Thông tin cuộc tiếp</th>									
                                    <th width="6%"></th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dscuoctiep)} 
								{$i=1}
								{$i = $i + $page}
                                {foreach $dscuoctiep as $cuoctiep}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
										<td class="text-center">{date_select($cuoctiep.ngaytiep)}</td>
										<td class="text-center">{$cuoctiep.quoc_gia}</td>
                                        <td style="">
										<p>- Tên đoàn: <a href="cuoctiep?id={$cuoctiep.id}"><b style="color:blue">{$cuoctiep.tendoan}</b></a></p>
											- Trưởng đoàn: <b>{$cuoctiep.truongdoan}</b>
										</td>
                                       
                                        <td class="">
											<p>- CQ chủ trì: <b>{$cuoctiep.coquanct}</b></p>
											<p>- LĐ TP tiếp: <b>{$cuoctiep.chuc_vu_vt} {$cuoctiep.cq_vt} {$cuoctiep.ten_ld}</b></p>
											- LĐ Sở dự: <b>{$cuoctiep.ldso}</b> 
										</td>
										
										<td>
										<p>- Nội dung cuộc tiếp: {$cuoctiep.noidung}</p>
										<p>- Tặng phẩm: {$cuoctiep.tangpham}</p>
										<p>- Phiên dịch: {$cuoctiep.phiendich}</p>
											- Cán bộ thực hiện: <b>{$cuoctiep.cvth}</b> 

										</td>
                                       
                                        <td class="text-center">
											{if ($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaPhongHD'] == 77) || $vanban['sHoTen'] == $cuoctiep.cvth || $vanban['PK_iMaCB'] == $cuoctiep.FK_iMaCBNhap}
												<p><a href="nhapcuoctiep?id={$cuoctiep.id}" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a></p>
												<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Bạn có chắc muốn xóa?');" name="xoa" value="{$cuoctiep.id}"><i class="fa fa-trash"></i></button>
												
											{else}
												-
											{/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'right');
    });
</script>
<script>
function myFunction($ma, $loai, $sodi) {	
	window.open("{$url}upload_files_vbdi?ma=" + $ma + "&loai=" + $loai + "&isodi=" + $sodi,"phathanh","menubar=1,location=no,resizable=0,width=550,height=320, top=180, left=400");
}
</script>