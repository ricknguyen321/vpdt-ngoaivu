<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Danh sách văn bản trình ký
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" class="form-horizontal" method="get">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-3"><a href="{$url}vanbantrinhky?trangthai=2" class="btn btn-default">Văn bản đã xem</a></div>
                        <div class="col-md-3"><a href="{$url}vanbantrinhky?trangthai=1" class="btn btn-default">Văn bản chưa xem</a></div>
                        <div class="col-md-4">
                            <input type="text" class="hide" name="trangthai" value="{($trangthai==1)?$trangthai:2}">
                            <input name="chude" type="text" value="{($chude)?$chude:''}" class="form-control" placeholder="Nhập chủ đề tìm kiếm">
                        </div>
                        <div class="col-md-1"><button class="btn btn-primary btn-sm">Tìm kiếm</button></div>
                    </div>
                </div>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%">Loại văn bản</th>
                                    <th width="10%">Số ký hiệu</th>
                                    <th width="12%" class="text-center" >Ngày tháng</th>
                                    <th width="28%">Trích yếu</th>
                                    <th width="17%">Nơi nhận</th>
                                    <th width="6%">Tệp tin</th>
                                    <th width="10%">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sTenLVB}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td class="text-center"><input type="text" name="ngaythang[{$dl.PK_iMaVBDi}][]" style="width:100%" value="{date('d/m/Y')}" class="datepic datemask"></td>
                                        <td ><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin" style="color: black !important;"><b>{$dl.sMoTa}</b></a>{if !empty($dl.iSoDen)}<br> <b>Trả lời cho văn bản đến số:</b><b style="color:red;">{$dl.iSoDen}</b>{/if}<br><i>(Người ký: {$dl.sHoTen})</i></td>
                                        <td>{$dl.sNoiNhan}</td>
                                        <td class="text-center">{if $dl.iFile==1}<a target="_blank" {if $trangthai==2} id="0" {else} id="{$dl.PK_iMaVBDi}" {/if} href="{$dl.sDuongDan}" class="tin1 layma">{if layDuoiFile($dl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}</td>
                                        <td class="text-center"><span class="label label-{($trangthai==2)?'success':'warning'}">{($trangthai==2)?'Đã xem':'Chưa xem'}</span></td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">{$phantrang}</div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $(document).on('click','.layma',function(){
            var a = $(this).attr('id');
            if(a!=0)
            {
                $('#'+a).parent().parent().addClass('hide');
                $.ajax({
                    url:url,
                    type:'post',
                    data:{
                        action:'capnhat',
                        mavanban:a
                    }
                });
            }
        });
    });
</script>