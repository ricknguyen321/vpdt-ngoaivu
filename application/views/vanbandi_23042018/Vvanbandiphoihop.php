<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản đi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="20%">Người gửi</th>
                                    {if $trangthai==1}<th width="30%">Ý kiến gửi đi</th>{/if}
                                    <th width="5%" class="text-center">Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td ><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">{$dl.sMoTa}</a> <br>-Người nhập: {$dscanbo[$dl.FK_iMaCB_Nhap]}</td>
                                        <td>{if !empty($dl.sYKien)} - {$dl.sYKien} <br> {/if}- {$dscanbo[$dl.FK_iMaCB_Gui]} <br>- {date_time($dl.sThoiGian)}</td>
                                        {if $trangthai==1}<td class="">
                                            {if $trangthai==1}
                                                <p>
                                                    <select name="nguoinhan[{$dl.PK_iMaVBDi}][]" multiple="" data-placeholder="Chọn người nhận" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                        {if !empty($nguoinhan)}
                                                            {foreach $nguoinhan as $nn}
                                                            {if $dl.FK_iMaCB_Nhap != $nn.PK_iMaCB}
                                                            <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                                            {/if}
                                                            {/foreach}
                                                        {/if}
                                                    </select>
                                                </p>
                                            {/if}
                                            <p>
                                                <textarea name="ykien[{$dl.PK_iMaVBDi}]" class="form-control" rows="2"></textarea>
                                            </p>
                                           
                                        </td>{/if}
                                        <td class="text-center">
                                        <p>
                                            <a href="{$url}xulyvanbandiphoihop/{$dl.PK_iMaPH}" ata-toggle="tooltip" data-placement="top" data-original-title="Xử lý" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                        </p>
                                        {if $trangthai==1}
                                            <input type="text" name="maphoihop[{$dl.PK_iMaVBDi}]" value="{$dl.PK_iMaPH}" class="hide">
                                            <button type="submit" name="gui" value="{$dl.PK_iMaVBDi}" ata-toggle="tooltip" data-placement="top" data-original-title="Gửi" class="btn btn-default"><i class="fa fa-send-o"></i></button>
                                        {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>