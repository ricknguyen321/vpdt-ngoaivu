<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách văn bản
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Loại văn bản:</label>

                                <div class="col-sm-7 sl2">
                                    <div class="row">
                                        <select name="loaivanban" id="" class="form-control select2" style="width:100%" onchange="this.form.submit()">
                                            <option value="">Văn bản đã nhập</option>
                                            <option value="1">Đã gửi mail</option>
                                            <option value="2">Chưa gửi mail</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6" style="    margin-top: 20px;">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" name="anhien" href="javascript:void(0);">Tìm kiếm văn bản</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form class="form-horizontal anhien hide" method="post" enctype="multipart/form-data" autocomplete="off">
                <div class="row"><br>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                                <div class="col-sm-8">
                                    <select name="loaivanban" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn loại văn bản --</option>
                                        {if !empty($dsloaivanban)}
                                            {foreach $dsloaivanban as $l}
                                            <option value="{$l.PK_iMaLVB}" {($loaivanban)?($l.PK_iMaLVB==$loaivanban)?'selected':'':''}>{$l.sTenLVB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                <div class="col-sm-8">
                                    <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Nơi dự thảo</label>

                                <div class="col-sm-8">
                                    <select name="noiduthao" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn nơi dự thảo --</option>
                                        {if !empty($dsduthao)}
                                            {foreach $dsduthao as $d}
                                                <option value="{$d.PK_iMaPB}" {($noiduthao)?($noiduthao==$d.PK_iMaPB)?'selected':'':''}>{$d.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Ngày văn bản</label>

                                <div class="col-sm-8">
                                    <input type="text" name="ngaythang" value="{($ngaythang)?$ngaythang:''}" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class=" col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Trích yếu</label>
                                    <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người ký</label>

                                <div class="col-sm-8">
                                    <select name="nguoiky" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn người ký --</option>
                                        {if !empty($nguoiky)}
                                            {foreach $nguoiky as $nk}
                                                <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6 hide">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người nhập</label>

                                <div class="col-sm-8">
                                    <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn người nhập --</option>
                                        {if !empty($dsnguoinhap)}
                                            {foreach $dsnguoinhap as $nn}
                                                <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                        </div>
                    </div>
                </div>
            </form>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="6%" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Loại văn bản</th>
                                    <th width="" class="text-center">Số đi</th>
                                    <th width="" class="text-center">Số ký hiệu</th>
                                    <th width="30%" class="text-center">Trích yếu</th>
                                    <th width="20%" class="text-center visible-lg">Nơi gửi</th>
                                    <th width="" class="text-center">Trả lời VB số đến</th>
                                    <th width="5%" class="text-center">Tệp tin</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbandi)} {$i=1}
                                    {foreach $vanbandi as $di}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{$di.sTenLVB}</td>
                                            <td class="text-center" style="color: red"><b>{$di.iSoVBDi}</b></td>
                                            <td class="text-center">{$di.sKyHieu}</td>
                                            <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                            <td ><a href="{$url}thongtinvanban?id={$di.PK_iMaVBDi}" class="tin" style="color: black !important;"><b>{$di.sMoTa}</b></a><br><i>(Người ký: {$di.sHoTen})</i></td>
                                            <td class="visible-lg">{$di.sNoiNhan}</td>
                                            <td class="text-center" >{str_replace(",",", ",$di.iSoDen)}</td>
                                            <td class="text-center"><a class="tin" href="{$url}teptindi?id={$di.PK_iMaVBDi}"><i class="fa {if $di.iFile==0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $di.iFile==1} <br><a target="_blank" href="{$di.sDuongDan}" class="tin">{if layDuoiFile($di.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}</td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">{$phantrang}</div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
        $('td').css('vertical-align', 'middle');
        $('.anhien').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>
