<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="refresh" content="30"> 30 giây refesh trang 1 lần-->
  <link href="" rel="shortcut icon" type="image/x-icon" />
  <title>{$title}</title>
  <base href="">
  <meta name="author" content="Nguyễn Xuân Hải">
  <meta name="copyright" content="Sở Ngoại Vụ">
  <meta name="description" content="LỊCH HỌP DO LÃNH ĐẠO SỞ CHỦ TRÌ ">
  <meta name="robots" content="noindex,nofollow">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">
  <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
  <style>
    .container {
        font-family:"Times New Roman", Times, serif;
        font-size:13pt;
    }
    .indam{
        font-weight: bold;
    }
  </style>
</head>

<body><br/>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {if !empty($content)}
                    <h4 class="{if $content['class']=='info'}text-primary{else}text-danger{/if}">{$content['content']} {if $content['class']=='info'} &nbsp;&nbsp;&nbsp; <button class="btn btn-primary btn-xs" onclick="closeWin()">Đóng lại</button>{/if}</h4> 
                {/if}
                <h3 class="indam">Upload Files</h3>
                <h4>Mỗi lần upload không quá 10 tệp tin</h4>
                <h4>Thời gian chờ có thể lâu tùy vào tổng dung lượng upload.</h4>
                <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="form-group">
                        <input type="file" name="files[]" multiple class="form-control" readonly="">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="upload" value="upload" class="btn btn-primary">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function closeWin() {
            window.close();
        }
    </script>
</body>
</html>
