<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Thông tin văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <form class="form-horizontal">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Nơi nhận:</label>
                                    <div class="col-md-8">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Số ký hiệu: </label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['sKyHieu']}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Loại văn bản: </label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['sTenLVB']}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Số văn bản:</label>
                                    <div class="col-md-8">
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Ngày ký:</label>
                                    <div class="col-md-8">
                                        {date_select($thongtin[0]['sNgayVBDi'])}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Lĩnh vực:</label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['sTenLV']}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Trích yếu</label>
                                    <div>{$thongtin[0]['sMoTa']}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Người ký:</label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['sHoTen']}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Chú ý:</label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['sGhiChu']}
                                    </div>
                                </div>
                                {if !empty($thongtin[0]['iSoDen'])}
                                <div class="form-group">
                                    <label for="" class="col-md-7">Là văn bản đầu ra của văn bản đến số:</label>
                                </div>
                                {/if}
                            </div>
                            {if !empty($thongtin[0]['iSoDen'])}
                                <div class="col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Số đến</th>
                                            <th class="text-center">Ký hiệu</th>
                                            <th class="text-center">Nơi gửi đến</th>
                                            <th class="text-center">Trích yếu</th>
                                            <th class="text-center">Người ký</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if !empty($dssoden)}{$i=1}
                                        {foreach $dssoden as $so}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td class="text-center"><a class="tin1" href="{$url}thongtindaura?id={$so.PK_iMaVBDen}">{$so.iSoDen}</a></td>
                                                <td class="text-center">{$so.sKyHieu}</td>
                                                <td class="text-center">{$so.sTenDV}</td>
                                                <td>{$so.sMoTa}</td>
                                                <td class="text-center">{$so.sTenNguoiKy}</td>
                                            </tr>
                                        {/foreach}
                                    {/if}
                                    </tbody>
                                </table>
                                </div>
                            {/if}
                            <div class="col-md-6 hide" >
                                <div class="form-group">
                                    <label for="" class="col-md-4">Người nhập:</label>
                                    <div class="col-md-8">
                                        {$nguoinhap['sHoTen']}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 hide">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Quá trình xử lý văn bản chính:</label>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th class="text-center" width="5%">STT</th>
                                                <th class="text-center" width="20%">Thời gian</th>
                                                <th class="text-center" width="20%">Người gửi</th>
                                                <th class="text-center" width="30%">Nội dung</th>
                                                <th class="text-center" width="20%">Người nhận</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {if !empty($dschuyen)}{$i=1}
                                            {foreach $dschuyen as $ch}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td class="text-center">{date_time($ch.sThoiGian)}</td>
                                                    <td class="">{$dscanbo[$ch.FK_iMaCB_Gui]}</td>
                                                    <td class="">{$ch.sYKien}</td>
                                                    <td class="">{$dscanbo[$ch.FK_iMaCB_Nhan]}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12 hide">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Phối hợp xử lý:</label>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th class="text-center" width="5%">STT</th>
                                                <th class="text-center" width="15%">Thời gian</th>
                                                <th class="text-center" width="20%">Người gửi</th>
                                                <th class="text-center" width="20%">Người nhận</th>
                                                <th class="text-center" width="30%">Nội dung xử lý</th>
                                                <th class="text-center" width="10%">File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {if !empty($dschuyenPH)}{$i=1}
                                            {foreach $dschuyenPH as $ph}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td >{date_time($ph.sThoiGian)}</td>
                                                    <td class="text-center">{$dscanbo[$ph.FK_iMaCB_Gui]}</td>
                                                    <td class="text-center">{$dscanbo[$ph.FK_iMaCB_Nhan]}</td>
                                                    <td class="">{$ph.noidung_xuly}</td>
                                                    <td class="text-center">{if !empty($ph.sFile)}<a href="{$url}{$ph.sFile}" download>[Tải Về]</a>{/if}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Tệp tin đính kèm:</label>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th class="text-center" width="10%">STT</th>
                                                <th class="text-center" width="30%">Tên tệp tin</th>
                                                <th class="text-center" width="20%">Tải về</th>
                                                <th class="text-center" width="20%">Ngày nhập</th>
                                                <th class="text-center" width="20%">Người gửi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {if !empty($dsfile)}{$i=1}
                                            {foreach $dsfile as $f}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td >{$f.sTenFile}</td>
                                                    <td class="text-center"><a class="tin1" href="{$f.sDuongDan}" download>[Tải tài liệu]</a></td>
                                                    <td class="text-center">{date_time($f.sThoiGian)}</td>
                                                    <td class="text-center">{if !empty($f.FK_iMaCB)}{$dscanbo[$f.FK_iMaCB]}{/if}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                        </tbody>
                                    </table><br>
                                    <a class="btn btn-default" href="javascript: history.back(1)" id="backLink">Quay lại >></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>