<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản trình ký
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" class="form-horizontal" method="get">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-2"><a href="{$url}vanbandichoxuly_cvp?tinhtrang=1" class="btn btn-default">Văn bản chưa xem</a></div>
                            <div class="col-md-2"><a href="{$url}vanbandichoxuly_cvp?tinhtrang=2" class="btn btn-default">Văn bản đã xem</a></div>
                            <div class="col-md-2"><a href="{$url}vanbandichoxuly_cvp?tinhtrang=3" class="btn btn-default">Văn bản góp ý</a></div>
                            <div class="col-md-4">
                                <input type="text" class="hide" name="tinhtrang" value="{$tinhtrang}">
                                <input name="chude" type="text" value="{($chude)?$chude:''}" class="form-control" placeholder="Nhập chủ đề tìm kiếm">
                            </div>
                            <div class="col-md-1"><button class="btn btn-primary btn-sm">Tìm kiếm</button></div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="20%">Ý kiến</th>
                                    <th width="6%">Tệp tin</th>
                                    <th width="30%">Ý kiến gửi đi</th>
                                    <th width="5%" class="text-center">Gửi</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td ><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">{$dl.sMoTa}</a> <br>-Người nhập: {$dl.sHoTen}</td>
                                        <td>{$dl.ykien_cvp}</td>
                                        <td class="text-center"><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><i class="fa {if $dl.iFile==0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $dl.iFile==1} <br><a target="_blank" href="{$dl.sDuongDan}" id="{$dl.PK_iMaVBDi}" class="tin1 layma">{if layDuoiFile($dl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if} 
                                            <p><span class="label label-{($dl.trangthai_xem_cvp==2)?'success':(($dl.trangthai_xem_cvp==1)?'warning':'info')}">{($dl.trangthai_xem_cvp==2)?'Đã xem':(($dl.trangthai_xem_cvp==1)?'Chưa xem':'Góp ý')}</span></p>
                                        </td>
                                        <td class="">
                                            <p>
                                                <textarea name="ykien[{$dl.PK_iMaVBDi}]" placeholder="Nhập ý kiến, góp ý tại đây" class="form-control" rows="3"></textarea>
                                            </p>
                                        </td>
                                        <td class="text-center">
                                            {if $trangthai==7 || $trangthai==6}
                                                {if $trangthai==$dl.trangthai_chuyen}
                                                <p><button type="submit" name="duyet" onclick="return confirm('Bạn có muốn duyệt không?');" value="{$dl.PK_iMaVBDi}" ata-toggle="tooltip" data-placement="top" data-original-title="Duyệt" class="btn btn-default"><i class="fa fa-check-square-o"></i></button></p>
                                                {/if}
                                            {/if}
                                            <button type="submit" name="gui" value="{$dl.PK_iMaVBDi}" ata-toggle="tooltip" data-placement="top" data-original-title="Gửi" class="btn btn-default"><i class="fa fa-send-o"></i></button>
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $(document).on('click','.layma',function(){
            var a = $(this).attr('id');
            $.ajax({
                url:url,
                type:'post',
                data:{
                    action:'capnhat',
                    maVB:a
                }
            });
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>