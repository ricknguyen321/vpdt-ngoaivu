<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản trình ký
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {if $trangthai==33||$trangthai==6||$trangthai==7}
                    <form action="" class="form-horizontal" method="get">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-2"><a href="{$url}vanbandichoxuly?tinhtrang=1" class="btn btn-default">Văn bản chưa xem</a></div>
                                <div class="col-md-2"><a href="{$url}vanbandichoxuly?tinhtrang=2" class="btn btn-default">Văn bản đã xem</a></div>
                                <div class="col-md-2"><a href="{$url}vanbandichoxuly?tinhtrang=3" class="btn btn-default">Văn bản góp ý</a></div>
                                <div class="col-md-4">
                                    <input type="text" class="hide" name="tinhtrang" value="{$tinhtrang}">
                                    <input name="chude" type="text" value="{($chude)?$chude:''}" class="form-control" placeholder="Nhập chủ đề tìm kiếm">
                                </div>
                                <div class="col-md-1"><button class="btn btn-primary btn-sm">Tìm kiếm</button></div>
                            </div>
                        </div>
                    </form>
                {/if}
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="20%">Ý kiến</th>
                                    <th width="6%">Tệp tin</th>
                                    <th width="30%">Ý kiến gửi đi</th>
                                    <th width="5%" class="text-center">Gửi</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td ><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">{$dl.sMoTa}</a> <br>-Người nhập: {$dl.sHoTen}</td>
                                        {if $trangthai==1}
                                            <td>{$dl.ykien_cv}</td>
                                        {/if}
                                        {if $trangthai==2}
                                            <td>{$dl.ykien_pp}</td>
                                        {/if}
                                        {if $trangthai==3 || $trangthai==333}
                                            <td>{$dl.ykien_tp}</td>
                                        {/if}
                                        {if $trangthai==33}
                                            <td>{$dl.ykien_bpth}</td>
                                        {/if}
                                        {if $trangthai==4}
                                            <td>{$dl.ykien_pcc}</td>
                                        {/if}
                                        {if $trangthai==5}
                                            <td>{$dl.ykien_tcc}</td>
                                        {/if}
                                        {if $trangthai==6}
                                            <td>{$dl.ykien_pgd}</td>
                                        {/if}
                                        {if $trangthai==7}
                                            <td>{$dl.ykien_gd}</td>
                                        {/if}
                                        <td class="text-center"><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><i class="fa {if $dl.iFile==0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $dl.iFile==1} <br><a target="_blank" href="{$dl.sDuongDan}" id="{$dl.PK_iMaVBDi}" class="tin1 layma">{if layDuoiFile($dl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if} 
                                            <p><span class="label label-{($dl.iSoVBDi>0)?'success':'warning'}">{($dl.iSoVBDi>0)?'Đã xem':'Chưa xem'}</span></p>
                                        </td>
                                        <td class="">
                                        {if $trangthai==1}
                                             <p>
                                                <select name="nguoinhan1[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan1} --</option>
                                                    {if !empty($nguoinhan1)}
                                                        {foreach $nguoinhan1 as $nn1}
                                                        <option value="{$nn1.PK_iMaCB}">{$nn1.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            {if !empty($dl.phophong)}
                                            <p>
                                                <select name="nguoinhan2[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan2} --</option>
                                                        <option value="{$dl.phophong}">{$dscanbo[$dl.phophong]}</option>
                                                </select>
                                            </p>
                                            {/if}
                                        {/if}
                                        {if $trangthai==2}
                                            <p>
                                                <select name="nguoinhan1[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan1} --</option>
                                                    {if !empty($nguoinhan1)}
                                                        {foreach $nguoinhan1 as $nn1}
                                                        <option value="{$nn1.PK_iMaCB}">{$nn1.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            {if !empty($dl.chuyenvien)}
                                            <p>
                                                <select name="nguoinhan2[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan2} --</option>
                                                        <option value="{$dl.chuyenvien}">{$dscanbo[$dl.chuyenvien]}</option>
                                                </select>
                                            </p>
                                            {/if}
                                        {/if}
                                        {if $trangthai==333}
                                            <p>
                                                <select name="nguoinhan1[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan1} --</option>
                                                    {if !empty($nguoinhan1)}
                                                        {foreach $nguoinhan1 as $nn1}
                                                        <option value="{$nn1.PK_iMaCB}">{$nn1.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <select name="nguoinhan2[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan2} --</option>
                                                    {if !empty($nguoinhan2)}
                                                        {foreach $nguoinhan2 as $nn2}
                                                        <option value="{$nn2.PK_iMaCB}">{$nn2.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            {if !empty($dl.phophong)}
                                            <p>
                                                <select name="nguoinhan3[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan3} --</option>
                                                        <option value="{$dl.phophong}">{$dscanbo[$dl.phophong]}</option>
                                                </select>
                                            </p>
                                            {/if}
                                            {if !empty($dl.chuyenvien)}
                                            <p>
                                                <select name="nguoinhan4[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan4} --</option>
                                                        <option value="{$dl.chuyenvien}">{$dscanbo[$dl.chuyenvien]}</option>
                                                </select>
                                            </p>
                                            {/if}
                                        {/if}
                                        {if $trangthai==3 || $trangthai==5}
                                            <p>
                                                <select name="nguoinhan1[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan1} --</option>
                                                    {if !empty($nguoinhan1)}
                                                        {foreach $nguoinhan1 as $nn1}
                                                        <option value="{$nn1.PK_iMaCB}">{$nn1.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <select name="nguoinhan2[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan2} --</option>
                                                    {if !empty($nguoinhan2)}
                                                        {foreach $nguoinhan2 as $nn2}
                                                        <option value="{$nn2.PK_iMaCB}">{$nn2.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <select name="nguoinhan6[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan6} --</option>
                                                    {if !empty($nguoinhan6)}
                                                        {foreach $nguoinhan6 as $nn6}
                                                        <option value="{$nn6.PK_iMaCB}">{$nn6.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <select name="nguoinhan3[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan3} --</option>
                                                    {if !empty($nguoinhan3)}
                                                        {foreach $nguoinhan3 as $nn3}
                                                        <option value="{$nn3.PK_iMaCB}">{$nn3.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            {if $trangthai==5}
                                                {if !empty($dl.phochicuc)}
                                                    <p>
                                                        <select name="nguoinhan7[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                            <option value="">-- {$tennguoinhan7} --</option>
                                                            <option value="{$dl.phochicuc}">{$dscanbo[$dl.phochicuc]}</option>
                                                        </select>
                                                    </p>
                                                {/if}
                                                {if !empty($dl.truongphong)}
                                                    <p>
                                                        <select name="nguoinhan8[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                            <option value="">-- {$tennguoinhan8} --</option>
                                                            <option value="{$dl.truongphong}">{$dscanbo[$dl.truongphong]}</option>
                                                        </select>
                                                    </p>
                                                {/if}
                                            {/if}
                                            {if !empty($dl.phophong)}
                                                <p>
                                                    <select name="nguoinhan4[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                        <option value="">-- {$tennguoinhan4} --</option>
                                                        <option value="{$dl.phophong}">{$dscanbo[$dl.phophong]}</option>
                                                    </select>
                                                </p>
                                            {/if}
                                            {if !empty($dl.chuyenvien)}
                                                <p>
                                                    <select name="nguoinhan5[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                        <option value="">-- {$tennguoinhan5} --</option>
                                                        <option value="{$dl.chuyenvien}">{$dscanbo[$dl.chuyenvien]}</option>
                                                    </select>
                                                </p>
                                            {/if}
                                        {/if}
                                        {if $trangthai==4}
                                            <p>
                                                <select name="nguoinhan1[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan1} --</option>
                                                    {if !empty($nguoinhan1)}
                                                        {foreach $nguoinhan1 as $nn1}
                                                        <option value="{$nn1.PK_iMaCB}">{$nn1.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            {if !empty($dl.truongphong)}
                                            <p>
                                                <select name="nguoinhan2[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan2} --</option>
                                                        <option value="{$dl.truongphong}">{$dscanbo[$dl.truongphong]}</option>
                                                </select>
                                            </p>
                                            {/if}
                                            {if !empty($dl.phophong)}
                                            <p>
                                                <select name="nguoinhan3[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan3} --</option>
                                                        <option value="{$dl.phophong}">{$dscanbo[$dl.phophong]}</option>
                                                </select>
                                            </p>
                                            {/if}
                                            {if !empty($dl.chuyenvien)}
                                            <p>
                                                <select name="nguoinhan4[{$dl.PK_iMaVBDi}]" id="" class="form-control select2 nguoinhan" style="width:100%">
                                                    <option value="">-- {$tennguoinhan4} --</option>
                                                        <option value="{$dl.chuyenvien}">{$dscanbo[$dl.chuyenvien]}</option>
                                                </select>
                                            </p>
                                            {/if}
                                        {/if}
                                            <p>
                                                <textarea name="ykien[{$dl.PK_iMaVBDi}]" placeholder="Nhập ý kiến, góp ý tại đây" class="form-control" rows="3"></textarea>
                                            </p>
                                           
                                        </td>
                                        <td class="text-center">
                                            {if $trangthai==7 || $trangthai==6}
                                                {if $trangthai==$dl.trangthai_chuyen}
                                                <p><button type="submit" name="duyet" onclick="return confirm('Bạn có muốn duyệt không?');" value="{$dl.PK_iMaVBDi}" ata-toggle="tooltip" data-placement="top" data-original-title="Duyệt" class="btn btn-default"><i class="fa fa-check-square-o"></i></button></p>
                                                {/if}
                                            {/if}
                                            <button type="submit" name="gui" value="{$dl.PK_iMaVBDi}" ata-toggle="tooltip" data-placement="top" data-original-title="Gửi" class="btn btn-default"><i class="fa fa-send-o"></i></button>
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $(document).on('click','.layma',function(){
            var a = $(this).attr('id');
            $.ajax({
                url:url,
                type:'post',
                data:{
                    action:'capnhat',
                    maVB:a
                }
            });
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>