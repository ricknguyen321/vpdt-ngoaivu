<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản đi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="20%">Người gửi</th>
                                    <th width="25%">Nội dung xử lý</th>
                                    <th width="10%" class="text-center">File</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td ><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">{$dl.sMoTa}</a> <br>-Người nhập: {$dscanbo[$dl.FK_iMaCB_Nhap]}</td>
                                        <td>{if !empty($dl.sYKien)} - {$dl.sYKien} <br> {/if}- {$dscanbo[$dl.FK_iMaCB_Gui]} <br>- {date_time($dl.sThoiGian)}</td>
                                        <td class="">
                                          - {$dl.noidung_xuly} <br> - {date_time($dl.thoigian_xuly)}
                                        </td>
                                        <td class="text-center">
                                            {if !empty($dl.sFile)} <a href="{$url}{$dl.sFile}">[Tải Về]</a>{/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>