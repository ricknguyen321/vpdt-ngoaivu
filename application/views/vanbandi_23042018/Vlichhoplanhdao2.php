<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="refresh" content="30"> 30 giây refesh trang 1 lần-->
  <link href="" rel="shortcut icon" type="image/x-icon" />
  <title>{$title}</title>
  <base href="">
  <meta name="author" content="Nguyễn Xuân Hải">
  <meta name="copyright" content="Sở Ngoại Vụ">
  <meta name="description" content="LỊCH HỌP DO LÃNH ĐẠO SỞ CHỦ TRÌ ">
  <meta name="robots" content="noindex,nofollow">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">
  <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$url}assets/js/plugins/font-awesome/css/font-awesome.min.css">
  <style>
    .container {
        font-family:"Times New Roman", Times, serif;
        font-size:13pt;
    }
    .indam{
        font-weight: bold;
    }
  </style>
</head>

<body><br/>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="indam text-center">LỊCH HỌP CỦA LÃNH ĐẠO</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    <a href="{$url}lichhoplanhdao?thoigian=tuantruoc" class="btn btn-info">Tuần trước</a> 
                    <a href="{$url}lichhoplanhdao?thoigian=hientai" class="btn btn-info">Tuần hiện tại</a> 
                    <a href="{$url}lichhoplanhdao?thoigian=tuantiep" class="btn btn-info">Tuần Tiếp theo</a>
                </div>
                <form action="" method="get" class="hide">
                    <div class="col-md-3">
                        <input type="text" class="hide" name="{($thoigian)?'thoigian':''}" value="{($thoigian)?$thoigian:''}">
                        <select name="lanhdao" id="" class="form-control">
                            <option value="">-- Chọn lãnh đạo --</option>
                            {if !empty($lanhdao)}
                                {foreach $lanhdao as $ld}
                                    {if $ld.iQuyenHan_DHNB >3}
                                        <option value="{$ld.PK_iMaCB}" {($malanhdao)?($malanhdao==$ld.PK_iMaCB)?'selected':'':''}>{($ld.iQuyenHan_DHNB==4)?'GĐ':'PGĐ'}. {$ld.sHoTen}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-info">Lọc</button>
                    </div>
                </form>
            </div>
        </div><br>
        <div class="row">
        <form action="" method="post" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr style="background:#ccc;">
                        <th width="6%" class="text-center">Thời gian</th>
                        <th width="40%" class="text-center">Nội dung giấy mời</th>
                        <th width="20%" class="text-center">Lãnh đạo chủ trì</th>
                        <th width="20%" class="text-center">Phòng ban</th>
                    </tr>
                    {if !empty($ngaytrongtuan)}
                        {foreach $ngaytrongtuan as $n}
                            <tr>
                                <td colspan="4" class="text-center">
                                    <b>{$n.0} - {$n.1}</b>
                                </td>
                            </tr>
                            {if isset($n.sang)}
                            <tr>
                                <td colspan="4" class="text-center">
                                    <b>{$n.sang.name} - {$n.1}</b>
                                </td>
                            </tr>
                            {foreach $n.sang.giaymoi as $sang}
                                <tr>
                                    <td>{$sang.sGioMoi}</td>
                                    <td>
                                        <p><b>{$sang.sTenDV}</b></p>
                                        <p>{$sang.sMoTa}</p>
                                        {if !empty($sang.sNoiDung)}
                                        <p>{$sang.sNoiDung}</p>
                                        {/if}
                                        <p>Địa điểm: <b>{$sang.sDiaDiemMoi}</b></p>
                                    </td>
                                    <td>
                                        <p class="indam">Đ/C {$sang.sHoTen} chủ trì</p>
                                    </td>
                                    <td>
                                        <p class="indam">{$sang.sTenPB}</p>
                                    </td>
                                </tr>
                            {/foreach}
                            {/if}
                            {if isset($n.chieu)}
                            <tr>
                                <td colspan="4" class="text-center">
                                    <b>{$n.chieu.name} - {$n.1}</b>
                                </td>
                            </tr>
                            {foreach $n.chieu.giaymoi as $chieu}
                                <tr>
                                    <td>{$chieu.sGioMoi}</td>
                                    <td>
                                        <p><b>{$chieu.sTenDV}</b></p>
                                        <p>{$chieu.sMoTa}</p>
                                        {if !empty($chieu.sNoiDung)}
                                        <p>{$chieu.sNoiDung}</p>
                                        {/if}
                                        <p>Địa điểm: <b>{$chieu.sDiaDiemMoi}</b></p>
                                    </td>
                                    <td>
                                       <p class="indam">Đ/C {$chieu.sHoTen} chủ trì</p>
                                    </td>
                                    <td>
                                        <p class="indam">{$chieu.sTenPB}</p>
                                    </td>
                                </tr>
                            {/foreach}
                            {/if}
                            
                        {/foreach}
                    {/if}
                    </tbody>    
                </table>
            </div>
        </form>
        </div>
        <style>
            .open-small-chat1 {
                height: 38px;
                width: 38px;
                display: block;
                background: #3c8dbc;
                padding: 9px 8px;
                text-align: center;
                color: #fff;
                border-radius: 50%;
            }
        </style>
        <div id="small-chat" style="    position: fixed;bottom: 20px;right: 70px;z-index: 100;">
            <a href="{$url}welcome.html" class="open-small-chat1" style="cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Về Trang Chủ">
                <i class="fa fa-home" style="font-size: 20px;"></i>
            </a>
        </div>
    <style>
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #0c0b0b;
        }
        hr{
            border-top: 1px solid #9E9E9E;
            margin: 10px 0px;
        }
        h2, h3 {
            margin-top: 0px;
            margin-bottom: 20px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('td').css('vertical-align', 'middle');
            $('th').css('vertical-align', 'middle');
        });
    </script>

</body>
</html>
