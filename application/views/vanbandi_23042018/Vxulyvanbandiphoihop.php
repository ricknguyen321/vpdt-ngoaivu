<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Xử lý văn bản đi phối hợp
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Xử lý công việc</div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Nội dung *</label>
                                            <div class="col-sm-9">
                                                <textarea name="ykien" id="" class="form-control" required rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Chọn tệp tin *</label>
                                            <div class="col-sm-9">
                                                <input type="file" name="files[]" readonly="" class="form-control" id="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for=""class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <a href="javascript: history.back(1)" class="btn btn-default">Quay lại >></a> <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>