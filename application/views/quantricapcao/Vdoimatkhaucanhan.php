<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Đổi mật khẩu
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
				 <div class="form-group">
					 <div class="col-md-12" style="margin-left: 25%">
						<p><p><h4>Quy tắc đổi mật khẩu: </h4></p>
						Mật khẩu là tập hợp của các kí tự <b>[a-z], [0-9] và kí tự đặc biệt !@#$&* </b><br>
						Tối thiểu phải có <b>8 kí tự</b> tối đa 16 kí tự. <br>
						Bắt buộc phải có tối thiểu <b>01 chữ thường, 01 số và 01 kí tự đặc biệt</b>. <br></p>
					</div>
				</div>
                    <div class="col-md-12">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="form-group">
                                <label for="">Tài khoản</label>
                                <input type="text" disabled="true" value="{$vanban['sTaiKhoan']}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Mật khẩu cũ</label>
                                <input type="password" name="matkhaucu" value="{($matkhaucu)?$matkhaucu:''}" minlength="3" required placeholder="Nhập mật khẩu cũ" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Mật khẩu mới</label>
                                <input type="password" name="matkhaumoi" value="{($matkhaumoi)?$matkhaumoi:''}" minlength="3" required placeholder="Nhập mật khẩu mới" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Xác nhận mật khẩu</label>
                                <input type="password" name="xacnhan" value="{($xacnhan)?$xacnhan:''}" minlength="3" required placeholder="Xác nhận lại mật khẩu mới" class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" name="doimatkhau" value="doimatkhau" class="btn btn-primary">Đổi mật khẩu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>