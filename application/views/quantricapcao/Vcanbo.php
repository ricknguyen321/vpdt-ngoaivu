<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quản lý thành viên
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="{if empty($active)}active{else}''{/if}"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Danh sách thành viên</a></li>
                        <li role="presentation" class="{if !empty($active)}active{else}''{/if}"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Cập nhật thành viên</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane {if empty($active)}active{else}NULL{/if}" id="home">
                            <div class="row"><br>
                                <div class="col-md-12">
                                    <form action="" method="get">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <select name="phongban" style="width:100%" id="" class="form-control select2">
                                                    <option value="">-- Chọn phòng ban --</option>
                                                    {if !empty($dsphongban)}
                                                        {foreach $dsphongban as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {($phongban)?($phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <select name="chucvu" style="width:100%" id="" class="form-control select2">
                                                    <option value="">-- Chọn chức vụ --</option>
                                                    {if !empty($dschucvu)}
                                                        {foreach $dschucvu as $cv}
                                                            <option value="{$cv.PK_iMaCV}" {($chucvu)?($chucvu==$cv.PK_iMaCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <select name="quyenhan" style="width:100%" id="" class="form-control select2">
                                                    <option value="">-- Chọn quyền hạn --</option>
                                                    {if !empty($quyenhan)}
                                                        {foreach $quyenhan as $qh}
                                                            <option value="{$qh.ma}" {($quyen)?($quyen==$qh.ma)?'selected':'':''}>{$qh.ten}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" value="{($hoten)?$hoten:''}" name="hoten" placeholder="Nhập họ tên tại đây" class="form-control">
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-primary">Tìm kiếm</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row"><br/>
                                <div class="col-md-12"  style="overflow-x:auto;">
                                    <table id="" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:5%" class="text-center">STT</th>
                                                <th style="width:20%">Họ Tên</th>
                                                <th style="width:10%">Tài khoản</th>
                                                <th style="width:10%">Mật khẩu</th>
                                                <th style="width:20%">Quyền hạn</th>
                                                <th style="width:12%">Điện thoại</th>
                                                <th style="width:13%">Trạng Thái</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {if !empty($dsdulieu)}{$i=1}
                                                {foreach $dsdulieu as $dl}
                                                    <tr>
                                                        <td>{$i++}</td>
                                                        <td><a href="{$url}canbo?id={$dl.PK_iMaCB}">{$dl.sHoTen}</a></td>
                                                        <td><a href="{$url}canbo?id={$dl.PK_iMaCB}">{$dl.sTaiKhoan}</a></td>
                                                        <td>{$dl.sBanRo}</td>
                                                        <td>
                                                        {foreach $quyenhan as $quyen}
                                                            {if $quyen.ma==$dl.iQuyenHan_DHNB}
                                                                {$quyen.ten}
                                                            {/if}
                                                        {/foreach}
                                                        </td>
                                                        <td>{$dl.sDienThoai}</td>
                                                        <td class="text-center"><label for="" class="label {($dl.iTrangThai==0)?'label-success':'label-warning'}">{($dl.iTrangThai==0)?'Hoạt động':'Tạm khóa'}</label></td>
                                                    </tr>
                                                {/foreach}
                                            {/if}
                                        </tbody>
                                    </table>
                                    <div class="pull-right">{$phantrang}</div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!--end danh sách đảng viên-->
                        
                        <div role="tabpanel" class="tab-pane {if !empty($active)}active{else}NULL{/if}" id="profile">
                            <form action=""  name="hocviencamtinhdang" method="post" accept-charset="utf-8" enctype="multipart/form-data" autocomplete="off">
                            <div class="row"><br/>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Họ tên *</label>
                                            <input type="text" name="hoten" class="form-control" placeholder="Nhập tên thành viên" required value="{($thongtin)?($thongtin[0]['sHoTen']):NULL}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phòng ban hoạt động *</label>
                                            <select name="phongbanhoatdong" style="width:100%" id="" class="form-control select2">
                                                <option value="">-- Chọn phòng ban hoạt động --</option>
                                                {if !empty($dsphongban)}
                                                    {foreach $dsphongban as $pb}
                                                        <option value="{$pb.PK_iMaPB}" {($thongtin)?($thongtin[0]['FK_iMaPhongHD']==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="col-md-12"><label for="">Phòng ban phụ trách *</label></div>
                                    {if !empty($dsphongban)}
                                        {foreach $dsphongban as $pbpt}
                                            <div class="col-md-6">
                                                <input type="checkbox" name="phongbanphutrach[]"
                                                {if !empty($phongph)}
                                                    {foreach $phongph as $ph}
                                                        {if $ph==$pbpt.PK_iMaPB}checked{/if}
                                                    {/foreach}
                                                {/if}
                                                value="{$pbpt.PK_iMaPB}"> {$pbpt.sTenPB}
                                            </div>
                                        {/foreach}
                                    {/if}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tên đăng nhập *</label>
                                            <input type="text" name="taikhoan" class="form-control" placeholder="Nhập tên tài khoản" required value="{($thongtin)?($thongtin[0]['sTaiKhoan']):NULL}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Mật khẩu *</label>
                                            <input type="password" name="matkhau" class="form-control" placeholder="Nhập mật khẩu" {($thongtin)?'readonly':'required minlength="3"'} value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Xác nhận mật khẩu *</label>
                                            <input type="password" name="xacnhan" class="form-control" placeholder="Xác nhận mật khẩu" {($thongtin)?'readonly':'required minlength="3"'} value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Trạng thái</label>
                                            <label style="padding-left:25px">
                                                <input type="radio" name="trangthai" value="0" class="flat-red" checked>  Hoạt động
                                            </label>
                                            <label style="padding-left:25px">
                                                <input type="radio" name="trangthai" value="1" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==1}checked{/if}{/if} class="flat-red">  Tạm khóa
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Chức vụ *</label>
                                            <select name="chucvu" style="width:100%" id="" class="form-control select2">
                                                <option value="">-- Chọn chức vụ --</option>
                                                {if !empty($dschucvu)}
                                                    {foreach $dschucvu as $cv}
                                                        <option value="{$cv.PK_iMaCV}" {($thongtin)?($thongtin[0]['FK_iMaCV']==$cv.PK_iMaCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Điện thoại*</label>
                                            <input type="text" name="dienthoai" class="form-control" placeholder="Nhập số điện thoại" required value="{($thongtin)?($thongtin[0]['sDienThoai']):NULL}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" name="email" class="form-control" placeholder="Nhập email" value="{($thongtin)?($thongtin[0]['sEmail']):NULL}">
                                        </div>
                                        <div class="form-group">
                                            <select name="phongchicuc" id="" class="form-control select2" style="width:100%">
                                                <option value="0">-- Chọn phòng của chi cục --</option>
                                                {foreach $phongchicuc as $pcc}
                                                    <option value="{$pcc.ma}" {($thongtin)?($thongtin[0]['iQuyenDB']==$pcc.ma)?'selected':'':''}>{$pcc.ten}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" style="">
                                        <label class="col-sm-2">Loại phần mềm</label>
                                        <div class="col-sm-8" style="padding-left: 5px;">
                                            <label>
                                                <input type="radio" name="phanmem" value="1" class="flat-red" checked>  Điều hành nội bộ
                                            </label>
                                            <label style="padding-left:25px">
                                                <input type="radio" name="phanmem" value="2" {if !empty($thongtin)} {if $thongtin[0]['iPhanMem']==2}checked{/if}{/if} class="flat-red">  Truyền nhận văn bản
                                            </label>
                                            <label style="padding-left:25px">
                                                <input type="radio" name="phanmem" value="3" {if !empty($thongtin)} {if $thongtin[0]['iPhanMem']==3}checked{/if}{/if} class="flat-red">  Cả hai
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row"><br>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label for="">Quyền hạn tại hệ thống điều hành nội bộ:</label> <br>
                                        <input value="1" name="quyendhnb" type="radio"> Quản trị <i>(Được thao tác tại mọi chức năng)</i> <br>
                                        <input value="2" name="quyendhnb" type="radio"> Văn thư <i>(Nhập văn bản và tạo ra các thuộc tính văn bản)</i>  <br>
                                        <input value="9" name="quyendhnb" type="radio"> Văn thư chuyên trách <i>(Quyền văn thư và có thể kiêm nghiệm tác vụ ban giám đốc)</i> <br>
                                        <input value="3" name="quyendhnb" type="radio"> Chánh văn phòng <i>(Xem nội dung và chuyển tiếp văn bản)</i> <br>
                                        <input value="4" name="quyendhnb" type="radio"> Giám đốc <i>(Xem toàn bộ văn bản, chỉ đạo giải quyết với văn bản của mình)</i> <br>
                                        <input value="5" name="quyendhnb" type="radio"> Phó giám đốc <i>(Xem văn bản của mình, chỉ đạo giải quyết)</i> <br>
                                        <input value="6" name="quyendhnb"type="radio"> Trưởng(giám đốc) phòng/ban/chi cục/trung tâm/ <i>(Xem văn bản của mình, chỉ đạo giải quyết hoặc tự giải quyết)</i> <br>
                                        <input value="7" name="quyendhnb" type="radio"> Phó phòng/ban(giám đốc) phòng/ban/chi cục/trung tâm/ <i>(Xem và giải quyết văn bản của mình)</i> <br>
                                        <input value="8" name="quyendhnb" checked="" type="radio"> Chuyên viên <i>(Xem và giải quyết văn bản của mình)</i><br>
                                        <input value="13" name="quyendhnb" type="radio"> Văn thư chi cục <i>(Nhập văn bản và tạo ra các thuộc tính văn bản)</i>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Quyền hạn tại hệ thống điều hành nội bộ:</label> <br>
                                        <input value="3" name="quyentnvb" type="radio"> Văn thư  <br>
                                        <input value="2" name="quyentnvb" type="radio"> Người dùng <br>
                                        <input value="1" name="quyentnvb" checked="" type="radio"> Quản trị <i>(Được thao tác tại mọi chức năng)</i>
                                    </div>  

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-8 col-md-4">
                                        <button type="submit" class="btn btn-primary pull-right margin-l" name="luudulieu" value="luudulieu">{($active)?'Cập nhật':'Thêm mới'}</button>
                                        {if !empty($active)}<a href="{$url}canbo" class="btn btn-default pull-right ">Quay lại >></a>{/if}
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <!--end thông tin lý lịch đảng viên-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).ready(function() {
        {if !empty($thongtin)} 
            $("input[name=quyendhnb][value='{$thongtin[0]['iQuyenHan_DHNB']}']").prop("checked",true); 
            $("input[name=quyentnvb][value='{$thongtin[0]['iQuyenHan_TNVB']}']").prop("checked",true); 
        {/if}
        $('label').css('text-align', 'left');
    });
</script>
<script>
    $(document).ready(function() {
        $(document).on('change','input[name=taikhoan]',function(){
            var url      = window.location.href;
            var id       = getParameterByName('id');
            var taikhoan = $('input[name=taikhoan]').val();
            $.ajax({
                url:url,
                type:'post',
                data:{
                    action:'kiemtrataikhoan',
                    id:id,
                    taikhoan:taikhoan
                },
                success:function(reponsive)
                {
                    var result = JSON.parse(reponsive);
                    if(result=='trung'){
                        showMessage('Tài khoản này đã tồn tại!','danger');
                        $('button[name=luudulieu]').attr('type','button');
                    }
                    else{
                        $('button[name=luudulieu]').attr('type','submit');
                    }
                }

            });
        });
        $(document).on('click','button[name=luudulieu]',function(){
            var matkhau  = $('input[name=matkhau]').val();
            var xacnhan  = $('input[name=xacnhan]').val();
            if(matkhau!=xacnhan)
            {
                showMessage('Mật khẩu xác nhận không khớp!','danger');
                return false;
            }
            else{
                return true;
            }
        });
    }); 
</script>
