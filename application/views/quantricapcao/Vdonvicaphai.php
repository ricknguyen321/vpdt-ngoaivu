<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quản lý đơn vị cấp hai
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <form action="" method="get" autocomplete="off">
                        <div class="col-md-12">
                            <div class="form-group">
                                <select name="phongban" id="" class="form-control select2" style="width: 100%" onchange="this.form.submit()">
                                    <option value="0">-- Chọn phòng ban --</option>
                                    {if !empty($phongban)}
                                        {foreach $phongban as $pb}
                                            <option value="{$pb.PK_iMaPB}" {if !empty($maphong)}{($maphong==$pb.PK_iMaPB)?'selected':''}{/if}>{$pb.sTenPB}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                        </div>
                    </form>
                    <form action="" method="post" autocomplete="off">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center">STT</th>
                                        <th width="20%">Họ tên</th>
                                        <th width="25%">Quyền hạn</th>
                                        <th width="20%">Phòng ban</th>
                                        <th width="20%">Tên định danh</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {if !empty($dscanbo)}{$i=1}
                                        {foreach $dscanbo as $cb}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td><input type="text" class="hide" name="macb[]" value="{$cb.PK_iMaCB}">{$cb.sHoTen}</td>
                                                <td >
                                                    <select name="quyenhan[]" style="width: 100%" id="" class="form-control select2">
                                                        <option value="">-- Chọn quyền hạn --</option>
                                                        {if !empty($quyenhan)}
                                                            {foreach $quyenhan as $qh}
                                                                <option value="{$qh.PK_iMaCV}" {($cb.quyenhan_caphai==$qh.PK_iMaCV)?'selected':''}>{$qh.sTenCV}</option>
                                                            {/foreach}
                                                        {/if}
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="phongban[]" style="width: 100%" id="" class="form-control select2">
                                                        <option value="">-- Chọn phòng --</option>
                                                        {if !empty($phong)}
                                                            {foreach $phong as $p}
                                                                <option value="{$p.PK_iMaPB}" {($cb.phong_caphai==$p.PK_iMaPB)?'selected':''}>{$p.sTenPhong}</option>
                                                            {/foreach}
                                                        {/if}
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" name="tendinhdanh[]" value="{$cb.tendinhdanh}" placeholder="Tên định danh" class="form-control">
                                                </td>
                                            </tr>
                                        {/foreach}
                                    {/if}
                                </tbody>
                            </table>
                            {if !empty($dscanbo)}
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm pull-right">Lưu dữ liệu</button>
                            {/if}
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>