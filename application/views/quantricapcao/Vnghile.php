<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Nghỉ lễ
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="">
                        <form action="" method="post" autocomplete="off">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="">Ngày nghỉ <label class="required" for="">*</label></label>
                                    <input type="text" name="ngaynghi" class="form-control datepic datemask" required value="{($thongtin)?(date_select($thongtin[0]['sNgayNghi'])):NULL}">
                                </div>
                                <div class="form-group">
                                    <label for="">Mô tả </label>
                                    <textarea name="mota" id="" class="form-control " required cols="" rows="4">{($thongtin)?($thongtin[0]['sTenNgayNghi']):NULL}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Trạng thái</label>
                                    <label style="padding-left:25px">
                                        <input type="radio" name="trangthai" value="1" class="flat-red" checked>  Hiển thị
                                    </label>
                                    <label style="padding-left:25px">
                                        <input type="radio" name="trangthai" value="2" class="flat-red"  {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==2}checked{/if}{/if} >  Không hiển thị
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                                    {if !empty($thongtin)}<a href="{$url}chucvu" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                                </div>
                            </div>
                        </form>
                        <form action="" method="post">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-12"  style="overflow-x:auto;">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="5%" class="text-center">STT</th>
                                                    <th width="35%">Mô tả</th>
                                                    <th width="20%">Ngày nghỉ</th>
                                                    <th width="20%">Trạng thái</th>
                                                    <th width="10%" class="text-center">Xóa</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {if !empty($dsdulieu)}{$i=1}
                                                    {foreach $dsdulieu as $dl}
                                                        <tr>
                                                            <td class="text-center">{$i++}</td>
                                                            <td><a href="{$url}nghile?id={$dl.PK_iMaNgayNghi}">{$dl.sTenNgayNghi}</a></td>
                                                            <td>{date_select($dl.sNgayNghi)}</td>
                                                            <td><label for="" class="label {($dl.iTrangThai==1)?'label-success':'label-warning'}">{($dl.iTrangThai==1)?'Hiển thị':'Không hiển thị'}</label></td>
                                                            <td class="text-center"><button name="xoa" value="{$dl.PK_iMaNgayNghi}" type="submit" onclick="return confirm('Bạn có muốn xóa?')" class="btn btn-default btn-xs"><i class="fa fa-trash-o"></i></button></td>
                                                        </tr>
                                                    {/foreach}
                                                {/if}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>