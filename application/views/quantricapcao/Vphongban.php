<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quản lý phòng ban
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="{if empty($active)}active{else}''{/if}"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Danh sách phòng ban</a></li>
                        <li role="presentation" class="{if !empty($active)}active{else}''{/if}"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Cập nhật phòng ban</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane {if empty($active)}active{else}NULL{/if}" id="home">
                            <div class="row"><br/>
                                <div class="col-md-12"  style="overflow-x:auto;">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:5%" class="text-center">STT</th>
                                                <th style="width:20%">Tên phòng ban</th>
                                                <th style="width:10%">Phân hệ</th>
                                                <th style="width:10%">Viết tắt</th>
                                                <th style="width:20%">Phần mềm sử dụng</th>
                                                <th style="width:15%">Ngày nhập</th>
                                                <th style="width:10%">Trạng Thái</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {if !empty($dsdulieu)}{$i=1}
                                                {foreach $dsdulieu as $dl}
                                                    <tr>
                                                        <td>{$i++}</td>
                                                        <td><a href="{$url}phongban?id={$dl.PK_iMaPB}">{$dl.sTenPB}</a></td>
                                                        <td class="text-center"><label for="" class="label {($dl.iPhanHe==1)?'label-success':'label-warning'}">{($dl.iPhanHe==1)?'Có':'Không'}</label></td>
                                                        <td>{$dl.sVietTat}</td>
                                                        <td>{($dl.iLoaiPhanMem==1)?'Điều hành nội bộ':(($dl.iLoaiPhanMem==2)?'Truyền nhận văn bản':'Cả hai (ĐHNB+TNVB)')}</td>
                                                        <td>{($dl.sNgayNhap=='0000-00-00')?'':date_select($dl.sNgayNhap)}</td>
                                                        <td class="text-center"><label for="" class="label {($dl.iTrangThai==0)?'label-success':'label-warning'}">{($dl.iTrangThai==0)?'Hiển thị':'Không hiển thị'}</label></td>
                                                    </tr>
                                                {/foreach}
                                            {/if}
                                        </tbody>
                                    </table>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!--end danh sách đảng viên-->
                        
                        <div role="tabpanel" class="tab-pane {if !empty($active)}active{else}NULL{/if}" id="profile">
                            <form action=""  name="hocviencamtinhdang" method="post" accept-charset="utf-8" enctype="multipart/form-data" autocomplete="off">
                            <div class="row"><br/>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tên phòng ban *</label>
                                            <input type="text" name="ten" class="form-control" placeholder="Nhập tên phòng ban" required value="{($thongtin)?($thongtin[0]['sTenPB']):NULL}">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Sử dụng phân hệ</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="phanhe" value="1" class="flat-red" checked>  Có
                                                </label>
                                                <label style="padding-left:59px">
                                                    <input type="radio" name="phanhe" value="0" {if !empty($thongtin)} {if $thongtin[0]['iPhanHe']==0}checked{/if}{/if} class="flat-red">  Không
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Trạng thái</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="trangthai" value="0" class="flat-red" checked>  Hiển thị
                                                </label>
                                                <label style="padding-left:25px">
                                                    <input type="radio" name="trangthai" value="1" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==1}checked{/if}{/if} class="flat-red">  Không hiển thị
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Chi cục</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="chicuc" value="1" class="flat-red" checked>  Không
                                                </label>
                                                <label style="padding-left:32px">
                                                    <input type="radio" name="chicuc" value="2" {if !empty($thongtin)} {if $thongtin[0]['iChiCuc']==2}checked{/if}{/if} class="flat-red">  Phải
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tên viết tắt</label>
                                            <input type="text" name="tenviettat" class="form-control" placeholder="Nhập tên viết tắt" value="{($thongtin)?($thongtin[0]['sVietTat']):NULL}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Đường dẫn phân hệ</label>
                                            <input type="text" name="mota" class="form-control" placeholder="Nhập tên viết tắt" value="{($thongtin)?($thongtin[0]['sMoTa']):NULL}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" style="padding-left:18px;">
                                        <label class="col-sm-2">Loại phần mềm</label>
                                        <div class="col-sm-8" style="padding-left: 5px;">
                                            <label>
                                                <input type="radio" name="phanmem" value="1" class="flat-red" checked>  Điều hành nội bộ
                                            </label>
                                            <label style="padding-left:25px">
                                                <input type="radio" name="phanmem" value="2" {if !empty($thongtin)} {if $thongtin[0]['iLoaiPhanMem']==2}checked{/if}{/if} class="flat-red">  Truyền nhận văn bản
                                            </label>
                                            <label style="padding-left:25px">
                                                <input type="radio" name="phanmem" value="3" {if !empty($thongtin)} {if $thongtin[0]['iLoaiPhanMem']==3}checked{/if}{/if} class="flat-red">  Cả hai
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-8 col-md-4">
                                        <button class="btn btn-primary pull-right margin-l" name="luudulieu" value="luudulieu">{($active)?'Cập nhật':'Thêm mới'}</button>
                                        {if !empty($active)}<a href="{$url}phongban" class="btn btn-default pull-right ">Quay lại >></a>{/if}
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <!--end thông tin lý lịch đảng viên-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <script>
    $(document).ready(function() {
        $('label').css('text-align', 'left');
    });
</script>