<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/x-icon" href="{$url}assets/img/iconsongoaivu.jpg" />
    <!-- <link href="" rel="shortcut icon" type="image/x-icon" /> -->

    <title>{$title}</title>

    <base href="{$url}">
	
	<!-- Tu dong refresh lai trang sau 15p -->
	{if $trangchu == 1}
		<meta http-equiv="refresh" content="600">
	{/if}
    <meta name="author" content="Sở ngoại vụ">

    <meta name="copyright" content="Sở ngoại vụ">

    <meta name="description" content="Sở ngoại vụ Hà Nội - Thành Phố Hà Nội">

    <meta name="robots" content="noindex,nofollow">

    <!-- Tell the browser to be responsive to screen width -->

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.6 -->

    <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="{$url}assets/js/plugins/font-awesome/css/font-awesome.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="{$url}assets/css/AdminLTE.min.css">

    <!-- iCheck for checkboxes and radio inputs -->

    <link rel="stylesheet" href="{$url}assets/js/plugins/iCheck/all.css">

    <!-- bootstrap datepicker -->

    <link rel="stylesheet" href="{$url}assets/js/plugins/datepicker/datepicker3.css">

    <link rel="stylesheet" href="{$url}assets/js/plugins/timepicker/bootstrap-timepicker.min.css">

    <!-- Select2 -->

    <link rel="stylesheet" href="{$url}assets/js/plugins/select2/select2.min.css">

    <!-- DataTables -->

    <link rel="stylesheet" href="{$url}assets/js/plugins/datatables/dataTables.bootstrap.css">

    <!-- notify-->

    <link rel="stylesheet" href="{$url}assets/js/plugins/notify/jquery.pnotify.default.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins

         folder instead of downloading all of them to reduce the load. -->

    <link rel="stylesheet" href="{$url}assets/css/skins/_all-skins.min.css">

    <!-- Bootstrap WYSIHTML5 -->

    <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- jQuery 2.2.0 -->

    <!--end style.css-->

    <link rel="stylesheet" href="{$url}assets/css/style.css">

    <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script src="{$url}assets/js/vgcaplugin.js"></script>

    <!-- Bootstrap 3.3.6 -->

    <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="{$url}assets/js/plugins/select2/select2.full.min.js"></script>

    <!--end <script src="{$url}assets/js/plugins/slimScroll/jquery.slimscroll.min.js"></script> form-->

    <!-- bootstrap datepicker -->

    <script src="{$url}assets/js/plugins/datepicker/bootstrap-datepicker.js"></script>

    <script src="{$url}assets/js/plugins/timepicker/bootstrap-timepicker.js"></script>

    <!--jquery-validation-->

    <!-- AdminLTE App -->

    <script src="{$url}assets/js/app.min.js"></script>

    <!-- AdminLTE for demo purposes -->

    <script src="{$url}assets/js/demo.js"></script>

    <!-- Bootstrap WYSIHTML5 -->

    <script src="{$url}assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

    <!-- iCheck 1.0.1 -->

    <script src="{$url}assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- InputMask -->

    <script src="{$url}assets/js/plugins/input-mask/jquery.inputmask.js"></script>

    <script src="{$url}assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

    <script src="{$url}assets/js/plugins/input-mask/jquery.inputmask.extensions.js"></script>

    <!-- Select2 -->

    <!-- DataTables -->

    <script src="{$url}assets/js/plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="{$url}assets/js/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- Notify -->

    <script type="text/javascript" src="{$url}assets/js/plugins/notify/jquery.pnotify.min.js"></script>

    <script type="text/javascript" src="{$url}assets/js/plugins/notify/pnotify.custom.min.js"></script>


    <!-- autocomplete-->

    <link rel="stylesheet" href="{$url}assets/js/src/jquery.typeahead.css">

    <script src="{$url}assets/js/src/jquery.typeahead.js"></script>

    <script src="{$url}assets/js/extension.js"></script>

    <script src="{$url}assets/js/vanbanden/inspinia.js"></script>

    <script src="{$url}assets/js/vanbanden/slimscroll/jquery.slimscroll.min.js"></script>
	
	<script src="{$url}assets/js/plugins/angularjs_165.js"></script>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>-->

    <!-- {if empty($vanban['ios'])}

    <script src="{$url}assets/js/plugins/firebase_223.js"></script>

    {/if}

    <script src="{$url}assets/js/plugins/test.js"></script> -->

    <style type="text/css" media="print">

        @media print {

          a[href]:after {

            content: none !important;

          }

        }

    </style>

    <style>

        .select2-selection__choice{

            background-color: #3c8dbc!important;

            border-color: #367fa9!important;

            padding: 1px 10px!important;

            color: #fff!important;

            border-radius: 4px!important;

        }

        .treeview-menu>li.active>a {

            font-weight: normal!important;

            font-size: 13px!important;

            line-height: 13px!important;

        }

        .sidebar-menu .treeview-menu>li>a {

            font-weight: normal!important;

            font-size: 13px!important;

            line-height: 13px!important;

        }

        .tin{

            color:#152bc7!important;

        }

        .tin:hover{

            color: #f44336!important;

        }

        .tin1{

            color:#152bc7!important;

        }

        .tin1:hover{

            color: #f44336!important;

        }

        .box-body{

            min-height: 510px!important;

        }

        .deletepadding{

            padding: 0px;margin: -20px;margin-left: 0px;

        }

        .deletepadding1{

            padding: 5px;margin: -22px;margin-left: 15px;

        }

        .font{

            font-size:14px !important;

            padding-top: 4px;

        }

        .btn-default1{

            background-color: #e6e3e3;

            color: #444;

            border-color: #c1bfbf;

            font-weight: 600;

        }

        .btn-group-xs>.btn, .btn-xs {

            padding: 4px 5px;

            font-size: 12px;

            line-height: 1.5;

            border-radius: 3px;

        }

        .abc{

            margin-top: 22px;

        }

        .sl2{

            margin-top: -3px;

        }

        .h1no{

            margin-top: -10px;

            margin-bottom: -10px;

        }

        .h2no{

            margin-top: -10px;

            margin-bottom: -17px;

        }

        .sl3{

            margin-top: -1px;

        }

        .tieude{

            margin-top: 12px;padding-left:0px

        }

           

        .skin-blue-light .sidebar-menu>li.active>a {

            color: white;

    		background: #1373bf;
			
			text-shadow: 1px 1px 2px black;

        }

        .skin-blue-light .sidebar-menu>li:hover>a{

            /*color: #555;

            background: rgba(204, 204, 204, 0.5);*/

            color: #ffffff;

    		background: #ff6042;

        }

        .skin-blue-light .main-header .logo {

            background-color: #c00100;

            color: #fff;

            border-bottom: 0 solid transparent;

        }

        .kengang{

            /*width: 1120px;*/

            /*float: left;*/

            background: url(assets/img/i_17.jpg) repeat;

            height: 16px;

        }

        .main-sidebar, .left-side {

            position: absolute;

            top: 0;

            left: 0;

            padding-top: 0px;

            min-height: 100%;

            width: 230px;

            z-index: 810;

            -webkit-transition: -webkit-transform .3s ease-in-out,width .3s ease-in-out;

            -moz-transition: -moz-transform .3s ease-in-out,width .3s ease-in-out;

            -o-transition: -o-transform .3s ease-in-out,width .3s ease-in-out;

            transition: transform .3s ease-in-out,width .3s ease-in-out;

        }

        .skin-blue-light .main-header .logo:hover {

            background-color: #134c81;

        }

        .skin-blue-light .sidebar-menu>li>.treeview-menu {

            background: #FFF;

        }

        .phancach{

            border-bottom: 1px solid #e0e0e0 !important;

            padding-bottom: 15px !important;

        }

        .sidebar-menu>li>a {

            padding: 9px 5px 6px 15px;

            display: block;

        }

        .skin-blue-light .sidebar-menu>li.header {

            color: #848484;

            background: #ffffff !important;

        }

        .xxs{

            padding: 4px 5px;

            font-size: 12px;

            line-height: 1.0;

            border-radius: 3px;

        }

    </style>

    <script type="text/javascript">

        var url = $('base').attr('href');

        $(document).ready(function() {

            $('thead>tr').css('background-color','rgb(60, 141, 188)');

            $('thead>tr').css('color','#FFF');

            $('thead>tr>th').addClass('text-center');

        });

    </script>
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body class="hold-transition {if empty($vanban['ios'])}skin-blue-light sidebar-mini{/if} {if !empty($vanban['ios'])}sidebar-collapse{/if}">

<style>

	.snowflake {
		  color: #fff;
		  font-size: 1em;
		  font-family: Arial, sans-serif;
		  text-shadow: 0 0 2px #999;
		}

    .loader,

    .loader:after {

        /*border-radius: 50%;*/

        width: 17em;

        /*height: 10em;*/

    }

    .loader {

        /*margin: 785px;*/

        /*font-size: 5px;*/

        position: fixed;

        text-indent: -9999em;

        background-image: url("assets/img/ajax-loader.gif");

    }

    #loadingDiv {

        position:fixed;

        top:0;

        left:0;

        width:100%;

        height:100%;

        z-index: 9;

        background:#ffffff;

    }
    /* Các CSS này dành cho toàn bộ website và desktop */

 
/* Dành cho điện thoại */
@media all and (max-width: 480px) {
 
}
 
/* Dành cho máy tính bảng */
@media all and (max-width: 1024px) {
 
}

</style>

<script>

    $(document).ready(function () {

        $(document).on('click','.trangchu',function () {

//                    alert(1);

            $('body').append('<div style="" id="loadingDiv"><div class="loader col-sm-6 col-sm-offset-6" style="margin-top: 250px;">Đang tải dữ liệu</div></div>');

        });

        $(document).on('click','.tin',function () {

//                    alert(1);

            $('body').append('<div style="" id="loadingDiv"><div class="loader col-sm-6 col-sm-offset-6" style="margin-top: 250px;">Đang tải dữ liệu</div></div>');

        });

    });

</script>

<!-- Site wrapper -->

{if empty($vanban['ios'])}

<header style="position: relative;z-index: 1030;">

    {if $vanban['iLoaiPhanMem']!=2}

    <div class="row">
		<div>
		{if date('m') == 12}
			<img src="{$url}assets/img/banner-noel.jpg" alt="" style="width: 100%" width="100%">
		{else}
			<img src="{$url}assets/img/banner.jpg" alt="" style="width: 100%" width="100%">
		{/if}
		</div>
    </div>

    {else}

    <div class="row">

        <img src="{$url}assets/img/banner3.jpg" alt="" style="width: 100%" width="100%">

    </div>

    {/if}

    <div class="row kengang">

    </div>

</header>

<header class="main-header visible-xs">

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top">

        <!-- Sidebar toggle button-->

        <a href="{$url}{if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9}vanbanden{elseif $vanban['iQuyenHan_DHNB'] == 1}welcomead{elseif $vanban['iLoaiPhanMem']==2}thongtinmoi{else}welcome.html{/if}" class="sidebar-toggle" data-toggle="offcanvas" role="button">

            <span class="sr-only">Trang Chủ</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

        </a>

    </nav>

</header>

<div class="wrapper">

    <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-body" style="padding: 0px !important;">

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="panel panel-info" style="margin-bottom: 0px;!important;">

                                <div class="panel-heading text-center"><h5 style="font-weight: 600;font-size: 15px; margin-top: 5px;margin-bottom: 5px">THỐNG KÊ VĂN BẢN THEO LOẠI</h5></div>

                                <div class="panel-body">

                                    <div class="col-sm-4">

                                        <a target="_blank" href="{$url}dsvanbanden?cacloaivanban=3"><p style="color: black;"><i class="fa fa-pie-chart"></i> Văn bản SNV Chủ Trì</p></a>

										<a target="_blank" href="{$url}dsvanbanden?cacloaivanban=9"><p style="color: black;"><i class="fa fa-pie-chart"></i> Văn bản TTHC</p></a>

                                    </div>

                                    <div class="col-sm-4">

                                        <a target="_blank" href="{$url}dsvanbanden?cacloaivanban=6"><p style="color: black;"><i class="fa fa-pie-chart"></i> Văn bản TBKL</p></a>

                                        <a target="_blank" href="{$url}dsvanbanden?cacloaivanban=8"><p style="color: black;"><i class="fa fa-pie-chart"></i> Văn bản QPPL</p></a>										

                                    </div>
									
									<div class="col-sm-4">
									
										<a target="_blank" href="{$url}dsvanbanden?cacloaivanban=10"><p style="color: black;"><i class="fa fa-pie-chart"></i> Văn bản Đảng</p></a>

                                    </div>
									
									<div class="col-sm-4">
									
										<a target="_blank" href="{$url}dsvanbanden?domat=2"><p style="color: black;"><i class="fa fa-pie-chart"></i> Văn bản Mật</p></a>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <aside class="main-sidebar" style="background: #d9edf72b;">

        <!-- sidebar: style can be found in sidebar.less -->

        <section class="sidebar">

            <ul class="sidebar-menu">

                <li style="background: #1373bf; text-shadow: 1px 1px 2px black;"><a href="{$url}{if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9}vanbanden{elseif $vanban['iQuyenHan_DHNB'] == 1}welcomead{elseif $vanban['iLoaiPhanMem']==2}thongtinmoi{else}welcome.html{/if}" class="trangchu" style="color: #FFF;">

                        <i class="fa fa-home" aria-hidden="true"></i> <span>TRANG CHỦ</span>

                    </a></li>

				{if $vanban['iQuyenHan_DHNB'] == 1}
				
				<li class="treeview active">
                        <a href="{$url}hosophongban">
                            <i class="fa fa-file-zip-o"></i> <span>XEM HỒ SƠ CV</span>
                        </a>
                    </li>

                <li class="treeview active phancach">

                    <a href="javascript:void(0);">

                        <i class="fa fa-users"></i> <span>QUẢN TRỊ CẤP CAO</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">

                        <li><a class="tin" href="{$url}phongban"><i class="fa fa-circle-o"></i>Quản lý phòng ban</a></li>

                        <li><a class="tin" href="{$url}chucvu"><i class="fa fa-circle-o"></i>Quản lý chức vụ</a></li>

                        <li><a class="tin" href="{$url}canbo"><i class="fa fa-circle-o"></i>Quản lý thành viên</a></li>

                        <li><a class="tin" href="{$url}nghile"><i class="fa fa-circle-o"></i>Ngày nghỉ lễ</a></li>

                        <li><a class="tin" href="{$url}quanlythongbao"><i class="fa fa-circle-o"></i>Quản lý thông báo</a></li>

                        <li><a class="tin" href="{$url}donvicaphai"><i class="fa fa-circle-o"></i>Đơn vị cấp hai</a></li>

                    </ul>

                </li>

                {/if}

                {if $vanban['iQuyenHan_DHNB'] == 1}

                <li class="treeview active phancach">

                    <a href="javascript:void(0);">

                        <i class="fa fa-bars"></i> <span>THUỘC TÍNH VĂN BẢN</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">
						<li><a class="tin" href="{$url}donvi_ngoai"><i class="fa fa-circle-o"></i>Đơn vị ngoài thành phố</a></li>
                        <li><a class="tin" href="{$url}nguoiky"><i class="fa fa-circle-o"></i>Quản lý người ký</a></li>

                        <li><a class="tin" href="{$url}nguoikyliennganh"><i class="fa fa-circle-o"></i>Người ký liên ngành</a></li>

                        <li><a class="tin" href="{$url}chucvunguoiky"><i class="fa fa-circle-o"></i>Chức vụ người ký</a></li>

                        <li><a class="tin" href="{$url}domat"><i class="fa fa-circle-o"></i>Quản lý độ mật</a></li>

                        <li><a class="tin" href="{$url}dokhan"><i class="fa fa-circle-o"></i>Quản lý độ khẩn</a></li>

                        <li><a class="tin" href="{$url}noiguiden"><i class="fa fa-circle-o"></i>Quản lý nơi gửi đến</a></li>

                        <li><a class="tin" href="{$url}khuvuc"><i class="fa fa-circle-o"></i>Quản lý khu vực</a></li>

                        <li><a class="tin" href="{$url}linhvuc"><i class="fa fa-circle-o"></i>Quản lý lĩnh vực</a></li>

                        <li><a class="tin" href="{$url}loaivanban"><i class="fa fa-circle-o"></i>Quản lý loại văn bản</a></li>

                        <li><a class="tin" href="{$url}emaildonvi"><i class="fa fa-circle-o"></i>Email các đơn vị</a></li>

                        <li><a class="tin" href="{$url}songayxuly"><i class="fa fa-circle-o"></i>Quản lý số ngày quản lý</a></li>

                        <li><a class="tin" href="{$url}quytrinhiso"><i class="fa fa-circle-o"></i>Thời hạn quy trình ISO</a></li>

                        <li><a class="tin" href="{$url}linhvuc_qldv"><i class="fa fa-circle-o"></i>Lĩnh vực đầu việc</a></li>

                    </ul>

                </li>

                {/if}
				

				{if $vanban['PK_iMaCB'] == 615}  

                <li class="treeview">

                    <a href="{$url}mocham">

                        <i class="fa fa-calendar"></i> <span style="color: red">MỞ CHẤM ĐIỂM TUẦN</span>

                    </a>

                </li>



                <li class="treeview">

                    <a href="{$url}mochamthang">

                        <i class="fa fa-calendar"></i> <span style="color: red">MỞ CHẤM THÁNG</span>

                    </a>

                </li>

                <li class="treeview">

                    <a href="{$url}thongkebaocaothangxs">

                        <i class="fa fa-calendar"></i> <span style="color: red">PHÂN LOẠI CÔNG CHỨC SỞ</span>

                    </a>

                </li>

				{/if}
				
				<li class="treeview active">

                    <a href="javacript:void(0)"><i class="fa fa-calendar"></i> <span>CHỌN NĂM</span></a></li>

                <li class="header">

                    <form action="" method="post">

                        <select name="nam" style="width: 100%" id="" class="form-control select2" onchange="this.form.submit()">

                        {for $i=2020 to date('Y')}

                            <option value="{$i}" {($year==$i)?'selected':''}>Năm {$i}</option>

                        {/for}

                        </select>

                    </form>

                </li>
				
				{if $vanban['iLoaiPhanMem']!=2}

                <li class="treeview active phancach">

                    <a href="javascript:void(0);">

                        <i class="fa fa-newspaper-o"></i> <span>VĂN BẢN ĐẾN</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">

                        {if $vanban['iQuyenHan_DHNB'] == 9 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731 || $vanban['PK_iMaCB'] == 705}

                        <li class="menu2 active">

                            <a href="javascript:void(0);">

                                <i class="fa fa-circle-o"></i>

                                <span>Nhập văn bản</span>

                                <i class="fa fa-angle-left pull-right"></i>

                            </a>

                            <ul class="treeview-menu">

                                {if date('Y') == $year} 
									<li class="active"><a class="tin" href="{$url}vanbanden"><i class="fa fa-circle-o"></i> <b>Nhập mới VB đến</b></a></li>
									<li class="active"><a class="tin" href="{$url}nhapgiaymoi"><i class="fa fa-circle-o"></i> Nhập mới giấy mời</a></li>
								{/if}
								<li class="active"><a class="tin" href="{$url}readmail?trangthai=0"><i class="fa fa-circle-o"></i> <b>Email văn thư SNgV</b></a></li>
                                

								<!--<li class="active"><a class="tin" href="{$url}congtacdang"><i class="fa fa-circle-o"></i> Nhập mới VB Đảng</a></li>-->

								<li class="active"><a class="tin" href="{$url}readmaildang?trangthai=0"><i class="fa fa-circle-o"></i> <b>Email Chi ủy SNgV</b></a></li>

                            </ul>

                        </li>

                      
								<li class="active"><a class="tin" href="{$url}danhsachcongtacdang"><i class="fa fa-circle-o"></i> Văn bản Đảng</a></li>


                        <!--<li class="menu2 active">

                            <a href="javascript:void(0);">

                                <i class="fa fa-circle-o"></i>

                                <span>Báo cáo, lưu trữ, in ấn</span>

                                <i class="fa fa-angle-left pull-right"></i>

                            </a>

                            <ul class="treeview-menu">

                                <li class="active"><a target="_blank" href="{$url}lichhopso" class="tin1"><i class="fa fa-circle-o"></i> Lịch công tác tuần</a></li>

                                <li class="active"><a href="{$url}tonghop_moi" class="tin"><i class="fa fa-circle-o"></i> Báo cáo tổng hợp</a></li>

                            </ul>

                        </li>-->

                        {/if}

                        {if $vanban['iQuyenHan_DHNB'] == 3}

                        <li class="treeview">
						<li class="active"><a class="tin" href="{$url}readmail?trangthai=0"><i class="fa fa-circle-o"></i> Tạo VB đến từ email</a></li>
						<li class="active"><a class="tin" href="{$url}vanbanden"><i class="fa fa-circle-o"></i> Tạo VB đến tự nhập</a></li>
						 <li class="active"><a class="tin" href="{$url}nhapgiaymoi"><i class="fa fa-circle-o"></i> Nhập giấy mời</a></li>
						<!--<li class="active"><a class="tin" href="{$url}congtacdang"><i class="fa fa-circle-o"></i> Nhập công tác Đảng</a></li>-->

						<li class="active"><a class="tin" href="{$url}danhsachcongtacdang"><i class="fa fa-circle-o"></i> Văn bản Đảng</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden"><i class="fa fa-circle-o"></i> Danh sách tổng thể</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden?loaivanban=Giấy mời"><i class="fa fa-circle-o"></i> Danh sách giấy mời</a></li>

                        <li style="border-bottom: 1px solid #CCC; margin-bottom: 10px; padding-bottom: 5px"></li>
						
						<li><a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal2"><i class="fa fa-circle-o"></i> Danh sách VB theo loại</a></li>

                        <li><a class="tin" href="{$url}baocaodauviecphong"><i class="fa fa-circle-o"></i>Thống kê VB theo phòng</a></li>

                        <!--<li class="active"><a href="{$url}tonghop_moi" class="tin"><i class="fa fa-circle-o"></i> Báo cáo tổng hợp</a></li>-->

                        </li>

                        {/if}

                        {if $vanban['iQuyenHan_DHNB'] == 4}

                        <li class="treeview">

						<li class="active"><a class="tin" href="{$url}danhsachcongtacdang"><i class="fa fa-circle-o"></i> Văn bản Đảng</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden"><i class="fa fa-circle-o"></i> Danh sách tổng thể</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden?loaivanban=Giấy mời"><i class="fa fa-circle-o"></i> Danh sách giấy mời</a></li>

                        <li style="border-bottom: 1px solid #CCC; margin-bottom: 10px; padding-bottom: 5px"></li>
						
						<li><a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal2"><i class="fa fa-circle-o"></i> Danh sách VB theo loại</a>

                        <li><a class="tin" href="{$url}baocaodauviecphong"><i class="fa fa-circle-o"></i>Thống kê VB theo phòng</a></li>

                        <!--<li class="active"><a href="{$url}tonghop_moi" class="tin"><i class="fa fa-circle-o"></i> Báo cáo tổng hợp</a></li>-->

                        </li>

                        {/if}

                        {if $vanban['iQuyenHan_DHNB'] == 5}

                        <li class="treeview">

						<li class="active"><a class="tin" href="{$url}danhsachcongtacdang"><i class="fa fa-circle-o"></i> Văn bản Đảng</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden"><i class="fa fa-circle-o"></i> Danh sách tổng thể</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden?loaivanban=Giấy mời"><i class="fa fa-circle-o"></i> Danh sách giấy mời</a></li>

                        <li style="border-bottom: 1px solid #CCC; margin-bottom: 10px; padding-bottom: 5px"></li>

                        <!--{if $year!=2017}

                        <li><a class="tin" href="{$url}thongkevanbantheolanhdao"><i class="fa fa-circle-o"></i>Thống kê VB theo chỉ đạo</a></li> 

                        {/if}-->
						
						<li><a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal2"><i class="fa fa-circle-o"></i> Danh sách VB theo loại</a>

                        <li><a class="tin" href="{$url}baocaodauviecphong"><i class="fa fa-circle-o"></i>Thống kê VB theo phòng</a></li>

                        <!--<li class="active"><a href="{$url}tonghop_moi" class="tin"><i class="fa fa-circle-o"></i> Báo cáo tổng hợp</a></li>-->

                        </li>

                        {/if}

                        {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 10 || $vanban['iQuyenHan_DHNB'] == 11}



                        <li class="treeview">

                        <li><a class="tin" href="{$url}dsvanbanden"><i class="fa fa-circle-o"></i> Danh sách tổng thể</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden?loaivanban=Giấy mời"><i class="fa fa-circle-o"></i> Danh sách giấy mời</a></li>

                        <li style="border-bottom: 1px solid #CCC; margin-bottom: 10px; padding-bottom: 5px"><a class="tin" href="{$url}dsvanbanden?cacloaivanban=3"></li>
						
						<li><a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal2"><i class="fa fa-circle-o"></i> Danh sách VB theo loại</a>

                        <li><a class="tin" href="{$url}baocaodauviecphong"><i class="fa fa-circle-o"></i>Thống kê VB theo phòng</a></li>

                        <!--<li class="active"><a href="{$url}tonghop_moi" class="tin"><i class="fa fa-circle-o"></i> Báo cáo tổng hợp</a></li>-->


                        </li>

                        {/if}

                        {if $vanban['iQuyenHan_DHNB'] == 7}

                        <li class="treeview">

                        <li><a class="tin" href="{$url}dsvanbanden"><i class="fa fa-circle-o"></i> Danh sách tổng thể</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden?loaivanban=Giấy mời"><i class="fa fa-circle-o"></i> Danh sách giấy mời</a></li>

                        <li style="border-bottom: 1px solid #CCC; margin-bottom: 10px; padding-bottom: 5px"></li>

						<li><a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal2"><i class="fa fa-circle-o"></i> Danh sách VB theo loại</a>

                        <li><a class="tin" href="{$url}baocaodauviecphong"><i class="fa fa-circle-o"></i>Thống kê VB theo phòng</a></li>

                        <!--<li class="active"><a href="{$url}tonghop_moi" class="tin"><i class="fa fa-circle-o"></i> Báo cáo tổng hợp</a></li>-->

                        </li>

                        {/if}

                        {if $vanban['iQuyenHan_DHNB'] == 8}

                        <li class="treeview">
	
						{if $vanban['PK_iMaCB'] == 700}<li><a class="tin" href="{$url}readmaildang?trangthai=0"><i class="fa fa-circle-o"></i> <b>Email Chi ủy SNV</b></a></li>{/if}
                        <li><a class="tin" href="{$url}dsvanbanden"><i class="fa fa-circle-o"></i> Danh sách tổng thể</a></li>

                        <li><a class="tin" href="{$url}dsvanbanden?loaivanban=Giấy mời"><i class="fa fa-circle-o"></i> Danh sách giấy mời</a></li>

                        <li style="border-bottom: 1px solid #CCC; margin-bottom: 10px; padding-bottom: 5px"></li>
						
						<li><a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal2"><i class="fa fa-circle-o"></i> Danh sách VB theo loại</a>

                        <li><a class="tin" href="{$url}baocaodauviecphong"><i class="fa fa-circle-o"></i>Thống kê VB theo phòng</a></li>

                        <!--<li class="active"><a href="{$url}tonghop_moi" class="tin"><i class="fa fa-circle-o"></i> Báo cáo tổng hợp</a></li>-->


                        </li>

                        {/if}



                    </ul>

                </li>

                <li class="treeview active phancach {if  $vanban['PK_iMaCB']==1}hide{/if}">

                    <a href="javascript:void(0);">

                        <i class="fa fa-newspaper-o"></i> VĂN BẢN ĐI</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">
						{if date('Y') == $year}							
							<li class="active"><a class="tin" href="{$url}vanban"><i class="fa fa-circle-o"></i> Tạo văn bản đi</a><li>
						{/if}
						
						<li class="active"><a class="tin" href="{$url}dsvanbanchoso"><i class="fa fa-circle-o"></i> VB chưa cấp số</a></li>

                        <li class="active">
							{$slduyet = laysoluongdaduyet()}
							<a class="tin" href="{$url}dsvanbanchoph"><i class="fa fa-circle-o"></i> VB đã duyệt chờ phát hành ({$slduyet[0].count})</a>	
						</li>
						{if $vanban['PK_iMaCB'] == 705 || $vanban['PK_iMaCB'] == 693}
							{$today = date('Y-m-d', time())}
						<li class="active">
							<a class="tin" href="{$url}dsvanban?tungay={$today}&dengay={$today}"><i class="fa fa-circle-o"></i> VB đã phát hành trong ngày</a>	
						</li>
						{/if}
                        <li class="active"><a class="tin" href="{$url}dsvanban"><i class="fa fa-circle-o"></i> Danh sách văn bản</a><li>

                        <li class="active"><a class="tin" href="{$url}dsvanban?loaivanban=10"><i class="fa fa-circle-o"></i> Danh sách giấy mời</a><li>
						
						{if $vanban['PK_iMaCB'] == 731}
						<li class="active"><a class="tin" href="{$url}emaildonvi"><i class="fa fa-circle-o"></i> Quản lý email đơn vị</a><li>
							{/if}
                    </ul>



                </li>
				
				
				<li class="treeview active phancach {if  $vanban['PK_iMaCB']==1}hide{/if}">

                    <a href="javascript:void(0);">

                        <i class="fa fa-calendar"></i> LỊCH HỌP</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">
						{if $vanban['iQuyenHan_DHNB'] <= 7 || $vanban['PK_iMaCB'] == 705 || $vanban['PK_iMaCB'] == 699 || $vanban['PK_iMaCB'] == 700}
							{if date('Y') == $year}
								<li class="active"><a class="tin" href="{$url}nhaplichhop"><i class="fa fa-circle-o"></i> Tạo lịch họp</a><li>
							{/if}
						{/if}

                        <li class="active"><a class="tin" href="{$url}lichhopso"><i class="fa fa-circle-o"></i> Lịch họp</a></li>

                        <li class="active"><a class="tin" href="{$url}lichtuanlanhdao"><i class="fa fa-circle-o"></i> Lịch họp LĐ sở</a></li>

                    </ul>



                </li>
				

                {else}

                <li class="treeview active phancach">

                    <a href="javascript:void(0);">

                        <i class="fa fa-bars"></i> <span>TRUYỀN NHẬN VĂN BẢN</span>

                    </a>

                    <ul class="treeview-menu">

                        <li class="active"><a class="tin" style="font-size:16px!important" href="{$url}thongtinmoi"><i class="fa fa-circle-o"></i>Thông tin mới</a></li>

                        <li class="active"><a class="tin" style="font-size:16px!important" href="{$url}thongtinguidi"><i class="fa fa-circle-o"></i>Thông tin đã gửi đi</a></li>

                        <li class="active"><a class="tin" style="font-size:16px!important" href="{$url}thongtindaxem"><i class="fa fa-circle-o"></i>Thông tin đã xem</a></li>

                        <li class="active"><a class="tin" style="font-size:16px!important" href="{$url}soanthaothongtin"><i class="fa fa-circle-o"></i>Soạn thảo thông tin</a></li>

                    </ul>

                </li>

                {/if}
				
				<li class="treeview active phancach">

                    <a href="javascript:void(0);">

                        <i class="fa fa-calendar"></i> THÔNG TIN NỘI BỘ</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>
					{$ttmoi = demVanBanMoiNhan2($vanban['PK_iMaCB'])}
                    <ul class="treeview-menu">
						<li class="active"><a class="tin" href="{$url}thongtinmoi"><i class="fa fa-circle-o"></i> Thông tin mới ({$ttmoi})</a><li>
						<li class="active"><a class="tin" href="{$url}thongtinguidi"><i class="fa fa-circle-o"></i> Thông tin đã gửi</a><li>
						<li class="active"><a class="tin" href="{$url}thongtindaxem"><i class="fa fa-circle-o"></i> Thông tin đã xem</a><li>
						{if date('Y') == $year}
							<li class="active"><a class="tin" href="{$url}soanthaothongtin"><i class="fa fa-circle-o"></i> Soạn thảo thông tin</a><li>
						{/if}
                    </ul>

                </li>				
				
				<li class="treeview active phancach">

                    <a href="javascript:void(0);">

                        <i class="fa fa-calendar"></i> DS HỒ SƠ ABTC</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">
						<li class="active"><a class="tin" href="{$url}dshosoabtc"><i class="fa fa-circle-o"></i> Danh sách hồ sơ</a><li>
						{if $vanban['FK_iMaPhongHD'] == 76 && date('Y') == $year} 
							<li class="active"><a class="tin" href="{$url}nhaphosoabtc"><i class="fa fa-circle-o"></i> Nhập mới</a></li>
						{/if}
                    </ul>

                </li>
				
				<li class="treeview active phancach">
				
                    <a href="javascript:void(0);">
					
                        <i class="fa fa-calendar"></i> DS HỘI NGHỊ, HỘI THẢO</span> <i class="fa fa-angle-left pull-right"></i>
						
                    </a>

                    <ul class="treeview-menu">
						<li class="active"><a class="tin" href="{$url}dshnht"><i class="fa fa-circle-o"></i> Danh sách hội nghị, hội thảo</a><li>
						{if $vanban['FK_iMaPhongHD'] == 77 && date('Y') == $year} 
							<li class="active"><a class="tin" href="{$url}nhaphnht"><i class="fa fa-circle-o"></i> Nhập mới</a></li>
						{/if}
                    </ul>

                </li>
				
				<li class="treeview active phancach">
				
                    <a href="javascript:void(0);">
					
                        <i class="fa fa-calendar"></i> DANH SÁCH CUỘC TIẾP</span> <i class="fa fa-angle-left pull-right"></i>
						
                    </a>

                    <ul class="treeview-menu">
						<li class="active"><a class="tin" href="{$url}dscuoctiep"><i class="fa fa-circle-o"></i> Danh sách cuộc tiếp</a><li>
						{if $vanban['FK_iMaPhongHD'] == 77 && date('Y') == $year} 
							<li class="active"><a class="tin" href="{$url}nhapcuoctiep"><i class="fa fa-circle-o"></i> Nhập mới</a></li>
						{/if}
                    </ul>

                </li>
				
				{if $vanban['iLoaiPhanMem']!=2 && $year >=2019}

					{if $vanban['iQuyenHan_DHNB']<=6}
                    
                    <li class="treeview">
                        <a href="{$url}hoso">
                            <i class="fa fa-file-zip-o"></i> <span>HỒ SƠ CÔNG VIỆC</span>
                        </a>
                    </li>
                    {else}
                    <li class="treeview ">
                        <a href="{$url}hoso">
                            <i class="fa fa-folder-open-o"></i> <span>HỒ SƠ CÔNG VIỆC</span>
                        </a>
                    </li>
                    {/if}

                {/if}
                

                

                {if $vanban['PK_iMaCB'] == 705 || $vanban['PK_iMaCB'] == 696}

                <li class="treeview ">

                    <a href="javascript:void(0);">

                        <img src="{$url}assets/img/soft_icon.png" alt="" width="8%"> QUẢN LÝ THÔNG BÁO</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">

                        <li class="active"><a class="tin" href="{$url}quanlythongbao"><i class="fa fa-circle-o"></i> Quản lý thông báo</a><li>

                    </ul>

                </li>

                {/if}
				

                <li class="treeview">

                    <a href="{$url}xemhuongdansudung">

                        <i class="fa fa-file-word-o"></i> <span>HƯỚNG DẪN SỬ DỤNG</span>

                    </a>

                </li>

                <li class="treeview ">

                    <a target="_blank" href="http://10.11.209.105/vanthu/Login.aspx">

                        <i class="fa fa-search"></i> <span>TRA CỨU THÔNG TIN <br> VĂN BẢN TRƯỚC 04/2020</span>

                    </a>

                </li>
				
				<li class="treeview ">

                    <a target="_blank" href="https://motcua.hanoi.gov.vn">

                        <i class="fa fa-newspaper-o" ></i> <span>DỊCH VỤ CÔNG TT</span>

                    </a>

                </li>

				<li class="treeview">
                    <a href="{$url}hosocanbo">
                        <i class="fa fa-file-zip-o"></i> <span>HỒ SƠ CBCC</span>
                    </a>
                </li>
				
                

                {if $vanban['iLoaiPhanMem'] != 2}

                <li class="treeview {if $vanban['iQuyenHan_DHNB']==4 ||$vanban['iQuyenHan_DHNB']==5 || ($vanban['iQuyenHan_DHNB']==3 && $vanban['FK_iMaCV'] !=6)}hide{/if}">

                    <a href="javascript:void(0);">

                        <img src="{$url}assets/img/soft_icon.png" alt="" width="8%"> <span>QUẢN LÝ ĐẦU VIỆC</span> <i class="fa fa-angle-left pull-right"></i>

                    </a>

                    <ul class="treeview-menu">

                        <li class="active hide"><a class="tin" href="{$url}addqldv"><i class="fa fa-circle-o"></i>Nhập mới</a></li>

                        <li class="active"><a class="tin" href="{$url}dsdauviecvanban"><i class="fa fa-circle-o"></i>DS đầu việc của Sở</a></li>

                        <li class="active hide"><a class="tin" href="{$url}dsdauviectheophong"><i class="fa fa-circle-o"></i>DS đầu việc theo phòng</a></li>

                        <li class="active hide"><a class="tin" href="{$url}dsdauvieclinhvuc"><i class="fa fa-circle-o"></i>DS đầu việc theo lĩnh vực</a></li>

                        {if $vanban['iQuyenHan_DHNB'] == 3 && $vanban['FK_iMaCV'] == 5}

                        {else}

                        <li class="active hide"><a class="tin" href="{$url}dauviecchogiaiquyet"><i class="fa fa-circle-o"></i>DS đầu việc của phòng chủ trì</a></li>

                        <li class="active hide"><a class="tin" href="{$url}dauviecdagiaiquyet"><i class="fa fa-circle-o"></i>DS đầu việc của phòng phối hợp</a></li>

                        <li class="active hide"><a class="tin" href="{$url}vanbannguoiphoihop"><i class="fa fa-circle-o"></i>DS đầu việc người phối hợp</a></li>

                        <li class="active hide"><a class="tin" href="{$url}vanbanphongphoihop"><i class="fa fa-circle-o"></i>DS đầu việc phòng phối hợp</a></li>

                         <!-- <li><a class="tin" href="{$url}danhsachtailieu"><i class="fa fa-circle-o"></i>Danh sách tài liệu họp HĐND</a></li>

                        <li><a class="tin" href="{$url}danhsachkiennghi"><i class="fa fa-circle-o"></i>Danh sách kiến nghị HĐND</a></li>-->

                        {/if}

                    </ul>

                </li>

                {/if}

                <li class="treeview active phancach">

                    <a href="javascript:void(0);">

                        <img src="{$url}assets/img/version_check.png" alt="" width="8%"> THÔNG TIN CÁN BỘ</span>

                    </a>

                    <ul class="treeview-menu">

                        <li class="active"><a class="tin1" href="javascript:void(0);"><i class="fa fa-user"></i>{$vanban['sHoTen']}</a><li>
						
                        {if date('Y') == $year}
							<li class="active"><a class="tin1" href="{$url}doimatkhaucn"><i class="fa fa-lock"></i>Đổi mật khẩu</a><li>
						{/if}

                        <li class="active"><a class="tin1" href="{$url}dangxuat"><i class="fa fa-sign-in"></i>Đăng xuất</a><li>

                    </ul>

                </li>

            </ul>

            <p class="hidden" id="id_user1">{$vanban['iQuyenHan_DHNB']}</p>

            <p class="hidden" id="id_cb">{$vanban['PK_iMaCB']}</p>

        </section>

        <!-- /.sidebar -->

    </aside>

    <script>

        $(document).ready(function() {

            $('td').css('vertical-align', 'middle');

            $('th').css('vertical-align', 'middle');

            $('.control-label').css('text-align', 'left');

        });

    </script>

    <script type="text/javascript">

        var allownoti = document.getElementById('allownoti');



        // Thực hiện hành động bên trong khi nhấp vào Cho phép thông báo

        allownoti.addEventListener('click', function (e)

        {

            e.preventDefault();



            // Nếu trình duyệt không hỗ trợ thông báo

            if (!window.Notification)

            {

                alert('Trình duyệt của bạn không hỗ trợ chức năng này.');

            }

            // Ngược lại trình duyệt có hỗ trợ thông báo

            else

            {

                // Gửi lời mời cho phép thông báo

                Notification.requestPermission(function (p) {

                    // Nếu không cho phép

                    if (p === 'denied')

                    {

                        alert('Bạn đã không cho phép thông báo trên trình duyệt.');

                    }

                    // Ngược lại cho phép

                    else

                    {

                        alert('Bạn đã cho phép thông báo trên trình duyệt, hãy bắt đầu thử Hiển thị thông báo.');

                    }

                });

            }

        });

    </script>

{/if}