<style>
    #small-chat {
        position: fixed;
        bottom: 20px;
        right: 12px;
        z-index: 100;
    }
    #thongbao {
        position: fixed;
        bottom: 20px;
        right: 20px;
        z-index: 100;
    }
    .open-small-chat {
        height: 58px;
        width: 58px;
        display: block;
        background: #bd0200;
        padding: 9px 8px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
    }
    .open-small-chat1 {
        height: 38px;
        width: 38px;
        display: block;
        background: #3c8dbc;
        padding: 9px 8px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
    }
    .small-chat-box {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 75px;
        background: #fff;
        border: 1px solid #e7eaec;
        width: 250px;
        height: 320px;
        border-radius: 4px;
    }
    .small-chat-box .heading {
        background: #bd0200;
        padding: 8px 15px;
        font-weight: bold;
        color: #fff;
    }
    .small-chat-box .chat-date {
        opacity: 0.6;
        font-size: 10px;
        font-weight: normal;
    }
    .small-chat-box .form-chat {
        padding: 10px 10px;
    }
    .small-chat-box .content {
        padding: 15px 15px;
    }
    .small-chat-box.active {
        display: block;
    }
    .small-chat-box .content .author-name {
        font-weight: bold;
        margin-bottom: 3px;
        font-size: 11px;
    }
    .small-chat-box .content .chat-message.active {
        background: rgb(225, 248, 251);
        color: #333;
    }
    .small-chat-box .content .right {
        text-align: right;
        clear: both;
    }
    .small-chat-box .content .left .chat-message {
        float: left;
    }
    .small-chat-box .content .chat-message {
        padding: 5px 10px;
        border-radius: 6px;
        font-size: 11px;
        line-height: 14px;
        max-width: 80%;
        background: #f3f3f4;
        margin-bottom: 10px;
    }
    .small-chat-box .content .right .chat-message {
        float: right;
    }
    .small-chat-box .content .left {
        text-align: left;
        clear: both;
    }
    .small-chat-box .content > div {
        padding-bottom: 20px;
    }
    .badge1 {
        display: inline-block;
        min-width: 10px;
        padding: 3px 6px;
        font-size: 12px;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        background-color: #777;
        border-radius: 10px;
    }
    .label-warning1, .badge-warning1 {
        background-color: #f8ac59;
        color: #FFFFFF;
    }
    #small-chat .badge1 {
        position: absolute;
        top: 25px;
        right: -4px;
    }
    .popover{
        max-width: 100%!important;
    }
</style>
{if empty($vanban['ios'])}
<div class="small-chat-box fadeInRight animated" id="hien">

    <div class="heading" draggable="true">
        <small class="chat-date pull-right">
           <!--{date('d.m.Y',time())}-->
        </small>
        MENU
    </div>

    <div class="content">
        <div class="left">
<!--            <div class="author-name">-->
<!--                Admin MD-->
<!--                </small>-->
<!--            </div>-->
            <div id="thongbao1">
				{if $vanban['iQuyenHan_DHNB'] == 8}
					<a class="tin1" href="{$url}vanbanchoxuly_cv"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đến chờ xử lý </p></a>
					<a class="tin1" href="{$url}giaymoi_cv"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Giấy mời chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandichoxuly_cv"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandiusertao"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi cá nhân tạo </p></a>
				{/if}
				{if $vanban['iQuyenHan_DHNB'] == 7}
					<a class="tin1" href="{$url}vanbanchoxuly_pp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đến chờ xử lý </p></a>
					<a class="tin1" href="{$url}giaymoi_pp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Giấy mời chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandichoxuly_pp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandiusertao"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi cá nhân tạo </p></a>
				{/if}
				{if $vanban['iQuyenHan_DHNB'] == 6}
					<a class="tin1" href="{$url}vanbanchoxuly_tp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đến chờ xử lý </p></a>
					<a class="tin1" href="{$url}giaymoi_tp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Giấy mời chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandichoxuly_tp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandiusertao"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi cá nhân tạo </p></a>
				{/if}				
				{if $vanban['iQuyenHan_DHNB'] == 5}
					<a class="tin1" href="{$url}vanbanchoxuly_pgd"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đến chờ xử lý </p></a>
					<a class="tin1" href="{$url}giaymoi_pgd"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Giấy mời chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandichoxuly_ld"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi chờ xử lý </p></a>
				{/if}
				{if $vanban['iQuyenHan_DHNB'] == 4}
					<a class="tin1" href="{$url}vanbanchoxuly_gd"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đến chờ xử lý </p></a>
					<a class="tin1" href="{$url}giaymoi_gd"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Giấy mời chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandichoxuly_ld"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi chờ xử lý </p></a>
				{/if}
				{if $vanban['iQuyenHan_DHNB'] == 3}
					<a class="tin1" href="{$url}vanbanchoxuly"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i> Phân loại văn bản đến </p></a>
					<a class="tin1" href="{$url}giaymoi_cvp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Phân loại giấy mời </p></a><hr>
					
					<a class="tin1" href="{$url}vanbanchoxuly_tp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đến chờ xử lý </p></a>
					<a class="tin1" href="{$url}giaymoi_tp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Giấy mời chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandichoxuly_tp"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi chờ xử lý </p></a>
					<a class="tin1" href="{$url}vanbandiusertao"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản đi cá nhân tạo </p></a>
				{/if}
				<a class="tin1" href="{$url}dsvanbanquahan"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  DS văn bản đến quá hạn </p></a>
				<a class="tin1" href="{$url}dsvbsapdenhan"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  DS văn bản sắp đến hạn </p></a>
				<a class="tin1" href="{$url}dsfilechung"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Biểu mẫu, tài liệu </p></a>
				<a class="tin1" href="{$url}lichhopso"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Lịch họp </p></a>
				<a class="tin1" href="{$url}dangxuat"><p style="margin-bottom:3px"><i class="fa fa-caret-right" aria-hidden="true"></i>  Đăng xuất </p></a>
            </div>
            <script>
                notificationsLastQuery.on("child_added", function(snap) {
                    var notifier = snap.val();
                    var id_user1 = document.getElementById("id_user1").innerHTML;
                    var id_cb1 = document.getElementById("id_cb").innerHTML;
//            console.log(id_phong);
                    // Kiểm tra node đã hiển thi chưa
                    if(notifier.is_notification==false && notifier.id_cb==id_cb1){
                        $('#thongbao1').append('<div class="chat-message active" style="100%"><a href="'+notifier.base_url+'" style="color:black;">'+notifier.notification+'</a></div>');
                        var dem = parseInt($('#dem').text())+1;
                        $('#dem').html(dem);
                        //$('#hien').addClass('active');
                    }
                    if(notifier.is_notification==false && notifier.id_cb1==id_cb1){
                        $('#thongbao1').append('<div class="chat-message active" style="100%"><a href="'+notifier.base_url+'" style="color:black;">Có '+notifier.notification+' văn bản đến</a></div>');
                        var dem = parseInt($('#dem').text())+1;
                        $('#dem').html(dem);
                        //$('#hien').addClass('active');
                    }
                    if(notifier.is_notification==false && notifier.id_user==id_user1){
                        $('#thongbao1').append('<div class="chat-message active" style="100%"><a href="'+notifier.base_url+'" style="color:black;">Văn bản mới có số đến là: '+notifier.notification+'</a></div>');
                        var dem = parseInt($('#dem').text())+1;
                        $('#dem').html(dem);
                        //$('#hien').addClass('active');
                    }
                });
                $(document).on('click','#small-chat',function(){
                    $('#dem').html(0);
                });
            </script>
        </div>
    </div>
</div>
<div id="small-chat">
    
    <a class="open-small-chat" style="cursor:pointer">
	<span class="badge1 badge-warning1 pull-right" id="dem">MENU</span>
        <i class="fa fa-newspaper-o"></i>
    </a>
</div>

{if $vanban['iLoaiPhanMem'] != 2}
<div id="small-chat" style="    position: fixed;bottom: 20px;right: 82px;z-index: 100;" class="{if $vanban['iQuyenHan_DHNB'] ==9}hide{/if}">
    <a href="{$url}welcome.html" class="open-small-chat1" style="cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Về Trang Chủ">
        <i class="fa fa-home" style="font-size: 20px;"></i>
    </a>
</div>
<div id="small-chat" style="    position: fixed;bottom: 20px;right: 134px;z-index: 100;" class="{if $vanban['iQuyenHan_DHNB'] ==9}hide{/if}">
    <a href="javascript: history.back(1)" class="open-small-chat1" style="cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Quay lại">
        <i class="fa fa-reply" style="font-size: 20px;"></i>
    </a>
</div>
{/if}

 <footer class="main-footer">
    <span style="font-size: 13px !important;">Time: {date('H:i d-m-Y')} | Sở Ngoại vụ Hà Nội |  Địa chỉ: Số 10 Lê Lai - Hoàn Kiếm - Hà Nội  |  Điện thoại: 084-024-38250471 - Fax: 084-024-38253584  |  URL: <a target="_blank" href="https://dfa.hanoi.gov.vn/">https://dfa.hanoi.gov.vn</a>  </span>
  </footer>
{/if}
 
</div>
<!-- ./wrapper -->
<!-- SlimScroll -->
<script>
	$('a').tooltip();
	$('button').tooltip();	
</script>
{if !empty($content)}
<script type='text/javascript'>
    $(document).ready(function() {
        showMessage( "{$content['content']}" ,"{$content['class']}");
    });
</script>";
{/if}

{if date('m') == 12 && $trangchu == 1}
	<link rel="stylesheet" href="../vpdtsongoaivu/assets/css/snow.css">
	<div class="snowflakes" aria-hidden="true">
		<div class="snowflake">•</div>	  <div class="snowflake">●</div> 	  <div class="snowflake">•</div>	  <div class="snowflake">•</div>
		<div class="snowflake">•</div>	  <div class="snowflake">●</div>	  <div class="snowflake">•</div>	  <div class="snowflake">•</div>
		<div class="snowflake">•</div>	  <div class="snowflake">●</div>	  <div class="snowflake">•</div>	  <div class="snowflake">•</div>
	</div>
	
	{if $is_mobile == 0 && $music == 1}
		<script src='http://103.205.100.97/vpdtsongoaivu/etc/xmas.js'></script>
		<audio src="http://103.205.100.97/vpdtsongoaivu/etc/xmas.mp3" autoplay ></audio>
	{/if}
{/if}

</body>

</html>