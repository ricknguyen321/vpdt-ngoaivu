<script>

    $(document).ready(function() {

        $('p').css('font-size', '16px');

    });

</script>

<style>

    .padding-box{

        padding-top: 0px;

        padding-bottom: 0px;

    }

    .tin2{

        color:#232221 !important;

    }

    .tin2:hover{

        color: #f44336!important;

    }

</style>

<div class="content-wrapper" style="background: rgb(255, 255, 255);">

    <div class="row">

		<marquee onmouseover="this.stop();" onmouseout="this.start();" style="white-space: nowrap; overflow: hidden; overflow-x:-webkit-marquee; color:red;padding-top: 6px; font-size: 16px" behavior="scroll" scrollamount="4" direction="left">
		{$i=3}
			{foreach $thongbao as $tb}
				{if date('Y-m-d H:i:s') <= $tb.sNgayKetThuc && date('Y-m-d H:i:s') >= $tb.sNgayBatDau && $tb.iTips == 0}
					<a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModa{$i}"><b style="color:red">{$tb.sTieuDe}</b></a> | 
					<div class="modal fade bs-example-modal-lg" id="myModa{$i++}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">	
								<div class="panel panel-info">
                                    <div class="panel-heading" style="font-size: 16px;"><b style="color:red">Thông báo</b></div>
                                    <div class="panel-body show-noidung" style="font-size: 16px;">{$tb.sNoiDung}</div>
                                    <!--<div class="panel-footer text-center" style="cursor:pointer; width:10%"  data-dismiss="modal" data-dismiss="modal">Đóng</div>-->
                                </div>
							</div>
						</div>
					</div>		
				{/if}
			{/foreach}
		</marquee>		
	
	</div>

<section class="content" style="margin-top: -22px !important;">

    <div class="box-body11">

        <div class="box-body">

            <div class="row">

                <div class="col-md-12" style="display: inline-flex; min-width: 100%;">

                    <div class="col-md-6" style="min-width: 50%;padding-right: 0px;">

                        <div class="panel panel-info vanbanden" style="border-radius: 10px;">

                            <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;">

                                <img src="{$url}assets/img/icon-vb.png" alt="" style="width: 25px;    margin-top: -5px;"> <b style="font-size: 15px; text-shadow: 1px 1px 2px black;"> VĂN BẢN ĐẾN <a  class="pull-right tin" href="{$url}dsvanbanden" style="color: white !important;"><i class="fa fa-search"></i> Tìm kiếm</a></b>

                            </div>

                            <div class="panel-body" >

                            	

									{if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 10 || $vanban['iQuyenHan_DHNB'] == 11}

										{if $tongvbquahan > 0} 
											<a class="tin" href="{$url}dsvanbanquahan"><p ><i class="fa fa-newspaper-o"></i> <b style="color: red"> Danh sách văn bản quá hạn của phòng </b><b class="pull-right" style="color: red"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$tongvbquahan}</marquee></b></p></a>
										{else}
											<a class="tin" href="{$url}dsvanbanquahan"><p ><i class="fa fa-newspaper-o"></i> Danh sách văn bản quá hạn của phòng</p></a>
										{/if}

									{/if}

	   

									<!-- {if $vanban['iQuyenHan_DHNB'] == 6}   
									<a class="tin" href="{$url}tpchiaviec"><p><i class="fa fa-newspaper-o"></i> Danh sách công việc lãnh đạo Sở giao chờ xử lý<b class="pull-right">{$tonglanhdaogiao}</b></p></a>
									
									<a class="tin" href="{$url}tpphchiaviec"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng phối hợp chờ xử lý<b class="pull-right">{$tongphlanhdaogiao}</b></p></a>
									{/if} -->

									{if $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 8}
										{if $tongvbquahan > 0}
											<a class="tin" href="{$url}dsvanbanquahan"><p ><i class="fa fa-newspaper-o"></i> <b style="color:red">Văn bản quá hạn</b><b class="pull-right" style="color: red"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$tongvbquahan}</marquee></b></p></a>
										{else}
											<a class="tin" href="{$url}dsvanbanquahan"><p ><i class="fa fa-newspaper-o"></i> Văn bản quá hạn</p></a>
										{/if}

									{/if}                                

									{if $vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5}
									
										{if $tongvbquahan > 0}
											<a class="tin" href="{$url}dsvanbanquahan"><p style="color: red"><i class="fa fa-newspaper-o"></i> <b>Danh sách văn bản quá hạn</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$tongvbquahan}</marquee></b></p></a>
										{else}
											<a class="tin" href="{$url}dsvanbanquahan"><p style=""><i class="fa fa-newspaper-o"></i> Danh sách văn bản quá hạn</p></a>
										{/if}

									{/if}
									
									
									{if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 10 || $vanban['iQuyenHan_DHNB'] == 11}

										{if $tongvbsapdenhan > 0} 
											<a class="tin" href="{$url}dsvbsapdenhan"><p ><i class="fa fa-newspaper-o"></i> <span style="color: orange"> Danh sách văn bản sắp đến hạn của phòng: </span><span class="pull-right" style="color: orange">{$tongvbsapdenhan}</span></p></a>
										{else}
											<a class="tin" href="{$url}dsvbsapdenhan"><p ><i class="fa fa-newspaper-o"></i> Danh sách văn bản sắp đến hạn của phòng</p></a>
										{/if}

									{/if}

	   

									<!-- {if $vanban['iQuyenHan_DHNB'] == 6}   
									<a class="tin" href="{$url}tpchiaviec"><p><i class="fa fa-newspaper-o"></i> Danh sách công việc lãnh đạo Sở giao chờ xử lý<b class="pull-right">{$tonglanhdaogiao}</b></p></a>
									
									<a class="tin" href="{$url}tpphchiaviec"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng phối hợp chờ xử lý<b class="pull-right">{$tongphlanhdaogiao}</b></p></a>
									{/if} -->

									{if $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 8}
										{if $tongvbsapdenhan > 0}
											<a class="tin" href="{$url}dsvbsapdenhan"><p ><i class="fa fa-newspaper-o"></i> <span style="color:orange">Văn bản sắp đến hạn:</span><span class="pull-right" style="color: orange">{$tongvbsapdenhan}</span></p></a>
										{else}
											<a class="tin" href="{$url}dsvbsapdenhan"><p ><i class="fa fa-newspaper-o"></i> Văn bản sắp đến hạn</p></a>
										{/if}

									{/if}                                

									{if $vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5}
									
										{if $tongvbsapdenhan > 0}
											<a class="tin" href="{$url}dsvbsapdenhan"><p style="color: orange"><i class="fa fa-newspaper-o"></i> Danh sách văn bản sắp đến hạn:<span class="pull-right">{$tongvbsapdenhan}</span></p></a>
										{else}
											<a class="tin" href="{$url}dsvbsapdenhan"><p style=""><i class="fa fa-newspaper-o"></i> Danh sách văn bản sắp đến hạn</p></a>
										{/if}

									{/if}

                                

                                {if $vanban['iQuyenHan_DHNB'] == 3}
                                <!--nhập mới lãnh đạo giao-->
                                {if $vanban['PK_iMaCB'] == 617}
                                    <a class="tin" href="{$url}addlanhdaogiao"><p><h4 style="font-weight: 600"><i class="fa fa-sitemap" aria-hidden="true"></i> Nhập mới kế hoạch lãnh đạo giao</h4></p></a>

                                    <a class="tin" href="{$url}chitietcvlanhdaogiao"><p><h4 style="font-weight: 600"><i class="fa fa-sitemap" aria-hidden="true"></i> Danh sách kế hoạch lãnh đạo giao</h4></p></a>
                                {/if}
                                <!--kết thúc nhập mới lãnh đạo giao-->

								<u><p><center><b>QUYỀN CHÁNH VĂN PHÒNG</b></center></p></u>
								<p>
								{if $plvb > 0}
								 <b><a class="tin" href="{$url}vanbanchoxuly"><i class="fa fa-sitemap" aria-hidden="true"></i> Phân loại văn bản <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$plvb}</marquee></b></a></b>
								{else}
								<a class="tin" href="{$url}vanbanchoxuly"><i class="fa fa-sitemap" aria-hidden="true"></i> Phân loại văn bản</a>
								{/if}
								</p>
								
								<p>
								{if $plvbctd > 0}
								 <b><a class="tin" href="{$url}congtacdang_cvp"><i class="fa fa-sitemap" aria-hidden="true"></i> Phân loại văn bản công tác Đảng <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$plvbctd}</marquee></b></a></b>
								{else}
								<a class="tin" href="{$url}congtacdang_cvp"><i class="fa fa-sitemap" aria-hidden="true"></i> Phân loại văn bản công tác Đảng</a>
								{/if}
								</p>

                                <a class="tin" href="{$url}vanbandaphanloai"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Văn bản đã phân loại<!-- <span class="pull-right">{$vbdpl}</span>--></p></a>
								
								<p>
								{if $vbcl > 0}
								 <b><a class="tin" href="{$url}vanbanchuyenlai"><i class="fa fa-sitemap" aria-hidden="true"></i> Văn bản các phòng chuyển lại <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcl}</marquee></b></a></b>
								{else}
								<a class="tin" href="{$url}vanbanchuyenlai"><i class="fa fa-sitemap" aria-hidden="true"></i> Văn bản các phòng chuyển lại</a>
								{/if}
								</p>

                                <hr style="border: 2px solid lightblue;">
								 <u><p><center><b>QUYỀN TRƯỞNG PHÒNG</b></center></p></u>
								
								{if $tongvbquahan > 0} 
									<a class="tin" href="{$url}dsvanbanquahan"><p ><i class="fa fa-newspaper-o"></i> <b style="color: red"> Danh sách văn bản quá hạn của phòng </b><b class="pull-right" style="color: red"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$tongvbquahan}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}dsvanbanquahan"><p ><i class="fa fa-newspaper-o"></i> Danh sách văn bản quá hạn của phòng</p></a>
								{/if}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="tin1">
											<p>										
											{if $pct > 0 || $tocongtac > 0}
												<i class="fa fa-newspaper-o"></i> <b>Văn bản giao phòng chờ xử lý</b> <b class="pull-right">
													<marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$pct + $tocongtac + $pph + $congtacdang}</marquee></b>
											{else}
												{if $pct + $tocongtac + $pph + $congtacdang > 0}
													<i class="fa fa-newspaper-o"></i> Văn bản giao phòng chờ xử lý <span class="pull-right">
													{$pct + $tocongtac + $pph + $congtacdang}</span>
												{else}
													<i class="fa fa-newspaper-o"></i> Văn bản giao phòng chờ xử lý 
												{/if}
											{/if}
											</p>
                                        </a>

                                    </h4>

                                    <div id="collapseOne" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanchoxuly_tp"><p style="{if $pct > 0}font-weight:bold{/if}"><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì chờ xử lý <span class="pull-right">
											 {$pct}</span>
											</p></a>

                                            {if !empty($congtacdang)}<a class="tin2" href="{$url}congtacdang_tp"><p><i class="fa fa-caret-right"></i> VB công tác Đảng chờ xử lý <b class="pull-right">{$congtacdang}</b></p></a>{/if}

                                            {if !empty($tocongtac)}<a class="tin2" href="{$url}vanbanchoxuly_tct"><p><i class="fa fa-caret-right"></i> Văn bản TTHC chờ xử lý <b class="pull-right">{$tocongtac}</b></p></a>{/if}

                                            <a class="tin2" href="{$url}phongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chờ phối hợp <span class="pull-right">{if !empty($pph)}{$pph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dschidao > $chidaodaxem}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidao - $chidaodaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
											{/if}

                                        </h4>

								</div>
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dsykien > $ykiendaxem}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykien - $ykiendaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
											{/if}

                                        </h4>

								</div>

                                <!--<div id="accordion">

								    <h4 class="panel-title">

								        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" class="tin1">

								            <p style=""><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao <span class="pull-right">{$tonglanhdaogiao+$tongphlanhdaogiao}</span></p>

								        </a>

								    </h4>

								    <div id="collapse8" class="panel-collapse collapse">

								        <div class="panel-body padding-box">
								            <a class="tin2" href="{$url}tpchiaviec"><p><i class="fa fa-newspaper-o"></i> Danh sách công việc lãnh đạo Sở giao phòng<span class="pull-right">{$tonglanhdaogiao}</span></p></a>

								            <a class="tin2" href="{$url}tpphchiaviec"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng phối hợp<span class="pull-right">{$tongphlanhdaogiao}</span></p></a>

                                            <a class="tin2" href="{$url}dsphongdangthuchien"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng đang thực hiện<span class="pull-right"></span></p></a>

                                            <a class="tin2" href="{$url}tpphdangthuchien"><p><i class="fa fa-newspaper-o"></i> Công việc phối hợp lãnh đạo Sở giao phòng đang thực hiện<span class="pull-right"></span></p></a>
								        </div>

								    </div>

								</div>-->

                                <div id="accordion">

                                    <h4 class="panel-title">
									
										<a class="tin1" href="{$url}vanbandachidao"><p><i class="fa fa-newspaper-o" aria-hidden="true"></i> Văn bản giao phòng đã xử lý <!--<span class="pull-right">{$vanbanchidao}</span>--></p></a>

                                       <!-- <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> Văn bản giao phòng đã chỉ đạo <span class="pull-right">{$vanbanchidao + $vanbandaxuly_stcph + $phdxl}</span></p>

                                        </a>-->

                                    </h4>

                                    <!--<div id="collapse6" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbandachidao"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì đã chỉ đạo <span class="pull-right">{$vanbanchidao}</span></p></a>

                                            <a class="tin2" href="{$url}vanbandaxulystcph_tp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản SNV phối hợp đã chỉ đạo <span class="pull-right">{if !empty($vanbandaxuly_stcph)}{$vanbandaxuly_stcph}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}vanbanphoihopdachidao"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp đã chỉ đạo <span class="pull-right">{if !empty($phdxl)}{$phdxl}{else}{/if}</span></p></a>

                                        </div>

                                    </div>-->

                                </div>

								{if $vbcduyet > 0}
									<b><a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcduyet}</marquee></b></p></a></b>
								{else}
									<a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt</p></a>
								{/if}
								
								
								{if $dsgiahan > 0}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> <b>Duyệt văn bản gia hạn giải quyết </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsgiahan}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> Duyệt văn bản gia hạn giải quyết</p></a>
								{/if}

                                <a class="tin" href="{$url}phoihopchuyenlai_tp"><p><i class="fa fa-reply"></i> Văn bản phối hợp trả lại <span class="pull-right">{if !empty($tuchoipp)}{$tuchoipp}{else}{/if}</span></p></a>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Văn bản quan trọng <!--<b class="pull-right">{$vbqt + $vbqtdgq}</b>--></p>

                                        </a>

                                    </h4>

                                    <div id="collapseTwo" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanquantrong"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Chưa hoàn thành<!-- <b class="pull-right">{$vbqt}</b>--></p></a>

                                            <a class="tin2" href="{$url}vanbanquantrong?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đã hoàn thành<!-- <span class="pull-right">{$vbqtdgq}</span>--></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Danh sách văn bản đến của phòng <span class="pull-right"></span></p>

                                        </a>

                                    </h4>

                                    <div id="collapseThree" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dsphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì<span class="pull-right">{$dschutrii}</span></p></a>

                                            <a class="tin2" href="{$url}dsphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp<span class="pull-right">{$dsphoihopp}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                {if !empty($dsdonthu) || !empty($dsdonthuht) }

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số lượng đơn thư tố cáo giao phòng xử lý <b class="pull-right">{$dsdonthu + $dsdonthuht}</b></p>

                                        </a>

                                    </h4>

                                    <div id="collapseFour" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            {if !empty($dsdonthu)}<a class="tin2" href="{$url}dsdonthu"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  Đơn thư KNTC chưa hoàn thành <b class="pull-right">{$dsdonthu}</b></p></a>{/if}

                                            {if !empty($dsdonthuht)}<a class="tin2" href="{$url}dsdonthu?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng Đơn thư KNTC hoàn thành <b class="pull-right">{$dsdonthuht}</b></p></a>{/if}

                                        </div>

                                    </div>

                                </div>

                                {/if}

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 4}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="tin1">

											{if $vbcxlcvp > 0}
												 <b><p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcxlcvp + $plvbctd}</marquee></b></p></b>
											{else}
												<p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý <span class="pull-right">
												{($vbcxlcvp + $plvbctd > 0) ? $vbcxlcvp + $plvbctd : ''}</span></p>
											{/if}

                                        </a>

                                    </h4>

                                    <div id="collapseOne" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanchoxuly_gd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản đến chờ xử lý <span class="pull-right">{$vbcxlcvp}</span></p></a>

                                            <a class="tin2" href="{$url}congtacdang_gd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản Đảng chờ xử lý <span class="pull-right">{if !empty($plvbctd)}{$plvbctd}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>



                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseTow" class="tin1">-->
										<a class="tin1" href="{$url}vanbandaxuly_gd">
                                            <p><i class="fa fa-newspaper-o"></i> Văn bản đến đã xử lý</p>

                                        </a>

                                    </h4>

                                    <!--<div id="collapseTow" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbandaxuly_gd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản Lãnh đạo đã chỉ đạo (chưa giải quyết) <span class="pull-right">{$vanbandaxuly}</span></p></a>

                                            <a class="tin2" href="{$url}vanbandaxulystcph_gd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản SNV phối hợp đã chỉ đạo <span class="pull-right">{if !empty($vanbandaxuly_stcph)}{$vanbandaxuly_stcph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>-->

                                </div>
								
								{if $dsgiahan > 0}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> <b>Duyệt văn bản gia hạn giải quyết </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsgiahan}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> Duyệt văn bản gia hạn giải quyết</p></a>
								{/if}
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dschidao > $chidaodaxem}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidao - $chidaodaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
											{/if}

                                        </h4>

								</div>
								
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dsykien > $ykiendaxem}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykien - $ykiendaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
											{/if}

                                        </h4>

								</div>
								
								<div id="accordion">

                                        <h4 class="panel-title">
											<a class="tin" href="{$url}dsvanbanden?ideadline=1"><p><i class="fa fa-list-alt"></i> Danh sách văn bản có thời hạn <span class="pull-right"><b></b></span></p></a>

                                        </h4>

								</div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Danh sách văn bản TTHC</p>

                                        </a>

                                    </h4>

                                    <div id="collapseFive" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dstocongtac"><p><i class="fa fa-caret-right" aria-hidden="true"></i> TTHC chưa xử lý</b></p></a>

                                            <a class="tin2" href="{$url}dstocongtac?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  TTHC đã xử lý <b class="pull-right"></b></p></a>

                                        </div>

                                    </div>

                                </div>


                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="tin1">
										{if $vbqt > 0}
                                            <p ><i class="fa fa-list-alt"></i> <b> Văn bản quan trọng </b><!--<b class="pull-right">{$vbqt + $vbqtdgq}</b>--></p>
										{else}
											<p style=""><i class="fa fa-list-alt"></i> Văn bản quan trọng <!--<span class="pull-right">{$vbqt + $vbqtdgq}</span>--></p>
										{/if}

                                        </a>

                                    </h4>

                                    <div id="collapseTwo" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanquantrong"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Chưa hoàn thành<!-- <b class="pull-right">{$vbqt}</b>--></p></a>

                                            <a class="tin2" href="{$url}vanbanquantrong?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đã hoàn thành<!-- <span class="pull-right">{$vbqtdgq}</span>--></p></a>

                                        </div>

                                    </div>

                                </div>



                                <a class="tin" href="{$url}baocaodauviecphong"><p><i class="fa fa-list-alt" aria-hidden="true"></i>  Danh sách văn bản theo phòng <b class="pull-right"></b></p></a>

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 5}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="tin1">
										
											{if $vbcxlcvp + $getDocAwaitPH > 0}
												 <b><p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcxlcvp + $plvbctd+$getDocAwaitPH}</marquee></b></p></b>
											{else}
												<p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý <span class="pull-right">
												{($vbcxlcvp + $plvbctd+$getDocAwaitPH > 0) ? $vbcxlcvp + $plvbctd+$getDocAwaitPH : ''}</span></p>
											{/if}


                                        </a>

                                    </h4>

                                    <div id="collapseOne" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanchoxuly_pgd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản đến chờ xử lý <b class="pull-right">{$vbcxlcvp+$getDocAwaitPH}</b></p></a>

                                            <a class="tin2" href="{$url}congtacdang_pgd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản Đảng chờ xử lý <b class="pull-right">{if !empty($plvbctd)}{$plvbctd}{else}{/if}</b></p></a>



                                        </div>

                                    </div>

                                </div>



                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseTow" class="tin1">-->
										<a class="tin1" href="{$url}vanbandaxuly_gd">
                                            <p><i class="fa fa-newspaper-o"></i> Văn bản đến đã xử lý <!--<span class="pull-right">{$vanbandaxuly}</span>--></p>

                                        </a>

                                    </h4>

                                    <div id="collapseTow" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbandaxuly_gd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản Lãnh đạo đã chỉ đạo (chưa gq) <b class="pull-right">{$vanbandaxuly}</b></p></a>

                                            <a class="tin2" href="{$url}vanbandaxulystcph_gd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản SNV phối hợp đã chỉ đạo <b class="pull-right">{if !empty($vanbandaxuly_stcph)}{$vanbandaxuly_stcph}{else}{/if}</b></p></a>

                                            <!--<a class="tin2" href="{$url}dsvb_bgd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản xem để biết <b class="pull-right">{if !empty($getDocSeenBGD)}{$getDocSeenBGD}{else}{/if}</b></p></a>-->

                                        </div>

                                    </div>

                                </div>
								
								{if $theodoivanban > 0}
									<a class="tin" href="{$url}vanbantheodoi"><p><i class="fa fa-newspaper-o"></i> <b> Văn bản GĐ giao chỉ đạo, đôn đốc </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$theodoivanban}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}vanbantheodoi"><p><i class="fa fa-newspaper-o"></i> Văn bản GĐ giao chỉ đạo, đôn đốc</p></a>
								{/if}                               
								
								{if $dsgiahan > 0}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> <b>Duyệt văn bản gia hạn giải quyết </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsgiahan}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> Duyệt văn bản gia hạn giải quyết</p></a>
								{/if}
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dschidao > $chidaodaxem}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidao - $chidaodaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
											{/if}

                                        </h4>

								</div>
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dsykien > $ykiendaxem}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykien - $ykiendaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
											{/if}
                                        </h4>

								</div>

                                <!--<div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số lượng TTHC giao phòng xử lý <span class="pull-right">{$dstocongtac + $dstocongtac_ht}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapseFive" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            {if !empty($dstocongtac)}<a class="tin2" href="{$url}dstocongtac"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  TTHC chưa hoàn thành <span class="pull-right">{$dstocongtac}</span></p></a>{/if}

                                            {if !empty($dstocongtac_ht)}<a class="tin2" href="{$url}dstocongtac?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng TTHC hoàn thành <span class="pull-right">{$dstocongtac_ht}</span></p></a>{/if}

                                        </div>

                                    </div>

                                </div>-->

                                {if !empty($dsdonthu) || !empty($dsdonthuht) }

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số lượng Đơn thư KNTC giao phòng xử lý <b class="pull-right">{$dsdonthu + $dsdonthuht}</b></p>

                                        </a>

                                    </h4>

                                    <div id="collapseFour" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            {if !empty($dsdonthu)}<a class="tin2" href="{$url}dsdonthu"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  Đơn thư KNTC chưa hoàn thành <b class="pull-right">{$dsdonthu}</b></p></a>{/if}

                                            {if !empty($dsdonthuht)}<a class="tin2" href="{$url}dsdonthu?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng Đơn thư KNTC hoàn thành <b class="pull-right">{$dsdonthuht}</b></p></a>{/if}

                                        </div>

                                    </div>

                                </div>

                                {/if}



                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="tin1">
										{if $vbqt > 0}
                                            <p><i class="fa fa-list-alt"></i> <b>Văn bản quan trọng </b><!--<b class="pull-right">{$vbqt + $vbqtdgq}</b>--></p>
										{else}
											<p style=""><i class="fa fa-list-alt"></i> Văn bản quan trọng <!--<span class="pull-right">{$vbqt + $vbqtdgq}</span>--></p>
										{/if}

                                        </a>

                                    </h4>

                                    <div id="collapseTwo" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanquantrong"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Chưa hoàn thành</p></a>

                                            <a class="tin2" href="{$url}vanbanquantrong?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đã hoàn thành</p></a>

                                        </div>

                                    </div>

                                </div>

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] != 15}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="tin1">
											
											<p>										
											{if $vbcxlcvp > 0 || $tocongtac > 0}
												<i class="fa fa-newspaper-o"></i> <b>Văn bản giao phòng chờ xử lý</b> <b class="pull-right">
													<marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcxlcvp + $congtacdang + $tocongtac +$pph}</marquee></b>
											{else}
												<i class="fa fa-newspaper-o"></i> Văn bản giao phòng chờ xử lý <span class="pull-right">
												{($vbcxlcvp + $congtacdang + $tocongtac +$pph >0) ? $vbcxlcvp + $congtacdang + $tocongtac +$pph : ''}</span>
											{/if}
											</p>

                                        </a>

                                    </h4>

                                    <div id="collapseOne" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                             <a class="tin2" href="{$url}vanbanchoxuly_tp"><p style="{if $vbcxlcvp > 0}font-weight:bold{/if}"><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản đến chờ xử lý <span class="pull-right">
											 {$vbcxlcvp}</span></p></a>

                                            {if !empty($congtacdang)}<a class="tin2" href="{$url}congtacdang_tp"><p><i class="fa fa-caret-right"></i> VB công tác Đảng chờ xử lý <span class="pull-right">{$congtacdang}</span></p></a>{/if}

                                            <a class="tin2" href="{$url}vanbanchoxuly_tct"><p style="{if $tocongtac > 0}font-weight:bold{/if}"><i class="fa fa-caret-right"></i> Văn bản TTHC chờ xử lý <span class="pull-right">{$tocongtac}</span></p></a>

                                            <a class="tin2" href="{$url}phongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chờ phối hợp <span class="pull-right">{if !empty($pph)}{$pph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                            {if $dschidao > $chidaodaxem}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidao - $chidaodaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
											{/if}

                                        </h4>

								</div>
								
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dsykien > $ykiendaxem}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykien - $ykiendaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
											{/if}

                                        </h4>

								</div>

                                <!--<div id="accordion">

								    <h4 class="panel-title">

								        <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" class="tin1">

								            <p style=""><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao <span class="pull-right">{$tonglanhdaogiao+$tongphlanhdaogiao}</span></p>

								        </a>

								    </h4>

								    <div id="collapse9" class="panel-collapse collapse">

								        <div class="panel-body padding-box">
								            <a class="tin2" href="{$url}tpchiaviec"><p><i class="fa fa-newspaper-o"></i> Danh sách công việc lãnh đạo Sở giao phòng<span class="pull-right">{$tonglanhdaogiao}</span></p></a>

								            <a class="tin2" href="{$url}tpphchiaviec"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng phối hợp<span class="pull-right">{$tongphlanhdaogiao}</span></p></a>

                                            <a class="tin2" href="{$url}dsphongdangthuchien"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng đang thực hiện<span class="pull-right"></span></p></a>

                                            <a class="tin2" href="{$url}tpphdangthuchien"><p><i class="fa fa-newspaper-o"></i> Công việc phối hợp lãnh đạo Sở giao phòng đang thực hiện<span class="pull-right"></span></p></a>
								        </div>

								    </div>

								</div>-->

                                <div id="accordion">

                                    <h4 class="panel-title">
									
										<a class="tin1" href="{$url}vanbandachidao"><p><i class="fa fa-newspaper-o" aria-hidden="true"></i> Văn bản giao phòng đã chỉ đạo</p></a>

                                        <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseTow" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> Văn bản giao phòng đã chỉ đạo <span class="pull-right">{$vanbanchidao + $vanbandaxuly_stcph + $phdxl}</span></p>

                                        </a>-->

                                    </h4>

                                    <!--<div id="collapseTow" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbandachidao"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì đã chỉ đạo <span class="pull-right">{$vanbanchidao}</span></p></a>

                                            <a class="tin2" href="{$url}vanbandaxulystcph_tp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản SNV phối hợp đã chỉ đạo <span class="pull-right">{if !empty($vanbandaxuly_stcph)}{$vanbandaxuly_stcph}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}vanbanphoihopdachidao"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp đã chỉ đạo <span class="pull-right">{if !empty($phdxl)}{$phdxl}{else}{/if}</span></p></a>



                                        </div>

                                    </div>-->

                                </div>

								{if $vbcduyet > 0}
									<a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> <b>Văn bản hoàn thành chờ phê duyệt </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcduyet}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt </p></a>
								{/if}

            
								{if $dsgiahan > 0}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> <b>Duyệt văn bản gia hạn giải quyết </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsgiahan}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> Duyệt văn bản gia hạn giải quyết</p></a>
								{/if}
								

                                <a class="tin" href="{$url}dshandadexuat"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Văn bản đã đề xuất gia hạn giải quyết<!--<span class="pull-right">{$dsdadexuat}</span>--></p></a>

                               
                                <div id="accordion">

                                    <h4 class="panel-title">
										
										<a class="tin1" href="{$url}dstocongtac"><p><i class="fa fa-list-alt" aria-hidden="true"></i>  Văn bản TTHC của phòng </p></a>

                                       <!-- <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Văn bản TTHC giao phòng <span class="pull-right">{$dstocongtac + $dstocongtac_ht}</span></p>

                                        </a>-->

                                    </h4>

                                    <!--<div id="collapseFive" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            {if !empty($dstocongtac)}<a class="tin2" href="{$url}dstocongtac"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  TTHC chưa hoàn thành <span class="pull-right">{$dstocongtac}</span></p></a>{/if}

                                            {if !empty($dstocongtac_ht)}<a class="tin2" href="{$url}dstocongtac?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng TTHC hoàn thành <span class="pull-right">{$dstocongtac_ht}</span></p></a>{/if}

                                        </div>

                                    </div>-->

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Văn bản đến của phòng <span class="pull-right"></span></p>

                                        </a>

                                    </h4>

                                    <div id="collapseThree" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dsphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì<span class="pull-right">{$dschutrii}</span></p></a>

                                            <a class="tin2" href="{$url}dsphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp<span class="pull-right">{$dsphoihopp}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="tin1">
										{if $vbqt > 0}
                                            <p ><i class="fa fa-list-alt"></i> Văn bản quan trọng </p>
										{else}
											<p style=""><i class="fa fa-list-alt"></i> Văn bản quan trọng</p>
										{/if}

                                        </a>

                                    </h4>

                                    <div id="collapseTwo" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanquantrong"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Chưa hoàn thành</p></a>

                                            <a class="tin2" href="{$url}vanbanquantrong?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đã hoàn thành</p></a>

                                        </div>

                                    </div>

                                </div>

                                <!--                                    <a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><p><i class="fa fa-newspaper-o"></i> Thống kê văn bản theo loại - nơi đến</p></a>-->
								
								<a class="tin" href="{$url}phoihopchuyenlai_tp"><p><i class="fa fa-reply"></i> Văn bản phối hợp trả lại <span class="pull-right">{if !empty($tuchoipp)}{$tuchoipp}{else}{/if}</span></p></a>

                                {/if}

                                <!--                                     giao diện chi cục trưởng-->

                                {if $vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] == 15}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <b><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="tin1">
		
                                            <p><i class="fa fa-newspaper-o"></i> Văn bản giao phòng chờ xử lý <b class="pull-right">
											{if $vbcxlcvp > 0 || $tocongtac > 0} <marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcxlcvp + $congtacdang + $tocongtac +$pph}</marquee>{else} {$vbcxlcvp + $congtacdang + $tocongtac +$pph} {/if}
											</b></p>

                                        </a></b>

                                    </h4>

                                    <div id="collapseOne" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <b><a class="tin2" href="{$url}vanbanchoxuly_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì chờ xử lý <b class="pull-right">
											{if $vbcxlcvp > 0} <marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcxlcvp}</marquee>{else} {$vbcxlcvp} {/if}</b></p></a></b>

                                            {if !empty($congtacdang)}<a class="tin2" href="{$url}congtacdang_cc"><p><i class="fa fa-caret-right"></i> VB công tác Đảng chờ xử lý <b class="pull-right">{$congtacdang}</b></p></a>{/if}

                                            <a class="tin2" href="{$url}vanbanchoxuly_tct"><p><i class="fa fa-caret-right"></i> Văn bản TTHC chờ xử lý <b class="pull-right">{$tocongtac}</b></p></a>

                                            <a class="tin2" href="{$url}phongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp chờ xử lý <b class="pull-right">{if !empty($pph)}{$pph}{else}{/if}</b></p></a>

                                        </div>

                                    </div>

                                </div>

                                <!--<div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" class="tin1">

                                            <p ><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao <span class="pull-right">{$tonglanhdaogiao+$tongphlanhdaogiao}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapse9" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">
                                            <a class="tin2" href="{$url}tpchiaviec"><p><i class="fa fa-newspaper-o"></i> Danh sách công việc lãnh đạo Sở giao phòng<span class="pull-right">{$tonglanhdaogiao}</span></p></a>

                                            <a class="tin2" href="{$url}tpphchiaviec"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng phối hợp<span class="pull-right">{$tongphlanhdaogiao}</span></p></a>

                                            <a class="tin2" href="{$url}dsphongdangthuchien"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao phòng đang thực hiện<span class="pull-right"></span></p></a>
                                        </div>

                                    </div>

                                </div>-->

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTow" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> Văn bản giao phòng đã chỉ đạo <b class="pull-right">{$vanbanchidao_cc + $vanbandaxuly_stcphcc + $phdxl}</b></p>

                                        </a>

                                    </h4>

                                    <div id="collapseTow" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbandaxuly_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì đã chỉ đạo <b class="pull-right">{$vanbanchidao_cc}</b></p></a>

                                            <a class="tin2" href="{$url}vanbandaxulystcph_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản SNV phối hợp đã chỉ đạo <b class="pull-right">{if !empty($vanbandaxuly_stcphcc)}{$vanbandaxuly_stcphcc}{else}{/if}</b></p></a>

                                            <a class="tin2" href="{$url}vanbanphoihopdachidao"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp đã chỉ đạo <b class="pull-right">{if !empty($phdxl)}{$phdxl}{else}{/if}</b></p></a>



                                        </div>

                                    </div>

                                </div>

                                <!--                                    <a class="tin" href="{$url}baocaodauvieccanhan"><p><b><i class="fa fa-list-alt" aria-hidden="true"></i>  Danh sách văn bản theo phòng <b class="pull-right"></b></b></p></a>-->

								
								{if $vbcduyet > 0}
									<b><a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcduyet}</marquee></b></p></a></b>
								{else}
									<a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt</p></a>
								{/if}

                                <a class="tin" href="{$url}phoihopchuyenlai_tp"><p><i class="fa fa-reply"></i> Văn bản phối hợp trả lại <b class="pull-right">{if !empty($tuchoipp)}{$tuchoipp}{else}{/if}</b></p></a>
								
								{if $dsgiahan > 0}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> <b>Duyệt văn bản gia hạn giải quyết </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsgiahan}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> Duyệt văn bản gia hạn giải quyết</p></a>
								{/if}

                                <a class="tin" href="{$url}dshandadexuat"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Văn bản đã đề xuất gia hạn giải quyết<b class="pull-right">{$dsdadexuat}</b></p></a>

                                {if !empty($dsdonthu) || !empty($dsdonthuht) }

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số lượng Đơn thư KNTC giao phòng xử lý <b class="pull-right">{$dsdonthu + $dsdonthuht}</b></p>

                                        </a>

                                    </h4>

                                    <div id="collapseFour" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            {if !empty($dsdonthu)}<a class="tin2" href="{$url}dsdonthu"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  Đơn thư KNTC chưa hoàn thành <b class="pull-right">{$dsdonthu}</b></p></a>{/if}

                                            {if !empty($dsdonthuht)}<a class="tin2" href="{$url}dsdonthu?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng Đơn thư KNTC hoàn thành <b class="pull-right">{$dsdonthuht}</b></p></a>{/if}

                                        </div>

                                    </div>

                                </div>

                                {/if}

                                {if !empty($dstocongtac) || !empty($dstocongtac_ht) }

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số lượng TTHC giao phòng xử lý <span class="pull-right">{$dstocongtac + $dstocongtac_ht}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapseFive" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            {if !empty($dstocongtac)}<a class="tin2" href="{$url}dstocongtac"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  TTHC chưa hoàn thành <span class="pull-right">{$dstocongtac}</span></p></a>{/if}

                                            {if !empty($dstocongtac_ht)}<a class="tin2" href="{$url}dstocongtac?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng TTHC hoàn thành <span class="pull-right">{$dstocongtac_ht}</span></p></a>{/if}

                                        </div>

                                    </div>

                                </div>

                                {/if}



                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Văn bản giao phòng xử lý <span class="pull-right"></span></p>

                                        </a>

                                    </h4>

                                    <div id="collapseThree" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dsphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì<span class="pull-right">{$dschutrii}</span></p></a>

                                            <a class="tin2" href="{$url}dsphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp<span class="pull-right">{$dsphoihopp}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="tin1">
										{if $vbqt > 0}
                                            <p ><i class="fa fa-list-alt"></i> <b>Văn bản quan trọng </b></p>
										{else}
											<p style=""><i class="fa fa-list-alt"></i> Văn bản quan trọng</p>
										{/if}

                                        </a>

                                    </h4>

                                    <div id="collapseTwo" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanquantrong"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Chưa hoàn thành</p></a>

                                            <a class="tin2" href="{$url}vanbanquantrong?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đã hoàn thành</p></a>

                                        </div>

                                    </div>

                                </div><div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="tin1">
										{if $vbqt > 0}
                                            <p ><i class="fa fa-list-alt"></i> <b>Văn bản quan trọng </b></p>
										{else}
											<p style=""><i class="fa fa-list-alt"></i> Văn bản quan trọng</p>
										{/if}

                                        </a>

                                    </h4>

                                    <div id="collapseTwo" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}vanbanquantrong"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Chưa hoàn thành</p></a>

                                            <a class="tin2" href="{$url}vanbanquantrong?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đã hoàn thành</p></a>

                                        </div>

                                    </div>

                                </div>

                                <!--                                    <a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><p><i class="fa fa-newspaper-o"></i> Thống kê văn bản theo loại - nơi đến</p></a>-->

                                {/if}

                                    {if $vanban['iQuyenHan_DHNB'] == 7}

                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="tin1">
												{if $vbcxlcvp > 0}
													<b><p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcxlcvp + $congtacdang + $tocongtac +$phophongphoihop+$theodoivanban+$pph}</marquee></b></p>
													
												{else}
													<p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý <span class="pull-right">{$vbcxlcvp + $congtacdang + $tocongtac +$phophongphoihop+$theodoivanban+$pph}</span></p>
												{/if}
                                            </a>
                                        </h4>

                                        <div id="collapseOne" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                <a class="tin2" href="{$url}vanbanchoxuly_pp"><p style="{if !empty($vbcxlcvp)}font-weight:bold{/if}"><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản đến chờ xử lý <span class="pull-right">{$vbcxlcvp}</span></p></a>

                                                {if !empty($congtacdang)}<a class="tin2" href="{$url}congtacdang_pp"><p><i class="fa fa-caret-right"></i> VB công tác Đảng chờ xử lý <span class="pull-right">{$congtacdang}</span></p></a>{/if}

                                                <a class="tin2" href="{$url}tocongtac_pp"><p style="{if !empty($tocongtac)}font-weight:bold{/if}"><i class="fa fa-caret-right"></i> Văn bản TTHC chờ xử lý <span class="pull-right">{$tocongtac}</span></p></a>

                                                {if !empty($phophongphoihop)}<a class="tin2" href="{$url}phophongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> VB phó phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($phophongphoihop)}{$phophongphoihop}{else}{/if}</span></p></a>{/if}

                                                <a class="tin2" href="{$url}phongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản phối hợp chờ xử lý <span class="pull-right">{if !empty($pph)}{$pph}{else}{/if}</span></p></a>

                                            </div>

                                        </div>

                                    </div>
									
									<div id="accordion">

                                        <h4 class="panel-title">
										
                                            {if $dschidao > $chidaodaxem}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidao - $chidaodaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
											{/if}

                                        </h4>

									</div>
									
									<div id="accordion">

                                        <h4 class="panel-title">
										
                                          {if $dsykien > $ykiendaxem}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykien - $ykiendaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
											{/if}

                                        </h4>

								</div>

                                  <!-- <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne11" class="tin1">

                                                <p style=""><i class="fa fa-newspaper-o"></i> Văn bản lãnh đạo Sở giao chờ xử lý <span class="pull-right">{$tongphophonglanhdaogiao +$tongphoihopthuly}</span></p>
												
												

                                            </a>

                                        </h4>

                                        <div id="collapseOne11" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                            <a class="tin" href="{$url}ppchiaviec"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao chờ xử lý<span class="pull-right">{$tongphophonglanhdaogiao}</span></p></a>

                                            <a class="tin" href="{$url}ldphchiaviec"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo giao phối hợp chờ xử lý<span class="pull-right">{$tongphoihopthuly}</span></p></a>


                                            <a class="tin" href="{$url}dsppdangchidao"><p><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao đang thực hiện<span class="pull-right"></span></p></a>

                                            <a class="tin" href="{$url}ldphdangthuchien"><p><i class="fa fa-newspaper-o"></i> Công việc phối hợp lãnh đạo Sở giao đang thực hiện<span class="pull-right"></span></p></a>
                                        </div>

                                    </div>

                                </div>-->
								
								
                                    <div id="accordion">

                                        <h4 class="panel-title">
										
											<a class="tin1" href="{$url}vanbandachidao"><p><i class="fa fa-newspaper-o" aria-hidden="true"></i> Văn bản đã chỉ đạo </p></a>

                                            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseTow" class="tin1">

                                                <p><i class="fa fa-newspaper-o"></i> Văn bản đã chỉ đạo <span class="pull-right">{$vanbanchidao + $vanbandaxuly_stcph + $phdxl}</span></p>

                                            </a>-->

                                        </h4>

                                        <!--<div id="collapseTow" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                <a class="tin2" href="{$url}vanbandachidao"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản chủ trì đã chỉ đạo <span class="pull-right">{$vanbanchidao}</span></p></a>

                                                <a class="tin2" href="{$url}vanbandaxulystcph_tp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản SNV phối hợp đã chỉ đạo <span class="pull-right">{if !empty($vanbandaxuly_stcph)}{$vanbandaxuly_stcph}{else}{/if}</span></p></a>

                                                <a class="tin2" href="{$url}vanbanphoihopdachidao"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản phối hợp đã chỉ đạo <span class="pull-right">{if !empty($phdxl)}{$phdxl}{else}{/if}</span></p></a>

                                            </div>

                                        </div>-->

                                    </div>
									
									{if $vbcduyet > 0}
										<b><a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcduyet}</marquee></b></p></a></b>
									{else}
										<!--<a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt</p></a>-->
									{/if}
									
									{if $dsgiahan > 0}
										<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> <b>Duyệt văn bản gia hạn giải quyết </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsgiahan}</marquee></b></p></a>
									{else}
										<!--<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> Duyệt văn bản gia hạn giải quyết</p></a>-->
									{/if}

                                    <a class="tin" href="{$url}dshandadexuat"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Văn bản đã đề xuất hạn giải quyết<!--<span class="pull-right">{$dsdadexuat}</span>--></p></a>

                                    <a class="tin" href="{$url}dsvanbanden?id={$vanban['PK_iMaCB']}"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Danh sách văn bản được giao</p></a>

                                    <!--<a class="tin" href="{$url}dsnguoidungphoihop"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Văn bản cá nhân phối hợp xử lý <span class="pull-right">{$dsphoihoppcv}</span></p></a>-->

                                    {if !empty($dsdonthu) || !empty($dsdonthuht) }

                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="tin1">

                                                <p><i class="fa fa-list-alt"></i> Tổng số lượng Đơn thư KNTC giao phòng xử lý <span class="pull-right">{$dsdonthu + $dsdonthuht}</span></p>

                                            </a>

                                        </h4>

                                        <div id="collapseFour" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                {if !empty($dsdonthu)}<a class="tin2" href="{$url}dsdonthu"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  Đơn thư KNTC chưa hoàn thành <span class="pull-right">{$dsdonthu}</span></p></a>{/if}

                                                {if !empty($dsdonthuht)}<a class="tin2" href="{$url}dsdonthu?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng Đơn thư KNTC hoàn thành <span class="pull-right">{$dsdonthuht}</span></p></a>{/if}

                                            </div>

                                        </div>

                                    </div>

                                    {/if}

                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a class="tin1" href="{$url}dstocongtac"><p><i class="fa fa-list-alt" aria-hidden="true"></i>  Văn bản TTHC giao phòng xử lý</p></a>

                                        </h4>

                                        <!--<div id="collapseFive" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                {if !empty($dstocongtac)}<a class="tin2" href="{$url}dstocongtac"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  TTHC chưa hoàn thành <span class="pull-right">{$dstocongtac}</span></p></a>{/if}

                                                {if !empty($dstocongtac_ht)}<a class="tin2" href="{$url}dstocongtac?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng TTHC hoàn thành <span class="pull-right">{$dstocongtac_ht}</span></p></a>{/if}

                                            </div>

                                        </div>-->

                                    </div>



                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="tin1">

                                                <p><i class="fa fa-list-alt"></i> Văn bản giao phòng xử lý <span class="pull-right"></span></p>

                                            </a>

                                        </h4>

                                        <div id="collapseThree" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                <a class="tin2" href="{$url}dsphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì<span class="pull-right">{$dschutrii}</span></p></a>

                                                <a class="tin2" href="{$url}dsphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp<span class="pull-right">{$dsphoihopp}</span></p></a>

                                            </div>

                                        </div>

                                    </div>

                                    {/if}

                                    {if $vanban['iQuyenHan_DHNB'] == 8}

                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="tin1">
											
												{if $vbcxlcvp > 0}
													<b><p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcxlcvp + $congtacdang + $vbph}</marquee></b></p>
									
												{else}
													{if $vbcxlcvp + $congtacdang + $vbph > 0}
														<p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý<span class="pull-right">{$vbcxlcvp + $congtacdang + $vbph}</span></p>
													{else}
														<p><i class="fa fa-newspaper-o"></i> Văn bản đến chờ xử lý</p>
													{/if}
													
												{/if}

                                            </a>

                                        </h4>

                                        <div id="collapseOne" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                <a class="tin2" href="{$url}vanbanchoxuly_cv"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  <b>Văn bản chủ trì chờ xử lý <span class="pull-right">{$vbcxlcvp}</span></b></p></a>

                                                {if !empty($congtacdang)}<a class="tin2" href="{$url}congtacdang_cv"><p><i class="fa fa-caret-right" aria-hidden="true"></i> VB công tác Đảng chờ xử lý <span class="pull-right">{if !empty($congtacdang)}{$congtacdang}{else}{/if}</span></p></a>{/if}

                                                <a class="tin2" href="{$url}vanbanchoxuly_cvph"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản chờ phối hợp <span class="pull-right">{$vbph}</span></p></a>

                                            </div>

                                        </div>

                                    </div>
									
									{if $dstocongtac + $dstocongtac_ht > 0}
                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="tin1">

												<p style="{if !empty($dstocongtac)}font-weight:bold{/if}"><i class="fa fa-list-alt"></i> Văn bản TTHC chờ xử lý   <span class="pull-right">
												{if !empty($dstocongtac)}<marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dstocongtac}</marquee>{else}{/if}
												</span>
												</p>

                                            </a>

                                        </h4>

                                        <div id="collapseFive" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                               
												<a class="tin2" href="{$url}tocongtac_cv"><p style="{if !empty($dstocongtac)}font-weight:bold{/if}"><i class="fa fa-caret-right"></i> Văn bản TTHC chờ xử lý   <span class="pull-right">{$dstocongtac}</span></p>

                                                <a class="tin2" href="{$url}dstocongtac?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Văn bản TTHC đã hoàn thành <span class="pull-right">{$dstocongtac_ht}</span></p></a>

                                            </div>

                                        </div>

                                    </div>
									{/if}
									
									<div id="accordion">

                                        <h4 class="panel-title">
										
                                            {if $dschidao > $chidaodaxem}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidao - $chidaodaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dschidao"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
											{/if}

                                        </h4>

									</div>
									
									<div id="accordion">

                                        <h4 class="panel-title">
										
                                          {if $dsykien > $ykiendaxem}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykien - $ykiendaxem}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dsykien"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
											{/if}

                                        </h4>

								</div>

                                    <!--<div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse10" class="tin1">

                                            <p ><i class="fa fa-newspaper-o"></i> Công việc lãnh đạo Sở giao <span class="pull-right">{$tongchuyenvienlanhdaogiao+$tongphoihopthuly}</span></p>

                                        </a>
										

                                    </h4>

                                    <div id="collapse10" class="panel-collapse collapse" style="padding-left: 15px;">

                                        <a class="tin2" href="{$url}dschuyenvienxuly"><p><i class="fa fa-caret-right"></i> Công việc lãnh đạo Sở giao chờ xử lý<span class="pull-right">{$tongchuyenvienlanhdaogiao}</span></p></a>

                                        <a class="tin2" href="{$url}dscvphxuly"><p><i class="fa fa-caret-right"></i> Công việc lãnh đạo giao phối hợp chờ xử lý<span class="pull-right">{$tongphoihopthuly}</span></p></a>

                                    </div>

                                </div>-->

                                    <div id="accordion">

                                        <h4 class="panel-title">
										
											<a class="tin1" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản đến đã hoàn thành</p></a>

                                            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseTow" class="tin1">

                                                <p><i class="fa fa-check-square-o"></i> Văn bản đến đã hoàn thành:  <span class="pull-right">{$vbcduyet + $vbhoanthanh}</span></p>

                                            </a>-->

                                        </h4>

                                        <!--<div id="collapseTow" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

												<a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành đang chờ phê duyệt <span class="pull-right">{$vbcduyet}</span></p></a>


                                                <a class="tin2" href="{$url}vanbanchopheduyet?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản hoàn thành đã được duyệt <span class="pull-right">{$vbhoanthanh}</span></p></a>

                                            </div>

                                        </div>-->

                                    </div>

                                    {if !empty($dsdonthu) || !empty($dsdonthuht) }

                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="tin1">

                                                <p><i class="fa fa-list-alt"></i> Tổng số lượng Đơn thư KNTC giao xử lý <span class="pull-right">{$dsdonthu + $dsdonthuht}</span></p>

                                            </a>

                                        </h4>

                                        <div id="collapseFour" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                {if !empty($dsdonthu)}<a class="tin2" href="{$url}dsdonthu"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng  Đơn thư KNTC chưa hoàn thành <span class="pull-right">{$dsdonthu}</span></p></a>{/if}

                                                {if !empty($dsdonthuht)}<a class="tin2" href="{$url}dsdonthu?cacloaivanban=2"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Số Lượng Đơn thư KNTC hoàn thành <span class="pull-right">{$dsdonthuht}</span></p></a>{/if}

                                            </div>

                                        </div>

                                    </div>

                                    {/if}

                                    <a class="tin" href="{$url}dshandadexuat"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Văn bản đã đề xuất hạn giải quyết<!--<span class="pull-right">{$dsdadexuat}</span>--></p></a>
									
									<div id="accordion">

                                        <h4 class="panel-title">
										
											<a class="tin1" href="{$url}dsvanbanden?id={$vanban['PK_iMaCB']}"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Danh sách văn bản được giao <span class="pull-right">{$canboxuly}</span></p></a>

                                            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="tin1">

                                                <p><i class="fa fa-list-alt"></i> Văn bản đến của cá nhân <span class="pull-right">{$canboxuly + $dsphoihoppcv}</span></p>

                                            </a>-->

                                        </h4>

                                        <div id="collapseThree" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                <a class="tin2" href="{$url}dsvanbanden?id={$vanban['PK_iMaCB']}"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản cá nhân chủ trì xử lý <span class="pull-right">{$canboxuly}</span></p></a>

                                                <a class="tin2" href="{$url}dsnguoidungphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản cá nhân phối hợp <span class="pull-right">{$dsphoihoppcv}</span></p></a>

                                            </div>

                                        </div>

                                    </div>

                                    <div id="accordion">

                                        <h4 class="panel-title">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="tin1">

                                                <p><i class="fa fa-list-alt"></i> Văn bản giao phòng xử lý <span class="pull-right"></span></p>

                                            </a>

                                        </h4>

                                        <div id="collapseSix" class="panel-collapse collapse">

                                            <div class="panel-body padding-box">

                                                <a class="tin2" href="{$url}dsphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng chủ trì<span class="pull-right">{$dschutrii}</span></p></a>

                                                <a class="tin2" href="{$url}dsphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Văn bản giao phòng phối hợp<span class="pull-right">{$dsphoihopp}</span></p></a>

                                            </div>

                                        </div>

                                    </div>

                                    <!--                                    <a class="tin" href="{$url}baocaodauvieccanhan"><p><span><i class="fa fa-list-alt" aria-hidden="true"></i>  Danh sách văn bản theo phòng <span class="pull-right"></span></span></p></a>-->

                                    <!--                                    <a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><p><i class="fa fa-newspaper-o"></i> Thống kê văn bản theo loại - nơi đến</p></a>-->

                                    {/if}

                                    {if $vanban['iQuyenHan_DHNB'] == 10}

                                    <a class="tin" href="{$url}vanbanchoxuly_ccp"><p><i class="fa fa-newspaper-o"></i> Văn bản phòng chủ trì chờ xử lý <span class="pull-right">{$vbcxlcvp}</span></p></a>
                                        
                           <!-- <div id="accordion">

                            <h4 class="panel-title">

                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSixone" class="tin1">

                                    <p style="color: red"><i class="fa fa-list-alt"></i> Danh sách công việc lãnh đạo Sở giao phòng xử lý <span class="pull-right">{$tongccplanhdaogiao + $tongphoihopthuly}</span></p>

                                </a>

                            </h4>

                            <div id="collapseSixone" class="panel-collapse collapse">

                                <div class="panel-body padding-box">

                                    <a class="tin" href="{$url}ccpchiaviec"><p><i class="fa fa-newspaper-o"></i>Công việc lãnh đạo giao chờ xử lý<span class="pull-right">{$tongccplanhdaogiao}</span></p></a>

                                    <a class="tin" href="{$url}ldphchiaviec"><p><i class="fa fa-newspaper-o"></i>Công việc lãnh đạo giao phối hợp chờ xử lý<span class="pull-right">{$tongphoihopthuly}</span></p></a>

                                    <a class="tin" href="{$url}ccpchidaodangthuchien"><p><i class="fa fa-newspaper-o"></i>Công việc lãnh đạo Sở giao đang thực hiện<span class="pull-right"></span></p></a>

                                    <a class="tin" href="{$url}ldphdangthuchien"><p><i class="fa fa-newspaper-o"></i> Công việc phối hợp lãnh đạo Sở giao đang thực hiện<span class="pull-right"></span></p></a>

                                </div>
                            </div>
                        </div>-->

                                    {if !empty($tocongtac)}<a class="tin" href="{$url}tocongtac_cc"><p><i class="fa fa-newspaper-o"></i> Văn bản TTHC chờ xử lý <span class="pull-right">{$tocongtac}</span></p></a>{/if}

                                    <a class="tin" href="{$url}vanbandaxuly_ccp"><p><i class="fa fa-newspaper-o"></i> Văn bản đã chỉ đạo (chưa hoàn thành) <span class="pull-right">{if !empty($vanbanchidao_pcc)}{$vanbanchidao_pcc}{else}{/if}</span></p></a>

                                    <a class="tin" href="{$url}vanbandaxulystcph_ccp"><p><i class="fa fa-newspaper-o"></i> Văn bản đã chỉ đạo (SNV phối hợp) <span class="pull-right">{$vanbandaxuly_stcphccp}</span></p></a>
									
									{if $dsgiahan > 0}
										<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> <b>Duyệt văn bản gia hạn giải quyết </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsgiahan}</marquee></b></p></a>
									{else}
										<a class="tin" href="{$url}dsdexuatgiahan"><p><i class="fa fa-check-square-o"></i> Duyệt văn bản gia hạn giải quyết</p></a>
									{/if}
									
									{if $vbcduyet > 0}
										<b><a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbcduyet}</marquee></b></p></a></b>
									{else}
										<a class="tin" href="{$url}vanbanchopheduyet"><p><i class="fa fa-check-square-o"></i> Văn bản hoàn thành chờ phê duyệt</p></a>
									{/if}

                                    {if !empty($dsdonthu)}<a class="tin" href="{$url}dsdonthu"><p><i class="fa fa-list-alt" aria-hidden="true"></i>  Danh sách văn bản Đơn thư KNTC <span class="pull-right">{$dsdonthu}</span></p></a>{/if}



                                    <a class="tin" href="{$url}dsphongchutri1"><p><i class="fa fa-list-alt" aria-hidden="true"></i> Số lượng văn bản giao chi cục phó chủ trì<span class="pull-right">{$dschutrii1}</span></p></a>

                                    

                                    <a class="tin" href="{$url}dsphongchutri"><p><i class="fa fa-list-alt" aria-hidden="true"></i>   Văn bản phòng chủ trì <span class="pull-right">{$dschutrii}</span></p></a>



                                    <a class="tin" href="{$url}dsphoihop"><p><i class="fa fa-list-alt" aria-hidden="true"></i>   Văn bản phòng phối hợp <span class="pull-right">{$dsphoihopp}</span></p></a>

                                    {/if}

                                    <!-- truong phong chi cuc-->

                                    

                                    {if $year!=2017}

                                  

                                    {if $vanban['iQuyenHan_DHNB']==8}

                                        <a class="tin" href="{$url}dexuatphoihop_cv"><p><i class="fa fa-newspaper-o"></i> Đề xuất công việc phối hợp chờ duyệt <span class="pull-right">{if !empty($dexuatphoihop)}{$dexuatphoihop}{else}{/if}</span></p></a>

                                        
										{if $dexuatphoihopxuly > 0}								
											<b><a class="tin" href="{$url}dexuatphoihop_cv_ph"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Công việc phối hợp chờ xử lý <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dexuatphoihopxuly}</marquee></b></p></a></b>								
										{else}								
											<a class="tin" href="{$url}dexuatphoihop_cv_ph"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Công việc phối hợp chờ xử lý</p></a>
										{/if}

                                    {/if}

                                    {if $vanban['iQuyenHan_DHNB']==7}

                                        <a class="tin" href="{$url}dexuatphoihop_pp"><p><i class="fa fa-newspaper-o"></i> Đề xuất công việc phối hợp chờ duyệt <span class="pull-right">{if !empty($dexuatphoihop)}{$dexuatphoihop}{else}{/if}</span></p></a>

                                       
										{if $dexuatphoihopxuly > 0}								
											<b><a class="tin" href="{$url}dexuatphoihop_pp_ph"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Công việc phối hợp chờ xử lý <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dexuatphoihopxuly}</marquee></b></p></a></b>								
										{else}								
											<a class="tin" href="{$url}dexuatphoihop_pp_ph"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Công việc phối hợp chờ xử lý</p></a>
										{/if}
										

                                    {/if}

                                    {if $vanban['iQuyenHan_DHNB']==3 || $vanban['iQuyenHan_DHNB']==6}

										{if $dexuatphoihop > 0}
											<a class="tin" href="{$url}dexuatphoihop_tp"><p><i class="fa fa-check-square-o"></i> <b> Đề xuất công việc phối hợp chờ duyệt </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dexuatphoihop}</marquee></b></p></a>
										{else}
											<a class="tin" href="{$url}dexuatphoihop_tp"><P><i class="fa fa-check-square-o"></i> Đề xuất công việc phối hợp chờ duyệt</p></A>
										{/if}

										{if $dexuatphoihopxuly > 0}
											<a class="tin" href="{$url}dexuatphoihop_tp_ph"><p><i class="fa fa-check-square-o"></i> <b> Công việc phối hợp chờ xử lý </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dexuatphoihopxuly}</marquee></b></p></a>
										{else}
											<a class="tin" href="{$url}dexuatphoihop_tp_ph"><P><i class="fa fa-check-square-o"></i> Công việc phối hợp chờ xử lý</p></A>
										{/if}

                                    {/if}

                                    {/if}

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6" style="min-width: 50%;">

                        <div class="panel panel-info giaymoi" style="border-radius: 10px; min-height: 95%">

                            <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;"><img src="{$url}assets/img/icon-vb.png" alt="" style="width: 25px;    margin-top: -5px;"> <b style="font-size: 15px; text-shadow: 1px 1px 2px black;">GIẤY MỜI</b></div>

                            <div class="panel-body">
								{if $vanban['iQuyenHan_DHNB'] != 3&&$vanban['iQuyenHan_DHNB'] != 4&&$vanban['iQuyenHan_DHNB'] != 5&&$vanban['iQuyenHan_DHNB'] != 9}
                               
									{if $giaymoidahopchuaxuly > 0}
										<a class="tin" href="{$url}giaymoidahopchuaxuly"><p><i class="fa fa-file-text-o"></i> <b> Giấy mời đã họp, chưa kết thúc:   </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$giaymoidahopchuaxuly}</marquee></b></p></a>
									{else}
										<a class="tin" href="{$url}giaymoidahopchuaxuly"><p><i class="fa fa-file-text-o"></i> Giấy mời đã họp, chưa kết thúc</p></a>
									{/if}
													
                                {/if}
                                {if $vanban['iQuyenHan_DHNB'] == 3}
								
								<u><p><center><b>QUYỀN CHÁNH VĂN PHÒNG</b></center></p></u>
								
								{if $plgm > 0}								
									<b><a class="tin" href="{$url}giaymoi_cvp"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Giấy mời chờ phân loại <b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$plgm}</marquee></b></p></a></b>								
								{else}								
									<a class="tin" href="{$url}giaymoi_cvp"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Giấy mời chờ phân loại</p></a>
								{/if}
								

                                <a class="tin" href="{$url}giaymoidaphanloai"><p><i class="fa fa-sitemap" aria-hidden="true"></i> Giấy mời đã phân loại<!-- <span class="pull-right">{$gmdpl}</span>--></p></a>
								
                                <br><br><br>
                                <hr style=" border: 2px solid lightblue;">
								<u><p><center><b>QUYỀN TRƯỞNG PHÒNG</b></center></p></u>
                                
								{if $vanban['iQuyenHan_DHNB'] == 3}
                                <a class="tin" href="{$url}giaymoidahopchuaxuly"><p style="font-size: 16px;"><i class="fa fa-file-text-o"></i> Giấy mời của phòng đã dự họp, chưa kết thúc <span class="pull-right">{($giaymoidahopchuaxuly)?$giaymoidahopchuaxuly:''}</span></p></a>
                                {/if}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="tin1">
										
											<p>
											{if $gmcxl > 0}
												<i class="fa fa-file-text-o"></i> <b>Giấy mời giao phòng chờ xử lý </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl + $gmphcxl}</marquee></b>
											{else}
												<i class="fa fa-file-text-o"></i> Giấy mời giao phòng chờ xử lý <span class="pull-right">{($gmphcxl>0) ? $gmphcxl : ''}</span>
											{/if}
											</p>

                                        </a>

                                    </h4>

                                    <div id="collapse1" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoi_tp"><p style="{if $gmcxl>0}font-weight:bold{/if}"><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng chủ trì chờ xử lý <span class="pull-right">{$gmcxl}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoiphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng chờ phối hợp <span class="pull-right">{if !empty($gmphcxl)}{$gmphcxl}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>



                                <div id="accordion">

                                    <h4 class="panel-title">
									
										 <a class="tin1" href="{$url}giaymoidaxuly"><p><i class="fa fa-file-text-o" aria-hidden="true"></i> Giấy mời giao phòng đã xử lý <!--<span class="pull-right">{$gmdxl}</span>--></p></a>

                                        <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="tin1">

                                            <p><i class="fa fa-file-text-o"></i> Giấy mời giao phòng đã chỉ đạo <span class="pull-right">{$gmdxl + $gmphdxl}</span></p>

                                        </a>-->

                                    </h4>

                                    <div id="collapse2" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoidaxuly"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng chủ trì đã chỉ đạo<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

                                            <a class="tin2" href="{$url}giaymoidaxuly_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng phối hợp đã chỉ đạo <span class="pull-right">{if !empty($gmphdxl)}{$gmphdxl}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>
								
								<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dschidaogm > $chidaodaxemgm}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidaogm - $chidaodaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
												{/if}

											</h4>

									</div>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dsykiengm > $ykiendaxemgm}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykiengm - $ykiendaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất <span class="pull-right"></span></p></a>
												{/if}

											</h4>

									</div>


								{if $vbduyetgiaymoi > 0}
									<a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> <b> Giấy mời đã hoàn thành chờ phê duyệt </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vbduyetgiaymoi}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> Giấy mời đã hoàn thành chờ phê duyệt</p></a>
								{/if}

                                <a class="tin" href="{$url}giaymoiphoihopchuyenlai_tp"><p><i class="fa fa-reply"></i> Giấy mời phối hợp trả lại <span class="pull-right">{if !empty($tuchoigm)}{$tuchoigm}{else}{/if}</span></p></a>
                               
                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 4}
								
									{if $gmcxl > 0}
										<a class="tin" href="{$url}giaymoi_gd"><p><i class="fa fa-file-text-o"></i> <b>Giấy mời chờ xử lý</b> <b class="pull-right"> <marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl}</marquee></b></p></a>
									{else} 
										<a class="tin" href="{$url}giaymoi_gd"><p><i class="fa fa-file-text-o"></i> Giấy mời chờ xử lý</p></a>
									{/if}

									<a class="tin" href="{$url}giaymoidaxuly_gd"><p><i class="fa fa-file-text-o"></i> Giấy mời đã xử lý <!--<span class="pull-right">{$gmdxl}</span>--></p></a>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dschidaogm > $chidaodaxemgm}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidaogm - $chidaodaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
												{/if}

											</h4>

									</div>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dsykiengm > $ykiendaxemgm}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykiengm - $ykiendaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
												{/if}

											</h4>

									</div>

									<!--<a class="tin" target="_blank" href="{$url}dsgiaymoi"><p> Danh sách giấy mời đến của Sở</p></a>-->

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 5}
								
									{if $gmcxl > 0}
										<a class="tin" href="{$url}giaymoi_pgd"><p><i class="fa fa-file-text-o"></i> <b>Giấy mời chờ xử lý</b> <b class="pull-right"> <marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl+$getAppoAwaitPH}</marquee></b></p></a>
									{else} 
										<a class="tin" href="{$url}giaymoi_pgd"><p><i class="fa fa-file-text-o"></i> Giấy mời chờ xử lý <span class="pull-right">{($gmcxl+$getAppoAwaitPH > 0)? $gmcxl+$getAppoAwaitPH : ''}</span></p></a>
									{/if}

									<div id="accordion">

										<h4 class="panel-title">

											<!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" class="tin1">-->
											<a class="tin1" href="{$url}giaymoidaxuly_gd">
												<p><i class="fa fa-newspaper-o"></i> Giấy mời đã xử lý<!--<span class="pull-right">{$gmdxl + $getDocSeenBGD_GM}</span>--></p>

											</a>

										</h4>

										<div id="collapseTen" class="panel-collapse collapse">

											<div class="panel-body padding-box">

												<a class="tin2" href="{$url}giaymoidaxuly_gd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời Lãnh đạo đã chỉ đạo<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

												<a class="tin2" href="{$url}dsgm_bgd"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời xem để biết <span class="pull-right">{if !empty($getDocSeenBGD_GM)}{$getDocSeenBGD_GM}{else}{/if}</span></p></a>

											</div>

										</div>

									</div>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dschidaogm > $chidaodaxemgm}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidaogm - $chidaodaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
												{/if}

											</h4>

									</div>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dsykiengm > $ykiendaxemgm}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykiengm - $ykiendaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
												{/if}

											</h4>

									</div>
								

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] != 15}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="tin1">

											<p>
                                            {if $gmcxl > 0}
												<i class="fa fa-file-text-o"></i> <b>Giấy mời giao phòng chờ xử lý </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl + $gmph}</marquee></b>
											{else}
												<i class="fa fa-file-text-o"></i> Giấy mời giao phòng chờ xử lý <span class="pull-right">{($gmcxl + $gmph >0)?$gmcxl + $gmph:''}</span>
											{/if}
											</p>

                                        </a>

                                    </h4>

                                    <div id="collapse1" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoi_tp"><p style="{if $gmcxl>0}font-weight:bold{/if}"><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời phòng chủ trì chờ xử lý <span class="pull-right">{$gmcxl}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoiphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($gmph)}{$gmph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>
								
								
								<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dschidaogm > $chidaodaxemgm}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidaogm - $chidaodaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
												{/if}

											</h4>

									</div>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dsykiengm > $ykiendaxemgm}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykiengm - $ykiendaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
												{/if}

											</h4>

									</div>



                                <div id="accordion">

                                    <h4 class="panel-title">
									
										<a class="tin1" href="{$url}giaymoidaxuly"><p><i class="fa fa-file-text-o" aria-hidden="true"></i> Giấy mời giao phòng đã chỉ đạo<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

                                        <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="tin1">

                                            <p><i class="fa fa-file-text-o"></i> Giấy mời giao phòng đã chỉ đạo <span class="pull-right">{$gmdxl + $gmphdxl + $gmdht}</span></p>

                                        </a>-->

                                    </h4>

                                    <div id="collapse2" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoidaxuly"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng chủ trì đã chỉ đạo<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

                                            <a class="tin2" href="{$url}giaymoihoanthanh"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng chủ trì đã hoàn thành <span class="pull-right">{$gmdht}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoidaxuly_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng phối hợp đã chỉ đạo <span class="pull-right">{if !empty($gmphdxl)}{$gmphdxl}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> Giấy mời đã hoàn thành chờ phê duyệt <span class="pull-right">{($vbduyetgiaymoi)?$vbduyetgiaymoi:''}</span></p></a>

                                <a class="tin" href="{$url}giaymoiphoihopchuyenlai_tp"><p><i class="fa fa-reply"></i> Giấy mời phối hợp trả lại <span class="pull-right">{if !empty($tuchoigm)}{$tuchoigm}{else}{/if}</span></p></a>

                                {/if}

                                <!--                                // giấy mời chi cục-->

                                {if $vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] == 15}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="tin1">

											<p>
                                            {if $gmcxl > 0}
												<i class="fa fa-file-text-o"></i> <b>Giấy mời giao phòng chờ xử lý </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl + $gmph}</marquee></b>
											{else}
												<i class="fa fa-file-text-o"></i> Giấy mời giao phòng chờ xử lý <span class="pull-right">{$gmcxl + $gmph}</span>
											{/if}
											</p>

                                        </a>

                                    </h4>

                                    <div id="collapse1" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoi_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời phòng chủ trì chờ xử lý <span class="pull-right">{$gmcxl}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoiphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($gmph)}{$gmph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>



                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="tin1">

                                            <p><i class="fa fa-file-text-o"></i> Giấy mời giao phòng đã chỉ đạo <span class="pull-right">{$gmdxl_cc + $gmphdxl}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapse2" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoidaxuly_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng chủ trì đã chỉ đạo <span class="pull-right">{$gmdxl_cc}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoidaxuly_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng phối hợp đã chỉ đạo <span class="pull-right">{if !empty($gmphdxl)}{$gmphdxl}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> Giấy mời đã hoàn thành chờ phê duyệt <span class="pull-right">{($vbduyetgiaymoi)?$vbduyetgiaymoi:''}</span></p></a>

                                <a class="tin" href="{$url}giaymoiphoihopchuyenlai_tp"><p><i class="fa fa-reply"></i> Giấy mời phối hợp trả lại <span class="pull-right">{if !empty($tuchoigm)}{$tuchoigm}{else}{/if}</span></p></a>

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 7}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="tin1">

									{if $gmcxl > 0}
										<b><p><i class="fa fa-newspaper-o"></i> Giấy mời chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl + $gm_phophongph + $gmph}</marquee></b></p>
									
									{else}
										<p><i class="fa fa-newspaper-o"></i> Giấy mời chờ xử lý <span class="pull-right">{($gmcxl + $gm_phophongph + $gmph > 0)?$gmcxl + $gm_phophongph + $gmph : ''}</span></p>
									{/if}
									

                                        </a>

                                    </h4>

                                    <div id="collapse5" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoi_pp"><p style="{if !empty($gmcxl)}font-weight:bold{/if}"><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời chờ xử lý <span class="pull-right">{$gmcxl}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoiphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời phối hợp chờ xử lý <span class="pull-right">{if !empty($gmph)}{$gmph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>
								
								<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dschidaogm > $chidaodaxemgm}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidaogm - $chidaodaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
												{/if}

											</h4>

									</div>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dsykiengm > $ykiendaxemgm}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykiengm - $ykiendaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
												{/if}

											</h4>

									</div>

                                <div id="accordion">

                                    <h4 class="panel-title">
									
										<a class="tin1" href="{$url}giaymoidaxuly"><p><i class="fa fa-file-text-o" aria-hidden="true"></i> Giấy mời giao phòng đã chỉ đạo<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

                                        <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="tin1">

                                            <p><i class="fa fa-file-text-o"></i> Giấy mời giao phòng đã chỉ đạo <span class="pull-right">{$gmdxl + $gmphdxl}</span></p>

                                        </a>-->

                                    </h4>

                                    <div id="collapse6" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoidaxuly"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng đã chỉ đạo<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

                                            <a class="tin2" href="{$url}giaymoidaxuly_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng phối hợp đã chỉ đạo <span class="pull-right">{if !empty($gmphdxl)}{$gmphdxl}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> Giấy mời đã hoàn thành chờ phê duyệt <span class="pull-right">{($vbduyetgiaymoi)?$vbduyetgiaymoi:''}</span></p></a>
								
                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 8}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="tin1">

                                            {if $gmcxl > 0}
												<b><p><i class="fa fa-newspaper-o"></i> Giấy mời chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl + $gm_phophongph + $gmph}</marquee></b></p>
									
											{else}
												<p><i class="fa fa-newspaper-o"></i> Giấy mời chờ xử lý <span class="pull-right">{($gmcxl + $gm_phophongph + $gmph > 0) ? $gmcxl + $gm_phophongph + $gmph : ''}</span></p>
											{/if}

                                        </a>

                                    </h4>

                                    <div id="collapse5" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoi_cv"><p style="{if $gmcxl > 0}font-weight:bold{/if}"><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời chờ xử lý <span class="pull-right">{$gmcxl}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoi_cvph"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời chờ phối hợp <span class="pull-right">{if !empty($gmph)}{$gmph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>
								
								<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dschidaogm > $chidaodaxemgm}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidaogm - $chidaodaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dschidaogm"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
												{/if}

											</h4>

									</div>
									
									<div id="accordion">

											<h4 class="panel-title">
											
											   {if $dsykiengm > $ykiendaxemgm}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykiengm - $ykiendaxemgm}</marquee></b></p></a>
												{else}
													<a class="tin" href="{$url}dsykiengm"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
												{/if}

											</h4>

									</div>

                                <a class="tin" href="{$url}giaymoidagiaiquyet"><p><i class="fa fa-file-text-o"></i> Giấy mời đã xử lý<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

                                <a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> Giấy mời hoàn thành chờ phê duyệt <span class="pull-right">{($vbduyetgiaymoi)?$vbduyetgiaymoi:''}</span></p></a>
								
                                {/if}

                                <!--                                phó chi cục-->

                                {if $vanban['iQuyenHan_DHNB'] == 10}

                                <a class="tin" href="{$url}giaymoi_ccp"><p><i class="fa fa-file-text-o"></i> Giấy mời chờ xử lý <span class="pull-right">{$gmcxl}</span></p></a>

                                <a class="tin" href="{$url}giaymoidaxuly_ccp"><p><i class="fa fa-file-text-o"></i> Giấy mời đã xử lý <span class="pull-right">{if !empty($gmdxl_pcc)}{$gmdxl_pcc}{else}{/if}</span></p></a>

                                <a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> Giấy mời hoàn thành chờ phê duyệt <span class="pull-right">{$vbduyetgiaymoi}</span></p></a>

                                {/if}

                                <!-- truong phong chi cuc -->

                                {if $vanban['iQuyenHan_DHNB'] == 11}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="tin1">
											
											<p>
                                            {if $gmcxl > 0}
												<i class="fa fa-file-text-o"></i> <b>Giấy mời giao phòng chờ xử lý </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$gmcxl + $gmph}</marquee></b>
											{else}
												<i class="fa fa-file-text-o"></i> Giấy mời giao phòng chờ xử lý <span class="pull-right">{$gmcxl + $gmph}</span>
											{/if}
											</p>

                                        </a>

                                    </h4>

                                    <div id="collapse5" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}giaymoi_tp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng chủ trì chờ xử lý <span class="pull-right">{$gmcxl}</span></p></a>

                                            <a class="tin2" href="{$url}giaymoiphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Giấy mời giao phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($gmph)}{$gmph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin" href="{$url}giaymoidaxuly"><p><i class="fa fa-file-text-o"></i> Giấy mời đã chỉ đạo<!-- <span class="pull-right">{$gmdxl}</span>--></p></a>

                                <a class="tin" href="{$url}giaymoichoduyet"><p><i class="fa fa-check-square-o"></i> Giấy mời hoàn thành chờ phê duyệt <span class="pull-right">{$vbduyetgiaymoi}</span></p></a>

                                <a class="tin" href="{$url}giaymoiphoihopdaxuly"><p><i class="fa fa-file-text-o"></i> Giấy mời phối hợp đã xử lý <span class="pull-right">{if !empty($phdxl)}{$phdxl}{else}{/if}</span></p></a>

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}

                                <a class="tin" href="{$url}duyetbaocao"><p><i class="fa fa-check-square-o"></i> Báo cáo cuộc họp chờ phê duyệt <span class="pull-right">{($bcchoduyet)? $bcchoduyet : ''}</span></p></a>

                                {/if}

                                <a class="tin" href="{$url}dssoanthaodagui"><p><i class="fa fa-share-square-o" aria-hidden="true"></i> Báo cáo kết quả cuộc họp đã gửi đi <span class="pull-right">{($dagui)? $dagui : ''}</span></p></a>

                                {if !empty($daguibituchoi)}<a class="tin" href="{$url}dssoanthaobituchoi"><p><i class="fa fa-ban" aria-hidden="true"></i> Giấy mời đã gửi đi bị từ chối <span class="pull-right">{$daguibituchoi}</span></p></a>{/if}

                                <a class="tin" href="{$url}dsgiaymoiketluanmoi"><p><i class="fa fa-flag"></i> Báo cáo kết quả cuộc họp mới <span class="pull-right">{($chuaxem)? $chuaxem : ''}</span></p></a>

                                <a class="tin" href="{$url}dsgiaymoiketluandaxem"><p><i class="fa fa-flag-o"></i> Báo cáo kết quả cuộc họp đã xem <span class="pull-right">{($daxem) ? $daxem : ''}</span></p></a>
								
								{if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] > 5}
									{if $lichhopphong > 0}
										<b> <a class="tin" href="{$url}lichhopphongct"><p><i class="fa fa-flag-o"></i> Lịch họp phòng chủ trì/ phối hợp <span class="pull-right">{$lichhopphong}</span></p></a> </b>
									{else}
										<a class="tin" href="{$url}lichhopphongct"><p><i class="fa fa-flag-o"></i> Lịch họp phòng chủ trì/ phối hợp</p></a>
									{/if}
								{/if}

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-12"  style="display: inline-flex; min-width: 100%;">

                    <div class="col-md-6" style="min-width: 50%; padding-right: 0px;">

                        <div class="panel panel-info vanbandi" style="border-radius: 10px;">

                            <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;"><img src="{$url}assets/img/icon-vb.png" alt="" style="width: 25px;    margin-top: -5px;"> <b style="font-size: 15px; text-shadow: 1px 1px 2px black;">VĂN BẢN ĐI <a class="pull-right tin"  href="{$url}dsvanban" style="color: white !important;"><i class="fa fa-search"></i> Tìm kiếm </a></b></div>
							
							
                            <div class="panel-body">
								{$slduyet = laysoluongdaduyet()}	
								{if $slduyet[0].count > 0}
									<a href="dsvanbanchoph"><marquee scrolldelay="200"><b style="color:red"><b style="font-size:16px">{$slduyet[0].count}</b> văn bản chờ số đã được duyệt trên hệ thống điện tử, đề nghị các phòng chuyên môn trình lãnh đạo Sở ký duyệt bản gốc để chuyển văn thư cấp số và phát hành văn bản./.</b></marquee></a>
								{/if}								
								
								
								<!--<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $slduyet[0].count > 0}
												<a class="tin" href="{$url}dsvanbanchoph"><p><i class="fa fa-check-square-o"></i> <b> VB đã được duyệt chờ phát hành </b><b class="pull-right"> {$slduyet[0].count} </b></p></a>
											{else}
												<a class="tin" href="{$url}dsvanbanchoph"><p><i class="fa fa-check-square-o"></i> VB đã được duyệt chờ phát hành</p></a>
											{/if}

                                        </h4>

								</div>-->
						
						
								<div id="accordion">

                                        <h4 class="panel-title">
										
                                           {if $dschidaovbdi > $chidaodaxemvbdi}
												<a class="tin" href="{$url}dschidaovbdi"><p><i class="fa fa-check-square-o" style="color:red;"></i> <b style="color:red;"> Chỉ đạo của lãnh đạo Sở </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dschidaovbdi - $chidaodaxemvbdi}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dschidaovbdi"><p><i class="fa fa-check-square-o"></i> Chỉ đạo của lãnh đạo Sở</p></a>
											{/if}

                                        </h4>

								</div>
								
								<div id="accordion">

                                        <h4 class="panel-title">
                                           {if $dsykienvbdi > $ykiendaxemvbdi}
												<a class="tin" href="{$url}dsykienvbdi"><p><i class="fa fa-check-square-o"></i> <b> Danh sách ý kiến, đề xuất </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$dsykienvbdi - $ykiendaxemvbdi}</marquee></b></p></a>
											{else}
												<a class="tin" href="{$url}dsykienvbdi"><p><i class="fa fa-check-square-o"></i> Danh sách ý kiến, đề xuất</p></a>
											{/if}

                                        </h4>

								</div>
								
                                {if $vanban['iQuyenHan_DHNB'] == 8 } <!--Chuyên viên-->

									{if $vanbandichoxuly > 0}
										<b><a class="tin" href="{$url}vanbandichoxuly_cv"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vanbandichoxuly}</marquee></b></p></a>
									
									{else}
										<a class="tin" href="{$url}vanbandichoxuly_cv"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</p></a>
									{/if}

                                    <a class="tin" href="{$url}vanbandiusertao"><p><i class="fa fa-newspaper-o"></i> Văn bản đi cá nhân tạo<!-- <span class="pull-right">{$vanbandiusertao}</span>--></p></a>

                                    <!--<a class="tin" href="{$url}vanbanditrinhky"><p><i class="fa fa-newspaper-o"></i> Văn bản đã được lãnh đạo ký <b class="pull-right">{$vanbanditrinhky}</b></p></a>-->

                                    <a class="tin1" href="{$url}dsvanban?noiduthao={$vanban['FK_iMaPhongHD']}"><p><i class="fa fa-list-alt"></i> Văn bản đi của phòng<!-- <span class="pull-right">{$vanbandi_phong}</span>--></p></a>

                                    <a class="tin1" href="{$url}giaymoiphongban"><p><i class="fa fa-list-alt"></i> Giấy mời đã gửi <!--<span class="pull-right">{$giaymoidi_phong}</span>--></p></a>
									{if date('Y') == $year}							
										<a class="tin" href="{$url}vanban"><p><i class="fa fa-edit"></i> Tạo văn bản đi</p></a>
									{/if}

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 7} <!-- Phó phòng-->
									
									{if $vanbandichoxuly > 0}
										<b><a class="tin" href="{$url}vanbandichoxuly_pp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vanbandichoxuly}</marquee></b></p></a>
									
									{else}
										<a class="tin" href="{$url}vanbandichoxuly_pp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</p></a>
									{/if}
									

                                    <a class="tin" href="{$url}vanbandidaxuly_pp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã xử lý<!-- <span class="pull-right">{$vanbandidaxuly}</span>--></p></a>

                                    <a class="tin" href="{$url}vanbandiusertao"><p><i class="fa fa-newspaper-o"></i> Văn bản đi cá nhân tạo<!-- <span class="pull-right">{$vanbandiusertao}</span>--></p></a>

                                    <a class="tin1" href="{$url}dsvanban?noiduthao={$vanban['FK_iMaPhongHD']}"><p><i class="fa fa-list-alt"></i> Văn bản đi của phòng<!-- <span class="pull-right">{$vanbandi_phong}</span>--></p></a>
									
									{if $vanban['PK_iMaCB'] == 697 || $vanban['PK_iMaCB'] == 698 || $vanban['PK_iMaCB'] == 710}
									 
										{if $daphchuaduyet > 0}
											<b><a class="tin" href="{$url}vbdidaphchuaduyet"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã phát hành chưa được duyệt</b><b class="pull-right">{$daphchuaduyet}</b></p></a>
										
										{else}
											<a class="tin" href="{$url}vbdidaphchuaduyet"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã phát hành chưa được duyệt</p></a>
										{/if}
									
									{/if}

                                    <a class="tin1" href="{$url}giaymoiphongban"><p><i class="fa fa-list-alt"></i> Giấy mời đã gửi <!--<span class="pull-right">{$giaymoidi_phong}</span>--></p></a>

                                    {if date('Y') == $year}							
										<a class="tin" href="{$url}vanban"><p><i class="fa fa-edit"></i> Tạo văn bản đi</p></a>
									{/if}

                                {/if}

                                {if ($vanban['iQuyenHan_DHNB'] == 6 && ($vanban['FK_iMaCV']==3||$vanban['FK_iMaCV']==7)) || ($vanban['iQuyenHan_DHNB'] == 3 && $vanban['FK_iMaCV']==6) || $vanban['iQuyenHan_DHNB'] == 11} <!-- trưởng phòng -->
									
									{if $vanbandichoxuly > 0}
										<b><a class="tin" href="{$url}vanbandichoxuly_tp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vanbandichoxuly}</marquee></b></p></a>
									
									{else}
										<a class="tin" href="{$url}vanbandichoxuly_tp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</p></a>
									{/if}
									

                                    <a class="tin" href="{$url}vanbandidaxuly_tp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã xử lý<!-- <span class="pull-right">{$vanbandidaxuly}</span>--></p></a>
									
									<a class="tin" href="{$url}vanbandiusertao"><p><i class="fa fa-newspaper-o"></i> Văn bản đi cá nhân tạo<!-- <span class="pull-right">{$vanbandiusertao}</span>--></p></a>

                                    <a class="tin1" href="{$url}dsvanban?noiduthao={$vanban['FK_iMaPhongHD']}"><p><i class="fa fa-list-alt"></i> Văn bản đi của phòng<!-- <span class="pull-right">{$vanbandi_phong}</span>--></p></a>
									
									{if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['PK_iMaCB']== 709}
									 
										{if $daphchuaduyet > 0}
											<b><a class="tin" href="{$url}vbdidaphchuaduyet"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã phát hành chưa được duyệt</b><b class="pull-right">{$daphchuaduyet}</b></p></a>
										
										{else}
											<a class="tin" href="{$url}vbdidaphchuaduyet"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã phát hành chưa được duyệt</p></a>
										{/if}
									
									{/if}

                                    <a class="tin1" href="{$url}giaymoiphongban"><p><i class="fa fa-list-alt"></i> Giấy mời đã gửi <!--<span class="pull-right">{$giaymoidi_phong}</span>--></p></a>

                                    {if date('Y') == $year}							
										<a class="tin" href="{$url}vanban"><p><i class="fa fa-edit"></i> Tạo văn bản đi</p></a>
									{/if}

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5} <!-- chi cục trưởng-->

									{if $vanbandichoxuly > 0}
										<b><a class="tin" href="{$url}vanbandichoxuly_ld"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$vanbandichoxuly}</marquee></b></p></a>
									
									{else}
										<a class="tin" href="{$url}vanbandichoxuly_ld"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</p></a>
									{/if}
									

                                    <a class="tin" href="{$url}vanbandild_xem"><p><i class="fa fa-newspaper-o"></i> Văn bản lãnh đạo xem để biết:<span class="pull-right">{$vanbandi_xem}</span></p></a>
									
									{if $daphchuaduyet > 0}
										<b><a class="tin" href="{$url}vbdidaphchuaduyet"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã phát hành chưa được duyệt</b><b class="pull-right">{$daphchuaduyet}</b></p></a>
									
									{else}
										<a class="tin" href="{$url}vbdidaphchuaduyet"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã phát hành chưa được duyệt</p></a>
									{/if}


                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV']==15} <!-- Phó phòng-->

                                    <b><a class="tin" href="{$url}vanbandichoxuly_cct"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</b><span class="pull-right">{$vanbandichoxuly}</span></p></a></b>

                                    <a class="tin" href="{$url}vanbandidaxuly_cct"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã xử lý<!-- <span class="pull-right">{$vanbandidaxuly}</span>--></p></a>

                                    <a class="tin1" href="{$url}dsvanban?noiduthao={$vanban['FK_iMaPhongHD']}"><p><i class="fa fa-list-alt"></i> Văn bản đi của phòng<!-- <span class="pull-right">{$vanbandi_phong}</span>--></p></a>

                                    <a class="tin1" href="{$url}giaymoiphongban"><p><i class="fa fa-list-alt"></i> Giấy mời đã gửi <!--<span class="pull-right">{$giaymoidi_phong}</span>--></p></a>

                                    {if date('Y') == $year}							
										<a class="tin" href="{$url}vanban"><p><i class="fa fa-edit"></i> Tạo văn bản đi</p></a>
									{/if}

                                {/if}

                                {if $vanban['iQuyenHan_DHNB'] == 10 && $vanban['FK_iMaCV']==16} <!--chi cục phó-->

                                    <b><a class="tin" href="{$url}vanbandichoxuly_ccp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi chờ xử lý</b><span class="pull-right">{$vanbandichoxuly}</span></p></a></b>

                                    <a class="tin" href="{$url}vanbandidaxuly_ccp"><p><i class="fa fa-newspaper-o"></i> Văn bản đi đã xử lý<!-- <span class="pull-right">{$vanbandidaxuly}</span>--></p></a>

                                    <a class="tin1" href="{$url}dsvanban?noiduthao={$vanban['FK_iMaPhongHD']}"><p><i class="fa fa-list-alt"></i> Văn bản đi của phòng<!-- <span class="pull-right">{$vanbandi_phong}</span>--></p></a>

                                    <a class="tin1" href="{$url}giaymoiphongban"><p><i class="fa fa-list-alt"></i> Giấy mời đã gửi <!--<span class="pull-right">{$giaymoidi_phong}</span>--></p></a>

                                    {if date('Y') == $year}							
										<a class="tin" href="{$url}vanban"><p><i class="fa fa-edit"></i> Tạo văn bản đi</p></a>
									{/if}

                                {/if}

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6" style="min-width: 50%;">

                        <div class="panel panel-info thongtinnoibo" style="border-radius: 10px;">

                            <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;"><img src="{$url}assets/img/icon-vb.png" alt="" style="width: 25px;    margin-top: -5px;"> <b style="font-size: 15px; text-shadow: 1px 1px 2px black;">THÔNG TIN NỘI BỘ - NGHỈ PHÉP</b></div>
							
							
							
	

                            <div class="panel-body">
							
							 <div class="row">

								<marquee onmouseover="this.stop();" onmouseout="this.start();" style="white-space: nowrap; overflow: hidden; overflow-x:-webkit-marquee; color:blue; font-size: 13px" behavior="scroll" scrollamount="4" direction="left">
								{$i=3}
									{foreach $thongbao as $tb}
										{if date('Y-m-d H:i:s') <= $tb.sNgayKetThuc && date('Y-m-d H:i:s') >= $tb.sNgayBatDau && $tb.iTips == 1}
											<a class="tin1" href="javascript:void(0)" data-toggle="modal" data-target="#myModa{$i}"><i style="color:blue">{$tb.sTieuDe}</i></a> | 
											<div class="modal fade bs-example-modal-lg" id="myModa{$i++}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">	
														<div class="panel panel-info">
															<div class="panel-heading" style="font-size: 16px;"><b style="color:blue">Tips</b></div>
															<div class="panel-body show-noidung" style="font-size: 16px;">{$tb.sNoiDung}</div>
															<!--<div class="panel-footer text-center" style="cursor:pointer; width:10%"  data-dismiss="modal" data-dismiss="modal">Đóng</div>-->
														</div>
													</div>
												</div>
											</div>		
										{/if}
									{/foreach}
								</marquee>		
							
							</div>
							
							
								{if $tinmoi > 0}
									<a class="tin" href="{$url}thongtinmoi"><p><i class="fa fa-info-circle" style="color:red;"></i> <b style="color:red; font-size: 18px">Thông tin mới </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$tinmoi}</marquee></b></p></a>
								{else}
									<a class="tin" href="{$url}thongtinmoi"><p><i class="fa fa-info-circle"></i> Thông tin mới</p></a>
								{/if}

                                <a class="tin" href="{$url}thongtinguidi"><p><i class="fa fa-info-circle"></i> Thông tin đã gửi <!--<span class="pull-right">{$tindagui}</span>--></p></a>

                                <a class="tin" href="{$url}thongtindaxem"><p><i class="fa fa-info-circle"></i> Thông tin đã xem <!-- <span class="pull-right">{$tindaxem}</span>--></p></a>

                                
								{if date('Y') == $year}
									<a class="tin" href="{$url}soanthaothongtin"><p><i class="fa fa-edit"></i> Soạn thảo thông tin</p></a>
								{/if}
								<hr>
								<a class="tin" href="{$url}dsfilechung"><p><i class="fa fa-newspaper-o"></i> Tài liệu, biểu mẫu</p></a>
								<hr>
								
								{if $vanban['iQuyenHan_DHNB'] < 8}
									{if $phepchoduyet > 0}
										<a class="tin" href="{$url}duyetnghiphep"><p><i class="fa fa-newspaper-o"></i> <b style="color:red; font-size: 16px">Nghỉ phép chờ phê duyệt </b><b class="pull-right"><marquee direction="down" height="20" scrolldelay="500" behavior="alternate">{$phepchoduyet}</marquee></b></p></a>
									{else}
										<a class="tin" href="{$url}duyetnghiphep"><p><i class="fa fa-newspaper-o"></i> Nghỉ phép chờ phê duyệt</p></a>
									{/if}
								{/if}
								
								{if $nghiphepmoi > 0}
									<a class="tin" href="{$url}dsnghiphep"><p><i class="fa fa-newspaper-o"></i> <b>Danh sách cán bộ nghỉ phép </b><b class="pull-right">{$dsnghiphep}</b></p></a>
								{else}
									<a class="tin" href="{$url}dsnghiphep"><p><i class="fa fa-newspaper-o"></i> Danh sách cán bộ nghỉ phép <span class="pull-right">{($dsnghiphep > 0) ? $dsnghiphep : ''}</span></p></a>
								{/if}
								{if $vanban['iQuyenHan_DHNB'] != 4 && $vanban['iQuyenHan_DHNB'] != 5}
									<a class="tin" href="{$url}dsnghiphep?id={$vanban['PK_iMaCB']}"><p><i class="fa fa-newspaper-o"></i> Danh sách nghỉ phép cá nhân <span class="pull-right">{($nghiphepcb)?$nghiphepcb:''}</span></p></a>
								{/if}
								{if $vanban['iQuyenHan_DHNB'] != 4 && $vanban['iQuyenHan_DHNB'] != 5 && date('Y') == $year}
									<a class="tin" href="{$url}xinnghiphep"><p><i class="fa fa-edit"></i> Tạo phê duyệt xin nghỉ phép</p></a>
								{/if}

                            </div>

                        </div>

                    </div>

                </div>



                <!--- QUẢN LÝ ĐẦU VIỆC -->

                <div class="col-md-12"  style="display: inline-flex; min-width: 100%;">

                    <div class="col-md-6" style="padding-right: 0px; min-width: 50%;">

                        <div class="panel panel-info kehoach" style="border-radius: 10px;">

                            <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;"><img src="{$url}assets/img/icon-vb.png" alt="" style="width: 25px;    margin-top: -5px;"> <b style="font-size: 15px; text-shadow: 1px 1px 2px black;"> QUẢN LÝ ĐẦU VIỆC NỘI BỘ SỞ</b></div>
							
							<br>
							 <label for="" class="col-sm-4 control-label"><a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><b style="color:blue">Xem chi tiết</b></a></label><br><br>
							 
                            <div class="panel-body collapse" id="collapseExample">

                                {if $trangthai_tk==1 || $trangthai_tk==2}

                                {if $trangthai_tk==1}

                                <a class="tin1" href="{$url}dauviecchoxuly_gd"><p><i class="fa fa-newspaper-o"></i> Đầu việc chờ xử lý <span class="pull-right">{$dauvieclanhdaochoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxuly_gd"><p><i class="fa fa-newspaper-o"></i> Đầu việc đã xử lý (Chưa hoàn thành)<span class="pull-right">{$dauvieclanhdaodaxuly}</span></p></a>

                                {/if}

                                {if $trangthai_tk==2}

                                <a class="tin1" href="{$url}dauviecchoxuly_pgd"><p><i class="fa fa-newspaper-o"></i> Đầu việc chờ xử lý <span class="pull-right">{$dauvieclanhdaochoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxuly_pgd"><p><i class="fa fa-newspaper-o"></i> Đầu việc đã xử lý (Chưa hoàn thành)<span class="pull-right">{$dauvieclanhdaodaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dexuatrahan_bgd"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn chờ xử lý <span class="pull-right">{$giahanchoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dadexuatrahan_bgd"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn đã xử lý <span class="pull-right">{$giahandaxuly}</span></p></a>

                                {/if}

                                <a class="tin1" href="{$url}dsdauviecvanban"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc của Sở</b></p></a>

                                 <!-- <a class="tin1" href="{$url}danhsachtailieu"><p><i class="fa fa-newspaper-o"></i> Danh sách tài liệu họp HĐND của tổng hợp</b></p></a>

                                <a class="tin1" href="{$url}danhsachtailieuphong"><p><i class="fa fa-newspaper-o"></i> Danh sách tài liệu họp HĐND của phòng</b></p></a>

                                <a class="tin1" href="{$url}danhsachkiennghi"><p><i class="fa fa-newspaper-o"></i> Danh sách kiến nghị HĐND</b></p></a>-->

                                {/if}

                                {if $trangthai_tk==55}

                                <a class="tin1" href="{$url}dsdauviecvanban"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc của Sở</p></a>

                                 <!-- <a class="tin1" href="{$url}danhsachtailieu"><p><i class="fa fa-newspaper-o"></i> Danh sách tài liệu họp HĐND của tổng hợp</b></p></a>

                                <a class="tin1" href="{$url}danhsachtailieuphong"><p><i class="fa fa-newspaper-o"></i> Danh sách tài liệu họp HĐND của phòng</b></p></a>

                                <a class="tin1" href="{$url}danhsachkiennghi"><p><i class="fa fa-newspaper-o"></i> Danh sách kiến nghị HĐND</b></p></a>-->

                                <a class="tin1" href="{$url}duyetketqua"><p><i class="fa fa-newspaper-o"></i> Đầu việc hoàn thành chờ lãnh đạo phòng duyệt <span class="pull-right">{$duyetketqua}</span></p></a>

                                <a class="tin" href="{$url}addqldv"><p><i class="fa fa-newspaper-o"></i> Nhập mới</p></a>

                                {/if}

                                {if $trangthai_tk==3}

                                <a class="tin1" href="{$url}danhsachquahanphong"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc quá hạn của phòng <span class="pull-right">{$dauviecquahan}</span></p></a>

                                <a class="tin1" href="{$url}dexuatrahan_cct"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn chờ xử lý <span class="pull-right">{$giahanchoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dadexuatrahan_cct"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn đã xử lý <span class="pull-right">{$giahandaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxuly_cct"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng chủ trì chờ xử lý <span class="pull-right">{$dauviecchutrichoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxulyph_cct"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng phối hợp chờ xử lý<span class="pull-right">{if isset($dauviecphoihopchoxuly_phph)}{$dauviecphoihopchoxuly_phph}{else}{/if}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxuly_cct"><p><i class="fa fa-newspaper-o"></i> DS đầu việc phòng chủ trì đã xử lý<span class="pull-right">{$dauviecchutridaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxulyph_cct"><p><i class="fa fa-newspaper-o"></i> DS đầu việc phòng phối hợp đã xử lý<span class="pull-right">{$dauviecphoihopdaxuly_phph}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongchutri"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng chủ trì<span class="pull-right">{$dauviecphongchutri}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongphoihop"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng phối hợp<span class="pull-right">{$dauviecphongphoihop}</span></p></a>

                                <a class="tin1" href="{$url}duyetketqua"><p><i class="fa fa-newspaper-o"></i> Đầu việc hoàn thành chờ chi cục trưởng duyệt <span class="pull-right">{$duyetketqua}</span></p></a>

                                <a class="tin1 hide" href="{$url}dauviecchoduyet"><p><i class="fa fa-newspaper-o"></i> Đầu việc chờ phê duyệt gửi lãnh đạo Sở <span class="pull-right">{$dauviecchoduyet}</span></p></a>

                                {/if}

                                {if $trangthai_tk==4}

                                <a class="tin1" href="{$url}danhsachquahanphong"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc quá hạn của phòng <span class="pull-right">{$dauviecquahan}</span></p></a>

                                <a class="tin1" href="{$url}dexuatrahan_ccp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn chờ xử lý <span class="pull-right">{$giahanchoxuly}</span></p></a>

                                    <a class="tin1" href="{$url}dadexuatrahan_ccp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn đã xử lý <span class="pull-right">{$giahandaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxuly_ccp"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng chủ trì chờ xử lý <span class="pull-right">{$dauviecchutrichoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxulyph_ccp"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng phối hợp chờ xử lý<span class="pull-right">{if isset($dauviecphoihopchoxuly_phph)}{$dauviecphoihopchoxuly_phph}{else}{/if}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxuly_ccp"><p><i class="fa fa-newspaper-o"></i> DS đầu việc phòng chủ trì đã xử lý<span class="pull-right">{$dauviecchutridaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxulyph_ccp"><p><i class="fa fa-newspaper-o"></i> DS đầu việc phòng phối hợp đã xử lý<span class="pull-right">{$dauviecchutridaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongchutri"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng chủ trì<span class="pull-right">{$dauviecphongchutri}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongphoihop"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng phối hợp<b class="pull-right">{$dauviecphongphoihop}</span></p></a>

                                {/if}

                                {if $trangthai_tk==5}

                                <a class="tin1" href="{$url}danhsachquahanphong"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc quá hạn của phòng <span class="pull-right">{$dauviecquahan}</span></p></a>

                                <a class="tin1" href="{$url}dexuatrahan_tp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn chờ xử lý <span class="pull-right">{$giahanchoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dadexuatrahan_tp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn đã xử lý <span class="pull-right">{$giahandaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxuly_tp"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng chủ trì chờ xử lý <span class="pull-right">{$dauviecchutrichoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxulyph_tp"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng phối hợp chờ xử lý<span class="pull-right">{$dauviecphoihopchoxuly_tp}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxuly_tp"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng chủ trì đã chỉ đạo (Chưa hoàn thành)<span class="pull-right">{$dauviecchutridaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxulyph_tp"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng phối hợp đã chỉ đạo  (Chưa hoàn thành)<span class="pull-right">{$dauviecphoihopdaxuly_tp}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongchutri"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng chủ trì<span class="pull-right">{$dauviecphongchutri}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongphoihop"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng phối hợp<span class="pull-right">{$dauviecphongphoihop}</span></p></a>

                                <a class="tin1" href="{$url}duyetketqua"><p><i class="fa fa-newspaper-o"></i> Đầu việc hoàn thành chờ lãnh đạo phòng duyệt <span class="pull-right">{$duyetketqua}</span></p></a>

                                <a class="tin1 hide" href="{$url}dauviecchoduyet"><p><i class="fa fa-newspaper-o"></i> Đầu việc chờ phê duyệt gửi lãnh đạo Sở <span class="pull-right">{$dauviecchoduyet}</span></p></a>

                                {/if}

                                {if $trangthai_tk==555}

                                <a class="tin1" href="{$url}danhsachquahanphong"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc quá hạn của phòng <span class="pull-right">{$dauviecquahan}</span></p></a>

                                <a class="tin1" href="{$url}dexuatrahan_tp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn chờ xử lý <span class="pull-right">{$giahanchoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dadexuatrahan_tp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn đã xử lý <span class="pull-right">{$giahandaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxuly_tp_cc"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng chủ trì chờ xử lý <span class="pull-right">{$dauviecchutrichoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxulyph_tp_cc"><p><i class="fa fa-newspaper-o"></i> Đầu việc phòng phối hợp chờ xử lý<span class="pull-right">{$dauviecphoihopchoxuly_tp}</span></p></a>

                                <a class="tin1" href="{$url}dauviecchoxulyphct_tp_cc"><p><i class="fa fa-newspaper-o"></i> Đầu việc trưởng phòng phối hợp (Phòng CT)<span class="pull-right">{$dauviecchutriphchoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxuly_tp_cc"><p><i class="fa fa-newspaper-o"></i> DS phòng chủ trì đã chỉ đạo (Chưa hoàn thành)<span class="pull-right">{$dauviecchutridaxuly}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxulyph_tp_cc"><p><i class="fa fa-newspaper-o"></i> DS phòng phối hợp đã chỉ đạo<span class="pull-right">{$dauviecphoihopdaxuly_phph}</span></p></a>

                                <a class="tin1" href="{$url}dauviecdaxulyphct_tp_cc"><p><i class="fa fa-newspaper-o"></i> DS đầu việc đã chỉ đạo trưởng phòng phối hợp<span class="pull-right">{$dauviecphoihopdaxuly_tpph}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongchutri"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng chủ trì<span class="pull-right">{$dauviecphongchutri}</span></p></a>

                                <a class="tin1" href="{$url}dsdauviecphongphoihop"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc phòng phối hợp<span class="pull-right">{$dauviecphongphoihop}</span></p></a>

                                <a class="tin1" href="{$url}duyetketqua"><p><i class="fa fa-newspaper-o"></i> Đầu việc hoàn thành chờ duyệt <span class="pull-right">{$duyetketqua}</span></p></a>

                                {/if}

                                {if $trangthai_tk==6}

                                <a class="tin1" href="{$url}danhsachquahanphong"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc quá hạn của phòng <span class="pull-right">{$dauviecquahan}</span></p></a>

                                <a class="tin1" href="{$url}dexuatrahan_pp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn chờ xử lý <span class="pull-right">{$giahanchoxuly}</span></p></a>

                                <a class="tin1" href="{$url}dadexuatrahan_pp"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn đã xử lý <span class="pull-right">{$giahandaxuly}</span></p></a>

                                {if $vanban['FK_iMaPhongHD']==12}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv1" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> DS đầu việc giao phòng xử lý <span class="pull-right">{$dauviecchutrichoxuly + $dauviecphoihopchoxuly + $dauviecchutriphchoxuly + $dauviecchoxulyctphct_pp_cc + $dauviecchoxulyctphph_pp_cc}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv1" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dauviecchoxuly_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng chủ trì chờ xử lý <span class="pull-right">{$dauviecchutrichoxuly}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyph_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($dauviecphoihopchoxuly_tp)}{$dauviecphoihopchoxuly_tp}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyphct_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng chủ trì chờ xử lý (PP PH) <span class="pull-right">{if !empty($dauviecchutriphchoxuly)}{$dauviecchutriphchoxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyctphct_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp chờ xử lý (PP CT) <span class="pull-right">{if !empty($dauviecchoxulyctphct_pp_cc)}{$dauviecchoxulyctphct_pp_cc}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyctphph_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp chờ xử lý (PP PH) <span class="pull-right">{if !empty($dauviecchoxulyctphph_pp_cc)}{$dauviecchoxulyctphph_pp_cc}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv11" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> DS đầu việc đã chỉ đạo <span class="pull-right">{$dauviecchutridaxuly+ $dauviecdaxulyctphct_pp_cc + $dauviecdaxulyctphph_pp_cc}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv11" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dauviecdaxuly_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng chủ trì đã xử lý <span class="pull-right">{$dauviecchutridaxuly}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecdaxulyph_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp đã xử lý <span class="pull-right">{if !empty($dauviecphoihopchoxuly)}{$dauviecphoihopchoxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecdaxulyphct_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng chủ trì đã xử lý (PP PH) <span class="pull-right">{if !empty($dauviecchutriphchoxuly)}{$dauviecchutriphchoxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecdaxulyctphct_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp đã xử lý (PP CT) <span class="pull-right">{if !empty($dauviecdaxulyctphct_pp_cc)}{$dauviecdaxulyctphct_pp_cc}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecdaxulyctphph_pp_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp đã xử lý (PP PH) <span class="pull-right">{if !empty($dauviecdaxulyctphph_pp_cc)}{$dauviecdaxulyctphph_pp_cc}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv2" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số đầu việc giao phòng xử lý <span class="pull-right">{$dauviecphongchutri + $dauviecphongphoihop}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv2" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dsdauviecphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Số lượng đầu việc phòng chủ trì<span class="pull-right">{$dauviecphongchutri}</span></p></a>

                                            <a class="tin2" href="{$url}dsdauviecphongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Số lượng đầu việc phòng phối hợp<span class="pull-right">{$dauviecphongphoihop}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin1" href="{$url}duyetketqua"><p><i class="fa fa-newspaper-o"></i> Đầu việc hoàn thành chờ phó phòng duyệt <span class="pull-right">{$duyetketqua}</span></p></a>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv3" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Đầu việc bị trả lại <span class="pull-right">{$dauviectralai}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv3" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2 hide" href="{$url}dauviectralai"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc nhập mới bị trả lại<span class="pull-right">{$dauviectralai}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoduyet"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc hoàn thành bị trả lại</p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin hide" href="{$url}addqldv"><p><i class="fa fa-newspaper-o"></i> Nhập mới</p></a>

                                {else}

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv1" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> DS đầu việc giao phòng xử lý <span class="pull-right">{$dauviecchutrichoxuly + $dauviecphoihopchoxuly + $dauviecchutriphchoxuly+$dauviecphoihopchoxuly_phph}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv1" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dauviecchoxuly_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng chủ trì chờ xử lý <span class="pull-right">{$dauviecchutrichoxuly}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyph_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($dauviecphoihopchoxuly)}{$dauviecphoihopchoxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyphct_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Đầu việc phòng chủ trì chờ xử lý (Phó phòng PH) <span class="pull-right">{if !empty($dauviecchutriphchoxuly)}{$dauviecchutriphchoxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyphph_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i>  Đầu việc phòng phối hợp chờ xử lý (Phó phòng PH) <span class="pull-right">{if !empty($dauviecphoihopchoxuly_phph)}{$dauviecphoihopchoxuly_phph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv11" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> DS đầu việc đã chỉ đạo <span class="pull-right">{$dauviecchutridaxuly + $dauviecphoihopdaxuly_ph + $dauviecchutriphdaxuly+$dauviecphoihopdaxuly_phph}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv11" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dauviecdaxuly_pp"><p><i class="fa fa-caret-right"></i> DS đầu việc phòng chủ trì đã xử lý <span class="pull-right">{$dauviecchutridaxuly}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecdaxulyph_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> DS đầu việc phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($dauviecphoihopdaxuly_ph)}{$dauviecphoihopdaxuly_ph}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecdaxulyphct_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> DS đầu việc phòng chủ trì đã xử lý  (Phó phòng PH) <span class="pull-right">{if !empty($dauviecchutriphdaxuly)}{$dauviecchutriphdaxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecdaxulyphph_pp"><p><i class="fa fa-caret-right" aria-hidden="true"></i> DS đầu việc phòng phối hợp chờ xử lý  (Phó phòng PH) <span class="pull-right">{if !empty($dauviecphoihopdaxuly_phph)}{$dauviecphoihopdaxuly_phph}{else}{/if}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv2" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số đầu việc giao phòng xử lý <span class="pull-right">{$dauviecphongchutri + $dauviecphongphoihop}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv2" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dsdauviecphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Số lượng đầu việc phòng chủ trì<span class="pull-right">{$dauviecphongchutri}</span></p></a>

                                            <a class="tin2" href="{$url}dsdauviecphongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Số lượng đầu việc phòng phối hợp<span class="pull-right">{$dauviecphongphoihop}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin1" href="{$url}duyetketqua"><p><i class="fa fa-newspaper-o"></i> Đầu việc hoàn thành chờ phó phòng duyệt <span class="pull-right">{$duyetketqua}</span></p></a>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv3" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Đầu việc bị trả lại <span class="pull-right">{$dauviectralai}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv3" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2 hide" href="{$url}dauviectralai"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc nhập mới bị trả lại<span class="pull-right">{$dauviectralai}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoduyet"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc hoàn thành bị trả lại</p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin hide" href="{$url}addqldv"><p><i class="fa fa-newspaper-o"></i> Nhập mới</p></a>

                                {/if}

                                {/if}

                                {if $trangthai_tk==7}

                                <a class="tin1" href="{$url}danhsachquahanphong"><p><i class="fa fa-newspaper-o"></i> Danh sách đầu việc quá hạn của phòng <span class="pull-right">{$dauviecquahan}</span></p></a>

                                <a class="tin1" href="{$url}dexuatrahan_cv"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn chờ duyệt <span class="pull-right">{$giahanchoduyet}</span></p></a>

                                    <a class="tin1" href="{$url}dadexuatrahan_cv"><p><i class="fa fa-newspaper-o"></i> DSĐV đề xuất gia hạn đã duyệt <span class="pull-right">{$giahandaduyet}</span></p></a>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv1" class="tin1">

                                            <p><i class="fa fa-newspaper-o"></i> DS đầu việc giao phòng xử lý <span class="pull-right">{$dauviecchutrichoxuly + $dauviecphoihopchoxuly + $dauviecchutriphchoxuly + $dauviecphoihopchoxuly_cvph + $dauviecchoxulyctphct_pp_cc + $dauviecchoxulyctphph_pp_cc}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv1" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dauviecchoxuly_cv"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng chủ trì chờ xử lý <span class="pull-right">{$dauviecchutrichoxuly}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyph_cv"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp chờ xử lý <span class="pull-right">{if !empty($dauviecphoihopchoxuly)}{$dauviecphoihopchoxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyphct_cv"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc chuyên viên phối hợp (Phòng CT) <span class="pull-right">{if !empty($dauviecchutriphchoxuly)}{$dauviecchutriphchoxuly}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyphph_cv"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc phòng phối hợp chờ xử lý(CV PH) <span class="pull-right">{if !empty($dauviecphoihopchoxuly_cvph)}{$dauviecphoihopchoxuly_cvph}{else}{/if}</span></p></a>

                                            {if $vanban['FK_iMaPhongHD']==12}

                                            <a class="tin2" href="{$url}dauviecchoxulyctphct_cv_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc chuyên viên chủ trì chờ xử lý(Phòng PH) <span class="pull-right">{if !empty($dauviecchoxulyctphct_pp_cc)}{$dauviecchoxulyctphct_pp_cc}{else}{/if}</span></p></a>

                                            <a class="tin2" href="{$url}dauviecchoxulyctphph_cv_cc"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc chuyên viên phối hợp chờ xử lý(Phòng PH) <span class="pull-right">{if !empty($dauviecchoxulyctphph_pp_cc)}{$dauviecchoxulyctphph_pp_cc}{else}{/if}</span></p></a>

                                            {/if}

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv2" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Tổng số đầu việc giao phòng xử lý <span class="pull-right">{$dauviecphongchutri + $dauviecphongphoihop}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv2" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2" href="{$url}dsdauviecphongchutri"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Số lượng đầu việc phòng chủ trì<span class="pull-right">{$dauviecphongchutri}</span></p></a>

                                            <a class="tin2" href="{$url}dsdauviecphongphoihop"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Số lượng đầu việc phòng phối hợp<span class="pull-right">{$dauviecphongphoihop}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <div id="accordion">

                                    <h4 class="panel-title">

                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsedv3" class="tin1">

                                            <p><i class="fa fa-list-alt"></i> Đầu việc bị trả lại <span class="pull-right">{$dauviectralai + $duyetketqua}</span></p>

                                        </a>

                                    </h4>

                                    <div id="collapsedv3" class="panel-collapse collapse">

                                        <div class="panel-body padding-box">

                                            <a class="tin2 hide" href="{$url}dauviectralai"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc nhập mới bị trả lại<span class="pull-right">{$dauviectralai}</span></p></a>

                                            <a class="tin2" href="{$url}duyetketqua"><p><i class="fa fa-caret-right" aria-hidden="true"></i> Đầu việc hoàn thành bị trả lại<span class="pull-right">{$duyetketqua}</span></p></a>

                                        </div>

                                    </div>

                                </div>

                                <a class="tin hide" href="{$url}addqldv"><p><i class="fa fa-newspaper-o"></i> Nhập mới</p></a>

                                {/if}

                                </p></a>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6" style="min-width: 50%;">

                        <div class="panel panel-info congtac" style="border-radius: 10px;">

                            <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;"><img src="{$url}assets/img/icon-vb.png" alt="" style="width: 25px;    margin-top: -5px;"> <b style="font-size: 15px; text-shadow: 1px 1px 2px black;">KẾ HOẠCH CÔNG TÁC - ĐÁNH GIÁ CBCC</b></div>
							
							<br>
							 <label for="" class="col-sm-4 control-label"><a data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2"><b style="color:blue">Xem chi tiết</b></a></label><br><br>

                            <div class="panel-body collapse" id="collapseExample2">
                                {if  $vanban['iQuyenHan_DHNB'] == 8}
                                <a class="tin" href="{$url}thongkeVBDtheocanbo"><p ><i class="fa fa-newspaper-o"></i> Thống kê công việc hoàn thành theo cá nhân</p></a>
                                {/if}

                                {if  $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 8 || $vanban['iQuyenHan_DHNB'] == 10 || $vanban['iQuyenHan_DHNB'] == 11}

                                <a class="tin" href="{$url}kehoachcongtac"><p><i class="fa fa-newspaper-o"></i> Nhập, giao, nhận xét kế hoạch-kết quả công việc tuần

                                        <span class="pull-right">{$tong_sl_cv}</span></p></a>

                                <a class="tin" href="{$url}xemkehoachcongtac/{$tuan}"><p><i class="fa fa-newspaper-o"></i> Tự đánh giá kết quả công tác tuần của cá nhân

                                    </p></a>

                                



                                <!-- <a class="tin" href="{$url}xemkehoachcongtacthang/{$taikhoan}/{$thang}"><p><i class="fa fa-newspaper-o"></i> Tự đánh giá kết quả công tác tháng của cá nhân</p></a> -->

                                <a class="tin" href="{$url}xemkehoachcongtacthang/{$taikhoan}"><p><i class="fa fa-newspaper-o"></i> Tự đánh giá kết quả công tác tháng của cá nhân</p></a>



                                <a class="tin" href="{$url}dskehoachcongtac/{$tuan}"><p><i class="fa fa-newspaper-o"></i> Xem, nhận xét kết quả công tác tuần của chuyên viên

                                    </p></a> 



                                <!-- <a class="tin" href="{$url}dskehoachcongtac/52"><p><i class="fa fa-newspaper-o"></i> Xem, nhận xét kết quả công tác tuần của chuyên viên

                                    </p></a> -->



                                <a class="tin" href="{$url}dskehoachcongtacthang"><p><i class="fa fa-newspaper-o"></i> Xem kết quả phân loại tháng của phòng</p></a>

                                <a class="tin hide" href="{$url}dskehoachcongtacquy"><p><i class="fa fa-newspaper-o"></i> Xem kết quả phân loại quý của phòng</p></a>



                                <a class="tin" href="{$url}dskehoachcongtactuanld"><p><i class="fa fa-newspaper-o"></i> Xem kết quả công tác tuần của các phòng trong toàn Sở</p></a>



                                <a class="tin" href="{$url}dskehoachcongtacthangtp"><p><i class="fa fa-newspaper-o"></i> Xem kết quả công tác tháng của toàn Sở</p></a>



                                <a class="tin" href="{$url}thongkebaocao"><p><i class="fa fa-newspaper-o"></i> Thống kê kết quả công tác tuần trong toàn Sở</p></a>



                                <a class="tin" href="{$url}thongkebaocaothang"><p><i class="fa fa-newspaper-o"></i> Thống kê kết quả công tác tháng trong toàn Sở</p></a>  



                                {if  $vanban['PK_iMaCB'] == 450}

                                <a class="tin" href="{$url}lanhdaonhanxetthang"><p><i class="fa fa-newspaper-o"></i> Đánh giá, phê duyệt kết quả  phân loại tháng của Sở </p></a>

                                {/if}

                                <a class="tin" href="{$url}dssangtao"><p><i class="fa fa-newspaper-o"></i> Đề xuất sáng tạo, đổi mới trong tháng</p></a>

                                {/if}



                                {if  $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}

                                <a class="tin hide" href="{$url}trinhlanhdao"><p><i class="fa fa-newspaper-o"></i> Nhập kế hoạch-công tác phòng trình lãnh đạo Sở

                                        <span class="pull-right">{if !empty($tong_cho_ldduyet)}{$tong_cho_ldduyet}{/if}</span></p></a>

                                        

                                <a class="tin" href="{$url}kehoachcongtac"><p><i class="fa fa-newspaper-o"></i>  Duyệt, giao KH, đánh giá công việc tuần của cấp dưới

                                        <span class="pull-right">{$tong_sl_cv}</span></p></a>



                                <a class="tin" href="{$url}dskehoachcongtac/{$tuan}"><p><i class="fa fa-newspaper-o"></i> Nhận xét, chấm điểm công việc tuần của cấp dưới

                                    </p></a>

                               <!--  <a class="tin" href="{$url}dskehoachcongtac/52"><p><i class="fa fa-newspaper-o"></i> Nhận xét, chấm điểm công việc tuần của cấp dưới

                                   </p></a> -->



                                <a class="tin" href="{$url}dskehoachcongtacthang"><p><i class="fa fa-newspaper-o"></i> Phân loại công chức của phòng theo tháng</p></a>

                                <a class="tin hide" href="{$url}dskehoachcongtacquy"><p><i class="fa fa-newspaper-o"></i> Đánh giá kết quả phân loại quý của phòng</p></a>



                                <a class="tin" href="{$url}xemkehoachcongtac_lanhdao"><p><i class="fa fa-newspaper-o"></i> Trưởng phòng tự chấm điểm tháng của cá nhân</p></a>



                                <a class="tin" href="{$url}dskehoachcongtacthangtp"><p><i class="fa fa-newspaper-o"></i> Xem kết quả phân loại của các trưởng phòng</p></a>



                                <a class="tin" href="{$url}dskehoachcongtactuanld"><p><i class="fa fa-newspaper-o"></i> Xem kết quả công tác tuần của các phòng trong Sở</p></a>



                                <a class="tin" href="{$url}thongkebaocao"><p><i class="fa fa-newspaper-o"></i> Thống kê kết quả công tác tuần trong toàn Sở</p></a>



                                <a class="tin" href="{$url}thongkebaocaothang"><p><i class="fa fa-newspaper-o"></i> Thống kê kết quả công tác tháng trong toàn Sở</p></a>



                                {if  $vanban['iQuyenHan_DHNB'] == 3}

                                <a class="tin" href="{$url}lanhdaonhanxetthang"><p><i class="fa fa-newspaper-o"></i> Đánh giá, phê duyệt kết quả  phân loại tháng của Sở </p></a>

                                {/if}

                                <a class="tin" href="{$url}dssangtao"><p><i class="fa fa-newspaper-o"></i> Đề xuất sáng tạo, đổi mới trong tháng</p></a>

                                {/if}



                                {if  $vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5} 

                                <a class="tin" href="{$url}dskehoachcongtactuanld"><p><i class="fa fa-newspaper-o"></i> Xem kế hoạch, kết quả công tác tuần của các phòng</p></a>                                        

                                <a class="tin" href="{$url}dskehoachcongtacthangld"><p><i class="fa fa-newspaper-o"></i> Xem kết quả phân loại tháng của các phòng</p></a>

                                <a class="tin hide" href="{$url}dskehoachcongtacquyld"><p><i class="fa fa-newspaper-o"></i> Kết quả phân loại quý của phòng</p></a>

                                <!--<a class="tin {($vanban['iQuyenHan_DHNB'] == 4)?'hide':''}" href="{$url}xemkehoachcongtac_lanhdao"><p><i class="fa fa-newspaper-o"></i> Phó Giám đốc tự chấm điểm tháng của cá nhân</p></a>-->

                                <a class="tin {($vanban['iQuyenHan_DHNB'] == 5)?'hide':''}" href="{$url}dskehoachcongtacthangtp"><p><i class="fa fa-newspaper-o"></i> Đánh giá, chấm điểm lãnh đạo phòng</p></a>

                                <a class="tin {($vanban['iQuyenHan_DHNB'] == 4)?'hide':''}" href="{$url}dskehoachcongtacthangtp"><p><i class="fa fa-newspaper-o"></i> PGĐ nhận xét tháng đối với lãnh đạo phòng</p></a>



                                <a class="tin hide" href="{$url}dskehoachcongtacquytp"><p><i class="fa fa-newspaper-o"></i> Kết quả công tác quý của lãnh đạo Sở, phòng, đơn vị</p></a>

                                

                                <a class="tin" href="{$url}thongkebaocao"><p><i class="fa fa-newspaper-o"></i> Thống kê kết quả công tác tuần trong toàn Sở</p></a> 



                                <a class="tin" href="{$url}thongkebaocaothang"><p><i class="fa fa-newspaper-o"></i> Thống kê kết quả công tác tháng trong toàn Sở</p></a>

                                {if  $vanban['iQuyenHan_DHNB'] == 4}

                                <a class="tin" href="{$url}lanhdaonhanxetthang"><p><i class="fa fa-newspaper-o"></i> Đánh giá, phê duyệt kết quả  phân loại tháng của Sở </p></a>

                                {/if}
                                <a class="tin" href="{$url}dssangtao"><p><i class="fa fa-newspaper-o"></i> Đề xuất sáng tạo, đổi mới trong tháng</p></a>
                                {/if}



                                

                            </div>

                        </div>

                    </div>

                </div>

                    <!-- Văn bản chi cục -->

                    <div class="col-md-12 {if $vanban['donvi_caphai']==0}hide{/if}">

                        <div class="col-md-6" style="padding-right: 0px;">

                            <div class="panel panel-info vanbanden_caphai" style="border-radius: 10px;">

                                <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;"><img src="{$url}assets/img/icon-vb.png" alt="" style="width: 27px;    margin-top: -5px;"> <b style="font-size: 15px;"> VĂN BẢN ĐƠN VỊ CẤP HAI</b></div>

                                <div class="panel-body">

                                    <a class="tin" href="{$url}danhsachtongthe_caphai"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản tổng thể <span class="pull-right"> {$vanbantongthe_caphai}</span></p></a>

                                    {if $vanban['quyenhan_caphai']==1}

                                        <a class="tin" href="{$url}vanbanchoxuly_ld"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ chỉ đạo <span class="pull-right">{$lanhdaochoxuly}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_ld"><p><i class="fa fa-newspaper-o"></i> Văn bản đã chỉ đạo (Phòng chưa xử lý) <span class="pull-right">{$lanhdaodaxuly}</span></p></a>

                                    {/if}

                                    {if $vanban['quyenhan_caphai']==2}

                                        <a class="tin" href="{$url}vanbanchoxuly_ldp"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ chỉ đạo <span class="pull-right">{$lanhdaochoxuly}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_ldp"><p><i class="fa fa-newspaper-o"></i> Văn bản đã chỉ đạo (Phòng chưa xử lý) <span class="pull-right">{$lanhdaodaxuly}</span></p></a>

                                    {/if}

                                    {if $vanban['quyenhan_caphai']==3}

                                        <a class="tin" href="{$url}vanbanchoxuly_tp_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ xử lý (VB chủ trì)<span class="pull-right">{$phongct_choxuly}</span></p></a>

                                        <a class="tin" href="{$url}vanbanchoxuly_tp_ch_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ xử lý (VB phối hợp)<span class="pull-right">{$phongph_choxuly}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_tp_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản đã xử lý (VB chủ trì - Chưa kết thúc) <span class="pull-right">{$phongct_daxuly}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_tp_ch_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản đã xử lý (VB phối hợp - Chưa kết thúc) <span class="pull-right">{$phongph_daxuly}</span></p></a>

                                        <a class="tin" href="{$url}duyetketqua_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ duyệt <span class="pull-right">{$vanbanchoduyet}</span></p></a>

                                        <a class="tin" href="{$url}dsvanbanphongchutri"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản phòng chủ trì<span class="pull-right">{$vanbanphongchutri}</span></p></a>

                                        <a class="tin" href="{$url}dsvanbanphongphoihop"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản phòng phối hợp<span class="pull-right">{$vanbanphongphoihop}</span></p></a>

                                    {/if}

                                    {if $vanban['quyenhan_caphai']==4}

                                        <a class="tin" href="{$url}vanbanchoxuly_pp_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ xử lý (VB chủ trì)<span class="pull-right">{$phongct_choxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbanchoxuly_pp_ch_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ xử lý (VB phối hợp)<span class="pull-right">{$phongph_choxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbanchoxuly_pp_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản phối hợp chờ xử lý<span class="pull-right">{$vanbanph_choxuly}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_pp_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản đã xử lý (VB chủ trì - Chưa kết thúc) <span class="pull-right">{$phongct_daxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_pp_ch_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản đã xử lý (VB phối hợp - Chưa kết thúc) <span class="pull-right">{$phongph_daxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_pp_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản phối hợp đã xử lý<span class="pull-right">{$vanbanph_daxuly}</span></p></a>

                                        <a class="tin" href="{$url}duyetketqua_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ duyệt <span class="pull-right">{$vanbanchoduyet}</span></p></a>

                                        <a class="tin" href="{$url}dsvanbanphongchutri"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản phòng chủ trì<span class="pull-right">{$vanbanphongchutri}</span></p></a>

                                        <a class="tin" href="{$url}dsvanbanphongphoihop"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản phòng phối hợp<span class="pull-right">{$vanbanphongphoihop}</span></p></a>

                                    {/if}

                                    {if $vanban['quyenhan_caphai']==5}

                                        <a class="tin" href="{$url}vanbanchoxuly_cv_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ xử lý (VB chủ trì)<span class="pull-right">{$phongct_choxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbanchoxuly_cv_ch_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản chờ xử lý (VB phối hợp)<span class="pull-right">{$phongph_choxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbanchoxuly_cv_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản phối hợp chờ xử lý<span class="pull-right">{$vanbanph_choxuly}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_cv_ch"><p><i class="fa fa-newspaper-o"></i> Văn bản đã xử lý (VB chủ trì) <span class="pull-right">{$phongct_daxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_cv_ch_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản đã xử lý (VB phối hợp) <span class="pull-right">{$phongph_daxuly_ppcv}</span></p></a>

                                        <a class="tin" href="{$url}vanbandaxuly_cv_ph"><p><i class="fa fa-newspaper-o"></i> Văn bản phối hợp đã xử lý<span class="pull-right">{$vanbanph_daxuly}</span></p></a>

                                        <a class="tin" href="{$url}dsvanbanphongchutri"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản phòng chủ trì<span class="pull-right">{$vanbanphongchutri}</span></p></a>

                                        <a class="tin" href="{$url}dsvanbanphongphoihop"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản phòng phối hợp<span class="pull-right">{$vanbanphongphoihop}</span></p></a>

                                    {/if}

                                    {if $vanban['quyenhan_caphai']==6}

                                        <a class="tin" href="{$url}nhapmoivanban"><p><i class="fa fa-newspaper-o"></i> Nhập mới văn bản</p></a>

                                        <a class="tin" href="{$url}nhapmoigiaymoi"><p><i class="fa fa-newspaper-o"></i> Nhập mới giấy mời</p></a>

                                    {/if}

                                    

                                    

                                </div>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="panel panel-info vanbandi_caphai" style="border-radius: 10px;"> 

                                <div class="panel-heading" style="padding: 10px; border-radius: 10px; background-color: #1373bf;color: white;"><img src="{$url}assets/img/icon-vb.png" alt="" style="width: 25px;    margin-top: -5px;"> <b style="font-size: 15px;">VĂN BẢN ĐI ĐƠN VỊ CẤP HAI</b></div>

                                <div class="panel-body">

                                    <a class="tin1" href="{$url}danhsachvanbandi"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản đi <span class="pull-right">{$vanbandi_caphai}</span></p></a>

                                    <a class="tin1" href="{$url}danhsachvanbanchoso"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản chờ số <span class="pull-right">{$vanbandichoso_caphai}</b></p></a>

                                    {if $vanban['quyenhan_caphai']==3 || $vanban['quyenhan_caphai']==4 || $vanban['quyenhan_caphai']==5}

                                    <a class="tin1" href="{$url}danhsachvanbandiphong"><p><i class="fa fa-newspaper-o"></i> Danh sách văn bản của phòng<span class="pull-right">{$vanbandiphong_caphai}</span></p></a>

                                    {/if}

                                    <a class="tin" href="{$url}nhapmoivanbandi"><p><i class="fa fa-newspaper-o"></i> Nhập mới văn bản đi</p></a>

                                </div>

                            </div>

                        </div>

                    </div>

                <div class="row">

                    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">

                        <div class="modal-dialog" role="document">

                            <div class="modal-content">

                                <div class="panel panel-info">

                                    <div class="panel-heading" style="font-size: 16px;"><b>Thông báo</b></div>

                                    <div class="panel-body show-noidung" style="font-size: 16px;"></div>

                                    <div class="panel-footer text-center" style="cursor:pointer"  data-dismiss="modal" data-dismiss="modal">Đóng</div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <script>

                        $(document).ready(function() {

                            $(document).on('click','.show-nd',function(){

                                var noidung = $(this).text();

                                $('.show-noidung').text(noidung);

                            });

                        });

                    </script>

                </div>

            </div>

        </div>

    </div>

</section>

<style>

    .popup_color{

        color:#040404;

    }

    .popup_color:hover,.popup_color:focus{

        color:red;

    }

</style>

</div>

<script>

    $(document).ready(function() {

        var vanbanden =$('.vanbanden').height();

//        alert(vanbanden);

        var giaymoi =$('.giaymoi').height();

        if(parseInt(vanbanden)>parseInt(giaymoi))

        {

            $('.giaymoi').css('min-height', vanbanden+'px');

        }

        else{

            $('.vanbanden').css('min-height', giaymoi+'px');

        }

        var vanbandi =$('.vanbandi').height();

        var thongtinnoibo =$('.thongtinnoibo').height();

        if(parseInt(vanbandi)>parseInt(thongtinnoibo))

        {

            $('.thongtinnoibo').css('height', vanbandi+'px');

        }

        else{

            $('.vanbandi').css('height', thongtinnoibo+'px');

        }

        var kehoach =$('.kehoach').height();

        var congtac =$('.congtac').height();

        if(parseInt(kehoach)>parseInt(congtac))

        {

            $('.congtac').css('min-height', kehoach+'px');

        }

        else{

            $('.kehoach').css('min-height', congtac+'px');

        }

        var vanbanden_caphai = $('.vanbanden_caphai').height();

        var vanbandi_caphai  = $('.vanbandi_caphai').height();

        if(parseInt(vanbanden_caphai)>parseInt(vanbandi_caphai))

        {

            $('.vanbandi_caphai').css('height', vanbanden_caphai+'px');

        }

        else{

            $('.vanbanden_caphai').css('height', vanbandi_caphai+'px');

        }

    });

</script>

