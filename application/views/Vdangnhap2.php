<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>VĂN PHÒNG ĐIỆN TỬ - SỞ NGOẠI VỤ HÀ NỘI</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
    <style type="text/css">
        html{
            margin: 0 auto;
        }
        body {
            font-family:arial,tahoma,verdana,helvetica,sans-serif;
            font-size:11px;
            text-align:center;
            background:#e1e1e1 url(assets/img/login/bg_main.jpg) repeat-x;
            line-height:22px;
            color:#000000;
            font-weight:bold;
            margin-left: 0px;
            margin-top: 77px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        #clear{
            clear: both;
            height:5px;
        }
        #table_main{
            margin: 0 auto;
            width:330px;
            height:297px;
            text-align:left;
            background:url(assets/img/login/center.jpg) repeat-x;
        }
        #left{
            width:15px;
            height:297px;
            float:left;
            background:url(assets/img/login/left.jpg) no-repeat;

        }
        #right{
            width:15px;
            height:297px;
            float:right;
            background:url(assets/img/login/right.jpg) no-repeat;

        }
        #center{
            height:298px;
            margin-left:15px;
            background:url(assets/img/login/center.jpg) repeat-x;
            vertical-align:top;

        }
        #img_left{
			
			
		}
        #content{
            width:100%;
        }
        #logo{
			background: url(assets/img/login/Group2.png) no-repeat left;
			background-size: 125px 125px;
            width:100%;
            height:165px;
			margin-left: 87px;
        }
        #text{
            width:100%;
            margin-top: -10px;
        }
        .fro{
            width:180px;
        }
        /*.msgerr
         {
            background: url(images/error-icon-16.gif) no-repeat left top;
             padding-left: 25px;
             margin-left: 5px;
             list-style: none;
             vertical-align: middle;
             text-align: left;
             color: #df2267;
             height:16px;
         }*/

        .errmsg
        {
            color:#ff0000;
        }
    </style>
    <style>
        .loader,
        .loader:after {
            /*border-radius: 50%;*/
            width: 18em;
            height: 0em;
            margin: auto;
            padding: 10px;
        }
        .loader {
            /*margin: 785px;*/
            /*font-size: 5px;*/
            /*position: fixed;*/
            /*text-indent: -9999em;*/
            background-image: url("assets/img/ajax-loader.gif");
        }
        #loadingDiv {
            position:fixed;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background:#ffffff;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(document).on('click','#dangnhap',function () {
//                    alert(1);
                $('body').append('<div style="" id="loadingDiv"><div class="loader" style="margin-top: 200px;">Đang tải dữ liệu</div></div>');
            });
        });
    </script>
</head>
<body>
<table width="1000"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" height="91%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><div id="table_main">
                            <div id="left"> </div>
                            <div id="right"> </div>
                            <div id="center">
                                <div id="content">
                                    <div id="logo"> </div>
                                    <div id="text">
                                        <form action="" method="post" autocomplete="off">
                                            <input type="hidden" name="login" value="yes">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    
                                                    <td colspan="3" style="text-align: center;" height="35">
                                                        <input name="taikhoan" type="text" id="user_name"  value="" size="25" style="font-family:Verdana; font-size:12px; border:1px solid #CCCCCC; text-align:center" autofocus="autofocus" placeholder="username">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td colspan="3" style="text-align: center;">
                                                        <input name="matkhau" type="password" id="user_pass" size="25" style="font-family:Verdana; font-size:12px; border:1px solid #CCCCCC; text-align:center" placeholder="password">
							<input type="hidden" name="db_year" id="db_year" value="{date('Y')}">                                                            
                                                    </td>
                                                </tr>
                                                
                                                <tr>
												<p style="text-align: center; margin-top: -5px; margin-bottom: 0px;">Vui lòng nhập username và password.</p>
                                                    {if $ketqua=='failed'}<p style="color:red; text-align: center; margin-top: -5px; margin-bottom: 0px;">Username / password không đúng.</p>{/if}
													
                                                </tr>
                                                <tr>
                                                    <td colspan="5" style="text-align: center;" height="35">
                                                        <input name="dangnhap" type="submit" id="dangnhap" value="Enter" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                                <div id="img_left"> </div>
                            </div>
                        </div></td>
                </tr>
                <!--<tr>
                    <td>
                        <h2>VĂN PHÒNG ĐIỆN TỬ - SỞ NGOẠI VỤ THÀNH PHỐ HÀ NỘI <br>
						Trình duyệt được khuyên dùng: <a href="https://www.google.com/chrome/">Google Chrome</a></h2>
                    </td>
                </tr>-->
            </table>
        </td>
    </tr>
</table>
</body>
