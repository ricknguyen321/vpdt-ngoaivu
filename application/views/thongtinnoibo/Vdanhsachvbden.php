<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Danh sách văn bản đến
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="collapse" id="collapseExample">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <form action="" method="get" class="form-horizontal">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Số đến</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="soden" value="{($soden)?$soden:''}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width:5%" class="text-center">STT</th>
                                <th width="6%" class="text-center">Chuyển</th>
                                <th width="15%">Loại VB/Số KH</th>
                                <th width="8%">Số đến</th>
                                <th width="">Trích yếu</th>
                                <th width="10%" class="text-center">Ngày ký</th>
                                <th width="15%">Người ký</th>
                            </tr>
                        </thead>
                        <tbody>
                        {if !empty($vanbanden)} {$i=1}
                            {foreach $vanbanden as $den}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-center"><a href="{$url}chitietvbdendi?id={$den.PK_iMaVBDen}&vb=den" class="tin"><button class="btn btn-primary btn-sm">Chuyển</button></a></td>
                                    <td class=""><p>- {$den.sTenLVB}</p> <p>- {$den.sKyHieu} </p></td>
                                    <td class="text-center"><b style="color: red; font-size:16px">{$den.iSoDen}</b></td>
                                    <td >
										<p><b><a href="quatrinhxuly/{$den.PK_iMaVBDen}">{$den.sMoTa}</a></b></p>
										- Nơi gửi: {$den.sTenDV}
									</td>
                                    <td class="text-center">{date_select($den.sNgayKy)}</td>
                                    <td>{$den.sTenNguoiKy}</td>
                                </tr>
                            {/foreach}
                        {/if}
                        </tbody>
                    </table>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        $('.control-label').css('text-align', 'left');
    });
</script>