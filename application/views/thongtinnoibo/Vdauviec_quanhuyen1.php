<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="30%" class="text-center">Mô tả</th>
                                    <th width="15%">Thời gian gửi</th>
                                    <th width="15%">Thời gian đọc</th>
                                    <th width="20%" class="text-center">Phòng quận huyện</th>
                                    <th width="15%" class="text-center">Họ tên</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dulieu)}{$i=1}
                                {foreach $dulieu as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sMoTa}</td>
                                        <td>{date_time($dl.sThoiGianGui)}</td>
                                        <td>{if !empty($dl.sThoiGianDoc)}{date_time($dl.sThoiGianDoc)}{/if}</td>
                                        <td class="text-center">{$dl.sTenPB}</td>
                                        <td>{$dl.sHoTen}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">
                            {$phantrang}
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        // $('th').css('vertical-align', 'middle');
        $('td').css('vertical-align', 'middle');
    });
</script>