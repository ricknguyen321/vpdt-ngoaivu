<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="8%" class="text-center">Chi tiết</th>
                                    <th width="15%">Loại VB/Số KH</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="10%" class="text-center">Thời gian</th>
                                    <th width="22%" class="text-center">Ý kiến</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsdulieu)}{$i=1}
                                {foreach $dsdulieu as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center"><a class="tin" href="{$url}xemchitietguidi?id={$dl.PK_iMaVB}">Xem</a></td>
                                        <td><p>{$dl.sTenLVB}</p><p>{$dl.sKyHieu}</p></td>
                                        <td>{$dl.sMoTa}</td>
                                        <td class="text-center">{date_time($dl.sNgayNhap)}</td>
                                        <td>{$dl.sNoiDungChiDao}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
						<div class="pull-right">
                            {$phantrang}
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        // $('th').css('vertical-align', 'middle');
        $('td').css('vertical-align', 'middle');
    });
</script>