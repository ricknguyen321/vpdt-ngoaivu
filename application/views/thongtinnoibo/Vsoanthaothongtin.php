<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Soạn thảo thông tin
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box" >
            <div class="box-body" style="margin: 0px 30px">
				<!--
                <div class="row {if $vanban['iLoaiPhanMem']==2}hide{/if}">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body" style="text-align:center">Bạn có thể chuyển văn bản đến, văn bản đi tại: <a class="tin" href="{$url}danhsachvbden"><b>Văn bản đến</b></a>&nbsp;&nbsp;&nbsp;<a class="tin" href="{$url}danhsachvbdi"><b>Văn bản đi</b></a></div>
                        </div>
                    </div>
                </div>
				-->
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12" style="margin-top:20px">
						<div class="col-md-9">
                        <div class="form-group">
							<div class="col-md-2">
								<label for="" class="col-md-12" style="text-align:right">Nội dung:</label><br>
								<hr>
							</div>
                            <div class="col-md-10" style="text-align:center; font-size:11px">
                                <textarea  name="noidung" required class="form-control" id="" cols="" rows="8" placeholder="Nhập thông tin muốn gửi.">{($thongtin)?($thongtin[0]['sMoTa']):(!empty($mota)&&($mota)?$mota:'')}</textarea>
								<!--(Mẹo: để bôi đậm chữ, sử dụng cặp &lt;b&gt; &lt;/b&gt;, ví dụ: &lt;b&gt;chữ đậm&lt;/b&gt; sẽ được <b>chữ đậm</b>, tương tự chữ nghiêng sử dụng cặp &lt;i&gt; &lt;/i&gt; ví dụ: &lt;i&gt;chữ nghiêng&lt;/i&gt; sẽ được <i>chữ nghiêng</i>, chú ý trong khung soạn thảo sẽ không hiển thị chữ đậm, chữ nghiêng tuy nhiên thông tin gửi đến người nhận sẽ được bôi đậm, nghiêng)-->
                            </div>
                        </div>
						</div>
						<!--
                        <div class="form-group">
                            <label for="" class="col-md-2 control-label">Ghi chú </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="tenteptin" placeholder="Nhập ghi chú (optional)">
                            </div>							
                        </div>-->
                        <div class="form-group">
                            <p>Đính kèm </p>
                            <div class="col-md-3 noidung" style="margin-bottom:5px">
                                <input type="file" class="form-control" name="files[]" id="" multiple="">
                            </div>							
							
							<div class="themsau" style="text-align:right"></div>
								<button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;float:right; margin-right:20px"><i class="fa fa-plus"></i></button>
							
							<br><br>	<br>	<br>								
							<p>
							Chuyển <a class="tin" href="{$url}danhsachvbden"><b>Văn bản đến</b></a>
							</p>
							<p>							
							Chuyển <a class="tin" href="{$url}danhsachvbdi"><b>Văn bản đi</b></a>
							</p>
                        </div>
                        <!--<label for=""> Đính kèm:</label>
						{if !empty($dsfile)}{$i=1}
                        <table id="" class="table table-bordered table-striped" style ="width:550px">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="50%" class="text-center">Tên file đính kèm</th>
                                    <th width="20%" class="text-center">Tải xuống</th>
                                    <th width="25%" class="text-center">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                {foreach $dsfile as $f}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$f.sTenFile}</td>
                                        <td class="text-center"><a href="{$f.sDuongDan}">Xem</a> | <a href="{$f.sDuongDan}" download>Download</a></td>
                                        <td class="text-center"><button type="submit" name="xoadulieu" value="{$f.sDuongDan}" onclick="return confirm('Bạn có chắc muốn xóa file?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa fa-trash"></i></button></td>
                                    </tr>
                                {/foreach}
                            
                            </tbody>
                        </table>
						{else} Empty <br>
						{/if}-->
                        <label for=""> Nơi nhận: </label> <i style="font-size:12px; color: brown">
						(Nếu gửi thông tin cho cả phòng thì chỉ cần tích chọn nút Toàn bộ phòng ...; nếu gửi thông tin cho một hoặc nhiều cán bộ trong phòng thì chọn tên các cán bộ tương ứng không tích chọn nút Toàn bộ phòng)</i>
						
                        <table id="phongban" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="25%"><input type="checkbox" name="checkallpb" id="allpb"> <label for="allpb">Gửi đến toàn Sở</label></th>
                                    <th width="">Người nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsphongban)}
                                    {foreach $dsphongban as $pb}
                                        <tr>
                                            <td>
											<input type="checkbox" name="phongban[]" value="{$pb.PK_iMaPB}" id="{$pb.PK_iMaPB}" 
                                            {if !empty($dsphong)}
                                                {foreach $dsphong as $p}
                                                    {if $pb.PK_iMaPB == $p.PK_iMaPB}checked{/if}
                                                {/foreach}
                                            {/if}>
											
											<label for="{$pb.PK_iMaPB}"><b>Toàn bộ {$pb.sTenPB}</b></label></td>
                                            <td>
                                                <select name="nguoinhan[]" multiple="" data-placeholder="Chọn người nhận" id="" class="form-control select2">
                                                    {if !empty($dsnguoinhanpb)}
                                                        {foreach $dsnguoinhanpb as $ngpb}
                                                            {if $ngpb.FK_iMaPhongHD == $pb.PK_iMaPB}
                                                                <option value="{$ngpb.PK_iMaCB}"
                                                                    {if !empty($dscanbo)}
                                                                        {foreach $dscanbo as $c}
                                                                            {if $ngpb.PK_iMaCB == $c.PK_iMaCB}selected{/if}
                                                                        {/foreach}
                                                                    {/if}
                                                                >{$ngpb.sHoTen}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </td>
                                            <!--<td><input name="ghichu[{$pb.PK_iMaPB}][]" type="text" placeholder="Nhập ghi chú tại đây" value="{if !empty($dsphong)}{foreach $dsphong as $p}{if $pb.PK_iMaPB == $p.PK_iMaPB}{$p.sGhiChu}{/if}{/foreach}{/if}" class="form-control">
                                            </td>-->
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        
                        <div class="col-sm-12" style="text-align:center">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Gửi'}</button> 
                             
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change',"input[name=checkallpb]",function () {
            $(this).parents('#phongban').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $(document).on('change',"input[name=checkallqh]",function () {
            $(this).parents('#quanhuyen').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $('td').css('vertical-align', 'middle');

    }); 
</script>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>

