<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">				
                    <div class="col-md-12">
						<i>Khi có ý kiến phản hổi mới, thông tin sẽ được đưa lại mục thông tin mới. Để đánh dấu đã xem nhấn nút "Đã xem", để xem chi tiết thông tin nhấn nút "Chi tiết"</i>&nbsp;&nbsp;&nbsp;
						{if date('Y') == $year}
							<a href="soanthaothongtin"><button type="button" class="btn btn-primary btn-sm" style="float:right;margin-bottom:3px">Soạn thảo TTNB</button></a>
						{/if}
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <!--<th width="5%" class="text-center">STT</th>-->
                                    <th width="12%" class="text-center">Người gửi</th>
                                    <th width="">Trích yếu - Nội dung</th>
									<th width="40%" class="text-center">Ý kiến phản hồi</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsthongtin)}{$i=1}
                                {foreach $dsthongtin as $dl}
									{$dsfile = laydsfilettnb($dl.PK_iMaVB)}
                                    <tr>
                                        <!-- <td class="text-center">{$i++}</td>-->
                                        <td class="text-center">
										{$cbtao = layTTCB($dl.FK_iMaCB_Nhap)}											
										<b>{$cbtao[0].sHoTen}</b>
										<br>
										{date_time2($dl.sNgayNhap)}
										</td>
										
                                        <td>
											<div style="max-height:250px;  overflow:auto; margin-bottom: 10px">
												<p style="text-align: justify; margin-right:5px">{$dl.sMoTa}</p>
													{if !empty($dl.sNoiDungChiDao)}<p style="text-align: justify; margin-right:5px">{$dl.sNoiDungChiDao}</p>{/if}
											</div>
											<hr style="margin:10px">
											{if !empty($dsfile)}												
												<b>Đính kèm:</b><br>
												{foreach $dsfile as $f}
													<p style="margin-bottom:3px">
													<a style="color:brown" target="_blank" href="{$url}{$f.sDuongDan}">{$f.sTenFile}</a></p>
												{/foreach}
											{/if}
											
											<span class="pull-right" style="margin-top:10px">
											<button type="submit" name="daxem" value="{$dl.PK_iMaVB}" class="btn btn-xs btn-primary">Đã xem</button>&nbsp;&nbsp;&nbsp;&nbsp;
											
											<a class="tin" href="{$url}xemchitiet?id={$dl.PK_iMaVB}"><button type="button" value="" class="btn btn-primary btn-xs" style="min-width: 100px; text-align:left">Chi tiết</button></a></span>
										</td>
										{$dsgopy = array_reverse(layGopY($dl.PK_iMaVB))}
										<td >
											<div style="max-height:300px;  overflow:auto">
											{foreach $dsgopy as $gy}
												<p>"{$gy.sNoiDung}" </p> 
													{$dstenfiles = explode(';', $gy.sTenFile)}
													{$dsduongdan = explode(';', $gy.sDuongDan)}
													{$i=0}
													{foreach $dstenfiles as $file}
														<p style="margin-bottom:3px"><a href="{$dsduongdan[$i++]}" target="_blank" style="color:brown">{substr($file, 0, 90)}</a></p>
													{/foreach}
													<span style="float:right"><b>{$gy.sHoTen}</b> {date_time2($gy.sThoiGian)}</span><br><hr style="margin:10px">
											{/foreach}
											</div>
										</td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
