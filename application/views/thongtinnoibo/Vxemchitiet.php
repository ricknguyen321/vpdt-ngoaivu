<style>
    /*.table>tbody>tr:nth-of-type(odd) {*/
    /*background-color: rgba(236, 240, 245, 0.47);*/
    /*}*/
    .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
        border: 0.5px solid rgba(195, 195, 195, 0.22);
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Xem chi tiết thông tin văn bản
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""> THÔNG TIN: </label>
                            <table id="" class="thongtin table table-bordered table-striped">
                                <thead>
                                <tr>
									<th width="15%">Người gửi:</th>
                                    <td  width="45%" class="mau">{$dulieu['sHoTen']}</td>                                
                                    <th width="15%">Ngày gửi:</th>
                                    <td width="25%" class="mau">{date_time($dulieu['sNgayNhap'])}</td>
								</tr>
                                    
								{if $dulieu['sKyHieu'] != '' || $dulieu['sTenLVB'] != ''}
                                <tr>
                                    <th width="15%">Số ký hiệu:</th>
                                    <td width="25%" class="mau">{$dulieu['sKyHieu']}</td>
                                    <th width="15%">Loại văn bản:</th>
                                    <td colspan="3" class="mau" style="">{$dulieu['sTenLVB']}</td>
                                </tr>
								
                                <tr>
                                    <th width="15%">Người ký:</th>
									<td class="mau" style="" width="25%">{$dulieu['sNguoiKy']}</td>
									 <th width="15%">Ngày ký:</th>
                                    <td colspan="3" class="mau" style="">{($dulieu['sNgayKy'] !='0000-00-00 00:00:00' && $dulieu['sNgayKy'] !='1970-01-01 00:00:00')?date_select($dulieu['sNgayKy']):''}</td>
                                </tr>
								{/if}
                                <tr>
                                    <th width="15%">Nội dung:</th>
                                    <td colspan="3" class="mau" style="font-size:14px;text-align: justify;">{$dulieu['sMoTa']}</td>
                                </tr>
								{if $dulieu['sKyHieu'] != '' || $dulieu['sTenLVB'] != ''}
                                <tr>
                                    <th width="15%">Ý kiến:</th>
                                    <td colspan="3" class="mau" style="">{$dulieu['sNoiDungChiDao']}</td>
                                </tr>

                                <!--<tr>
                                    <th width="15%">Chú thích:</th>
                                    <td colspan="3" class="mau">{$dulieu['sGhiChu']}</td>
                                </tr>-->
								{/if}
								
								<tr>
                                    <th width="15%" ><span style="">Đính kèm:</span></th>
                                    <td colspan="3" class="mau">
										{if !empty($dsfile)}
											{foreach $dsfile as $f}
												<p style="margin-bottom:3px"><a style="color:brown" target="_blank" href="{$f.sDuongDan}">{$f.sTenFile}</a></p>
											{/foreach}
										{/if}
									
									</td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            
                            <table id="" class="thongtin table table-bordered table-striped">
                                <tr>
                                    <th colspan="2"><a class="btn btn-primary btn-xs" style="" href="javascript:void(0);" name="ykien">Thêm ý kiến phản hồi</a>  <!--<a class="btn btn-default1 btn-xs" style="color:#333" href="{$url}chuyendi?id={$mavanban}">Chuyển tiếp</a>--></th>
                                </tr>
                                <form action="" method="post">
                                    <tr class="an hide">
                                        <td width="15%"><label for="">Nội dung ý kiến:</label></td>
                                        <td style="display:flex"><textarea name="noidunggopy" id="" cols="" rows="6" class="form-control"placeholder="Nhập nội dung ý kiến/chỉ đạo ..."></textarea>&nbsp;&nbsp;&nbsp;
										<button type="submit" name="luudulieu" value="luudulieu" class="btn btn-sm btn-primary">Gửi</button></td>
										 
                                    </tr>
									
									 <tr class="an hide">
										<td width="15%"><label for="">Đính kèm:</label></td>
                                        <td style="display:flex"><div class="col-md-3 noidung" style="margin-bottom:5px">
											<input type="file" class="form-control" name="files[]" id="" multiple="">
										</div>							
										
										<div class="themsau" style="text-align:right"></div>
											<button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;float:right; margin-right:20px"><i class="fa fa-plus"></i></button>
										</td>
									 </tr>
                                </form>
                            </table>
							
                           {if !empty($dsgopy)}
							{$dsgopy = array_reverse($dsgopy)}
						   {$i=1}
                            <label for=""> Ý KIẾN PHẢN HỒI:</label>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="20%">Người gửi</th>
                                    <th width="">Nội dung</th>
                                </tr>
                                </thead>
                                <tbody>
                               
                                {foreach $dsgopy as $gy}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-center">
										<p><b>{$gy.sHoTen}</b></p>
										{date_time2($gy.sThoiGian)}
									</td>
                                    <td><p>{$gy.sNoiDung}</p>
									{$dstenfiles = explode(';', $gy.sTenFile)}
									{$dsduongdan = explode(';', $gy.sDuongDan)}
									{$i=0}
									{foreach $dstenfiles as $file}
										<p style="margin-bottom:3px"><a href="{$dsduongdan[$i++]}" target="_blank" style="color:brown">{substr($file, 0, 90)}</a></p>
									{/foreach}
									
									</td>
                                </tr>
                                {/foreach}
                               
                                </tbody>
                            </table>
							 {/if}
							 
							 <div class="col-md-6">
                        <label for=""> Chưa xem:</label>
                        <table id="" class="table table-bordered table-striped" style="">
                            <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="35%">Tên phòng ban</th>
                                    <th width="">Người nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsphong)}{$i=1}
                                {foreach $dsphong as $p}
									{if !empty($dscanbo)}
										<tr>
											<td class="text-center">{$i++}</td>
											<td>{$p.sTenPB}</td>
											<td>
												
													{foreach $dscanbo as $cb}
														{if $cb.FK_iMaPhongHD==$p.PK_iMaPB && empty($cb.sThoiGianDoc)}
															<p>{$cb.sHoTen} - <i style="color:{($cb.sThoiGianDoc)?'#00bcd4':'red'};"class="">{($cb.sThoiGianDoc)?date_time2($cb.sThoiGianDoc):'Chưa xem'}</i></p>
														{/if}
													{/foreach}
											   
											</td>
											<!--<td>{$p.sGhiChu}</td>-->
										</tr>
									{/if}
                                {/foreach}
                            {/if}
                            </tbody>
							<BR>
							<!--
                            <tfoot>
                                <tr>
                                    <td colspan="4" style="margin-top: 30px">
                                        <a href="{$url}soanthaothongtin?id={$mavanban}" class="btn btn-primary">Sửa</a>
                                    </td>
                                </tr>
                            </tfoot>-->
                        </table>
						</div>
						<div class="col-md-6">
						<label for=""> Đã xem:</label>
                        <table id="" class="table table-bordered table-striped" style="">
                            <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="35%">Tên phòng ban</th>
                                    <th width="">Người nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsphong)}{$i=1}
                                {foreach $dsphong as $p}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td>{$p.sTenPB}</td>
                                        <td>
                                            {if !empty($dscanbo)}
                                                {foreach $dscanbo as $cb}
                                                    {if $cb.FK_iMaPhongHD==$p.PK_iMaPB && !empty($cb.sThoiGianDoc)}
                                                        <p>{$cb.sHoTen} - <i style="color:{($cb.sThoiGianDoc)?'#00bcd4':'red'};"class="">{($cb.sThoiGianDoc)?date_time2($cb.sThoiGianDoc):'Chưa xem'}</i></p>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </td>
                                        <!--<td>{$p.sGhiChu}</td>-->
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
							
                        </table>
						</div>
						
                            <!--<a href="javascript: history.back(1)" class="btn btn-default" data-original-title="" title="">Quay lại &gt;&gt;</a>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.thongtin th').css('background', 'rgb(225, 248, 251)');
        $('.thongtin th').css('color', '#333');
        $('.mau').css('background', 'rgb(255, 255, 255)');
        $('.mau').css('color', '#333');
        $('.thongtin').css('border', '1px solid rgb(225, 248, 251)');
        $(document).on('click','a[name=ykien]',function(){
            $('.an').toggle();
            $('.an').removeClass('hide');
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
    });
</script>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>