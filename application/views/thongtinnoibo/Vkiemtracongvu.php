<script type="text/javascript" src="{$url}assets/datetime_picker/moment.min.js"></script>
<script type="text/javascript" src="{$url}assets/datetime_picker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{$url}assets/datetime_picker/daterangepicker.css" />
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Kiểm tra công vụ
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
            <form action="" method="get" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12" style="padding-bottom: 10px">                                               
                        <label for=""> DANH SÁCH KIỂM TRA CÔNG VỤ:</label> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" style="padding-bottom: 10px">
                        <label for="" style="color: blue">Chọn tuần:<span style="color: red">* <i>(Chọn đúng tuần kiểm tra)</i></span> </label>
                    </div>
                    <div class="col-md-4" style="padding-bottom: 10px">
                        <select name="tuan" class="form-control select2" onchange="this.form.submit()">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option {if $tuan == $i} selected {/if} value="{$i}" >Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2" style="padding-bottom: 10px">   
                        <label  style="color: blue">&nbsp;&nbsp; Chọn phòng:<span style="color: red">*</span></label>
                    </div> 
                    <div class="col-md-3" style="padding-bottom: 10px"> 
                        <select name="phongbankt" class="form-control select2" onchange="this.form.submit()">
                            <option value="" > --Chọn phòng-- </option>
                            {foreach $dsphongban as $phong_id => $phong_name}
                            {if $phong_name['PK_iMaPB'] != 33}
                                <option {if $phong_name['PK_iMaPB'] == $phongbankt} selected {/if} value="{$phong_name['PK_iMaPB']}" >{$phong_name['sTenPB']}</b></option>
                            {/if}
                            {/foreach}
                        </select>
                    </div> 
        </form> 
        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">             

                        <table id="phongban" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="15%">Danh sách cán bộ</th>
                                    <th width="73%">Lỗi vi phạm</th>
                                    <th width="12%">Ngày kiểm tra</th>
                                    
                                </tr>
                            </thead>
                            <tbody>                                        
                            {foreach $dsnguoinhanpb as $ngpb}
                            <tr>
                                <td>{$ngpb['sHoTen']}</td>
                                <td>
                                    <select name="nhomvipham_{$ngpb['PK_iMaCB']}">
                                        <option> -- Chọn lỗi vi phạm -- </option>
                                        <option value="1">vi phạm chủ trương, đường lối, chính sách pháp luật, nội quy, quy chế làm việc của cơ quan</option>
                                        <option value="2">Hách dịch, cửa quyền, sách nhiễu, gây phiền hà, tiêu cực trong thực hiện nhiệm vụ, công vụ</option>
                                        <option value="3">Thiếu phẩm chất chính trị, đạo đức, lối sống lành mạnh; tác phong, lề lối làm việc chuẩn mực;</option>
                                        <option value="4">Thiếu tinh thần trách nhiệm với công việc; gây mất đoàn kết, trong cơ quan, đơn vị</option>
                                        <option value="5">Vi phạm văn hóa nơi công sở, giao tiếp, trang phục, đeo thẻ công chức trong giờ làm việc</option>

                                        <option value="6">Gây mất hình ảnh, uy tín cho bản thân, cơ quan, đơn vị và đồng nghiệp;</option>
                                        <option value="7">vi phạm các quy định của pháp luật về chế độ bảo mật, bí mật và kỷ luật phát ngôn của đơn vị</option>
                                        <option value="8">Vi phạm thời gian làm việc, giải quyết việc riêng trong giờ làm việc</option>
                                    </select><br><br>
                                    <textarea name="noidungvipham_{$ngpb['PK_iMaCB']}" placeholder="Ghi rõ nội dung vi phạm" rows="5" cols="110"> </textarea>
                                </td>
                                <td><input type="text" value="" name="ngaykiemtra_{$ngpb['PK_iMaCB']}" class="form-control datepic datemask" placeholder="Ngày kiểm tra">
                                <input type="hidden" name="macanbo[]" value="{$ngpb['PK_iMaCB']}">
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                <script>
                    $(document).ready(function() {
                        //Date picker
                        $('.datepic').datepicker({
                            autoclose: true
                        });
                        //Datemask dd/mm/yyyy
                        $(".datemask").inputmask("dd/mm/yyyy");
                    });
                </script>
                        </table>
                        
                        <div class="col-sm-12">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu Kiểm Tra</button> 
                        </div>
                    </div>
                </div>
            </form>
            <div>
                <table id="phongban" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="5%">STT</th>
                            <th width="10%">Tên cán bộ</th>
                            <th width="10%">Phòng</th>
                            <th width="5%">Tuần</th>
                            <th width="50%">Lỗi vi phạm</th>
                            <th width="10%">Cán bộ nhập</th>
                            <th width="10%">ngày vi phạm</th>
                            <th width="5%">Xóa</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {$i=1}
                        {foreach $danhsachvipham as $mc}
                        <tr>
                        <td>{$i++}</td>
                        <td>{$arr_canbo[$mc['macanbo']]}</td>
                        <td>{$mangtenphong[$mc['phong']]}</td>
                        <td>{$mc['tuan']}</td>
                        <td>{$mc['noidungvipham']}</td>
                        <td>{$arr_canbo[$mc['canbonhap']]}</td>
                        <td>{date_select($mc['ngaykiemtra'])}</td>
                        <td><a href="{$url}kiemtracongvu?id={$mc['id']}&tuan={$mc['tuan']}&phongbankt={$mc['phong']}">xóa</a></td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change',"input[name=checkallpb]",function () {
            $(this).parents('#phongban').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $(document).on('change',"input[name=checkallqh]",function () {
            $(this).parents('#quanhuyen').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $('td').css('vertical-align', 'middle');
    }); 

    $('.datetimes').daterangepicker({
            timePicker: true,
            startDate: ($('#ngaybd').val())?($('#ngaybd').val()):moment().startOf('hour'),
            endDate: ($('#ngaykt').val())?($('#ngaykt').val()):moment().startOf('hour').add(1, 'hour').add(30, 'minute'),
            locale: {
                format: 'DD/MM/Y HH:mm'
            }
        });
</script>

