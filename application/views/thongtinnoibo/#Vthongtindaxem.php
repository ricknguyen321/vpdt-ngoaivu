<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="15%">Thời gian</th>
                                    <th width="20%">Nơi gửi</th>
                                    <th width="50%">Trích yếu - Nội dung</th>
                                    <th width="10%" class="text-center">Xem file</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($danhsach)}{$i=1}
                                    {foreach $danhsach as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td>{date_time($dl.sNgayNhap)}</td>
                                            <td>{$dl.sTenPB}</td>
                                            <td>{$dl.sMoTa}</td>
                                            <td class="text-center"><a class="tin" href="{$url}xemchitiet?id={$dl.PK_iMaVB}">Xem chi tiết</a></td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">
                            {$phantrang}
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
</script>
