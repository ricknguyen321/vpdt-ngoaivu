
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Soạn thảo thông tin
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""> THÔNG TIN VĂN BẢN:</label>
                            <table id="" class="thongtin table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="15%">Ngày nhập:</th>
                                    <td width="35%" class="mau">{date_time($dulieu['sNgayNhap'])}</td>
                                    <th width="15%">Ngày ký:</th>
                                    <td  width="20%" class="mau">{($dulieu['sNgayKy']>'1970-01-01 00:00:00')?date_select($dulieu['sNgayKy']):''}</td>
                                </tr>
                                <tr>
                                    <th width="15%">Trích yếu:</th>
                                    <td colspan="3" style="font-size: 14px;" class="mau">{$dulieu['sMoTa']}</td>
                                </tr>
                                <tr>
                                    <th width="15%">Ý kiến:</th>
                                    <td colspan="3" style="font-size: 14px;" class="mau">{$dulieu['sNoiDungChiDao']}</td>
                                </tr>
                               
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                           <b>Đính kèm:</b>
							{if !empty($dsfile)}{$i=1}
                                {foreach $dsfile as $f}
                                    <span style="margin-left: 20px; margin-right: 20px"><a href="{$f.sDuongDan}" target="_blank" style="color:brown">{$f.sTenFile}</a></span>
                                {/foreach}
                                {/if}
                          <br><br>
                        <label for=""> Nơi nhận:</label> <i>Nếu gửi thông tin cho cả phòng thì chỉ cần tích chọn nút Toàn bộ phòng ...; nếu gửi thông tin cho một hoặc nhiều cán bộ trong phòng thì chọn tên các cán bộ tương ứng không tích chọn nút Toàn bộ phòng ... nếu không văn bản sẽ được gửi đến toàn bộ phòng</i>
                        <table id="phongban" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="25%"><input type="checkbox" name="checkallpb"> Gửi đến toàn bộ cán bộ</th>
                                    <th width="">Người nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsphongban)}
                                    {foreach $dsphongban as $pb}
                                        <tr {if $vanban['iLoaiPhanMem']!=2} {else}{if $pb.PK_iMaPB==73} class="hide" {/if}{/if}>
                                            
                                            <td><input type="checkbox" name="phongban[]" value="{$pb.PK_iMaPB}" 
                                            {if !empty($dsphong)}
                                                {foreach $dsphong as $p}
                                                    {if $pb.PK_iMaPB == $p.PK_iMaPB}checked{/if}
                                                {/foreach}
                                            {/if}> <b>Toàn bộ {$pb.sTenPB} </b></td>
                                            <td>
                                                <select name="nguoinhan[]" multiple="" data-placeholder="Chọn người nhận" id="" class="form-control select2">
                                                    {if !empty($dsnguoinhanpb)}
                                                        {foreach $dsnguoinhanpb as $ngpb}
                                                            {if $ngpb.FK_iMaPhongHD == $pb.PK_iMaPB}
                                                                <option value="{$ngpb.PK_iMaCB}"
                                                                    {if !empty($dscanbo)}
                                                                        {foreach $dscanbo as $c}
                                                                            {if $ngpb.PK_iMaCB == $c.PK_iMaCB}selected{/if}
                                                                        {/foreach}
                                                                    {/if}
                                                                >{$ngpb.sHoTen}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                            <div class="col-sm-12">
                                <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Gửi</button> <button type="reset" class="btn btn-default">Bỏ qua >></button>
                                <!-- <a href="javascript:void(0);" id="backLink" class="btn btn-primary">Quay lại >></a> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change',"input[name=checkallpb]",function () {
            $(this).parents('#phongban').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        $("#backLink").click(function() {
            event.preventDefault();
            history.back(1);
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('.thongtin th').css('background', 'rgb(225, 248, 251)');
        $('.thongtin th').css('color', '#333');
        $('.mau').css('background', 'rgb(255, 255, 255)');
        $('.mau').css('color', '#333');
        $('.thongtin').css('border', '1px solid rgb(225, 248, 251)');
        $(document).on('click','a[name=ykien]',function(){
            $('.an').toggle();
            $('.an').removeClass('hide');
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        $("#backLink").click(function() {
            event.preventDefault();
            history.back(1);
        });
    });
</script>
