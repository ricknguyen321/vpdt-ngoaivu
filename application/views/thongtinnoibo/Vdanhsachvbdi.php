<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Danh sách văn bản đi
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="collapse" id="collapseExample">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <form action="" method="get" class="form-horizontal">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                                                <div class="col-sm-8">
                                                    <select name="loaivanban" id="" class="form-control select2" style="width:100%">
                                                        <option value="">-- Chọn loại văn bản --</option>
                                                        {if !empty($dsloaivanban)}
                                                            {foreach $dsloaivanban as $l}
                                                            <option value="{$l.PK_iMaLVB}" {($loaivanban)?($loaivanban==$l.PK_iMaLVB)?'selected':'':''}>{$l.sTenLVB}</option>
                                                            {/foreach}
                                                        {/if}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Người ký</label>
                                                <div class="col-sm-8">
                                                    <select name="nguoiky" id="" class="form-control select2" style="width:100%">
                                                        <option value="">-- Chọn người ký --</option>
                                                        {if !empty($nguoiky)}
                                                            {foreach $nguoiky as $nk}
                                                                <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        {/if}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Ngày lấy số</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="ngaythang" value="{($ngaythang)?$ngaythang:''}" class="form-control datepic datemask" id="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">Trích yếu</label>

                                                <div class="col-sm-8">
                                                    <textarea name="trichyeu" class="form-control" rows="3">{($trichyeu)?$trichyeu:''}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">Nơi nhận</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="noinhan" value="{($noinhan)?$noinhan:''}" class="form-control" id="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width:5%" class="text-center">STT</th>
                                <th width="6%" class="text-center">Chuyển</th>
                                <th width="15%">Loại VB/Số KH</th>
                                <th width="">Trích yếu</th>
                                <th width="10%" class="text-center">Ngày lấy số</th>
                                <th width="25%">Nơi nhận</th>
                            </tr>
                        </thead>
                        <tbody>
                        {if !empty($vanbandi)} {$i=1}
                            {foreach $vanbandi as $di}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-center"><a href="{$url}chitietvbdendi?id={$di.PK_iMaVBDi}" class="tin"><button class="btn btn-primary btn-sm">Chuyển</button></a></td>
                                    <td class="text-center">
										<p>- Số vb: <b style="color:red; font-size:16px">{$di.iSoVBDi}</b></p>
										{$di.sTenLVB}<br>{$di.sKyHieu}
									</td>
                                    <td ><b><a href="thongtinvanban?id={$di.PK_iMaVBDi}"><p>{$di.sMoTa}</p>										
										</a></b>
										- Người tạo: {$cbtao = layTTCB($di.FK_iMaCB_Nhap)} {$cbtao[0].sHoTen} <br>
										- Thời gian: {date_time2($di.sNgayNhap)}
									</td>
                                    <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                    <td><textarea style="background:none; border: none; width:100%" rows="5">{$di.sNoiNhan}</textarea></td>
                                </tr>
                            {/foreach}
                        {/if}
                        </tbody>
                    </table>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        $('.control-label').css('text-align', 'left');
    });
</script>