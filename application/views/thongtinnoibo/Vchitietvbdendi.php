<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Thông tin văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4">Ngày tạo văn bản:</label>

                                            <div class="col-sm-8">
                                                {date('d/m/Y H:i:s')}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4">Khối phát hành:</label>

                                            <div class="col-sm-8">
                                                {($phongban)?$phongban[0]['sTenPB']:''}
                                                <input type="text" name="phongban" value="{($phongban)?$phongban[0]['sTenPB']:''}" class="form-control hide">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4">Người gửi văn bản:</label>

                                            <div class="col-sm-8">
                                                {$vanban['sHoTen']}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4">Người ký:</label>

                                            <div class="col-sm-8">{($thongtin)?$thongtin[0]['sHoTen']:''}
                                                <input type="text" name="nguoiky" value="{($thongtin)?$thongtin[0]['sHoTen']:''}" class="form-control hide">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4">Ngày ký:</label>

                                            <div class="col-sm-8">{($thongtin)?date_select($thongtin[0]['sNgayVBDi']):''}
                                                <input type="text" name="ngayky" value="{($thongtin)?date_select($thongtin[0]['sNgayVBDi']):''}" class="form-control hide datepic datemask">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4">Số ký hiệu:</label>

                                            <div class="col-sm-8">{($thongtin)?$thongtin[0]['sKyHieu']:''}
                                                <input type="text" name="kyhieu" value="{($thongtin)?$thongtin[0]['sKyHieu']:''}" class="form-control hide">
                                            </div>
                                        </div>
                                      
                                    </div>
									 <div class="col-md-6">
                                       
                                        <div class="form-group">
                                            <label for="" class="col-sm-4">Loại văn bản *:</label>

                                            <div class="col-sm-8">{($thongtin)?$thongtin[0]['sTenLVB']:''}
                                                <input type="text" name="loaivanban" value="{($thongtin)?$thongtin[0]['sTenLVB']:''}" class="form-control hide">
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="" class="col-sm-2">Trích yếu *:</label>

                                            <div class="col-sm-8">{($thongtin)?$thongtin[0]['sMoTa']:''}
                                                <textarea name="trichyeu" required class="form-control hide" id="" rows="3">{($thongtin)?$thongtin[0]['sMoTa']:''}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-2">Ý kiến:</label>

                                            <div class="col-sm-8">
                                                <textarea name="ykienchidao" class="form-control" id="" rows="2"></textarea>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
						<div style="">
							<label for="" style="background:#ffc107">ĐÍNH KÈM:</label>
							<table id="" class="table table-bordered table-striped" style="width:600px">
								<thead>
									<tr>
										<th width="5%">STT</th>
										<th width="">Tên file / ghi chú</th>
										<th width="15%" class="text-center">Xem</th>
										<th width="15%" class="text-center">Xóa</th>
									</tr>
								</thead>
								<tbody>
								{if !empty($dsfile)}{$i=1}
									{foreach $dsfile as $f}
										<tr>
											<td>{$i++}</td>
											<td>{$f.sTenFile}</td>
											<td class="text-center"><a href="{$f.sDuongDan}">Xem</a></td>
											<td class="text-center">-</td>
										</tr>
									{/foreach}
								{/if}
								</tbody>
							</table>
						   
							<a href="javascript: history.back(1);" id="backLink" class="btn btn-default">Quay lại >></a>
							<button type="submit" value="luudulieu" name="luudulieu" class="btn btn-primary">Chuyển đi</button>
						
						</div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>