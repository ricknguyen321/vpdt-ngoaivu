<script type="text/javascript" src="{$url}assets/datetime_picker/moment.min.js"></script>
<script type="text/javascript" src="{$url}assets/datetime_picker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{$url}assets/datetime_picker/daterangepicker.css" />
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Mở chấm điểm tuần
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12" style="padding-bottom: 10px">                                               
                        <label for=""> DANH SÁCH CÁ NHÂN CẦN MỞ CHẤM:</label> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2" style="padding-bottom: 10px">
                        <label for="" style="color: blue">Chọn tháng:<span style="color: red">*</span> </label>
                    </div>
                    <div class="col-md-3" style="padding-bottom: 10px">
                        <select name="thang" class="form-control select2">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option {if $thang == $i} selected {/if} value="{$i}" >Tháng: {$i++} </b></option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2" style="padding-bottom: 10px">   
                        <label  style="color: blue">&nbsp;&nbsp; Chọn thời gian:<span style="color: red">*</span></label>
                    </div> 
                    <div class="col-md-5" style="padding-bottom: 10px"> 
                        <input type="text" class="form-control datepic datetimes" id="thoigian" value="" name="thoigian" style="border: 1px solid #3c8dbc;" >
                    </div> 
                      

                        <table id="phongban" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="30%">Tên phòng ban</th>
                                    <th width="65%">Người mở chấm</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsphongban)}
                                    {foreach $dsphongban as $pb}
                                    {if $pb.PK_iMaPB!=73 and $pb.PK_iMaPB!=33 }
                                        <tr {if $vanban['iLoaiPhanMem']!=2} {else}{if $pb.PK_iMaPB==73} class="hide" {/if}{/if}>
                                            <td class="text-center"><input name="phongban[]" value="{$pb.PK_iMaPB}" type="checkbox"></td>
                                            <td>{$pb.sTenPB}</td>
                                            <td>
                                                <select name="nguoinhan[{$pb.PK_iMaPB}][]" multiple="" data-placeholder="Chọn người nhận" id="" class="form-control select2">
                                                    {if !empty($dsnguoinhanpb)}
                                                        {foreach $dsnguoinhanpb as $ngpb}
                                                            {if $ngpb.FK_iMaPhongHD == $pb.PK_iMaPB}
                                                                <option value="{$ngpb.PK_iMaCB}"
                                                                    {if !empty($dscanbo)}
                                                                        {foreach $dscanbo as $c}
                                                                            {if $ngpb.PK_iMaCB == $c.PK_iMaCB}selected{/if}
                                                                        {/foreach}
                                                                    {/if}
                                                                >{$ngpb.sHoTen}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </td>
                                            
                                        </tr>
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        
                        <div class="col-sm-12">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu mở chấm</button> 
                        </div>
                    </div>
                </div>
            </form>
            <div>
                <form action="" method="post">
                <button type="submit" value="1" name="xoatat" onclick="return confirm('Bạn có chắc muốn xóa tất cả?');" class="btn btn-danger pull-right">Xóa tất cả</button>
                </form>
                <table id="phongban" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="5%">STT</th>
                            <th width="20%">Tên cán bộ mở chấm</th>
                            <th width="20%">Phòng</th>
                            <th width="10%">Tháng</th>
                            <th width="20%">Thời gian mở</th>
                            <th width="20%">Thời gian đóng</th>
                            <th width="5%">Xóa</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {$i=1}
                        {foreach $mocham as $mc}
                        <tr>
                        <td>{$i++}</td>
                        <td>{$arr_canbo[$mc['canbo']]}</td>
                        <td>{$mangtenphong[$mc['phong']]}</td>
                        <td>{$mc['thang']}</td>
                        <td>{date_time($mc['batdau'])}</td>
                        <td>{date_time($mc['ketthuc'])}</td>
                        <td><a href="{$url}mochamthang?id={$mc['id']}">xóa</a></td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change',"input[name=checkallpb]",function () {
            $(this).parents('#phongban').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $(document).on('change',"input[name=checkallqh]",function () {
            $(this).parents('#quanhuyen').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $('td').css('vertical-align', 'middle');
    }); 

    $('.datetimes').daterangepicker({
            timePicker: true,
            startDate: ($('#ngaybd').val())?($('#ngaybd').val()):moment().startOf('hour'),
            endDate: ($('#ngaykt').val())?($('#ngaykt').val()):moment().startOf('hour').add(1, 'hour').add(30, 'minute'),
            locale: {
                format: 'DD/MM/Y HH:mm'
            }
        });
</script>

