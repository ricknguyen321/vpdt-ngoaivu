<style>
    /*.table>tbody>tr:nth-of-type(odd) {*/
    /*background-color: rgba(236, 240, 245, 0.47);*/
    /*}*/
    .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
        border: 0.5px solid rgba(195, 195, 195, 0.22);
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Xem chi tiết thông tin văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <label for=""> Thông tin đã gửi:</label>
                        <table id="" class="thongtin table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="15%">Ngày nhập:</th>
                                    <td width="25%" class="mau">{date_time($dulieu['sNgayNhap'])}</td>
                                    <th width="15%">Người nhập:</th>
                                    <td  width="45%" class="mau">{$dulieu['sHoTen']}</td>
                                </tr>
								{if $dulieu['sKyHieu'] != '' || $dulieu['sTenLVB'] != ''}
                                <tr>
                                    <th width="15%">Số ký hiệu:</th>
                                    <td width="25%" class="mau">{$dulieu['sKyHieu']}</td>
                                    <th width="15%">Loại văn bản:</th>
                                    <td  width="45%" class="mau">{($dulieu['sTenLVB'])?$dulieu['sTenLVB']:''}</td>
                                </tr>
                                <tr>
                                    <th width="15%">Người ký:</th>
                                    <td class="mau" style="" width="25%">{$dulieu['sNguoiKy']}</td>
									 <th width="15%">Ngày ký:</th>
                                    <td colspan="3" class="mau" style="font-size: 14px;">{($dulieu['sNgayKy'] !='0000-00-00 00:00:00' && $dulieu['sNgayKy'] !='1970-01-01 00:00:00')?date_select($dulieu['sNgayKy']):''}</td>
                                </tr>
								{/if}
                                <tr>
                                    <th width="15%">Nội dung:</th>
                                    <td colspan="3" class="mau">{$dulieu['sMoTa']}</td>
                                </tr>
								{if $dulieu['sKyHieu'] != '' || $dulieu['sTenLVB'] != ''}
                                <tr>
                                    <th width="15%">Ý kiến:</th>
                                    <td colspan="3" class="mau">{$dulieu['sNoiDungChiDao']}</td>
                                </tr>
								{/if}
								<tr>
                                    <th width="15%" ><span style="color:orange">Đính kèm:</span></th>
                                    <td colspan="3" class="mau">
										
											{if !empty($dsfile)}
											{foreach $dsfile as $f}
												<p style="margin-bottom:3px"><a style="color:brown" target="_blank" href="{$f.sDuongDan}">{$f.sTenFile}</a></p>
											{/foreach}
											{/if}
									
									</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        
                       {if !empty($dsgopy)}
							{$dsgopy = array_reverse($dsgopy)}
						   {$i=1}
                            <label for=""> Ý KIẾN PHẢN HỒI:</label>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="20%">Người gửi</th>
                                    <th width="">Nội dung</th>
                                </tr>
                                </thead>
                                <tbody>
                               
                                {foreach $dsgopy as $gy}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-center">
										<p><b>{$gy.sHoTen}</b></p>
										{date_time2($gy.sThoiGian)}
									</td>
                                    <td><p>{$gy.sNoiDung}</p>
									{$dstenfiles = explode(';', $gy.sTenFile)}
									{$dsduongdan = explode(';', $gy.sDuongDan)}
									{$i=0}
									{foreach $dstenfiles as $file}
										<p style="margin-bottom:3px"><a href="{$dsduongdan[$i++]}" target="_blank" style="color:brown">{substr($file, 0, 90)}</a></p>
									{/foreach}
									
									</td>
                                </tr>
                                {/foreach}
                               
                                </tbody>
                            </table>
							 {/if}
						<div class="col-md-6">
                        <label for=""> Chưa xem:</label>
                        <table id="" class="table table-bordered table-striped" style="">
                            <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="35%">Tên phòng ban</th>
                                    <th width="">Người nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsphong)}{$i=1}
                                {foreach $dsphong as $p}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td>{$p.sTenPB}</td>
                                        <td>
                                            {if !empty($dscanbo)}
                                                {foreach $dscanbo as $cb}
                                                    {if $cb.FK_iMaPhongHD==$p.PK_iMaPB && empty($cb.sThoiGianDoc)}
                                                        <p>{$cb.sHoTen} - <i style="color:{($cb.sThoiGianDoc)?'#00bcd4':'red'};"class="">{($cb.sThoiGianDoc)?date_time2($cb.sThoiGianDoc):'Chưa xem'}</i></p>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </td>
                                        <!--<td>{$p.sGhiChu}</td>-->
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
							<BR>
							<!--
                            <tfoot>
                                <tr>
                                    <td colspan="4" style="margin-top: 30px">
                                        <a href="{$url}soanthaothongtin?id={$mavanban}" class="btn btn-primary">Sửa</a>
                                    </td>
                                </tr>
                            </tfoot>-->
                        </table>
						</div>
						<div class="col-md-6">
						<label for=""> Đã xem:</label>
                        <table id="" class="table table-bordered table-striped" style="">
                            <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="35%">Tên phòng ban</th>
                                    <th width="">Người nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsphong)}{$i=1}
                                {foreach $dsphong as $p}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td>{$p.sTenPB}</td>
                                        <td>
                                            {if !empty($dscanbo)}
                                                {foreach $dscanbo as $cb}
                                                    {if $cb.FK_iMaPhongHD==$p.PK_iMaPB && !empty($cb.sThoiGianDoc)}
                                                        <p>{$cb.sHoTen} - <i style="color:{($cb.sThoiGianDoc)?'#00bcd4':'red'};"class="">{($cb.sThoiGianDoc)?date_time2($cb.sThoiGianDoc):'Chưa xem'}</i></p>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </td>
                                        <!--<td>{$p.sGhiChu}</td>-->
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
							
                        </table>
						</div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.thongtin th').css('background', 'rgb(225, 248, 251)');
        $('.thongtin th').css('color', '#333');
        $('.mau').css('background', 'rgb(255, 255, 255)');
        $('.mau').css('color', '#333');
        $('.thongtin').css('border', '1px solid rgb(225, 248, 251)');
        $(document).on('click','a[name=ykien]',function(){
            $('.an').toggle();
            $('.an').removeClass('hide');
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        $("#backLink").click(function() {
            event.preventDefault();
            history.back(1);
        });
    });
</script>
