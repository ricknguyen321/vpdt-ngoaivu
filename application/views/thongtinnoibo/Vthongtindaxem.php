<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách thông tin đã xem <b>({$count})</b>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="get" class="form-horizontal">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea name="noidung" id="" rows="3" class="form-control" placeholder="Nhập nội dung cần tìm">{($noidung)?$noidung:''}</textarea>
                            </div>
                            
                        </div>
                        
                        <div class="col-md-6">
                            <div class="row">
                                    <div class="form-group">
                                    <label for="" class="col-md-3">Từ ngày:</label>
                                    <div class="col-md-9">
                                        <input type="text" name="tungay" value="{($tungay)?$tungay:''}" class="form-control datepic datemask" placeholder="dd/mm/yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group">
                                    <label for="" class="col-md-4 text-center" >Đến ngày:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="denngay" value="{($denngay)?$denngay:''}" class="form-control datepic datemask" placeholder="dd/mm/yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-md-12">
                                <select name="phongban" onchange="this.form.submit()" id="" class="form-control select2">
                                    <option value="">-- Chọn phòng ban --</option>
                                    {if !empty($ds_phongban)}
                                        {foreach $ds_phongban as $pb}
                                            <option value="{$pb.PK_iMaPB}" {($phongban)?($phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <select name="canbo" onchange="this.form.submit()" id="" class="form-control select2">
                                    <option value="">-- Chọn người soạn thảo --</option>
                                    {if !empty($ds_canbo)}
                                        {foreach $ds_canbo as $cb}
                                            <option value="{$cb.PK_iMaCB}" {($canbo)?($canbo==$cb.PK_iMaCB)?'selected':'':''}>{$cb.sHoTen}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-sm">Tìm kiếm</button> &nbsp;&nbsp;&nbsp;<a href="soanthaothongtin"><button type="button" class="btn btn-primary btn-sm">Soạn thảo TTNB</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
						<div class="" style="">
                            {$phantrang}
                        </div>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
									<th width="50px" class="text-center">STT</th>
                                    <th width="12%">Người gửi</th>
                                    <th width="">Trích yếu - Nội dung</th>
									<th width="40%">Ý kiến phản hồi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($danhsach)}{$i=1}
                                    {foreach $danhsach as $dl}
										{$dsfile = laydsfilettnb($dl.PK_iMaVB)}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">
												{$cb = layTTCB($dl.FK_iMaCB_Nhap)}
												<p><b>{$cb[0].sHoTen}</b></p>
												{date_time2($dl.sNgayNhap)}
												</td>
                                            <td>
												<div style="max-height:250px;  overflow:auto; margin-bottom: 10px">
													<p style="text-align: justify; margin-right:5px">{$dl.sMoTa}</p>
													{if !empty($dl.sNoiDungChiDao)}<p style="text-align: justify; margin-right:5px">{$dl.sNoiDungChiDao}</p>{/if}
												</div>
												<hr style="margin:10px">
												{if !empty($dsfile)}
													<b style="">Đính kèm:</b><br>
													{foreach $dsfile as $f}
														<p style="margin-bottom:3px">
														<a style="color:brown" target="_blank" href="{$f.sDuongDan}">{$f.sTenFile}</a></p>
													{/foreach}
												{/if}
												<span class="pull-right" style="margin-top:10px"><a style="color:brown" href="{$url}xemchitiet?id={$dl.PK_iMaVB}"><button type="button" value="" class="btn btn-primary btn-xs" style="min-width: 100px; text-align:left">Chi tiết</button></a> 
												&nbsp;
												<button type="submit" name="markxoa" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" value="{$dl.PK_iMaNguoiNhanVB}" onclick="return confirm('Bạn có muốn xóa thông tin này không?')" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
												</span>
											</td>
											{$dsgopy = array_reverse(layGopY($dl.PK_iMaVB))}
											<td >
												<div style="max-height:300px;  overflow:auto">
												{foreach $dsgopy as $gy}
													</p>"{$gy.sNoiDung}" </p>
													{$dstenfiles = explode(';', $gy.sTenFile)}
													{$dsduongdan = explode(';', $gy.sDuongDan)}
													{$j=0}
													{foreach $dstenfiles as $file}
														<p style="margin-bottom:3px"><a href="{$dsduongdan[$j++]}" target="_blank" style="color:brown">{substr($file, 0, 90)}</a></p>
													{/foreach}
													<span style="float:right"><b>{$gy.sHoTen}</b> {date_time2($gy.sThoiGian)}</span><br><hr style="margin:10px">
												{/foreach}
												</div>
											</td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">
                            {$phantrang}
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
