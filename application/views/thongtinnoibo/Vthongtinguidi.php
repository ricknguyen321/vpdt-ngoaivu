<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách thông tin gửi đi <b>({$count})</b>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="get" class="form-horizontal">
                <div class="row">
                    <div class="col-md-7">
                        <textarea name="noidung" id="" rows="5" placeholder="Nhập nội dung tìm kiếm" class="form-control">{($noidung)?$noidung:''}</textarea>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="col-md-4" for="">Từ ngày: </label>
                            <div class="col-md-8">
                                <input type="text" name="tungay" class="form-control datepic datemask" placeholder="dd/mm/yyyy" value="{($tungay)?$tungay:''}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="">Đến ngày: </label>
                            <div class="col-md-8">
                                <input type="text" name="denngay" class="form-control datepic datemask" placeholder="dd/mm/yyyy" value="{($denngay)?$denngay:''}">
                            </div>
                        </div>
                        
                        <button class="btn btn-primary btn-sm">Tìm kiếm</button>&nbsp;&nbsp;&nbsp;<a href="soanthaothongtin"><button type="button" class="btn btn-primary btn-sm">Soạn thảo TTNB</button></a>
                    </div>
                </div>
            </form>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
						<div class="">
                            {$phantrang}
                        </div>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Gửi lúc</th>
                                    <th width="">Trích yếu - Nội dung</th>
                                    <th width="40%" class="text-center">Ý kiến phản hồi</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsdulieu)}{$i=1}
                                {foreach $dsdulieu as $dl}
									{$dsfile = laydsfilettnb($dl.PK_iMaVB)}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
										<td class="text-center">{date_time($dl.sNgayNhap)}</td>
                                        <td>
											<div style="max-height:250px;  overflow:auto; margin-bottom: 10px">
												<p style="text-align: justify; margin-right:5px">{$dl.sMoTa}</p>
												{if !empty($dl.sNoiDungChiDao)}<p style="text-align: justify; margin-right:5px">{$dl.sNoiDungChiDao}</p>{/if}
											</div>
											{if !empty($dsfile)}
												<b>Đính kèm:</b><br>
												{foreach $dsfile as $f}
													<p style="margin-bottom:3px">
													<a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></p>
												{/foreach}
											{/if}
											<span class="pull-right"><a class="tin" href="{$url}xemchitietguidi?id={$dl.PK_iMaVB}"><button type="button" value="" class="btn btn-primary btn-xs" style="min-width: 100px; text-align:left">Chi tiết</button></a>
											&nbsp;
											<button type="submit" name="markxoa" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" value="{$dl.PK_iMaVB}" onclick="return confirm('Bạn có muốn xóa thông tin này không?')" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
											</span>
										</td>
                                        
										{$dsgopy = array_reverse(layGopY($dl.PK_iMaVB))}
										<td>
											<div style="max-height:300px;  overflow:auto">
											{foreach $dsgopy as $gy}
												<p>"{$gy.sNoiDung}" </p> 
												{$dstenfiles = explode(';', $gy.sTenFile)}
												{$dsduongdan = explode(';', $gy.sDuongDan)}
												{$j=0}
												{foreach $dstenfiles as $file}
													<p style="margin-bottom:3px"><a href="{$dsduongdan[$j++]}" target="_blank" style="color:brown">{substr($file, 0, 90)}</a></p>
												{/foreach}
												<span style="float:right"><b>{$gy.sHoTen}</b> {date_time2($gy.sThoiGian)}</span><br><hr style="margin:10px">
											{/foreach}
											</div>
										</td>
											
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
						<div class="pull-right">
                            {$phantrang}
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        // $('th').css('vertical-align', 'middle');
        $('td').css('vertical-align', 'middle');
    });
</script>