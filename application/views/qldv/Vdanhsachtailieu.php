<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách tài liệu
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="" method="get">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="nhiemky" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn nhiệm kỳ --</option>
                                        {if !empty($nhiemky)}
                                            {foreach $nhiemky as $nk}
                                                <option value="{$nk.sNhiemKy}" {($g_nhiemky)?($g_nhiemky==$nk.sNhiemKy)?'selected':'':''}>{$nk.sNhiemKy}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="khoa" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn khoa --</option>
                                        {if !empty($khoa)}
                                            {foreach $khoa as $k}
                                                <option value="{$k.sKhoa}" {($g_khoa)?($g_khoa==$k.sKhoa)?'selected':'':''}>{$k.sKhoa}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="kyhop" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn kỳ họp --</option>
                                        {if !empty($kyhop)}
                                            {foreach $kyhop as $kh}
                                                <option value="{$kh.sKyHop}" {($g_kyhop)?($g_kyhop==$kh.sKyHop)?'selected':'':''}>{$kh.sKyHop}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="tieude" class="form-control" value="{($g_tieude)?$g_tieude:''}" placeholder="Nhập tên phụ lục">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="noidung" id="" rows="3" class="form-control" placeholder="Nhập nội dung tìm kiếm">{($g_noidung)?$g_noidung:''}</textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="linhvuc" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lĩnh vực --</option>
                                        {if !empty($linhvuc)}
                                            {foreach $linhvuc as $lv}
                                                <option value="{$lv.linhVuc_id}" {($g_linhvuc)?($g_linhvuc==$lv.linhVuc_id)?'selected':'':''}>{$lv.linhVuc_name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="phongban" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn phòng chủ trì --</option>
                                        {if !empty($phongban)}
                                            {foreach $phongban as $pb}
                                                <option value="{$pb.PK_iMaPB}" {($g_phongban)?($g_phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="lanhdao" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lãnh đạo --</option>
                                        {if !empty($lanhdao)}
                                            {foreach $lanhdao as $ld}
                                                <option value="{$ld.PK_iMaCB}" {($g_lanhdao)?($g_lanhdao==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-success btn-xs" name="timkiem" value="timkiem">Tìm kiếm</button>
                                </div>
                            </div>
                    </form>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                         {if $quyen==3}<button name="capnhatthutu" value="capnhatthutu" class="btn btn-primary btn-sm pull-right">Cập nhật thứ tự</button>{/if}
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="44%">Nội dung</th>
                                    {if $quyen==3}<th width="6%">STT</th>{/if}
                                    <th width="10%" class="text-center">Tên phụ lục</th>
                                    <th width="25% class="text-center">Phòng chủ trì</th>
                                    {if $quyen==3}<th width="10%" >File</th>{/if}
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieu)}{$i=1} {$j=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td >{$dl.qlvDetails_desc}
                                                {if !empty($dl.dsfile)}
                                                <!-- {$d=0} -->
                                                    {foreach $dl.dsfile as $f}
                                                    <!-- {$d++} -->
                                                        {($d>1)?', ':''}<a target="_blank" data-toggle="tooltip" data-placement="top" title="{substr($f.qlvFile_path,27)}" href="{$url}{$f.qlvFile_path}" class="tin1">[Xem File]</a>
                                                    {/foreach}
                                                {/if}
                                            </td>
                                            {if $quyen==3}<td>
                                                <input type="text" name="madauviec[]" class="hide" value="{$dl.qlvDetails_id}">
                                                <input type="text" name="stt[]" value="{($dl.sapxep)?$dl.sapxep:$j++}" class="form-control">
                                            </td>{/if}
                                            <td class="text-center">{$dl.sTieuDe}</td>
                                            <td class="text-center">{if isset($mangphong[$dl.main_department])}{$mangphong[$dl.main_department]}{/if}</td>
                                            {if $quyen==3}<td class="text-center">
                                                <a href="{$url}teptintailieu?ma={$dl.qlvDetails_id}" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Thêm file"><i class="fa fa-search-plus"></i></a>
                                                <button type="submit" name="xoa" value="{$dl.qlvDetails_id}" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="return confirm('Bạn có chắc muốn xóa');"><i class="fa fa-trash"></i></button>
                                                <button type="button" name="sua" value="{$dl.qlvDetails_id}" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil-square-o"></i></button>
                                            </td>{/if}
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">
                        {$phantrang}
                    </div>
                </div>
                <div class="col-md-12">
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <form action="" method="post">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Sửa thông tin tài liệu</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="text" name="tailieu" placeholder="Tên phụ lục" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <textarea name="noidung" id="" placeholder="Nội dung phụ lục" rows="4" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Đóng</button>
                                        <button type="submit" name="suatailieu" value="" class="btn btn-primary btn-sm">Lưu lại</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $(document).on('click','button[name=sua]',function(){
            var ma = $(this).val();
            $('button[name=suatailieu]').val(ma);
            $.ajax({
                url:url,
                type:'POST',
                data:{
                    action:'suatailieu',
                    ma:ma
                },
                success:function(repon){
                    var result = JSON.parse(repon);
                    $('input[name=tailieu]').val(result[0]['sTieuDe']);
                    $('textarea[name=noidung]').val(result[0]['qlvDetails_desc']);
                }
            });
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.control-label').css('text-align', 'left');
    });
</script>