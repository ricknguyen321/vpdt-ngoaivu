<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Nội dung trích yếu</th>
                                    <th width="30%">Ý kiến đề xuất hạn</th>
                                    <th width="30%">Trình tự truyền nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieu)} {$i=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td> 
                                                <a href="{$url}chitietdauviec/{$dl.qlvDetails_id}" class="tin1"><b>{$dl.qlvDetails_desc}</b></a>
                                                <p>Người nhập: {$mangCB[$dl.input_per]}</p>
                                                <p>Ngày nhập: {date_select($dl.qlvDetails_date)}</p>
                                                <p>Hạn văn bản: {($dl.qlvDetails_limit_time>'1970-01-01')?date_select($dl.qlvDetails_limit_time):''}</p>
                                            </td>
                                            <td>
                                                <p><span style="color:red;">*</span> <i>Đề xuất gia hạn của đ/c </i><b>{$mangCB[$dl.Fk_iMaCB_Chuyen]}:</b></p>
                                                <p><i>{$dl.sLyDo_Chuyen}</i></p>
                                                <p style="color: red;">Hạn cũ: {($dl.han_thongke>'1970-01-01')?date_select($dl.han_thongke):''}</p>
                                                <p>Hạn mới : {date_select($dl.sHanDeXuat)}</p>
                                            </td>
                                            <td>
                                                {foreach $dl.chuyennhan as $cn}
                                                <p>{$mangCB[$cn.FK_iMaCB_Nhan]}</p>
                                                <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                                {/foreach}
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>