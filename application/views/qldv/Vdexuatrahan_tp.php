<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Nội dung trích yếu</th>
                                    <th width="30%">Ý kiến đề xuất hạn</th>
                                    <th width="30%">Trình tự truyền nhận</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieu)} {$i=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td> 
                                                <a href="{$url}chitietdauviec/{$dl.qlvDetails_id}" class="tin1"><b>{$dl.qlvDetails_desc}</b></a>
                                                <p>Người nhập: {$mangCB[$dl.input_per]}</p>
                                                <p>Ngày nhập: {date_select($dl.qlvDetails_date)}</p>
                                                <p>Hạn văn bản: {($dl.qlvDetails_limit_time>'1970-01-01')?date_select($dl.qlvDetails_limit_time):''}</p>
                                            </td>
                                            <td>
                                                <p><span style="color:red;">*</span> <i>Đề xuất gia hạn của đ/c </i><b>{$mangCB[$dl.Fk_iMaCB_Chuyen]}:</b></p>
                                                <p><i>{$dl.sLyDo_Chuyen}</i></p>
                                                <p style="color: red;">Hạn cũ: {($dl.han_thongke>'1970-01-01')?date_select($dl.han_thongke):''}</p>
                                                <p>Hạn đề xuất : {date_select($dl.sHanDeXuat)}</p>
                                                <p class="hide"><input type="text" name="hanchuyenvien_{$dl.qlvDetails_id}" value="{date_select($dl.sHanDeXuat)}" data-id="{$dl.PK_iMaDX}"></p>
                                                <button type="button" name="dexuatrahan" value="{$dl.qlvDetails_id}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal">Đề xuất gia hạn</button>
                                                <button type="button" name="tuchoirahan" value="{$dl.qlvDetails_id}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">Từ chối</button>
                                            </td>
                                            <td>
                                                {foreach $dl.chuyennhan as $cn}
                                                <p>{$mangCB[$cn.FK_iMaCB_Nhan]}</p>
                                                <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                                {/foreach}
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModal">
                        <div class="modal-dialog" role="document">
                            <form action="" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModal">Đề xuất gia hạn</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Ngày đề xuất:</label>
                                        <input type="text" required="" name="handexuat"  class="form-control datepic datemask" placeholder="dd/mm/yyyy">
                                        <input type="text" class="hide" name="madexuat">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="control-label ykienchidao">Lý do đề xuất:</label>
                                        <textarea  required="" name="lydodexuat" class="form-control" rows="4" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Đóng</button>
                                    <button type="submit" name="dexuat" value="" class="dexuatdexuat btn btn-primary btn-sm">Đề xuất</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=dexuatrahan]',function(){
            $('.dexuatdexuat').attr('name', 'dexuat');
            $('.dexuatdexuat').text('Đề xuất');
            $('.ykienchidao').text('Lý do đề xuất:');
            var mavanban  = $(this).val();
            var handexuat = $('input[name=hanchuyenvien_'+mavanban+']').val();
            var madexuat  = $('input[name=hanchuyenvien_'+mavanban+']').attr('data-id');
            $('textarea[name=lydodexuat]').removeAttr('required');
            $('input[name=handexuat]').attr('required', true);
            $('input[name=madexuat]').val(madexuat);
            $('input[name=handexuat]').val(handexuat);
            $('button[name=dexuat]').val(mavanban);
        });
        $(document).on('click','button[name=tuchoirahan]',function(){
            $('.dexuatdexuat').attr('name', 'tuchoi');
            $('.dexuatdexuat').text('Từ chối');
            $('.ykienchidao').text('Lý do từ chối:');
            var mavanban  = $(this).val();
            var handexuat = $('input[name=hanchuyenvien_'+mavanban+']').val();
            var madexuat  = $('input[name=hanchuyenvien_'+mavanban+']').attr('data-id');
            $('textarea[name=lydodexuat]').attr('required',true);
            $('input[name=madexuat]').val(madexuat);
            $('input[name=handexuat]').removeAttr('required');
            $('input[name=handexuat]').val(handexuat);
            $('button[name=tuchoi]').val(mavanban);
        });
    });
</script>