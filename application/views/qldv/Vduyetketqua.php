<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Kết quả hoàn thành
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <form action="" method="get">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <select name="nhiemky" id="" class="form-control select2" style="width: 100%">
                                    <option value="">-- Chọn nhiệm kỳ --</option>
                                    {if !empty($nhiemky)}
                                        {foreach $nhiemky as $nk}
                                            <option value="{$nk.sNhiemKy}" {($g_nhiemky)?($g_nhiemky==$nk.sNhiemKy)?'selected':'':''}>{$nk.sNhiemKy}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select name="khoa" id="" class="form-control select2" style="width: 100%">
                                    <option value="">-- Chọn khoa --</option>
                                    {if !empty($khoa)}
                                        {foreach $khoa as $k}
                                            <option value="{$k.sKhoa}" {($g_khoa)?($g_khoa==$k.sKhoa)?'selected':'':''}>{$k.sKhoa}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="kyhop" id="" class="form-control select2" style="width: 100%">
                                    <option value="">-- Chọn kỳ họp --</option>
                                    {if !empty($kyhop)}
                                        {foreach $kyhop as $kh}
                                            <option value="{$kh.sKyHop}" {($g_kyhop)?($g_kyhop==$kh.sKyHop)?'selected':'':''}>{$kh.sKyHop}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="loaivanban" id="" class="form-control select2" style="width: 100%">
                                    <option value="">-- Chọn loại văn bản --</option>
                                    {if !empty($loaivanban)}
                                        {foreach $loaivanban as $key => $lvb}
                                            <option value="{$key}" {($g_loaivanban)?($g_loaivanban==$key)?'selected':'':''}>{$lvb}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-primary btn-xs" name="locdulieu" value="locdulieu">Lọc dữ liệu</button>
                            </div>
                        </div>
                    </form>
                </div><br>
                <div class="row">
                    <form  method="post" action="" class="form-horizontal">
                        <div class="col-md-12">
                            <table id="" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width:5%" class="text-center">STT</th>
                                        <th width="40%">Nội dung</th>
                                        <th width="15%">Tiêu đề</th>
                                        <th width="15%">Loại văn bản</th>
                                        <th width="10%">File</th>
                                        <th width="15%">Thời gian</th>
                                        <th width="10%">
                                            <button class="btn btn-success btn-xs" name="duyet" value="duyet">Duyệt</button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {if !empty($dulieu)}{$i=1}
                                        {foreach $dulieu as $dl}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td><a class="tin1" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="{$mangphongban[$dl.department_id]}">{$dl.qlvDetails_desc}</a></td>
                                                <td class="text-center">{$dl.sTieuDe}</td>
                                                <td class="text-center">{($dl.loaivanban==1)?'Tài liệu':'Kiến nghị'}</td>
                                                <td class="text-center"><a class="tin1" target="_blank" href="{$url}{$dl.qlvFile_path}">[Xem file]</a></td>
                                                <td class="text-center">{date_time($dl.qlvFile_date)}</td>
                                                <td class="text-center">
                                                    <input type="checkbox" name="mafile[{$dl.qlvFile_id}]" value="{$dl.qlvFile_id}">
                                                    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" type="button" name="tralai" value="{$dl.qlvFile_id}"> Trả lại</button>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    {/if}
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <form action="" method="post">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Lý do trả lại</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                        <textarea name="noidungtralai" id="" required="" placeholder="Nhập lý do trả lại" rows="3" class="form-control"></textarea>
                                        <input type="text" class="hide" name="mafile_an" value="">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Đóng</button>
                                        <button type="submit" name="guitralai" value="guitralai" class="btn btn-primary btn-xs">Gửi trả lại</button>
                                    </div>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=tralai]',function(){
            var ma = $(this).val();
            $('input[name=mafile_an]').val(ma);
        });
    });
</script>
