<div class="content-wrapper">
    <!-- Main content -->
    <section class="content" style="font-size:13px;">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" class="form-horizontal hide">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:0px!importent;">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Số Ký hiệu:</label>
                                        <div class="col-md-8">
                                            <input type="text" name="sokyhieu" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Lãnh đạo chủ trì:</label>
                                        <div class="col-md-8">
                                            <select name="lanhdao" style="width:100%" id="" class="form-control select2">
                                                <option value="">Chọn lãnh đạo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Người ký:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="nguoiky" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ngày văn bản:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="ngayvanban" class="form-control datepic datemask">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ngày ban hành từ:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="ngaybanhanhtu" class="form-control datepic datemask">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ngày ban hành đến:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="ngaybanhanhden" class="form-control datepic datemask">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Trích yếu:</label>
                                    <div class="col-md-8">
                                        <textarea name="trichyeu" class="form-control" rows="2"></textarea>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" value="timkiem" name="timkiem">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th rowspan="3" style="width:5%" class="text-center">STT</th>
                                <th rowspan="3" class="text-center">Số ký hiệu</th>
                                <th rowspan="3" class="text-center">Ngày ban hành</th>
                                <th rowspan="3" class="text-center" width="30%">Trích yếu</th>
                                <th colspan="6" class="text-center">Thoi dõi đôn đốc</th>
                            </tr>
                            <tr>
                                <th rowspan="2" class="text-center">Tổng số đầu việc</th>
                                <th colspan="2" class="text-center">Đang thực hiện</th>
                                <th colspan="2" class="text-center">Đã hoàn thành</th>
                                <th rowspan="2" class="text-center">Sắp đến hạn</th>
                            </tr>
                            <tr>
                                <th class="text-center" width="8%">Trong hạn</th>
                                <th class="text-center" width="8%">Quá hạn</th>
                                <th class="text-center" width="8%">Trong hạn</th>
                                <th class="text-center" width="8%">Quá hạn</th>
                            </tr>
                        </thead>
                        <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                <tr>
                                    <th class="text-center" colspan="3">Tổng Số Văn Bản</th>
                                    <th class="text-center">{$tongvanban}</th>
                                    <th class="text-center">{if isset($tong_tongVBChau)} <a href="{$url}dauviecchitiettong" class="tin">{$tong_tongVBChau}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($tong_danglam_tronghan)}<a href="{$url}dauviecchitiettong?trangthai=1&dunghan=1" class="tin">{$tong_danglam_tronghan}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($tong_danglam_quahan)}<a href="{$url}dauviecchitiettong?trangthai=1&quahan=1" class="tin">{$tong_danglam_quahan}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($tong_hoanthanh_tronghan)}<a href="{$url}dauviecchitiettong?trangthai=2&dunghan=1" class="tin">{$tong_hoanthanh_tronghan}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($tong_hoanthanh_quahan)}<a href="{$url}dauviecchitiettong?trangthai=2&quahan=1" class="tin">{$tong_hoanthanh_quahan}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($tong_sapden_han)}<a href="{$url}dauviecchitiettong?trangthai=3" class="tin">{$tong_sapden_han}</a>{else}0{/if}</th>
                                </tr>
                                {foreach $dsvanban as $vb}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td>{$vb.qlv_code}</td>
                                        <td>{date_select($vb.qlv_date)}</td>
                                        <td><a href="{$url}viewldvDetails/{$vb.qlv_id}" style="color:black!important;">{$vb.qlv_noidung}</a></td>
                                        <td class="text-center">{if isset($mang_tongVBChau[$vb.qlv_id])}<a href="{$url}dauviecchitiet?qlv_id={$vb.qlv_id}" class="tin">{$mang_tongVBChau[$vb.qlv_id]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_danglam_tronghan[$vb.qlv_id])}<a href="{$url}dauviecchitiet?qlv_id={$vb.qlv_id}&trangthai=1&dunghan=1" class="tin">{$mang_danglam_tronghan[$vb.qlv_id]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_danglam_quahan[$vb.qlv_id])}<a href="{$url}dauviecchitiet?qlv_id={$vb.qlv_id}&trangthai=1&quahan=1" class="tin">{$mang_danglam_quahan[$vb.qlv_id]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_hoanthanh_tronghan[$vb.qlv_id])}<a href="{$url}dauviecchitiet?qlv_id={$vb.qlv_id}&trangthai=2&dunghan=1" class="tin">{$mang_hoanthanh_tronghan[$vb.qlv_id]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_hoanthanh_quahan[$vb.qlv_id])}<a href="{$url}dauviecchitiet?qlv_id={$vb.qlv_id}&trangthai=2&quahan=1" class="tin">{$mang_hoanthanh_quahan[$vb.qlv_id]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_sapden_han[$vb.qlv_id])}<a href="{$url}dauviecchitiet?qlv_id={$vb.qlv_id}&trangthai=3" class="tin">{$mang_sapden_han[$vb.qlv_id]}</a>{else}0{/if}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>