<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc chờ xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="" method="get">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="nhiemky" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn nhiệm kỳ --</option>
                                        {if !empty($nhiemky)}
                                            {foreach $nhiemky as $nk}
                                                <option value="{$nk.sNhiemKy}" {($g_nhiemky)?($g_nhiemky==$nk.sNhiemKy)?'selected':'':''}>{$nk.sNhiemKy}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="khoa" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn khoa --</option>
                                        {if !empty($khoa)}
                                            {foreach $khoa as $k}
                                                <option value="{$k.sKhoa}" {($g_khoa)?($g_khoa==$k.sKhoa)?'selected':'':''}>{$k.sKhoa}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="kyhop" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn kỳ họp --</option>
                                        {if !empty($kyhop)}
                                            {foreach $kyhop as $kh}
                                                <option value="{$kh.sKyHop}" {($g_kyhop)?($g_kyhop==$kh.sKyHop)?'selected':'':''}>{$kh.sKyHop}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="tieude" class="form-control" value="{($g_tieude)?$g_tieude:''}" placeholder="Nhập tên phụ lục, cậu hỏi">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="noidung" id="" rows="3" class="form-control" placeholder="Nhập nội dung tìm kiếm">{($g_noidung)?$g_noidung:''}</textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="linhvuc" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lĩnh vực --</option>
                                        {if !empty($linhvuc)}
                                            {foreach $linhvuc as $lv}
                                                <option value="{$lv.linhVuc_id}" {($g_linhvuc)?($g_linhvuc==$lv.linhVuc_id)?'selected':'':''}>{$lv.linhVuc_name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="phongban" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn phòng chủ trì --</option>
                                        {if !empty($phongban)}
                                            {foreach $phongban as $pb}
                                                <option value="{$pb.PK_iMaPB}" {($g_phongban)?($g_phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="lanhdao" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lãnh đạo --</option>
                                        {if !empty($lanhdao)}
                                            {foreach $lanhdao as $ld}
                                                <option value="{$ld.PK_iMaCB}" {($g_lanhdao)?($g_lanhdao==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-success btn-xs" name="timkiem" value="timkiem">Tìm kiếm</button>
                                </div>
                            </div>
                    </form>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Nội dung trích yếu</th>
                                    <th width="30%">Ý kiến</th>
                                    <th width="30%">Chỉ đạo</th>
                                    <th style="width:5%" class=""><button type="submit" name="duyet" value="duyet" class="btn btn-success btn-xs">Duyệt</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieuOng)}{$ii=1}
                                    {foreach $dulieuOng as $dlo}
                                        <tr>
                                            <th class="text-center">{intToRoman($ii++)}</th>
                                            <th colspan="4">{$dlo.qlv_noidung} {if !empty($dlo.qlvFile_path)} <a href="{$url}{$dlo.qlvFile_path}" target="_blank">Xem File</a>{/if}</th>
                                        </tr>
                                        {if !empty($dulieu)} {$i=1}
                                            {foreach $dulieu as $dl}
                                                {if $dlo.qlv_id == $dl.qlv_id}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td> 
                                                        <a href="{$url}chitietdauviec/{$dl.qlvDetails_id}" class="tin1"><b>{$dl.qlvDetails_desc}</b></a>
                                                        <p>Người nhập: {$mangCB[$dl.input_per]}</p>
                                                        <p>Ngày nhập: {date_select($dl.qlvDetails_date)}</p>
                                                        <p>Hạn văn bản: {($dl.qlvDetails_limit_time>'1970-01-01')?date_select($dl.qlvDetails_limit_time):''}</p>
                                                        <p><span style="color:red;">*</span> <i>Nội dung chỉ đạo của đ/c </i><b>{$mangCB[$dl.FK_iMaCB_Gui]}:</b></p>
                                                        <p><i>{$dl.sNoiDungChuyen}</i></p>
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <select name="chicucpho_{$dl.qlvDetails_id}" class="form-control chicucpho select2" style="width: 100%" id="{$dl.qlvDetails_id}">
                                                                <option value="0">-- Chọn chi cục phó --</option>
                                                                {if !empty($chicucpho)}
                                                                    {foreach $chicucpho as $ccp}
                                                                        <option value="{$ccp.PK_iMaCB}">{$ccp.sHoTen}</option>
                                                                    {/foreach}
                                                                {/if}
                                                            </select>
                                                        </p> 
                                                        <p>
                                                            <select name="phongchutri_{$dl.qlvDetails_id}" class="form-control phongchutri select2" style="width: 100%" id="{$dl.qlvDetails_id}">
                                                                <option value="0">-- Chọn phòng chủ trì --</option>
                                                                {if !empty($phongbanchicuc)}
                                                                    {foreach $phongbanchicuc as $pb}
                                                                        <option value="{$pb.PK_iMaPB}">{$pb.sTenPB}</option>
                                                                    {/foreach}
                                                                {/if}
                                                            </select>
                                                        </p> 
                                                        <p>
                                                            <input type="text" name="phongph_{$dl.qlvDetails_id}" value=""  class="hide">
                                                            <button type="button" disabled name="phoihop" value="{$dl.qlvDetails_id}" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-sm phongph_{$dl.qlvDetails_id}">Chọn phối hợp</button>
                                                        </p>
                                                        <p>
                                                            <input type="text" name="hanxuly_{$dl.qlvDetails_id}" value="{($dl.sHanXuLy>'1970-01-01')?date_select($dl.sHanXuLy):''}" placeholder="hạn xử lý" class="form-control">
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <input type="text" class="form-control" name="tenchicucpho_{$dl.qlvDetails_id}" placeholder="Nội dung chuyển chi cục phó">
                                                        </p>
                                                        <p>
                                                            <input type="text" class="form-control" name="chutri_{$dl.qlvDetails_id}" value="" placeholder="Nội dung chuyển phòng chủ trì">
                                                        </p>
                                                        <p>
                                                            <textarea name="tenphongph_{$dl.qlvDetails_id}" id="" class="form-control" rows="3" placeholder="Nội dung chuyển phòng phối hợp"></textarea>
                                                        </p>
                                                    </td>
                                                    <td class="text-center">
                                                        <span style="color:red;">Chọn duyệt</span>
                                                        <input type="checkbox" name="mavanban[]" class="mavanban_{$dl.qlvDetails_id}" disabled value="{$dl.qlvDetails_id}">
                                                    </td>
                                                </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Phòng ban phối hợp</h4>
                            </div>
                            <div>
                            {if !empty($phongbanchicuc)}
                                {foreach $phongbanchicuc as $ph}
                                    <div class="col-md-6">
                                        <input type="checkbox" class="Checkbox phongphoihop" data-id="{$ph.sTenPB}" name="phongphoihop" value="{$ph.PK_iMaPB}" id="id{$ph.PK_iMaPB}"> {$ph.sTenPB}
                                    </div>
                                {/foreach}
                            {/if}
                            </div>
                            <div class="modal-footer">
                                <input type="text" name="ma" class="hide">
                                <button type="button" name="ghilai" class="btn btn-primary btn-sm" data-dismiss="modal">Ghi Lại</button>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change','.chicucpho',function(){
            var mavanban = $(this).attr('id');
            var chicucpho = $('select[name=chicucpho_'+mavanban+']').val();
            var phongchutri = $('select[name=phongchutri_'+mavanban+']').val();
            var tenchicucpho = $(this).find("option:selected").text();
            if(chicucpho>0)
            {
                $('input[name=tenchicucpho_'+mavanban+']').val('Chuyển đ/c '+tenchicucpho+' chủ trì giải quyết');
            }
            else{
                $('input[name=tenchicucpho_'+mavanban+']').val('');
            }
            if(chicucpho==0 && phongchutri==0)
            {
                $('.mavanban_'+mavanban+'').attr('checked', false); 
                $('.mavanban_'+mavanban+'').attr("disabled", 'disabled');
            }
            else{
                $('.mavanban_'+mavanban+'').removeAttr('disabled');
            }

        });
        $(document).on('click','button[name=phoihop]',function(){
            var mavanban = $(this).val();
            $('input[name=ma]').val(mavanban);
            var str1 = $('input[name=phongph_'+mavanban+']').val();
            var p_chutri = $('select[name=phongchutri_'+mavanban+']').val();
            $('#id'+p_chutri).prop('disabled', true);
            if(str1 !=''){
                $('.phongphoihop').each(function(i){
                    if(str1.indexOf($(this).val()) != -1){
                        $('#id'+$(this).val()).prop('checked', true);
                    }
                });
            } 
            $('#id'+p_chutri).prop('checked', false);
        });
        $(document).on('hide.bs.modal','.modal', function () {
            $("input[type=checkbox]").removeAttr('disabled');
        });
        $(document).on('change','.phongchutri',function(){
            var mavanban = $(this).attr('id');
            var maphong  = $(this).val();
            var tenphong = $(this).find("option:selected").text();
            if(maphong>0)
            {
                $('input[name=chutri_'+mavanban+']').val(tenphong+' chủ trì giải quyết.');
                $('.phongph_'+mavanban+'').removeAttr('disabled');
            }
            else{
                $('input[name=phongph_'+mavanban+']').val('');
                $('input[name=chutri_'+mavanban+']').val('');
                $('textarea[name=tenphongph_'+mavanban+']').val('');
                $('.phongph_'+mavanban+'').attr("disabled", 'disabled');
            }
            var chicucpho = $('select[name=chicucpho_'+mavanban+']').val();
            var phongchutri = $('select[name=phongchutri_'+mavanban+']').val();

            if(chicucpho==0 && phongchutri==0)
            {
                $('.mavanban_'+mavanban+'').attr("disabled", 'disabled');
                $('.mavanban_'+mavanban+'').attr('checked', false); 
            }
            else{
                $('.mavanban_'+mavanban+'').removeAttr('disabled');
            }
        });
        $(document).on('click','button[name=ghilai]',function () {
            var tenpb = $('.Checkbox:checked').map(function() {
                return $(this).attr('data-id');
            }).get().join(',');
            var mapb = $('.Checkbox:checked').map(function() {
                return this.value;
            }).get().join(',');
            var mavanban = $('input[name=ma]').val();
            if(mapb.length>0)
            {
                $('input[name=phongph_'+mavanban+']').val(mapb);
                $('textarea[name=tenphongph_'+mavanban+']').val(' '+tenpb+' phối hợp giải quyết');
            }
            else{
                $('input[name=phongph_'+mavanban+']').val('');
                $('textarea[name=tenphongph_'+mavanban+']').val('');
            }
            $('.Checkbox').attr('checked', false); 
        });
    });
</script>