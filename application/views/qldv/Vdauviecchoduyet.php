<script src="{$url}assets/js/qldv/dauviec.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc chờ duyệt
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Nội dung trích yếu</th>
                                    <th width="30%">Lãnh đạo chủ trì</th>
                                    <th width="30%">Ý kiến trả lại</th>
                                    <th style="width:5%" class=""><button type="submit" name="duyet" value="duyet" class="btn btn-success btn-xs">Duyệt</button></th>
                                </tr>
                            </thead>
                            <tbody>
                               {if !empty($dulieu)} {$i=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td> 
                                                <b>{$dl.qlvDetails_desc}</b>
                                                <p>Người nhập: {$mangCB[$dl.input_per]}</p>
                                                <p>Ngày nhập: {date_select($dl.qlvDetails_date)}</p>
                                                <p>Hạn văn bản: {date_select($dl.qlvDetails_limit_time)}</p>
                                            </td>
                                            <td>
                                                <p>Lãnh đạo: {if isset($lanhdao[$dl.FK_iMaCB_LanhDao])}{$lanhdao[$dl.FK_iMaCB_LanhDao]}{/if}</p>
                                                <p>Phòng chủ trì: {$mangPB[$dl.main_department]}</p>
                                                {if !empty($dl.department_id)}
                                                    {$mangph = explode(',',$dl.department_id)}
                                                    <p>Phòng phối hợp: 
                                                        {foreach $mangph as $ph}
                                                            {$mangPB[$ph]},
                                                        {/foreach}
                                                    </p>
                                                {/if}
                                            </td>
                                            <td>
                                                <textarea name="noidungtralai{$dl.qlvDetails_id}" id="{$dl.qlvDetails_id}" class="form-control noidungtralai" rows="5" placeholder="Nhập ý kiến trả lại (NẾU CÓ)"></textarea>
                                                <input type="text" class="hide" name="{$dl.qlvDetails_id}" value="{$dl.input_per}">
                                                <input type="text" name="hanvanban[{$dl.qlvDetails_id}]" class="hide" value="{$dl.qlvDetails_limit_time}">
                                                <input type="text" name="lanhdao[{$dl.qlvDetails_id}]" class="hide" value="{$dl.FK_iMaCB_LanhDao}">
                                            </td>
                                            <td class="text-center">
                                                <span style="color:red;">Chọn duyệt</span>
                                                <input type="checkbox" name="mavanban[]" value="{$dl.qlvDetails_id}">
                                                <button type="button" class="btn btn-primary btn-xs"  id="ma_{$dl.qlvDetails_id}" name="tralai" value="{$dl.qlvDetails_id}">Trả lại</button>
                                            </td>
                                        </tr>
                                    {/foreach}
                               {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change','.noidungtralai', function(){
            var noidung = $(this).val();
            var ma = $(this).attr('id');
            if(noidung.length>0)
            {
                $('#ma_'+ma).attr('type','submit');
            }
            else{
                $('#ma_'+ma).attr('type','button');
            }
        });
    });
</script>