<script src="{$url}assets/js/qldv/qldv.js"></script>
<script src="{$url}assets/js/qldv/addqldvDetails.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Cập nhật đầu việc chi tiết
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-2">Tiêu đề đầu việc</label>

                                <div class="col-sm-10">
                                    {$qldvDetail[0]['qlvDetails_desc']}
                                </div>
                            </div>
                        </div> 
                        
                       <div class="col-md-12">   
                            <div class="form-group">
                                <label for="" class="col-sm-2 text-primary">LV chính </label>
                                <div class="col-sm-4">
                                    {if isset($manglinhvuc[$qldvDetail[0]['linhVuc_id']])}{$manglinhvuc[$qldvDetail[0]['linhVuc_id']]}{/if}
                                </div>
                                <label for="" class="col-sm-2 text-primary">LV chi tiết</label>
                                <div class="col-sm-4">
                                    {if isset($manglinhvucsub[$qldvDetail[0]['linhvuc_sub']])}{$manglinhvucsub[$qldvDetail[0]['linhvuc_sub']]}{/if}
                                </div>
                            </div>
                        </div>
                            
                        <div class="col-sm-12" style="margin-top: -20px;">
                            <hr style="border: 1px dashed #ccc">
                        </div>
                            <div class="themsau">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Nội dung đầu việc *</label>
                                        <div class="col-sm-9">
                                            <textarea name="noidunghop" required class="form-control" rows="2" style="border: 1px solid red;">{$qldvDetailSub[0]['qlvDetails_desc']}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label">C.hỏi - P.lục *</label>
                                        <div class="col-sm-8 ">
                                           <input type="text" name="tieude" value="{($qldvDetailSub[0]['sTieuDe'])?$qldvDetailSub[0]['sTieuDe']:''}" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label">Lãnh đạo* </label>
                                        <div class="col-sm-8">
                                            <select name="lanhdao" id="lanhdao" required class="form-control select2" style="width:100%">
                                                <option value="0">-- Chọn lãnh đạo --</option>
                                                {if !empty($lanhdao)}
                                                    {foreach $lanhdao as $ld}
                                                        <option value="{$ld.PK_iMaCB}" {($sllanhdao)?($sllanhdao==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Đơn vị chủ trì *</label>
                                        <div class="col-sm-9">
                                            <select name="main_department" id="main_department" required class="form-control select2" style="width:100%">
                                                <option value="0">-- Chọn đơn vị --</option>
                                                {if !empty($main_depart)}
                                                {foreach $main_depart as $p}
                                                <option value="{$p.PK_iMaPB}"{($qldvDetailSub)?($qldvDetailSub[0]['main_department']==$p.PK_iMaPB)?"selected":"":""}>{$p.sTenPB}</option>
                                                {/foreach}{/if}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label">Hạn xử lý</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control datepic datemask" value="{($qldvDetailSub[0]['qlvDetails_limit_time'])?date_select($qldvDetailSub[0]['qlvDetails_limit_time']):''}" name="qlvDetails_limit_time">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Đơn vị phối hợp</label>
                                    <div class="col-sm-9">
                                        <select name="department_id[]"  multiple="" class="form-control select2" style="width:100%">
                                            {if !empty($main_depart)}
                                                {foreach $main_depart as $p}
                                                    {if !empty($qldvDetailSub[0]['department_id'])}
                                                        {$mang = explode(',',$qldvDetailSub[0]['department_id'])}
                                                        <option value="{$p.PK_iMaPB}" {foreach $mang as $m} {($m==$p.PK_iMaPB)?'selected':''}{/foreach}>{$p.sTenPB}</option>
                                                    {else}
                                                        <option value="{$p.PK_iMaPB}">{$p.sTenPB}</option>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Cử tri</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{($qldvDetailSub[0]['sCuTri'])?$qldvDetailSub[0]['sCuTri']:''}" name="cutri">
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-7">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Phân công trả lời của TP</label>
                                    <div class="col-sm-9">
                                        <textarea name="noidungphancong" id="" class="form-control" rows="2">{($qldvDetailSub)?$qldvDetailSub[0]['sNoiDungPhanCong']:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phân công trả lời của TP</label>
                                    <div class="col-sm-8">
                                        <select name="trangthaiphancong" class="form-control select2" style="width:100%">
                                            <option value="0">-- Chọn chủ tri phối hợp --</option>
                                            <option value="1" {($qldvDetailSub[0]['iTrangThaiPhanCong'])?($qldvDetailSub[0]['iTrangThaiPhanCong']==1)?'selected':'':''}>SNV chủ trì</option>
                                            <option value="2" {($qldvDetailSub[0]['iTrangThaiPhanCong'])?($qldvDetailSub[0]['iTrangThaiPhanCong']==2)?'selected':'':''}>SNV phối hợp</option></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" name="luulai" value="luulai" class="btn btn-primary">Cập nhật</button> 
                                <a href="javascript: history.back(1)" class="btn btn-default" data-original-title="" title="">Quay lại &gt;&gt;</a> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

