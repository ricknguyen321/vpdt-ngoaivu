<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">  
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    Thêm mới kế hoạch tuần
                </span>
            </div>
            <div class="col-md-7 sl3">
                <span class="4 font" style="font-weight: 550">
                    Tuần công tác: 
                </span>
                <label class="8 sl2">
                    <select name="tuan" style="" class="form-control select2" onchange="this.form.submit()">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </label>
            </div>
            <div class="col-md-1">
                <span class="font">
                    Biểu số 1
                </span>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-sm-2">Nội dung công việc <span style="color: red">*</span></div>
                        <div class="col-sm-10">
                            <textarea name="kh_noidung" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc">{($giatri)?$giatri[0]['kh_noidung']:''}</textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-sm-5">Cán bộ thực hiện <span style="color: red">*</span></div>
                        <div class="col-sm-7">
                            {if $iQuyenHan_DHNB == 8}
                            <select name="canbo_id" required class="form-control" style="width:100%;border:1px solid #3c8dbc;" >
                                {foreach $canbo as $cb}
                                    {if $cb.PK_iMaCB == $PK_iMaCB}
                                        <option value="{$cb.PK_iMaCB}" {($giatri)?($giatri[0]["canbo_id"]==$cb.PK_iMaCB)?"selected":"":""}> {$cb.sHoTen}</option>
                                        }
                                    {/if}
                                {/foreach}
                            {else}
                            <select name="canbo_id" required class="form-control select2" style="width:100%;border:1px solid #3c8dbc;" >
                            <option value="">-- Chọn Cán Bộ --</option>
                                {foreach $canbo as $cb}
                                    <option value="{$cb.PK_iMaCB}" {($giatri)?($giatri[0]["canbo_id"]==$cb.PK_iMaCB)?"selected":"":""}> {$cb.sHoTen}</option>
                                {/foreach}
                            {/if}
                            </select>
                        </div>
                        {if $iQuyenHan_DHNB < 8}
                        <div class="col-sm-5" style="padding-top: 3px;">
                            Cán bộ phối hợp:
                        </div> 
                        <div class="col-sm-7" style="padding-top: 3px;">
                            <input type="button" name="themphoihop"  value="Chọn phối hợp" data-toggle="modal" data-target="#themphoihop" class="btn btn-primary">&nbsp;&nbsp;                
                        </div>
                        <input type="hidden" name="chuyenvien_ph" value="{($giatri)?$giatri[0]['phoihop']:''}"> 
                        {/if}
                    </div>  
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-1">Hạn xử lý </div>
                        <div class="col-sm-2" style="padding-left: 42px;">
                           <input type="text" class="form-control datepic datemask" id="ngay_han" value="{if !empty($giatri) && $giatri[0]['ngay_han'] >'2017-01-01'}{date_select($giatri[0]['ngay_han'])}{/if}" name="ngay_han" style="border: 1px solid #3c8dbc;" >
                        </div>
                        <div class="col-sm-2">
                           <input type="radio" class="minimal" name="loai_kh" value="2" style="border: 1px solid #3c8dbc;" {if empty($giatri) ||  $giatri[0]['loai_kh'] == 2} checked {/if} >  &nbsp;&nbsp;<b>Nhiệm vụ đột xuất Lãnh đạo giao</b>
                        </div>
                        <div class="col-sm-3">
                           <input type="radio" class="minimal" name="loai_kh" value="3" style="border: 1px solid #3c8dbc;" {if !empty($giatri) &&  $giatri[0]['loai_kh'] == 3} checked {/if} >  &nbsp;&nbsp;<label>Nhiệm vụ thường xuyên </label><i>(Công việc có tính chất lặp lại theo tuần)</i>
                        </div>
                        <div class="col-sm-3">
                            <input type="radio" class="minimal" name="loai_kh" value="5" style="border: 1px solid #3c8dbc;" {if !empty($giatri) &&  $giatri[0]['loai_kh'] == 5} checked {/if} >  &nbsp;&nbsp;<label>Công việc khác</label><i> (Công đoàn,...)</i>
                        </div>
                        <div class="col-sm-1">
                            <input type="hidden" name="kehoach_ma" value="{$kehoach_ma}">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </div> 
                </div>
                </form>
                <div class="row" style="height: 12px">

                    <div class="modal fade" id="themphoihop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header" style="padding: 8px;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1">Chọn cán bộ phối hợp</h4>
                          </div>
                          <div class="modal-body">
                              <div class="col-md-12">
                                    {foreach $canbo as $cb}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="checkbox" name="chuyenvien" value="{$cb.PK_iMaCB}"> {$cb.sHoTen} 
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ghilai()">Ghi lại</button>
                            <!-- <button type="button" class="btn btn-primary">Lưu lại</button> -->
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="modal fade" id="chuahoanthanh" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1"></h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="canbo_id">
                                    <input type="text" class="hide" name="ngay_han">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Nội dung:</label>
                                    <textarea name="lydo" class="form-control" rows="4" ></textarea>
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Chọn file:</label>
                                    <input type="file" class="form-control" name="files[]">
                                  </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luuchuahoanthanh" value="Chưa hoàn thành" class="btn btn-primary">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                      </div>
                    </div>
                </div>
                
                <div class="modal fade" id="TP_danhgia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title1" >Đánh giá kết quả công việc</h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="canbo_id">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Đánh giá:</label>
                                    <textarea name="lydo" class="form-control" rows="4" ></textarea>
                                  </div>
                                  <div class="form-group">
                                        <input type="radio" name="chatluong" value="1" checked="checked"> Chất lượng đảm bảo &nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="chatluong" value="2"> Không đảm bảo chất lượng <br/>
                                        <input type="radio" name="chatluong" value="3"> Có tính sáng tạo, đổi mới, hiệu quả &nbsp;
                                  </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luutruongphongdanhgia" value="Chưa hoàn thành" class="btn btn-primary">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                      </div>
                </div>

                <div class="modal fade" id="TP_danhgia1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title1" >Công việc chưa hoàn thành</h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="canbo_id">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Lý do:</label>
                                    <textarea name="lydo" class="form-control" rows="4" ></textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luutruongphongdanhgia" value="Chưa hoàn thành" class="btn btn-primary">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                    </div>
                </div>

                <div class="modal fade" id="TP_duyet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title2" ></h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Lý do:</label>
                                    <textarea name="lydo" class="form-control" rows="4" ></textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="tpduyet" value="Chưa hoàn thành" class="btn btn-primary">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" style="font-size: 14px;font-weight: bold;">Danh mục kế hoạch công việc-kết quả giải quyết tuần:
                        {foreach $arr as $key => $value}
                            {$giatri = $key+1}
                            {if $week == $giatri} {$week} ({$value}) {/if}                            
                        {/foreach}
                    </div>
                </div>
                <form method="post">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center" rowspan="2">STT</th>
                                    {if $iQuyenHan_DHNB < 7}
                                    <th width="8%" class="text-center" rowspan="2">Cán bộ thực hiện</th>
                                    {/if}
                                    <th width="8%" class="text-center" rowspan="2">Cán Bộ chỉ đạo</th>
                                    <th width="5%" class="text-center" rowspan="2">Ngày nhận</th>
                                    <th width="5%" class="text-center" rowspan="2">Số ký hiệu</th>
                                    <th width="" class="text-center" rowspan="2">Nội dung</th>
                                    <th width="5%" class="text-center" rowspan="2">Hạn xử lý</th>
                                    <th width="8%" class="text-center" colspan="2">Kết quả thực hiện</th>
                                </tr>
                                <tr>
                                    <th width="4%" class="text-center" >Hoàn Thành Trong Hạn</th>
                                    <th width="4%" class="text-center">Hoàn Thành Quá Hạn</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>I</th>
                                <th colspan="8">NHIỆM VỤ LÃNH ĐẠO GIAO, NHIỆM VỤ THƯỜNG XUYÊN, NHIỆM VỤ KHÁC</th>
                            </tr>
                            {$i = 1}
                            {foreach $list_kh as $kh}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                {if $iQuyenHan_DHNB < 7}
                                <td width="8%" class="text-center">
                                    {foreach $canbo as $cb}
                                        {if $kh.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}
                                    {foreach $canbo as $cb}
                                        {if $kh.active == 0 && $kh.lanhdao_id == $cb.PK_iMaCB}
                                            <i>(LĐ giao việc {$cb.sHoTen})</i>
                                        {/if}
                                    {/foreach}
                                </td>
                                {/if}
                                <td>
                                    {if $kh.loai_kh ==5}
                                        Nhiệm vụ khác
                                    {/if}
                                    {if $kh.loai_kh ==3 or $kh.loai_kh ==4}
                                        Nhiệm vụ thường xuyên
                                    {/if}
                                    {if $kh.loai_kh ==2 && $iQuyenHan_DHNB == 8}
                                       {if $kh.canbo_id == $PK_iMaCB} Lãnh đạo giao {else} Phối hợp giải quyết {/if}
                                    {/if}
                                    {if $kh.loai_kh ==2 && $iQuyenHan_DHNB < 8}
                                       Lãnh đạo giao
                                    {/if}
                                </td>
                                <td>{date_select($kh.ngay_nhan)}</td>
                                <td class="text-center">{$kh.vanban_skh}</td>
                                <td class="text-left" align="text-left">
                                    {if ($PK_iMaCB == $kh.user_input && $kh.active == 1)||($kh.active == 0 && $PK_iMaCB == $kh.lanhdao_id)}
                                    <a href="kehoachcongtac?id={$kh.kh_id}">{$kh.kh_noidung}</a>
                                    {if $iQuyenHan_DHNB == 7 && $PK_iMaCB == $kh.lanhdao_id && $kh.ykien_tp !=''}
                                        <br><b>Ý kiến của lãnh đạo :</b> <i>{$kh.ykien_tp}</i>
                                    {/if}
                                    <button type="submit" name="xoabanghi" onclick="return confirm('Bạn có chắc chắn xóa công việc')" value="{$kh.kh_id}"> Xóa Công việc</button> 
                                    {else}
                                    {$kh.kh_noidung}
                                    {/if}
                                    {if $kh.active > 1}<hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />{/if}
                                    {if $kh.khokhan_vuongmac != ''}<br>
                                    <label>Lý do chưa hoàn thành: </label>
                                    <span style="color: red">{$kh.khokhan_vuongmac} - {date_select($kh.ngay_hoanthanh)}</span>
                                    {/if}
                                    
                                    {if $kh.sangtao ==2}<br><span style="color: red;font-weight: 900">Có sáng tạo đổi mới</span>
                                    {/if}

                                    {if $kh.ket_qua_hoanthanh != ''}<br>
                                    <label>Kết quả hoàn thành: </label>
                                    <span style="color: blue">{$kh.ket_qua_hoanthanh} - {date_select($kh.ngay_hoanthanh)}</span>
                                    {/if} 
                                    {if isset($kh.filename)}<br>
                                    <label>Tệp tin đính kèm: </label>
                                    {foreach $kh.filename as $file}
                                        {if $file !=''}<a href="kehoach_2018/{$file}"> [File] </a>{/if}
                                    {/foreach}
                                    {/if}
                                    
{$kiemtra=1}
{if isset($kh.file_phoihop)}
{if isset($kh.file_phoihop.0.canbo_id)}<br><i> Danh sách phối hợp:</i>{/if}
    {foreach $kh.file_phoihop as $phoihop}    
        <br> - <b>{$array_cb[$phoihop.canbo_id]}: </b>{$phoihop.lydo} &nbsp;&nbsp; 
        {if $phoihop.file_upload !=''}<a href="kehoach_2018/{$phoihop.file_upload}">[File] </a>{/if}

        {if $phoihop.canbo_id == $PK_iMaCB && $phoihop.active == 1 && $phoihop.chutri == 2}{$kiemtra=2}{/if}
    {/foreach}
{/if}

                                    {if $kh.danh_gia != ''}<hr style="margin-bottom: 6px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    <label>Đánh giá của lãnh đạo: </label>
                                    <span style="color: blue">{$kh.danh_gia} - {date_select($kh.ngay_danhgia)}</span>
                                    {/if}

                                    <div class="danhgia">
                                        <br />
                                        {if $iQuyenHan_DHNB >= 7 && $kh.active ==1 && $kh.vanban_id =="" && $kiemtratuan == 2}
                                        
                                        <input type="hidden" name="han_xl_{$kh.kh_id}" value="{$kh.ngay_han}">
                                        <input type="hidden" name="{$kh.kh_id}" value="{$kh.canbo_id}">
                                        {if $kh.loai_kh == 3}
                                        <input type="checkbox" name="loai_kh" value="4"> <label>Kết thúc nhiệm vụ thường xuyên</label><i> (Tích chọn nút này nhiệm vụ thường xuyên sẽ kết thúc và không tự động chuyển sang tuần mới)</i><br>
                                        {/if}
                                        {if $kiemtra == 1}
                                        <input type="button" name="hoanthanh" id="{$kh.kh_id}" value="Hoàn thành" data-toggle="modal" data-target="#chuahoanthanh">&nbsp;&nbsp;

                                        <input type="button" name="chuahoanthanh" id="{$kh.kh_id}" data-toggle="modal" data-target="#chuahoanthanh" value="Chưa hoàn thành">
                                        {/if}
                                        {/if}
                                        {if $kiemtra == 1 && $iQuyenHan_DHNB == 8 && $kh.active ==2 && $kh.vanban_id =="" && $kh.chutri == 2 && $kiemtratuan == 2}
                                        <input type="button" name="hoanthanh" id="{$kh.kh_id}" value="Hoàn thành" data-toggle="modal" data-target="#chuahoanthanh">&nbsp;&nbsp;

                                        <input type="button" name="chuahoanthanh" id="{$kh.kh_id}" data-toggle="modal" data-target="#chuahoanthanh" value="Chưa hoàn thành">
                                        {/if}

                                         {if $iQuyenHan_DHNB == 7 && $kh.active ==2 && $kh.vanban_id =="" && $kh.lanhdao_id == $PK_iMaCB}
                                        <br/>
    
                                        <input type="button" name="danhgia" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_danhgia" value="Hoàn thành">&nbsp;&nbsp;
                                        <input type="button" name="danhgia1" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_danhgia1" value="Chưa hoàn thành">
                                        {/if}

                                        {if $iQuyenHan_DHNB < 7 && $kh.active ==2 && $kh.vanban_id ==""}
                                        <br/>
    
                                        <input type="button" name="danhgia" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_danhgia" value="Hoàn thành">&nbsp;&nbsp;
                                        <input type="button" name="danhgia1" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_danhgia1" value="Chưa hoàn thành">
                                        {/if}

                                        {if $iQuyenHan_DHNB < 7 && $kh.active ==0 && $kh.vanban_id ==""}
                                            <input type="button" name="khongduyet" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_duyet" value="Không duyệt">&nbsp;&nbsp;
                                            <input type="button" name="duyetviec" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_duyet" value="Duyệt">
                                        {/if}

                                    </div>
                                </td>
                                <td class="text-center">{if $kh.ngay_han >'2017-01-01'}{date_select($kh.ngay_han)}{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh.active>=2 || $kiemtra == 2}X{/if}</td>
                                <td class="text-center" style="color: red;font-weight: bold;">{if $kh.active==1 && $kiemtra == 1}X{/if}</td>
                                
                            </tr>
                            {/foreach}
                            <tr>
                                <th>II</th>
                                <th colspan="8">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ</th>
                            </tr>
                            <tr>
                                <th>A</th>
                                <th colspan="8">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ CÓ YÊU CẦU VĂN BẢN ĐẦU RA</th>
                            </tr>
                            {foreach $list_kh1 as $kh1}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                {if $iQuyenHan_DHNB < 7}
                                <td width="8%" class="text-center">
                                    {foreach $canbo as $cb}
                                        {if $kh1.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}
                                </td>
                                {/if}
                                <td>
                                     {foreach $canbo as $cb}
                                        {if $kh1.lanhdao_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}
                                </td>
                                <td>{date_select($kh1.ngay_nhan)}</td>
                                <td class="text-center">{$kh1.vanban_skh}</td>
                                <td class="text-left" align="text-left">
                                    {if $kh1.active >= 1  && $kh1.vanban_skh != ''}
                                        {if $kh1.thuc_hien ==3 && $kh1.trangthai_vbd == 1}
                                        <a href="giaymoichuyenvienchutrixuly/{$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==2 && $kh1.trangthai_vbd == 1}
                                        <a href="giaymoichuyenvienxuly/{$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==1 && $kh1.trangthai_vbd == 1}
                                        <a href="quytrinhgiaymoi/{$kh1.vanban_id}">
                                        {/if}

                                        {if $kh1.thuc_hien ==3 && $kh1.trangthai_vbd == 0}
                                        <a href="chuyenvienchutrixuly?vbd={$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==2 && $kh1.trangthai_vbd == 0}
                                        <a href="chuyenvienxuly?vbd={$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==1 && $kh1.trangthai_vbd == 0}
                                        <a href="quatrinhxuly/{$kh1.vanban_id}">
                                        {/if}
                                    {$kh1.kh_noidung}</a>
                                    {else}
                                    {$kh1.kh_noidung}
                                    {/if}
                                    {if $kh.sangtao ==2}<br/><span style="color: red;font-weight: 900">Có sáng tạo đổi mới</span>
                                    {/if}
                                </td>
                                <td class="text-center">{if $kh1.ngay_han >'2017-01-01'}{date_select($kh1.ngay_han)}{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh1.qua_han==1}X{/if}</td>
                                <td class="text-center" style="color: red;font-weight: bold;">{if $kh1.qua_han==2}X{/if}</td>
                                
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <table id="" class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                 <th colspan="4">B.  CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ KHÔNG CÓ VĂN BẢN ĐẦU RA</th>
                            </tr>
                            <tr>
                                <td class="text-center" style="font-weight:700;font-size:14px  ">VB Chủ Trì Hoàn Thành Trong Hạn</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">VB Chủ Trì Hoàn Thành Quá Hạn</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">VB Phối Hợp Hoàn Thành Trong Hạn</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">VB Phối Hợp Hoàn Thành Quá Hạn</td>
                            </tr>
                            <tr>
                                <td class="text-center" style="font-weight:700;font-size:14px  ">
                                    {if $tong_vbct_dh >0}<a href="{$url}dsphongchutri?PK_iMaVBDen={$ds_idctdh}"> {/if}
                                        {$tong_vbct_dh}
                                    {if $tong_vbct_dh >0}</a>{/if}</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">
                                    {if $tong_vbct_qh >0}<a href="{$url}dsphongchutri?PK_iMaVBDen={$ds_idctqh}"> {/if}
                                        {$tong_vbct_qh}
                                    {if $tong_vbct_qh >0}</a>{/if}
                                </td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">
                                    {if $tong_vbph_dh >0}<a href="{$url}dsphoihop?PK_iMaVBDen={$ds_idphdh}"> {/if}
                                        {$tong_vbph_dh}
                                    {if $tong_vbph_dh >0}</a>{/if}
                                </td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">
                                    {if $tong_vbph_qh >0}<a href="{$url}dsphoihop?PK_iMaVBDen={$ds_idphqh}"> {/if}
                                        {$tong_vbph_qh}
                                    {if $tong_vbph_qh >0}</a>{/if}
                                </td>
                            </tr>
                        
                            
                        </tbody>
                        </table>
                    </div>
                </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 text-left" style="margin-bottom: 5px;">
                            {if $iQuyenHan_DHNB >= 7}
                            <a href="dskehoachcongtac/{$week}"><input type="button" class="btn btn-danger" style="font-weight: bold;" name="danhgiakehoachtuan" value="Biểu số 5: Tổng hợp kết quả chấm điểm tuần {$week} của phòng"></a>
                            {/if}
                        </div>
                        <div class="col-md-6 text-right" style="margin-bottom: 5px;">
                            {if $iQuyenHan_DHNB >= 7 && $kiemtratuan == 2}
                            <a href="xemkehoachcongtac/{$week}"><input type="button" class="btn btn-danger" style="font-weight: bold;" name="danhgiakehoachtuan" value="Biểu số 2: đánh giá chấm điểm tuần {$week}"></a>
                            {/if}
                            {if $iQuyenHan_DHNB < 7 && $kiemtratuan == 2}
                            <a href="dskehoachcongtac/{$week}"><input type="button" class="btn btn-danger" style="font-weight: bold;" name="danhgiakehoachtuan" value="Biểu số 2: đánh giá chấm điểm tuần {$week}"></a>
                            {/if}
                        </div>
                </div>
            </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var url = window.location.href;
        $(document).on('click','input[name=hoanthanh]',function(){
            var id = $(this).attr('id');
            var canbo = $('input[name='+id+']').val();
            var ngay_han = $('input[name=han_xl_'+id+']').val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=ngay_han]').val(ngay_han);
            $('input[name=trangthai]').val(1);
            $('.modal-title').text("Hoàn thành công việc");
        });

        $(document).on('click','input[name=chuahoanthanh]',function(){
            var id = $(this).attr('id');
            var canbo = $('input[name='+id+']').val();
            var ngay_han = $('input[name=han_xl_'+id+']').val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=ngay_han]').val(ngay_han);
            $('input[name=trangthai]').val(2);
            $('.modal-title').text("Lý do chưa hoàn thành công việc");
            
        });

        $(document).on('click','input[name=danhgia]',function(){
            var id = $(this).attr('id');
            var chatluong = $('input[name=chatluong]:checked').val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=trangthai]').val(1);
        });

        $(document).on('click','input[name=danhgia1]',function(){
            var id = $(this).attr('id');
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=trangthai]').val(2);
        });

         $(document).on('click','input[name=duyetviec]',function(){
            var id = $(this).attr('id');
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(1);
            $('.modal-title2').text("Duyệt đề xuất của phó phòng");
        });
         $(document).on('click','input[name=khongduyet]',function(){
            var id = $(this).attr('id');
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(2);
            $('.modal-title2').text("Không duyệt đề xuất của phó phòng");
        });

       
    });

    function ghilai(){ 
        var str_id ='';
        $.each($("input[name='chuyenvien']:checked"), function(){
            str_id = str_id + "["+$(this).val()+"],";
        });
         str_id = str_id.substring(0, str_id.length - 1);
        $('input[name=chuyenvien_ph]').val(str_id);
    }     
</script>
