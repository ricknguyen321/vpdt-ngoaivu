<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-8">
                <span class="font">
                    Nhập mới kế hoạch thực hiện nhiệm vụ trọng tâm tháng
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn tháng công tác: 
                </span>
                <label class="8 sl2">
                    <select name="month" style="" class="form-control" onchange="this.form.submit()">
                            {$i=1}
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>Tháng: {$i} </option>
                            {/for}
                    </select>
                </label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <tr>
                                <td width="10%" align="left">
                                    <label>Nhiệm vụ chính</label>
                                </td>
                                <td width="75%" align="left" colspan="4">
                                    <textarea name="noidung" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung nhiệm vụ chính"></textarea>
                                </td>
                                <td width="15%" align="left" >
                                    <label>Lãnh đạo sở chỉ đạo</label>
                                
                                    <select name="ld_so" class="form-control select2">  
                                    <option value="0">Chọn lãnh đạo</option>                          
                                {foreach $lanhdaoso as $ldso}
                                <option value="{$ldso['PK_iMaCB']}" >{$ldso['sHoTen']}</option>
                                {/foreach}
                            </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%" align="left">
                                   <label>LĐ phòng chỉ đạo</label>
                                </td>
                                <td width="15%" align="left">
                                    <select name="ld_phong[]" class="form-control select2" multiple="multiple">
                                        {foreach $canbophong as $ldphong}
                                        {if $ldphong['iQuyenHan_DHNB'] == 6 or $ldphong['iQuyenHan_DHNB'] == 7}
                                        <option value="{$ldphong['PK_iMaCB']}" >{$ldphong['sHoTen']}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                </td>
                                <td width="10%" align="left">
                                    <label>CV thực hiện</label>
                                </td>
                                <td width="15%" align="left">
                                     <select name="user_id[]" class="form-control select2" multiple="multiple"> 
                                        {foreach $canbophong as $ldphong}
                                        {if $ldphong['iQuyenHan_DHNB'] == 8 or $ldphong['iQuyenHan_DHNB'] == 7}
                                        <option value="{$ldphong['PK_iMaCB']}" {if $ldphong['PK_iMaCB'] == $giatri['ld_phong']} selected {/if}>{$ldphong['sHoTen']}</option>
                                        {/if}
                                        {/foreach}
                                    </select>
                                </td>
                                <td width="15%" align="left">
                                    <label>ngày bắt đầu</label>                                
                                     <input type="text" value="" name="date_start" class="form-control datepic datemask" placeholder="Ngày bắt đầu">
                                    <script>
                                        $(document).ready(function() {
                                            //Date picker
                                            $('.datepic').datepicker({
                                                autoclose: true
                                            });
                                            //Datemask dd/mm/yyyy
                                            $(".datemask").inputmask("dd/mm/yyyy");
                                        });
                                    </script>
                                </td>
                                <td width="15%" align="left">
                                    <label>ngày kết thúc</label>                             
                                     <input tabindex="8" type="text" value="" name="date_end" class="form-control datepic datemask" placeholder="Ngày kết thúc">
                                    <script>
                                        $(document).ready(function() {
                                            //Date picker
                                            $('.datepic').datepicker({
                                                autoclose: true
                                            });
                                            //Datemask dd/mm/yyyy
                                            $(".datemask").inputmask("dd/mm/yyyy");
                                        });
                                    </script>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="themsau"></div>
                        <div class="col-md-12 text-right">
                            <div class="col-sm-8">
                                <button type="button" name="themmoi" value="1" class="btn btn-success"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung chi tiết</b>
                                <input type="hidden" name="soluong" id="soluong" value="1">                          
                            </div>
                        </div>
                    </div>

                   
                    <div class="row" style="height: 7px"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button> 
                        </div>
                    </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

<script>
    $(document).ready(function() {
        $(document).on('click','button[name=themmoi]',function(){
            var tong = $(this).val();
            var noidung = '';
                noidung+= '<div class="col-md-12"><table id="" class="table table-bordered table-striped"><tr><td width="10%" align="left"><span>Nhiệm vụ chi tiết</span></td><td width="75%" align="left" colspan="4"><textarea name="noidung'+tong+'" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung nhiệm vụ chính"></textarea></td><td width="15%" align="left" ><span>Lãnh đạo sở chỉ đạo</span><select name="ld_so'+tong+'" class="form-control"><option value="0">Chọn lãnh đạo</option>{foreach $lanhdaoso as $ldso}<option value="{$ldso['PK_iMaCB']}" {if $ldso['PK_iMaCB'] == $giatri['ld_so']} selected {/if}>{$ldso['sHoTen']}</option>{/foreach}</select></td></tr>';
                noidung+= '<tr><td width="10%" align="left"><span>LĐ phòng chỉ đạo</span></td><td width="15%" align="left"><select name="ld_phong'+tong+'[]" class="form-control select2" multiple="multiple">{foreach $canbophong as $ldphong}{if $ldphong['iQuyenHan_DHNB'] == 6 or $ldphong['iQuyenHan_DHNB'] == 7}<option value="{$ldphong['PK_iMaCB']}" {if $ldphong['PK_iMaCB'] == $giatri['ld_phong']} selected {/if}>{$ldphong['sHoTen']}</option>{/if}{/foreach}</select></td><td width="10%" align="left"><span>CV thực hiện</span></td><td width="15%" align="left"><select name="user_id'+tong+'[]" class="form-control select2" multiple="multiple">{foreach $canbophong as $ldphong}{if $ldphong['iQuyenHan_DHNB'] == 8 or $ldphong['iQuyenHan_DHNB'] == 7}<option value="{$ldphong['PK_iMaCB']}" {if $ldphong['PK_iMaCB'] == $giatri['ld_phong']} selected {/if}>{$ldphong['sHoTen']}</option>{/if}{/foreach} </select></td><td width="15%" align="left"><span>ngày bắt đầu</span><input type="text" value="" name="date_start'+tong+'" class="form-control datepic datemask" placeholder="Ngày bắt đầu"></td><td width="15%" align="left"><span>ngày kết thúc</span><input tabindex="8" type="text" value="" name="date_end'+tong+'" class="form-control datepic datemask" placeholder="Ngày kết thúc"></td></tr></table></div>';
                
            $('.themsau').before(noidung);
            tong = parseInt(tong) +1;
            $('button[name=themmoi]').val(tong);
            $('#soluong').val(tong);
            //datepic dd/mm/yyyy
            $('.datepic').datepicker({
              autoclose: true
            });
            $(".select2").select2();
            //Datemask dd/mm/yyyy
            $(".datemask").inputmask("dd/mm/yyyy");
            $('.control-label').css('text-align', 'left');
        });
    });
</script>

