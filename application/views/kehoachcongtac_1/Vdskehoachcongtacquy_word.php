<style type="text/css">
    .border_main{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        padding-left: 15px;
    }
    .border_main1{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
        padding-left: 15px;
    }
</style>
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 20%;vertical-align: top; font-size: 14px">
                                    Sở Ngoại Vụ HÀ NỘI<BR>
                                    <span style="text-transform: uppercase;">
                                        <b>{$phongban[$phong_id]}</b>
                                    </span>
                                </td>
                                <td colspan="13" style="text-align: center;width: 80%">
                                    <B>TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ, CHẤM ĐIỂM MỨC ĐỘ HOÀN THÀNH NHIỆM VỤ HÀNG QUÝ</B><BR>
                                    <b><span style="padding: 30px 0 30px 0">Quý:{$quy}</span></b>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="border_main" rowspan="3" >STT</th>
                                    <th class="border_main" rowspan="3" >Người thực hiện</th>
                                    <th width="5%" class="border_main" rowspan="3" >Tổng số việc theo kế hoạch</th>
                                    <th width="30%" class="border_main" colspan="6" >Công việc đã hoàn thành trong quý</th>
                                    <th width="15%" class="border_main" colspan="3" >Công việc còn tồn cuối quý</th>
                                    <th width="8%" class="border_main" rowspan="3"  >Tỉ lệ hoàn thành (%)</th>
                                    <th width="6%" class="border_main" rowspan="3" >Điểm trung bình của quý</th>
                                    <th width="10%" class="border_main1" rowspan="3" >Kết quả phân loại mức độ hoàn thành nhiệm vụ</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="border_main" rowspan="2" >Số lượng</th>
                                    <th width="10%" class="border_main" colspan="2" >Thời gian</th>
                                    <th width="10%" class="border_main" colspan="2"  >Chất lượng</th>
                                    <th width="5%" class="border_main"  rowspan="2" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="border_main" rowspan="2" >Số lượng</th>
                                    <th width="5%" class="border_main"  rowspan="2">Trong hạn</th>
                                    <th width="5%" class="border_main"  rowspan="2">Chậm tiến độ</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="border_main" >Trong hạn</th>
                                    <th width="5%" class="border_main" >Quá hạn</th>
                                    <th width="5%" class="border_main" >Đảm bảo</th>
                                    <th width="5%" class="border_main" >Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="border_main"><b>I</b></td>
                                    <td colspan="14" class="border_main1" style=" text-align: left;padding-left: 5px"><b>Lãnh đạo phòng, đơn vị</b></td>
                                </tr>

                                {$jj=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key <= 10}
                                <tr>
                                    <td class="border_main">{$jj++}</td>
                                    <td class="border_main" style=" text-align: left;padding-left: 5px">
                                            {$giatri[11]}
                                    </td>
                                    <td class="border_main">{$giatri[0]}</td>
                                    <td class="border_main">{$giatri[1]}</td>
                                    <td class="border_main">{$giatri[2]}</td>
                                    <td class="border_main">{$giatri[3]}</td>
                                    <td class="border_main">{$giatri[4]}</td>
                                    <td class="border_main">{$giatri[5]}</td>
                                    <td class="border_main">{$giatri[6]}</td>
                                    <td class="border_main">{$giatri[7]}</td>
                                    <td class="border_main">{$giatri[8]}</td>
                                    <td class="border_main">{$giatri[9]}</td>
                                    <td class="border_main">{$giatri[10]} %</td>
                                    <td class="border_main">
                                        {$giatri[13]['diem']}
                                    </td>
                                    <td class="border_main1">{if $giatri[14]['danhgiaquy_ld'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==5}<span style="color:#00EE00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==4}<span style="color:#00FF00">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                    </td>

                                </tr>
                                {/if}
                                {/foreach}

                                <tr>
                                    <td class="border_main"><b>II</b></td>
                                    <td colspan="14"  class="border_main1" style=" text-align: left;padding-left: 5px"><b>Công chức, viên chức, LĐHĐ</b></td>
                                </tr>
                                {$ii=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key > 10}
                                <tr>
                                    <td class="border_main">{$ii++}</td>
                                    <td  class="border_main" style=" text-align: left;padding-left: 5px">
                                            {$giatri[11]}
                                    <td class="border_main">{$giatri[0]}</td>
                                    <td class="border_main">{$giatri[1]}</td>
                                    <td class="border_main">{$giatri[2]}</td>
                                    <td class="border_main">{$giatri[3]}</td>
                                    <td class="border_main">{$giatri[4]}</td>
                                    <td class="border_main">{$giatri[5]}</td>
                                    <td class="border_main">{$giatri[6]}</td>
                                    <td class="border_main">{$giatri[7]}</td>
                                    <td class="border_main">{$giatri[8]}</td>
                                    <td class="border_main">{$giatri[9]}</td>
                                    <td class="border_main">{$giatri[10]} %</td>
                                    <td  class="border_main">
                                        {$giatri[13]['diem']}
                                    </td>
                                    <td  class="border_main1">{if $giatri[14]['danhgiaquy_ld'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==5}<span style="color:#00FF00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==4}<span style="color:#00CC66">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                    </td>                              
                                </tr>
                                {/if}
                                {/foreach}
                                
                                <tr>
                                    <td  class="border_main" colspan="2" style="text-align: center;"><b>Tổng cộng</b></td>
                                    <td class="border_main"><b>{$count_arr1[0]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[1]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[2]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[3]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[4]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[5]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[6]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[7]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[8]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[9]}</b></td>
                                    <td class="border_main"><b>{$count_arr1[10]} %</b></td>
                                    <td class="border_main"><b>
                                         {if isset($diem_tb)} {$diem_tb} {/if}
                                    </b></td>
                                    <td  class="border_main1"><b></b></td>
                                </tr>
                                <tr><td colspan="15" style="border-top: 1px solid #000"></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              
                </div>   
                <div class="row" style="height: 35px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="3" style="text-align: center;vertical-align: top;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b> Giám đốc sở</b><br><br><br><br>
                                    
                                </td>
                                <td colspan="6" style="text-align: center;vertical-align: top;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b>Phó giám đốc sở</b><br><br><br><br>
                                    
                                </td>
                                <td colspan="6" style="text-align: center;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b>Trưởng phòng, đơn vị</b><br>
                                    <i>(ký, ghi rõ họ tên)</i><br><br><br><br>

                         
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>                
            </div>
        </div>
      <!-- /.box -->

    </section>
 


