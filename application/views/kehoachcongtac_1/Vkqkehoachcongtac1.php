<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header">
        <div class="row" >
            <div class="col-md-6">
                <span style="font-size: 20px;width: 400px">
                    Nhập kết quả kế hoạch công tác
                </span>
            </div>
            <div class="col-md-6">
                <span class="4" style="font-weight: 550">
                    Tuần công tác: 
                </span>
                <label class="8">
                 {$arr}    
                 <input type="hidden" name="id" value="{$giatri.id}" />
                 <input type="hidden" name="date_danhgia2" value="{($giatri)?$giatri['date_danhgia2']:''}" />
                 <input type="hidden" name="date_danhgia3" value="{($giatri)?$giatri['date_danhgia3']:''}" />
                 <input type="hidden" name="date_danhgia4" value="{($giatri)?$giatri['date_danhgia4']:''}" />
                 <input type="hidden" name="date_danhgia5" value="{($giatri)?$giatri['date_danhgia5']:''}" />
                 <input type="hidden" name="date_danhgia6" value="{($giatri)?$giatri['date_danhgia6']:''}" />
                 <input type="hidden" name="date_danhgia7" value="{($giatri)?$giatri['date_danhgia7']:''}" />

                </label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 2 sáng: </label>
                        {($giatri)?$giatri['viec2s']:''}
                    </div>
                    <div class="col-md-6"><label style="color: blue">Thứ 2 chiều: </label>
                        {($giatri)?$giatri['viec2c']:''}
                    </div>
                </div>
               
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 2: </span>
                        {($giatri)?$giatri['phatsinh2s']:''}
                     </div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 2: </span>
                        {($giatri)?$giatri['phatsinh2c']:''}
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                       <label style="color: red">Đánh giá kết quả thực hiện kế hoạch ngày thứ 2 </label>
                    </div>
                    <div class="col-md-10">
                        <textarea name="danhgia2" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 2">{($giatri)?$giatri['danhgia2']:''}</textarea>
                    </div>   
                </div>

                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 3 sáng: </label>
                         {($giatri)?$giatri['viec3s']:''}
                    </div>
                    <div class="col-md-6"><label style="color: blue">Thứ 3 chiều: </label>
                        {($giatri)?$giatri['viec3c']:''}
                     </div>
                </div>
                
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 3: </span>
                        {($giatri)?$giatri['phatsinh3s']:''}
                    </div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 3: </span>
                        {($giatri)?$giatri['phatsinh3c']:''}
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                       <label style="color: red">Đánh giá kết quả thực hiện kế hoạch ngày thứ 3 </label>
                    </div>
                    <div class="col-md-10">
                        <textarea name="danhgia3" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 3">{($giatri)?$giatri['danhgia3']:''}</textarea>
                    </div>   
                </div>
                
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 4 sáng: </label>{($giatri)?$giatri['viec4s']:''}</div>
                    <div class="col-md-6"><label style="color: blue">Thứ 4 chiều: </label>
                    {($giatri)?$giatri['viec4c']:''}</div>
                </div>
            
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 4: </span>
                    {($giatri)?$giatri['phatsinh4s']:''}</div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 4: </span>
                    {($giatri)?$giatri['phatsinh4c']:''}</div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                       <label style="color: red">Đánh giá kết quả thực hiện kế hoạch ngày thứ 4 </label>
                    </div>
                    <div class="col-md-10">
                        <textarea name="danhgia4" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 4">{($giatri)?$giatri['danhgia4']:''}</textarea>
                    </div>   
                </div>
                
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 5 sáng: </label> {($giatri)?$giatri['viec5s']:''}</div>
                    <div class="col-md-6"><label style="color: blue">Thứ 5 chiều: </label> {($giatri)?$giatri['viec5c']:''}</div>
                </div>
                
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 5: </span>{($giatri)?$giatri['phatsinh5s']:''}</div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 5: </span> {($giatri)?$giatri['phatsinh5c']:''}</div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                       <label style="color: red">Đánh giá kết quả thực hiện kế hoạch ngày thứ 5 </label>
                    </div>
                    <div class="col-md-10">
                        <textarea name="danhgia5" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 5">{($giatri)?$giatri['danhgia5']:''}</textarea>
                    </div>   
                </div>
                
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 6 sáng: </label> {($giatri)?$giatri['viec6s']:''}</div>
                    <div class="col-md-6"><label style="color: blue">Thứ 6 chiều: </label> {($giatri)?$giatri['viec6c']:''}</div>
                </div>
                
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 6: </span>{($giatri)?$giatri['phatsinh6s']:''}</div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 6: </span>{($giatri)?$giatri['phatsinh6c']:''}</div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                       <label style="color: red">Đánh giá kết quả thực hiện kế hoạch ngày thứ 6 </label>
                    </div>
                    <div class="col-md-10">
                        <textarea name="danhgia6" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 6">{($giatri)?$giatri['danhgia6']:''}</textarea>
                    </div>   
                </div>
                
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 7 sáng: </label> {($giatri)?$giatri['viec7s']:''}</div>
                    <div class="col-md-6"><label style="color: blue">Thứ 7 chiều: </label>{($giatri)?$giatri['viec7c']:''}</div>
                </div>
                
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 7: </span>{($giatri)?$giatri['phatsinh7s']:''}</div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 7: </span> {($giatri)?$giatri['phatsinh7c']:''}</div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                       <label style="color: red">Đánh giá kết quả thực hiện kế hoạch ngày thứ 7 </label>
                    </div>
                    <div class="col-md-10">
                        <textarea name="danhgia7" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 7">{($giatri)?$giatri['danhgia7']:''}</textarea>
                    </div>   
                </div>

                <div class="row" style="height: 7px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button> 
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

