<style type="text/css">
    .border_main{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        padding-left: 15px;
    }
    .border_main1{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
        padding-left: 15px;
    }
</style>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                <table id="" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="3%" class="text-center" rowspan="3" class="border_main" >STT</th>
                            <th class="text-center" rowspan="3"  class="border_main">Người thực hiện</th>
                            <th width="5%" class="text-center" rowspan="3"  class="border_main">Tổng số việc theo kế hoạch</th>
                            <th width="30%" class="text-center" colspan="6"  class="border_main">Công việc đã hoàn thành trong tháng</th>
                            <th width="15%" class="text-center" colspan="3"  class="border_main">Công việc còn tồn cuối tháng</th>
                            <th width="8%" class="text-center" style="border-left: 1px solid #000" rowspan="3"  class="border_main" >Tỉ lệ hoàn thành (%)</th>
                            <th width="6%" class="text-center" rowspan="3"  class="border_main">Điểm</th>
                            <th width="10%" class="text-center" rowspan="3"  class="border_main1">Nhận xét về ưu điểm, hạn chế</th>
                        </tr>
                        <tr>
                            <th width="5%" class="text-center" rowspan="2"  class="border_main">Số lượng</th>
                            <th width="10%" class="text-center" colspan="2"  class="border_main">Thời gian</th>
                            <th width="10%" class="text-center" colspan="2"   class="border_main">Chất lượng</th>
                            <th width="5%" class="text-center" style="border-left: 1px solid #000" rowspan="2" class="border_main" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                            <th width="5%" class="text-center" rowspan="2"  class="border_main">Số lượng</th>
                            <th width="5%" class="text-center"  rowspan="2" class="border_main">Trong hạn</th>
                            <th width="5%" class="text-center"  rowspan="2" class="border_main">Chậm tiến độ</th>
                            
                        </tr>
                        <tr>
                            <th width="5%" class="text-center"  class="border_main">Trong hạn</th>
                            <th width="5%" class="text-center"  class="border_main">Quá hạn</th>
                            <th width="5%" class="text-center"  class="border_main">Đảm bảo</th>
                            <th width="5%" class="text-center"  class="border_main">Chưa đảm bảo</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="border_main"><b>I</b></td>
                            <td  class="border_main" style=" text-align: left;padding-left: 5px"><b>Lãnh đạo sở</b></td>
                            <td colspan="13" class="border_main1"></td>
                        </tr>

                        {$jj=1}
                        {foreach $count_arr as $key => $giatri}
                        {if $key <= 10}
                        <tr>
                            <td class="border_main">{$jj++}</td>
                            <td class="border_main" style=" text-align: left;padding-left: 5px">
                            {if $quyen == 5 || $quyen ==4}
                                {if $jj == 2}Tổng Việc Toàn Sở{else}PGĐ {$giatri[11]}{/if}
                            {else}
                                {if $jj == 2}Tổng Toàn Sở {else}PGĐ {$giatri[11]}{/if}
                            {/if}
                            </td>
                            
                            <td class="border_main">{$giatri[0]['tong']}</td>
                            <td class="border_main">{$giatri[1]['tong']}</td>
                            <td class="border_main">{$giatri[2]['tong']}</td>
                            <td class="border_main">{$giatri[3]['tong']}</td>
                            <td class="border_main">{$giatri[4]['tong']}</td>
                            <td class="border_main">{$giatri[5]['tong']}</td>
                            <td class="border_main">{$giatri[6]['tong']}</td>
                            <td class="border_main">{$giatri[7]['tong']}</td>
                            <td class="border_main">{$giatri[8]['tong']}</td>
                            <td class="border_main">
                                <span" style="color: red">{$giatri[9]['tong']}</span>
                            </td>
                            <td class="border_main">{$giatri[10]} %</td>
                            <td class="border_main">
                                {$giatri[13]['diem_tong_tp']}
                            </td>
                            <td class="border_main1">{if $giatri[14]['danhgia_ld'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==5}<span style="color:#00EE00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==4}<span style="color:#00FF00">Hoàn thành nhiệm vụ</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                            </td>

                        </tr>
                        {/if}
                        {/foreach}

                        <tr>
                            <td class="border_main"><b>II</b></td>
                            <td  class="border_main" style=" text-align: left;padding-left: 5px"><b>Lãnh đạo phòng</b></td>
                            <td colspan="13" class="border_main1"></td>

                        </tr>
                        {$ii=1}
                        {foreach $count_arr as $key => $giatri}
                        {if $key > 10}
                        <tr>
                            <td class="border_main">{$ii++}</td>
                            <td  class="border_main" style=" text-align: left;padding-left: 5px">
                                <span>{$giatri[11]}</span></td>
                            <td class="border_main">{$giatri[0]['tong']}</td>
                            <td class="border_main">{$giatri[1]['tong']}</td>
                            <td class="border_main">{$giatri[2]['tong']}</td>
                            <td class="border_main">{$giatri[3]['tong']}</td>
                            <td class="border_main">{$giatri[4]['tong']}</td>
                            <td class="border_main">{$giatri[5]['tong']}</td>
                            <td class="border_main">{$giatri[6]['tong']}</td>
                            <td class="border_main">{$giatri[7]['tong']}</td>
                            <td class="border_main">{$giatri[8]['tong']}</td>
                            <td class="border_main"><span style="color: red">{$giatri[9]['tong']}</span></td>
                            <td class="border_main">{$giatri[10]} %</td>
                            <td class="border_main">
                                {$giatri[13]['diem_tong_tp']}
                            </td>
                            <td class="border_main1">{if $giatri[14]['danhgia_ld'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==5}<span style="color:#00FF00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==4}<span style="color:#00CC66">Hoàn thành nhiệm vụ</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                {if $giatri[14]['danhgia_ld'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                            </td>                              
                        </tr>
                        {/if}
                        {/foreach}
                        <tr>
                            <td colspan="15" style="border-top: 1px solid #000"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
      
        </div>   
        </div>
    </div>
  <!-- /.box -->

</section>
<!-- /.content -->
