<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-3">
                <span class="font">
                    Danh sách kế hoạch công tác quý
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn quý công tác: 
                </span>
                <label class="8 sl2">
                    <select name="quy" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=4;$i++}
                                <option value="{$i}" {if $quy == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                </label>
            </div>
            <div class="col-md-4 sl3">
                <!-- <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word"> -->
            </div>
            <div class="col-md-1 sl3"></div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3" >STT</th>
                                    <th class="text-center" rowspan="3" >Phòng ban, đơn vị</th>
                                    <th width="10%" class="text-center" rowspan="3" >Tổng số việc theo kế hoạch</th>
                                    <th width="30%" class="text-center" colspan="6" >Công việc đã hoàn thành trong tháng</th>
                                    <th width="15%" class="text-center" colspan="3" >Công việc còn tồn cuối tháng</th>
                                    <th width="8%" class="text-center" style="border-left: 1px solid #FFF" rowspan="3"  >Tỉ lệ hoàn thành (%)</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2" >Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2"  >Chất lượng</th>
                                    <th width="5%" class="text-center" style="border-left: 1px solid #FFF" rowspan="2" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="5%" class="text-center"  rowspan="2">Trong hạn</th>
                                    <th width="5%" class="text-center"  rowspan="2">Chậm tiến độ</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" >Trong hạn</th>
                                    <th width="5%" class="text-center" >Quá hạn</th>
                                    <th width="5%" class="text-center" >Đảm bảo</th>
                                    <th width="5%" class="text-center" >Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                {$ii=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key > 10}
                                <tr>
                                    <td>{$ii++}</td>
                                    <td style=" text-align: left;padding-left: 5px">
                                        <a href="dskehoachcongtacquy/{$giatri[15]}/{$quy}">{$phongban[$giatri[15]]}</a></td>
                                    <td>{$giatri[0]['tong']}</td>
                                    <td>{$giatri[1]['tong']}</td>
                                    <td>{$giatri[2]['tong']}</td>
                                    <td>{$giatri[3]['tong']}</td>
                                    <td>{$giatri[4]['tong']}</td>
                                    <td>{$giatri[5]['tong']}</td>
                                    <td>{$giatri[6]['tong']}</td>
                                    <td>{$giatri[7]['tong']}</td>
                                    <td>{$giatri[8]['tong']}</td>
                                    <td>{$giatri[9]['tong']}</td>
                                    <td>{$giatri[10]} %</td>
                                </tr>
                                {/if}
                                {/foreach}
                               
                            </tbody>
                        </table>
                    </div>
                </div>
              
                </div>   
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','top');
    });
</script>

