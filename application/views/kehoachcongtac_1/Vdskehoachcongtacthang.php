<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-3">
                <span class="font">
                    Danh sách kế hoạch công tác tháng
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn Tháng công tác: 
                </span>
                <label class="8 sl2">
                    <select name="month" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                </label>
            </div>
            <div class="col-md-4 sl3">
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
            </div>
            <div class="col-md-1 sl3">Biểu số 6</div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3" >STT</th>
                                    <th class="text-center" rowspan="3" >Người thực hiện</th>
                                    <th width="5%" class="text-center" rowspan="3" >Tổng số việc theo kế hoạch</th>
                                    <th width="30%" class="text-center" colspan="6" >Công việc đã hoàn thành trong tháng</th>
                                    <th width="15%" class="text-center" colspan="3" >Công việc còn tồn cuối tháng</th>
                                    <th width="8%" class="text-center" style="border-left: 1px solid #FFF" rowspan="3"  >Tỉ lệ hoàn thành (%)</th>
                                    <th width="6%" class="text-center" rowspan="3" >Điểm trung bình của tháng</th>
                                    <th width="10%" class="text-center" rowspan="3" >Kết quả phân loại mức độ hoàn thành nhiệm vụ</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2" >Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2"  >Chất lượng</th>
                                    <th width="5%" class="text-center" style="border-left: 1px solid #FFF" rowspan="2" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="5%" class="text-center"  rowspan="2">Trong hạn</th>
                                    <th width="5%" class="text-center"  rowspan="2">Chậm tiến độ</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" >Trong hạn</th>
                                    <th width="5%" class="text-center" >Quá hạn</th>
                                    <th width="5%" class="text-center" >Đảm bảo</th>
                                    <th width="5%" class="text-center" >Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>I</b></td>
                                    <td style=" text-align: left;padding-left: 5px"><b>Lãnh đạo phòng, đơn vị</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: left;padding-left: 5px"></td>
                                    <td>
                                    </td>
                                    <td >
                                    </td>
                                </tr>

                                {$jj=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key <= 10}
                                <tr>
                                    <td>{$jj++}</td>
                                    <td style=" text-align: left;padding-left: 5px">
                                        {if $quyen ==3 || $quyen ==6}
                                            <a href="xemkehoachcongtacthang/{$giatri[12]}/{$month}">{$giatri[11]}</a></td>
                                        {else}
                                            {$giatri[11]}
                                        {/if}
                                    </td>
                                    <td>{$giatri[0]}</td>
                                    <td>{$giatri[1]}</td>
                                    <td>{$giatri[2]}</td>
                                    <td>{$giatri[3]}</td>
                                    <td>{$giatri[4]}</td>
                                    <td>{$giatri[5]}</td>
                                    <td>{$giatri[6]}</td>
                                    <td>{$giatri[7]}</td>
                                    <td>{$giatri[8]}</td>
                                    <td>{$giatri[9]}</td>
                                    <td>{$giatri[10]} %</td>
                                    <td >
                                        {$giatri[13]['diem']}
                                    </td>
                                    <td >{if $giatri[14]['danhgia_tp'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==5}<span style="color:#00EE00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==4}<span style="color:#00FF00">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                    </td>

                                </tr>
                                {/if}
                                {/foreach}

                                <tr>
                                    <td><b>II</b></td>
                                    <td style=" text-align: left;padding-left: 5px"><b>Công chức, viên chức, LĐHĐ</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: left;padding-left: 5px"></td>
                                    <td > </td>
                                    <td></td>

                                </tr>
                                {$ii=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key > 10}
                                <tr>
                                    <td>{$ii++}</td>
                                    <td style=" text-align: left;padding-left: 5px">
                                        {if $quyen ==3 || $quyen ==6}
                                            <a href="xemkehoachcongtacthang/{$giatri[12]}/{$month}">{$giatri[11]}</a></td>
                                        {else}
                                            {$giatri[11]}
                                        {/if}
                                    <td>{$giatri[0]}</td>
                                    <td>{$giatri[1]}</td>
                                    <td>{$giatri[2]}</td>
                                    <td>{$giatri[3]}</td>
                                    <td>{$giatri[4]}</td>
                                    <td>{$giatri[5]}</td>
                                    <td>{$giatri[6]}</td>
                                    <td>{$giatri[7]}</td>
                                    <td>{$giatri[8]}</td>
                                    <td>{$giatri[9]}</td>
                                    <td>{$giatri[10]} %</td>
                                    <td >
                                        {$giatri[13]['diem']}
                                    </td>
                                    <td >{if $giatri[14]['danhgia_tp'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==5}<span style="color:#00FF00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==4}<span style="color:#00CC66">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                    </td>                              
                                </tr>
                                {/if}
                                {/foreach}
                                
                                <tr>
                                    <td colspan="2" style="text-align: center;"><b>Tổng cộng</b></td>
                                    <td><b>{$count_arr1[0]}</b></td>
                                    <td><b>{$count_arr1[1]}</b></td>
                                    <td><b>{$count_arr1[2]}</b></td>
                                    <td><b>{$count_arr1[3]}</b></td>
                                    <td><b>{$count_arr1[4]}</b></td>
                                    <td><b>{$count_arr1[5]}</b></td>
                                    <td><b>{$count_arr1[6]}</b></td>
                                    <td><b>{$count_arr1[7]}</b></td>
                                    <td><b>{$count_arr1[8]}</b></td>
                                    <td><b>{$count_arr1[9]}</b></td>
                                    <td><b>{$count_arr1[10]} %</b></td>
                                    <td ><b>
                                         {if isset($diem_tb)} {$diem_tb} {/if}
                                    </b></td>
                                    <td ><b>
                                    </b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              
                </div>   
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','top');
    });

    $(document).ready(function(){
        var url = window.location.href;
        $(document).on('click','button[name=chamdiem]',function(){
            var danhgia_id = $(this).val();
            var nhan_xet  = $('textarea[name="nhanxet_' + danhgia_id + '"]').val();
            var diem_1_tp  = $('select[name="diem_1_tp_' + danhgia_id + '"] option:selected').val();
            var diem_3_tp  = $('select[name="diem_3_tp_' + danhgia_id + '"] option:selected').val();
            var diem_4_tp  = $('select[name="diem_4_tp_' + danhgia_id + '"] option:selected').val();
            var diem_5_tp  = $('select[name="diem_5_tp_' + danhgia_id + '"] option:selected').val();
            var diem_6_tp  = $('select[name="diem_6_tp_' + danhgia_id + '"] option:selected').val();
            var diem_7_tp  = $('select[name="diem_7_tp_' + danhgia_id + '"] option:selected').val();
            var diem_8_tp  = $('select[name="diem_8_tp_' + danhgia_id + '"] option:selected').val();
            var diem_9_tp  = $('select[name="diem_9_tp_' + danhgia_id + '"] option:selected').val();
            var diem_11_tp  = $('select[name="diem_11_tp_' + danhgia_id + '"] option:selected').val();
            var diem_12_tp  = $('select[name="diem_12_tp_' + danhgia_id + '"] option:selected').val();
            var diem_13_tp  = $('select[name="diem_13_tp_' + danhgia_id + '"] option:selected').val();
            var diem_14_tp  = $('select[name="diem_14_tp_' + danhgia_id + '"] option:selected').val();
            var diem_15_tp  = $('select[name="diem_15_tp_' + danhgia_id + '"] option:selected').val();
            var diem_16_tp  = $('select[name="diem_16_tp_' + danhgia_id + '"] option:selected').val();
            var diem_18_tp  = $('select[name="diem_18_tp_' + danhgia_id + '"] option:selected').val();
            var diem_19_tp  = $('select[name="diem_19_tp_' + danhgia_id + '"] option:selected').val();
            var diem_2_tp = parseFloat(diem_3_tp)+parseFloat(diem_4_tp)+parseFloat(diem_5_tp)+parseFloat(diem_6_tp)+parseFloat(diem_7_tp)+parseFloat(diem_8_tp)+parseFloat(diem_9_tp);
            var diem_10_tp = parseFloat(diem_11_tp)+parseFloat(diem_12_tp)+parseFloat(diem_13_tp)+parseFloat(diem_14_tp)+parseFloat(diem_15_tp)+parseFloat(diem_16_tp)+parseFloat(diem_18_tp);
            var diem_tong_tp = parseFloat(diem_1_tp)+parseFloat(diem_2_tp)+parseFloat(diem_10_tp)+parseFloat(diem_19_tp);
            $.ajax({
                url:url,
                type:'POST',
                data:{
                    action:'chamdiem',
                    danhgia_id:danhgia_id,
                    diem_1_tp:diem_1_tp,
                    diem_2_tp:diem_2_tp,
                    diem_3_tp:diem_3_tp,
                    diem_4_tp:diem_4_tp,
                    diem_5_tp:diem_5_tp,
                    diem_6_tp:diem_6_tp,
                    diem_7_tp:diem_7_tp,
                    diem_8_tp:diem_8_tp,
                    diem_9_tp:diem_9_tp,
                    diem_10_tp:diem_10_tp,
                    diem_11_tp:diem_11_tp,
                    diem_12_tp:diem_12_tp,
                    diem_13_tp:diem_13_tp,
                    diem_14_tp:diem_14_tp,
                    diem_15_tp:diem_15_tp,
                    diem_16_tp:diem_16_tp,
                    diem_18_tp:diem_18_tp,
                    diem_19_tp:diem_19_tp,
                    diem_tong_tp:diem_tong_tp,
                    nhan_xet:nhan_xet
                },
                success:function(repon){ 
                    var result = JSON.parse(repon);
                    if(result>0)
                    {   
                        location.reload();
                        showMessage('Bạn đã chấm điểm thành công','info');
                    }


                }
            });
        });
    });
</script>

