<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    Danh sách kế hoạch công tác tháng: {$month}
                </span>
            </div>
            <div class="col-md-8">
                <input type="hidden" name="thang" value="{$month}">
            </div>
                 
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th class="text-center">Nội dung</th>
                                    <th width="5%" class="text-center">Lãnh đạo sở chỉ đạo</th>
                                    <th width="6%" class="text-center">Lãnh đạo phòng chỉ đạo</th>
                                    <th width="6%" class="text-center">Cán bộ thực hiện</th>                         
                                    <th width="5%" class="text-center">Ngày bắt đầu</th>
                                    <th width="5%" class="text-center">Ngày kết thúc</th>
                                    <th width="25%" class="text-center">Kết quả</th>
                                    <th width="18%" class="text-center">Ghi chú</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            {$i = 1}
                            {foreach $giatri as $thang}
                            {if $thang.id_sub == 0}
                                {$j=1}
                                <tr>
                                    <th class="text-center">{intToRoman($i++)}</th>
                                    <th class="text-left">{$thang.noidung}</th>
                                    <th class="text-center">{$lanhdaoso[$thang.ld_so]}</th>
                                    <th class="text-center">
                                    {if $thang.ld_phong !=''}
                                        {$arr_id = explode(',',$thang.ld_phong)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if}  
                                    </th>
                                    <th class="text-left">
                                    {if $thang.ld_phong !=''}
                                        {$arr_id = explode(',',$thang.user_id)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if} 
                                    </th>
                                    <th class="text-left">{date_month_day($thang.date_start)}</th>
                                    <th class="text-left">{date_month_day($thang.date_end)}</th>
                                    <th class="text-left">
                                        <textarea name="ketqua[]" class="form-control" rows="8" style="border: 1px solid #3c8dbc;" placeholder="Nhập kết quả">{($thang)?$thang.ketqua:''}</textarea>
                                    </th>
                                    <th class="text-left">
                                    <textarea name="ghichu" class="form-control" rows="8" style="border: 1px solid #3c8dbc;" placeholder="Nhập ghichu">{($thang)?$thang.ghichu:''}</textarea>
                                    </th>
                                    <input type="hidden" name="arr_id[]" value="{$thang.id}">
                                    
                                </tr>
                                {foreach $giatri as $thangsub}
                                    {if $thang.id == $thangsub.id_sub}
                                    <tr>
                                    <td class="text-center">{$j++}</td>
                                    <td class="text-left">{$thangsub.noidung}</td>
                                    <td class="text-center">{$lanhdaoso[$thangsub.ld_so]}</td>
                                    <td class="text-center">
                                    {if $thangsub.ld_phong !=''}
                                        {$arr_id = explode(',',$thangsub.ld_phong)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if}  
                                    </td>
                                    <td class="text-left">
                                    {if $thangsub.ld_phong !=''}
                                        {$arr_id = explode(',',$thangsub.user_id)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if} 
                                    </td>
                                    <td class="text-left">{date_month_day($thangsub.date_start)}</td>
                                    <td class="text-left">{date_month_day($thangsub.date_end)}</td>
                                    <td class="text-left">
                                        <textarea name="ketqua[]" class="form-control" rows="6" style="border: 1px solid #3c8dbc;" placeholder="Nhập kết quả">{($thang)?$thang.ketqua:''}</textarea>
                                    </td>
                                    <td class="text-left">
                                    <textarea name="ghichu[]" class="form-control" rows="6" style="border: 1px solid #3c8dbc;" placeholder="Nhập kết quả">{($thang)?$thang.ghichu:''}</textarea>
                                    </td>
                                    <input type="hidden" name="arr_id[]" value="{$thangsub.id}">
                                </tr>                               
                                {/if}
                                {/foreach}
                            {/if}
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div> 

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button> 
                    </div>
                </div>  
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','top');
    });
</script>

