<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    Chấm điểm kết quả kế hoạch công tác tháng: 
                </span>
                <span class="4" style="font-weight: 550">
                     {$month}
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn Tháng công tác: 
                </span>
                <label class="8 sl2">
                    <select name="month" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                </label>
            </div>
            <div class="col-md-4 sl3" style="padding-bottom: 3px;">
                {if !empty($list_diem)}
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
                {/if}
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3">STT</th>
                                    <th width="30%" class="text-center" rowspan="3">Nội dung</th>
                                    <th width="5%" class="text-center" rowspan="3">Tổng số việc của văn phòng điện tử</th>
                                    <th width="25%" class="text-center" colspan="6">Công việc đã hoàn thành trong tháng</th>
                                    <th width="10%" class="text-center" colspan="3">Công việc còn tồn cuối tháng</th>
                                    <th width="5%" class="text-center" rowspan="3"  style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Tỉ lệ hoàn thành (%)</th>
                                    <th width="5%" class="text-center" rowspan="3">Cá nhân tự chấm điểm</th>
                                    <th width="5%" class="text-center" rowspan="3">lãnh đạo chấm điểm</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2">Chất lượng</th>
                                    <th width="5%" class="text-center" rowspan="2" style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center">Trong hạn</th>
                                    <th width="5%" class="text-center">Quá hạn</th>
                                    <th width="5%" class="text-center">Đảm bảo</th>
                                    <th width="5%" class="text-center">Chưa đảm bảo</th>
                                    <th width="5%" class="text-center" >Trong hạn</th>
                                    <th width="5%" class="text-center" >Quá hạn</th>
                                </tr>
                            </thead>
                            <tbody>                                
                                <tr>
                                    <td class="text-center"><b>A</b></td>
                                    <td class="text-left"><b>Kết quả thực hiện nhiệm vụ theo kế hoạch công tác tuần được duyệt <br>(tối đa 65 điểm)</b></td>
                                    <td class="text-center">{$count_arr[0]['tong']}</td>
                                    <td class="text-center">{$count_arr[1]['tong']}</td>
                                    <td class="text-center">{$count_arr[2]['tong']}</td>
                                    <td class="text-center" style="color: red">{$count_arr[3]['tong']}</td>
                                    <td class="text-center">{$count_arr[4]['tong']}</td>
                                    <td class="text-center">{$count_arr[5]['tong']}</td>
                                    <td class="text-center">{$count_arr[6]['tong']}</td>
                                    <td class="text-center">{$count_arr[7]['tong']}</td>
                                    <td class="text-center">{$count_arr[8]['tong']}</td>
                                    <td class="text-center" style="color: red">{$count_arr[9]['tong']}</td>
                                    <td class="text-center">{$count_arr[10]}% </td>
                                    <td class="text-center">
                                    {if $quyen == 3 || $quyen == 6}
                                    <select name="diem_1">
                                        <option value="0">Điểm</option>
                                        {for $i=65;$i>=0;$i--}
                                        {$j=$i+0.5}
                                        {if $j<65 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_1']==$j)?'selected':''}>{$j}</option>{/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_1']==$i)?selected:''}>{$i}.0</option>                                       
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_1']>0)?$list_diem[0]['diem_1']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen == 4 }
                                    <select name="diem_1_tp">
                                        {for $i=0;$i<66;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_1_tp']=$list_diem[0]['diem_1']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_1_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<65 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_1_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_1_tp']}
                                    
                                    {/if}
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text-center"><b>B</b></td>
                                    <td class="text-left"><b>Ý thức tổ chức kỷ luật (tối đa 20 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_2']:''}</b></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_2_tp']:''}</b></td>
                                </tr> 
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Chấp hành tốt đường lối, chủ trương của Đảng; chính sách,pháp luật của nhà nước <br><b>- Điểm tối đa: 4</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                    {if $quyen == 3 || $quyen == 6}
                                    <select  name="diem_3">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_3']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_3']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_3']>0)?$list_diem[0]['diem_3']:''}
                                    {/if}
                                    </select></td>
                                    <td class="text-center">
                                    {if $quyen == 4}
                                    <select  name="diem_3_tp">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_3_tp']=$list_diem[0]['diem_3']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_3_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j < 4 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_3_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_3_tp']}
                                    {/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-left">Chấp hành nghiêm nội quy, quy chế làm việc của cơ quan, đơn vị, quy tắc ứng xử của cán bộ, công chức, viên chức, người lao động <i>(ngoài các tiêu chí nêu ở các mục phần này)</i> <br><b>- Điểm tối đa: 4</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                    {if $quyen == 3 || $quyen == 6}
                                    <select name="diem_4">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_4']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j} {(!empty($list_diem) && $list_diem[0]['diem_4']==$j)?'selected':''}">{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_4']>0)?$list_diem[0]['diem_4']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen == 4}
                                    <select name="diem_4_tp">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_4_tp']=$list_diem[0]['diem_4']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_4_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_4_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_4_tp']}
                                    {/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-left">Có phẩm chất chính trị, đạo đức tốt; có lối sống lành mạnh, có tác phong lề lối làm việc chuẩn mực; tận tụy trách nhiệm trong công việc; Giữ gìn đoàn kết nội bộ, thực hiện nguyên tắc tập trung dân chủ; có tinh thần cầu thị, lắng nghe cộng tác, giúp đỡ đồng nghiệp<br><b>- Điểm tối đa: 3</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                    {if $quyen == 3 || $quyen == 6}
                                    <select name="diem_5">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_5']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_5']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_5']>0)?$list_diem[0]['diem_5']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_5_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_5_tp']=$list_diem[0]['diem_5']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_5_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_5_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_5_tp']}
                                    {/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">4</td>
                                    <td class="text-left">Có thái độ phục vụ nhân dân đúng mực; Không hách dịch, cửa quyền, sách nhiễu, gây phiền hà, tiêu cực trong thực hiện nhiệm vụ, Không hẹn gặp giải quyết công việc bên ngoài cơ quan và ngoài giờ làm việc<br><b>- Điểm tối đa: 3</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                    {if $quyen == 3 || $quyen == 6}
                                    <select name="diem_6">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_6']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_6']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_6']>0)?$list_diem[0]['diem_6']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                        {if $quyen == 4}<select name="diem_6_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_6_tp']=$list_diem[0]['diem_6']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_6_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_6_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5|| $quyen == 6)}
                                        {$list_diem[0]['diem_6_tp']}
                                    {/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">5</td>
                                    <td class="text-left">Thực hiện tốt văn hóa nơi công sở; Mặc trang phục lịch sự, đầu tóc gọn gàng, đúng quy định; Đeo thẻ công chức, viên chức, nhân viên trong giờ làm việc; Tư thế cử chỉ nghiêm túc; Thái độ khiêm tốn lịch sự; Ngôn ngữ chuẩn mực, rõ ràng, hòa nhã; Không hút thuốc tại cơ quan, phòng làm việc; Không sử dụng đồ uống có cồn trong giờ làm việc; Sắp xếp nơi làm việc ngăn nắp,gọn gàng, sạch sẽ  <br><b>- Điểm tối đa: 3</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                    {if $quyen == 3 || $quyen == 6}
                                    <select name="diem_7">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_7']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_7']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_7']>0)?$list_diem[0]['diem_7']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_7_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_7_tp']=$list_diem[0]['diem_7']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_7_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_7_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_7_tp']}
                                    {/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">6</td>
                                    <td class="text-left">Xây dựng hình ảnh, giữ gìn uy tín cho bản thân, cơ quan, đơn vị và đồng nghiệp; giữ gìn bí mật cơ quan, đơn vị và thực hiện ngiêm kỷ luật phát ngôn <br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                    {if $quyen == 3 || $quyen == 6}<select name="diem_8">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_8']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_8']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_8']>0)?$list_diem[0]['diem_8']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_8_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_8_tp']=$list_diem[0]['diem_8']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_8_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_8_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_8_tp']}
                                    {/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">7</td>
                                    <td class="text-left">Chấp hành nghiêm quy định về thời gian làm việc của nhà nước, của cơ quan; sắp xếp, sử dụng thời gianlàm việc khoa học và hiệu quả<br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_9">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_9']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_9']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_9']>0)?$list_diem[0]['diem_9']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_9_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_9_tp']=$list_diem[0]['diem_9']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_9_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_9_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_9_tp']}
                                    {/if}</td>

                                </tr> 

                                <tr>
                                    <td class="text-center"><b>C</b></td>
                                    <td class="text-left"><b>Năng lực và kỹ năng thực hiện nhiệm vụ (tối đa 10 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_10']:''}</b></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_10_tp']:''}</b></td>

                                </tr> 
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Chủ động học tập, rèn luyện, nâng cao trình độ, kỹ năng, năng lực chuyên môn nghiệp vụ, kiến thức ngoại ngữ, tin học; tham mưu đầy đủ, có chất lượng các văn bản phục vụ công tác chỉ đạo, điều hành của đơn vị/bộ phận theo chỉ đạo của lãnh đạo và kế hoạch công tác <br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_11">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_11']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_11']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_11']>0)?$list_diem[0]['diem_11']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_11_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_11_tp']=$list_diem[0]['diem_11']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_11_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_11_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_11_tp']}
                                    {/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-left">Xây dựng kế hoạch công tác của đơn vị theo lĩnh vực được phân công và kế hoạch công tác của cá nhân rõ nội dung, tiến độ<br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_12">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_12']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_12']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_12']>0)?$list_diem[0]['diem_12']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_12_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_12_tp']=$list_diem[0]['diem_12']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_12_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_12_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_12_tp']}
                                    {/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-left">Chỉ đạo, điều hành, kiểm soát việc thực hiện nhiệm vụ của đơn vị, bộ phận đảm bảo kịp thời, không bỏ sót nhiệm vụ. Phân cong, chỉ đạo giải quyết công việc linh hoạt, có định hướng, đúng quy trình<br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_13">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_13']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_13']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_13']>0)?$list_diem[0]['diem_13']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_13_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_13_tp']=$list_diem[0]['diem_13']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_13_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_13_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_13_tp']}
                                    {/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">4</td>
                                    <td class="text-left">Kiểm tra, bao quát, đôn đốc việc thực hiện nhiệm vụ của CBCCVC trong đơn vị/bộ phận và giải quyết kịp thời những khó khăn, vướng mắc theo thẩm quyền<br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_14">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_14']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_14']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_14']>0)?$list_diem[0]['diem_14']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_14_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_14_tp']=$list_diem[0]['diem_14']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_14_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_14_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_14_tp']}
                                    {/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">5</td>
                                    <td class="text-left">Có năng lực tập hợp CBCCVC, xây dựng đơn vị/bộ phận đoàn kết, thống nhất; phối hợp, tạo lập mối quan hệ tốt với cá nhân, tổ chức có liên quan trong thực hiện nhiệm vụ<br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_15">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_15']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_15']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_15']>0)?$list_diem[0]['diem_15']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_15_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_15_tp']=$list_diem[0]['diem_15']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_15_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_15_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_15_tp']}
                                    {/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">6</td>
                                    <td class="text-left">Sử dụng thành thạo các phần mềm, ứng dụng CNTT đáp ứng yêu cầu công việc <br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_16">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_16']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_16']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_16']>0)?$list_diem[0]['diem_16']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_16_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_16_tp']=$list_diem[0]['diem_16']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_16_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_16_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_16_tp']}
                                    {/if}</td>

                                </tr> 
                                
                                <input type="hidden" name="diem_17" value="0">
                                <input type="hidden" name="diem_17_tp" value="0">
                                <tr>
                                    <td class="text-center">7</td>
                                    <td class="text-left">Thiết lập hồ sơ công việc đầy đủ theo các đầu mục công việc được phân công; lưu trữ hồ sơ, tài liệu đúng nguyên tắc; xây dựng kho dữ liệu cá nhân làm cơ sở để xây dựng kho dữ liệu dùng chung của cơ quan, đơn vị<br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_18">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_18']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_18']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_18']>0)?$list_diem[0]['diem_18']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_18_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_18_tp']=$list_diem[0]['diem_18']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_18_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_18_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_18_tp']}
                                    {/if}</td>

                                </tr>  

                                <tr>
                                    <th class="text-center">D</th>
                                    <td class="text-left"><b>Điểm thưởng (điểm tối đa: 5)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">{if $quyen == 3 || $quyen == 6}<select name="diem_19">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_19']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j< 5 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_19']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_19']>0)?$list_diem[0]['diem_19']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen == 4}<select name="diem_19_tp">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_19_tp']=$list_diem[0]['diem_19']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_19_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j < 5 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_19_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen == 3 || $quyen == 5 || $quyen == 6)}
                                        {$list_diem[0]['diem_19_tp']}
                                    {/if}</td>

                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                {if $PK_iMaCB == $cb_id}
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="danhgia" style="rows=2" placeholder="Cán bộ tự nhận xét về ưu điểm hạn chế">{if !empty($list_diem)}{$list_diem[0]['danhgia']}{/if}</textarea>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                        {if $quyen == 6 || $quyen == 3 }<label>CÁ NHÂN TỰ PHÂN LOẠI:</label>{else}
                        <label>Lãnh đạo phân loại:</label>
                        {/if}
                    </div>
                    <div class="col-md-2">
                        {if $quyen == 6 || $quyen == 3}
                            <input type="radio" name="danhgiathang" value="6" {if isset($list_diem) && $list_diem[0]['tinhtranghoanthanhtp'] == 6}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="6" {if isset($list_diem) && $list_diem['tinhtranghoanthanhld'] == 6}checked{/if}>
                        {/if}
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen == 6 || $quyen == 3}
                            <input type="radio" name="danhgiathang" value="5" {if isset($list_diem) && $list_diem[0]['tinhtranghoanthanhtp'] == 5}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="5" {if isset($list_diem) && $list_diem['tinhtranghoanthanhld'] == 5}checked{/if}>
                        {/if}
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen == 6 || $quyen == 3}
                            <input type="radio" name="danhgiathang" value="4" {if isset($list_diem) && $list_diem[0]['tinhtranghoanthanhtp'] == 4}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="4" {if isset($list_diem) && $list_diem['tinhtranghoanthanhld'] == 4}checked{/if}>
                        {/if}
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen == 6 || $quyen == 3}
                            <input type="radio" name="danhgiathang" value="3" {if isset($list_diem) && $list_diem[0]['tinhtranghoanthanhtp'] == 3}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="3" {if isset($list_diem) && $list_diem['tinhtranghoanthanhld'] == 3}checked{/if}>
                        {/if}
                        <b> Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen == 6 || $quyen == 3}
                            <input type="radio" name="danhgiathang" value="2" {if isset($list_diem) && $list_diem[0]['tinhtranghoanthanhtp'] == 2}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="2" {if isset($list_diem) && $list_diem['tinhtranghoanthanhld'] == 2}checked{/if}>
                        {/if}
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                    
                </div>  
                          
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <!--<input type="hidden" name="month" value="{$month}">-->
                        <input type="hidden" name="phong" value="{$phong}">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật điểm tháng {$month}</button>
                    </div>
                </div>
                {/if}
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

