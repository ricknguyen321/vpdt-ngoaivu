<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-6">
                <span class="font">
                    Nhập nhiệm vụ phát sinh - Sửa kế hoạch công tác
                </span>
            </div>
            <div class="col-md-6 sl3">
                <span class="4" style="font-weight: 550">
                    Tuần công tác: 
                </span>
                <label class="8 sl2">
                 {$arr}    
                 <input type="hidden" name="tuan" value="{$tuan}" /> 
                 <input type="hidden" name="date_start" value="{$giatri.date_start}" />   
                 <input type="hidden" name="date_end" value="{$giatri.date_end}" /> 
                 <input type="hidden" name="date_nhap" value="{$giatri.date_nhap}" /> 
                 <input type="hidden" name="id" value="{$giatri.id}" />

                 <input type="hidden" name="date_phatsinh2" value="{($giatri)?$giatri['date_phatsinh2']:''}" />
                 <input type="hidden" name="date_phatsinh3" value="{($giatri)?$giatri['date_phatsinh3']:''}" />
                 <input type="hidden" name="date_phatsinh4" value="{($giatri)?$giatri['date_phatsinh4']:''}" />
                 <input type="hidden" name="date_phatsinh5" value="{($giatri)?$giatri['date_phatsinh5']:''}" />
                 <input type="hidden" name="date_phatsinh6" value="{($giatri)?$giatri['date_phatsinh6']:''}" />
                 <input type="hidden" name="date_phatsinh7" value="{($giatri)?$giatri['date_phatsinh7']:''}" />

                </label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 2 sáng: </label></div>
                    <div class="col-md-6"><label style="color: blue">Thứ 2 chiều: </label></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="viec2s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 2">{($giatri)?$giatri['viec2s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="viec2c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 2">{($giatri)?$giatri['viec2c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 2: </span></div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 2: </span></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="phatsinh2s" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh sáng thứ 2">{($giatri)?$giatri['phatsinh2s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="phatsinh2c" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh chiều thứ 2">{($giatri)?$giatri['phatsinh2c']:''}</textarea>
                    </div>   
                </div>

                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 3 sáng: </label></div>
                    <div class="col-md-6"><label style="color: blue">Thứ 3 chiều: </label></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="viec3s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 3">{($giatri)?$giatri['viec3s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="viec3c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 3">{($giatri)?$giatri['viec3c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 3: </span></div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 3: </span></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="phatsinh3s" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh sáng thứ 3">{($giatri)?$giatri['phatsinh3s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="phatsinh3c" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh chiều thứ 3">{($giatri)?$giatri['phatsinh3c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 4 sáng: </label></div>
                    <div class="col-md-6"><label style="color: blue">Thứ 4 chiều: </label></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="viec4s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 4">{($giatri)?$giatri['viec4s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="viec4c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 4">{($giatri)?$giatri['viec4c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 4: </span></div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 4: </span></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="phatsinh4s" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh sáng thứ 4">{($giatri)?$giatri['phatsinh4s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="phatsinh4c" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh chiều thứ 4">{($giatri)?$giatri['phatsinh4c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 5 sáng: </label></div>
                    <div class="col-md-6"><label style="color: blue">Thứ 5 chiều: </label></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="viec5s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 5">{($giatri)?$giatri['viec5s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="viec5c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 5">{($giatri)?$giatri['viec5c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 5: </span></div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 5: </span></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="phatsinh5s" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh sáng thứ 5">{($giatri)?$giatri['phatsinh5s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="phatsinh5c" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh chiều thứ 5">{($giatri)?$giatri['phatsinh5c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 6 sáng: </label></div>
                    <div class="col-md-6"><label style="color: blue">Thứ 6 chiều: </label></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="viec6s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 6">{($giatri)?$giatri['viec6s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="viec6c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 6">{($giatri)?$giatri['viec6c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 6: </span></div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 6: </span></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="phatsinh6s" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh sáng thứ 6">{($giatri)?$giatri['phatsinh6s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="phatsinh6c" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh chiều thứ 6">{($giatri)?$giatri['phatsinh6c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><label style="color: blue">Thứ 7 sáng: </label></div>
                    <div class="col-md-6"><label style="color: blue">Thứ 7 chiều: </label></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="viec7s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 7">{($giatri)?$giatri['viec7s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="viec7c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 7">{($giatri)?$giatri['viec7c']:''}</textarea>
                    </div>   
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row" >
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh sáng thứ 7: </span></div>
                    <div class="col-md-6"><span style="color: blue">Nhiệm vụ phát sinh chiều thứ 7: </span></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <textarea name="phatsinh7s" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh sáng thứ 7">{($giatri)?$giatri['phatsinh7s']:''}</textarea>
                            
                    </div>
                    <div class="col-md-6">
                        <textarea name="phatsinh7c" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Nhập nhiệm vụ phát sinh chiều thứ 7">{($giatri)?$giatri['phatsinh7c']:''}</textarea>
                    </div>   
                </div>

                <div class="row" style="height: 7px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button> 
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

