<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    Danh sách kế hoạch công tác tháng
                </span>
            </div>
            <div class="col-md-8 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn tháng công tác: 
                </span>
                <label class="8 sl2">
                    <select name="month" style="" class="form-control" onchange="this.form.submit()">
                            {$i=1}
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>Tháng: {$i} </option>
                            {/for}
                    </select>
                </label>
                <label><a href="{$url}khctphong" class="btn btn-primary xxs"> Thêm mới nội dung KHCT tháng </a></label>
                <label><a href="{$url}kqkhctphong" class="btn btn-primary xxs"> Nhập kết quả KHTC tháng </a></label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th class="text-center">Nội dung</th>
                                    <th width="10%" class="text-center">Lãnh đạo sở chỉ đạo</th>
                                    <th width="10%" class="text-center">Lãnh đạo phòng chỉ đạo</th>
                                    <th width="10%" class="text-center">Cán bộ thực hiện</th>                         
                                    <th width="10%" class="text-center">Ngày bắt đầu</th>
                                    <th width="10%" class="text-center">Ngày kết thúc</th>
                                    <th width="5%" class="text-center">Sửa</th>
                                    <th width="5%" class="text-center">Xóa</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            {$i = 1}
                            {foreach $giatri as $thang}
                            {if $thang.id_sub == 0}
                                {$j=1}
                                <tr>
                                    <th class="text-center">{intToRoman($i++)}</th>
                                    <th class="text-left">{$thang.noidung}</th>
                                    <th class="text-center">{$lanhdaoso[$thang.ld_so]}</th>
                                    <th class="text-center">
                                    {if $thang.ld_phong !=''}
                                        {$arr_id = explode(',',$thang.ld_phong)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if}  
                                    </th>
                                    <th class="text-left">
                                    {if $thang.ld_phong !=''}
                                        {$arr_id = explode(',',$thang.user_id)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if} 
                                    </th>
                                    <th class="text-left">{date_select($thang.date_start)}</th>
                                    <th class="text-left">{date_select($thang.date_end)}</th>
                                    <th class="text-left"><a href="{$url}suakhctphong/{$thang.id}">Sửa</a></th>
                                    <th class="text-left"><a href="{$url}suakhctphong/{$thang.id}/1"  onclick="return confirm('Bạn có chắc chắn muốn xóa ?')">Xóa</a></th>
                                    
                                </tr>
                                {foreach $giatri as $thangsub}
                                    {if $thang.id == $thangsub.id_sub}
                                    <tr>
                                    <td class="text-center">{$j++}</td>
                                    <td class="text-left">{$thangsub.noidung}</td>
                                    <td class="text-center">{$lanhdaoso[$thangsub.ld_so]}</td>
                                    <td class="text-center">
                                    {if $thangsub.ld_phong !=''}
                                        {$arr_id = explode(',',$thangsub.ld_phong)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if}  
                                    </td>
                                    <td class="text-left">
                                    {if $thangsub.ld_phong !=''}
                                        {$arr_id = explode(',',$thangsub.user_id)}
                                        {foreach $arr_id as $id}
                                            {$canbophong[$id]},&nbsp;  
                                        {/foreach}
                                    {/if} 
                                    </td>
                                    <td class="text-left">{date_select($thangsub.date_start)}</td>
                                    <td class="text-left">{date_select($thangsub.date_end)}</td>
                                    <td class="text-left"><a href="{$url}suakhctphong/{$thangsub.id}">Sửa</a></td>
                                    <td class="text-left"><a href="{$url}suakhctphong/{$thangsub.id}/1" onclick="return confirm('Bạn có chắc chắn muốn xóa ?')" >Xóa</a></td>
                                </tr>                               
                                {/if}
                                {/foreach}
                            {/if}
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>   
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','top');
    });
</script>

