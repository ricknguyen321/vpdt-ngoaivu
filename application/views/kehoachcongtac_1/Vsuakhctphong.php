<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-8">
                <span class="font">
                    Sửa kế hoạch thực hiện nhiệm vụ trọng tâm tháng
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Tháng công tác: 
                </span>
                <label class="8 sl2">
                    {$giatri['thang']}
                    <input type="hidden" name="thang" value="{$giatri['thang']}">
                    <input type="hidden" name="id" value="{$giatri['id']}">
                    <input type="hidden" name="id_sub" value="{$giatri['id_sub']}">
                    <input type="hidden" name="department_id" value="{$giatri['department_id']}">
                </label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <tr>
                                <td width="10%" align="left">
                                    <label>Nội dung nhiệm vụ</label>
                                </td>
                                <td width="75%" align="left" colspan="4">
                                    <textarea name="noidung" class="form-control" rows="2" style="border: 1px solid #3c8dbc;">{($giatri)?$giatri['noidung']:''}</textarea>
                                </td>
                                <td width="15%" align="left" >
                                    <label>Lãnh đạo sở chỉ đạo</label>
                                
                                    <select name="ld_so" class="form-control select2">  
                                    <option value="0">Chọn lãnh đạo</option>                          
                                    {foreach $lanhdaoso as $ldso}
                                    <option value="{$ldso['PK_iMaCB']}" {if $ldso['PK_iMaCB'] == $giatri['ld_so']} selected {/if}>{$ldso['sHoTen']}</option>
                                    {/foreach}
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%" align="left">
                                   <label>LĐ phòng chỉ đạo</label>
                                </td>
                                <td width="15%" align="left">
                                    <select name="ld_phong[]" class="form-control select2" multiple="multiple">
                                        {foreach $canbophong as $ldphong}
                                        
                                        <option value="{$ldphong['PK_iMaCB']}" {if in_array($ldphong['PK_iMaCB'],explode(',',$giatri['ld_phong']))} selected="selected"{/if}>{$ldphong['sHoTen']}</option>
                                        {/foreach}
                                    </select>
                                </td>
                                <td width="10%" align="left">
                                    <label>CV thực hiện</label>
                                </td>
                                <td width="15%" align="left">
                                     <select name="user_id[]" class="form-control select2" multiple="multiple"> 
                                        {foreach $canbophong as $ldphong}
                                        <option value="{$ldphong['PK_iMaCB']}"  {if in_array($ldphong['PK_iMaCB'],explode(',',$giatri['user_id']))} selected="selected"{/if}>{$ldphong['sHoTen']}</option>
                                        {/foreach}
                                    </select>
                                </td>
                                <td width="15%" align="left">
                                    <label>ngày bắt đầu</label>                                
                                     <input type="text" value="{date('d-m-Y',strtotime($giatri['date_start']))}" name="date_start" class="form-control datepic datemask" >
                                    <script>
                                        $(document).ready(function() {
                                            //Date picker
                                            $('.datepic').datepicker({
                                                autoclose: true
                                            });
                                            //Datemask dd/mm/yyyy
                                            $(".datemask").inputmask("dd/mm/yyyy");
                                        });
                                    </script>
                                </td>
                                <td width="15%" align="left">
                                    <label>ngày kết thúc</label>                             
                                     <input tabindex="8" type="text" value="{date('d-m-Y',strtotime($giatri['date_end']))}" name="date_end" class="form-control datepic datemask">
                                    <script>
                                        $(document).ready(function() {
                                            //Date picker
                                            $('.datepic').datepicker({
                                                autoclose: true
                                            });
                                            //Datemask dd/mm/yyyy
                                            $(".datemask").inputmask("dd/mm/yyyy");
                                        });
                                    </script>
                                </td>
                            </tr>
                        </table>
                    </div> 
                    </div>                  
                   
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button> 
                        </div>
                    </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>



