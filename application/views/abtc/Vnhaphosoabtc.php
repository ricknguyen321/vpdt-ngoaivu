<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới hồ sơ ABTC
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:100%; justify-content: center;">
                        <div class="col-md-12" style="margin-top: 10px; ">

							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ngày đến</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngay_den" tabindex="8" value="{($thongtin)?($thongtin[0]['ngay_den']!=0000-00-00)?date_select($thongtin[0]['ngay_den']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số hiệu</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="so_hieu" value="{($thongtin)?$thongtin[0]['so_hieu']:''}" class="form-control req" id="" placeholder="VD: 200102-0003" >
                                    </div>
                                </div>  
								
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tên đơn vị *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="don_vi" tabindex="8" value="{($thongtin)?$thongtin[0]['don_vi']:''}" class="form-control req" id="" placeholder="VD: Công ty TNHH Cơ khí nhựa Thành Công" required>
                                    </div>
                                </div>   
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Mã số thuế</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="mst" tabindex="8" value="{($thongtin)?$thongtin[0]['mst']:''}" class="form-control req" id="" >
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Địa chỉ</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="dia_chi" tabindex="8" value="{($thongtin)?$thongtin[0]['dia_chi']:''}" class="form-control req" id="" >
                                    </div>
                                </div>
								
								 <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng thẻ </label>

                                    <div class="col-sm-8">
                                        <input type="text" name="so_luong_the" tabindex="8" value="{($thongtin)?$thongtin[0]['so_luong_the']:'1'}" class="form-control req" id="" required>
                                    </div>
                                </div>   
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Người được cấp - Chức vụ</label>

                                    <div class="col-sm-8">
                                        <textarea name="nguoi_duoc_cap" id="" class="form-control" placeholder="VD: Nguyễn Văn Bình - Chủ tịch HĐQT; Đường Vân Long - Tổng giám đốc, ..." rows="2">{($thongtin)?$thongtin[0]['nguoi_duoc_cap']:''}</textarea>
                                    </div>
                                </div>  

								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Cán bộ xử lý</label>

                                    <div class="col-sm-8">
										  <select name="cb_xu_ly" id="" required class="form-control select2">
											<option value="">-- Chọn cán bộ xử lý --</option>
                                            {if !empty($dscbls)}
                                                {foreach $dscbls as $cb}
                                                    <option value="{$cb.sHoTen}" {($thongtin)?($thongtin[0]['cb_xu_ly']==$cb.sHoTen)?'selected':'':''}>{$cb.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đến</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="vb_den" tabindex="8" value="{($thongtin)?$thongtin[0]['vb_den']:''}" class="form-control req" id="" placeholder="Nhập văn bản đến liên quan. VD: 3024, 3025, 3026">
                                    </div>
                                </div>   
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đi</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="vb_di" tabindex="8" value="{($thongtin)?$thongtin[0]['vb_di']:''}" class="form-control req" id="" placeholder="Nhập văn bản đi liên quan. VD: 324, 325, 326">
                                    </div>
                                </div>   
								
                            </div>							
							
							<div class="col-md-6">
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đề nghị cấp thẻ</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_dn" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_dn']:''}" class="form-control req" id="" placeholder="VD: 345, 346">
                                    </div>
                                </div>
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản gửi Công an</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_ca" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_ca']:''}" class="form-control req" id="" placeholder="VD: 345, 346">
                                    </div>
                                </div>
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản gửi Thuế</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_thue" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_thue']:''}" class="form-control req" id="" placeholder="VD: 445, 446">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản gửi BHXH</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_bhxh" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_bhxh']:''}" class="form-control req" id="" placeholder="VD: 545, 546">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản trình UBND</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_ubnd" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_ubnd']:''}" class="form-control req" id="" placeholder="VD: 645, 646">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản Công an trả lời</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_ca_tl" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_ca_tl']:''}" class="form-control req" id="" placeholder="VD: 645, 646">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản Thuế trả lời</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_thue_tl" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_thue_tl']:''}" class="form-control req" id="" placeholder="VD: 645, 646">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản BHXH trả lời</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_bhxh_tl" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_bhxh_tl']:''}" class="form-control req" id="" placeholder="VD: 645, 646">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản kết quả</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="vb_kq" tabindex="10" value="{($thongtin)?$thongtin[0]['vb_kq']:''}" class="form-control req" id="" placeholder="Nhập số hoặc ký hiệu văn bản kết quả">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ghi chú</label>

                                    <div class="col-sm-8">
										<textarea name="note" id="" class="form-control" placeholder="" rows="2">{($thongtin)?$thongtin[0]['note']:''}</textarea>
                                    </div>
                                </div>
								
								
                                
                                <div class="form-group noidung">
									<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                    <label for="" class="control-label col-sm-3" style="width:33.333333%; padding-top: 7px;">Tài liệu</label>
									
                                    <div class="col-sm-9" style="width:66.666666666%">
                                        <input type="file" name="files[]" multiple="" tabindex="6" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                    </div>
									</form>
                                </div>
								
								<div class="themsau"></div>
                              
							  
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style=""></label>

                                    <div class="col-sm-8">
                                        <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px; float:right"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
							  
								
								
								
                            </div>
                            
                        </div>
						
						
						<div class="col-md-12" style="margin-top:10px; margin-bottom:20px; text-align: center;">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm"><b>{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</b></button> &nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dshosoabtc"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">&#8594; Danh sách hồ sơ</button> </a>
                        </div>
						
						{if !empty($tailieu)}
							<div class="col-md-12" style="text-align: -webkit-center;">
                            <div class="form-group">
                                <label for="">Tài liệu đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style=""></th>
                                            <th class="text-center" width="15%"></th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                            <th class="text-center" width="8%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                        {foreach $tailieu as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td >{$f.sTenFile}</td>
												<td class="text-center">
													<a class="tin1" href="{$f.sDuongDan}" target="_blank">Xem</a> / <a class="tin1" href="{$f.sDuongDan}" download>Download</a>
												</td>
												
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
												
                                                <td class="text-center">
														<button type="submit" class="btn btn-default" onclick="return confirm('Bạn có chắc muốn xóa tệp tin này?');" name="xoa" value="{$f.sDuongDan}"><i class="fa fa-trash"></i></button>
																							
                                                </td>
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
                                </table><br>
                                
                            </div>
                        </div>
							{/if}
							
							
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

