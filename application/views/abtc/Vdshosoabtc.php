<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3" style="width:20%">
                    <h3 class="font">
                        Danh sách hồ sơ ABTC <b> ({$count})</b>
						<!--{if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs" onclick="myFunction()">Gửi nhiều email cùng lúc</button>{/if}-->
                    </h3>
                </div>
                <div class="col-sm-9" style="width:80%">
					<div class="col-md-5" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
									<input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm kiếm tổng hợp">
                               
                            </div>		
						</form>
					</div>
					<div class="col-md-5" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
								{if $vanban['FK_iMaPhongHD'] == 76 && date('Y') == $year} 
									<a href="nhaphosoabtc"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Tạo hồ sơ</button> </a>
								{/if}
                            </div>		
						</form>
					</div>
					
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
				<div class="pull-right" style="margin: -15px 10px">{$phantrang}</div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px">STT</th>
                                    <th width="8%">Số hiệu</th>
                                    <th width="25%" class="text-center" >Thông tin đơn vị</th>
									<th width="20%">Văn bản gửi các đơn vị</th>
									<th width="20%">Ý kiến của các đơn vị</th>
                                    <th width="">Ghi chú</th>
                                    <th width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsabtc)} 
								{$i=1}
								{$i = $i + $page}
                                {foreach $dsabtc as $abtc}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center" style="">
										<p>{date_select($abtc.ngay_den)}</p>
											<b>{$abtc.so_hieu}</b>
										</td>
                                       
                                        <td class="">
											<p><a href="hosoabtc?id={$abtc.id}"><b>{$abtc.don_vi}</b></a></p>
											- Số lượng thẻ: <b>{$abtc.so_luong_the}</b> <br>
											- Người được cấp: <b>{$abtc.nguoi_duoc_cap}</b><br>
											{$vbdn = explode(',' , $abtc.vb_dn)}
											- Văn bản đề nghị cấp thẻ: 
												{foreach $vbdn as $dn} 
													{$dn = trim($dn)}
														<a href="dsvanbanden?tonghop={$dn}" target="_blank"><b style="color:red">{$dn}</b></a>, 													
												{/foreach}
										</td>
										
										<td>
											{$vbden = explode(',' , $abtc.vb_den)}
											{$vbdi = explode(',' , $abtc.vb_di)}
											{$vbca = explode(',' , $abtc.vb_ca)}
											{$vbthue = explode(',' , $abtc.vb_thue)}
											{$vbbhxh = explode(',' , $abtc.vb_bhxh)}
											{$vbubnd = explode(',' , $abtc.vb_ubnd)}											
												
											<p>- VB gửi Công an:
												{foreach $vbca as $ca} 
													{$ca = trim($ca)}
													{if is_numeric($ca) && $ca > 500 && $abtc.ngay_den > '2020-04-01'}
														<a href="dsvanban?kyhieu={$ca}" target="_blank"><b style="color:red">{$ca}</b></a>, 
													{else}
														{$ca}, 
													{/if}
												{/foreach}
											</p>
											<p>- VB gửi đơn vị Thuế: 
												{foreach $vbthue as $thue} 
													{$thue = trim($thue)}
													{if is_numeric($thue) && $thue > 500 && $abtc.ngay_den > '2020-04-01'}
														<a href="dsvanban?kyhieu={$thue}" target="_blank"><b style="color:red">{$thue}</b></a>, 
													{else}
														{$thue}, 
													{/if}
												{/foreach}
											</p>
											<p>- VB gửi đơn vị BHXH: 
												{foreach $vbbhxh as $bhxh} 
													{$bhxh = trim($bhxh)}
													{if is_numeric($bhxh) && $bhxh > 500 && $abtc.ngay_den > '2020-04-01'}
														<a href="dsvanban?kyhieu={$bhxh}" target="_blank"><b style="color:red">{$bhxh}</b></a>, 
													{else}
														{$bhxh}, 
													{/if}
												{/foreach}
											</p>
											<p>- VB trình UBND: 
												{foreach $vbubnd as $ubnd} 
													{$ubnd = trim($ubnd)}
													{if is_numeric($ubnd) && $ubnd > 297 && $abtc.ngay_den > '2020-04-01'}
														<a href="dsvanban?kyhieu={$ubnd}" target="_blank"><b style="color:red">{$ubnd}</b></a>, 
													{else}
														{$ubnd}, 
													{/if}
												{/foreach}
											</p>
											
											{if !empty($abtc.vb_den)}
												- VB đến liên quan: 
												{foreach $vbden as $den} 
													{$den = trim($den)}
													{if is_numeric($den)}
														<a href="dsvanbanden?tonghop={$den}" target="_blank"><b style="color:red">{$den}</b></a>, 
													{else}
														{$den}, 
													{/if}
												{/foreach}
												<br>
											{/if}
											{if !empty($abtc.vb_di)}
												- VB đi liên quan: 
												{foreach $vbdi as $di} 
													{$di = trim($di)}
													{if is_numeric($di)}
														<a href="dsvanban?kyhieu={$di}" target="_blank"><b style="color:red">{$di}</b></a>, 
													{else}
														{$di}, 
													{/if}
												{/foreach}
											{/if}

										</td>
										<td>											
											{$vbcatl = explode(',' , $abtc.vb_ca_tl)}
											{$vbthuetl = explode(',' , $abtc.vb_thue_tl)}
											{$vbbhxhtl = explode(',' , $abtc.vb_bhxh_tl)}
											
											{$vbkq = explode(',' , $abtc.vb_kq)}											
												
											<p>- VB Công an trả lời: 
												{foreach $vbcatl as $catl} 
													{$catl = trim($catl)}
													{if is_numeric($catl)}
														<a href="dsvanbanden?tonghop={$catl}" target="_blank"><b style="color:red">{$catl}</b></a>, 
													{else}
														{$catl}, 
													{/if}
												{/foreach}
											</p>
											<p>- VB đơn vị Thuế trả lời: 
												{foreach $vbthuetl as $thuetl} 
													{$thuetl = trim($thuetl)}
													{if is_numeric($thuetl)}
														<a href="dsvanbanden?tonghop={$thuetl}" target="_blank"><b style="color:red">{$thuetl}</b></a>, 
													{else}
														{$thuetl}, 
													{/if}
												{/foreach}
											</p>
											<p>- VB đơn vị BHXH trả lời: 
												{foreach $vbbhxhtl as $bhxhtl} 
													{$bhxhtl = trim($bhxhtl)}
													{if is_numeric($bhxhtl)}
														<a href="dsvanbanden?tonghop={$bhxhtl}" target="_blank"><b style="color:red">{$bhxhtl}</b></a>, 
													{else}
														{$bhxhtl}, 
													{/if}
												{/foreach}
											</p>
											
											<p>- VB kết quả của UBND: 
												{foreach $vbkq as $kq} 
													{$kq = trim($kq)}
													{if is_numeric($kq)}
														<a href="dsvanbanden?tonghop={$kq}" target="_blank"><b style="color:red">{$kq}</b></a>, 
													{else}
														{$kq}, 
													{/if}
												{/foreach}
											</p>
											

										</td>
										<td>
											<p><b>{(!empty($abtc.trang_thai))?$abtc.trang_thai:(!empty($abtc.note))?$abtc.note:NULL}</b></p>
											- Xử lý HS: <b>{$abtc.cb_xu_ly}</b><br>
											- Nhập HS: {$abtc.cb_nhap}
										
										</td>
                                       
                                        <td class="text-center">
											{if ($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaPhongHD'] == 76) || $vanban['sHoTen'] == $abtc.cb_xu_ly || $vanban['PK_iMaCB'] == $abtc.FK_iMaCBNhap}
												<p><a href="nhaphosoabtc?id={$abtc.id}" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a></p>
												{if $vanban['PK_iMaCB'] == $abtc.FK_iMaCBNhap}
													<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Bạn có chắc muốn xóa?');" name="xoa" value="{$abtc.id}"><i class="fa fa-trash"></i></button>
												{/if}
											{else}
												-
											{/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
function myFunction($ma, $loai, $sodi) {	
	window.open("{$url}upload_files_vbdi?ma=" + $ma + "&loai=" + $loai + "&isodi=" + $sodi,"phathanh","menubar=1,location=no,resizable=0,width=550,height=320, top=180, left=400");
}
</script>