<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3" style="width:20%">
                    <h3 class="font">
                        Danh sách hồ sơ cấp thẻ ABTC <b> ({$count})</b>
						<!--{if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs" onclick="myFunction()">Gửi nhiều email cùng lúc</button>{/if}-->
                    </h3>
                </div>
                <div class="col-sm-9" style="width:80%">
					<div class="col-md-5" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
									<input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm kiếm tổng hợp">
                               
                            </div>		
						</form>
					</div>
					
					
                   
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
				<div class="pull-right" style="margin: -15px 10px">{$phantrang}</div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px">STT</th>
                                    <th width="14%">Họ tên</th>
                                    <th width="25%" class="text-center" >Công ty</th>
                                    <th width="12%">Số thẻ</th>
									<th width="10%">Ngày cấp</th>
									<th width="10%">Ngày hết hạn</th>
                                    <th width="">Trạng thái</th>
                                    <th width="8%">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsabtc)} 
								{$i=1}
								{$i = $i + $page}
								{$dsdonvi = layemaildonvi()}
                                {foreach $dsabtc as $abtc}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center" style="">
											<p><a href="hosoabtc?id={$abtc.id}"><b>{$abtc.name}</b></a></p>
											{$abtc.chucvu}
										</td>
                                       
                                        <td class="">
											<b>{$abtc.congty}</b>
											<br>- MST: {$abtc.mst}
											<br>- Địa chỉ: {$abtc.diachi}
										</td>
										<td class="text-center">
											<a href="hosoabtc?id={$abtc.id}"><b>{$abtc.doc_no}</b></a>
										</td>
										<td class="text-center">
											<b>{($abtc.ngaycap!='1970-01-01')?date_select($abtc.ngaycap):''}</b>
										</td>
										<td class="text-center">
											<b>{($abtc.expire_date!='1970-01-01')?date_select($abtc.expire_date):''}</b>
										</td>
										<td>
											<b>{$abtc.trang_thai}</b> 
										
										</td>
                                       
                                        {if $vanban['iQuyenHan_DHNB']==9 || $vanban['PK_iMaCB'] == 705 || $vanban['iQuyenHan_DHNB']==3 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731}
                                        <td class="text-center">
                                            <p><a href="nhaptheabtc?id={$abtc.id}" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default"><i class="fa fa-edit"></i></a></p>
											
                                        </td>
                                        <td class="text-center hide"><input type="checkbox" name="mavanban[{$di.PK_iMaVBDi}]"></td>
                                        {/if}
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
function myFunction($ma, $loai, $sodi) {	
	window.open("{$url}upload_files_vbdi?ma=" + $ma + "&loai=" + $loai + "&isodi=" + $sodi,"phathanh","menubar=1,location=no,resizable=0,width=550,height=320, top=180, left=400");
}
</script>