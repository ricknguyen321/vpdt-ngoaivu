<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới hồ sơ ABTC
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:100%; justify-content: center;">
                        <div class="col-md-12" style="margin-top: 10px; ">

							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Họ tên *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="name" tabindex="8" value="{($thongtin)?$thongtin[0]['name']:''}" class="form-control req" id="" required>
                                    </div>
                                </div>   

								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Ngày sinh *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="date_of_birth" tabindex="8" value="{($thongtin)?($thongtin[0]['date_of_birth']!=0000-00-00)?date_select($thongtin[0]['date_of_birth']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div> 								
								
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Giới tính</label>

                                    <div class="col-sm-8">
                                        <select name="sex" id="" required class="form-control select2">
                                            <option value="Male" {($thongtin)?($thongtin[0]['sex']=='Male')?'selected':'':''}>Male</option>
											<option value="Female" {($thongtin)?($thongtin[0]['sex']=='Female')?'selected':'':''}>Female</option>
											<option value="Other" {($thongtin)?($thongtin[0]['sex']=='Other')?'selected':'':''}>Other</option>
                                        </select>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">CMT/CCCD/Hộ chiếu *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="cmt_cccd_hc" value="{($thongtin)?$thongtin[0]['cmt_cccd_hc']:''}" class="form-control req" id="" required>
                                    </div>
                                </div>   
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ngày cấp thẻ ABTC</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngaycap" tabindex="8" value="{($thongtin)?($thongtin[0]['ngaycap']!='1970-01-01')?date_select($thongtin[0]['ngaycap']):'':''}" class="form-control datepic datemask" id="">
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ngày hết hạn</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="expire_date" tabindex="8" value="{($thongtin)?($thongtin[0]['expire_date']!='1970-01-01')?date_select($thongtin[0]['expire_date']):'':''}" class="form-control datepic datemask" id="">
                                    </div>
                                </div> 	
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số thẻ</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="doc_no" tabindex="10" value="{($thongtin)?$thongtin[0]['doc_no']:''}" class="form-control " id="" >
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Trạng thái</label>

                                    <div class="col-sm-8">
										<textarea name="trang_thai" id="" class="form-control" placeholder="Đã cấp/Chờ ý kiến của CA/Chờ ý kiến của Thuế/..." rows="2">{($thongtin)?$thongtin[0]['trang_thai']:''}</textarea>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đến</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="vbden" tabindex="8" value="{($thongtin)?$thongtin[0]['vbden']:''}" class="form-control req" id="" placeholder="Nhập văn bản đến liên quan. VD: 3024, 3025, 3026">
                                    </div>
                                </div>   
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đi</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="vbdi" tabindex="8" value="{($thongtin)?$thongtin[0]['vbdi']:''}" class="form-control req" id="" placeholder="Nhập văn bản đi liên quan. VD: 324, 325, 326">
                                    </div>
                                </div>  
								
                            </div>							
							
							<div class="col-md-6">
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Quốc tịch *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="country" tabindex="10" value="{($thongtin)?$thongtin[0]['country']:'Việt Nam'}" class="form-control req" id="" required>
                                    </div>
                                </div>
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Chức vụ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="chucvu" tabindex="10" value="{($thongtin)?$thongtin[0]['chucvu']:''}" class="form-control req" id="" required>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Công ty *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="congty" tabindex="10" value="{($thongtin)?$thongtin[0]['congty']:''}" class="form-control req" id="" required>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Mã số thuế *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="mst" tabindex="10" value="{($thongtin)?$thongtin[0]['mst']:''}" class="form-control req" id="" required>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Địa chỉ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="diachi" tabindex="10" value="{($thongtin)?$thongtin[0]['diachi']:''}" class="form-control req" id="" required>
                                    </div>
                                </div>
                                
                                <div class="form-group noidung">
									<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                    <label for="" class="control-label col-sm-3" style="width:33.333333%; padding-top: 7px;">Tài liệu</label>
									
                                    <div class="col-sm-9" style="width:66.666666666%">
                                        <input type="file" name="files[]" multiple="" tabindex="6" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                    </div>
									</form>
                                </div>
								
								<div class="themsau"></div>
                              
							  
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style=""></label>

                                    <div class="col-sm-8">
                                        <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px; float:right"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
							  
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ghi chú</label>

                                    <div class="col-sm-8">
										<textarea name="note" id="" class="form-control" placeholder="" rows="2">{($thongtin)?$thongtin[0]['note']:''}</textarea>
                                    </div>
                                </div>
								
								
                            </div>
                            
                        </div>
						
						{if !empty($tailieu)}
							<div class="col-md-12" style="text-align: -webkit-center;">
                            <div class="form-group">
                                <label for="">Tài liệu đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style=""></th>
                                            <th class="text-center" width="15%"></th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                            <th class="text-center" width="8%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                        {foreach $tailieu as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td >{$f.sTenFile}</td>
												<td class="text-center">
													<a class="tin1" href="{$f.sDuongDan}" target="_blank">Xem</a> / <a class="tin1" href="{$f.sDuongDan}" download>Download</a>
												</td>
												
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
												
                                                <td class="text-center">
														<button type="submit" class="btn btn-default" onclick="return confirm('Bạn có chắc muốn xóa tệp tin này?');" name="xoa" value="{$f.sDuongDan}"><i class="fa fa-trash"></i></button>
																							
                                                </td>
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
                                </table><br>
                                
                            </div>
                        </div>
							{/if}
							
							<div class="col-md-12" style="margin-top:20px; text-align: center;">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</button> &nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dstheabtc"><button tabindex="11" type="button"  class="btn btn-primary">Danh sách ABTC</button> </a>
                            </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

