<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Thông tin hồ sơ ABTC
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:100%; justify-content: center;">
                        <div class="col-md-12" style="margin-top: 10px; ">
							{$vbden = explode(',' , $thongtin[0]['vb_den'])}
							{$vbdi = explode(',' , $thongtin[0]['vb_di'])}
							{$vbdn = explode(',' , $thongtin[0]['vb_dn'])}
							{$vbca = explode(',' , $thongtin[0]['vb_ca'])}
							{$vbthue = explode(',' , $thongtin[0]['vb_thue'])}
							{$vbbhxh = explode(',' , $thongtin[0]['vb_bhxh'])}
							{$vbcatl = explode(',' , $thongtin[0]['vb_ca_tl'])}
							{$vbthuetl = explode(',' , $thongtin[0]['vb_thue_tl'])}
							{$vbbhxhtl = explode(',' , $thongtin[0]['vb_bhxh_tl'])}
							{$vbubnd = explode(',' , $thongtin[0]['vb_ubnd'])}
							{$vbkq = explode(',' , $thongtin[0]['vb_kq'])}
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Ngày nhận hồ sơ:</label>

                                    <div class="col-sm-8">
                                        <b>{date_select(($thongtin)?$thongtin[0]['ngay_den']:'')}</b>
                                    </div>
                                </div>  
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số hiệu hồ sơ:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['so_hieu']:''}</b>
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Tên đơn vị:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['don_vi']:''}</b>
                                    </div>
                                </div>  
								 <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Mã số thuế:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['mst']:''}</b>
                                    </div>
                                </div>  
								 <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Địa chỉ:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['dia_chi']:''}</b>
                                    </div>
                                </div>  

								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Số lượng thẻ xin cấp:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['so_luong_the']:''}</b>
                                    </div>
                                </div>  	
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Người được cấp - chức vụ:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['nguoi_duoc_cap']:''}</b>
                                    </div>
                                </div>  	
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Cán bộ xử lý:</label>

                                    <div class="col-sm-8">
                                        <b>{($thongtin)?$thongtin[0]['cb_xu_ly']:''}</b>
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đến liên quan:</label>

                                    <div class="col-sm-8">
                                        {foreach $vbden as $den} 
													{$den = trim($den)}
													{if is_numeric($den)}
														<a href="dsvanbanden?tonghop={$den}" target="_blank"><b style="color:red">{$den}</b></a>, 
													{else}
														{$den}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đi liên quan:</label>

                                    <div class="col-sm-8">
                                        {foreach $vbdi as $di} 
													{$di = trim($di)}
													{if is_numeric($di)}
														<a href="dsvanban?kyhieu={$di}" target="_blank"><b style="color:red">{$di}</b></a>, 
													{else}
														{$di}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								
                            </div>							
							
							<div class="col-md-6">
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản đề nghị cấp thẻ:</label>

                                    <div class="col-sm-8">
										{foreach $vbdn as $dn} 
													{$dn = trim($dn)}
													{if is_numeric($dn)}
														<a href="dsvanbanden?tonghop={$dn}" target="_blank"><b style="color:red">{$dn}</b></a>, 
													{else}
														{$dn}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản gửi Công an:</label>

                                    <div class="col-sm-8">
										{foreach $vbca as $ca} 
													{$ca = trim($ca)}
													{if is_numeric($ca) && $ca > 500  && $thongtin[0]['ngay_den'] > '2020-04-01'}
														<a href="dsvanban?kyhieu={$ca}" target="_blank"><b style="color:red">{$ca}</b></a>, 
													{else}
														{$ca}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản gửi Thuế:</label>

                                    <div class="col-sm-8">
                                        {foreach $vbthue as $thue} 
													{$thue = trim($thue)}
													{if is_numeric($thue) && $thue > 500  && $thongtin[0]['ngay_den'] > '2020-04-01'}
														<a href="dsvanban?kyhieu={$thue}" target="_blank"><b style="color:red">{$thue}</b></a>, 
													{else}
														{$thue}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản gửi BHXH:</label>

                                    <div class="col-sm-8">
                                        {foreach $vbbhxh as $bhxh} 
													{$bhxh = trim($bhxh)}
													{if is_numeric($bhxh) && $bhxh > 500  && $thongtin[0]['ngay_den'] > '2020-04-01'}
														<a href="dsvanban?kyhieu={$bhxh}" target="_blank"><b style="color:red">{$bhxh}</b></a>, 
													{else}
														{$bhxh}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản Công an trả lời:</label>

                                    <div class="col-sm-8">
										{foreach $vbcatl as $catl} 
													{$catl = trim($catl)}
													{if is_numeric($catl)}
														<a href="dsvanbanden?tonghop={$catl}" target="_blank"><b style="color:red">{$catl}</b></a>, 
													{else}
														{$catl}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản Thuế trả lời :</label>

                                    <div class="col-sm-8">
                                        {foreach $vbthuetl as $thuetl} 
													{$thuetl = trim($thuetl)}
													{if is_numeric($thuetl)}
														<a href="dsvanbanden?tonghop={$thuetl}" target="_blank"><b style="color:red">{$thuetl}</b></a>, 
													{else}
														{$thuetl}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản BHXH trả lời:</label>

                                    <div class="col-sm-8">
                                        {foreach $vbbhxhtl as $bhxhtl} 
													{$bhxhtl = trim($bhxhtl)}
													{if is_numeric($bhxhtl)}
														<a href="dsvanbanden?tonghop={$bhxhtl}" target="_blank"><b style="color:red">{$bhxhtl}</b></a>, 
													{else}
														{$bhxhtl}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Văn bản trình UBND:</label>

                                    <div class="col-sm-8">
										{foreach $vbubnd as $ubnd} 
													{$ubnd = trim($ubnd)}
													{if is_numeric($ubnd) && $ubnd > 297  && $thongtin[0]['ngay_den'] > '2020-04-01'}
														<a href="dsvanban?kyhieu={$ubnd}" target="_blank"><b style="color:red">{$ubnd}</b></a>, 
													{else}
														{$ubnd}, 
													{/if}
												{/foreach}
                                    </div>
                                </div> 
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Kết quả:</label>

                                    <div class="col-sm-8">
										{foreach $vbkq as $kq} 
													{$kq = trim($kq)}
													{if is_numeric($kq)}
														<a href="dsvanbanden?tonghop={$kq}" target="_blank"><b style="color:red">{$kq}</b></a>, 
													{else}
														{$kq}, 
													{/if}
												{/foreach}
                                    </div>
                                </div>                                         
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Trạng thái/Ghi chú:</label>

                                    <div class="col-sm-8">
										{(!empty($thongtin[0]['trang_thai']))?$thongtin[0]['trang_thai']:(!empty($thongtin[0]['note']))?$thongtin[0]['note']:NULL}
                                    </div>
                                </div>
								{$cb = layTTCB($thongtin[0]['FK_iMaCBNhap'])}
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Người nhập hồ sơ:</label>

                                    <div class="col-sm-8">
										{$cb[0].sHoTen}
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Thời gian nhập:</label>

                                    <div class="col-sm-8">
										{date_time(($thongtin)?$thongtin[0]['ngay_nhap']:'')}
                                    </div>
                                </div>
								
                            </div>
                            
                        </div>
						
						<div class="col-md-12" style="margin-top:10px; margin-bottom:20px; text-align: center;">
								{if ($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaPhongHD'] == 76) || $vanban['sHoTen'] == $thongtin[0]['cb_xu_ly'] || $vanban['PK_iMaCB'] == $thongtin[0]['FK_iMaCBNhap']}
									<a href="nhaphosoabtc?id={$thongtin[0]['id']}"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Sửa hồ sơ</button> </a> 
								{/if}
								&nbsp;&nbsp;&nbsp;&nbsp;
								<a data-toggle="collapse" href="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3"><b style="color:black"> <button class="btn btn-primary btn-sm">Tải biểu mẫu</button></b></a>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dshosoabtc"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">&#8594; Danh sách hồ sơ</button> </a>
                        </div>
							<div class="col-sm-12 collapse" id="collapseExample3" style="text-align: center;">
								<div class="col-md-6">
									<p><a href="dshosoabtc">Công văn gửi Công an</a></p>
									<p><a href="dshosoabtc">Công văn gửi Thuế</a></p>
									<p><a href="dshosoabtc">Công văn gửi BHXH</a></p>
									<p><a href="dshosoabtc">Công văn trình UBND Thành phố</a></p>
								</div>
								<div class="col-md-6">
									<p><a href="dshosoabtc">Công văn đôn đốc gửi Công an</a></p>
									<p><a href="dshosoabtc">Công văn đôn đốc gửi Thuế</a></p>
									<p><a href="dshosoabtc">Công văn đôn đốc gửi BHXH</a></p>
								</div>
                            </div>
                            
						
							<div class="col-md-12" style="text-align: -webkit-center; margin-top:20px; margin-bottom:100px;">
                            <div class="form-group">
                                <label for="">Tài liệu đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style=""></th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                        {foreach $tailieu as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td ><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile} </a></td>												
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
												
                                             
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
                                </table>
								
								{if $i == 1}
									<p>Chưa có tài liệu đính kèm. </p>
									Vui lòng scan tài liệu, hồ sơ và upload lên hệ thống để phục vụ công tác lưu trữ hồ sơ điện tử.
								{/if}
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');
		$('.control-label').css('font-weight', '200');
		$('.control-label').css('padding-top', '0px');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

