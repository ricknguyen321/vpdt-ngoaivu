<link rel="stylesheet" href="{$url}assets/css/custom_ad.css">
<div class="col-sm-6">
	<div class="col-sm-12 shadow">
		<div class="loaitin deign-xs">
			TIN TỨC - SỰ KIỆN TIÊU BIỂU
		</div>
		{if !empty($tinnoibat)}
		<div class="slider">
			<div id="banner-slide">
		        <ul class="bjqs">
					{foreach $tinnoibat as $key => $tin}
					{if $key < 3}
                 	<li class="bjqs-slide"><a target="_blank" href="{$url}xemtintuc/{$tin.PK_iMaTT}_{clear($tin.sTieuDe)}.html"><img src="{$url}{if $tin.duongdan=='dung'}anhbaiviet/{$tin.PK_iMaTT}.jpg{else}assets/img/no-image.jpg{/if}" title="{$tin.sTieuDe}"></a><p class="bjqs-caption">{$tin.sTieuDe}</p></li>
                 	{/if}
                 	{/foreach}

		        </ul>
		        <!-- end Basic jQuery Slider -->
      			<ul class="bjqs-controls v-centered"><li class="bjqs-prev"><a href="#" data-direction="previous" style="top: 50%;"><span class="glyphicon glyphicon-chevron-left"></span></a></li><li class="bjqs-next"><a href="#" data-direction="forward" style="top: 50%;"><span class="glyphicon glyphicon-chevron-right"></span></a></li></ul>
      			<script>
		      		$(document).ready(function() {
		      			$('ul.bjqs-controls').eq(0).addClass('hide');
		      		});
	      		</script>
	    	</div>
		</div>
		<div class="">
			<ul class="sortable-list connectList agile-list ui-sortable" id="todo" style="padding:0px;">
			{foreach $tinnoibat as $key => $tin}
				{if $key >= 3}
	                <li class="success-element">     
		                <a class="tinmoi" href="{$url}xemtintuc/{$tin.PK_iMaTT}_{clear($tin.sTieuDe)}.html">{$tin.sTieuDe}</a>
		            </li>
		        {/if}
            {/foreach}
	        </ul>
		</div>
		{/if}
	</div>
	{if !empty($khauhieu)}
		{if $khauhieu[0]['iTrangThai']!=1}
		{if !empty($khauhieu[0]['sLink'])}<a href="{$khauhieu[0]['sLink']}">{/if}
		<div class="col-sm-12 shadow" style="width:100%; height:96px; background-image:url({$url}assets/img/khauhieu.jpg)">
			<h2 class="text-center" style="color:#FFF;padding:10px!important;font-size:1.3em!important"><b>{$khauhieu[0]['sNoiDung']}</b></h2>
		</div>
		{if !empty($khauhieu[0]['sLink'])}</a>{/if}
		{/if}
	{/if}
	{if !empty($dsloaitin)}
		{foreach $dsloaitin as $lt}
			<div class="col-sm-12 shadow">
				<div class="loaitin deign-xs" style="text-transform:uppercase">
					<a style="color:#fff;" href="{$url}{$lt.PK_iMaLT}_{clear($lt.sTenLT)}">{$lt.sTenLT}</a>
				</div>
				<div style="padding-left:10px;">
					{if !empty($dstin)}
						{foreach $dstin as $key => $t}
							{if $t.FK_iMaLT==$lt.PK_iMaLT}
								{if $key < 3}
								<div class="col-sm-12" style="margin-top: 10px;padding-right: 10px !important;border-bottom: 1px solid #eee;">
									<div class="col-sm-3">
										<a href="{$url}xemtintuc/{$t.PK_iMaTT}_{clear($t.sTieuDe)}.html"><img src="{$url}{if $t.duongdan=='dung'}anhbaiviet/{$t.PK_iMaTT}.jpg{else}assets/img/no-image.jpg{/if}" width="95%" alt=""></a>
									</div>
									<div class="col-sm-9">
										<h2><a href="{$url}xemtintuc/{$t.PK_iMaTT}_{clear($t.sTieuDe)}.html"><b>{$t.sTieuDe}</b></a></h2>
										<div class="calendar-rmenu"><span class="glyphicon glyphicon-calendar"></span> {date_select($t.sNgayDang)}</div>
										<p >{$t.sTomTat}</p>
									</div>
								</div>
								{/if}
							{/if}
						{/foreach}
					{/if}
<!--					<div class="col-sm-12"><a class="pull-right" href="{$url}{$lt.PK_iMaLT}_{clear($lt.sTenLT)}/">Xem thêm <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>-->
				</div>
			</div>
		{/foreach}
	{/if}
</div>