<div class="content-wrapper">
{if $vanban['PK_iMaCB'] == $tenhoso[0]['FK_iMaCB']}
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Chi tiết hồ sơ:<b> {$tenhoso[0]['sTenHS']}</b>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
					
						 <form action="" method="post">
						 <div class="modal-dialog" role="document" style="width: 90%; margin-top: 0px;">
										<div class="modal-content">
											
											<div class="modal-body">
												<input type="text" class="hide" name="mahoso" value="{$tenhoso[0]['PK_iMaHS']}">
												<label for="">Tìm kiếm văn bản để thêm vào hồ sơ</label> <Br>
												
												<div style="display:flex">
												<input type="text" name="trichyeu" class="form-control" placeholder="Nhập trích yếu cần tìm" style="width:25%" value="{$trichyeu}"> &nbsp;&nbsp;&nbsp; hoặc &nbsp;&nbsp;&nbsp;
												<input type="text" name="sovanban" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control " placeholder="Nhập số văn bản" style="width:15%" value="{$sovb}">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="vanban" value="di" class="" {if $loai == 'di'}checked{/if}> Văn bản đi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="vanban" value="den" class="" {if $loai == 'den'}checked{/if}> Văn bản đến 
												</div>
												<br><button type="submit" value="timkiem2" name="timkiem2" class="btn btn-primary btn-sm">Tìm kiếm</button>
												<hr>
												<div class="ketqua" style="margin-top: 10px;">
													{foreach $dsvb as $vb}
														{$vb.sMoTa = limit_word($vb.sMoTa,23)}
														<p><button type="submit" class="btn btn-primary btn-sm" name="luuhoso2" value="{$vb.mavanban}">+</button> &nbsp;&nbsp; <label style="width:90%"><b style="color:red">{$vb.iso}</b> - {if $loai == 'den'}<a href="quatrinhxuly/{$vb.mavanban}" target="_blank">{$vb.sMoTa}</a>{else}<a href="thongtinvanban?id={$vb.mavanban}" target="_blank">{$vb.sMoTa}</a> {/if}</label></p>
													{/foreach}
													
												</div>
											</div>
											
										</div>
						</div>
						</form>
						
                        <form action="" method="post">
						 {if !empty($dsdulieu)}{$i=1}
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="8%">Số Đến</th>
                                    <th width="8%">Số Đi</th>
                                    <th width="8%">Ngày</th>
                                    <th width="">Mô tả</th>
                                    <!--<th width="5%">File</th>-->
                                    <th width="5%">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                                    {foreach $dsdulieu as $dl}
										{$dl.sMoTa = limit_word($dl.sMoTa, 40)}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center" style="font-weight: bold; color:red">{($dl.FK_iMaVBDen>0)?$dl.iSoVB:''}</td>
                                            <td class="text-center" style="font-weight: bold; color:red">{($dl.FK_iMaVBDi>0)?$dl.iSoVB:''}</td>
                                            <td >{date_select($dl.sNgayNhap)}</td>
                                            <td >
											<p><a target="_blank" href="{$url}{if $dl.FK_iMaVBDi>0}thongtinvanban?id={$dl.FK_iMaVBDi}{else}quatrinhxuly/{$dl.FK_iMaVBDen}{/if}" class="tin1"><b>{$dl.sMoTa}</b></a></p>
											<!--- Ngày nhập: {date_select($dl.sNgayNhap)}<br>-->
											
											</td>
											
                                            <!--<td class="text-center">{if !empty($dl.sDuongDan) && $dl.sDuongDan!='doc_uploads/'}<a href="{$dl.sDuongDan}" target="_blank" class="tin1">Xem</a>{/if}</td>-->
                                            <td class="text-center">{if $dl.FK_iMaCB==$vanban['PK_iMaCB']}<button type="submit" value="{$dl.PK_iMaHSVB}" name="xoavanban" class="btn btn-default btn-sm" onclick="return confirm('Bạn có muốn xóa văn bản này?')"><i class="fa fa-trash-o"></i></button>{else}-{/if}</td>
                                        </tr>
                                    {/foreach}

                              
                               
                            </tbody>
                        </table>
						  {else}
                              
                                    <div class="text-center"><b>Chưa có văn bản nào trong hồ sơ này ...</b></div>
                               
						 {/if}
                        </form>
                        <div><a href="{$url}hoso" class="btn btn-default btn-sm"><< Quay lại</a></div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
	{/if}
</div>

<script>
    $(document).ready(function() {
        var url = window.location.href;
        $(document).on('click','button[name=gomnhomvanban]',function(){
            var ten = $(this).val();
            var id  = $(this).attr('id');
            $('input[name=mahoso]').val(id);
            $('button[name=timkiem]').val(id);
            $('h4').html(ten);
        });
        $(document).on('click','button[name=timkiem]',function(){
            var vanban   = $('input[name=vanban]:checked').val();
            var sovanban = $('input[name=sovanban]').val();
			var trichyeu = $('input[name=trichyeu]').val();
            var mahoso   = $(this).val();
            if(sovanban.length>0 || trichyeu.length>0)
            {
                $.ajax({
                    url:url,
                    type:'POST',
                    data:{
                        action:'timkiem',
                        vanban:vanban,
                        sovanban:sovanban,
						trichyeu:trichyeu,
                        mahoso:mahoso
                    },
                    success:function(repon){
                        var result = JSON.parse(repon);
                         var tong  = result.length;
                        if(tong>0){
                            var html='';
                            for(var i=0; i<tong;i++)
                            {
                                html+="<p><input type='radio' name='ma' value='"+result[i]['mavanban']+"' /> ";
                                html+="<button style='max-width:100%' name='ten' class='btn btn-default btn-sm'>";
                                html+='('+result[i]['sKyHieu']+') '+litmit_word(result[i]['sMoTa'],35);
                                html+="</button></p>";
                            }
                            $('div.ketqua').html(html);
                        }
                        else{
                            $('div.ketqua').html('');
                        }
                    }
                });
            }
            else{
                $('div.ketqua').html('');
            }
            
        });
        $(document).on('click','button[name=luuhoso]',function(){
            var loaivanban = $('input[name=vanban]:checked').val();
            var mahoso     = $('input[name=mahoso]').val();
            var mavanban   = $('input[name=ma]:checked').val();
            // var mavanban = $("input[name^='ma']:checked").map(function (idx, ele) {
            //    return $(ele).val();
            // }).get();
            if(typeof(mavanban) != "undefined" && mavanban !== null) {
                $.ajax({
                    url:url,
                    type:'post',
                    data:{
                        action:'themdulieu',
                        loaivanban: loaivanban,
                        mahoso:mahoso,
                        mavanban:mavanban
                    },
                    success:function(repon){
                        var result = JSON.parse(repon);
                        if(result=='trung'){
                            $('button[name=close]').click();
                            showMessage('Văn bản này đã có trong hồ sơ','danger');
                        }
                        if(result=='ok')
                        {
                            $('button[name=close]').click();
                            showMessage('Thêm văn bản vào hồ sơ thành công','info');
							window.location="chitiethoso?id="+mahoso;
                        }
                    }
                });
            }
            else{
                $('button[name=close]').click();
                showMessage('Bạn chưa chọn văn bản cho hồ sơ','danger');
            }
        });
        closeModal();
    });
    function closeModal(){
        $(document).on('hide.bs.modal','.modal', function () {
            // $("#hoso").val("").trigger("change");
            // $('#hoso').select2();
            $('input[name=sovanban]').val('');
            $('input[name=mahoso]').val();
            $('div.ketqua').html('');
        });
    }
    function litmit_word(text,word)
    {
        var mang = text.split(' ');
        var tong = mang.length;
        var ketqua = '';
        if(tong>=word)
        {
            for(var i=0;i<word;i++)
            {
                ketqua+=mang[i]+' ';
            }
            return ketqua=ketqua.trim()+' ...';
        }
        else{
            
            return text;
        }
    }
</script>