<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Chi tiết hồ sơ {$tenhoso[0]['sTenHS']}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
                        <form action="" method="post">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="10%">Số VB Đến</th>
                                    <th width="10%">Số VB Đi</th>
                                    <th width="10%">Loại văn bản</th>
                                    <th width="10%">Số ký hiệu</th>
                                    <th width="10%">Ngày nhập</th>
                                    <th width="40%">Mô tả</th>
                                    <th width="5%">File</th>
                                    <th width="5%">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsdulieu)}{$i=1}
                                    {foreach $dsdulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center" style="font-weight: bold;">{($dl.FK_iMaVBDen>0)?$dl.iSoVB:''}</td>
                                            <td class="text-center" style="font-weight: bold;">{($dl.FK_iMaVBDi>0)?$dl.iSoVB:''}</td>
                                            <td >{$dl.sTenLVB}</td>
                                            <td >{$dl.sKyHieu}</td>
                                            <td class="text-center">{date_select($dl.sNgayNhap)}</td>
                                            <td ><a href="{$url}{if $dl.FK_iMaVBDi>0}thongtinvanban?id={$dl.FK_iMaVBDi}{else}quatrinhxuly/{$dl.FK_iMaVBDen}{/if}" class="tin1">{$dl.sMoTa}</a></td>
                                            <td class="text-center">{if !empty($dl.sDuongDan) && $dl.sDuongDan!='doc_uploads/'}<a href="{$url}{$dl.sDuongDan}" target="_blank" class="tin1">Xem</a>{/if}</td>
                                            <td class="text-center">
                                                {if $dl.FK_iMaCB==$taikhoan}
                                                <button type="submit" value="{$dl.PK_iMaHSVB}" name="xoavanban" class="btn btn-default btn-sm" onclick="return confirm('Bạn có muốn xóa văn bản này?')"><i class="fa fa-trash-o"></i></button>
                                                {/if}
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        </form>
                        <div><a href="{$url}hoso" class="btn btn-default btn-sm">Quay lại >></a></div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>