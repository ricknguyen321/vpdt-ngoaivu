<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Quản lý hồ sơ
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" method="get" autocomplete="off">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="phongban" style="width: 100%" id="" class="form-control select2">
                                    <option value="">-- Chọn phòng ban --</option>
                                    {if !empty($dsphongban)}
                                        {foreach $dsphongban as $pb}
                                            <option value="{$pb.PK_iMaPB}" {($phongban)?($phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <button class="btn btn-primary btn-xs">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
                </form>
                <br><br>
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="30%">Tên hồ sơ</th>
                                    <th width="40%">Mô tả</th>
                                    <th width="10%">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsdulieu)}{$i=1}
                                    {foreach $dsdulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td><a href="{$url}chitiethoso?id={$dl.PK_iMaHS}">{$dl.sTenHS}</a></td>
                                            <td >{$dl.sMoTa}</td>
                                            <td class="text-center">
                                                <a href="{$url}chitiethoso?id={$dl.PK_iMaHS}" data-toggle="tooltip" data-placement="top" data-original-title="Xem chi tiết" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>