<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Quản lý hồ sơ
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">Tên hồ sơ <label class="required" for="">*</label></label>
                            <input type="text" name="ten" class="form-control" placeholder="Nhập tên hồ sơ" required value="{($thongtin)?($thongtin[0]['sTenHS']):NULL}">
                        </div>
                        <div class="form-group">
                            <label for="">Mô tả </label>
                            <textarea name="mota" id="" class="form-control" cols="" rows="4">{($thongtin)?($thongtin[0]['sMoTa']):NULL}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Trạng thái</label>
                            <label style="padding-left:25px">
                                <input type="radio" name="trangthai" value="1" class="flat-red" checked>  Hiển thị
                            </label>
                            <label style="padding-left:25px">
                                <input type="radio" name="trangthai" value="2" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==2}checked{/if}{/if} class="flat-red">  Không hiển thị
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                            {if !empty($thongtin)}<a href="{$url}hoso" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="30%">Tên hồ sơ</th>
                                    <th width="40%">Mô tả</th>
                                    <th width="15%">Trạng thái</th>
                                    <th width="10%">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsdulieu)}{$i=1}
                                    {foreach $dsdulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td><a href="{$url}hoso?id={$dl.PK_iMaHS}">{$dl.sTenHS}</a></td>
                                            <td >{$dl.sMoTa}</td>
                                            <td class="text-center"><label for="" class="label {($dl.iTrangThai==1)?'label-success':'label-warning'}">{($dl.iTrangThai==1)?'Hiển thị':'Không'}</label></td>
                                            <td class="text-center">
                                                {if $dl.iTrangThai==1}<button type="button" name="gomnhomvanban" value="{$dl.sTenHS}" id="{$dl.PK_iMaHS}" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Gom nhóm văn bản"><i class="fa fa-plus"></i></button>{/if}
                                                <a href="{$url}chitiethoso?id={$dl.PK_iMaHS}" data-toggle="tooltip" data-placement="top" data-original-title="Xem chi tiết" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document" style="width: 80%">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="text" class="hide" name="mahoso">
                                        <label for="">Tạo hồ sơ từ: </label> <input type="radio" name="vanban" value="di" class="sovanban" checked=""> Văn bản đi &nbsp;&nbsp;&nbsp;<input type="radio" name="vanban" value="den" class="sovanban"> Văn bản đến 
                                        <input type="text" name="sovanban" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control sovanban" placeholder="Nhập số văn bản cần tìm"><br>
                                        <div class="ketqua"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" name="close" value="close" class="btn btn-default btn-sm" data-dismiss="modal">Đóng</button>
                                        <button type="button" class="btn btn-primary btn-sm" name="luuhoso" value="luuhoso">Lưu lại</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $(document).on('click','button[name=gomnhomvanban]',function(){
            var ten = $(this).val();
            var id  = $(this).attr('id');
            $('input[name=mahoso]').val(id);
            $('h4').html(ten);
        });
        $(document).on('change','.sovanban',function(){
            var vanban   = $('input[name=vanban]:checked').val();
            var sovanban = $('input[name=sovanban]').val();
            if(sovanban.length>0)
            {
                $.ajax({
                    url:url,
                    type:'POST',
                    data:{
                        action:'timkiem',
                        vanban:vanban,
                        sovanban:sovanban
                    },
                    success:function(repon){
                        var result = JSON.parse(repon);
                         var tong  = result.length;
                        if(tong>0){
                            var html='';
                            for(var i=0; i<tong;i++)
                            {
                                html+="<p><input type='radio' name='ma' value='"+result[i]['mavanban']+"' /> ";
                                html+="<button style='max-width:100%' name='ten' class='btn btn-default btn-sm'>";
                                html+='('+result[i]['sKyHieu']+') '+litmit_word(result[i]['sMoTa'],35);
                                html+="</button></p>";
                            }
                            $('div.ketqua').html(html);
                        }
                        else{
                            $('div.ketqua').html('');
                        }
                    }
                });
            }
            else{
                $('div.ketqua').html('');
            }
            
        });
        $(document).on('click','button[name=luuhoso]',function(){
            var loaivanban = $('input[name=vanban]:checked').val();
            var mahoso     = $('input[name=mahoso]').val();
            var mavanban   = $('input[name=ma]:checked').val();
            // var mavanban = $("input[name^='ma']:checked").map(function (idx, ele) {
            //    return $(ele).val();
            // }).get();
            if(typeof(mavanban) != "undefined" && mavanban !== null) {
                $.ajax({
                    url:url,
                    type:'post',
                    data:{
                        action:'themdulieu',
                        loaivanban: loaivanban,
                        mahoso:mahoso,
                        mavanban:mavanban
                    },
                    success:function(repon){
                        var result = JSON.parse(repon);
                        if(result=='trung'){
                            $('button[name=close]').click();
                            showMessage('Văn bản này đã có trong hồ sơ','danger');
                        }
                        if(result=='ok')
                        {
                            $('button[name=close]').click();
                            showMessage('Thêm văn bản vào hồ sơ thành công','info');

                        }
                    }
                });
            }
            else{
                $('button[name=close]').click();
                showMessage('Bạn chưa chọn văn bản cho hồ sơ','danger');
            }
        });
        closeModal();
    });
    function closeModal(){
        $(document).on('hide.bs.modal','.modal', function () {
            // $("#hoso").val("").trigger("change");
            // $('#hoso').select2();
            $('input[name=sovanban]').val('');
            $('input[name=mahoso]').val();
            $('div.ketqua').html('');
        });
    }
    function litmit_word(text,word)
    {
        var mang = text.split(' ');
        var tong = mang.length;
        var ketqua = '';
        if(tong>=word)
        {
            for(var i=0;i<word;i++)
            {
                ketqua+=mang[i]+' ';
            }
            return ketqua=ketqua.trim()+' ...';
        }
        else{
            
            return text;
        }
    }
</script>