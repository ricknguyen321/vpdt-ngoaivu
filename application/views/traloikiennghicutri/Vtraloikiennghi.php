<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Trả lời kiến nghị
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3" style="padding-right: 0px;">
            <a href="javascript:void(0)" class="btn btn-primary btn-block margin-bottom">{if isset($mangphong[$thongtin[0]['FK_iMaPhong_CT']])}{$mangphong[$thongtin[0]['FK_iMaPhong_CT']]}{/if}</a>

            <div class="box box-solid">
                <div class="no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        {if !empty($lienquan)}{$dem=1}
                            {foreach $lienquan as $lq}
                                <li style="text-align: justify;"><a href="{$url}traloikiennghi/{$lq.PK_iMaKienNghi}">{$dem++}. {$lq.sNoiDung}</a></li>
                            {/foreach}
                        {/if}
                    </ul>
                </div>
            <!-- /.box-body -->
            </div>   
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
            <!-- /.box-header -->
                <div class="no-padding">
                    <div class="mailbox-read-info">
                        <h3>{$thongtin[0]['sNoiDung']}</h3>
                    </div>
                  <!-- /.mailbox-controls -->
                    <div class="mailbox-read-message">
                       {$thongtin[0]['sNoiDungTraLoi']}
                    </div>
                  <!-- /.mailbox-read-message -->
                </div>
            <!-- /.box-footer -->
            </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>