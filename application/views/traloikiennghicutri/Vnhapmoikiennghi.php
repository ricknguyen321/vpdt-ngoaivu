<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới kiến nghị
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">HĐND TPHN Khóa *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin)?$thongtin[0]['sKhoa']:''}" name="khoa">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nhiệm kỳ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{($thongtin)?$thongtin[0]['sNhiemKy']:''}" required id="" value="" name="nhiemky">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Kỳ họp thứ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin)?$thongtin[0]['sKyHop']:''}" name="kyhop">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="padding-bottom: 5px;">
                                    <div class="col-md-12">
                                        <input type="radio" name="stcchutri" value="1" required {($thongtin && isset($thongtin[0]['iTrangThai']))?($thongtin[0]['iTrangThai']==1)?'selected':'':''}> <label style="color:red; padding-right:85px;" for="" class="control-label">SNV Chủ trì</label>
                                        <input type="radio" name="stcchutri" value="2" {($thongtin && isset($thongtin[0]['iTrangThai']))?($thongtin[0]['iTrangThai']==2)?'selected':'':''}> <label style="color:red;" for="" class="control-label">SNV Phối hợp</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Câu hỏi *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin)?$thongtin[0]['sCauHoi']:''}" name="cauhoi">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Cử tri *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin && isset($thongtin[0]['sTenCuTri']))?$thongtin[0]['sTenCuTri']:''}" name="cutri">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nội dung câu hỏi *</label>
                                        <textarea name="noidung" id="" required class="form-control" rows="4">{($thongtin && isset($thongtin[0]['sNoiDung']))?$thongtin[0]['sNoiDung']:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực cha*</label>

                                    <div class="col-sm-8">
                                        <select name="linhvuccha" required="" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lĩnh vực cha --</option>
                                            {if !empty($linhvuc)}
                                                {foreach $linhvuc as $lv}
                                                    <option value="{$lv.PK_iMaLV}" {($thongtin && isset($thongtin[0]['FK_iMaLV_Cha']))?($thongtin[0]['FK_iMaLV_Cha']==$lv.PK_iMaLV)?'selected':'':''}>{$lv.sTenLV}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực*</label>

                                    <div class="col-sm-8">
                                        <select name="linhvuccon" required="" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lĩnh vực --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lãnh đạo sở*</label>

                                    <div class="col-sm-8">
                                        <select name="lanhdao" required="" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lãnh đạo --</option>
                                            {if !empty($lanhdao)}
                                                {foreach $lanhdao as $ld}
                                                    <option value="{$ld.PK_iMaCB}" {($thongtin && isset($thongtin[0]['FK_iMaCB_LD']))?($thongtin[0]['FK_iMaCB_LD']==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phòng chủ trì*</label>

                                    <div class="col-sm-8">
                                        <select name="phongchutri" required="" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn phòng chủ trì --</option>
                                            {if !empty($phongban)}
                                                {foreach $phongban as $pb}
                                                    <option value="{$pb.PK_iMaPB}" {($thongtin && isset($thongtin[0]['FK_iMaPhong_CT']))?($thongtin[0]['FK_iMaPhong_CT']==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phòng phối hợp</label>

                                    <div class="col-sm-8">
                                        <select name="phongphoihop[]" id="" class="form-control select2" multiple="" data-placeholder="Chọn phòng phối hợp" style="width: 100%">
                                            {if !empty($phongban)}
                                                {if !empty($thongtin) && isset($thongtin[0]['FK_iMaPhong_PH'])}
                                                    {$phoihop = explode(',',$thongtin[0]['FK_iMaPhong_PH'])}
                                                {/if}
                                                {foreach $phongban as $pb}
                                                    <option value="{$pb.PK_iMaPB}" {if !empty($phoihop) && isset($thongtin[0]['FK_iMaPhong_PH'])}{(in_array($pb.PK_iMaPB,$phoihop))}{/if}>{$pb.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nội dung trả lời</label>
                                        <textarea name="noidungtraloi" id="" required class="form-control textarea" rows="8">{($thongtin && isset($thongtin[0]['sNoiDungTraLoi']))?$thongtin[0]['sNoiDungTraLoi']:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button  type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Thêm mới'}</button> <button onclick="history.go(-1)" class="btn btn-default">Quay lại >></button> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        var url = window.location.href;
        $(document).on('change','select[name=linhvuccha]',function(){
            var macha = $(this).val();
            if(macha.length>0)
            {
                $.ajax({
                    url:url,
                    type:'POST',
                    data:{
                        action:'laylinhvuc',
                        macha:macha
                    },
                    success:function(repon)
                    {
                        var result = JSON.parse(repon);
                        var html='<option value="">-- Chọn lĩnh vực --</option>';;
                        var tong = result.length;
                        if(tong>0)
                        {
                            for(var i=0;i<tong;i++)
                            {
                                html+='<option value="';
                                html+=result[i]['PK_iMaLV'];
                                html+='">';
                                html+=result[i]['sTenLV'];
                                html+='</option>';
                            }
                        }
                        $('select[name=linhvuccon]').html(html);
                    }
                });
            }
        });
    });
</script>

