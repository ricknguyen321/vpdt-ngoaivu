<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách kiến nghị
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a  class="font tin1" name="anhien" href="javascript:void(0);">Tìm kiếm kiến nghị</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal hide anhien" method="get" enctype="multipart/form-data">
                    <div class="row"><br>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">HĐND TPHN Khóa *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($khoa)?$khoa:''}" name="khoa">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nhiệm kỳ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($nhiemky)?$nhiemky:''}" name="nhiemky">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Kỳ họp thứ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($kyhop)?$kyhop:''}" name="kyhop">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="padding-bottom: 5px;">
                                    <div class="col-md-12">
                                        <input type="radio" name="trangthai" value="1"> <label style="color:red; padding-right:85px;" for="" class="control-label">SNV Chủ trì</label>
                                        <input type="radio" name="trangthai" value="2"> <label style="color:red;" for="" class="control-label">SNV Phối hợp</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Câu hỏi *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($cauhoi)?$cauhoi:''}" name="cauhoi">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Cử tri *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($cutri)?$cutri:''}" name="cutri">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nội dung câu hỏi *</label>
                                        <textarea name="noidungcauhoi" id="" class="form-control" rows="2">{($noidungcauhoi)?$noidungcauhoi:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực cha*</label>

                                    <div class="col-sm-8">
                                        <select name="linhvuccha" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lĩnh vực cha --</option>
                                            {if !empty($linhvuc)}
                                                {foreach $linhvuc as $lv}
                                                    <option value="{$lv.PK_iMaLV}" {($linhvuccha)?($linhvuccha==$lv.PK_iMaLV)?'selected':'':''}>{$lv.sTenLV}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực*</label>

                                    <div class="col-sm-8">
                                        <select name="linhvuccon"="" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lĩnh vực --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lãnh đạo sở*</label>

                                    <div class="col-sm-8">
                                        <select name="lanhdao"="" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lãnh đạo --</option>
                                            {if !empty($lanhdao)}
                                                {foreach $lanhdao as $ld}
                                                    <option value="{$ld.PK_iMaCB}" {($lanhdaoo)?($lanhdaoo==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phòng chủ trì*</label>

                                    <div class="col-sm-8">
                                        <select name="phongchutri"="" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn phòng chủ trì --</option>
                                            {if !empty($phongban)}
                                                {foreach $phongban as $pb}
                                                    <option value="{$pb.PK_iMaPB}" {($phongchutri)?($phongchutri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button  type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </div><br>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="5%" class="text-center">STT</th>
                                    <th colspan="2" class="text-center">UBND THÀNH PHỐ PHÂN CÔNG</th>
                                    <th colspan="4" class="text-center">Sở Ngoại Vụ PHÂN CÔNG</th>
                                </tr>
                                <tr>
                                    <!-- <th width="5%" class="text-center">STT</th> -->
                                    <th width="40%">Nội dung</th>
                                    <th width="10%" class="text-center">Trạng thái</th>
                                    <th width="10%" class="text-center">Phòng chủ trì</th>
                                    <th width="14%" class="text-center">phòng phối hợp</th>
                                    <th width="16%" class="text-center">Lãnh đạo sở chỉ đạo</th>
                                    <th width="5%" class="text-center hide">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($linhvuc)}{$ilv=1}
                                    {foreach $linhvuc as $lv}
                                        <tr>
                                            <td></td>
                                            <th>{intToRoman($ilv++)}. {$lv.sTenLV}</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        {if !empty($linhvuccon)}{$ilvc=1}
                                            {foreach $linhvuccon as $lvc}
                                                {if $lvc.FK_iMaLV_Cha==$lv.PK_iMaLV}
                                                <tr>
                                                    <td></td>
                                                    <th>{$ilvc++}. {$lvc.sTenLV}</th>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                {if !empty($dulieu)}{$dem=1}
                                                    {foreach $dulieu as $dl}
                                                        {if $lvc.PK_iMaLV==$dl.FK_iMaLV_Con}
                                                        <tr>
                                                            <td class="text-center">{$dem++}</td>
                                                            <td><a href="{$url}traloikiennghi/{$dl.PK_iMaKienNghi}" target="_blank" class="tin1">{$dl.sNoiDung}</a></td>
                                                            <td class="text-center"><label for="" class="label label-{if $dl.iTrangThai==1}success{else}info{/if}">SNV {if $dl.iTrangThai==1}chủ trì{else}phối hợp{/if}</label></td>
                                                            <td>{if isset($mangphong[$dl.FK_iMaPhong_CT])}{$mangphong[$dl.FK_iMaPhong_CT]}{/if}</td>
                                                            <td> 
                                                            {if !empty($dl.FK_iMaPhong_PH)}
                                                                <!-- {$dem=0} -->
                                                                {$phoihop = explode(',',$dl.FK_iMaPhong_PH)}
                                                                {foreach $phoihop as $ph}
                                                                <!-- {$dem++} -->
                                                                    {($dem>1)?', ':''}{$mangphong[$ph]}
                                                                {/foreach}
                                                            {/if}
                                                            </td>
                                                            <td>{if isset($manglanhdao[$dl.FK_iMaCB_LD])}{$manglanhdao[$dl.FK_iMaCB_LD]}{/if}</td>
                                                        </tr>
                                                        
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right"></div>
                </div>
                <div class="col-md-12">
                    <!-- Modal -->
                    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document" style="width: 50%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Câu trả lời kiến nghị cử tri</h4>
                                </div>
                                <div class="modal-body">
                                   <textarea name="" id="" class="form-control"  rows="10">{$thongtin[0]['sNoiDungTraLoi']}</textarea> 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
        var url = window.location.href;
        $(document).on('change','select[name=linhvuccha]',function(){
            var macha = $(this).val();
            if(macha.length>0)
            {
                $.ajax({
                    url:url,
                    type:'POST',
                    data:{
                        action:'laylinhvuc',
                        macha:macha
                    },
                    success:function(repon)
                    {
                        var result = JSON.parse(repon);
                        var html='<option value="">-- Chọn lĩnh vực --</option>';;
                        var tong = result.length;
                        if(tong>0)
                        {
                            for(var i=0;i<tong;i++)
                            {
                                html+='<option value="';
                                html+=result[i]['PK_iMaLV'];
                                html+='">';
                                html+=result[i]['sTenLV'];
                                html+='</option>';
                            }
                        }
                        $('select[name=linhvuccon]').html(html);
                    }
                });
            }
        });
    });
</script>