<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản đã xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="8%" class="text-center">Ngày nhập</th>
                                    <th width="30%">Trích yếu - Thông tin</th>
                                    <th width="22%">Ý kiến</th>
                                    <th width="30%">Chỉ đạo</th>
                                    <th width="5%" class="text-center"><button type="submit" name="duyet" value="duyet" class="btn btn-success btn-xs">Duyệt</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{date_select($dl.sNgayNhap)}</td>
                                        <td>
                                            <p><b><a href="{$url}vanbanchoxulygq?ma={$dl.PK_iMaVB}" class="tin1">{$dl.sMoTa}</a></b></p>
                                            {if $dl.iGiayMoi==1}
                                                {if !empty($dl.noidungvanban)}<p><i>({$dl.noidungvanban})</i></p>{/if}
                                            {else}
                                                {if $dl.sNgayMoi > '2017-01-01' }<p><i>(Vào hồi {$dl.sGioMoi} ngày {date_select($dl.sNgayMoi)}, tại {$dl.sDiaDiem})</i></p>{/if}
                                            {/if}
                                            <p>- Số ký hiệu: {$dl.sKyHieu}</p>
                                            <p>- Số đến: <span style="color:red;font-weight: bold;font-size: 16px;">{$dl.iSoDen}</span></p>
                                            <p>- Nơi gửi: {$dl.sNoiGuiDen}</p>
                                            {if !empty($dl.sDuongDan)}
                                            <p>
                                                <a href="{$url}{$dl.sDuongDan}" class="tin1">Xem file</a>
                                            </p>
                                            {/if}
                                        </td>
                                        <td>
                                            <p>
                                                <select name="lanhdaopho_{$dl.PK_iMaVB}" class="form-control lanhdaopho select2" style="width: 100%" id="{$dl.PK_iMaVB}">
                                                    {if !empty($lanhdaopho)}
                                                        <option value="0">-- Chọn {$lanhdaopho[0]['tendinhdanh']} --</option>
                                                        {foreach $lanhdaopho as $ldp}
                                                            <option value="{$ldp.PK_iMaCB}" {($dl.FK_iMaCB_LanhDao_Pho==$ldp.PK_iMaCB)?'selected':''}>{$ldp.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p> 
                                            <p>
                                                <select name="phongchutri_{$dl.PK_iMaVB}" class="form-control phongchutri select2" style="width: 100%" id="{$dl.PK_iMaVB}">
                                                    <option value="0">-- Chọn phòng chủ trì --</option>
                                                    {if !empty($phongban)}
                                                        {foreach $phongban as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {($dl.FK_iMaPhong_CT==$pb.PK_iMaPB)?'selected':''}>{$pb.sTenPhong}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p> 
                                            <p>
                                                <input type="text" name="phongph_{$dl.PK_iMaVB}" value="{$dl.FK_iMaPhong_PH}"  class="hide">
                                                <button type="button" {if empty($dl.FK_iMaPhong_CT)}disabled{/if} name="phoihop" value="{$dl.PK_iMaVB}" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-sm phongph_{$dl.PK_iMaVB}">Chọn phối hợp</button>
                                            </p>
                                            <p>
                                                <input type="text" name="hanxuly_{$dl.PK_iMaVB}" class="form-control datepic datemask" placeholder="hạn xử lý" value="{if $dl.sHanGiaiQuyet > '1970-01-01'}{date_select($dl.sHanGiaiQuyet)}{/if}">
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <input type="text" class="form-control" value="{$dl.sGoiY_LanhDao_Pho}" name="tenlanhdaopho_{$dl.PK_iMaVB}" placeholder="Nội dung chuyển {if !empty($lanhdaopho)}{$lanhdaopho[0]['tendinhdanh']}{/if}">
                                            </p>
                                            <p>
                                                <input type="text" class="form-control" name="chutri_{$dl.PK_iMaVB}" value="{$dl.sGoiY_Phong_CT}" placeholder="Nội dung chuyển phòng chủ trì">
                                            </p>
                                            <p>
                                                <textarea name="tenphongph_{$dl.PK_iMaVB}" id="" class="form-control" rows="3" placeholder="Nội dung chuyển phòng phối hợp">{$dl.sGoiY_Phong_PH}</textarea>
                                            </p>
                                        </td>
                                        <td class="text-center">
                                            <span style="color:red;">Chọn duyệt</span>
                                            <input type="checkbox" name="mavanban[]" class="mavanban_{$dl.PK_iMaVB}" {if empty($dl.FK_iMaPhong_CT)}disabled{/if} value="{$dl.PK_iMaVB}">
                                        </td>
                                    </tr>
                                {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right"></div>
                </div>
                <div class="col-md-12">
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Phòng ban phối hợp</h4>
                            </div>
                            <div>
                            {if !empty($phongban)}
                                {foreach $phongban as $ph}
                                    <div class="col-md-6">
                                        <input type="checkbox" class="Checkbox" data-id="{$ph.sTenPhong}" name="phongphoihop" value="{$ph.PK_iMaPB}"> {$ph.sTenPhong}
                                    </div>
                                {/foreach}
                            {/if}
                            </div>
                            <div class="modal-footer">
                                <input type="text" name="ma" class="hide">
                                <button type="button" name="ghilai" class="btn btn-primary btn-sm" data-dismiss="modal">Ghi Lại</button>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change','.lanhdaopho',function(){
            var mavanban = $(this).attr('id');
            var lanhdaopho = $('select[name=lanhdaopho_'+mavanban+']').val();
            var phongchutri = $('select[name=phongchutri_'+mavanban+']').val();
            var tenlanhdaopho = $(this).find("option:selected").text();
            if(lanhdaopho>0)
            {
                $('input[name=tenlanhdaopho_'+mavanban+']').val('Chuyển đ/c '+tenlanhdaopho+' chủ trì giải quyết');
            }
            else{
                $('input[name=tenlanhdaopho_'+mavanban+']').val('');
            }
            if(lanhdaopho==0 && phongchutri==0)
            {
                $('.mavanban_'+mavanban+'').attr('checked', false); 
                $('.mavanban_'+mavanban+'').attr("disabled", 'disabled');
            }
            else{
                $('.mavanban_'+mavanban+'').removeAttr('disabled');
            }

        });
        $(document).on('click','button[name=phoihop]',function(){
            var mavanban = $(this).val();
            $('input[name=ma]').val(mavanban);
        });
        $(document).on('change','.phongchutri',function(){
            var mavanban = $(this).attr('id');
            var maphong  = $(this).val();
            var tenphong = $(this).find("option:selected").text();
            if(maphong>0)
            {
                $('input[name=chutri_'+mavanban+']').val(tenphong+' chủ trì giải quyết.');
                $('.phongph_'+mavanban+'').removeAttr('disabled');
            }
            else{
                $('input[name=phongph_'+mavanban+']').val('');
                $('input[name=chutri_'+mavanban+']').val('');
                $('textarea[name=tenphongph_'+mavanban+']').val('');
                $('.phongph_'+mavanban+'').attr("disabled", 'disabled');
            }
            var lanhdaopho = $('select[name=lanhdaopho_'+mavanban+']').val();
            var phongchutri = $('select[name=phongchutri_'+mavanban+']').val();

            if(lanhdaopho==0 && phongchutri==0)
            {
                $('.mavanban_'+mavanban+'').attr("disabled", 'disabled');
                $('.mavanban_'+mavanban+'').attr('checked', false); 
            }
            else{
                $('.mavanban_'+mavanban+'').removeAttr('disabled');
            }
        });
        $(document).on('click','button[name=ghilai]',function () {
            var tenpb = $('.Checkbox:checked').map(function() {
                return $(this).attr('data-id');
            }).get().join(',');
            var mapb = $('.Checkbox:checked').map(function() {
                return this.value;
            }).get().join(',');
            var mavanban = $('input[name=ma]').val();
            $('input[name=phongph_'+mavanban+']').val(mapb);
            if(mavanban.length>0)
            {
                $('textarea[name=tenphongph_'+mavanban+']').val(' '+tenpb+' phối hợp giải quyết');
            }
            else{
                $('input[name=phongph_'+mavanban+']').val('');
                $('textarea[name=tenphongph_'+mavanban+']').val('');
            }
            $('.Checkbox').attr('checked', false); 
        });
    });
</script>