<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<!--    <section class="content-header">-->
<!--      <h1>-->
<!--        Nhập mới giấy mời-->
<!--      </h1>-->
<!--    </section>-->

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-12"><br>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu *</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="sokyhieu" value="{$thongtin[0]['sKyHieu']}" class="form-control" id="" required placeholder="Nhập số hiệu">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input  name="noiguiden" type="text" class="noiguiden form-control" value="{$thongtin[0]['sNoiGuiDen']}" placeholder="Nhập nơi gửi đến">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngayky" value="{date_select($thongtin[0]['sNgayKy'])}" class="form-control datepic datemask" id="" required placeholder="Nhập ngày ký">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group" style="margin-top: -15px;">
                                        <label for="" class="control-label">Trích yếu *</label>
                                        <textarea  name="noidung" required id="" class="form-control" rows="3" placeholder="Nhập trích yêu">{$thongtin[0]['sMoTa']}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Giờ họp *</label>

                                    <div class="col-sm-8 bootstrap-timepicker">
                                        <input type="text" name="giohopgm" class="form-control" value="{$thongtin[0]['sGioMoi']}" placeholder=" Ví dụ: {date('h:i',time())}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Địa điểm *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="diadiem" value="{$thongtin[0]['sDiaDiem']}" required class="form-control" id="" placeholder="Nhập địa điểm">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký *</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input type="text" required name="nguoiky" class="nguoiky form-control" value="{$thongtin[0]['sNguoiKy']}" placeholder="Nhập người ký">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày họp *</label>

                                    <div class="col-sm-8">
                                            <input type="text" name="ngayhopgm" value="{date_select($thongtin[0]['sNgayMoi'])}" class="form-control datepic datemask" id="" required placeholder="Nhập ngày họp">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người chủ trì</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="nguoichutri" value="{$thongtin[0]['sNguoiChuTri']}" class="form-control" id="" placeholder="Người chủ trì">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="chucvu" value="{$thongtin[0]['sChucVu']}" class="form-control" id="" placeholder="Nhập chức vụ">
                                    </div>
                                </div>
                            </div>
<!--                            <hr style="border: 1px dashed blue">-->


                            <div class="col-sm-12" style="  margin: -25px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung họp *</label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung ">{$thongtin[0]['sNoiDung']}</textarea>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><br>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Ngày nhận</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="ngaynhan" value="{date_select($thongtin[0]['sNgayNhan'])}" class="form-control datepic datemask" id="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Số đến</label>

                                            <div class="col-sm-8">
                                                <input type="number" name="soden" class="form-control" value="{$thongtin[0]['iSoDen']}" id="soden" style="color: red; font-weight: 600; font-size: 18px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Số trang</label>

                                            <div class="col-sm-8">
                                                <input type="number" name="sotrang" value="{$thongtin[0]['iSoTrang']}" class="form-control" id="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" name="luulai" value="luulai" class="btn btn-primary">Cập nhật</button> <label for=""> Người nhập văn bản này là: {$dscanbo[$thongtin[0]['FK_iMaCB']]}</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
    var data = {
        "noiden": [
            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                "{$dv.sTenDV}",
            {/foreach}
        ],
        "nguoiky": [
            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                "{$nk.sHoTen}",
            {/foreach}
        ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".noiguiden",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>