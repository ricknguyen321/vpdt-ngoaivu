<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px;padding-left: 15px; ">
        <div class="row" style="    margin-top: -10px;">
            <div class="col-sm-3">
                <h1 style="font-size: 13px; font-weight: 600">Nhập mới văn bản</h1>
            </div>

            <div class="col-sm-3">
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content" style="padding-top: 0px">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {if isset($content)}
                <div class="alert alert-warning alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <label for="" style="color:black;">Thêm thành công số đến là: <b style="font-size: 20px;color:red;" id="thongbaoden"></b></label>
                </div>
                {/if}
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-3"><h5><label for="">Là VB QPPL:</label> <input type="checkbox" {if $thongtin[0]['iVanBanQPPL']==2}checked{/if} name="iVanBanQPPL" value="2"></h5></div>
                            <div class="col-md-3"><h5><label for="">SNV chủ trì:</label> <input type="checkbox" {if $thongtin[0]['iSTCChuTri']==2}checked{/if} name="iSTCChuTri" value="2"></h5></div>
                            <div class="col-md-3"><h5><label for="">TBKL:</label> <input type="checkbox" {if $thongtin[0]['iVanBanTBKL']==2}checked{/if} name="iVanBanTBKL" value="2"></h5></div>
                            <div class="col-md-3"><h5><label for="">Văn bản mật:</label> <input type="checkbox" {if $thongtin[0]['iVanBanMat']==2}checked{/if} name="iVanBanMat" value="2"></h5></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực:</label>

                                    <div class="col-sm-8">
                                        <select name="khuvuc" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn cấp ban hành --</option>
                                            {foreach layDuLieu(NULL,NULL,'tbl_khuvuc') as $kv}
                                            <option value="{$kv.sTenKV}" {if $thongtin[0]['sTenKV']==$kv.sTenKV}selected{/if}>{$kv.sTenKV}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" required class="form-control" name="sokyhieu" value="{$thongtin[0]['sKyHieu']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" class="form-control select2" style="width: 100%" id="">
                                            <option value="">-- Chọn loại văn bản --</option>
                                            {if !empty($loaivanban)}
                                                {foreach $loaivanban as $lvb}
                                                    <option value="{$lvb.sTenLVB}" {($thongtin)?($thongtin[0]['sTenLVB']==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input required="" type="text" class="donvi form-control" value="{$thongtin[0]['sNoiGuiDen']}" name="donvi" placeholder="Nhập nơi gửi đến">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control datemask datepic" id="" value="{date_select($thongtin[0]['sNgayKy'])}" name="ngayky" placeholder="Ngày ký" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input type="text" class="linhvuc form-control" value="{$thongtin[0]['sTenLV']}" name="linhvuc" placeholder="Nhập lĩnh vực">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class=" col-md-12">
                                    <div class="form-group" style="margin-top: -16px;">
                                        <label for="" class="control-label">Trích yếu <span class="text-danger">*</span></label>
                                        <textarea  name="trichyeu" id="trichyeu" class="form-control ngayhan" rows="2" required placeholder="Nhập trích yếu">{$thongtin[0]['sMoTa']}</textarea>
                                        <p id="baoloi"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="    margin: -30px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung *</label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung">{$thongtin[0]['sNoiDung']}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="    margin: -25px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input type="text" name="nguoiky" class="nguoiky form-control" value="{$thongtin[0]['sNguoiKy']}">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày nhận:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{date_select($thongtin[0]['sNgayNhan'])}" name="ngaynhan" class="form-control ngayhan datepic datemask" placeholder="Nhập ngày nhận">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số đến:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{$thongtin[0]['iSoDen']}" name="soden" id="soden" class="form-control" placeholder="Nhập số đến" style="font-size: 16px; color: red; font-weight: 600">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="chucvu" value="{$thongtin[0]['sChucVu']}" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="hangiaiquyet" value="{($thongtin)?($thongtin[0]['sHanGiaiQuyet']>'2017-01-01')?date_select($thongtin[0]['sHanGiaiQuyet']):'':''}" class="form-control datepic datemask" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{$thongtin[0]['iSoTrang']}" name="sotrang" class="form-control col-sm-4" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button  type="submit" class="btn btn-primary btn-sm" name="luulai" value="luulai">Cập nhật</button> <label for="">Người nhập văn bản này là:{$dscanbo[$thongtin[0]['FK_iMaCB']]}</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
    var data = {
        "noiden": [
            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                "{$dv.sTenDV}",
            {/foreach}
        ],
        "linhvuc": [
            {foreach layDuLieu(NULL,NULL,'tbl_linhvuc') as $lv}
                "{$lv.sTenLV}",
            {/foreach}
        ],
        "nguoiky": [
            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                "{$nk.sHoTen}",
            {/foreach}
        ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".linhvuc",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            linhvuc: {
                data: data.linhvuc
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".donvi",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>
