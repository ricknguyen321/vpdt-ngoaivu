<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản đã xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="8%" class="text-center">Ngày nhập</th>
                                    <th width="30%">Trích yếu - Thông tin</th>
                                    <th width="25%">Ý kiến</th>
                                    <th width="25%">Quy trình</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{date_select($dl.sNgayNhap)}</td>
                                        <td>
                                            <p><b><a href="{$url}vanbanchoxulygq?ma={$dl.PK_iMaVB}" class="tin1">{$dl.sMoTa}</a></b></p>
                                            {if $dl.iGiayMoi==1}
                                                {if !empty($dl.noidungvanban)}<p><i>({$dl.noidungvanban})</i></p>{/if}
                                            {else}
                                                {if $dl.sNgayMoi > '2017-01-01' }<p><i>(Vào hồi {$dl.sGioMoi} ngày {date_select($dl.sNgayMoi)}, tại {$dl.sDiaDiem})</i></p>{/if}
                                            {/if}
                                            <p>- Số ký hiệu: {$dl.sKyHieu}</p>
                                            <p>- Số đến: <span style="color:red;font-weight: bold;font-size: 16px;">{$dl.iSoDen}</span></p>
                                            <p>- Nơi gửi: {$dl.sNoiGuiDen}</p>
                                            {if !empty($dl.sDuongDan)}
                                            <p>
                                                <a href="{$url}{$dl.sDuongDan}" class="tin1">Xem file</a>
                                            </p>
                                            {/if}
                                            
                                        </td>
                                        <td>
                                            <p><span style="color:red;">*</span> <i>Nội dung chỉ đạo của đ/c </i><b>{$dscanbo[$dl.FK_iMaCB_Gui]}:</b></p>
                                            <p><i>{$dl.sNoiDung}</i></p>
                                        </td>
                                        <td>
                                            {if !empty($dl.chuyennhan)}
                                                {foreach $dl.chuyennhan as $cn}
                                                    <p>{$dscanbo[$cn.FK_iMaCB_Nhan]}</p>
                                                    <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                                {/foreach}
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right"></div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>