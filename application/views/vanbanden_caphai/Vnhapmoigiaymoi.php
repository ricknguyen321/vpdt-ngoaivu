<script src="{$url}assets/js/vanbanden/vanbanden.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<!--    <section class="content-header">-->
<!--      <h1>-->
<!--        Nhập mới giấy mời-->
<!--      </h1>-->
<!--    </section>-->

    <!-- Main content -->
    <section class="content">
        <div class="col-sm-12 hidden">
            <div class="col-md-8 noidung">
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Nội dung họp *</label>

                    <div class="col-sm-9">
                        <textarea name="noidunghop[]" class="form-control" rows="3" style="border: 1px solid red;"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Địa điểm họp</label>

                    <div class="col-sm-9">
                        <input type="text" name="diadiemhop[]" value="" class="form-control" style="border: 1px solid red;">
                    </div>
                </div>
            </div>
            <div class="col-md-4 noidung">
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label" required>Giờ họp</label>

                    <div class="col-sm-8">
                        <input type="text" name="giohop[]" class="form-control" value="" placeholder="Ví dụ: {date('h:i',time())}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Ngày họp</label>

                    <div class="col-sm-8">
                        <input type="text" name="ngayhop[]" value="" class="form-control datepic datemask" id="" placeholder="Ví dụ: {date('d/m/Y',time())}">
                    </div>
                </div>
            </div>
        </div>
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-12"><br>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu *</label>
                                    <div class="col-sm-8">
                                        <input tabindex="1" type="text" name="sokyhieu" value="" class="form-control" id="" required placeholder="Nhập số hiệu">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="2" name="noiguiden" tabindex="" type="text" class="noiguiden form-control" value="" placeholder="Nhập nơi gửi đến">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký *</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" name="ngayky" class="form-control datepic datemask" id="" required placeholder="Nhập ngày ký">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group" style="margin-top: -15px;">
                                        <label for="" class="control-label">Trích yếu *</label>
                                        <textarea tabindex="4" name="noidung" required id="" class="form-control" rows="3" placeholder="Nhập trích yêu"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Giờ họp *</label>

                                    <div class="col-sm-8 bootstrap-timepicker">
                                        <input tabindex="5" type="text" name="giohopgm" class="form-control" value="" placeholder=" Ví dụ: {date('h:i',time())}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Địa điểm *</label>

                                    <div class="col-sm-8">
                                        <input tabindex="7" type="text" name="diadiem" value="" required class="form-control" id="" placeholder="Nhập địa điểm">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký *</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="9" type="text" required name="nguoiky" class="nguoiky form-control" value="" placeholder="Nhập người ký">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày họp *</label>

                                    <div class="col-sm-8">
                                            <input tabindex="6" type="text" name="ngayhopgm" value="" class="form-control datepic datemask" id="" required placeholder="Nhập ngày họp">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người chủ trì</label>

                                    <div class="col-sm-8">
                                        <input tabindex="8" type="text" name="nguoichutri" class="form-control" id="" placeholder="Người chủ trì">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="chucvu" class="form-control" id="" placeholder="Nhập chức vụ">
                                    </div>
                                </div>
                            </div>
<!--                            <hr style="border: 1px dashed blue">-->


                            <div class="col-sm-12" style="  margin: -25px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung họp *</label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop[]" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung "></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Địa điểm họp</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="diadiemhop[]" class="form-control" value="" style="border: 1px solid red;" placeholder="Nhập địa điểm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" required>Giờ họp</label>

                                    <div class="col-sm-8 bootstrap-timepicker">
                                        <input type="text" name="giohop[]" class="form-control" value="" id="" placeholder="Ví dụ: {date('h:i',time())}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày họp</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngayhop[]" class="form-control datepic datemask" value="" id="" placeholder="Ví dụ: {date('d/m/Y',time())}">
                                    </div>
                                </div>
                            </div>
                            <div class="themsau"></div>
                            <div class="col-md-12 text-right">
                                <div class="col-sm-8">
                                    <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><br>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Ngày nhận</label>
                                            <div class="col-sm-8">
                                                <input tabindex="10" type="text" name="ngaynhan" value="{date('d/m/Y',time())}" class="form-control datepic datemask" id="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Số đến</label>

                                            <div class="col-sm-8">
                                                <input type="number" name="soden" class="form-control" value="{$soden}" id="soden" style="color: red; font-weight: 600; font-size: 18px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Số trang</label>

                                            <div class="col-sm-8">
                                                <input type="number" name="sotrang" value="1" class="form-control" id="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button tabindex="11" type="submit" name="luulai" value="Thêm mới" class="btn btn-primary">Thêm mới</button> <button class="btn btn-default">Quay lại >></button> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung2 = $('.noidung')[1].outerHTML;
            var noidung = noidung1+noidung2;
            $('.themsau').before(noidung);
            $(".timepicker").timepicker({
                showInputs: false
            });
            $('.datepic').datepicker({
              autoclose: true
            });
            //Datemask dd/mm/yyyy
            $(".datemask").inputmask("dd/mm/yyyy");
            });
    });
</script>
<script>
    var data = {
        "noiden": [
            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                "{$dv.sTenDV}",
            {/foreach}
        ],
        "nguoiky": [
            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                "{$nk.sHoTen}",
            {/foreach}
        ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".noiguiden",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>