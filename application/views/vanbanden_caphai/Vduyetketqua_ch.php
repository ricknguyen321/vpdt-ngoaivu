<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản chờ xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="30%">Trích yếu - Thông tin</th>
                                    <th width="25%">Kết quả hoàn thành</th>
                                    <th width="25%">Quy trình</th>
                                    <th width="8%" class="text-center">
                                    	<button type="submit" name="duyet" value="duyet" class="btn btn-success btn-xs">Duyệt</button><br>
                                    	<input type="checkbox" name="duyetall">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td>
                                            <p><b><a href="{$url}vanbanchoxulygq?ma={$dl.PK_iMaVB}" class="tin1">{$dl.sMoTa}</a></b></p>
                                            {if $dl.iGiayMoi==1}
                                                {if !empty($dl.noidungvanban)}<p><i>({$dl.noidungvanban})</i></p>{/if}
                                            {else}
                                                {if $dl.sNgayMoi > '2017-01-01' }<p><i>(Vào hồi {$dl.sGioMoi} ngày {date_select($dl.sNgayMoi)}, tại {$dl.sDiaDiem})</i></p>{/if}
                                            {/if}
                                            <p>- Số ký hiệu: {$dl.sKyHieu}</p>
                                            <p>- Số đến: <span style="color:red;font-weight: bold;font-size: 16px;">{$dl.iSoDen}</span></p>
                                            <p>- Nơi gửi: {$dl.sNoiGuiDen}</p>
                                            {if !empty($dl.sDuongDan)}
                                            <p>
                                                <a href="{$url}{$dl.sDuongDan}" class="tin1">Xem file</a>
                                            </p>
                                            {/if}
                                            {if !empty($dl.sHan_ThongKe)}
                                            	<p>- Hạn văn bản: {date_select($dl.sHan_ThongKe)}</p>
                                            {/if}
                                            
                                        </td>
                                        <td>
                                            <p><span style="color:red;">*</span> <i>đ/c hoàn thành: </i><b>{$dscanbo[$dl.FK_iMaCB]}</b></p>
                                            <p><b>Nội dung: </b><i>{$dl.sNoiDung}</i></p>
                                            {if !empty($dl.duongdan)}
                                            <p><b>File kết quả: </b><a target="_blank" class="tin1" href="{$url}{$dl.duongdan}">[Xem File]</a></p>
                                            {/if}
                                            <p><b>Thời gian: </b><i>{date_time($dl.sThoiGian)}</i></p>
                                        </td>
                                        <td>
                                            {if !empty($dl.chuyennhan)}
                                                {foreach $dl.chuyennhan as $cn}
                                                    <p>{$dscanbo[$cn.FK_iMaCB_Nhan]}</p>
                                                    <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                                {/foreach}
                                            {/if}
                                        </td>
                                        <td class="text-center">
                                        	<span style="color:red;">Chọn duyệt</span>
                                            <input type="checkbox" name="mavanban[]" class="mavanban_{$dl.PK_iMaVB}" value="{$dl.PK_iMaVB}">
                                        </td>
                                    </tr>
                                {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right"></div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
	$(document).ready(function() {
		$(document).on('change','input[name=duyetall]',function(){
			if ($('input[name=duyetall]').is(':checked')){
                $("input[type=checkbox]").prop( "checked", true );
            } else {
                $("input[type=checkbox]").prop( "checked", false );
            }
		});
	});
</script>