<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách văn bản chủ trì
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {if !empty($dsloaivanban)}
                                                                {foreach $dsloaivanban as $lvb}
                                                                    <option value="{$lvb.PK_iMaLVB}">{$lvb.sTenLVB}</option>
                                                                {/foreach}
                                                            {/if}
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaynhap" class="form-control datepic datemask" value="" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {if !empty($dsnoiguiden)}
                                                                {foreach $dsnoiguiden as $ng}
                                                                    <option value="{$ng.PK_iMaDV}">{$ng.sTenDV}</option>
                                                                {/foreach}
                                                            {/if}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayden" value="" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class=" col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Trích yếu</label>
                                                        <textarea name="trichyeu" id="" class="form-control" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaymoi" class="form-control datepic datemask" id="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Người ký</label>

                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số đến</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="denngay" class="form-control datepic datemask" id="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-12 deletepadding">
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="8%" class="text-center">Ngày nhập</th>
                                    <th width="8%" class="text-center">Loại văn bản</th>
                                    <th width="8%" class="text-center">Số đến</th>
                                    <th width="8%" class="text-center">Số ký hiệu</th>
                                    <th width="12%" class="text-center">Nơi gửi</th>
                                    <th width="30%" class="text-center">Trích yếu</th>
                                    <th width="" class="text-center">Quy trình</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dulieu)}{$i=1}
                                {foreach $dulieu as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td>{date_select($dl.sNgayNhap)}</td>
                                        <td class="text-center">{$dl.sTenLVB}</td>
                                        <td style="color: red; font-size: 16px" class="text-center"><b>{$dl.iSoDen}</b></td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td class="text-center">{$dl.sNoiGuiDen}</td>
                                        <td><a href="{$url}vanbanchoxulygq?ma={$dl.PK_iMaVB}" style="font-weight: bold;color: black !important;" class="tin1">{$dl.sMoTa}</a>
                                            {if $dl.iGiayMoi==1}
                                                {if !empty($dl.sNoiDung)} <p><i>({$dl.sNoiDung})</i></p>{/if}
                                            {else}
                                                <p><i>(Họp lúc {$dl.sGioMoi} ngày {date_select($dl.sNgayMoi)} tại {$dl.sDiaDiem})</i></p>
                                            {/if}
                                            <p>{if !empty($dl.sDuongDan)} <a href="{$url}{$dl.sDuongDan}" target="_blank" class="tin1">Xem File</a>{/if}</p>
                                        </td>
                                        <td>
                                            {if !empty($dl.chuyennhan)}
                                            {$j=1}
                                                {foreach $dl.chuyennhan as $cn}
                                                    <p>{$j++}. {$dscanbo[$cn.FK_iMaCB_Nhan]}</p>
                                                    <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                                {/foreach}
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-12">
                    {$phantrang}
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
