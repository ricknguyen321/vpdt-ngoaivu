<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px;padding-left: 15px; ">
        <div class="row" style="    margin-top: -10px;">
            <div class="col-sm-3">
                <h1 style="font-size: 13px; font-weight: 600">Nhập mới văn bản</h1>
            </div>

            <div class="col-sm-3">
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content" style="padding-top: 0px">
        <div class="col-sm-12 hidden">
            <div class="noidung">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Nội dung *</label>

                        <div class="col-sm-9">
                            <textarea name="noidunghop[]" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="col-sm-4 control-label">Hạn giải quyết </label>

                        <div class="col-sm-8">
                            <input tabindex="8" type="text" value="" name="hannoidung[]" class="form-control datepic datemask" placeholder="Hạn giải quyết của nội dung">
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        //Date picker
                        $('.datepic').datepicker({
                            autoclose: true
                        });
                        //Datemask dd/mm/yyyy
                        $(".datemask").inputmask("dd/mm/yyyy");
                    });
                </script>
            </div>
        </div>

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {if isset($content)}
                <div class="alert alert-warning alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <label for="" style="color:black;">Thêm thành công số đến là: <b style="font-size: 20px;color:red;" id="thongbaoden"></b></label>
                </div>
                {/if}
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                                    <div class="col-md-3"><h5><label for="">Là VB QPPL:</label> <input type="checkbox" name="iVanBanQPPL" value="2"></h5></div>
                                    <div class="col-md-3"><h5><label for="">SNV chủ trì:</label> <input type="checkbox" name="iSTCChuTri" value="2"></h5></div>
                                    <div class="col-md-3"><h5><label for="">TBKL:</label> <input type="checkbox" name="iVanBanTBKL" value="2"></h5></div>
                            <div class="col-md-3"><h5><label for="">Văn bản mật:</label> <input type="checkbox" name="iVanBanMat" value="2"></h5></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực:</label>

                                    <div class="col-sm-8">
                                        <select name="khuvuc" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn cấp ban hành --</option>
                                            {foreach layDuLieu(NULL,NULL,'tbl_khuvuc') as $kv}
                                            <option value="{$kv.sTenKV}">{$kv.sTenKV}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="2" required class="form-control" name="sokyhieu" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" class="form-control select2" style="width: 100%" id="">
                                            <option value="">-- Chọn loại văn bản --</option>
                                            {if !empty($loaivanban)}
                                                {foreach $loaivanban as $lvb}
                                                    <option value="{$lvb.sTenLVB}">{$lvb.sTenLVB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="1" required="" type="text" class="donvi form-control" value="" name="donvi" placeholder="Nhập nơi gửi đến">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" class="form-control datemask datepic" id="" name="ngayky" placeholder="Ngày ký" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="5" type="text" class="linhvuc form-control" value="" name="linhvuc" placeholder="Nhập lĩnh vực">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class=" col-md-12">
                                    <div class="form-group" style="margin-top: -16px;">
                                        <label for="" class="control-label">Trích yếu <span class="text-danger">*</span></label>
                                        <textarea tabindex="6" name="trichyeu" id="trichyeu" class="form-control ngayhan" rows="2" required placeholder="Nhập trích yếu"></textarea>
                                        <p id="baoloi"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="    margin: -30px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung *</label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop[]" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết </label>

                                    <div class="col-sm-8">
                                        <input type="text" value="" name="hannoidung[]" class="form-control datepic datemask" placeholder="Hạn giải quyết của nội dung">
                                    </div>
                                </div>
                            </div>
                            <div class="themsau"></div>
                            <div class="col-md-12 text-right">
                                <div class="col-sm-12" style="    margin-top: -40px;">
                                    <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                </div>
                            </div>
                            <div class="col-sm-12" style="    margin: -25px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="7" type="text" required="" name="nguoiky" class="nguoiky form-control" value="">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày nhận:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="9" type="text" value="{date('d/m/Y')}" name="ngaynhan" class="form-control ngayhan datepic datemask" placeholder="Nhập ngày nhận">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số đến:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{$soden}" name="soden" id="soden" class="form-control" placeholder="Nhập số đến" style="font-size: 16px; color: red; font-weight: 600">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="8" class="form-control" name="chucvu" value="" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="hangiaiquyet" value="" class="form-control datepic datemask" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang:</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="10" value="" name="sotrang" class="form-control col-sm-4" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button tabindex="10" type="submit" class="btn btn-primary btn-sm" name="luulai" value="Lưu lại">Lưu lại</button> <label for="">Người nhập văn bản này là:{$vanban['sHoTen'] }</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "noiden": [
            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                "{$dv.sTenDV}",
            {/foreach}
        ],
        "linhvuc": [
            {foreach layDuLieu(NULL,NULL,'tbl_linhvuc') as $lv}
                "{$lv.sTenLV}",
            {/foreach}
        ],
        "nguoiky": [
            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                "{$nk.sHoTen}",
            {/foreach}
        ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".linhvuc",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            linhvuc: {
                data: data.linhvuc
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".donvi",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>
