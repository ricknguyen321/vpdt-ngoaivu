<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Thông tin văn bản
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Khu vực: </label>
                                    <div class="col-sm-8">
                                        <label for="">{$thongtin['sTenLV']}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                    <div class="col-sm-8">
                                        <p>{$thongtin['sNoiGuiDen']}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Ký hiệu: </label>
                                    <div class="col-sm-8">
                                        <label for="">{$thongtin['sKyHieu']}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Ngày ký:</label>
                                    <div class="col-sm-8">
                                        <p>{if $thongtin['sNgayKy'] > '1970-01-01'}{date_select($thongtin['sNgayKy'])}{/if}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Loại văn bản: </label>
                                    <div class="col-sm-8">
                                        <label for="">{$thongtin['sTenLVB']}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Lĩnh vực:</label>
                                    <div class="col-sm-8">
                                        <p>{$thongtin['sTenLV']}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2">Trích yếu:</label>
                                    <div class="col-sm-10">
                                        <p><b>{$thongtin['sMoTa']}</b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Người ký: </label>
                                    <div class="col-sm-8">
                                        <p>{$thongtin['sNguoiKy']}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Ngày nhận:</label>
                                    <div class="col-sm-8">
                                        <p>{if $thongtin['sNgayNhan'] > '1970-01-01'}{date_select($thongtin['sNgayNhan'])}{/if}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                    <div class="col-sm-8">
                                        <p>{$thongtin['sChucVu']}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Hạn giải quyết:</label>
                                    <div class="col-sm-8">
                                        <p>{if $thongtin['sHanGiaiQuyet'] > '1970-01-01'}{date_select($thongtin['sHanGiaiQuyet'])}{/if}</p>
                                    </div>
                                </div>
                            </div>
                            {if $thongtin['iGiayMoi']==2}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Giờ mời: </label>
                                    <div class="col-sm-8">
                                        <label for="">{$thongtin['sGioMoi']}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Ngày Mời:</label>
                                    <div class="col-sm-8">
                                        <p>{if $thongtin['sNgayMoi'] > '1970-01-01'}{date_select($thongtin['sNgayMoi'])}{/if}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Địa điểm:</label>
                                    <div class="col-sm-8">
                                        <p>{$thongtin['sDiaDiem']}</p>
                                    </div>
                                </div>
                            </div>
                            {/if}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">{$thongtin['iSoDen']}</label>
                                    </div>
                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>
                                    <label for="" class="col-sm-1">Độ khẩn:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>
                                </div>
                            </div>
                            {if !empty($trinhtu)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2">Trình tự giải quyết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Ngày nhận</th>
                                            <th class="text-center">Chuyển từ</th>
                                            <th width="40%">Nội dung</th>
                                            <th class="text-center">Chuyển đến</th>
                                            <th class="text-center">Hạn xử lý</th>
                                        </tr>
                                        </thead>
                                        <tbody>{$i=1}
                                            {foreach $trinhtu as $tt}
                                            <tr class="info">
                                                <td class="text-center">{$i++}</td>
                                                <td class="text-center">{date_select($tt.sThoiGian)}</td>
                                                <td class="text-center">{$canbo[$tt.FK_iMaCB_Gui]}</td>
                                                <td>{$tt.sNoiDung}</td>
                                                <td class="text-center">{$canbo[$tt.FK_iMaCB_Nhan]}</td>
                                                <td class="text-center">{if $tt.sHanGiaiQuyet > '1970-01-01'}{date_select($tt.sHanGiaiQuyet)}{/if}</td>
                                            </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2">Tệp tin đính kèm:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="10%" class="text-center">STT</th>
                                            <th>Tên tệp tin</th>
                                            <th class="text-center">Tải về</th>
                                            <th class="text-center">Ngày nhập</th>
                                            <th class="text-center">Người nhập</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {if !empty($file)}{$i=1}
                                            {foreach $file as $f}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td>{($f.sTenFile)?$f.sTenFile:'Chưa đặt tên'}</td>
                                                    <td class="text-center"><a target="_blank" href="{$url}{$f.sDuongDan}" class="tin1">Tải về</a></td>
                                                    <td class="text-center">{date_select($f.sThoiGian)}</td>
                                                    <td>{$canbo[$f.FK_iMaCB]}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {if !empty($filekqchutri)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-10">Kết quả giải quyết - phòng thụ lý:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="10%">STT</th>
                                            <th>Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th>File</th>
                                            <th class="text-center">Ngày nhập</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $filekqchutri as $tl}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td width="20%" class="text-center">{$canbo[$tl.FK_iMaCB]}</td>
                                            <td>{$tl.sNoiDung}</td>
                                            <td width="15%" class="text-center">{if !empty($tl.sDuongDan)} <a href="{$url}{$tl.sDuongDan}">[Xem File]</a>{/if}</td>
                                            <td width="15%" class="text-center">{date_select($tl.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}
                            {if !empty($filekqphoihopct) || !empty($filekqphoihopph)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-10">Kết quả giải quyết - phòng phối hợp:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="10%">STT</th>
                                            <th>Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th width="15%" >File</th>
                                            <th>Ngày nhập</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $filekqphoihopct as $re}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td width="20%" class="text-center">{$canbo[$re.FK_iMaCB]}</td>
                                            <td>{$re.sNoiDung}</td>
                                            <td>{if !empty($re.sDuongDan)} <a href="{$url}{$re.sDuongDan}">[Xem File]</a>{/if}</td>
                                            <td width="15%" class="text-center">{date_select($re.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        {$j=$i}
                                        {foreach $filekqphoihopph as $ree}
                                        <tr >
                                            <td class="text-center">{$j++}</td>
                                            <td width="20%" class="text-center">{$canbo[$ree.FK_iMaCB]}</td>
                                            <td>{$ree.sNoiDung}</td>
                                            <td>{if !empty($ree.sDuongDan)} <a href="{$url}{$ree.sDuongDan}">[Xem File]</a>{/if}</td>
                                            <td width="15%" class="text-center">{date_select($ree.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}
                            {if !empty($chammuon)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-10">Lý do chậm muộn - phòng thụ lý:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="10%">STT</th>
                                            <th>Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th class="text-center">Ngày nhập</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $chammuon as $cm}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td width="20%">{$canbo[$cm.FK_iMaCB]}</td>
                                            <td>{$cm.sNoiDung}</td>
                                            <td width="15%" class="text-center">{date_select($cm.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}
                        </div>
                    </div>
                <form class="form-horizontal {$anhien}" method="post" action="" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-12">Kết quả giải quyết <br><span style="color: red">  <i>(chỉ nhập khi có kết quả giải quyết)</i></span></label>
                                    <div class="col-sm-12">
                                        <textarea name="ketqua" id="" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="file" class="form-control" readonly="" name="file">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button class="btn btn-primary" type="submit" name="luulaiketqua" value="luulaiketqua">Hoàn thành công việc</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-12">Lý do chưa kết thúc văn bản:<br> <span style="color: red"><i>(do phải thu thập thông tin từ các đơn vị khác ,...)</i></span></label>
                                    <div class="col-sm-12">
                                        <textarea name="chammuon" id="" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button class="btn btn-primary" type="submit" name="luulaichammuon" value="luulaichammuon">Lý do chậm muộn</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>