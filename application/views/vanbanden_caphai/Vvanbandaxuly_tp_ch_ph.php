<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản chờ xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="8%" class="text-center">Ngày nhập</th>
                                    <th width="30%">Trích yếu - Thông tin</th>
                                    <th width="22%">Ý kiến</th>
                                    <th width="30%">Chỉ đạo</th>
                                    <th width="5%" class="text-center"><button type="submit" name="duyet" value="duyet" class="btn btn-success btn-xs">Duyệt</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{date_select($dl.sNgayNhap)}</td>
                                        <td>
                                            <p><b><a href="{$url}vanbanchoxulygq?ma={$dl.PK_iMaVB}" class="tin1">{$dl.sMoTa}</a></b></p>
                                            {if $dl.iGiayMoi==1}
                                                {if !empty($dl.noidungvanban)}<p><i>({$dl.noidungvanban})</i></p>{/if}
                                            {else}
                                                {if $dl.sNgayMoi > '2017-01-01' }<p><i>(Vào hồi {$dl.sGioMoi} ngày {date_select($dl.sNgayMoi)}, tại {$dl.sDiaDiem})</i></p>{/if}
                                            {/if}
                                            <p>- Số ký hiệu: {$dl.sKyHieu}</p>
                                            <p>- Số đến: <span style="color:red;font-weight: bold;font-size: 16px;">{$dl.iSoDen}</span></p>
                                            <p>- Nơi gửi: {$dl.sNoiGuiDen}</p>
                                            {if !empty($dl.sDuongDan)}
                                            <p>
                                                <a href="{$url}{$dl.sDuongDan}" class="tin1">Xem file</a>
                                            </p>
                                            {/if}
                                            <p><span style="color:red;">*</span> <i>Nội dung chỉ đạo của đ/c </i><b>{$dscanbo[$dl.FK_iMaCB_Gui]}:</b></p>
                                            <p><i>{$dl.sNoiDung}</i></p>
                                        </td>
                                        <td>
                                            <p>
                                                <select name="phophong_{$dl.PK_iMaVB}" class="form-control phophong select2" style="width: 100%" id="{$dl.PK_iMaVB}">
                                                    <option value="0">-- Chọn phó phòng --</option>
                                                    {if !empty($phophong)}
                                                        {foreach $phophong as $pp}
                                                            <option value="{$pp.PK_iMaCB}" {($dl.FK_iMaCB_PP_CT==$pp.PK_iMaCB)?'selected':''}>{$pp.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p> 
                                            <p>
                                                <input type="text" name="phophongph_{$dl.PK_iMaVB}" value="{$dl.FK_iMaCB_PP_PH}"  class="hide">
                                                <button type="button" name="phophongphoihop" {if empty($dl.FK_iMaCB_PP_CT)} disabled="" {/if} value="{$dl.PK_iMaVB}" data-toggle="modal" data-target="#myModalphophong" class="btn btn-success btn-sm phophongph_{$dl.PK_iMaVB}"><b>Chọn phó phòng phối hợp</b></button>
                                            </p>
                                            <p>
                                                <select name="chuyenvien_{$dl.PK_iMaVB}" class="form-control chuyenvien select2" style="width: 100%" id="{$dl.PK_iMaVB}">
                                                    <option value="0">-- Chọn chuyên viên --</option>
                                                    {if !empty($chuyenvien)}
                                                        {foreach $chuyenvien as $cv}
                                                            <option value="{$cv.PK_iMaCB}" {($dl.FK_iMaCB_CV_CT==$cv.PK_iMaCB)?'selected':''}>{$cv.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p> 
                                            <p>
                                                <input type="text" name="chuyenvienph_{$dl.PK_iMaVB}" value="{$dl.FK_iMaCB_CV_PH}"  class="hide">
                                                <button type="button" name="chuyenvienphoihop" {if empty($dl.FK_iMaCB_CV_CT)} disabled="" {/if} value="{$dl.PK_iMaVB}" data-toggle="modal" data-target="#myModalchuyenvien" class="btn btn-success btn-sm chuyenvienph_{$dl.PK_iMaVB}"><b>Chọn phối hợp</b></button>
                                            </p>
                                            <p>
                                                <input type="text" name="hanxuly_{$dl.PK_iMaVB}" class="form-control datepic datemask" placeholder="hạn xử lý" value="{if $dl.sHanGiaiQuyet > '1970-01-01'}{date_select($dl.sHanGiaiQuyet)}{/if}">
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <input type="text" class="form-control" value="{$dl.sGoiY_PP_CT}" name="tenphophong_{$dl.PK_iMaVB}" placeholder="Nội dung chuyển phó phòng">
                                            </p>
                                            <p>
                                                <textarea name="tenphophongph_{$dl.PK_iMaVB}" id="" class="form-control" rows="3" placeholder="Nội dung chuyển phó phòng phối hợp">{$dl.sGoiY_PP_PH}</textarea>
                                            </p>
                                            <p>
                                                <input type="text" class="form-control" name="tenchuyenvien_{$dl.PK_iMaVB}" value="{$dl.sGoiY_CV_CT}" placeholder="Nội dung chuyển chuyên viên chủ trì">
                                            </p>
                                            <p>
                                                <textarea name="tenchuyenvienph_{$dl.PK_iMaVB}" id="" class="form-control" rows="3" placeholder="Nội dung chuyển chuyên viên phối hợp">{$dl.sGoiY_CV_PH}</textarea>
                                            </p>
                                        </td>
                                        <td class="text-center">
                                            <span style="color:red;">Chọn duyệt</span>
                                            <input type="checkbox" name="mavanban[]" class="mavanban_{$dl.PK_iMaVB}" {if empty($dl.FK_iMaCB_PP_CT) && empty($dl.FK_iMaCB_CV_CT)} disabled {/if} value="{$dl.PK_iMaVB}">
                                        </td>
                                    </tr>
                                {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right"></div>
                </div>
                <div class="col-md-12">
                    <div class="modal fade" id="myModalphophong" tabindex="-1" role="dialog" aria-labelledby="myModalLabelphophong">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Phó phòng phối hợp</h4>
                            </div>
                            <div>
                            {if !empty($phophong)}
                                {foreach $phophong as $pp}
                                    <div class="col-md-6">
                                        <input type="checkbox" class="Checkboxpp" data-id="{$pp.sHoTen}" name="phophongphoihop" value="{$pp.PK_iMaCB}"> {$pp.sHoTen}
                                    </div>
                                {/foreach}
                            {/if}
                            </div>
                            <div class="modal-footer">
                                <input type="text" name="ma-pp" class="hide">
                                <button type="button" name="ghilaiphophong" class="btn btn-primary btn-sm" data-dismiss="modal">Ghi Lại</button>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="modal fade" id="myModalchuyenvien" tabindex="-1" role="dialog" aria-labelledby="myModalLabelchuyenvien">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Chuyên viên phối hợp</h4>
                            </div>
                            <div>
                            {if !empty($chuyenvien)}
                                {foreach $chuyenvien as $cv}
                                    <div class="col-md-6">
                                        <input type="checkbox" class="Checkboxcv" data-id="{$cv.sHoTen}" name="chuyenvienphoihop" value="{$cv.PK_iMaCB}"> {$cv.sHoTen}
                                    </div>
                                {/foreach}
                            {/if}
                            </div>
                            <div class="modal-footer">
                                <input type="text" name="ma-cv" class="hide">
                                <button type="button" name="ghilaichuyenvien" class="btn btn-primary btn-sm" data-dismiss="modal">Ghi Lại</button>
                            </div>
                        </div>
                      </div>
                    </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change','.phophong',function(){
            var mavanban = $(this).attr('id');
            var phophong = $('select[name=phophong_'+mavanban+']').val();
            var chuyenvien = $('select[name=chuyenvien_'+mavanban+']').val();
            var tenphophong = $(this).find("option:selected").text();
            if(phophong>0)
            {
                $('input[name=tenphophong_'+mavanban+']').val('Chuyển đ/c '+tenphophong+' chủ trì giải quyết.');
                $('.phophongph_'+mavanban+'').removeAttr('disabled');
            }
            else{
                $('input[name=tenphophong_'+mavanban+']').val('');
                $('textarea[name=tenphophongph_'+mavanban+']').val('');
                $('.phophongph_'+mavanban+'').attr('disabled','disabled');
            }
            if(phophong==0 && chuyenvien==0)
            {
                $('.mavanban_'+mavanban+'').attr('checked', false); 
                $('.mavanban_'+mavanban+'').attr("disabled", 'disabled');
            }
            else{
                $('.mavanban_'+mavanban+'').removeAttr('disabled');
            }

        });
        $(document).on('click','button[name=phophongphoihop]',function(){
            var mavanban = $(this).val();
            $('input[name=ma-pp]').val(mavanban);
        });
        $(document).on('click','button[name=chuyenvienphoihop]',function(){
            var mavanban = $(this).val();
            $('input[name=ma-cv]').val(mavanban);
        });
        $(document).on('change','.chuyenvien',function(){
            var mavanban = $(this).attr('id');
            var chuyenvien  = $(this).val();
            var tenchuyenvien = $(this).find("option:selected").text();
            if(chuyenvien>0)
            {
                $('input[name=ma-cv]').val(mavanban);
                $('input[name=tenchuyenvien_'+mavanban+']').val('Chuyển đ/c '+tenchuyenvien+' chủ trì giải quyết.');
                $('.chuyenvienph_'+mavanban+'').removeAttr('disabled');
            }
            else{
                $('input[name=chuyenvienph_'+mavanban+']').val('');
                $('input[name=tenchuyenvien_'+mavanban+']').val('');
                $('textarea[name=tenchuyenvienph_'+mavanban+']').val('');
                $('.chuyenvienph_'+mavanban+'').attr("disabled", 'disabled');
            }
            var phophong = $('select[name=phophong_'+mavanban+']').val();

            if(chuyenvien==0 && phophong==0)
            {
                $('.mavanban_'+mavanban+'').attr("disabled", 'disabled');
                $('.mavanban_'+mavanban+'').attr('checked', false); 
            }
            else{
                $('.mavanban_'+mavanban+'').removeAttr('disabled');
            }
        });
        $(document).on('click','button[name=ghilaiphophong]',function () {
            var tenpp = $('.Checkboxpp:checked').map(function() {
                return $(this).attr('data-id');
            }).get().join(',');
            var mapp = $('.Checkboxpp:checked').map(function() {
                return this.value;
            }).get().join(',');
            var mavanban = $('input[name=ma-pp]').val();
            $('input[name=phophongph_'+mavanban+']').val(mapp);
            if(mapp.length>0)
            {
                $('textarea[name=tenphophongph_'+mavanban+']').val(' '+tenpp+' phối hợp giải quyết');
            }
            else{
                $('input[name=phophongph_'+mavanban+']').val('');
                $('textarea[name=tenphophongph_'+mavanban+']').val('');
            }
            $('.Checkboxpp').attr('checked', false); 
        });
        $(document).on('click','button[name=ghilaichuyenvien]',function () {
            var tencv = $('.Checkboxcv:checked').map(function() {
                return $(this).attr('data-id');
            }).get().join(',');
            var macv = $('.Checkboxcv:checked').map(function() {
                return this.value;
            }).get().join(',');
            var mavanban = $('input[name=ma-cv]').val();
            $('input[name=chuyenvienph_'+mavanban+']').val(macv);
            if(macv.length>0)
            {
                $('textarea[name=tenchuyenvienph_'+mavanban+']').val(' '+tencv+' phối hợp giải quyết');
            }
            else{
                $('input[name=chuyenvienph_'+mavanban+']').val('');
                $('textarea[name=tenchuyenvienph_'+mavanban+']').val('');
            }
            $('.Checkboxcv').attr('checked', false); 
        });
    });
</script>