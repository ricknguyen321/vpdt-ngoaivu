<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Email đơn vị
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Email <label class="required" for="">*</label></label>
                                <input type="email" name="email" class="form-control" placeholder="Nhập email" required value="{($thongtin)?($thongtin[0]['sEmail']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Tên đơn vị <label class="required" for="">*</label></label>
                                <input type="text" name="donvi" class="form-control" placeholder="Nhập tên đơn vị" required value="{($thongtin)?($thongtin[0]['sTenDV']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Mail group <label class="required" for="">*</label></label>
                                <select name="mailgroup" required style="width:100%" id="" class="form-control select2">
                                    <option value="">-- Chọn Mail group --</option>
                                    {foreach $mailgroup as $m}
                                        <option value="{$m.ma}" {($thongtin)?($thongtin[0]['mail_group']==$m.ma)?'selected':'':''}>{$m.ten}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Số điện thoại <label class="required" for="">*</label></label>
                                <input type="text" class="form-control" name="sdt" value="{($thongtin)?($thongtin[0]['sSDT']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="0" class="flat-red" checked>  Hiển thị
                                </label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="1" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==1}checked{/if}{/if} class="flat-red">  Không hiển thị
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                                {if !empty($thongtin)}<a href="{$url}emaildonvi" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                            <div class="col-md-12">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">TT</th>
                                            <th>Email</th>
                                            <th>Đơn vị</th>
                                            <th>Ngày nhập</th>
                                            <th>Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td><a href="{$url}emaildonvi?id={$dl.PK_iMaEmailDV}">{$dl.sEmail}</a></td>
                                                    <td >{$dl.sTenDV}</td>
                                                    <td>{($dl.sNgayNhap=='0000-00-00')?'':date_select($dl.sNgayNhap)}</td>
                                                    <td><label for="" class="label {($dl.iTrangThai==0)?'label-success':'label-warning'}">{($dl.iTrangThai==0)?'Hiển thị':'Không hiển thị'}</label></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('padding-right', '0px');
    });
</script>