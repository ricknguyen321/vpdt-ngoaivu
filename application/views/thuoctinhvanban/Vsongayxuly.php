<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Quản lý số ngày xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Tên nội dung xử lý <label class="required" for="">*</label></label>
                                <textarea name="noidung" id="" required class="form-control" cols="" rows="4">{($thongtin)?($thongtin[0]['sNoiDung']):NULL}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Viết tắt <label class="required" for="">*</label></label>
                                <input type="text" name="viettat" class="form-control" placeholder="Nhập tên viết tắt" required value="{($thongtin)?($thongtin[0]['sVietTat']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Số ngày </label>
                                <input type="text" name="songay" class="form-control" onkeypress='return event.charCode >= 46 && event.charCode <= 57' placeholder="Nhập số ngày" required value="{($thongtin)?($thongtin[0]['fSoNgay']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="0" class="flat-red" checked>  Hiển thị
                                </label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="1" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==1}checked{/if}{/if} class="flat-red">  Không hiển thị
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                                {if !empty($thongtin)}<a href="{$url}songayxuly" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">STT</th>
                                            <th width="45%">Tên độ khẩn</th>
                                            <th width="15%">Viết tắt</th>
                                            <th width="15%">Sô ngày</th>
                                            <th width="20%">Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td><a href="{$url}songayxuly?id={$dl.PK_iMaSoNgay}">{$dl.sNoiDung}</a></td>
                                                    <td >{$dl.sVietTat}</td>
                                                    <td>{$dl.fSoNgay}</td>
                                                    <td><label for="" class="label {($dl.iTrangThai==0)?'label-success':'label-warning'}">{($dl.iTrangThai==0)?'Hiển thị':'Không hiển thị'}</label></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>