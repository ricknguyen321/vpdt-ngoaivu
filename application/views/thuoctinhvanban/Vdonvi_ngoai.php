<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Mã định danh <label class="required" for="">*</label></label>
                                <input type="text" name="madinhdanh" class="form-control" placeholder="Nhập mã định danh" required value="{($thongtin)?($thongtin[0]['sMaDinhDanh']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Tên đơn vị <label class="required" for="">*</label></label>
                                <input type="text" name="donvi" class="form-control" placeholder="Tên đơn vị" required value="{($thongtin)?($thongtin[0]['sTenDV']):NULL}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                                {if !empty($thongtin)}<a href="{$url}donvi_ngoai" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">STT</th>
                                            <th width="27%">Mã định danh</th>
                                            <th width="30%">Đơn vị</th>
                                            <th width="10%" class="text-center">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td>{$dl.sMaDinhDanh}</td>
                                                    <td>{$dl.sTenDV}</td>
                                                    <td class="text-center"><a href="{$url}donvi_ngoai?id={$dl.PK_iMaTT}"><i class="fa fa-edit"></i></a></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>