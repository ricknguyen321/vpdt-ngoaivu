<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Quản lý loại văn bản
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="{if empty($active)}active{else}''{/if}"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Danh sách loại văn bản</a></li>
                        <li role="presentation" class="{if !empty($active)}active{else}''{/if}"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Cập nhật loại văn bản</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane {if empty($active)}active{else}NULL{/if}" id="home">
                            <div class="row"><br/>
                                <div class="col-md-12"  style="overflow-x:auto;">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:5%" class="text-center">STT</th>
                                                <th style="width:15%">Tên loại văn bản</th>
                                                <th style="width:10%">Viết tắt</th>
                                                <th style="width:12%">Văn bản đi</th>
                                                <th style="width:15%">Tên PB theo SKH</th>
                                                <th style="width:15%">Tên LVB theo SKH</th>
                                                <th style="width:10%" class="text-center">Số đi riêng</th>
                                                <th style="width:10%">Trạng Thái</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {if !empty($dsdulieu)}{$i=1}
                                                {foreach $dsdulieu as $dl}
                                                    <tr>
                                                        <td>{$i++}</td>
                                                        <td><a href="{$url}loaivanban?id={$dl.PK_iMaLVB}">{$dl.sTenLVB}</a></td>
                                                        <td>{$dl.sTenVietTat}</td>
                                                        <td class="text-center"><label for="" class="label {($dl.iVanBanDi==1)?'label-success':'label-warning'}">{($dl.iVanBanDi==1)?'Có':'Không'}</label></td>
                                                        <td class="text-center"><label for="" class="label {($dl.iTenPBSKH==1)?'label-success':'label-warning'}">{($dl.iTenPBSKH==1)?'Có':'Không'}</label></td>
                                                        <td class="text-center"><label for="" class="label {($dl.iTenLVBSKH==1)?'label-success':'label-warning'}">{($dl.iTenLVBSKH==1)?'Có':'Không'}</label></td>
                                                        <td class="text-center"><label for="" class="label {($dl.iSoDiRieng==1)?'label-success':'label-warning'}">{($dl.iSoDiRieng==1)?'Có':'Không'}</label></td>
                                                        <td class="text-center"><label for="" class="label {($dl.iTrangThai==0)?'label-success':'label-warning'}">{($dl.iTrangThai==0)?'Hiển thị':'Không hiển thị'}</label></td>
                                                    </tr>
                                                {/foreach}
                                            {/if}
                                        </tbody>
                                    </table>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!--end danh sách đảng viên-->
                        
                        <div role="tabpanel" class="tab-pane {if !empty($active)}active{else}NULL{/if}" id="profile">
                            <form action=""  name="hocviencamtinhdang" method="post" accept-charset="utf-8" enctype="multipart/form-data" autocomplete="off">
                            <div class="row"><br/>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tên loại văn bản <label class="required" for="">*</label></label>
                                            <input type="text" name="ten" class="form-control" placeholder="Nhập tên loại văn bản" required value="{($thongtin)?($thongtin[0]['sTenLVB']):NULL}">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Văn bản đi</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="vanbandi" value="1" class="flat-red" checked>  Có
                                                </label>
                                                <label style="padding-left:25px">
                                                    <input type="radio" name="vanbandi" value="0" {if !empty($thongtin)} {if $thongtin[0]['iVanBanDi']==0}checked{/if}{/if} class="flat-red">  Không
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Tên PB theo SKH</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="pb" value="1" class="flat-red" checked>  Có
                                                </label>
                                                <label style="padding-left:25px">
                                                    <input type="radio" name="pb" value="0" {if !empty($thongtin)} {if $thongtin[0]['iTenPBSKH']==0}checked{/if}{/if} class="flat-red">  Không
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Chuyển thẳng GĐ</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="giamdoc" value="1" class="flat-red" checked>  Có
                                                </label>
                                                <label style="padding-left:25px">
                                                    <input type="radio" name="giamdoc" value="0" {if !empty($thongtin)} {if $thongtin[0]['iChuyenDenGD']==0}checked{/if}{/if} class="flat-red">  Không
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tên viết tắt <label class="required" for="">*</label></label>
                                            <input type="text" name="tenviettat" class="form-control" placeholder="Nhập tên viết tắt" required value="{($thongtin)?($thongtin[0]['sTenVietTat']):NULL}">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Số đi riêng</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="sorieng" value="1" class="flat-red" {if !empty($thongtin)} {($thongtin[0]['iSoDiRieng']==1)?'checked':''}{/if}>  Có
                                                </label>
                                                <label style="padding-left:25px">
                                                    <input type="radio" name="sorieng" value="0" class="flat-red" {if empty($thongtin)}checked{else}{($thongtin[0]['iSoDiRieng']==0)?'checked':''}{/if}>  Không
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Tên LVB theo SKH</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="lvb" value="1" class="flat-red" checked>  Có
                                                </label>
                                                <label style="padding-left:25px">
                                                    <input type="radio" name="lvb" value="0" {if !empty($thongtin)} {if $thongtin[0]['iTenLVBSKH']==0}checked{/if}{/if} class="flat-red">  Không
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Trạng thái</label>
                                            <div class="col-sm-8">
                                                <label>
                                                    <input type="radio" name="trangthai" value="0" class="flat-red" checked>  Hiển thị
                                                </label>
                                                <label style="padding-left:25px">
                                                    <input type="radio" name="trangthai" value="1" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==1}checked{/if}{/if} class="flat-red">  Không hiển thị
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-8 col-md-4">
                                        <button class="btn btn-primary pull-right margin-l" name="luudulieu" value="luudulieu">{($active)?'Cập nhật':'Thêm mới'}</button>
                                        {if !empty($active)}<a href="{$url}loaivanban" class="btn btn-default pull-right ">Quay lại >></a>{/if}
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <!--end thông tin lý lịch đảng viên-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <script>
    $(document).ready(function() {
        $('label').css('text-align', 'left');
    });
</script>