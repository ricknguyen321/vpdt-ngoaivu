<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Quản lý lĩnh vực
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Lĩnh vực cha</label>
                                <select name="linhvuccha" style="width:100%" id="" class="form-control select2">
                                    <option value="0">-- Chọn lĩnh vực --</option>
                                    {if ($thongtin)}
                                        {if ($thongtin[0]['id_parent']!=0)}
                                            {if !empty($linhvuccha)}
                                                {foreach $linhvuccha as $lv}
                                                    <option value="{$lv.linhVuc_id}" {($thongtin)?($thongtin[0]['id_parent']==$lv.linhVuc_id)?'selected':'':''}>{$lv.linhVuc_name}</option>
                                                {/foreach}
                                            {/if}
                                        {/if}
                                    {else}
                                        {if !empty($linhvuccha)}
                                                {foreach $linhvuccha as $lv}
                                                    <option value="{$lv.linhVuc_id}">{$lv.linhVuc_name}</option>
                                                {/foreach}
                                            {/if}
                                    {/if}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tên lĩnh vực <label class="required" for="">*</label></label>
                                <input type="text" name="linhvuc" class="form-control" placeholder="Nhập tên lĩnh vực" required value="{($thongtin)?($thongtin[0]['linhVuc_name']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Mô tả </label>
                                <textarea name="mota" class="form-control" id="" cols="" rows="3">{($thongtin)?($thongtin[0]['linhVuc_desc']):NULL}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="1" class="flat-red" checked>  Hiển thị
                                </label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="0" {if !empty($thongtin)} {if $thongtin[0]['linhVuc_active']==0}checked{/if}{/if} class="flat-red">  Không hiển thị
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                                {if !empty($thongtin)}<a href="{$url}linhvuc_qldv" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="text-center">STT</th>
                                            <th width="45%">Tên lĩnh vực</th>
                                            <th width="15%">Lĩnh vực cha</th>
                                            <th width="15%">Ngày nhập</th>
                                            <th width="20%">Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td><a href="{$url}linhvuc_qldv?id={$dl.linhVuc_id}">{$dl.linhVuc_name}</a></td>
                                                    <td class="text-center"><label for="" class="text-{($dl.id_parent==0)?'primary':'warning'}"><i class="fa {($dl.id_parent==0)?'fa-check':'fa-close'}"></i></label></td>
                                                    <td>{date_select($dl.linhVuc_date)}</td>
                                                    <td><label for="" class="label {($dl.linhVuc_active==1)?'label-success':'label-warning'}">{($dl.linhVuc_active==1)?'Hiển thị':'Không hiển thị'}</label></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>