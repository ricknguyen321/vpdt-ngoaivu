<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Quy trình ISO
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Tên quy trình <label class="required" for="">*</label></label>
                                <textarea name="tenquytrinh" id="" required class="form-control" cols="" rows="3">{($thongtin)?($thongtin[0]['sTenQuyTrinh']):NULL}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Số ngày <label class="required" for="">*</label></label>
                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="songay" class="form-control" required value="{($thongtin)?($thongtin[0]['iSoNgay']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="1" class="flat-red" checked>  Hiển thị
                                </label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="2" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==2}checked{/if}{/if} class="flat-red">  Không hiển thị
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                                {if !empty($thongtin)}<a href="{$url}quytrinhiso" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">STT</th>
                                            <th width="55%">Tên quy trình</th>
                                            <th width="20%">Số ngày</th>
                                            <th width="20%">Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td><a href="{$url}quytrinhiso?id={$dl.PK_iMaQuyTrinh}">{$dl.sTenQuyTrinh}</a></td>
                                                    <td >{$dl.iSoNgay}</td>
                                                    <td><label for="" class="label {($dl.iTrangThai==1)?'label-success':'label-warning'}">{($dl.iTrangThai==1)?'Hiển thị':'Không hiển thị'}</label></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>