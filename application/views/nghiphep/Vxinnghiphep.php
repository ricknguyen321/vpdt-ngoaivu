<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập phê duyệt nghỉ phép
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
	<div class="modal fade" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="width:60%; padding-top:100px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Hướng dẫn tạo phê duyệt xin nghỉ phép</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12" style="margin-bottom:30px">
                                    <div class="col-sm-12">
										<p>Bước 1: Chọn loại hình nghỉ phép: Nghỉ phép | Đi công tác/học | Nghỉ ốm/việc riêng | Nghỉ không lương.</p>
										<p>Bước 2: Nhập số ngày xin nghỉ (Nếu xin nghỉ nửa ngày thì nhập 0)</p>
										<p>Bước 3: Nhập thời gian nghỉ từ hh1:mm1 giờ ngày dd1/mm1/yyyy1 đến hh2:mm2 giờ ngày dd2/mm2/yyyy2 (Nếu xin nghỉ nửa ngày thì không cần nhập giờ, phút chỉ cần nhập ngày)</p>
										<p>Bước 4: Nhập lý do xin nghỉ phép, ý kiến trình lên lãnh đạo.</p>
										<p>Bước 5: Nhập thông tin số lượng văn bản/nhiệm vụ đang triển khai: Thống kê rõ tên các văn bản/nhiệm vụ, tiến độ triển khai, kiến nghị đề xuất.</p>
										<p>Bước 6: Chọn lãnh đạo phòng duyệt nghỉ phép.</p>
										<p>Bước 7: Nhấn "Thêm mới".</p>
										
										<b>Quy trình xin nghỉ phép: Chuyên viên làm phê duyệt => Lãnh đạo phòng cho ý kiến, duyệt=> Lãnh đạo Văn phòng rà soát, cho ý kiến, duyệt => Giám đốc chỉ đạo, duyệt</b>
										
                                    </div>
                                </div>
                                
                            </div>
                        </div>                       
                    </form>
                </div>
            </div>
        </div>
		

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:80%; justify-content: center; margin-left:10%; padding-top:30px">
						<p style="text-align:right">
							  <button tabindex="11" type="button" data-toggle="modal" data-target="#myModal1" class="btn btn-primary btn-sm"><b>Hướng dẫn</b></button>
						</p>
						<div class="col-md-12" style="margin-top: 10px; ">
							<div class="col-md-6">
								<div class="form-group">
									<label for="" class="col-sm-4 control-label" style="">Ngày phép hiện có:</label>

                                    <div class="col-sm-8" style="margin-top: 5px; ">
									{$ngayphepconlai} ngày
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 10px; ">
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Loại nghỉ</label>

                                    <div class="col-sm-8">
										<select name="loai_phep" id="" required class="form-control select2 " style="">
											<option value="Nghỉ phép" {($thongtin[0]['loai_phep'] == 'Nghỉ phép') ? 'selected': ''}>Nghỉ phép</option>
											<option value="Đi công tác/học" {($thongtin[0]['loai_phep'] == 'Đi công tác/học') ? 'selected': ''}>Đi công tác/học</option>
											<option value="Nghỉ ốm/việc riêng" {($thongtin[0]['loai_phep'] == 'Nghỉ ốm/việc riêng') ? 'selected': ''}>Nghỉ ốm/việc riêng</option>
											<option value="Nghỉ không lương" {($thongtin[0]['loai_phep'] == 'Nghỉ không lương') ? 'selected': ''}>Nghỉ không lương</option>
										</select>
                                    </div>
                                </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Số ngày nghỉ *</label>

                                    <div class="col-sm-8">
										<input type="number" name="so_ngay_nghi" tabindex="9" value="{($thongtin)?($thongtin[0]['so_ngay_nghi']):''}" class="form-control req" id="" placeholder="Nếu nghỉ nửa ngày nhập: 0" required>
                                    </div>
                                </div>
							</div>
						</div>
						
						<div class="col-md-12" >
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style=""></label>

                                    <div class="col-sm-8">
										<div style="display: flex; font-size:12px; text-align:center">
											<i>(Chú ý chọn đúng loại nghỉ phép và nhập chính xác số ngày nghỉ để hệ thống trừ chính xác số ngày phép còn lại.)</i>
										</div>
                                        <!--<input type="text" name="giomoi" tabindex="9" value="{($thongtin)?($thongtin[0]['sGioMoi']):''}" class="form-control req" id="" placeholder="hh:mm ví dụ: 08:30">-->
                                    </div>										
                                </div>
							</div>
							
                        <div class="col-md-12" style="margin-top: 10px; ">							
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Từ</label>
                                    <div class="col-sm-8">
										<div style="display: flex">
											<input type="text" name="giotu" value="{($giotu)?$giotu:''}" class="form-control req" id="" style="width:20%" placeholder="giờ">
										
											&nbsp;:&nbsp;
										
											<input type="text" name="phuttu" value="{($phuttu)?$phuttu:''}" class="form-control req" id="" style="width:20%" placeholder="phút">
											 &nbsp;&nbsp; ngày &nbsp;&nbsp;
											 <input  style="width:40%" type="text" name="ngay_tu" tabindex="8" value="{($thongtin)?($thongtin[0]['ngay_tu']!=0000-00-00)?date_select($thongtin[0]['ngay_tu']):'':''}" class="form-control datepic datemask req" id="" required placeholder="ngày tháng">
										</div>
										
                                        <!--<input type="text" name="giomoi" tabindex="9" value="{($thongtin)?($thongtin[0]['sGioMoi']):''}" class="form-control req" id="" placeholder="hh:mm ví dụ: 08:30">-->
                                    </div>		

									<div>
									
									</div>
                                </div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Đến</label>

                                    <div class="col-sm-8">
										<div style="display: flex">
										
											<input type="text" name="gioden" value="{($gioden)?$gioden:''}" class="form-control req" id="" style="width:20%" placeholder="giờ">
										
											&nbsp;:&nbsp;
										
											<input type="text" name="phutden" value="{($phutden)?$phutden:''}" class="form-control req" id="" style="width:20%" placeholder="phút">
											 &nbsp;&nbsp; ngày &nbsp;&nbsp;
											<input  style="width:40%" type="text" name="ngay_den" tabindex="8" value="{($thongtin)?($thongtin[0]['ngay_den']!=0000-00-00)?date_select($thongtin[0]['ngay_den']):'':''}" class="form-control datepic datemask req" id="" required placeholder="ngày tháng">
										</div>
                                        <!--<input type="text" name="giomoi" tabindex="9" value="{($thongtin)?($thongtin[0]['sGioMoi']):''}" class="form-control req" id="" placeholder="hh:mm ví dụ: 08:30">-->
                                    </div>										
                                </div>
								
                            </div>							
							
							<!--<div class="col-md-6">
							
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue; float:left; text-align: left !important">ngày</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngay_tu" tabindex="8" value="{($thongtin)?($thongtin[0]['ngay_tu']!=0000-00-00)?date_select($thongtin[0]['ngay_tu']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>
								
								
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">ngày</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngay_den" tabindex="8" value="{($thongtin)?($thongtin[0]['ngay_den']!=0000-00-00)?date_select($thongtin[0]['ngay_den']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>
							
								
                            </div>-->
							<div class="col-md-12" >
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style=""></label>

                                    <div class="col-sm-8">
										<div style="display: flex; font-size:12px; text-align:center">
											<i>(Nếu nghỉ cả ngày hoặc nghỉ nhiều ngày chỉ cần chọn ngày không cần nhập giờ)</i>
										</div>
                                        <!--<input type="text" name="giomoi" tabindex="9" value="{($thongtin)?($thongtin[0]['sGioMoi']):''}" class="form-control req" id="" placeholder="hh:mm ví dụ: 08:30">-->
                                    </div>										
                                </div>
							</div>
							<div class="col-md-12" style="margin-top: 10px; ">
								<div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color:blue">Ý kiến (lý do) xin nghỉ *</label>
                                    <div class="col-sm-10">
										<textarea name="ly_do" id="" class="form-control" placeholder="Nhập lý do, ý kiến trình lãnh đạo xin nghỉ." rows="3" required>{($thongtin)?$thongtin[0]['ly_do']:''}</textarea>
                                    </div>
                                </div>  
							</div>
							
							<div class="col-md-12" style="margin-top: 10px; ">
							
								<div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="">Số lượng văn bản/nhiệm vụ đang triển khai:</label>
                                    <div class="col-sm-10">
										<textarea name="cong_viec" id="" class="form-control" placeholder="Thống kê rõ tên các văn bản/nhiệm vụ, tiến độ triển khai, kiến nghị đề xuất" rows="4" >{($thongtin)?$thongtin[0]['cong_viec']:''}</textarea>
                                    </div>
                                </div>  
							</div>
                            
                        </div>
						<div class="col-sm-12" style="margin-bottom:20px; ">
									<div class="col-sm-6">
										{if $vanban['iQuyenHan_DHNB'] > 6}
											<div class="form-group">											
												<label for="" class="col-sm-4 control-label" style="">Lãnh đạo duyệt</label>

												<div class="col-sm-8">
												
													{if !empty($dsldduyet)}
														<select name="ldduyet" style="width: 100%" id="" class="form-control select2">
															{foreach $dsldduyet as $ld}
																{if $ld.PK_iMaCB == $vanban['PK_iMaCB']}
																	{continue}
																{/if}
																<option value="{$ld.PK_iMaCB}" {if $ld.PK_iMaCB == $thongtin[0]['cb_giu']}selected{/if}>{$ld.sHoTen}</option>
															{/foreach}
														</select>
													{/if}
												</div>
											</div>
										{/if}
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top:10px; margin-bottom:20px; text-align: center;">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm"><b>{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</b></button> &nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dsnghiphep"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">&#8594; Danh sách nghỉ phép</button> </a>
                        </div>
						
							
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

