<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3" style="width:20%">
                    <h3 class="font">
                        Danh sách cán bộ nghỉ phép <b> ({$count})</b>
						<!--{if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs" onclick="myFunction()">Gửi nhiều email cùng lúc</button>{/if}-->
                    </h3>
                </div>
                <div class="col-sm-9" style="width:80%">
					<div class="col-md-5" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
									<input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm kiếm tổng hợp">
                               
                            </div>		
						</form>
					</div>
										
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
				<i>Ghi chú: Cán bộ được bôi đỏ là cán bộ đang nghỉ hoặc sắp nghỉ phép.</i>
				<div class="pull-right" style="margin: -15px 10px">{$phantrang}</div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50px">STT</th>
                                    <th width="18%">Cán bộ, thời gian nghỉ</th>
									<th width="35%">Thông tin nghỉ phép</th>			
									<th width="">Ý kiến của lãnh đạo</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsnghiphep)} 
								{$i=1}
								{$i = $i + $page}
                                {foreach $dsnghiphep as $np}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        {if $np.bgd_duyet == 1}<td style="background: url(/vpdtsongoaivu/files/images/approved.png) top no-repeat;vertical-align: middle;background-size: 40%;" class="text-center">{else} <td class="text-center">{/if}									
											<b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{$np.ho_ten}</b><br><br>
											<p>- Số ngày nghỉ: <b>{$np.so_ngay_nghi} ngày</b> </p>
											{if $np.ngay_tu == $np.ngay_den}
												{if $np.gio_tu == ':' && $np.gio_den == ':'}
													Ngày <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{date_select($np.ngay_tu)}</b>
												{else}
													Từ <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{($np.gio_tu != ':')?$np.gio_tu:''}</b> đến <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{($np.gio_den != ':')?$np.gio_den:''}</b> ngày <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{date_select($np.ngay_tu)}</b>
												{/if}
											{else}
												Từ <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{($np.gio_tu != ':')?$np.gio_tu:''}</b> ngày <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{date_select($np.ngay_tu)}</b> <br>
												đến <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{($np.gio_den != ':')?$np.gio_den:''}</b> ngày <b style="{(date('Y-m-d',time()) <= $np.ngay_den)?'color:red':''}">{date_select($np.ngay_den)}</b>
											{/if}
											<br><br>
											- Ngày tạo: {date_time2($np.ngay_nhap)}
										</td>
										<td>
											<p>- Loại nghỉ: <b>{$np.loai_phep}</b></p>
											<p>- Lý do xin nghỉ: <b>{$np.ly_do}</b></p>
											<p>- Bàn giao công việc: {$np.cong_viec}</p>
											{if $vanban['PK_iMaCB'] == $np.FK_iMaCB && $np.ldvp_duyet != 1 && $np.tp_duyet != 1 && $np.bgd_duyet != 1}
												<hr>
												<p style="text-align:right"><a href="{$url}xinnghiphep?id={$np.id}" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a> &nbsp;&nbsp; <button type="submit" name="xoa" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" value="{$np.id}" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button></p>
											{/if}
										</td>
										<td>
											<p>{$np.yk_bgd}</p>
											<hr>
											<p>{$np.yk_ldvp}</p>
											<hr>
											<p>{$np.yk_tp}</p>

										</td>
                                       
                                        <!--<td class="text-center">
											{if ($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaPhongHD'] == 76) || $vanban['sHoTen'] == $np.cb_xu_ly || $vanban['PK_iMaCB'] == $np.FK_iMaCBNhap}
												<p><a href="nhaphosoabtc?id={$np.id}" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default"><i class="fa fa-edit"></i></a></p>
											{else}
												-
											{/if}
                                        </td>-->
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
function myFunction($ma, $loai, $sodi) {	
	window.open("{$url}upload_files_vbdi?ma=" + $ma + "&loai=" + $loai + "&isodi=" + $sodi,"phathanh","menubar=1,location=no,resizable=0,width=550,height=320, top=180, left=400");
}
</script>