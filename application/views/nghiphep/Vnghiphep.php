<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Thông tin xin nghỉ phép của cán bộ {$thongtin[0]['ho_ten']}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:80%; justify-content: center; margin-left:10%">
                        <div class="col-md-12" style="margin-top: 10px; ">
							<div class="col-md-12" style="margin-top: 10px; ">
							
								<div class="form-group">
                                    <div for="" class="col-sm-2" style="">Lý do xin nghỉ:</div>
                                    <div class="col-sm-10"><b>{($thongtin)?$thongtin[0]['ly_do']:''}</b>
                                    </div>
                                </div>  
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                    <div for="" class="col-sm-4" style="">Nghỉ từ:</div>

                                    <div class="col-sm-8">
										<b>{$thongtin[0]['gio_tu']}</b> ngày <b>{($thongtin)?($thongtin[0]['ngay_tu']!=0000-00-00)?date_select($thongtin[0]['ngay_tu']):'':''}</b>
                                    </div>										
                                </div>
								
								<div class="form-group">
                                    <div for="" class="col-sm-4" style="">Nghỉ đến:</div>

										<div class="col-sm-8">
											<b>{$thongtin[0]['gio_den']}</b> ngày <b> {($thongtin)?($thongtin[0]['ngay_den']!=0000-00-00)?date_select($thongtin[0]['ngay_den']):'':''}</b>
										</div>								
                                </div>
                                
								
								<div class="form-group">
                                    <div for="" class="col-sm-4" style="">Loại nghỉ:</div>

                                    <div class="col-sm-8">
										<b>{$thongtin[0]['loai_phep']}</b>
                                    </div>
                                </div>
								
                            </div>							
							
							
							
							<div class="col-md-12" style="margin-top: 10px; ">
							
								<div class="form-group">
                                    <div for="" class="col-sm-2" style="">Ghi chú:</div>
                                    <div class="col-sm-10">
										<b>{($thongtin)?$thongtin[0]['note']:''}</b>
                                    </div>
                                </div>  
							</div>
                            
                        </div>
						
						
						<div class="col-md-12" style="margin-top:30px; margin-bottom:20px; text-align: center;">
                                <a href="xinnghiphep?id={($thongtin)?$thongtin[0]['id']:''}"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Sửa</button> </a>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="dsnghiphep"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">&#8594; Danh sách nghỉ phép</button> </a>
                        </div>
						
							
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-div').css('text-align', 'right');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

