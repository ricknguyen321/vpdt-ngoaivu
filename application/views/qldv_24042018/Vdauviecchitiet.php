<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Đầu việc chi tiết
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$thongtin[0]['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($thongtin[0]['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($thongtin[0]['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b> <a href="{($file_dk)?$file_dk[0]['qlvFile_path']:''}">{($file_dk)?'[Xem File]':''}</a></span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$thongtin[0]['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($thongtin[0]['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$thongtin[0]['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$thongtin[0]['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$thongtin[0]['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($thongtin[0]['qlv_date_nhap'])}</span>
                                
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table id="" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width:5%" class="text-center">STT</th>
                                        <th width="45%">Nội dung Công Việc</th>
                                        <th width="20%">Phòng chủ trì</th>
                                        <th width="20%">Phòng phối hợp</th>
                                        <th width="15%">Hạn xử lý</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {if !empty($qldvDetail)}{$i=0}
                                        {foreach $qldvDetail as $dv}
                                        <!-- {$i++} -->
                                            <tr style="background:#e4e4e4">
                                                <th class="text-center">{intToRoman($i)}</td>
                                                <th colspan="4">{$dv.qlvDetails_desc}</th>
                                            </tr>
                                            <!-- {$j=0} -->
                                            {foreach $qldvDetail1 as $dv1}
                                            <!-- {$j++} -->
                                                {if $dv.qlvDetails_id == $dv1.qlv_id_sub}
                                                    <tr>
                                                        <td class="text-center">{$i}.{$j}</td>
                                                        <td><a href="{$url}chitietdauviec/{$dv1.qlvDetails_id}" class="tin">{$dv1.qlvDetails_desc}</a></td>
                                                        <td>{if isset($mangPB[$dv1.main_department])}{$mangPB[$dv1.main_department]}{/if}</td>
                                                        <td>
                                                            {if !empty($dv1.department_id)}
                                                                {$phongban = explode(',',$dv1.department_id)}
                                                                {foreach $phongban as $pb}
                                                                    {$mangPB[$pb]}, 
                                                                {/foreach}
                                                            {/if}
                                                        </td>
                                                        <td>{($dv1.han_thongke>'1970-01-01')?date_select($dv1.han_thongke):''}</td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                        {/foreach}
                                    {/if}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

