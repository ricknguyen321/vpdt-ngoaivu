<script src="{$url}assets/js/qldv/qldv.js"></script>
<script src="{$url}assets/js/qldv/addqldvDetails.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới đầu việc chi tiết
      </h1>
    </section>
    <!-- start Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $main_depart as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" class="Checkbox"> {$de.sTenPB}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hide" >
                                <input type="text" name="phongchutri1" value="" class="hide" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- emd Modal -->
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-2">Tiêu đề đầu việc</label>

                                <div class="col-sm-10">
                                    {$qldvDetail[0]['qlvDetails_desc']}
                                </div>
                            </div>
                        </div> 
                        <div class="col-sm-12" style="margin-top: -20px;">
                            <hr style="border: 1px dashed #ccc">
                        </div>
                            <div class="themsau"></div>
                            <div class="col-md-12 text-right">
                                <div class="col-sm-8">
                                    <button type="button" name="themmoi" value="0" class="btn btn-success"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                    
                                    <input type="text" name="tong_dv_chitiet" class="hidden" value="">
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <button type="submit" name="luulai" value="Thêm mới" class="btn btn-primary">Thêm mới</button> <a href="javascript: history.back(1)" class="btn btn-default" data-original-title="" title="">Quay lại &gt;&gt;</a> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $(document).on('click','button[name=themmoi]',function(){
            var stt = $(this).val();
            var tong = parseInt(stt)+1;
           var noidung1 = '<div class="col-md-8 noidung"><div class="form-group"><label for="" class="col-sm-3 control-label">Nội dung đầu việc *</label><div class="col-sm-9"><textarea name="noidunghop[]" class="form-control" rows="2" style="border: 1px solid red;"></textarea></div></div></div>';

            var noidung2 = ' <div class="col-md-4 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label" required>C.hỏi - P.lục *</label><div class="col-sm-8 "><input name="tieudeviec[]" class="form-control"/></div></div><div class="form-group"><label for="" class="col-sm-4 control-label">Lãnh đạo * </label><div class="col-sm-8"><select name="lanhdao[]" id="" required class="form-control select2" style="width:100%"><option value="0">-- Chọn lãnh đạo --</option>{if !empty($lanhdao)}{foreach $lanhdao as $ld}<option value="{$ld.PK_iMaCB}">{$ld.sHoTen}</option>{/foreach}{/if}</select></div></div></div><div class="col-md-6 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label" required>Đơn vị chủ trì *</label><div class="col-sm-8 "><select name="phongchutri[]" id="" required class="form-control select2" style="width:100%"><option value="0">-- Chọn đơn vị --</option>{if !empty($main_depart)}{foreach $main_depart as $p}<option value="{$p.PK_iMaPB}"}>{$p.sTenPB}</option>{/foreach}{/if}</select></div></div></div>';


            var noidung3 = '<div class="col-md-2 noidung"><div class="form-group<div style="margin-bottom: 10px"><input type="hidden" name="phongphoihop[]" id="100_'+stt+'" value=""><button type="button" name="chonphoihop" value="'+stt+'" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Chọn phối hợp</button></div></div></div>';

            var noidung4 = '<div class="col-md-4 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label">Hạn xử lý</label><div class="col-sm-8"><input type="text" class="form-control datepic datemask" id="" value="" name="hanxuly[]"></div></div></div> ';
            var noidung5 = '<div class="col-md-8 noidung"><div class="form-group"><label for="" class="col-sm-3 control-label">Phân công trả lời của TP</label><div class="col-sm-9"><textarea name="noidungphancong[]" class="form-control" rows="2"></textarea></div></div></div>';
            var noidung6 = ' <div class="col-md-4 noidung"><div class="form-group"><label for="" class="col-sm-4 control-label" required>Cử tri</label><div class="col-sm-8 "><input name="cutri[]" class="form-control"/></div></div><div class="form-group"><label for="" class="col-sm-4 control-label">Phân công trả lời của TP </label><div class="col-sm-8"><select name="trangthaiphancong[]" class="form-control select2" style="width:100%"><option value="0">-- Chọn chủ tri phối hợp --</option><option value="1">SNV chủ trì</option><option value="2">SNV phối hợp</option></select></div></div></div>';
            var noidung = noidung1+noidung2+noidung3+noidung4+noidung5+noidung6;
            $('.themsau').before(noidung);
            $(".timepicker").timepicker({
                showInputs: false
            });
            $('button[name=themmoi]').val(tong);
            $('input[name=tong_dv_chitiet]').val(tong);
            $(".select2").select2();
            //datepic dd/mm/yyyy
            $('.datepic').datepicker({
              autoclose: true
            });
            //Datemask dd/mm/yyyy
            $(".datemask").inputmask("dd/mm/yyyy");
            $('.control-label').css('text-align', 'left');
        });
    });
</script>

