<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Đầu việc chi tiết
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="45%">Nội dung Công Việc</th>
                                    <th width="20%">Phòng chủ trì</th>
                                    <th width="20%">Phòng phối hợp</th>
                                    <th width="15%">Hạn xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbanong)}{$ii=1}
                                    {foreach $vanbanong as $vb}
                                        <tr style="background:#e4e4e4">
                                            <th class="text-center">{intToRoman($ii++)}</th>
                                            <th colspan="4">{$vb.qlv_noidung}</th>
                                        </tr>
                                        {if !empty($qldvDetail)}
                                            {foreach $qldvDetail as $dv}
                                            <!-- {$i=1} -->
                                                {if $vb.qlv_id == $dv.qlv_id}
                                                    <tr >
                                                        <td class="text-center">{$i}</td>
                                                        <td colspan="4">{$dv.qlvDetails_desc}</td>
                                                    </tr>
                                                    <!-- {$j=0} -->
                                                    {foreach $qldvDetail1 as $dv1}
                                                        {if $dv.qlvDetails_id == $dv1.qlv_id_sub}
                                                        <!-- {$j++} -->
                                                            <tr>
                                                                <td class="text-center">{$i}.{$j}</td>
                                                                <td><a href="{$url}chitietdauviec/{$dv1.qlvDetails_id}" class="tin">{$dv1.qlvDetails_desc}</a></td>
                                                                <td>{if isset($mangPB[$dv1.main_department])}{$mangPB[$dv1.main_department]}{/if}</td>
                                                                <td>
                                                                    {if !empty($dv1.department_id)}
                                                                        {$phongban = explode(',',$dv1.department_id)}
                                                                        {foreach $phongban as $pb}
                                                                            {$mangPB[$pb]}, 
                                                                        {/foreach}
                                                                    {/if}
                                                                </td>
                                                                <td>{($dv1.han_thongke>'1970-01-01')?date_select($dv1.han_thongke):''}</td>
                                                            </tr>
                                                        {/if}
                                                    {/foreach}
                                                    <!-- {$i++} -->
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

