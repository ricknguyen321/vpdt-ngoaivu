<script src="{$url}assets/js/qldv/dauviec.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="30%">{($trangthai==5)?'Cán bộ phối hợp':'Lãnh đạo chỉ đạo'}</th>
                                    <th width="30%">{($trangthai==5)?'Nội dung chuyển':'Cán bộ phối hợp'}</th>
                                    <th style="width:5%" class="{if $trangthai!=5}hide{/if}"><button type="submit" name="duyet" value="duyet" class="btn btn-primary btn-xs">Duyệt</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbanong)}{$i=1}
                                    {foreach $vanbanong as $vb}
                                        <tr style="background:#e4e4e4">
                                            <th class="text-center">{intToRoman($i++)}</td>
                                            <th colspan="{if $trangthai==5}4{else}5{/if}">
                                                {$vb.qlv_noidung}
                                                {if $vb.qlv_file==1}
                                                    <a class="tin" href="{$vb.qlvFile_path}">[Xem File]</a>
                                                {/if}
                                            </td>
                                        </tr>
                                        {if !empty($vanbancon)}{$j=1}
                                            {foreach $vanbancon as $vbc}
                                                {if $vbc.qlv_id==$vb.qlv_id}
                                                    <tr>
                                                        <td class="text-center">{$j++}</td>
                                                        <td width="30%">
                                                            <p><a href="{$url}chitietdauviecphoihop/{$vbc.qlvDetails_id}" class="tin">{$vbc.qlvDetails_desc}</a></p>
                                                            <p>- Người nhập:{$vbc.sHoTen}</p>
                                                            <p>- Ngày nhập:{date_time($vbc.qlvDetails_date)}</p>
                                                            <p>- Hạn văn bản:{date_select($vbc.qlvDetails_limit_time)}</p>
                                                        </td>
                                                        <td>
                                                            {if $trangthai==5}
                                                                <p>
                                                                    <select style="width:100%" name="chuyenvienPH[{$vbc.qlvDetails_id}][]" multiple="" data-placeholder="Chọn chuyên viên phối hợp" id="{$vbc.qlvDetails_id}" class="form-control chuyenvienPH select2">
                                                                        {if !empty($dsCV)}
                                                                            {foreach $dsCV as $cvph}
                                                                                <option value="{$cvph.PK_iMaCB}" 
                                                                                    {if !empty($vbc.user_nhan)}
                                                                                        {$phoihopcv = explode(',',$vbc.user_nhan)}
                                                                                        {foreach $phoihopcv as $phcv}
                                                                                            {if $phcv==$cvph.PK_iMaCB}
                                                                                                selected
                                                                                            {/if}
                                                                                        {/foreach}
                                                                                    {/if}
                                                                                >{$cvph.sHoTen}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                </p>
                                                                <p>
                                                                    <input type="text" name="hanxuly[{$vbc.qlvDetails_id}]" value="{($vbc.han_giaiquyet!='0000-00-00' && !empty($vbc.han_giaiquyet))?date_select($vbc.han_giaiquyet):''}" class="form-control datepic datemask" placeholder="Hạn giải quyết">
                                                                </p>
                                                            {else}
                                                            <p><b>{$truongphong['sHoTen']}</b></p>
                                                                {if $vbc.han_giaiquyet!='0000-00-00' && !empty($vbc.han_giaiquyet)}
                                                                    <p>Hạn giải quyết: {date_select($vbc.han_giaiquyet)}</p>
                                                                {/if}
                                                            {/if}
                                                        </td>
                                                        <td>
                                                            {if $trangthai!=5}
                                                                {if !empty($vbc.user_nhan)}
                                                                {$canbonhan = explode(',',$vbc.user_nhan)}
                                                                {$i=1}
                                                                    {foreach $canbonhan as $n}
                                                                        {if $i>1}
                                                                        <hr style="border-bottom: 1px dashed gold; margin: 5px 5px 5px 2px; border-top:0px">
                                                                        {/if}
                                                                        <p>{$i++}. {$mangCB[$n]}</p>
                                                                    {/foreach}
                                                                {/if}
                                                            {/if}
                                                            <textarea class="form-control noidungchuyen_CVPH_{$vbc.qlvDetails_id} {($trangthai==5)?'':'hide'}" name="noidungchuyen_CVPH{$vbc.qlvDetails_id}" id="" rows="8">{$vbc.noidung_nhan}</textarea>
                                                        </td>
                                                        <td class="text-center {if $trangthai!=5}hide{/if}">
                                                            <input name="mavanbanchau[]" class="{($vbc.user_nhan)?'hide':''}" value="{$vbc.qlvDetails_id}" type="checkbox">
                                                        </td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>