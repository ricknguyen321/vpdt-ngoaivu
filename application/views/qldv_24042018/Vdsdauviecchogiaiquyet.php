<script src="{$url}assets/js/qldv/dauviec.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel">Văn bản trả lại</h4>
                                </div>
                                <form method="post">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="control-label">Người trả:</label>
                                            <input readonly="" type="text" value="{$vanban['sHoTen']}" class="form-control" name="nguoitra">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Nội dung:</label>
                                            <textarea required minlength="7" class="form-control" name="noidung"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                        <button type="submit" name="guidi" value="" class="btn btn-primary">Gửi đi</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Trích yếu + Lý do trả lại</th>
                                    <th width="30%">Lãnh đạo chủ trì</th>
                                    <th width="30%">Nội dung chuyển</th>
                                    <th style="width:5%" class="{if $quyen==8}hide{/if}"><button type="submit" name="duyet" value="duyet" class="btn btn-primary btn-xs">Duyệt</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbanong)}{$i=1}
                                    {foreach $vanbanong as $vb}
                                        <tr style="background:#e4e4e4">
                                            <th class="text-center">{intToRoman($i++)}</td>
                                            <th colspan="{if $quyen==8}4{else}5{/if}">
                                                {$vb.qlv_noidung}
                                                {if $vb.qlv_file==1}
                                                    <a class="tin" href="{$vb.qlvFile_path}">[Xem File]</a>
                                                {/if}
                                            </td>
                                        </tr>
                                        {if !empty($vanbancon)}{$j=1}
                                            {foreach $vanbancon as $vbc}
                                                {if $vbc.qlv_id==$vb.qlv_id}
                                                    <tr>
                                                        <td class="text-center">{$j++}</td>
                                                        <td width="30%">
                                                            <p><a href="{$url}chitietdauviec/{$vbc.qlvDetails_id}" class="tin">{$vbc.qlvDetails_desc}</a></p>
                                                            <p>- Người nhập:{$vbc.sHoTen}</p>
                                                            <p>- Ngày nhập:{date_time($vbc.qlvDetails_date)}</p>
                                                            <p>- Hạn văn bản:{($vbc.qlvDetails_limit_time>'1970-01-01')?date_select($vbc.qlvDetails_limit_time):''}</p>
                                                            {if !empty($vbc.noidung_tralai)}
                                                            <hr style="border-bottom: 1px dashed gold; margin: 5px; border-top:0px">
                                                            <p style="color:red">{$vbc.noidung_tralai}</p>
                                                            <p>- Người trả:{$vbc.hoten_tralai}</p>
                                                            <p>- Ngày trả:{date_time($vbc.thoigian_tralai)}</p>
                                                            {/if}
                                                        </td>
                                                        <td width="28%">
                                                            {if $quyen==4} <!--end GD được chuyển cho PGD form-->
                                                            <p>
                                                                <select style="width:100%" name="phogiamdoc[{$vbc.qlvDetails_id}]" id="{$vbc.qlvDetails_id}" class="form-control phogiamdoc select2">
                                                                    <option value="">Chuyển phó giám đốc</option>
                                                                    {if !empty($phogiamdoc)}
                                                                        {foreach $phogiamdoc as $pgd}
                                                                            <option value="{$pgd.PK_iMaCB}" {($vbc.process_per==$pgd.PK_iMaCB)?'selected':''}>{$pgd.sHoTen}</option>
                                                                        {/foreach}
                                                                    {/if}
                                                                </select>
                                                            </p>
                                                            {/if}
                                                            {if $quyen==4 || $quyen==5} <!--end GD or PGD được chuyển cho phòng ban form-->
                                                            <p>
                                                                <select style="width:100%" name="phongchutri[{$vbc.qlvDetails_id}]" id="{$vbc.qlvDetails_id}" class="form-control phongchutri select2">
                                                                    <option value="">Chọn phòng chủ trì</option>
                                                                    {if !empty($phongban)}
                                                                        {foreach $phongban as $pb}
                                                                            <option value="{$pb.PK_iMaPB}" {($vbc.main_department==$pb.PK_iMaPB)?'selected':''}>{$pb.sTenPB}</option>
                                                                        {/foreach}
                                                                    {/if}
                                                                </select>
                                                            </p>
                                                            <p>
                                                                <select style="width:100%" name="phongphoihop[{$vbc.qlvDetails_id}][]" {($vbc.main_department)?'':'disabled'} multiple="" id="{$vbc.qlvDetails_id}" data-placeholder="Chọn phòng phối hợp" class="form-control phongphoihop select2">
                                                                    {if !empty($phongban)}
                                                                        {foreach $phongban as $pbph}
                                                                            <option value="{$pbph.PK_iMaPB}" 
                                                                                {if !empty($vbc.department_id)}
                                                                                    {$phoihop = explode(',',$vbc.department_id)}
                                                                                    {foreach $phoihop as $ph}
                                                                                        {if $ph==$pbph.PK_iMaPB}
                                                                                            selected
                                                                                        {/if}
                                                                                    {/foreach}
                                                                                {/if}
                                                                            >{$pbph.sTenPB}</option>
                                                                        {/foreach}
                                                                    {/if}
                                                                </select>
                                                            </p>
                                                            {/if}
                                                            {if $quyen==6 || $quyen==3 || $quyen==11 } <!--end TP được chuyển cho PTP form-->
                                                                {if $chucvu == 15} <!--end CCT form-->
                                                                <p>
                                                                    <select style="width:100%" name="chicucpho[{$vbc.qlvDetails_id}]" id="{$vbc.qlvDetails_id}" class="form-control chicucpho select2">
                                                                        <option value="">Chọn chi cục phó</option>
                                                                        {if !empty($dsCCP)}
                                                                            {foreach $dsCCP as $ccp}
                                                                            <option value="{$ccp.PK_iMaCB}">{$ccp.sHoTen}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                </p>
                                                                {else}
                                                                <p>
                                                                    <select style="width:100%" name="photruongphong[{$vbc.qlvDetails_id}]" id="{$vbc.qlvDetails_id}" class="form-control photruongphong select2">
                                                                        <option value="">Chọn phó trưởng phòng</option>
                                                                        {if !empty($dsPTP)}
                                                                            {foreach $dsPTP as $ptp}
                                                                                <option value="{$ptp.PK_iMaCB}">{$ptp.sHoTen}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                </p>
                                                                {/if}
                                                            {/if}
                                                            {if $quyen==6 || $quyen==7 || $quyen==3 || $quyen==10 || $quyen==11} <!--end TP được chuyển cho PTP form-->
                                                                {if $chucvu == 15 || $chucvu == 16 || $quyen==10} <!--end CCT || CCP form-->
                                                                <p>
                                                                    <select style="width:100%" name="phongchutriCC[{$vbc.qlvDetails_id}]" id="{$vbc.qlvDetails_id}" class="form-control phongchutriCC select2">
                                                                        <option value="">Chọn phòng chủ trì</option>
                                                                        {foreach $mangPB as $ctcc}
                                                                            <option value="{$ctcc.MaPhong}" {($vbc.FK_iMaPB_CC_CT==$ctcc.MaPhong)?'selected':''}>{$ctcc.TenPhong}</option>
                                                                        {/foreach}
                                                                    </select>
                                                                </p>
                                                                <p>
                                                                    <select style="width:100%" name="phongphoihopCC[{$vbc.qlvDetails_id}][]" {($vbc.FK_iMaPB_CC_CT)?'':'disabled'} multiple="" id="{$vbc.qlvDetails_id}" data-placeholder="Chọn phòng phối hợp" class="form-control phongphoihopCC select2">
                                                                        {if !empty($mangPB)}
                                                                            {foreach $mangPB as $ctph}
                                                                                <option value="{$ctph.MaPhong}" 
                                                                                    {if !empty($vbc.FK_iMaPB_CC_PH)}
                                                                                        {$phoihop = explode(',',$vbc.FK_iMaPB_CC_PH)}
                                                                                        {foreach $phoihop as $ph}
                                                                                            {if $ph==$ctph.MaPhong}
                                                                                                selected
                                                                                            {/if}
                                                                                        {/foreach}
                                                                                    {/if}
                                                                                >{$ctph.TenPhong}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                </p>
                                                                {else}
                                                                <p>
                                                                    <select style="width:100%" name="chuyenvien[{$vbc.qlvDetails_id}]" id="{$vbc.qlvDetails_id}" class="form-control chuyenvien select2">
                                                                        <option value="">Chọn chuyên viên</option>
                                                                        {if !empty($dsCV)}
                                                                            {foreach $dsCV as $cv}
                                                                                <option value="{$cv.PK_iMaCB}" {($vbc.user_chuyenvien==$cv.PK_iMaCB)?'selected':''}>{$cv.sHoTen}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                </p>
                                                                <p>
                                                                    <select style="width:100%" name="chuyenvienPH[{$vbc.qlvDetails_id}][]" {($vbc.user_chuyenvien)?'':'disabled'} multiple="" data-placeholder="Chọn chuyên viên phối hợp" id="{$vbc.qlvDetails_id}" class="form-control chuyenvienPH select2">
                                                                        {if !empty($dsCV)}
                                                                            {foreach $dsCV as $cvph}
                                                                                <option value="{$cvph.PK_iMaCB}" 
                                                                                    {if !empty($vbc.user_chuyenvien_phoihop)}
                                                                                        {$phoihopcv = explode(',',$vbc.user_chuyenvien_phoihop)}
                                                                                        {foreach $phoihopcv as $phcv}
                                                                                            {if $phcv==$cvph.PK_iMaCB}
                                                                                                selected
                                                                                            {/if}
                                                                                        {/foreach}
                                                                                    {/if}
                                                                                >{$cvph.sHoTen}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                </p>
                                                                {/if}
                                                            {/if}
                                                            {if $quyen==8}
                                                            <p>
                                                                Người chỉ đạo: {($vbc.user_photruongphong)?$mangCB[$vbc.user_photruongphong]:$mangCB[$vbc.user_truongphong]}
                                                            </p>
                                                            <p>
                                                                {if !empty($vbc.han_PTP)}
                                                                    Hạn xử lý: {date_select($vbc.han_PTP)}
                                                                {else}
                                                                    {if !empty($vbc.han_TP)}
                                                                    Hạn xử lý: {date_select($vbc.han_TP)}
                                                                    {/if}
                                                                {/if}
                                                            </p>
                                                            {elseif $quyen==5}
                                                            <p>
                                                                <input type="text" name="hanxuly[{$vbc.qlvDetails_id}]" value="{($vbc.han_GD)?date_select($vbc.han_GD):''}" class="form-control datepic datemask" placeholder="Hạn xử lý">
                                                            </p>
                                                            {elseif $quyen==6 || $quyen==3 || $quyen==11}
                                                                {if $chucvu ==3 || $chucvu ==15 || $chucvu == 6 || $chucvu == 7}
                                                                <p>
                                                                    <input type="text" name="hanxuly[{$vbc.qlvDetails_id}]" value="{($vbc.han_PGD)?date_select($vbc.han_PGD):(($vbc.han_GD)?date_select($vbc.han_GD):'')}" class="form-control datepic datemask" placeholder="Hạn xử lý">
                                                                </p>
                                                                {else}
                                                                <p>
                                                                    <input type="text" name="hanxuly[{$vbc.qlvDetails_id}]" value="{($vbc.Han_CCP!='0000-00-00')?date_select($vbc.Han_CCP):($vbc.Han_CCT)?date_select($vbc.Han_CCT):''}" class="form-control datepic datemask" placeholder="Hạn xử lý">
                                                                </p>
                                                                {/if}
                                                            {elseif $quyen==7}
                                                            <p>
                                                                <input type="text" name="hanxuly[{$vbc.qlvDetails_id}]" value="{($vbc.han_TP)?date_select($vbc.han_TP):''}" class="form-control datepic datemask" placeholder="Hạn xử lý">
                                                            </p>
                                                            {elseif $quyen==4}
                                                            <p>
                                                                <input type="text" name="hanxuly[{$vbc.qlvDetails_id}]" value="" class="form-control datepic datemask" placeholder="Hạn xử lý">
                                                            </p>
                                                            {elseif $quyen==10}
                                                            <p>
                                                                <input type="text" name="hanxuly[{$vbc.qlvDetails_id}]" value="{($vbc.Han_CCT!='0000-00-00')?date_select($vbc.Han_CCT):''}" class="form-control datepic datemask" placeholder="Hạn xử lý">
                                                            </p>
                                                            {/if}
                                                        </td>
                                                        <td width="32%">
                                                            {if $quyen==4}
                                                                <p><input type="text" name="noidungchuyen_PGD[{$vbc.qlvDetails_id}]" class="form-control namenoidungchuyen_PGD_{$vbc.qlvDetails_id}" id="" value="{if !empty($vbc.process_per)}Chuyển đồng chí {$mangPGD[$vbc.process_per]}{/if}"></p>
                                                            {/if}
                                                            {if $quyen==4 || $quyen==5}
                                                                <p><textarea name="noidungchuyen_phongban[{$vbc.qlvDetails_id}]" class="form-control noidungchuyen_phongban_{$vbc.qlvDetails_id}" id="" rows="5">{if !empty($vbc.main_department)}Chuyển phòng {$mangPB[$vbc.main_department]} phụ trách giải quyết{/if}{if !empty($vbc.noidung_chuyenphongban)}. {$vbc.noidung_chuyenphongban}{/if}</textarea></p>
                                                                <p><input type="text" class="form-control phongchutri_{$vbc.qlvDetails_id} hide" name="phongchutri_{$vbc.qlvDetails_id}" id="" value="{if !empty($vbc.main_department)}Chuyển phòng {$mangPB[$vbc.main_department]} phụ trách giải quyết{/if}"></p>
                                                                <p><textarea class="form-control phongphoihop_{$vbc.qlvDetails_id} hide" name="phongphoihop_{$vbc.qlvDetails_id}" id="" rows="5">{$vbc.noidung_chuyenphongban}</textarea></p>
                                                            {/if}
                                                            {if $quyen==6 || $quyen==3 || $quyen==11}
                                                                {if $chucvu == 15}
                                                                    <p><input class="form-control noidungchuyen_CCP_{$vbc.qlvDetails_id}" name="noidungchuyen_CCP{$vbc.qlvDetails_id}" id=""></p>
                                                                {else}
                                                                    <p><input class="form-control noidungchuyen_PTP_{$vbc.qlvDetails_id}" name="noidungchuyen_PTP{$vbc.qlvDetails_id}" id=""></p>
                                                                {/if}
                                                            {/if}
                                                            {if $quyen==6 || $quyen==7 || $quyen==3 || $quyen==10 || $quyen==11}
                                                                {if $chucvu == 15 || $chucvu == 16 || $quyen==10}
                                                                    <p><input class="form-control noidungchuyen_CCCT_{$vbc.qlvDetails_id}" name="noidungchuyen_CCCT{$vbc.qlvDetails_id}" value="{$vbc.text_chicucpho}" id=""></p>
                                                                    <p><textarea class="form-control noidungchuyen_CCPH_{$vbc.qlvDetails_id}" name="noidungchuyen_CCPH{$vbc.qlvDetails_id}" id="" rows="4">{$vbc.noidung_phoihop_chicuc}</textarea></p>
                                                                {else}
                                                                <p><input class="form-control noidungchuyen_CV_{$vbc.qlvDetails_id}" name="noidungchuyen_CV{$vbc.qlvDetails_id}" value="{$vbc.text_photruongphong}" id=""></p>
                                                                <p><textarea class="form-control noidungchuyen_CVPH_{$vbc.qlvDetails_id}" name="noidungchuyen_CVPH{$vbc.qlvDetails_id}" id="" rows="2">{$vbc.noidung_chuyenvien_phoihop}</textarea></p>
                                                                {/if}
                                                            {/if}
                                                            {if $quyen==8}
                                                            <p>
                                                                {($vbc.text_photruongphong)?$vbc.text_photruongphong:$vbc.text_truongphong}
                                                            </p>
                                                            {/if}
                                                        </td>
                                                        <td class="text-center {if $quyen==8}hide{/if}">
                                                            <button type="button" class="btn btn-primary btn-xs {($tralai)?'':'hide'} {($vbc.qlvDetails_active==9)?'hide':''}" data-toggle="modal" data-target="#exampleModal" name="tralai" value="{$vbc.qlvDetails_id}">Trả lại</button>
                                                            <input name="mavanbanchau[]" value="{$vbc.qlvDetails_id}" type="checkbox">
                                                            <input type="text" class="hide" name="vanbanong[{$vbc.qlvDetails_id}]" value="{$vbc.qlv_id}">
                                                            <input type="text" class="hide" name="vanbancha[{$vbc.qlvDetails_id}]" value="{$vbc.qlv_id_sub}">
                                                        </td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=tralai]',function(){
            var ma = $(this).val();
            $('button[name=guidi]').val(ma);
        });
        $(document).on('hide.bs.modal','.modal', function () {
        $("input[type=submit], textarea").val("");
    });
    });
</script>