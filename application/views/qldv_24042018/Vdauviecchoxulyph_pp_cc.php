<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc chờ xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="" method="get">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="nhiemky" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn nhiệm kỳ --</option>
                                        {if !empty($nhiemky)}
                                            {foreach $nhiemky as $nk}
                                                <option value="{$nk.sNhiemKy}" {($g_nhiemky)?($g_nhiemky==$nk.sNhiemKy)?'selected':'':''}>{$nk.sNhiemKy}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="khoa" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn khoa --</option>
                                        {if !empty($khoa)}
                                            {foreach $khoa as $k}
                                                <option value="{$k.sKhoa}" {($g_khoa)?($g_khoa==$k.sKhoa)?'selected':'':''}>{$k.sKhoa}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="kyhop" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn kỳ họp --</option>
                                        {if !empty($kyhop)}
                                            {foreach $kyhop as $kh}
                                                <option value="{$kh.sKyHop}" {($g_kyhop)?($g_kyhop==$kh.sKyHop)?'selected':'':''}>{$kh.sKyHop}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="tieude" class="form-control" value="{($g_tieude)?$g_tieude:''}" placeholder="Nhập tên phụ lục, cậu hỏi">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="noidung" id="" rows="3" class="form-control" placeholder="Nhập nội dung tìm kiếm">{($g_noidung)?$g_noidung:''}</textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="linhvuc" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lĩnh vực --</option>
                                        {if !empty($linhvuc)}
                                            {foreach $linhvuc as $lv}
                                                <option value="{$lv.linhVuc_id}" {($g_linhvuc)?($g_linhvuc==$lv.linhVuc_id)?'selected':'':''}>{$lv.linhVuc_name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="phongban" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn phòng chủ trì --</option>
                                        {if !empty($phongban)}
                                            {foreach $phongban as $pb}
                                                <option value="{$pb.PK_iMaPB}" {($g_phongban)?($g_phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="lanhdao" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lãnh đạo --</option>
                                        {if !empty($lanhdao)}
                                            {foreach $lanhdao as $ld}
                                                <option value="{$ld.PK_iMaCB}" {($g_lanhdao)?($g_lanhdao==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-success btn-xs" name="timkiem" value="timkiem">Tìm kiếm</button>
                                </div>
                            </div>
                    </form>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Nội dung trích yếu</th>
                                    <th width="30%">Ý kiến</th>
                                    <th width="30%">Chỉ đạo</th>
                                    <th style="width:5%" class=""><button type="submit" name="duyet" value="duyet" class="btn btn-success btn-xs">Duyệt</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieuOng)}{$ii=1}
                                    {foreach $dulieuOng as $dlo}
                                        <tr>
                                            <th class="text-center">{intToRoman($ii++)}</th>
                                            <th colspan="4">{$dlo.qlv_noidung} {if !empty($dlo.qlvFile_path)} <a href="{$url}{$dlo.qlvFile_path}" target="_blank">Xem File</a>{/if}</th>
                                        </tr>
                                        {if !empty($dulieu)} {$i=1}
                                            {foreach $dulieu as $dl}
                                                {if $dlo.qlv_id == $dl.qlv_id}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td> 
                                                        <a href="{$url}chitietdauviecphoihop/{$dl.qlvDetails_id}" class="tin1"><b>{$dl.qlvDetails_desc}</b></a>
                                                        <p>Người nhập: {$mangCB[$dl.input_per]}</p>
                                                        <p>Ngày nhập: {date_select($dl.qlvDetails_date)}</p>
                                                        <p>Hạn văn bản: {($dl.qlvDetails_limit_time>'1970-01-01')?date_select($dl.qlvDetails_limit_time):''}</p>
                                                        <p><span style="color:red;">*</span> <i>Nội dung chỉ đạo của đ/c </i><b>{$mangCB[$dl.FK_iMaCB_Gui]}:</b></p>
                                                        <p><i>{$dl.sNoiDungChuyen}</i></p>
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <input type="text" name="chuyenvienph_{$dl.qlvDetails_id}" value=""  class="hide">
                                                            <button type="button" name="chuyenvienphoihop" value="{$dl.qlvDetails_id}" data-toggle="modal" data-target="#myModalchuyenvien" class="btn btn-success btn-sm chuyenvienph_{$dl.qlvDetails_id}"><b>Chọn chuyên viên phối hợp</b></button>
                                                        </p>
                                                        <p>
                                                            <input type="text" name="hanxuly_{$dl.qlvDetails_id}" value="{($dl.sHanXuLy>'1970-01-01')?date_select($dl.sHanXuLy):''}" placeholder="hạn xử lý" class="form-control">
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <textarea name="tenchuyenvienph_{$dl.qlvDetails_id}" id="" class="form-control" rows="8" placeholder="Nội dung chuyển chuyên viên phối hợp"></textarea>
                                                        </p>
                                                    </td>
                                                    <td class="text-center">
                                                        <span style="color:red;">Chọn duyệt</span>
                                                        <input type="checkbox" name="mavanban[]" class="mavanban_{$dl.qlvDetails_id}" disabled value="{$dl.qlvDetails_id}">
                                                    </td>
                                                </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="modal fade" id="myModalchuyenvien" tabindex="-1" role="dialog" aria-labelledby="myModalLabelchuyenvien">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Phó phòng phối hợp</h4>
                            </div>
                            <div>
                            {if !empty($chuyenvien)}
                                {foreach $chuyenvien as $cv}
                                    <div class="col-md-6">
                                        <input type="checkbox" class="Checkboxcv" data-id="{$cv.sHoTen}" name="chuyenvienphoihop" value="{$cv.PK_iMaCB}"> {$cv.sHoTen}
                                    </div>
                                {/foreach}
                            {/if}
                            </div>
                            <div class="modal-footer">
                                <input type="text" name="ma-cv" class="hide">
                                <button type="button" name="ghilaichuyenvien" class="btn btn-primary btn-sm" data-dismiss="modal">Ghi Lại</button>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=chuyenvienphoihop]',function(){
            var mavanban = $(this).val();
            $('input[name=ma-cv]').val(mavanban);
        });
        $(document).on('click','button[name=ghilaichuyenvien]',function () {
            var tencv = $('.Checkboxcv:checked').map(function() {
                return $(this).attr('data-id');
            }).get().join(',');
            var macv = $('.Checkboxcv:checked').map(function() {
                return this.value;
            }).get().join(',');
            var mavanban = $('input[name=ma-cv]').val();
            $('input[name=chuyenvienph_'+mavanban+']').val(macv);
            if(macv.length>0)
            {
                $('textarea[name=tenchuyenvienph_'+mavanban+']').val(tencv+' phối hợp giải quyết');
                $('.mavanban_'+mavanban+'').removeAttr('disabled');
            }
            else{
                $('input[name=chuyenvienph_'+mavanban+']').val('');
                $('textarea[name=tenchuyenvienph_'+mavanban+']').val('');
                $('.mavanban_'+mavanban+'').attr("disabled", 'disabled');
                $('.mavanban_'+mavanban+'').attr('checked', false);
            }
            $('.Checkboxcv').attr('checked', false); 
        });
    });
</script>