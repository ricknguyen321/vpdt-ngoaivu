<script src="{$url}assets/js/qldv/addqldvDetails.js"></script>
<script src="{$url}assets/js/qldv/qldv.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Chỉnh sửa đầu việc con
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$thongtin[0]['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($thongtin[0]['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($thongtin[0]['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b> <a href="{($file_dk)?$file_dk[0]['qlvFile_path']:''}">{($file_dk)?'[Xem File]':''}</a></span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$thongtin[0]['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($thongtin[0]['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$thongtin[0]['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$thongtin[0]['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$thongtin[0]['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($thongtin[0]['qlv_date_nhap'])}</span>
                            </div>
                        </div>

                        <div class="col-sm-12" style="margin-top: -30px;">
                                <hr style="border: 1px dashed #ddd;">
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Tiêu đề đầu việc*</label>

                                <div class="col-sm-10">
                                    <textarea tabindex="1" name="qlvDetails_desc" id="qlvDetails_desc" required class="form-control" rows="2">{$qldvDetail[0]['qlvDetails_desc']}</textarea>
                                </div>
                            </div>
                        </div> 
                        <div class="col-sm-12" style="margin-top: -20px;">
                            <hr style="border: 1px dashed #ccc">
                        </div>
                            
                            <div class="col-md-12">
                                <button type="submit" name="luulai" value="luulai" class="btn btn-primary">Cập nhật</button> <a href="javascript: history.back(1)" class="btn btn-default" data-original-title="" title="">Quay lại &gt;&gt;</a> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

