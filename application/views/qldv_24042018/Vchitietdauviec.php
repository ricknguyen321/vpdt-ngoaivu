<script src="{$url}assets/js/qldv/dauviec.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Chi tiết đầu việc
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$thongtin[0]['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($thongtin[0]['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($thongtin[0]['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b> <a href="{($file_dk)?$file_dk[0]['qlvFile_path']:''}">{($file_dk)?'[Xem File]':''}</a></span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$thongtin[0]['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($thongtin[0]['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$thongtin[0]['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$thongtin[0]['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$thongtin[0]['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($thongtin[0]['qlv_date_nhap'])}</span>
                            </div>
                        </div>

                        <div class="col-sm-12" style="margin-top: -30px;">
                                <hr style="border:1px dashed #5d5757">
                        </div>
                        <!-- {$i=0} {$dem=0} -->
                        {foreach $qldvDetail as $dv}
                        <!-- {$i++} {$dem++} -->
                        {if $dem>1}
                        <div class="col-sm-12" style="margin-top: -20px;">
                            <hr style="border: 1px dashed #5d5757">
                        </div>
                        {/if}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-2 text-primary"> Tiêu đề đầu việc</label>

                                <div class="col-sm-10"> 
                                    {$dv.qlvDetails_desc}
                                </div>
                            </div>
                        </div> 
                        <!-- {$j=0} -->
                        {foreach $qldvDetail1 as $dv1}
                        

                        {if $dv.qlvDetails_id == $dv1.qlv_id_sub}
                        <!-- {$j++} -->
                            <div class="col-sm-12" style="margin-top: -30px;">
                                <hr style="border:1px dashed #c5dcef">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2"> Nội dung đầu việc </label>

                                    <div class="col-sm-10">
                                        {$dv1.qlvDetails_desc}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Tiêu đề việc</label>
                                    <div class="col-sm-8">
                                        {$dv1.sTieuDe}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Lãnh đạo chỉ đạo</label>
                                    <div class="col-sm-8">
                                        {if isset($manglanhdao[$dv1.FK_iMaCB_LanhDao])}{$manglanhdao[$dv1.FK_iMaCB_LanhDao]}{/if}
                                    </div>
                                </div>
                            </div>
                            {if !empty({$dv1.sNoiDungPhanCong})}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2" required>Phân công trả lời của TP </label>
                                    <div class="col-sm-8">
                                        {$dv1.sNoiDungPhanCong} <i>{if !empty($dv1.sCuTri)}({$dv1.sCuTri}){/if}</i>
                                    </div>
                                </div>
                            </div>
                            {/if}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4" required> Đơn vị chủ trì</label>

                                    <div class="col-sm-8 ">
                                        {if !empty($dv1.main_department)}
                                           {$mangPB[$dv1.main_department]}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4"> Hạn xử lý</label>
                                    <div class="col-sm-8">
                                        {($dv1.qlvDetails_limit_time&&$dv1.qlvDetails_limit_time>'1970-01-01')?date_select($dv1.qlvDetails_limit_time):''}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2" required> Đơn vị phối hợp </label>

                                    <div class="col-sm-9 ">
                                        {if !empty($dv1.department_id)}<!-- {$pp=0} -->
                                        {$mang = explode(',',$dv1.department_id)}
                                            {foreach $mang as $m}
                                                {$mangPB[$m]}, 
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                            
                            {/if}
                            {/foreach}
                            {/foreach}
                    </div>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <h4>Trình tự giải quyết:</h4>
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width:5%" class="text-center">STT</th>
                                <th width="15%">Thời gian chuyển</th>
                                <th width="15%">Người chuyển</th>
                                <th width="35%">Nội dung chuyển</th>
                                <th width="15%">Người nhận</th>
                                <th class="text-center" width="15%">Hạn xử lý</th>
                            </tr>
                        </thead>
                        <tbody>
                            {if !empty($dschuyennhan)}{$i=1}
                                {foreach $dschuyennhan as $dl}
                                    <tr class="success">
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{date_time($dl.sThoiGian)}</td>
                                        <td >{$mangCB[$dl.FK_iMaCB_Gui]}</td>
                                        <td >{$dl.sNoiDungChuyen}</td>
                                        <td >{$mangCB[$dl.FK_iMaCB_Nhan]}</td>
                                        <td class="text-center">
                                            {if $dl.sHanXuLy > '1970-01-01'}{date_select($dl.sHanXuLy)}{/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <h4>Kết quả giải quyết:</h4>
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width:5%" class="text-center">STT</th>
                                <th width="40%">Nội dung</th>
                                <th width="15%">Cán bộ giải quyết</th>
                                <th width="15%">Thời gian giải quyết</th>
                                <th width="15%">Tệp tin</th>
                            </tr>
                        </thead>
                        <tbody>
                            {if !empty($ketquachinh)}{$i=1}
                                {foreach $ketquachinh as $kq}
                                    <tr class="success">
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$kq.qlvFile_desc}</td>
                                        <td >{$kq.sHoTen}</td>
                                        <td >{date_time($kq.qlvFile_date)}</td>
                                        <td >{if !empty($kq.qlvFile_path)} <a href="{$kq.qlvFile_path}" class="tin">[Tải File]</a>{/if}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <h4>Kết quả phối hợp:</h4>
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width:5%" class="text-center">STT</th>
                                <th width="20%">Nội dung</th>
                                <th width="20%">Cán bộ + đơn vị</th>
                                <th width="15%">Thời gian giải quyết</th>
                                <th width="15%">Tệp tin</th>
                            </tr>
                        </thead>
                        <tbody>
                            {if !empty($ketquaphoihop)}{$i=1}
                                {foreach $ketquaphoihop as $kq}
                                    <tr class="success">
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$kq.qlvFile_desc}</td>
                                        <td >
                                            <p>{$kq.sHoTen}</p>
                                            <p>{$kq.sTenPB}</p>
                                        </td>
                                        <td >{date_time($kq.qlvFile_date)}</td>
                                        <td >{if !empty($kq.qlvFile_path)} <a href="{$kq.qlvFile_path}" class="tin">[Tải File]</a>{/if}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                        </tbody>
                    </table>
                </div>
                <div class="row {$anhien}">
                    <form action="" method="post" enctype='multipart/form-data'>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea required name="noidunggiaiquyet" class="form-control" id=""rows="4" placeholder="Nhập nội dung hoàn thành công việc tại đây"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="file" name="file" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" value="hoanthanh" name="hoanthanh">Hoàn thành</button>
                            <a href="javascript: history.back(1)" class="btn btn-default" data-original-title="" title="">Quay lại &gt;&gt;</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>