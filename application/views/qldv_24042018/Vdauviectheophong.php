<div class="content-wrapper">
    <!-- Main content -->
    <section class="content" style="font-size:13px;">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:0px!importent;">
                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Ngày ban hành:</label>
                                        <div class="col-md-4">
                                            <input type="text" name="ngaybanhanh_tu" placeholder="Từ" class="form-control datepic datemask">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" name="ngaybanhanh_den"placeholder="Đến"  class="form-control datepic datemask">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Hạn xử lý:</label>
                                        <div class="col-md-4">
                                            <input type="text" name="hanxuly_tu" placeholder="Từ" class="form-control datepic datemask">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" name="hanxuly_den" placeholder="Đến" class="form-control datepic datemask">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button class="btn btn-primary" value="timkiem" name="timkiem">Lọc</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th rowspan="3" style="width:5%" class="text-center">STT</th>
                                <th rowspan="3" class="text-center" width="30%">Phòng ban</th>
                                <th colspan="5" class="text-center">Tình hình thực hiện nhiệm vụ theo CV chỉ đạo, TBKL</th>
                            </tr>
                            <tr>
                                <th rowspan="2" class="text-center">Tổng số đầu việc</th>
                                <th colspan="2" class="text-center">Đang thực hiện</th>
                                <th colspan="2" class="text-center">Đã hoàn thành</th>
                            </tr>
                            <tr>
                                <th class="text-center" width="12%">Trong hạn</th>
                                <th class="text-center" width="12%">Quá hạn</th>
                                <th class="text-center" width="12%">Trong hạn</th>
                                <th class="text-center" width="12%">Quá hạn</th>
                            </tr>
                        </thead>
                        <tbody>
                            {if !empty($phongban)}{$i=1}
                                <tr>
                                    <th class="text-center" colspan="2">Tổng cộng</th>
                                    <th class="text-center">{if isset($tongdauviec)} <a href="" class="tin">{$tongdauviec}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($thuchien_dunghan)} <a href="" class="tin">{$thuchien_dunghan}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($thuchien_quahan)} <a href="" class="tin">{$thuchien_quahan}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($hoanthanh_dunghan)} <a href="" class="tin">{$hoanthanh_dunghan}</a>{else}0{/if}</th>
                                    <th class="text-center">{if isset($thuchien_quahan)} <a href="" class="tin">{$thuchien_quahan}</a>{else}0{/if}</th>
                                </tr>
                                {foreach $phongban as $pb}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <th >{$pb.sTenPB}</th>
                                        <td class="text-center">{if isset($mang_tongdauviec[$pb.PK_iMaPB])} <a href="" class="tin">{$mang_tongdauviec[$pb.PK_iMaPB]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_thuchien_dunghan[$pb.PK_iMaPB])} <a href="" class="tin">{$mang_thuchien_dunghan[$pb.PK_iMaPB]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_thuchien_quahan[$pb.PK_iMaPB])} <a href="" class="tin">{$mang_thuchien_quahan[$pb.PK_iMaPB]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_hoanthanh_dunghan[$pb.PK_iMaPB])} <a href="" class="tin">{$mang_hoanthanh_dunghan[$pb.PK_iMaPB]}</a>{else}0{/if}</td>
                                        <td class="text-center">{if isset($mang_hoanthanh_quahan[$pb.PK_iMaPB])} <a href="" class="tin">{$mang_hoanthanh_quahan[$pb.PK_iMaPB]}</a>{else}0{/if}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>