<script src="{$url}assets/js/qldv/dauviec.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc chờ duyệt
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Nội dung trích yếu</th>
                                    <th width="30%">Lãnh đạo chủ trì</th>
                                    <th width="30%">Ý kiến của trưởng phòng</th>
                                </tr>
                            </thead>
                            <tbody>
                               {if !empty($dulieu)} {$i=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td> 
                                                <a href="{$url}editDetailSub/{$dl.qlvDetails_id}" class="tin1"><b>{$dl.qlvDetails_desc}</b></a>
                                                <p>Người nhập: {$mangCB[$dl.input_per]}</p>
                                                <p>Ngày nhập: {date_select($dl.qlvDetails_date)}</p>
                                                <p>Hạn văn bản: {($dl.qlvDetails_limit_time>'1970-01-01')?date_select($dl.qlvDetails_limit_time):''}</p>
                                            </td>
                                            <td>
                                                <p>Lãnh đạo: {if isset($lanhdao[$dl.FK_iMaCB_LanhDao])}{$lanhdao[$dl.FK_iMaCB_LanhDao]}{/if}</p>
                                                <p>Phòng chủ trì: {$mangPB[$dl.main_department]}</p>
                                                {if !empty($dl.department_id)}
                                                    {$mangph = explode(',',$dl.department_id)}
                                                    <p>Phòng phối hợp: 
                                                        {foreach $mangph as $ph}
                                                            {$mangPB[$ph]},
                                                        {/foreach}
                                                    </p>
                                                {/if}
                                            </td>
                                            <td>
                                                <p>{$dl.noidungtralai}</p>
                                            </td>
                                        </tr>
                                    {/foreach}
                               {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change','.noidungtralai', function(){
            var noidung = $(this).val();
            var ma = $(this).attr('id');
            if(noidung.length>0)
            {
                $('#ma_'+ma).attr('type','submit');
            }
            else{
                $('#ma_'+ma).attr('type','button');
            }
        });
    });
</script>