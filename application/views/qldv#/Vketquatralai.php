<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Kết quả hoàn thành
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <form  method="post" action="" class="form-horizontal">
                        <div class="col-md-12">
                            <table id="" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width:5%" class="text-center">STT</th>
                                        <th width="40%">Nội dung</th>
                                        <th width="15%">Tiêu đề</th>
                                        <th width="10%">File cũ</th>
                                        <th width="15%">Thời gian</th>
                                        <th width="5%">Tải file</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {if !empty($dulieu)}{$i=1}
                                        {foreach $dulieu as $dl}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td><a class="tin1" href="javascript:void(0)">{$dl.qlvDetails_desc}</a></td>
                                                <td class="text-center">{$dl.sTieuDe}</td>
                                                <td class="text-center"><a class="tin1" target="_blank" href="{$url}{$dl.qlvFile_path}">[Xem file]</a></td>
                                                <td class="text-center">{date_time($dl.qlvFile_date)}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-default btn-sm" name="mafile" value="{$dl.qlvFile_id}" data-toggle="modal" data-target="#myModal"><i class="fa fa-search-plus"></i></button>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    {/if}
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <form method="post" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Tải lại file</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="file" name="files[]" required readonly="" class="form-control" id="">
                                        <input type="text" name="mafile_an" class="hide" value="">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-default btn-sm" data-dismiss="modal">Đóng</button>
                                        <button type="submit" class="btn btn-primary btn-sm" name="luudulieu" value="luudulieu">Tải file</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=mafile]',function(){
            var mafile = $(this).val();
            $('input[name=mafile_an]').val(mafile);
        });
    });
</script>

