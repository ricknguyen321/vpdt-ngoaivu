<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel">Văn bản trả lại</h4>
                                </div>
                                <form method="post">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="control-label">Người trả:</label>
                                            <input readonly="" type="text" value="{$vanban['sHoTen']}" class="form-control" name="nguoitra">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Nội dung:</label>
                                            <textarea required minlength="7" class="form-control" name="noidung"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                        <button type="submit" name="guidi" value="" class="btn btn-primary">Gửi đi</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="45%">Trích yếu</th>
                                    <th width="45%">Nội dung chuyển</th>
                                    <th style="width:5%" class="text-center {($tralai)?'':'hide'}">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbanong)}{$i=1}
                                    {foreach $vanbanong as $vb}
                                        <tr style="background:#e4e4e4">
                                            <th class="text-center">{intToRoman($i++)}</td>
                                            <th colspan="{($tralai)?3:2}">
                                                {$vb.qlv_noidung}
                                                {if $vb.qlv_file==1}
                                                    <a class="tin" href="{$vb.qlvFile_path}">[Xem File]</a>
                                                {/if}
                                            </td>
                                        </tr>
                                        {if !empty($vanbancon)}{$j=1}
                                            {foreach $vanbancon as $vbc}
                                                {if $vbc.qlv_id==$vb.qlv_id}
                                                    <tr>
                                                        <td class="text-center">{$j++}</td>
                                                        <td>
                                                            <p><a href="{$url}chitietdauviec/{$vbc.qlvDetails_id}" class="tin">{$vbc.qlvDetails_desc}</a></p>
                                                            <p>- Người nhập:{$vbc.sHoTen}</p>
                                                            <p>- Ngày nhập:{date_time($vbc.qlvDetails_date)}</p>
                                                            <p>- Hạn văn bản:{date_select($vbc.qlvDetails_limit_time)}</p>
                                                        </td>
                                                        <td>
                                                            {if !empty($vbc.coordinate_per_text)}
                                                                <p>{$vbc.coordinate_per_text}</p>
                                                            {/if}
                                                            {if !empty($vbc.process_per_text)}
                                                                <hr style="border-bottom: 1px dashed gold; margin: 5px; border-top:0px">
                                                                <p>{$vbc.process_per_text}</p>
                                                            {/if}
                                                            {if !empty($vbc.text_chicuctruong)}
                                                                <hr style="border-bottom: 1px dashed gold; margin: 5px; border-top:0px">
                                                                <p>{$vbc.text_chicuctruong}</p>
                                                            {/if}
                                                            {if !empty($vbc.text_chicucpho)}
                                                                <hr style="border-bottom: 1px dashed gold; margin: 5px; border-top:0px">
                                                                <p>{$vbc.text_chicucpho}</p>
                                                            {/if}
                                                            {if !empty($vbc.text_truongphong)}
                                                                <hr style="border-bottom: 1px dashed gold; margin: 5px; border-top:0px">
                                                                <p>{$vbc.text_truongphong}</p>
                                                            {/if}
                                                            {if !empty($vbc.text_photruongphong) && !empty($vbc.user_photruongphong)}
                                                                <hr style="border-bottom: 1px dashed gold; margin: 5px; border-top:0px">
                                                                <p>{$vbc.text_photruongphong}</p>
                                                            {/if}
                                                        </td>
                                                        <td class="{($tralai)?'':'hide'} {($vbc.qlvDetails_active==9)?'hide':''}"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#exampleModal" name="tralai" value="{$vbc.qlvDetails_id}">Trả lại</button></td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=tralai]',function(){
            var ma = $(this).val();
            $('button[name=guidi]').val(ma);
        });
        $(document).on('hide.bs.modal','.modal', function () {
        $("input[type=submit], textarea").val("");
    });
    });
</script>
