<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Đầu việc chi tiết
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
       
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form  method="post" action="" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Số ký hiệu:</b> {$thongtin[0]['qlv_code']}</span>
                                <span for="" class="col-sm-6"><b>Loại VB: </b>
                                    {foreach $dsloaivanban as $l}
                                        {($thongtin[0]['qlv_loai']==$l.PK_iMaLVB)?$l.sTenLVB:''}</option>
                                    {/foreach}
                                </span>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày văn bản:</b> {date_select($thongtin[0]['qlv_date'])}</span>

                                <span for="" class="col-sm-6"><b>file văn bản:</b> <a href="{($file_dk)?$file_dk[0]['qlvFile_path']:''}">{($file_dk)?'[Xem File]':''}</a></span>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Nội dung:</b> {$thongtin[0]['qlv_noidung']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 {($thongtin[0]['thongbao_ketluan'])?'':'hide'}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span for="" class="control-label"><b>Kết luận:</b> {$thongtin[0]['thongbao_ketluan']} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Lãnh đạo chủ trì:</b> {$thongtin[0]['qlv_ld_chutri']}</span>
                                <span for="" class="col-sm-6"><b>Người ký:</b> {$thongtin[0]['qlv_ld_ky']}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span for="" class="col-sm-6"><b>Ngày nhập:</b> {date_select($thongtin[0]['qlv_date_nhap'])}</span>
                                <span for="" class="col-sm-6">
                                <a href="{$url}addqldv/{$thongtin[0]['qlv_id']}" class="btn btn-primary btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Chỉnh sửa</a> 
                                <a href="{$url}addqldvDetails/{$thongtin[0]['qlv_id']}" class="btn btn-primary btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Thêm mới</a>
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-12" style="margin-top: -10px;">
                                <hr style="border:1px dashed #5d5757">
                        </div>
                        <!-- {$i=0} {$dem=0} -->
                        {foreach $qldvDetail as $dv}
                        <!-- {$i++} {$dem++} -->
                        {if $dem>1}
                        <div class="col-sm-12" style="margin-top: -10px;">
                            <hr style="border: 1px dashed #5d5757">
                        </div>
                        {/if}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-2 text-primary">{$i}. Tiêu đề đầu việc</label>

                                <div class="col-sm-10">
                                    {$dv.qlvDetails_desc}
                                </div>
                            </div>
                        </div> 
                        
                        <div class="col-md-12">   
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <a href="{$url}editDetail/{$dv.qlvDetails_id}" class="btn btn-primary btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Chỉnh sửa</a>
                                    <a href="{$url}addDetailSub/{$dv.qlvDetails_id}" class="btn btn-primary btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Thêm mới</a>
                                </div>
                            </div>
                        </div>
                            
                        <!-- {$j=0} -->
                        {foreach $qldvDetail1 as $dv1}
                        

                        {if $dv.qlvDetails_id == $dv1.qlv_id_sub}
                        <!-- {$j++} -->
                            <div class="col-sm-12" style="margin-top: -10px;">
                                <hr style="border:1px dashed #c5dcef">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2">{$i}.{$j} Nội dung đầu việc </label>

                                    <div class="col-sm-10">
                                        {$dv1.qlvDetails_desc}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">{$i}.{$j} Tiêu đề việc</label>
                                    <div class="col-sm-8">
                                        {$dv1.sTieuDe}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">{$i}.{$j} Lãnh đạo chỉ đạo</label>
                                    <div class="col-sm-8">
                                        {if isset($manglanhdao[$dv1.FK_iMaCB_LanhDao])}{$manglanhdao[$dv1.FK_iMaCB_LanhDao]}{/if}
                                    </div>
                                </div>
                            </div>
                            {if !empty({$dv1.sNoiDungPhanCong})}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2" required>{$i}.{$j} Phân công trả lời của TP </label>
                                    <div class="col-sm-8">
                                        {$dv1.sNoiDungPhanCong} <i>{if !empty($dv1.sCuTri)}({$dv1.sCuTri}){/if}</i>
                                    </div>
                                </div>
                            </div>
                            {/if}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4" required>{$i}.{$j} Đơn vị chủ trì</label>

                                    <div class="col-sm-8 ">
                                        {if !empty($main_depart)}
                                            {foreach $main_depart as $p}
                                                {if $dv1.main_department==$p.PK_iMaPB}
                                                    {$p.sTenPB}
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">{$i}.{$j} Hạn xử lý</label>
                                    <div class="col-sm-8">
                                        {($dv1.qlvDetails_limit_time&&$dv1.qlvDetails_limit_time>'1970-01-01')?date_select($dv1.qlvDetails_limit_time):''}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2" required>{$i}.{$j} Đơn vị phối hợp </label>

                                    <div class="col-sm-9 ">
                                        {if !empty($main_depart)}
                                        {if !empty($dv1.department_id)}<!-- {$pp=0} -->
                                        {$mang = explode(',',$dv1.department_id)}
                                            {foreach $main_depart as $p}
                                                {foreach $mang as $m}
                                                
                                                    {if $m==$p.PK_iMaPB}
                                                    <!-- {$pp++} -->
                                                        {($pp>1)?'-':''} {$p.sTenPB}
                                                    {/if}
                                                {/foreach}
                                            {/foreach}
                                        {/if}
                                        {/if}
                                    </div>
                                    <div class="sm-1">
                                        <a href="{$url}editDetailSub/{$dv1.qlvDetails_id}" class="btn btn-info btn-sm {($thongtin[0]['input_per']!=$vanban['PK_iMaCB'])?'hide':''}">Chỉnh sửa</a>
                                    </div>
                                </div>
                            </div>
                            
                            {/if}
                            {/foreach}
                          
                            {/foreach}
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

