<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách tài liệu
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="" method="get">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="nhiemky" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn nhiệm kỳ --</option>
                                        {if !empty($nhiemky)}
                                            {foreach $nhiemky as $nk}
                                                <option value="{$nk.sNhiemKy}" {($g_nhiemky)?($g_nhiemky==$nk.sNhiemKy)?'selected':'':''}>{$nk.sNhiemKy}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="khoa" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn khoa --</option>
                                        {if !empty($khoa)}
                                            {foreach $khoa as $k}
                                                <option value="{$k.sKhoa}" {($g_khoa)?($g_khoa==$k.sKhoa)?'selected':'':''}>{$k.sKhoa}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="kyhop" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn kỳ họp --</option>
                                        {if !empty($kyhop)}
                                            {foreach $kyhop as $kh}
                                                <option value="{$kh.sKyHop}" {($g_kyhop)?($g_kyhop==$kh.sKyHop)?'selected':'':''}>{$kh.sKyHop}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="tieude" class="form-control" value="{($g_tieude)?$g_tieude:''}" placeholder="Nhập tên phụ lục">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="noidung" id="" rows="3" class="form-control" placeholder="Nhập nội dung tìm kiếm">{($g_noidung)?$g_noidung:''}</textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="linhvuc" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lĩnh vực --</option>
                                        {if !empty($linhvuc)}
                                            {foreach $linhvuc as $lv}
                                                <option value="{$lv.linhVuc_id}" {($g_linhvuc)?($g_linhvuc==$lv.linhVuc_id)?'selected':'':''}>{$lv.linhVuc_name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="phongban" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn phòng chủ trì --</option>
                                        {if !empty($phongban)}
                                            {foreach $phongban as $pb}
                                                <option value="{$pb.PK_iMaPB}" {($g_phongban)?($g_phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="lanhdao" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn lãnh đạo --</option>
                                        {if !empty($lanhdao)}
                                            {foreach $lanhdao as $ld}
                                                <option value="{$ld.PK_iMaCB}" {($g_lanhdao)?($g_lanhdao==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-success btn-xs" name="timkiem" value="timkiem">Tìm kiếm</button>
                                </div>
                            </div>
                    </form>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="40%">Nội dung</th>
                                    <th width="10%" class="text-center">Tên phụ lục</th>
                                    <th width="10% class="text-center">Phòng chủ trì</th>
                                    <th width="14% class="text-center">phòng phối hợp</th>
                                    <th width="16%" class="text-center">Lãnh đạo sở chỉ đạo</th>
                                    <th width="5%" class="text-center hide">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieu)}{$i=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td >{$dl.qlvDetails_desc}
                                                {if !empty($dl.dsfile)}
                                                <!-- {$d=0} -->
                                                    {foreach $dl.dsfile as $f}
                                                    <!-- {$d++} -->
                                                        {($d>1)?', ':''}<a target="_blank" href="{$url}{$f.qlvFile_path}" class="tin1">[Xem File]</a>
                                                    {/foreach}
                                                {/if}
                                            </td>
                                            <td class="text-center">{$dl.sTieuDe}</td>
                                            <td class="text-center">{if isset($mangphong[$dl.main_department])}{$mangphong[$dl.main_department]}{/if}</td>
                                            <td class="text-center">
                                                {if !empty($dl.department_id)}
                                                    <!-- {$dem=0} -->
                                                    {$phoihop = explode(',',$dl.department_id)}
                                                    {foreach $phoihop as $ph}
                                                    <!-- {$dem++} -->
                                                        {($dem>1)?', ':''}{$mangphong[$ph]}
                                                    {/foreach}
                                                {/if}
                                            </td>
                                            <td class="text-center">{if isset($manglanhdao[$dl.FK_iMaCB_LanhDao])}{$manglanhdao[$dl.FK_iMaCB_LanhDao]}{/if}</td>
                                            <td class="text-center hide"><button class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">
                        {$phantrang}
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.control-label').css('text-align', 'left');
    });
</script>