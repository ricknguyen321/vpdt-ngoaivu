<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="30%">Nội dung hoàn thành</th>
                                    <th width="27%">Nội dung đánh giá</th>
                                    <th style="width:8%" class="text-center"><button class="btn btn-primary btn-xs" name="danhgia" value="danhgia">Đánh giá</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbanong)}{$i=1}
                                    {foreach $vanbanong as $vb}
                                        <tr style="background:#e4e4e4">
                                            <th class="text-center">{intToRoman($i++)}</td>
                                            <th colspan="4">
                                                {$vb.qlv_noidung}
                                                {if $vb.qlv_file==1}
                                                    <a class="tin" href="{$vb.qlvFile_path}">[Xem File]</a>
                                                {/if}
                                            </td>
                                        </tr>
                                        {if !empty($vanbancon)}{$j=1}
                                            {foreach $vanbancon as $vbc}
                                                {if $vbc.qlv_id==$vb.qlv_id}
                                                    <tr>
                                                        <td class="text-center">{$j++}</td>
                                                        <td>
                                                            <p><a href="{$url}chitietdauviec/{$vbc.qlvDetails_id}" class="tin">{$vbc.qlvDetails_desc}</a></p>
                                                            <p>- Người nhập:{$mangCB[$vbc.input_per]}</p>
                                                            <p>- Ngày nhập:{date_time($vbc.qlvDetails_date)}</p>
                                                            <p>- Hạn văn bản:{date_select($vbc.qlvDetails_limit_time)}</p>
                                                        </td>
                                                        <td>
                                                            <p class="text-primary">- {$vbc.qlvDetails_traloi}</p>
                                                            <p>- Người hoàn thành:{$mangCB[$vbc.qlvDetails_user_traloi]}</p>
                                                            <p>- Ngày hoàn thành:{date_time($vbc.date_traloi)}</p>
                                                            <p>- File hoàn thành:{if !empty($vbc.qlvFile_path)} <a download href="{$vbc.qlvFile_path}" class="tin">[Tải File]</a>{/if}</p>
                                                        </td>
                                                        <td>
                                                            <textarea name="noidungdanhgia[{$vbc.qlvDetails_id}]" id="" placeholder="Nếu không ghi gì thì nội dung đánh giá là: Hoàn thành công việc" class="form-control" rows="5"></textarea>
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="checkbox" name="mavanban[]" value="{$vbc.qlvDetails_id}">
                                                        </td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>