<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách đầu việc văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="35%">Nội dung hoàn thành</th>
                                    <th width="30%">Nội dung đánh giá</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbanong)}{$i=1}
                                    {foreach $vanbanong as $vb}
                                        <tr style="background:#e4e4e4">
                                            <th class="text-center">{intToRoman($i++)}</td>
                                            <th colspan="3">
                                                {$vb.qlv_noidung}
                                                {if $vb.qlv_file==1}
                                                    <a class="tin" href="{$vb.qlvFile_path}">[Xem File]</a>
                                                {/if}
                                            </td>
                                        </tr>
                                        {if !empty($vanbancon)}{$j=1}
                                            {foreach $vanbancon as $vbc}
                                                {if $vbc.qlv_id==$vb.qlv_id}
                                                    <tr>
                                                        <td class="text-center">{$j++}</td>
                                                        <td>
                                                            <p><a href="{$url}chitietdauviec/{$vbc.qlvDetails_id}" class="tin">{$vbc.qlvDetails_desc}</a></p>
                                                            <p>- Người nhập:{$mangCB[$vbc.input_per]}</p>
                                                            <p>- Ngày nhập:{date_time($vbc.qlvDetails_date)}</p>
                                                            <p>- Hạn văn bản:{($vbc.qlvDetails_limit_time>'1970-01-01')?date_select($vbc.qlvDetails_limit_time):''}</p>
                                                        </td>
                                                        <td>
                                                            <p class="text-primary">- {$vbc.qlvDetails_traloi}</p>
                                                            <p>- Người hoàn thành:{$mangCB[$vbc.qlvDetails_user_traloi]}</p>
                                                            <p>- Ngày hoàn thành:{date_time($vbc.date_traloi)}</p>
                                                            <p>- File hoàn thành:{if !empty($vbc.qlvFile_path)} <a download href="{$vbc.qlvFile_path}" class="tin">[Tải File]</a>{/if}</p>
                                                        </td>
                                                        <td>
                                                            {if !empty($vbc.user_danhgia)}
                                                                {if !empty($vbc.noidung_danhgia)}
                                                                    <p class="text-primary">- {$vbc.noidung_danhgia}</p>
                                                                    <p>- Người đánh giá:{$mangCB[$vbc.user_danhgia]}</p>
                                                                    <p>- Ngày đánh giá:{date_time($vbc.thoigian_danhgia)}</p>
                                                                {else}
                                                                    <p class="text-primary">- Chưa được đánh giá</p>
                                                                {/if}
                                                            {else}
                                                                <p class="text-primary">- Không có đánh giá</p>
                                                            {/if}
                                                        </td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>