<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách tài liệu
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a  class="font tin1" name="anhien" href="javascript:void(0);">Tìm kiếm tài liệu</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal hide anhien" method="get" enctype="multipart/form-data" autocomplete="off">
                    <div class="row"><br>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">HĐND TPHN Khóa </label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($khoa)?$khoa:''}" name="khoa">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nhiệm kỳ </label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($nhiemky)?$nhiemky:''}" name="nhiemky">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Kỳ họp thứ </label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($kyhop)?$kyhop:''}" name="kyhop">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="padding-bottom: 5px;">
                                    <div class="col-md-12">
                                        <input type="radio" name="dinhkybatthuong" value="1" {($dinhkybatthuong)?($dinhkybatthuong==1)?'checked':'':''}> <label style="color:red; padding-right:65px;" for="" class="control-label">Kỳ họp định kỳ</label>
                                        <input type="radio" name="dinhkybatthuong" value="2" {($dinhkybatthuong)?($dinhkybatthuong==2)?'checked':'':''}> <label style="color:red;" for="" class="control-label">Kỳ họp bất thường</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Tên phụ lục </label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  id="" value="{($tenphuluc)?$tenphuluc:''}" name="tenphuluc">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="padding-bottom: 5px;">
                                    <div class="col-md-12">
                                        <input type="radio" name="trangthaitailieu" value="1" {($trangthaitailieu)?($trangthaitailieu==1)?'checked':'':''}> <label style="color:red; padding-right:20px;" for="" class="control-label">Tài liệu thường xuyên</label>
                                        <input type="radio" name="trangthaitailieu" value="2" {($trangthaitailieu)?($trangthaitailieu==2)?'checked':'':''}> <label style="color:red;" for="" class="control-label">Tài liệu không xuyên</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nội dung tài liệu </label>
                                        <textarea name="noidungtailieu" id="" class="form-control" rows="2">{($noidungtailieu)?$noidungtailieu:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực</label>

                                    <div class="col-sm-8">
                                        <select name="linhvuc" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lĩnh vực --</option>
                                            {if !empty($linhvuc)}
                                                {foreach $linhvuc as $lv}
                                                    <option value="{$lv.PK_iMaLV}" {($linhvuc)?($linhvuc==$ld.PK_iMaLV)?'selected':'':''}>{$lv.sTenLV}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lãnh đạo sở</label>

                                    <div class="col-sm-8">
                                        <select name="lanhdao" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lãnh đạo --</option>
                                            {if !empty($lanhdao)}
                                                {foreach $lanhdao as $ld}
                                                    <option value="{$ld.PK_iMaCB}" {($lanhdao)?($lanhdao==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phòng chủ trì</label>

                                    <div class="col-sm-8">
                                        <select name="phongchutri" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn phòng chủ trì --</option>
                                            {if !empty($phongban)}
                                                {foreach $phongban as $pb}
                                                    <option value="{$pb.PK_iMaPB}" {($phongchutri)?($phongchutri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại tài liệu</label>

                                    <div class="col-sm-8">
                                        <select name="loaitailieu" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn loại tài liệu --</option>
                                            {if !empty($loaitailieu)}
                                                {foreach $loaitailieu as $ltl}
                                                    <option value="{$ltl.PK_iMaLTL}" {($loaitailieu)?($loaitailieu==$ltl.PK_iMaLTL)?'selected':'':''}>{$ltl.sTenLoai}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button  type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm">Tìm kiếm </button>
                            </div>
                        </div>
                    </div><br>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="40%">Nội dung</th>
                                    <th width="10%" class="text-center">Tên phụ lục</th>
                                    <th width="10% class="text-center">Phòng chủ trì</th>
                                    <th width="14% class="text-center">phòng phối hợp</th>
                                    <th width="16%" class="text-center">Lãnh đạo sở chỉ đạo</th>
                                    <th width="5%" class="text-center hide">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieu)}{$i=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td >{$dl.sNoiDung}
                                                {if !empty($dl.dsfile)}
                                                <!-- {$d=0} -->
                                                    {foreach $dl.dsfile as $f}
                                                    <!-- {$d++} -->
                                                        {($d>1)?', ':''}<a target="_blank" href="{$url}{$f.sDuongDan}" class="tin1">[Xem File]</a>
                                                    {/foreach}
                                                {/if}
                                            </td>
                                            <td class="text-center">{if $dl.FK_iMaCB_Nhap==$vanban['PK_iMaCB']} <a class="tin1" href="{$url}nhapmoitailieu?ma={$dl.PK_iMaTaiLieu}">{$dl.sTenPhuLuc}</a>{else}{$dl.sTenPhuLuc}{/if}</td>
                                            <td class="text-center">{if isset($mangphong[$dl.FK_iMaPhong_CT])}{$mangphong[$dl.FK_iMaPhong_CT]}{/if}</td>
                                            <td class="text-center">
                                                {if !empty($dl.FK_iMaPhong_PH)}
                                                    <!-- {$dem=0} -->
                                                    {$phoihop = explode(',',$dl.FK_iMaPhong_PH)}
                                                    {foreach $phoihop as $ph}
                                                    <!-- {$dem++} -->
                                                        {($dem>1)?', ':''}{$mangphong[$ph]}
                                                    {/foreach}
                                                {/if}
                                            </td>
                                            <td class="text-center">{if isset($manglanhdao[$dl.FK_iMaCB_LanhDao])}{$manglanhdao[$dl.FK_iMaCB_LanhDao]}{/if}</td>
                                            <td class="text-center hide"><button class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">
                        {$phantrang}
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>