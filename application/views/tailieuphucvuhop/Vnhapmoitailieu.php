<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới tài liệu phục vụ họp
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">HĐND TPHN Khóa *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin)?$thongtin[0]['sKhoa']:''}" name="khoa">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nhiệm kỳ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin)?$thongtin[0]['sNhiemKy']:''}" name="nhiemky">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Kỳ họp thứ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin)?$thongtin[0]['sKyHop']:''}" name="kyhop">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="padding-bottom: 5px;">
                                    <div class="col-md-12">
                                        <input type="radio" name="dinhkybatthuong" value="1" required {($thongtin)?($thongtin[0]['iTrangThaiKyHop']==1)?'checked':'':''}> <label style="color:red; padding-right:65px;" for="" class="control-label">Kỳ họp định kỳ</label>
                                        <input type="radio" name="dinhkybatthuong" value="2" {($thongtin)?($thongtin[0]['iTrangThaiKyHop']==2)?'checked':'':''}> <label style="color:red;" for="" class="control-label">Kỳ họp bất thường</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Tên phụ lục *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  required id="" value="{($thongtin && isset($thongtin[0]['sTenPhuLuc']))?$thongtin[0]['sTenPhuLuc']:''}" name="tenphuluc">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="padding-bottom: 5px;">
                                    <div class="col-md-12">
                                        <input type="radio" name="thuongxuyen" value="1" {($thongtin && isset($thongtin[0]['iTrangThaiTaiLieu']))?($thongtin[0]['iTrangThaiTaiLieu']==1)?'checked':'':''}> <label style="color:red; padding-right:25px;" for="" class="control-label">Tài liệu thường xuyên</label>
                                        <input type="radio" name="thuongxuyen" value="2" {($thongtin && isset($thongtin[0]['iTrangThaiTaiLieu']))?($thongtin[0]['iTrangThaiTaiLieu']==2)?'checked':'':''}> <label style="color:red;" for="" class="control-label">Tài liệu không xuyên</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nội dung tài liệu *</label>
                                        <textarea name="noidung" id="" required class="form-control" rows="4">{($thongtin && isset($thongtin[0]['sNoiDung']))?$thongtin[0]['sNoiDung']:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực*</label>

                                    <div class="col-sm-8">
                                        <select name="linhvuc" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lĩnh vực --</option>
                                            {if !empty($linhvuc)}
                                                {foreach $linhvuc as $lv}
                                                    <option value="{$lv.PK_iMaLV}" {($thongtin && isset($thongtin[0]['FK_iMaLV']))?($thongtin[0]['FK_iMaLV']==$lv.PK_iMaLV)?'selected':'':''}>{$lv.sTenLV}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lãnh đạo sở*</label>

                                    <div class="col-sm-8">
                                        <select name="lanhdao" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn lãnh đạo --</option>
                                            {if !empty($lanhdao)}
                                                {foreach $lanhdao as $ld}
                                                    <option value="{$ld.PK_iMaCB}" {($thongtin && isset($thongtin[0]['FK_iMaCB_LanhDao']))?($thongtin[0]['FK_iMaCB_LanhDao']==$ld.PK_iMaCB)?'selected':'':''}>{$ld.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phòng chủ trì*</label>

                                    <div class="col-sm-8">
                                        <select name="phongchutri" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn phòng chủ trì --</option>
                                            {if !empty($phongban)}
                                                {foreach $phongban as $pb}
                                                    <option value="{$pb.PK_iMaPB}" {($thongtin && isset($thongtin[0]['FK_iMaPhong_CT']))?($thongtin[0]['FK_iMaPhong_CT']==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phòng phối hợp</label>

                                    <div class="col-sm-8">
                                        <select name="phongphoihop[]" id="" class="form-control select2" multiple="" data-placeholder="Chọn phòng phối hợp" style="width: 100%">
                                           {if !empty($phongban)}
                                                {if !empty($thongtin)}
                                                    {$phoihop = explode(',',$thongtin[0]['FK_iMaPhong_PH'])}
                                                {/if}
                                                {foreach $phongban as $pb}
                                                    <option value="{$pb.PK_iMaPB}" {if !empty($phoihop)}{(in_array($pb.PK_iMaPB,$phoihop))}{/if}>{$pb.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại tài liêu*</label>

                                    <div class="col-sm-8">
                                        <select name="loaitailieu" id="" class="form-control select2" style="width: 100%">
                                            <option value="">-- Chọn loại tài liêu --</option>
                                            {if !empty($loaitailieu)}
                                                {foreach $loaitailieu as $ltl}
                                                    <option value="{$ltl.PK_iMaLTL}" {($thongtin && isset($thongtin[0]['FK_iMaLTL']))?($thongtin[0]['FK_iMaLTL']==$ltl.PK_iMaLTL)?'selected':'':''}>{$ltl.sTenLoai}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Tệp tin</label>

                                    <div class="col-sm-8">
                                        <input type="file" name="files[]" multiple="" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button  type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($ma)?'Cập nhật':'Thêm mới<i></i>'}</button> <button onclick="history.go(-1)" class="btn btn-default">Quay lại >></button> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>

