<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Sắp xếp tài liệu họp
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form action="" method="get">
                                <div class="col-md-3">
                                    <select name="khoa" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn khóa --</option>
                                        {if !empty($khoa)}
                                            {foreach $khoa as $k}
                                                <option value="{$k.sKhoa}" {($get_khoa)?($get_khoa==$k.sKhoa)?'selected':'':''}>{$k.sKhoa}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="nhiemky" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn nhiệm kỳ --</option>
                                        {if !empty($nhiemky)}
                                            {foreach $nhiemky as $nk}
                                                <option value="{$nk.sNhiemKy}" {($get_nhiemky)?($get_nhiemky==$nk.sNhiemKy)?'selected':'':''}>{$nk.sNhiemKy}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="kyhop" id="" class="form-control select2" style="width: 100%">
                                        <option value="">-- Chọn kỳ họp --</option>
                                        {if !empty($kyhop)}
                                            {foreach $kyhop as $kh}
                                                <option value="{$kh.sKyHop}" {($get_kyhop)?($get_kyhop==$kh.sKyHop)?'selected':'':''}>{$kh.sKyHop}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary btn-xs" name="loc" value="loc">Lọc tài liệu</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post" autocomplete="off">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="50%">Nội dung</th>
                                    <th width="20%" class="text-center">Tên phụ lục</th>
                                    <th>Thứ tự sắp xếp</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dulieu)}{$i=1}{$j=1}
                                    {foreach $dulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td >{$dl.sNoiDung}</td>
                                            <td class="text-center">{$dl.sTenPhuLuc}</td>
                                            <td class="text-center">
                                                <input type="text" class="hide" value="{$dl.PK_iMaTaiLieu}" name="ma[]">
                                                <input type="text" name="thutu[]" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  class="form-control" value="{($dl.stt)?$dl.stt:$j++}">
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">
                            <button class="btn btn-primary btn-sm" name="capnhat" value="capnhat">Cập nhật</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>