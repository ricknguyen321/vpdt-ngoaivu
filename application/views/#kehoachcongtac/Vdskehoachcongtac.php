<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    Danh sách kế hoạch công tác tuần
                </span>
            </div>
            <div class="col-md-8 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn tuần công tác: 
                </span>
                <label class="8 sl2">
                    <select name="tuan" style="" class="form-control select2" onchange="this.form.submit()">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </label>
                <label><a href="{$url}kehoachcongtac" class="btn btn-primary xxs"> Thêm mới kế hoạch công tác tuần </a></label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="15%" class="text-center">Cán Bộ</th>
                                    <th width="5%" class="text-center">Tuần</th>
                                    <th width="10%" class="text-center">Ngày nhập</th>
                                    <th width="35%" class="text-center">Nhiệm vụ phát sinh</th>
                                    <th width="20%" class="text-center">Đánh giá của lãnh đạo</th>
                                    <th width="5%" class="text-center">Điểm</th>
                                    <th width="5%" class="text-center">Nhập KQ</th>
                                    <th width="5%" class="text-center">Sửa</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            {$i = 1}
                            {foreach $giatri as $tuan}
                            <tr>
                                <td style="vertical-align: top !important">{$i++}</td>
                                <td><a href="{$url}xemkehoachcongtac/{$tuan.id}">{$canbophong[$tuan.user_id]}</a></td>
                                <td class="text-center">{$tuan.tuan}</td>
                                <td class="text-center">{date_select($tuan.date_nhap)}</td>
                                <td class="text-left">
                                    {if $tuan.phatsinh2s !=""}{$tuan.phatsinh2s} <i>({date_time($tuan.date_phatsinh2)})</i><br/>{/if}
                                    {if $tuan.phatsinh2c !=""}{$tuan.phatsinh2c} <i>({date_time($tuan.date_phatsinh2)})</i><br/>{/if}
                                    {if $tuan.phatsinh3s !=""}{$tuan.phatsinh3s} <i>({date_time($tuan.date_phatsinh3)})</i><br/>{/if}
                                    {if $tuan.phatsinh3c !=""}{$tuan.phatsinh3c} <i>({date_time($tuan.date_phatsinh3)})</i><br/>{/if}
                                    {if $tuan.phatsinh4s !=""}{$tuan.phatsinh4s} <i>({date_time($tuan.date_phatsinh4)})</i><br/>{/if}
                                    {if $tuan.phatsinh4c !=""}{$tuan.phatsinh4c} <i>({date_time($tuan.date_phatsinh4)})</i><br/>{/if}
                                    {if $tuan.phatsinh5s !=""}{$tuan.phatsinh5s} <i>({date_time($tuan.date_phatsinh5)})</i><br/>{/if}
                                    {if $tuan.phatsinh5c !=""}{$tuan.phatsinh5c} <i>({date_time($tuan.date_phatsinh5)})</i><br/>{/if}
                                    {if $tuan.phatsinh6s !=""}{$tuan.phatsinh6s} <i>({date_time($tuan.date_phatsinh6)})</i><br/>{/if}
                                    {if $tuan.phatsinh6c !=""}{$tuan.phatsinh6c} <i>({date_time($tuan.date_phatsinh6)})</i><br/>{/if}
                                    {if $tuan.phatsinh7s !=""}{$tuan.phatsinh7s} <i>({date_time($tuan.date_phatsinh7)})</i><br/>{/if}
                                    {if $tuan.phatsinh7c !=""}{$tuan.phatsinh7c} <i>({date_time($tuan.date_phatsinh7)})</i><br/>{/if}
                                </td>
                                <td class="text-left">
                                {if $vanban['iQuyenHan_DHNB'] == 6 or $vanban['iQuyenHan_DHNB'] == 7}
                                <a href="{$url}viewkehoachcongtac/{$tuan.id}">   
                                    {($tuan.tp_danhgia)?$tuan.tp_danhgia:'LĐ đánh giá'}                 
                                </a>
                                <i>
                                {($tuan.tp_danhgia)?$canbophong[$tuan.user_tp]:""} 
                                {($tuan.tp_danhgia)?date_time($tuan.date_tp_danhgia):""}
                                </i>
                                {else}
                                    {if $tuan.tp_danhgia !=''}
                                        <span style="color: blue">{$tuan.tp_danhgia}</span>
                                        <i>{$canbophong[$tuan.user_tp]} {date_time($tuan.date_tp_danhgia)} </i>
                                    {/if}
                                {/if}
                                </td>
                                <td class="text-center"><b>{$tuan.tp_diem}</b></td>
                                <td class="text-center">
                                {if $vanban['PK_iMaCB'] == $tuan.user_id}
                                    <a href="{$url}kqkehoachcongtac/{$tuan.tuan}">Nhập KQ</a>
                                {else}
                                    -
                                {/if}
                                </td>
                                <td class="text-center">
                                {if $vanban['PK_iMaCB'] == $tuan.user_id}
                                    <a href="{$url}suakehoachcongtac/{$tuan.tuan}">Sửa</a>
                                {else}
                                    -
                                {/if}
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>   
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','top');
    });
</script>

