<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    Nhập mới kế hoạch công tác
                </span>
            </div>
            <div class="col-md-8 sl3">
                <span class="4 font" style="font-weight: 550">
                    Chọn tuần công tác: 
                </span>
                <label class="8 sl2">
                    <select name="tuan" style="" class="form-control select2" onchange="this.form.submit()">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <textarea name="viec2s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 2">{($giatri)?$giatri['viec2s']:''}</textarea>
                                
                        </div>
                        <div class="col-md-6">
                            <textarea name="viec2c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 2">{($giatri)?$giatri['viec2c']:''}</textarea>
                        </div>   
                    </div>
                    <div class="row" style="height: 7px"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <textarea name="viec3s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 3">{($giatri)?$giatri['viec3s']:''}</textarea>
                                
                        </div>
                        <div class="col-md-6">
                            <textarea name="viec3c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 3">{($giatri)?$giatri['viec3c']:''}</textarea>
                        </div>   
                    </div>
                    <div class="row" style="height: 7px"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <textarea name="viec4s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 4">{($giatri)?$giatri['viec4s']:''}</textarea>
                                
                        </div>
                        <div class="col-md-6">
                            <textarea name="viec4c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 4">{($giatri)?$giatri['viec4c']:''}</textarea>
                        </div>   
                    </div>
                    <div class="row" style="height: 7px"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <textarea name="viec5s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 5">{($giatri)?$giatri['viec5s']:''}</textarea>
                                
                        </div>
                        <div class="col-md-6">
                            <textarea name="viec5c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 5">{($giatri)?$giatri['viec5c']:''}</textarea>
                        </div>   
                    </div>
                    <div class="row" style="height: 7px"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <textarea name="viec6s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 6">{($giatri)?$giatri['viec6s']:''}</textarea>
                                
                        </div>
                        <div class="row" style="height: 7px"></div>
                        <div class="col-md-6">
                            <textarea name="viec6c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 6">{($giatri)?$giatri['viec6c']:''}</textarea>
                        </div>   
                    </div>
                    <div class="row" style="height: 7px"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <textarea name="viec7s" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc sáng thứ 7">{($giatri)?$giatri['viec7s']:''}</textarea>
                                
                        </div>
                        <div class="col-md-6">
                            <textarea name="viec7c" class="form-control" rows="3" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc chiều thứ 7">{($giatri)?$giatri['viec7c']:''}</textarea>
                        </div>   
                    </div>

                    <div class="row" style="height: 7px"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button> 
                        </div>
                    </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

