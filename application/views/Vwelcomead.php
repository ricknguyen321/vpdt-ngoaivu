<div class="content-wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <style>
                    .dalam{
                        color:#00f;
                    }
                    .chualam{
                        color:#F00;
                    }
                    .gachchan{
                        border-bottom:1px dashed #ccc;
                    }
                    </style>
                </div>
                <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading"><b>Thống kê hệ thống</b></div>
                                <div class="panel-body">
                                    <p class="gachchan">Số phòng ban: <span class="pull-right">{banghi('tbl_phongban')}</span></p>
                                    <p class="gachchan">Số chức vụ: <span class="pull-right">{banghi('tbl_chucvu')}</span></p>
                                    <p class="gachchan">Số thành viên: <span class="pull-right">{banghi('tbl_canbo')}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading"><b>Thống kê hệ thống văn bản</b></div>
                                <div class="panel-body">
                                    <p class="gachchan">Số người ký: <span class="pull-right">{banghi('tbl_nguoiky')}</span></p>
                                    <p class="gachchan">Số chức vụ ký: <span class="pull-right">{banghi('tbl_chucvunguoiky')}</span></p>
                                    <p class="gachchan">Số nới gửi đến: <span class="pull-right">{banghi('tbl_donvi')}</span></p>
                                    <p class="gachchan">Số độ mật: <span class="pull-right">{banghi('tbl_domat')}</span></p>
                                    <p class="gachchan">Số độ khẩn: <span class="pull-right">{banghi('tbl_dokhan')}</span></p>
                                    <p class="gachchan">Số khu vực: <span class="pull-right">{banghi('tbl_khuvuc')}</span></p>
                                    <p class="gachchan">Số lĩnh vực: <span class="pull-right">{banghi('tbl_linhvuc')}</span></p>
                                    <p class="gachchan">Số chủng loại: <span class="pull-right">{banghi('tbl_loaivanban')}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading"><b>Thống kê văn bản</b></div>
                                <div class="panel-body">
                                    <p class="gachchan chualam">Văn bản chưa giải quyết: <span class="pull-right">{banghi2('tbl_vanbanden',0)}</span></p>
                                    <p class="gachchan dalam">Văn bản đã giải quyết: <span class="pull-right">{banghi2('tbl_vanbanden',1)}</span></p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
</div>
