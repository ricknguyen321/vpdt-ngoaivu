<script src="{$url}assets/js/vanbanden/vanbanden.js"></script>
<script src="{$url}assets/js/plugins/checksoden.js"></script>
<style>
    .alert-warning {
        color: #8a6d3b !important;
        background-color: #fcf8e3 !important;
        border-color: #faebcc !important;
    }
    .alert {
        padding: 15px !important;
        margin-bottom: 0px !important;
        border: 1px solid transparent !important;
        border-radius: 4px !important;
    }
    .alert-dismissable .close {
        position: relative;
        top: -2px;
        right: 0px;
        color: inherit;
    }
    button.close {
        -webkit-appearance: none;
        padding: 0;
        cursor: pointer;
        background: 0 0;
        border: 0;
    }
    .close {
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        filter: alpha(opacity=20);
        opacity: .2;
    }
</style>
<script>
    $(document).ready(function() {
        $('#soden').css('color', 'red');
        $('input').css('font-size', '14px');
        $('textarea').css('font-size', '14px');
    });
</script>
<div class="content-wrapper">

 <section class="content-header">
        <h3 class="font" style="text-align:center; font-size:30px;">
        </h3>
    </section>
		<h3 style="text-align:center; font-size:30px; margin-top:-30px">
            GIẤY MỜI
        </h3>
		
    <!-- Main content -->
    <section class="content">
        <div class="col-sm-12 hidden">
            <div class="col-md-8 noidung">
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Nội dung họp </label>

                    <div class="col-sm-9">
                        <textarea name="noidunghop[]" class="form-control" rows="3" "></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Địa điểm họp</label>

                    <div class="col-sm-9">
                        <input type="text" name="diadiemhop[]" value="" class="form-control"">
                    </div>
                </div>
            </div>
            <div class="col-md-4 noidung">
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label" required>Giờ họp</label>

                    <div class="col-sm-8">
                        <input type="text" name="giohop[]" class="form-control" value="" placeholder="Ví dụ: {date('h:i',time())}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Ngày họp</label>

                    <div class="col-sm-8">
                        <input type="text" name="ngayhop[]" value="" class="form-control datepic datemask" id="" placeholder="Ví dụ: {date('d/m/Y',time())}">
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {if isset($content.class)&& $content.class == info}
                <div class="alert alert-warning alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <label for="" style="color:black;">Thêm thành công số đến là: <b style="font-size: 20px;color:red;" id="thongbaoden"></b></label>
                </div>
                {/if}
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="on">
                    <div class="row">
                        <div class="col-sm-12"><br>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu </label>
                                    <div class="col-sm-8">
                                        <input tabindex="1" type="text" name="sokyhieu" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sKyHieu'] : NULL }" class="form-control" id="" placeholder="Nhập số hiệu" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="2" name="noiguiden" tabindex="" type="text" class="noiguiden form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenDV'] : NULL }" autocomplete="off" placeholder="Nhập nơi gửi đến" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" name="ngayky" class="form-control datepic datemask" id="" value="{!empty($getDocGo) &&($getDocGo) ? date_select($getDocGo[0]['sNgayKy']) : NULL }" placeholder="Nhập ngày ký">
                                    </div>
                                </div>
                            </div>
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color:blue"><br>Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                      <textarea tabindex="4" name="noidung" required id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sMoTa'] : NULL }</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Giờ họp *:</label>

                                    <div class="col-sm-8 bootstrap-timepicker">
                                        <input tabindex="5" type="text" name="giohopgm" class="form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sGiayMoiGio'] : NULL }" placeholder=" Ví dụ: {date('h:i',time())}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Địa điểm *:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="7" type="text" name="diadiem" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sGiayMoiDiaDiem'] : NULL }" class="form-control" id="" placeholder="Nhập địa điểm" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="9" type="text" name="nguoiky" class="nguoiky form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenNguoiKy'] : NULL }" autocomplete="off" placeholder="Nhập người ký">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Ngày họp *:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="6" type="text" name="ngayhopgm" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sGiayMoiNgay'] : NULL }" class="form-control datepic datemask" id="" required placeholder="Nhập ngày họp">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người chủ trì</label>

                                    <div class="col-sm-8">
                                        <input tabindex="8" type="text" name="nguoichutri" class="form-control" id="" placeholder="Người chủ trì" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sGiayMoiChuTri'] : NULL }">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="chucvu" class="form-control" id="" placeholder="Nhập chức vụ" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sChucVu'] : NULL }">
                                    </div>
                                </div>
                            </div>
                            <!--                            <hr style="border: 1px dashed blue">-->


                          
                            {if !empty($getDocGos)}
                            {foreach $getDocGos as $bc}
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung họp </label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop[]" class="form-control" rows="2" placeholder="Nhập nội dung ">{$bc.sNoiDung}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Địa điểm họp</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="diadiemhop[]" class="form-control" value="{$bc.sGiayMoiDiaDiem}" placeholder="Nhập địa điểm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" required>Giờ họp</label>

                                    <div class="col-sm-8 bootstrap-timepicker">
                                        <input type="text" name="giohop[]" class="form-control" value="{$bc.sGiayMoiGio}" id="" placeholder="Ví dụ: {date('h:i',time())}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày họp</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngayhop[]" class="form-control" value="{date_select($bc.sGiayMoiNgay)}" id="" placeholder="Ví dụ: {date('d/m/Y',time())}">
                                    </div>
                                </div>
                            </div>
                            {/foreach}
                            {else}
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung họp </label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop[]" class="form-control" rows="3" placeholder="Nhập nội dung "></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Địa điểm họp</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="diadiemhop[]" class="form-control" value="" placeholder="Nhập địa điểm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" required>Giờ họp</label>

                                    <div class="col-sm-8 bootstrap-timepicker">
                                        <input type="text" name="giohop[]" class="form-control" value="" id="" placeholder="Ví dụ: {date('h:i',time())}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày họp</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngayhop[]" class="form-control" value="" id="" placeholder="Ví dụ: {date('d/m/Y',time())}">
                                    </div>
                                </div>
                            </div>
                            {/if}
                            <div class="themsau"></div>
                            <div class="col-md-12 text-right">
                                <div class="col-sm-8">
                                    <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                </div>
                            </div>
							<div class="row">
							 <div class="col-sm-12">
							<div class="col-md-6">
								<form method="post" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Đính kèm:</label>
                                    <div class="col-sm-8">                                        
										<input type="file" name="files[]" readonly="" placeholder="Click vào đây để chọn files" class="form-control" id="" multiple="" required="required">
                                    </div>
                                </div>
								</form>
                            </div>
							</div>
							</div>
                            <div class="row">
                                <div class="col-sm-12"><br>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Ngày nhận</label>
                                            <div class="col-sm-8">
                                                <input tabindex="10" type="text" name="ngaynhan" value="{date('d/m/Y',time())}" class="form-control datepic datemask" id="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Số đến</label>

                                            <div class="col-sm-8">
                                                <input type="number" name="soden" class="form-control" value="{$soden}" id="soden" style="color: red; font-weight: 600; font-size: 18px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Số trang</label>

                                            <div class="col-sm-8">
                                                <input type="number" name="sotrang" value="1" class="form-control" id="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							 
                            <div class="col-md-12" style="text-align:center; margin: 30px">
                                <button tabindex="10" type="button" class="btn btn-primary btn-sm hidden" name="luuao" disabled>Thêm mới</button> <button tabindex="11" type="submit" name="luulai" value="Thêm mới" class="btn btn-primary btn-sm">Thêm mới</button>
                                <div ng-app="myModule" ng-controller="mtController" class="hidden">
                                    <button ng-click="checkSoDen()" name="checksoden">Kiểm tra số đến</button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung2 = $('.noidung')[1].outerHTML;
            var noidung = noidung1+noidung2;
            $('.themsau').before(noidung);
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
        $('#thongbaoden').text(parseInt($.trim($('#soden').val())) - 1);
        $(document).on('click','button[name=luulai]',function(){
            $('button[name=checksoden]').click();
        });
        $(document).on('click','button[name=luulai]',function () {
            $('button[name=luulai]').addClass('hidden');
            $('button[name=luuao]').removeClass('hidden');
        });
		$(document).on('keyup','input',function () {
            $('button[name=luulai]').removeClass('hidden');
            $('button[name=luuao]').addClass('hidden');
        });
    });
</script>
<script>
    var data = {
        "noiden": [
            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
            "{$dv.sTenDV}",
            {/foreach}
    ],
    "nguoiky": [
        {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
        "{$nk.sHoTen}",
        {/foreach}
    ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".noiguiden",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>