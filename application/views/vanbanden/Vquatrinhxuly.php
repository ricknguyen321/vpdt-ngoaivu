<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header h1no">
      <h1 class="font">
         Quá trình xử lý văn bản đến 
      </h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Default box -->
      <div class="box">
         <div class="box-body">
            <form class="form-horizontal" method="post" action="">
               <div class="row">
                  <input class="pull-right btn btn-primary btn-sm" style="margin-right:30px;background-color: blue;color: white;font-weight: bold;padding: 6px;border: none;border-radius: 5px;" type="submit" name="ketxuatword" value="Tải phiếu trình">
                  {if $vanban['PK_iMaCB'] == 705 || $vanban['iQuyenHan_DHNB'] == 3}
				  <a href="quatrinhxuly/{$getdAppointment[0]['PK_iMaVBDen']}?kx=word"><button class="pull-right btn btn-primary btn-sm" style="margin-right:10px;background-color: blue;color: white;font-weight: bold;padding: 6px;border: none;border-radius: 5px;" type="button" name="ketxuatword" value="">Tải phiếu xử lý</button></a>
				  
				  {/if}
                  <a class="pull-right" data-toggle="collapse" href="#collapseExample{$getdAppointment[0]['PK_iMaVBDen']}" aria-expanded="false" aria-controls="collapseExample{$getdAppointment[0]['PK_iMaVBDen']}" style="margin-right:10px"><b style="color:black"> <button class="btn btn-primary btn-xs" style="">Thêm vào hồ sơ</button></b></a>
                  <div class="col-sm-8 collapse pull-right" id="collapseExample{$getdAppointment[0]['PK_iMaVBDen']}" style="margin-bottom: 20px; ">
                     <div class="col-sm-10">												
                        <select name="manghs{$getdAppointment[0]['PK_iMaVBDen']}[]" multiple="" id="" data-placeholder="" class="form-control select2" style="width:100%">
                        {if !empty($dshoso)}
                        {foreach $dshoso as $hs}
                        <option value="{$hs.PK_iMaHS}" {if in_array($hs.PK_iMaHS, $dshosovb)}selected{/if}>{$hs.sTenHS}</option>
                        {/foreach}
                        {/if}
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <button type="submit" name="luuhoso" value="{$getdAppointment[0]['PK_iMaVBDen']}" class="btn btn-primary btn-sm">Lưu</button>
                     </div>
                  </div>
                  <div class="col-sm-12" {if $getdAppointment[0]['iTrangThai'] == 1}style="background: url(/vpdtsongoaivu/files/images/hoanthanh.png) no-repeat; background-position-x: right; background-size: 10%;"{/if}>
				   <div class="col-md-2">
                     <h5>
                     <label for="" style="{!empty($getdAppointment) &&($getdAppointment[0]['iSTCPhoiHop'] == '1') ? 'color:blue' : NULL }"><input type="checkbox" name="iSTCPhoiHop" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iSTCPhoiHop'] == '1') ? checked : NULL }> VB Đảng</label> </h4>
                  </div>
                  <div class="col-md-2">
                     <h5>
                     <label for=""  style="{!empty($getdAppointment) &&($getdAppointment[0]['iVanBanQPPL'] == '1') ? 'color:blue' : NULL }"><input type="checkbox" name="iVanBanQPPL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanQPPL'] == '1') ? checked : NULL }> VB QPPL</label> </h4>
                  </div>
                  <!--<div class="col-md-2">
                     <h5>
                     <label for=""><input type="checkbox" name="iSTCChuTri" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iSTCChuTri'] == '1') ? checked : NULL }> SNV chủ trì</label> </h4>
                  </div>-->
                  <div class="col-md-2">
                     <h5>
                     <label for="" style="{!empty($getdAppointment) &&($getdAppointment[0]['iVanBanTBKL'] == '1') ? 'color:blue' : NULL }"><input type="checkbox" name="iVanBanTBKL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanTBKL'] == '1') ? checked : NULL }> TBKL</label> </h4>
                  </div>
                  <div class="col-md-2">
                     <h5><label for="" style="{!empty($getdAppointment) &&($getdAppointment[0]['iToCongTac'] == '1') ? 'color:blue' : NULL }"><input type="checkbox" name="iToCongTac" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iToCongTac'] == '1') ? checked : NULL }> TTHC</label> </h5>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Khu vực: </label>
                        <div class="col-sm-8">
                           <label for=""></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Số ký hiệu:</label>
                        <div class="col-sm-8">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }</p>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Loại văn bản:</label>
                        <div class="col-sm-8">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLVB'] : NULL }</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Nơi gửi đến:</label>
                        <div class="col-sm-8">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }</p>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">	Ngày ký:</label>
                        <div class="col-sm-8">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : NULL }</p>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Lĩnh vực / Hashtag:</label>
                        <div class="col-sm-8">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLV'] : NULL }</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Trích yếu:</label>
                        <div class="col-sm-10" style="padding-top:7px">
                           {if $getdAppointment[0]['iTrangThai'] != 1}
                           {if (date('Y-m-d',time()) <= $getdAppointment[0]['sHanThongKe']) && (date('Y-m-d',time()+86400*3) >= $getdAppointment[0]['sHanThongKe'])}
                           <b style="color: orange;font-weight: bold;">{$getdAppointment[0]['sMoTa']}</b>
                           {elseif $getdAppointment[0]['sHanThongKe'] < date('Y-m-d',time()) && $getdAppointment[0]['sHanThongKe'] > '2017-01-01'}
                           <b style="color: red">{$getdAppointment[0]['sMoTa']}</b>
                           {else}
                           <b>{$getdAppointment[0]['sMoTa']}</b>
                           {/if}
                           {else}
                           {if $getdAppointment[0]['sNgayGiaiQuyet'] > $getdAppointment[0]['sHanThongKe']}
                           <b style="color: red">{$getdAppointment[0]['sMoTa']}</b>
                           {else}
                           <b>{$getdAppointment[0]['sMoTa']}</b>
                           {/if}
                           {/if}
                           <!--<p><b style="color: blue">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</b></p>-->
                        </div>
                     </div>
                  </div>
				  {if !empty($getdAppointment[0]['sNoiDung'])}
				  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Nội dung:</label>
                        <div class="col-sm-10" style="padding-top:7px;padding-bottom:7px">
                           <b>{$getdAppointment[0]['sNoiDung']}</b>
                        </div>
                     </div>
                  </div>
				  {/if}
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Người ký: </label>
                        <div class="col-sm-8">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenNguoiKy'] : NULL }</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;" style="margin-top: -7px;">Chức vụ <span class="text-info"></span>:</label>
                        <div class="col-sm-8">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sChucVu'] : NULL }</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Ngày nhận:</label>
                        <div class="col-sm-2">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayNhan']) : NULL }</p>
                        </div>
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Hạn nhập:</label>
                        <div class="col-sm-2">
                           <p>{!empty($getdAppointment) &&($getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getdAppointment[0]['sHanGiaiQuyet']) : NULL }</p>
                        </div>
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Số trang:</label>
                        <div class="col-sm-2">
                           <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoTrang'] : NULL }</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;" style="color: red; font-size: 16px">Số đến:</label>
                        <div class="col-sm-2">
                           <label style="color: red;font-size: 20px">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }</label>
                        </div>
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Độ mật:</label>
                        <div class="col-sm-2">
                           <p>
                              {$domat = layDoMat($getdAppointment[0]['FK_iMaDM'])}											
                              {if $getdAppointment[0]['FK_iMaDM'] > 1} <b style="color:red; font-size:16px">{$domat[0].sTenDoMat}</b>{else}{$domat[0].sTenDoMat}{/if}
                           </p>
                        </div>
                        <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Độ khẩn:</label>
                        <div class="col-sm-2">
                           <p>
                              {$dokhan = layDoKhan($getdAppointment[0]['FK_iMaDK'])} 
                              {if $getdAppointment[0]['FK_iMaDK'] > 1} <b style="color:red; font-size:16px"> {$dokhan[0].sTenDoKhan} </b> {else}{$dokhan[0].sTenDoKhan}{/if}
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label" style="width:50%; font-size:16px; margin-top:20px"><a href="teptinden?id={$getdAppointment[0]['PK_iMaVBDen']}"><b>Tài liệu <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></b></a></label>
						</div>
						<div style="max-height:250px;  overflow:auto; margin-bottom:10px">
                        <table class="table table-bordered" style="width: 73%">
                           <thead>
                              <tr>
                                 <th class="text-center" style="width: 50px">STT</th>
                                 <th style="">Tên file</th>
                                 <th class="text-center" style="width:15%">Thời gian</th>
                                 <th class="text-center" style="width:17.5%">Cán bộ</th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {foreach $Filedinhkem as $fi}
                              <tr>
                                 <td class="text-center">{$i++}</td>
                                 <td><a style="color:brown" href="{$fi.sDuongDan}" target="_blank">{substr($fi.sTenFile, 0, 90)}</a></td>
                                 <td class="text-center" >{date_time2($fi.sThoiGian)}</td>
                                 <td class="text-center">{$cb = layTTCB($fi.FK_iMaCB)}{$cb[0].sHoTen}</td>
                              </tr>
                              {/foreach}
                           </tbody>
                        </table>
                     </div>
                  </div>
                  {if !empty($idDocGo)}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <b style="margin-left:20px"> Trình tự giải quyết:</b>
                        {if !empty($process)}{$i=1}
                        {foreach $process as $pr}
                        <b style="font-size:14px; {if $i == count($process)} color:blue {/if}"> {$pr.sHoTen} </b> {if $i <count($process)} 	&rarr; {/if}
                        {$i = $i + 1}
                        {/foreach}
                        {else} 
                        <p>----</p>
                        {/if}
                        {if !empty($getdAppointment[0]['iDeadline']) && $getdAppointment[0]['iDeadline'] == 1}
                        <label for="" class="col-sm-4 control-label" style="float:right; "><b style=" color:blue">Văn bản này là văn bản Lãnh đạo giao có thời hạn</b>: <b style="font-size:20px; color:red">{date_select($getdAppointment[0]['sHanThongKe'])}</b></label>
                        {else}
                        <label for="" class="col-sm-4 control-label" style="float:right; ">Hạn xử lý: <b style="font-size:20px; color:red">{date_select($getdAppointment[0]['sHanThongKe'])}</b></label>
                        {/if}
                     </div>
                     {if !empty($process)}{$i=1}								
                     <!--<div class="form-group">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center" width="50px">STT</th>
                                
                                <th class="text-center" width="15%">Chuyển từ</th>
                        
                                <th>Nội dung</th>
                                <th class="text-center" width="15%">Chuyển đến</th>
                        <th class="text-center" width="12%">Thời gian</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            {$i=1}
                            {foreach $process as $pr}
                            <tr class="info">
                                <td class="text-center">{$i++}</td>
                        
                                <td class="text-center">
                                    {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                        {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                        {$cb.sHoTen}
                        {/if}
                                    {/foreach}
                                </td>
                        
                                <td>{$pr.sMoTa}</td>
                                <td class="text-center">{$pr.sHoTen}</td>
                        <td class="text-center">{date_time2($pr.sThoiGian)}</td>
                                
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        </div>-->
                     {else} 
                     <p>----</p>
                     {/if}
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label">Lưu vết chuyển văn bản:</label>
                     </div>
                     <div class="form-group">
                        <table class="table table-bordered table-striped">
                           <thead>
                              <tr>
                                 <th class="text-center" style="width:50px">STT</th>
                                 <th class="text-center" style="width:15%">Chuyển từ</th>
                                 <th>Chỉ đạo/Ý kiến</th>
                                 <th class="text-center" style="width:15%">Chuyển đến</th>
                                 <th class="text-center" style="width:12%">Thời gian</th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {$cbchuyen =''}
                              {foreach $luuvetchuyennhan as $lvet}
                              <tr class="info">
                                 <td class="text-center">{if $dscanbo[$lvet.FK_iMaCB_Chuyen] != $cbchuyen}{$i}{/if}</td>
                                 <td class="text-center">
                                    {if $dscanbo[$lvet.FK_iMaCB_Chuyen] != $cbchuyen}
                                    {if $lvet.FK_iMaCB_Chuyen == 693}
                                    <b>{$dscanbo[$lvet.FK_iMaCB_Chuyen]}</b>
                                    {else}
                                    {$dscanbo[$lvet.FK_iMaCB_Chuyen]}
                                    {/if}
                                    {$i=$i+1}
                                    {/if}
                                 </td>
                                 <td>
                                    {if $lvet.FK_iMaCB_Chuyen == 693}
                                    <b>{$lvet.sNoiDung}</b>
                                    {else}
                                    {$lvet.sNoiDung}
                                    {/if}
                                 </td>
                                 <td  class="text-center">{$dscanbo[$lvet.FK_iMaCB_Nhan]}{if $i - 1 == count($luuvetchuyennhan)} <br><b>(đang giữ VB)</b>{/if}</td>
                                 <td class="text-center">{date_time2($lvet.sThoiGian)}</td>
                              </tr>
                              {$cbchuyen = $dscanbo[$lvet.FK_iMaCB_Chuyen]}
                              {/foreach}
                           </tbody>
                        </table>
                     </div>
                  </div>
                  {if !empty($chidao)}
				  {$chidao = array_reverse($chidao)}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="color:blue; font-size:15px">Chỉ đạo của lãnh đạo:</label>
                     </div>
                     <div class="form-group">
                        <table class="table table-bordered table-striped" style="width: 88%">
                           <thead>
                              <tr style="background-color: orange !important">
                                 <th class="text-center" style="width:50px">STT</th>
                                 <th class="text-center" style="width:17%">Lãnh đạo</th>
                                 <th>Nội dung</th>
                                 <th class="text-center" style="width:17%">Thời gian</th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {foreach $chidao as $cd}
                              {$ld = layTTCB($cd.FK_iMaCB)}
                              <tr class="info">
                                 <td class="text-center">{$i++}</td>
                                 <td class="text-center"><span style="color: red"> {$ld[0].sHoTen} </span></td>
                                 <td><span style="color: red"> {$cd.sNoiDung}</span></td>
                                 <td class="text-center">{date_time2($cd.sThoiGian)}&nbsp; {if $cd.FK_iMaCB == $vanban['PK_iMaCB'] && $getdAppointment[0]['iTrangThai'] != 1}<button type="submit" value="{$cd.id}" name="xoachidao" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                              </tr>
                              {/foreach}
                           </tbody>
                        </table>
                     </div>
                  </div>
                  {/if}
                  {if !empty($dexuat)}
				  {$dexuat = array_reverse($dexuat)}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="color:blue; font-size:15px">Đề xuất / ý kiến:</label>
                     </div>
                     <div class="form-group">
                        <table class="table table-bordered table-striped" style="width: 88%">
                           <thead>
                              <tr style="background-color: orange !important">
                                 <th class="text-center" style="width:50px">STT</th>
                                 <th class="text-center" style="width:17%">Cán bộ</th>
                                 <th>Nội dung</th>
                                 <th class="text-center" style="width:17%">Thời gian</th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {foreach $dexuat as $dx}
                              {$ld = layTTCB($dx.FK_iMaCB)}
                              <tr class="info">
                                 <td class="text-center">{$i++}</td>
                                 <td class="text-center">{$ld[0].sHoTen}</td>
                                 <td>{$dx.sNoiDung}</td>
                                 <td class="text-center">{date_time2($dx.sThoiGian)} &nbsp; {if $dx.FK_iMaCB == $vanban['PK_iMaCB'] && $getdAppointment[0]['iTrangThai'] != 1}<button type="submit" value="{$dx.id}" name="xoadexuat" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                              </tr>
                              {/foreach}
                           </tbody>
                        </table>
                     </div>
                  </div>
                  {/if}
                  {if !empty($qtgiahan)}
					  {$qtgiahan = array_reverse($qtgiahan)}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="">Quá trình gia hạn giải quyết:</label>
                     </div>
                     <div class="form-group">
                        <table class="table table-bordered table-striped" >
                           <thead>
                              <tr style="background-color: orange !important">
                                 <th class="text-center" style="width:50px">STT</th>
                                 <th class="text-center" style="width:15%">Cán bộ</th>
                                 <th>Nội dung</th>
                                 <th class="text-center" style="width:15%">Hạn đề xuất</th>
                                 <th class="text-center" style="width:12%">Thời gian</th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {foreach $qtgiahan as $gh}
                              {if $gh.FK_iMaCB_Gui > 0}
                              {$cb = layTTCB($gh.FK_iMaCB_Gui)}
                              {$isld = 0}
                              {else} 
                              {$cb = layTTCB($gh.FK_iMaCB_Duyet)}
                              {$isld = 1}
                              {/if}
                              <tr class="info">
                                 <td class="text-center">{$i++}</td>
                                 <td class="text-center" {if $isld == 1}style="font-weight:bold"{/if}>{$cb[0].sHoTen}</td>
                                 <td {if $isld == 1}style="font-weight:bold"{/if}>{if !empty($gh.noidungtuchoi)}
                                 {$gh.noidungtuchoi}
                                 {else}
                                 {if !empty($gh.noidunggiahan)}
                                 {$gh.noidunggiahan}
                                 {elseif $gh.iTrangThai_ThemHan == 2}
                                 Duyệt
                                 {elseif $gh.iTrangThai_ThemHan == 3}
                                 Từ chối
                                 {/if}
                                 {/if}
                                 </td>
                                 <td class="text-center">
                                    {if $gh.sHanThongKe != '0000-00-00'}
                                    {date_select($gh.sHanThongKe)}
                                    {elseif $gh.iTrangThai_ThemHan == 3}
                                    <span style="color:red">Từ chối</span>
                                    {/if}
                                 </td>
                                 <td class="text-center">
                                    {if !empty($gh.ngaygiahan)}
                                    {date_time2($gh.ngaygiahan)}
                                    {else}
                                    {if !empty($gh.ngayduyet)}
                                    {date_time2($gh.ngayduyet)}
                                    {/if}
                                    {/if}
                                 </td>
                              </tr>
                              {/foreach}
                           </tbody>
                        </table>
                     </div>
                  </div>
                  {/if}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label"><a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><b style="color:black">Thông tin cán bộ phối hợp {if !empty($phongbanphoihop)}(click to view){else}(N/A){/if}</b></a></label>
                     </div>
                     <div class="form-group collapse" id="collapseExample">
                        {if !empty($phongbanphoihop)}
                        <table class="table table-bordered table-striped">
                           <thead>
                              <tr>
                                 <th class="text-center" style="width:50px">STT</th>
                                 <th class="text-center" style="width:15%">Chuyển từ</th>
                                 <th>Nội dung</th>
                                 <th class="text-center" style="width:15%">Chuyển đến</th>
                                 <th class="text-center" style="width:12%">Thời gian</th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {foreach $phongbanphoihop as $phh}
                              {if $phh.input_per != null}
                              <tr class="info">
                                 <td class="text-center">{$i++}</td>
                                 <td class="text-center">{$dscanbo[$phh.input_per]}</td>
                                 <td>{$phh.sMoTa}</td>
                                 <td class="text-center">{$dscanbo[$phh.PK_iMaCB]}</td>
                                 <td class="text-center">{date_time2($phh.sThoiGian)}</td>
                              </tr>
                              {/if}
                              {/foreach}
                           </tbody>
                        </table>
                        {else} 									
                        <p style="margin-left:20px
                           ">Không có cán bộ phối hợp</p>
                        {/if}
                     </div>
                  </div>
                  {if !empty($chuyennhan_phoihop)}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-4 control-label">Quá trình chuyền nhận phối hợp</label>
                     </div>
                     <div class="form-group">
                        <table class="table table-bordered table-striped">
                           <thead>
                              <tr>
                                 <th class="text-center" style="width: 50px">STT</th>
                                 <th >Nội dung</th>
                                 <th class="text-center" width="15%">Người nhận</th>
                                 <th class="text-center"></th>
                                 <th class="text-center"></th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {foreach $chuyennhan_phoihop as $cn}
                              <tr class="success">
                                 <td class="text-center">{$i++}</td>
                                 <td >{$cn.sNoiDung} {if $cn.iTrangThai==3} <span class="label label-success">Kết thúc</span>{/if} - <b> {$dscanbo[$cn.FK_iMaNguoi_Gui]}</b> - {date_time2($cn.sThoiGian_Chuyen)}</td>
                                 <td class="text-center">{$dscanbo[$cn.FK_iMaNguoi_Nhan]}</td>
                                 <td class="text-center" >{if !empty($cn.sFile_YeuCau)}<a href="{$url}{$cn.sFile_YeuCau}" target="_blank">[Xem file]</a>{/if}</td>
                                 <td class="text-center" >{if !empty($cn.sFile_TraLoi)}<a href="{$url}{$cn.sFile_TraLoi}" target="_blank">[Xem file]</a>{/if}</td>
                              </tr>
                              {/foreach}
                           </tbody>
                        </table>
                     </div>
                  </div>
                  {/if}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-10 control-label"><a data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2"><b style="color:black">Ý kiến / kết quả phối hợp {if !empty($resultPPH)}(click to view){else}(N/A){/if}</b></a></label>
                     </div>
                     <div class="form-group collapse" id="collapseExample2">
                        {if !empty($resultPPH)}
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th style="width: 50px">STT</th>
                                 <th style="width: 15%">Cán bộ</th>
                                 <th>Nội dung</th>
                                 <th style="width: 12%">Thời gian</th>
                              </tr>
                           </thead>
                           <tbody>
                              {$i=1}
                              {foreach $resultPPH as $re}
                              <tr >
                                 <td class="text-center">{$i++}</td>
                                 <td  class="text-center">{$re.sHoTen}</td>
                                 <td>{$re.sMoTa}</td>
                                 <td class="text-center">{date_time2($re.date_file)}</td>
                              </tr>
                              {/foreach}
                           </tbody>
                        </table>
                        {else}
                        <p style="margin-left:20px">----</p>
                        {/if}
                     </div>
                  </div>
                  {if !empty($resultPTL)}
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label" style="width:50%">Kết quả phòng chủ trì:</label>
                     </div>
                     <div class="form-group">
            <form action="" method="post">
            <table class="table table-bordered">
            <thead>
            <tr>
            <th width="50px">STT</th>
            <th width="15%">Cán bộ</th>
            <th>Nội dung</th>					
            <th width="12%" class="text-center">Thời gian</th>
            </tr>
            </thead>
            <tbody>
            {$i=1}
            {$mota = ''}
            {foreach $resultPTL as $tl}
            <tr>
            <td class="text-center">{$i++}</td>
            <td class="text-center" >{$tl.sHoTen}</td>
            <td>{if $tl.sMoTa != $mota}<p>{$tl.sMoTa}</p>{/if}
            {if $tl.sDuongDanFile !=''  && $tl.sDuongDanFile != 'doc_uploads/'}
            <a class="tin1 pull-right" href="{$tl.sDuongDanFile}" target="_blank"><span style="color:brown">{$tl.sTenFiles}</span></a>
            {/if}
            </td>			
            <td class="text-center">{date_time2($tl.date_file)}</td>                          
            </tr>
            {$mota = $tl.sMoTa}
            {/foreach}
            </tbody>
            </table>
            </form>
            </div>
            </div>
            {/if}
            <div class="col-sm-12">
            <div class="form-group">
            <label for="" class="col-sm-12 control-label">Quá trình xử lý văn bản:</label>
            </div>
            {if !empty($luuvetduyet)}{$i=1}
            <div class="form-group">
            <form action="" method="post">
            <table class="table table-bordered">
            <thead>
            <tr>
            <th class="text-center" style="width:50px">STT</th>
            <th class="text-center" style="width:15%">Chuyển từ</th>
            <th>Nội dung</th>
            <th class="text-center" style="width:15%">Chuyển đến</th>
            <th class="text-center" style="width:12%">Thời gian</th>
            </tr>
            </thead>
            <tbody>                                        
            {foreach $luuvetduyet as $lvd}
            <tr >
            <td class="text-center">{$i++}</td>
            <td class="text-center" >{$dscanbo[$lvd.FK_iMaCB_Chuyen]}</td>
            <td>{$lvd.sNoiDung}</td>
            <td class="text-center" >{$dscanbo[$lvd.FK_iMaCB_Nhan]}</td>
            <td class="text-center">{date_time2($lvd.sThoiGian)}</td>
            </tr>
            {/foreach}
            </tbody>
            </table>
            </form>
            </div>
            {else} ----
            {/if}
            </div>
            {if !empty($VBDi)}
            <div class="col-sm-12">
            <div class="form-group">
            <label for="" class="col-sm-2 control-label">Văn bản đi</label>
            </div>
            <div class="form-group">
            <table class="table table-bordered table-striped" style="width: 88%">
            <thead>
            <tr>
            <th class="text-center" style="width: 50px">STT</th>
            <th style="width: 12%" class="text-center">Số văn bản đi</th>
            <th style="width: 15%" class="text-center" >Ngày văn bản</th>
            <th class="text-center">Trích Yếu</th>
            </tr>
            </thead>
            <tbody>
            {$i=1}
            {foreach $VBDi as $di}
            <tr class="success">
            <td class="text-center">{$i++}</td>
            <td class="text-center">
            {if $di.iSoVBDi == '0' || $di.iSoVBDi == '00'} 
            Chưa cấp số 
            {else}
            <a href="dsvanban?kyhieu={$di.iSoVBDi}"><b style="color: red; font-size: 16px">{$di.iSoVBDi}</b></a>
            {/if}
            </td>
            <td class="text-center">{date_select($di.sNgayVBDi)}</td>
            <td>
            {$dsfiledi = layDuLieu('FK_iMaVBDi',$di.PK_iMaVBDi,'tbl_files_vbdi')}
            <p><a href="{$url}thongtinvanban?id={$di.PK_iMaVBDi}" class="tin">{$di.sMoTa}</a></p>
            {foreach $dsfiledi as $file}
            <p style="margin-bottom: 3px; text-align:right"><a style="color:brown;" href="{$file.sDuongDan}" target="_blank">{$file.sTenFile}</a></p>
            {/foreach}
            </td>
            </tr>
            {/foreach}
            </tbody>
            </table>
            </div>
            </div>
            {/if}
            {/if}
            <div class="col-md-12" style = "text-align:center">							
            {if $getdAppointment[0]['iTrangThai'] != 1  && $vanban['iQuyenHan_DHNB'] < 6}
            <div class="col-sm-12" style="margin-bottom:20px; margin-top: 10px">
            <div class="col-sm-8" style="margin-left: 16%">
            <div class="form-group">
            <label for="" class="col-sm-12">Lãnh đạo có ý kiến chỉ đạo: </label>
            <div class="col-sm-12" style="display: inline-flex">
            <textarea name="chidao" id="" class="form-control" rows="3" placeholder="Ý kiến chỉ đạo"></textarea>
            <button class="btn btn-primary" type="submit" name="luulai" value="Chỉ đạo" style="margin-left: 5px">Gửi</button>
            </div>
            </div>
            </div>
            </div>
            {else}
            {if $getdAppointment[0]['iTrangThai'] != 1}
            <div class="col-sm-12" style="margin-bottom:20px; margin-top: 10px">
            <div class="col-sm-8" style="margin-left: 16%">
            <div class="form-group">
            <label for="" class="col-sm-12">Ý kiến / đề xuất </label>
            <div class="col-sm-12" style="display: inline-flex">
            <textarea name="dexuat" id="" class="form-control" rows="3" placeholder="Nhập ý kiến / đề xuất"></textarea>
            <button class="btn btn-primary" type="submit" name="luuykien" value="Lưu lại" style="margin-left: 5px">Gửi</button>
            </div>
            </div>
            </div>
            </div>
            {/if}
            {/if}							
            </div>
            </div>
         </div>
         </form>
      </div>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>
<script>
   $(document).ready(function() {
       $('.control-label').css('text-align', 'left');
   $('.form-group').css('margin-bottom', '0px');
   });
</script>