<div class="content-wrapper">
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1" style="overflow-x: auto;">
                <div class="col-sm-3">
                    <h3 class="font">
                        Văn bản hoàn thành <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Văn bản :</label>

                                <div class="col-sm-8 sl2">
                                    <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="this.form.submit()">
										<option value="0" {!empty($cacloaivanban)&&($cacloaivanban)?($cacloaivanban=="0")?'selected':'':''}>Tất cả</option>
                                        <option value="1" {!empty($cacloaivanban)&&($cacloaivanban)?($cacloaivanban=="1")?'selected':'':''}>Chưa duyệt</option>
                                        <option value="2" {!empty($cacloaivanban)&&($cacloaivanban)?($cacloaivanban=="2")?'selected':'':''}>Đã duyệt</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade phongphoihop" id="myModal4" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Trả lại kết quả</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nội dung trả lại</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Chưa đạt yêu cầu ..."></textarea>
                                    </div>
                                </div>
                                <input type="text" name="iddoc" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Từ chối kết quả">Trả lại</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <span class="pull-left">{$phantrang}</span>
                        {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11 || $vanban['iQuyenHan_DHNB'] == 10}
						{if $cacloaivanban != 2}
                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button> 
						{/if}
                        {/if}
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="10%" class="text-center">Số đến</th>
                                <th width="" class="text-center">Trích yếu - Thông tin</th>
                                <!--<th width="10%" class="text-center">Trình tự xử lý</th>-->
                                {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11 || $vanban['iQuyenHan_DHNB'] == 10}
                                <th width="" class="text-center">
									{if $cacloaivanban != 2}<input type="checkbox" id="select_all"> Chọn hết {/if}
								</th>
                                {else}
                                <th width="20%" class="text-center">Trạng thái</th>
                                {/if}
                            </tr>
                            </thead>
                            <tbody>
                            {$i=1}
                            {foreach $getDocAwait as $pl}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td class="text-center"><p><b style="color: red; font-size: 20px">{$pl.iSoDen}</b></p>
								{date_select($pl.sNgayNhap)}
								</td>
                                <td>
									<p><b><a style="color:black;" href="{$url}quatrinhxuly/{$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe) && $pl.iTrangThai != 1}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01' && $pl.iTrangThai != 1}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
									
									- Số ký hiệu: {$pl.sKyHieu}<br>
									- Nơi gửi: {$pl.sTenDV}<br>
                                    <p>{if $pl.sHanThongKe > '2017-01-01'}- Hạn giải quyết : <b>{date_select($pl.sHanThongKe)}</b>{/if}</p>
									
                                    <!--{if $pl.file_ketqua !='' and $pl.file_ketqua !='doc_uploads/' }<p><b>Kết quả</b>: <i><a target="_blank" href="{$pl.file_ketqua}"> [Xem file]</a></i></p>{/if}-->
									
									<p class="" style="text-align:right">
										{$dsfile=layDuLieu('FK_iMaVBDen',$pl.PK_iMaVBDen,'tbl_files_vbden')}
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a><br>
											{foreach $dsfile as $f}
												<p style="margin-bottom:3px;text-align:right"><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{if strlen($f.sTenFile) < 60}{$f.sTenFile}{else}{substr($f.sTenFile,0,60)}...{/if}</a></p>
											{/foreach}
                                        </p>
										
										 <p>* Nội dung hoàn thành của đ/c <b>{$pl.Ten_NHT}</b>: </p>
									<p><b style="color:blue">{$pl.MoTa_NHT}</b></p>
									{if !empty($pl.lydochammuon)}<p><b>Lý do chưa kết thúc văn bản</b>: <i>{$pl.lydochammuon}</i></p>{/if}
                                    
								</td>
                                <!--<td width="15%">
                                    {$a=1}
                                    {foreach $pl.TrinhTu as $pr}
                                    <p>{$a++}. {$pr}</p>
                                    <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                    {/foreach}
                                    <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden">
                                    {if $pl.vbdaura_tp == 1}
										<span style="color: blue;"><b>Lãnh đạo yêu cầu có văn bản trả lời</b></span
                                    {/if}
                                </td>-->
                                <td class="text-center" width="12%">
                                    {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11 || $vanban['iQuyenHan_DHNB'] == 10}
										{if $pl.iTrangThai == 2}
                                    <p><input type="checkbox" name="duyet[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="duyet"> Chọn duyệt </p>
									<input type="checkbox" name="cosangtao[{$pl.PK_iMaVBDen}]" value="1" > Có sáng tạo
                                            <br>
											{/if}
											<!--{if $pl.vbdaura_cv == 1}
                                            <span style="color: red;"> Có văn bản trả lời: <input type="checkbox" value="1" checked  disabled="">                              
                                            {else}<span style="color: red;"> Chỉ để lưu: <input type="checkbox" checked disabled="">
                                            {/if}    
											<br>-->
                                            <input type="hidden" name="vbdaura_cv[{$pl.PK_iMaVBDen}]" value="{$pl.vbdaura_cv}">
                                            <input type="hidden" name="luukho[{$pl.PK_iMaVBDen}]" value="{$pl.sLuuVaoKho}">
											<input type="hidden" name="canbo" value="{$pl.PK_iMaCBHoanThanh}"><br>
                                            <p><button type="button" name="tuchoikq"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs"  data-toggle="modal" data-target="#myModal4" style="margin-bottom: 10px;"><i class="fa fa-reply" aria-hidden="true"></i> Trả lại</button></p>
                                    {else}
                                            {if $pl.iTrangThai == 1}
                                            <p><a href="javascript:void(0)" class="btn btn-success btn-xs" value="Duyệt" style="margin-bottom: 10px"><i class="fa fa-check"></i> Đã duyệt</a></p>
                                            {if $pl.iCoSangTao == 1}
                                            <br><span style="color: red;"> <input type="checkbox" name="" value="1" ($pl.iCoSangTao == 1) ? checked : NULL> Có sáng tạo <br>
                                            {/if}
                                            <!-- {if $pl.sLuuVaoKho == 1}
                                            <br><span style="color: red;"> Chỉ để lưu: <input type="checkbox" name="" value="1" ($pl.sLuuVaoKho == 1) ? checked : NULL><br>
                                            {/if}-->
                                            {else if $pl.iTrangThai == 2}
												<p><button type="button" class="btn btn-xs btn-primary" style="background-color: #00a65a;border-color: #00a65a;"><i class="fa fa-refresh" aria-hidden="true"></i> Chờ duyệt</button></p>                                             
												<!--{if $pl.sLuuVaoKho == 1}
                                            <br><span style="color: red;"> Chỉ để lưu: <input type="checkbox" name="" value="1" ($pl.sLuuVaoKho == 1) ? checked : NULL><br>
                                            {/if}-->
                                            {/if}
                                    {/if}
                                    <!--<b style="color: blue; margin-top:5px"> {$dscanbo[$pl.PK_iMaCBDuyet]} </b>-->
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11|| $vanban['iQuyenHan_DHNB'] == 10}
						{if $cacloaivanban != 2}
                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
						{/if}
                        {/if}
						 <span class="pull-left">{$phantrang}</span>
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script>
    $("#select_all").click(function(){
        var checked_status = this.checked;
        $(".duyet").each(function(){
            this.checked = checked_status;
        });
    });
    $("button[name=tuchoikq]").each(function (i, e) {
        $(this).click(function () {
            $('input[name=iddoc]').val($(this).val());
        });
    });
</script>
