<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách ý kiến, đề xuất <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-sm-9">
                    
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
						<span class="pull-right">Để đánh dấu đã xem, nhấn nút "Đã xem".</span>
                            <span class="pull-right">{$phantrang}</span>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th width="" class="text-center">Trích yếu - Thông tin</th>
                                    <th width="55%" class="text-center">Ý kiến, đề xuất</th>
									<th width="6%" class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i=1}
                                {if !empty($dsykien)}

                                {foreach $dsykien as $yk}

                                <tr>

                                    <td class="text-center">{$i++}</td>

                                    <td><!--{if (date('Y-m-d',time()) <= $yk.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $yk.sHanThongKe)} style="background: #fffbd1;" {/if}-->

                                        <p><b><a style="color: black;" href="{$url}{if $yk.iGiayMoi == 1}quytrinhgiaymoi{else}quatrinhxuly{/if}/{$yk.FK_iMaVBDen}">{if (date('Y-m-d',time()) <= $yk.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $yk.sHanThongKe)}<span style="color: #FF9800;font-weight: bold;">{$yk.sMoTa}</span>{elseif $yk.sHanThongKe < date('Y-m-d',time()) && $yk.sHanThongKe > '2017-01-01'}<span style="color: red">{$yk.sMoTa}</span>{else}<span>{$yk.sMoTa}</span>{/if}</a></b></p>
										
                                        <p>- Số đến: <b style="color:red">{$yk.iSoDen}</b><br>
										{if $yk.sHanThongKe > '2017-01-01'}- Hạn giải quyết : </b>{date_select($yk.sHanThongKe)}{/if}</p>

                                    </td>
                                    
                                    <td>
										{if $yk.iDaXem != 1}
											<p><b style="color:blue">{$yk.sNoiDung}</b> </p> <b>{$yk.sHoTen}</b> - {date_time2($yk.sThoiGian)}
										{else}
											<p>{$yk.sNoiDung} </p> <b>{$yk.sHoTen}</b> - {date_time2($yk.sThoiGian)}
										{/if}
									</td>
									
									<td class="text-center">
										{if $yk.iDaXem != 1}
											<button type="submit" name="daxem" class="btn btn-primary btn-xs" value="{$yk.id}">Đã xem</button>
										{/if}
									</td>
                                </tr>

                                {/foreach}

                                {/if}

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
