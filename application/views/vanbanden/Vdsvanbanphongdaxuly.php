<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách phòng đã xử lý
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Loại văn bản:</label>

                                <div class="col-sm-7 sl2">
                                    <div class="row">
                                        <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="javascript:handleSelect(this)">
                                            <option value="dsphongchutri" >-- Tất cả --</option>
                                            <option value="vanbanchoxuly_tp">Văn bản mới nhận</option>
                                            <option value="dsphoihop">Phối hợp giải quyết</option>
                                            <!--<option value="dsphongchutri?cacloaivanban=1">Đã chỉ đạo chưa giải quyết</option>-->
                                            <option value="dsvanbanphongdaxuly" {!empty($urlcv)&&($urlcv)?($urlcv=="dsvanbanphongdaxuly")?'selected':'':''}>Đã giải quyết</option>
                                        </select>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    function handleSelect(elm)
                                    {
                                        window.location = elm.value;
                                    }
                                </script>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> <i class="fa fa-search"></i> Tìm kiếm</a><b>({$count} văn bản)</b></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <span class="pull-right">{$phantrang}</span>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="" class="text-center">Ngày nhập</th>
                                <th class="text-center">Số đến</th>
                                <th class="text-center">Số ký hiệu</th>
                                <th width="10%"class="text-center">Nơi gửi</th>
                                <th width="35%" class="text-center">Trích yếu - Thông tin</th>
                                <th width="" class="text-center">Trình tự xử lý</th>
                                <th width="" class="text-center">Tệp tin</th>
                            </tr>
                            </thead>
                            <tbody>
                            {$y=1}
                            {foreach $getDocAwait as $pl}
                            <tr>
                                <td class="text-center">{$y++}</td>
                                <td class="text-center">{date_select($pl.sNgayNhap)}</td>
                                <td><b style="color: red; font-size: 16px">{$pl.iSoDen}</b></td>
                                <td>{$pl.sKyHieu}</td>
                                <td>{$pl.sTenDV}</td>
                                <td>
                                    <p><b><a style="color: black;" href="{$url}chuyenvienchutrixuly?vbd={$pl.PK_iMaVBDen}">{$pl.sMoTa}</a></b></p>
                                    <p>(<b>Hạn giải quyết : </b>{date_select($pl.sHanGiaiQuyet)})</p>
                                </td>
                                <td class="chidao">
                                    {$i=1}
                                    {foreach $process as $pr}
                                    {if $pr.PK_iMaVBDen == $pl.PK_iMaVBDen}
                                    <p>{$i++}. {$pr.sHoTen}</p>
                                    <hr style="border-bottom: 1px dashed gold; margin: 5px">
                                    {/if}
                                    {/foreach}
                                </td>
                                <td class="text-center">
									
									<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}
								
								</td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly_tp.js"></script>
