<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách email Chi ủy Sở Ngoại vụ - Lần cuối hệ thống cập nhật email: {date_time($lastfetch)}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="" class="form-horizontal" method="get">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-2"><a href="{$url}readmaildang?trangthai=1" class="btn {if $trangthai==1}btn-primary {else}btn-default{/if}">Email đã xem</a></div>
                                    <div class="col-md-2"><a href="{$url}readmaildang?trangthai=0" class="btn {if $trangthai==0}btn-primary {else}btn-default{/if}">Email chưa xem</a></div>
									<div class="col-md-2">
										{if date('Y') == $year}
											<a href="{$url}readmaildang" class="btn btn-primary">Tải Email về hệ thống</a>
										{else}
											Chọn năm hiện tại để tải mail và tạo văn bản đến
										{/if}
									</div>
                                    <div class="col-md-4">
                                        <input type="text" class="hide" name="trangthai" value="{($trangthai)?$trangthai:1}">
                                        <input name="chude" type="text" value="{($chude)?$chude:''}" class="form-control" placeholder="Nhập chủ đề tìm kiếm">
                                    </div>
                                    <div class="col-md-1"><button class="btn btn-primary">Tìm kiếm</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto">
				<div class="col-sm-10 deletepadding">{$phantrang}</div>
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:50px" class="text-center">STT</th>
                                    <th width="">Chủ đề</th>
                                    <th width="15%">Nơi gửi</th>
                                    <th width="10%" class="text-center" >Thời gian</th>
									<!--<th width="8%" class="text-center" >Đính kèm</th>-->
                                    <th width="5%"></th>
                                    <th width="10%" class="text-center" >Tình trạng</th>
									{if $vanban['PK_iMaCB'] == 705}
                                    <th width="8%" class="text-center" ><button name="xoanhieu" value="xoanhieu" class="btn btn-xs btn-danger" style="padding: 2px 10px">Xóa</button>
                                    
										<input type="checkbox" name="select_all"  id="select_all"></th>
									{/if}
                                   
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsmail)}{$i=1}
                                    {foreach $dsmail as $m}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td><p>{$m.mail_subject}</p>
												{$dsfiles = explode(';', $m.mail_files)}
												{foreach $dsfiles as $file}
													{$file = trim($file, ' ')}
													{if strpos($file, '.sdk') == false}
														<p style="margin-bottom:3px"><a href="emaildangFile_{date("Y",strtotime($m.mail_date))}/{$file}" target="_blank" style="color:brown">{substr($file, 13, 70)}</a></p>
													{/if}
												{/foreach}
											</td>
                                            <td>{($dsdonvi[$m.mail_from])?$dsdonvi[$m.mail_from]:$m.mail_from}</td>
                                            <td class="text-center">{date_time($m.mail_date)}</td>
											<!--<td class="text-center"><a href="emaildangFile_2020/{$m.mail_pdf}" target="_blank">Xem</a></td>-->
                                            <td class="text-center">
												{$m.mail_from = str_replace('"','')}
												{if date('Y') == $year}
													<a href="{$url}vanbandentumaildang?id={$m.mail_id}&xml={$m.mail_attachment}&pdf={$m.mail_pdf}&mail={$m.mail_from}" class="tin"><button type="button" class="btn btn-success btn-xs" title="">Tạo</button></a>
												{/if}
											
											</td>
                                            <td class="text-center"><span class="label label-{($m.mail_status == 1)?'success':'warning'}" style="padding:8px">
												{if $m.mail_status == 0}Chưa xem{else}Đã xem{/if}</span>
											
											</td>
											{if $vanban['PK_iMaCB'] == 705}
                                            <td class="text-center"><button type="submit" name="xoa" value="{$m.mail_id}" onclick="return confirm('Bạn có muốn xóa email này?')" class="btn btn-default btn-xs"><i class="fa fa-trash-o"></i></button>
                                                <input type="checkbox" name="mangxoa[]" value="{$m.mail_id}">
                                            </td>
											{/if}
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="col-sm-10 deletepadding">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('#select_all').change(function() {
            var checkboxes = $(this).closest('form').find(':checkbox');
            checkboxes.prop('checked', $(this).is(':checked'));
        });
    });
</script>