<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header" style="padding: 0px">
      <div class="row">
         <div class="col-sm-12 deletepadding1">
            <div class="col-sm-3" style="width:20%">
               <h3 class="font">
                  DS văn bản đến <b> ({$count})</b>
               </h3>
            </div>
            <div class="col-sm-12" style="width:80%;">
               <div class="col-md-8 tieude">
                  <form action="" method="get">
                     <div class="form-group">
                        <div class="col-sm-8" style="width: 80%; margin-bottom: 10px">
                           <input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm tổng hợp theo số đến, trích yếu, ký hiệu, ...">
                        </div>
                     </div>
                  </form>
                  <!--<form action="" method="get">    
                     <div class="form-group">
                     	<div class="col-sm-8" style="width: 25%; margin-bottom: 10px">
                     		<input type="text" name="soden" class="form-control" id="soden" value="{($soden)?$soden:''}" placeholder="Tìm theo số đến">
                                              </div>
                                          </div>		
                     </form>
                     <form action="" method="get">    
                     <div class="form-group">
                     	<div class="col-sm-8" style="width: 35%; margin-bottom: 10px">
                     		<input type="text" name="trichyeu" class="form-control" id="trichyeu" value="{($trichyeu)?$trichyeu:''}" placeholder="Tìm theo trích yếu">
                                              </div>
                                          </div>		
                     
                                      </form>-->
               </div>
               <div class="col-md-4 abc">
                  <div class="form-group">
                     <label for="" class="col-sm-12 control-label"><a class="font {if $vanban['iQuyenHan_DHNB']==9 || $vanban['iQuyenHan_DHNB']==2} btn btn-primary btn-xs {/if}" {if $vanban['iQuyenHan_DHNB']==9 || $vanban['iQuyenHan_DHNB']==2} style="font-size: 12px!important" {/if} data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Nâng cao</a></label>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Default box -->
      <div class="box">
         <div class="box-body">
            <div class="row">
               <div class="col-sm-12">
                  <div class="collapse" id="collapseExample">
                     <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                        <div class="row">
                           <div class="col-md-12">
                              <form action="{$url}dsvanbanden" target="_blank" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                                       <div class="col-sm-8">
                                          <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                             <option value="">-- Chọn loại văn bản --</option>
                                             {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                             <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                             {/foreach}
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Số ký hiệu</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="sokyhieu" class="form-control" id="" value="{($sokyhieu)?$sokyhieu:''}">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{($ngaynhap > '2017-01-01')?(date_select($ngaynhap)):''}" id="">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Năm</label>
                                       <div class="col-sm-8">
                                          <select name="nam" id="" class="form-control">
                                             <option value="">--- Chọn năm văn bản ---</option>
                                             <option value="{date('Y')}" {!empty($nam)&&($nam)?($nam == date('Y'))?'selected':'':''}>{date('Y')}</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>
                                       <div class="col-sm-8">
                                          <div class="typeahead__container">
                                             <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                <input tabindex="1" type="text" class="donvi form-control" value="{!empty($dv.sTenDV) &&($dv.sTenDV) ? $dv.sTenDV : NULL }" name="donvi" placeholder="Nhập nơi gửi đến">
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Ngày ký</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="ngayky" class="form-control datepic datemask" value="{($ngayky > '2017-01-01')?(date_select($ngayky)):''}" id="">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="ngayden" value="{($ngayden > '2017-01-01')?(date_select($ngayden)):''}" class="form-control datepic datemask" id="">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Phòng chủ trì</label>
                                       <div class="col-sm-8">
                                          <select name="sPhongChuTri" id="" class="form-control select2" style="width:100%" >
                                             <option value="">-- Chọn phòng chủ trì --</option>
                                             {foreach layDuLieu(PK_iMaPB,11,'tbl_phongban') as $pb}
                                             <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                             {/foreach}
                                             {foreach layDuLieu(PK_iMaPB,74,'tbl_phongban') as $pb}
                                             <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                             {/foreach}
                                             {foreach layDuLieu(PK_iMaPB,75,'tbl_phongban') as $pb}
                                             <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                             {/foreach}
                                             {foreach layDuLieu(PK_iMaPB,76,'tbl_phongban') as $pb}
                                             <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                             {/foreach}
                                             {foreach layDuLieu(PK_iMaPB,77,'tbl_phongban') as $pb}
                                             <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                             {/foreach}
                                             {foreach layDuLieu(PK_iMaPB,78,'tbl_phongban') as $pb}
                                             <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                             {/foreach}                                                            
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <div class=" col-md-12">
                                       <div class="form-group">
                                          <label for="" class="control-label">Trích yếu</label>
                                          <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="ngaymoi" class="form-control datepic datemask" id="" value="{($ngaymoi > '2017-01-01')?(date_select($ngaymoi)):''}">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Số đến</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="soden" class="form-control" id="" value="{($soden)?$soden:''}">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Đến ngày</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="denngay" class="form-control datepic datemask" id="" value="{($denngay > '2017-01-01')?(date_select($denngay)):''}">
                                       </div>
                                    </div>
                                    <!--<div class="form-group">
                                       <label for="" class="col-sm-4 control-label">Đã hoàn thành</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" name="ihoanthanh" value="1" {if $ihoanthanh == 1}checked{/if}>
                                       </div>
                                    </div>-->
                                 </div>
                                 <div class="col-md-12" style="text-align:center; margin-top: 10px">
                                    <button class="btn btn-primary btn-sm">Tìm kiếm</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-5 deletepadding">
               {$phantrang} 
            </div>
            <div class="col-sm-7 pull-right" style>
               <a href="dsvanbanden" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;" >ALL</button></a>
			   
               <a href="dsvanbanden?domat=2&ngaynhap={$ngaynhap}&ngayden={$ngayden}&tonghop={$tonghop}&id={$id}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px;{if $domat == 2}background-color:orange{/if}" >VB MẬT</button></a>
			   
               <a href="dsvanbanden?cacloaivanban=9&ngaynhap={$ngaynhap}&ngayden={$ngayden}&tonghop={$tonghop}&id={$id}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px;{if $cacloaivanban==9}background-color:orange{/if}" >VB TTHC</button></a>
			   
               <a href="dsvanbanden?cacloaivanban=6&ngaynhap={$ngaynhap}&ngayden={$ngayden}&tonghop={$tonghop}&id={$id}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px;{if $cacloaivanban==6}background-color:orange{/if}" >VB TBKL</button></a>
			   
               <a href="dsvanbanden?cacloaivanban=10&ngaynhap={$ngaynhap}&ngayden={$ngayden}&tonghop={$tonghop}&id={$id}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px;{if $cacloaivanban==10}background-color:orange{/if}" >VB ĐẢNG</button></a>
			   
               <a href="dsvanbanden?ideadline=1&ngaynhap={$ngaynhap}&ngayden={$ngayden}&tonghop={$tonghop}&id={$id}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px; {if $ideadline}background-color:orange{/if}" >VB DEADLINE</button></a>
			   
			   {if $ideadline==1}
			   <a href="dsvanbanden?ideadline=1&ngaynhap={$ngaynhap}&ngayden={$ngayden}&tonghop={$tonghop}&id={$id}&ihoanthanh={if $ihoanthanh == 1}0{else}1{/if}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px; {if $ihoanthanh == 1}background-color:orange{/if}" >Đã hoàn thành</button></a>
			   <a href="dsvanbanden?ideadline=1&ngaynhap={$ngaynhap}&ngayden={$ngayden}&tonghop={$tonghop}&id={$id}&ihoanthanh={if $ihoanthanh == 2}0{else}2{/if}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px; {if $ihoanthanh==2}background-color:orange{/if}" >Chưa hoàn thành</button></a>
				{/if}
            </div>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
               <div class="row">
                  <div class="col-md-12" style=" width: 100%;overflow-x: auto;">
                     <table id="" class="table table-bordered table-striped table-responsive">
                        <thead>
                           <tr>
                              <th width="50px" class="text-center">STT</th>
                              <th width="10%" class="text-center">Số đến</th>
                              <th width="" class="text-center">Trích yếu</th>
                              <th width="15%" class="text-center">Nơi gửi</th>
                              <th width="15%" class="text-center">Phòng chủ trì</th>
                              {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 9 || $vanban['PK_iMaCB'] == 705 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731}
                              <th width="5%" class="text-center"></th>
                              {/if}
                           </tr>
                        </thead>
                        <tbody>
                           {$i = 1}
                           {$i = $i + $page}
                           {foreach $getDocGo as $vbd}
                           <tr>
                              <td class="text-center">{$i++}</td>
                              <td class="text-center">
                                 <p><b style="color: red; font-size: 20px">{if $vbd.iSoDen <10}0{$vbd.iSoDen}{else}{$vbd.iSoDen}{/if}</b></p>
                                 {date_time2($vbd.sNgayNhap)}
                              </td>
                              {if $vbd.iTrangThai == 1}
                              <td style="background: url(/vpdtsongoaivu/files/images/hoanthanh.png) right no-repeat;vertical-align: middle;background-size: 12%;">{else} 
                              <td>
                                 {/if}
                                 {if $vbd.iGiayMoi == '1'}
									 <p>
										<a href="{$url}quytrinhgiaymoi/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b">
									 <p><b>{$vbd.sMoTa}</b></p></a>
									 - Thời gian: <b style="color: blue">{$vbd.sGiayMoiGio}</b> ngày <b style="color: blue">{date_select($vbd.sGiayMoiNgay)}</b>, tại <b style="color: blue">{$vbd.sGiayMoiDiaDiem}</b></p>
                                 {else}
									 {if $vbd.iTrangThai == 1}
										 <a href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b">
											<p><b>
											   {if $vbd.sNgayGiaiQuyet > $vbd.sHanThongKe}
											   <b style="color: red">{$vbd.sMoTa}</b>
											   {else}
											   <b>{$vbd.sMoTa}</b>
											   {/if}</b>
											</p>
										 </a>
									 {else}
										<p><a style="color: black;" href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}">{if (date('Y-m-d',time()) <= $vbd.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $vbd.sHanThongKe)}<b style="color: orange;font-weight: bold;">{$vbd.sMoTa}</b>{elseif $vbd.sHanThongKe < date('Y-m-d',time()) && $vbd.sHanThongKe > '2017-01-01'}<b style="color: red">{$vbd.sMoTa}</b>{else}<b>{$vbd.sMoTa}</b>{/if}</a></p>
									 {/if}
                                 {/if}
                                 - Số ký hiệu: {$vbd.sKyHieu} <br>
                                 {if !empty($vbd.sTenLV)}- Hashtag: {$vbd.sTenLV}<br>{/if}
                                 {if $vbd.FK_iMaDM > 1} {$domat = layDoMat($vbd.FK_iMaDM)}  - Độ mật: <b style="color:red; font-size:16px">{$domat[0].sTenDoMat}</b><br>{/if}
                                 {if $vbd.FK_iMaDK > 1} {$dokhan = layDoKhan($vbd.FK_iMaDK)}  - Độ khẩn:<b style="color:red; font-size:16px">{$dokhan[0].sTenDoKhan}</b><br>{/if}
                                 <p>- Người nhập: {$vbd.sHoTen}<br>
                                    {if $vbd.iSTCChuTri == 1 || $vbd.iVanBanTBKL == 1 || $vbd.iToCongTac == 1 || $vbd.iSTCPhoiHop == 1 || $vbd.iVanBanQPvbd == 1}
                                 <h5>- Loại VB: <b style="color: blue"> {($vbd.iSTCChuTri == 1) ? 'SNV chủ trì ' : NULL }{($vbd.iVanBanTBKL == 1) ? 'TBKL ' : NULL } {($vbd.iToCongTac == 1) ? 'TTHC ' : NULL }{($vbd.iSTCPhoiHop == 1) ? 'VB Đảng ' : NULL }{($vbd.iVanBanQPPL == 1) ? 'QPPL ' : NULL }</b></h5>
                                 {/if}
                                 </p>
                                 <p>{if !empty($vbd.iDeadline) && $vbd.iDeadline == 1}
                                    <label for="" class="control-label" style=""><b style=" color:blue">LĐ giao có thời hạn</b>: <b style="color:red">{date_select($vbd.sHanThongKe)}</b></label>
                                    {else}
                                    <label for="" class="control-label" style="">Hạn xử lý: <b style="">{date_select((!empty($vbd.sHanThongKe))?$vbd.sHanThongKe:$vbd.sHanGiaiQuyet)}</b></label>
                                    {/if}
                                 </p>
                                 <span><i>{if !empty($vbd.sNoiDung)}- Nội dung*: <b>{$vbd.sNoiDung}{/if}</b></i></span>
                                 <p><span style="float:right"><a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a>{if $vbd.iFile==1} | <a target="_blank" href="{$vbd.sDuongDan}" class="tin1">{if $vbd.iMail == 1} Xem file{else} Xem file{/if}</a>{/if}</span></p>
                              </td>
                              <td class="text-center">{$vbd.sTenDV}</td>
                              <td class="text-center">
                                 <p>{$vbd.sPhongChuTri}
                                    {$pagination.page}		
                                 </p>
                                 {if !empty($vbd.VBDi)}
                                 <input type="hidden" name="sNgayGiaiQuyet_{$vbd.PK_iMaVBDen}" value="{$vbd.sNgayGiaiQuyet}">
                                 <!--<input type="button" name="danhgia" id="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#VPtraketqua" class="btn-warning" value="Trả lại">
                                    <br><br>-->
                                 <b style="color: blue;">Có văn bản đi</b>
                                 <a href="dsvanban?kyhieu={$vbd.VBDi.iSoVBDi}"><b style="color: red; font-size: 16px">{$di.iSoVBDi}</b></a>
                                 {else}
                                 {if $vbd.iTrangThai == 1}
                                 <b>Đã giải quyết</b>
                                 {else}
                                 <b style="color: orange;">Chưa hoàn thành</b>
                                 {/if}
                                 {/if}										
                              </td>
                              {if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 9 || $vanban['PK_iMaCB'] == 705 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731}
							  
                              <td class="text-center" >
								{if date('Y') == $year} 
									 {if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['PK_iMaCB'] == 705 || empty($vbd.sPhongChuTri)}
									 <button type="submit" value="{$vbd.PK_iMaVBDen}" name="xoavanban" onclick="return confirm('Bạn muốn xóa văn bản đến số {$vbd.iSoDen} ?')" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Xóa" style="margin-top: 20px"><i class="fa fa-trash"></i></button>
									 {/if}
									 <p><a href="{if $vbd.iGiayMoi == '1'}{$url}nhapgiaymoi/{$vbd.PK_iMaVBDen}{else}{$url}vanbanden/{$vbd.PK_iMaVBDen}{/if}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="sửa văn bản" style="margin-top: 20px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></p>
								{/if}
                              </td>
                              {/if}
                           </tr>
                           {/foreach}
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="col-sm-12 deletepadding">
                  {$phantrang}
               </div>
            </form>
         </div>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</div>
<script>
   function myFunction() {
       window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
   }
</script>
<script>
   var data = {
   "noiden": [
       {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
       "{$dv.sTenDV}",
       {/foreach}
   ]
   
   };
   typeof $.typeahead === 'function' && $.typeahead({
       input: ".donvi",
       minLength: 1,
       maxItem: 10,
       order: "asc",
       hint: true,
       backdrop: {
           "background-color": "#fff"
       },
       source: {
           noiden: {
               data: data.noiden
           }
       },
       debug: false
   });
   
   // trả lại văn bản để phòng thực hiện lại
   
   $(document).on('click','input[name=danhgia]',function(){
           var id = $(this).attr('id');
           var ngay_han = $('input[name=sNgayGiaiQuyet_'+id+']').val();
           //alert(id);alert(ngay_han);
           $('input[name=PK_iMaVBDen]').val(id);
           $('input[name=sNgayGiaiQuyet]').val(ngay_han);
       });
</script>