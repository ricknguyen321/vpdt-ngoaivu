<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="font">
           
        </h1>
    </section>
	<h3 style="text-align:center; font-size:30px; margin-top:-5px">
        GIẤY MỜI
    </h3>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenKV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Số ký hiệu <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }</p>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">	Ngày ký <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : NULL }</p>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 ">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <p><b><i>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</b></i></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr class="info">
                                        <td><p class="col-sm-12"><b>Thời gian họp:</b> <b style="color:blue">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiGio'] : NULL }</b>  <span class="text-info" style="color:black">ngày </span> <b style="color:blue">{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sGiayMoiNgay']) : NULL }</b></p></td>
                                    </tr>
                                    <tr class="info">
                                        <td><p class="col-sm-7"><b>Địa điểm:</b>	<b style="color:blue"> {!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiDiaDiem'] : NULL }</b> 	<b></p><p class="col-sm-5">Người chủ trì:</b>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiChuTri'] : NULL } </p></td>
                                    </tr>
                                    <tr class="info">
                                        <td>
                                            <label for="" class="col-sm-2 ">Nội dung</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="" required value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sNoiDung'] : NULL } ">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
							
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }</label>
                                    </div>
                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
										<p>
												{if $getdAppointment[0]['FK_iMaDM'] == 2} <b style="color:red; font-size:16px">Mật</b> {else}
													{if $getdAppointment[0]['FK_iMaDM'] == 3} <b style="color:red; font-size:16px">Tối mật</b> {else}
													{if $getdAppointment[0]['FK_iMaDM'] == 4} <b style="color:red; font-size:16px">Tuyệt mật</b> {else}
													Bình thường {/if}{/if}{/if}
										</p>
                                    </div>

                                    <label for="" class="col-sm-1">Độ khẩn:</label>
                                    <div class="col-sm-2">
                                        <p>
											{if $getdAppointment[0]['FK_iMaDK'] == 2} <b style="color:orange; font-size:16px"> Khẩn </b>{else}
													{if $getdAppointment[0]['FK_iMaDK'] == 3} <b style="color:red; font-size:16px">Tối khẩn </b> {else}
													Bình thường {/if}{/if}

										</p>
                                    </div>
                                </div>
                            </div>
                           
							
							<div class="col-sm-12">

                                <div class="form-group">
								
								<label for="" class="col-sm-2 control-label" style="width:50%; font-size:16px;margin-top:20px"><a href="teptinden?id={$getdAppointment[0]['PK_iMaVBDen']}"><b>Tài liệu <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></b></a> &larr; Nhấn vào tài liệu và tải tài liệu phục vụ họp</label>

                                    <table class="table table-bordered" style="width: 70%">

                                        <thead>

                                        <tr>

                                            <th class="text-center" style="width: 50px">STT</th>

                                            <th style=""></th>

                                            <th class="text-center" style="width:20%">Thời gian</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}
                                        {if !empty($Filedinhkem)}
                                        {foreach $Filedinhkem as $fi}

                                        <tr>

                                            <td class="text-center">{$i++}</td>

                                            <td><a style="color:brown" href="{$fi.sDuongDan}" target="_blank">{$fi.sTenFile}</a></td>

                                            <td class="text-center" width="15%">{date_time2($fi.sThoiGian)}</td>

                                        </tr>

                                        {/foreach}
                                        {/if}
                                        </tbody>

                                    </table>

                                </div>

                            </div>
							
                            {if !empty($idDocGo)}

                            <div class="col-sm-12">
                                <div class="form-group">
                                   <b style="margin-left:20px"> Trình tự giải quyết:</b>
										{if !empty($process)}{$i=1}
								{foreach $process as $pr}
                                    <b style="font-size:14px; {if $i == count($process)} color:blue {/if}"> {$pr.sHoTen} </b> {if $i <count($process)} 	&rarr; {/if}
									{$i = $i + 1}
                                {/foreach}
                               
								{else} <p>----</p> {/if}
									
									{if !empty($getdAppointment[0]['iDeadline']) && $getdAppointment[0]['iDeadline'] == 1}
										<label for="" class="col-sm-4 control-label" style="float:right; "><b style=" color:blue">Lãnh đạo giao có thời hạn</b>: <b style="font-size:16px; color:red">{date_select($getdAppointment[0]['sHanThongKe'])}</b></label>
										
									{else}
										<label for="" class="col-sm-4 control-label" style="float:right; ">Hạn xử lý: <b style="font-size:16px; color:red">{if !empty($getdAppointment[0]['sHanThongKe'])}
												{date_select($getdAppointment[0]['sHanThongKe'])}
											{else}
												{if $pr.sThoiGianHetHan > '2017-01-01'}{date_select($pr.sThoiGianHetHan)} {else}
													{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanGiaiQuyet'])}{/if}
												{/if}
											{/if}</b></label>
									{/if}
									
                                </div>
								
								
								{if !empty($process)}{$i=1}								
                                
								{else} <p>----</p> {/if}
                            </div>

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <table class="table table-bordered table-striped">

                                        <thead>

                                        <tr>

                                            <th class="text-center"  width="50px">STT</th>


                                            <th class="text-center" width="15%">Chuyển từ</th>

                                            <th width="">Chỉ đạo của lãnh đạo</th>

                                            <th class="text-center"  width="15%">Chuyển đến</th>

                                            <th class="text-center"  width="15%"><b style="font-size:16px;">Hạn LĐ giao</b></th>
											
											<th class="text-center"  width="15%">Ngày nhận</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $process as $pr}

                                        <tr class="info">

                                            <td class="text-center">{$i++}</td>

                                            <td class="text-center">

                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}

                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}

                                                {$cb.sHoTen}

                                                {/if}

                                                {/foreach}

                                            </td>

                                            <td>{$pr.sMoTa}</td>

                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">											
											<b style="font-size:16px; color:red">
											{if !empty($getdAppointment[0]['sHanThongKe'])}
												{date_select($getdAppointment[0]['sHanThongKe'])}
											{else}
												{if $pr.sThoiGianHetHan > '2017-01-01'}{date_select($pr.sThoiGianHetHan)} {else}
													{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanGiaiQuyet'])}{/if}
												{/if}
											{/if}
											</b>
											</td>
											 <td class="text-center">{date_time($pr.sThoiGian)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Trình tự chuyển nhận - lưu vết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th  width="50px" class="text-center">STT</th>
                                            
                                            <th  width="15%" class="text-center">Chuyển từ</th>
                                            <th width="">Nội dung</th>
                                            <th  width="15%" class="text-center">Chuyển đến</th>
											<th  width="15%" class="text-center">Ngày nhận</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $luuvetchuyennhan as $lvet}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>
                                            
                                            <td class="text-center">{$dscanbo[$lvet.FK_iMaCB_Chuyen]}</td>
                                            <td>{$lvet.sNoiDung}</td>
                                            <td class="text-center">{$dscanbo[$lvet.FK_iMaCB_Nhan]}</td>
											<td class="text-center">{date_time($lvet.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							
							{if !empty($chidao)}
									<div class="col-sm-12">
										<div class="form-group">
											<label for="" class="col-sm-4 control-label" style="color:blue">Chỉ đạo của lãnh đạo Sở:</label>
										</div>
										<div class="form-group">
											<table class="table table-bordered table-striped" style="width: 88%">
												<thead>
												<tr style="background-color: orange !important">
													<th class="text-center" style="width:50px">STT</th>
													<th class="text-center" style="width:17%">Lãnh đạo</th>
													<th>Nội dung</th>
													<th class="text-center" style="width:17%">Thời gian</th>
												</tr>
												</thead>
												<tbody>
												{$i=1}
												
												{foreach $chidao as $cd}
												{$ld = layTTCB($cd.FK_iMaCB)}
												<tr class="info">
													<td class="text-center">{$i++}</td>                                            
													<td class="text-center"><span style="color: red"> {$ld[0].sHoTen}</span></td>
													<td><span style="color: red"> {$cd.sNoiDung} </span></td>
													<td class="text-center">{date_time2($cd.sThoiGian)}</td>
												</tr>
												{/foreach}
												</tbody>
											</table>
										</div>
									</div>
								{/if}
								
								
							{if !empty($dexuat)}
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Đề xuất / ý kiến:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped" style="width: 88%">
                                        <thead>
                                        <tr style="background-color: orange !important">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="width:17%">Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th class="text-center" style="width:17%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										
                                        {foreach $dexuat as $dx}
										{$ld = layTTCB($dx.FK_iMaCB)}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>                                            
                                            <td class="text-center">{$ld[0].sHoTen}</td>
                                            <td>{$dx.sNoiDung}</td>
                                            <td class="text-center">{date_time2($dx.sThoiGian)} &nbsp; {if $dx.FK_iMaCB == $vanban['PK_iMaCB']}<button type="submit" value="{$dx.id}" name="xoadexuat" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							{/if}
							
							
                            {if !empty($resultPPH)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng phối hợp:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th width="50px">STT</th>

                                            <th width="20%">Cán bộ-đơn vị</th>

                                            <th width="">Nội dung</th>

                                            <th class="text-center" width="10%">Tài liệu</th>

                                            <th width="12%">Thời gian</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $resultPPH as $re}

                                        <tr >

                                            <td class="text-center">{$i++}</td>

                                            <td width="20%">{$re.sHoTen} - {$re.sTenPB}</td>

                                            <td>{$re.sMoTa}</td>

                                            <td width="8%" class="text-center">
												 {if $tl.sDuongDanFile !=''  && $tl.sDuongDanFile != 'doc_uploads/'}
													
                                                <a href="{$url}{$tl.sDuongDanFile}">Xem</a>
												{/if}
											</td>
											

                                            <td width="15%" class="text-center">{date_select($re.date_file)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            {/if}
                            {if !empty($resultPTL)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng chủ trì:</label>

                                </div>

                                <div class="form-group">
                                    <form action="" method="post">
                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>
                                            <th width="50px">STT</th>
                                            <th width="25%">Cán bộ - Đơn vị</th>
                                            <th width="">Nội dung</th>
                                            <th width="10%" class="text-center">Tài liệu</th>
                                            <th width="10%" class="text-center">Thời gian</th>
                                            <th width="10%" class="text-center">Tác vụ</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $resultPTL as $tl}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td >{$tl.sHoTen} - {$tl.sTenPB}</td>
                                            <td>{$tl.sMoTa}</td>
											{if !empty($tl.sDuongDanFile)}
                                            <td width="8%" class="text-center">
												 {if $tl.sDuongDanFile !=''  && $tl.sDuongDanFile != 'doc_uploads/'}
													
                                                <a href="{$url}{$tl.sDuongDanFile}">Xem</a>
												{/if}
											</td>
											{else}
											<td class="text-center"> - </td>
											{/if}
                                            <td  class="text-center">{date_select($tl.date_file)}</td>
                                            {if $tl.active == 2}
                                                <td class="text-center"><button type="submit" value="{$tl.PK_iMaKetQua}" name="xoafile" onclick="return confirm('Bạn muốn xóa {$tl.sMoTa} ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button></td>
											{else}
											<td class="text-center"> Đã duyệt</td>
											{/if}
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                    </form>
                                </div>

                            </div>
            
                            {/if}
                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-12 control-label">Trình tự hoàn thành công việc - lưu vết:</label>

                                </div>

                                <div class="form-group">
                                    <form action="" method="post">
                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

											<th  width="50px" class="text-center">STT</th>
                                            <th  width="15%" class="text-center">Ngày nhận</th>
                                            <th  width="20%" class="text-center">Chuyển từ</th>
                                            <th width="">Nội dung</th>
                                            <th  width="15%" class="text-center">Chuyển đến</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        
                                        {if !empty($luuvetduyet)}{$i=1}
                                        {foreach $luuvetduyet as $lvd}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_time($lvd.sThoiGian)}</td>
                                            <td class="text-center">{$dscanbo[$lvd.FK_iMaCB_Chuyen]}</td>
                                            <td>{$lvd.sNoiDung}</td>
                                            <td class="text-center">{($lvd.FK_iMaCB_Nhan>0)?$dscanbo[$lvd.FK_iMaCB_Nhan]:''}</td>
                                        </tr>
                                        {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                    </form>
                                </div>

                            </div>
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                            {if $getdAppointment[0]['iTrangThai'] != 1  || $getdAppointment[0]['PK_iMaCBHoanThanh'] == $vanban['PK_iMaCB']}
                            <div class="col-sm-12">

                                <div class="col-sm-6">

                                    <div class="form-group">

                                        <label for="" class="col-sm-12">Kết quả giải quyết <br><span style="color: red">  <i>(chỉ nhập khi có kết quả giải quyết)</i></span></label>

                                        <div class="col-sm-12">

                                            <textarea name="ketqua" id="" class="form-control" rows="4"></textarea>

                                        </div>

                                    </div>

                                </div>
                                {if $getdAppointment[0]['sHanGiaiQuyet'] <= date('Y-m-d',time())}
                                <br><br>
                                <!--<div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="form-group" id="themtailieu">
                                                <label for="" class="col-sm-12">
                                                    Chọn lãnh đạo chỉ đạo: <span class="text-danger">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="lanhdao" id="" class="form-control phogiamdoc1" style="cursor:pointer" required>
                                                        <option value=""> --- Chọn Lãnh đạo đã chỉ đạo văn bản ---</option>
                                                        <optgroup label="Chọn Trưởng phòng">
                                                            <option value="{$truongphong.PK_iMaCB}">{$truongphong.sHoTen}</option>
                                                        </optgroup>
                                                        <optgroup label="Chọn Phó phòng">
                                                            {foreach $getAccountDeputyDirector as $pgd}
                                                            <option value="{$pgd.PK_iMaCB}">{$pgd.sHoTen}</option>
                                                            {/foreach}
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-sm-6">
                                    <div class="form-group">
<!--                                        <label for="" class="col-sm-12">Lý do chưa hoàn thành <br> <span style="color: red"><i>(trường hợp chưa kết thúc được văn bản thì nhập lý do, tiến độ giải quyết-sau đó click vào ô "hoàn thành công việc")</i></span></label>-->
                                        <div class="col-sm-12">
<!--                                            <textarea name="chammuon" id="" class="form-control" rows="4"></textarea>-->
                                           <!-- <div class="form-group">
                                                <label for="" class="col-sm-12">
                                                    Báo cáo phục vụ họp: <span class="text-danger">*</span></label>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6">
                                                        <input type="text" name="fileketqua" class="form-control" placeholder="Tên file" value="Báo cáo" multiple="">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="file" name="files" value="">
                                                    </div>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="form-group noidung" id="themtailieu">
                                               
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6" >
													 <label for="" class="col-sm-12">
                                                    Đính tài liệu phục vụ họp:</span></label>
                                                        <input type="file" name="files[]" value="" multiple>
                                                    </div>
                                                </div>
                                                <script>
                                                    $(document).ready(function() {
                                                        //Date picker
                                                        $('.datepic').datepicker({
                                                            autoclose: true
                                                        });
                                                        //Datemask dd/mm/yyyy
                                                        $(".datemask").inputmask("dd/mm/yyyy");
                                                    });
                                                </script>
                                            </div>
                                            <div class="themsau">
                                            </div>
                                            <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs pull-right" style="background: green;color: white;padding: 4px 10px;"><i class="fa fa-plus"></i></button><b class="text-danger"> </b>
                                        </div>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            {/if}
                            {/if}
                            {if $getdAppointment[0]['iTrangThai'] != 1  || $getdAppointment[0]['PK_iMaCBHoanThanh'] == $vanban['PK_iMaCB']}
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-sm" type="submit" name="luulai" value="Lưu lại">Hoàn thành</button>
								<a href="{$url}soanbaocao/{$getdAppointment[0]['PK_iMaVBDen']}" target="_blank"  class="btn btn-danger btn-xs">Soạn BC KQ họp</a>
                            </div>
                            {/if}
                            </form>
                        </div>
                    </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
        $('.').css('text-align', 'left');
    });
</script>