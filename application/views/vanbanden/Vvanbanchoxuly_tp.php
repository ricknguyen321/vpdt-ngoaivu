<style>
    .btn-success:hover {
        color: #fff !important;
        background-color: #449d44;
        border-color: #398439;
    }
    .btn-success:hover, .btn-success:active, .btn-success.hover {
        background-color: #008d4c;
        color: #FFF !important;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3" style="width:30%">
                    <h3 class="font">
                        Danh sách văn bản chờ xử lý <b> ({$count})</b>
                    </h3>
                </div>
                <div class="col-sm-9"  style="width:65%">
                    <!--<div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Loại văn bản:</label>

                                <div class="col-sm-7 sl2">
                                    <div class="row">
                                        <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="javascript:handleSelect(this)">
                                            <option value="dsphongchutri" >-- Tất cả --</option>
                                            <option value="vanbanchoxuly_tp" {!empty($urltp)&&($urltp)?($urlcv=="vanbanchoxuly_tp")?'selected':'':''}>Văn bản mới nhận</option>
                                            <option value="dsphoihop">Phối hợp giải quyết</option>
                                            <option value="dsphongchutri?cacloaivanban=1">Đã chỉ đạo chưa giải quyết</option>
                                            <option value="dsvanbanphongdaxuly">Đã giải quyết</option>
                                        </select>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    function handleSelect(elm)
                                    {
                                        window.location = elm.value;
                                    }
                                </script>
                            </div>
                        </form>
                    </div>-->
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn chuyên viên phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!--- từ chối -->
        <div class="modal fade" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nội dung từ chối...." required></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập ghi chú</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="ghichu" class="form-control" placeholder="Nhập ghi chú ....">
                                    </div>
                                </div>
                                <input type="text" name="maphongban" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Chuyển chánh van phong">Chuyển chánh văn phòng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade phophoihop" id="myModal3" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phó phòng phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_B" method="POST">
                                {foreach $getAccountDeputyDirector as $pp}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$pp.PK_iMaCB}" data-id="{$pp.sHoTen}" class="Checkbox"> {$pp.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphophongban" value="" class="hidden" >
                                <input type="text" name="phochutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
		<!-- Modal về trưởng phòng-->
        <div class="modal fade phongphoihop" id="myModal4" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung từ chối...."></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập ghi chú</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="ghichu" class="form-control" placeholder="Nhập ghi chú ....">
                                    </div>
                                </div>
                                <input type="text" name="maphongbantp" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoitp" class="btn btn-primary btn-sm" value="Chuyển trưởng phòng">Chuyển Chi Cục Trưởng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!----- thêm hạn văn bản ------->
        <div class="modal fade" id="myModal5" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Đề xuất hạn giải quyết</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập hạn đề xuất</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="handexuat" class="form-control datepic datemask" placeholder="Nhập hạn đề xuất ....">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <label for="" class="col-sm-12">Lý do thêm hạn giải quyết</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập lý do...."></textarea>
                                    </div>
                                </div>
                                <input type="text" name="mavbd" value="" class="hidden">
                                <input type="text" name="ldgui" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="themhan" class="btn btn-primary btn-sm" value="Chuyển chánh van phong">Gửi Lãnh đạo</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                              
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                             
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post">
<!--                            <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>-->
<!--                            <br>-->
						<br>
						<div class="col-sm-12 deletepadding" style="">
 							<div class="col-sm-3">
								<p style="font-size: 15px"><b>Ghi chú màu Trích yếu:</b></p>
							</div>
							<div class="col-sm-3">
								<span><b style="color: red"><i class="fa fa-circle" aria-hidden="true"></i></b>: Quá hạn</span>
							</div>
							<div class="col-sm-3">
							<span><b style="color: orange"><i class="fa fa-circle" aria-hidden="true"></i></b>: Sắp đến hạn</span>
							</div>
							<div class="col-sm-3">
							<span><b style="color: black"><i class="fa fa-circle" aria-hidden="true"></i></b>: Trong hạn</span>
							</div>

						</div>
						<div class="" style="margin-bottom:-20px; margin-top:-20px; text-align:center">{$phantrang}</div>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th class="text-center" >Trích yếu - Thông tin</th>
                                    <th width="15%" >Tác vụ</th>
                                    <th width="25%" class="text-center">Chỉ đạo</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i=1}
                                {foreach $getDocAwait as $pl}
                                <tr>
                                    <td class="text-center">{$i++ + $pagei}</td>
                                    <td>
                                        <p><b><a style="color: black;" href="{$url}chuyenvienchutrixuly?vbd={$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
                                        <input type="text" name="noidungvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sMoTa}">
										<input type="text" name="sohieu[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sKyHieu}">
                                        <input type="text" name="ngaynhapvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sNgayNhap}">
                                        - Số đến: <b style="color: red; font-size: 20px">{$pl.iSoDen}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Số ký hiệu: {$pl.sKyHieu} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Ngày nhập: {date_select($pl.sNgayNhap)}<br>
                                        
                                        <p>- Nơi gửi: <span style="color: black;">{$pl.sTenDV}</span><br>
										{if !empty($pl.sTenLV)}- Hashtag: {$pl.sTenLV}<br>{/if}
										
										{$domat = layDoMat($pl.FK_iMaDM)} 											
										{if $pl.FK_iMaDM > 1} - Độ mật: <b style="color:red; font-size:16px">{$domat[0].sTenDoMat}</b><br>{else}{/if}
										
										{$dokhan = layDoKhan($pl.FK_iMaDK)} 
										{if $pl.FK_iMaDK > 1} - Độ khẩn: <b style="color:red; font-size:16px"> {$dokhan[0].sTenDoKhan} </b>{else}{/if}
											
										</p>
                                        {if !empty($pl.sNoiDung)}<br><p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
										{if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
										<span><span style="color: red">*</span><b>{if !empty($pl.ChiDao)}Chỉ đạo của Lãnh đạo Sở{else}Tham mưu của đ/c Chánh Văn Phòng{/if}{if $pl.QuyenHan == 4}{/if}{if $pl.QuyenHan == 5}{/if}:</b></span>
                                        <p><span><i>{$pl.smotald}</i></span></p>
										
										<p>
										{if !empty($pl.iDeadline) && $pl.iDeadline == 1}
											<label for="" class="control-label" style=""><b style=" color:blue">LĐ giao có thời hạn</b>: <b style="color:red">{date_select($pl.sHanThongKe)}</b></label>
										
										{else}
											<label for="" class="control-label" style="">Hạn xử lý: <b style="">{date_select($pl.sHanThongKe)}</b></label>
										{/if}
										</p>
										
										{if !empty($pl.sLyDo)}<span style="color: red"><b>Lý do từ chối:</b></span> <span><i>{$pl.sLyDo}</i></span>
                                        <br> <p>(<span>Cán bộ từ chối: </span> <b>{$pl.sNguoiNhap}</b>)</p> {/if}
										{/if}
                                        {if ($pl.sLyDoTuChoiVanBanDen != ' ') && (!empty($pl.sLyDoTuChoiVanBanDen))}<p><b class="text-danger">Lý do từ chối của {$pl.sCVTuChoi}: </b><br>
										<i>{$pl.sLyDoTuChoiVanBanDen}</i></p>{/if}
                                    	{if $pl.iSTCChuTri == 1 || $pl.iVanBanTBKL == 1 || $pl.iToCongTac == 1 || $pl.iSTCPhoiHop == 1 || $pl.iVanBanQPPL == 1}
                                        <h5>- Loại VB: <b style="color: blue"> <i>{($pl.iSTCChuTri == 1) ? 'SNV chủ trì.' : NULL }</i> <i>{($pl.iVanBanTBKL == 1) ? 'TBKL.' : NULL }</i> <i>{($pl.iToCongTac == 1) ? 'TTHC.' : NULL }</i> <i>{($pl.iSTCPhoiHop == 1) ? 'VB Đảng' : NULL }</i> <i>{($pl.iVanBanQPPL == 1) ? 'QPPL.' : NULL }</i></b></h5>
                                        {/if}
                                        {if !empty($pl.sLyDo1)}<p><b class="text-danger">Nội dung gửi lại của <b>{$pl.PhongChuyenLai['sTenPB']}</b>*:</b> <i>{$pl.sLyDo1}</i></p>{/if}
										
										<p class="" style="text-align:right">
										{$dsfile=layDuLieu('FK_iMaVBDen',$pl.PK_iMaVBDen,'tbl_files_vbden')}
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p>
											{foreach $dsfile as $f}
											<p style="margin-bottom:3px;text-align:right">
												<a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></p>
											{/foreach}
                                        <br>
										{if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
											{if $pl.iTrangThai_ThemHan != 1}
												<button type="button" name="dexuathan"  value="{$pl.PK_iMaVBDen}" data-id="{if !empty($pl.ChiDao)}{$pl.ChiDao}{/if}" data-num="{if !empty($pl.PK_iMaLDCD)}{$pl.PK_iMaLDCD}{/if}" class="btn btn-success btn-xs" style="padding: 2px 5px;" data-toggle="modal" data-target="#myModal5" >Đề xuất thêm hạn</button>
											{/if}
                                        {/if}
										
										<a data-toggle="collapse" href="#collapseExample{$pl.PK_iMaVBDen}" aria-expanded="false" aria-controls="collapseExample{$pl.PK_iMaVBDen}"><b style="color:black"> <button class="btn btn-success btn-xs" style="padding: 2px 5px;">Hashtag</button></b></a>
										<div class="col-sm-12 collapse" id="collapseExample{$pl.PK_iMaVBDen}" style="margin-bottom: 20px; "><br>
	
											<div class="col-sm-10">
														<textarea name="hashtag{$pl.PK_iMaVBDen}" id="" class="form-control" rows="1" >{if !empty($pl.sTenLV)} {$pl.sTenLV}{/if}</textarea> 
											</div>
											<div class="col-sm-2">
												<button type="submit" name="luuhashtag" value="{$pl.PK_iMaVBDen}" class="btn btn-primary btn-sm">Lưu</button>
											</div>

										</div> 
										<a href="chuyenvienchutrixuly?vbd={$pl.PK_iMaVBDen}&kx=word"><button class="btn btn-primary btn-xs pull-right" style="margin-right:5px" type="button" value="">Tải phiếu trình</button></a> 
										{if date('Y') == $year}										
											<a href="vanban?isoden={$pl.iSoDen}&mvb={$pl.PK_iMaVBDen}"><button class="btn btn-primary btn-xs pull-right" style="margin-right:5px" type="button" value="">Tạo VB đi</button></a> 	
										{/if}
										
										<!--<p class="pull-right"><a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}</p>-->

                                    </td>
                                    <td class="ykien">
                                        <div style="margin-bottom: 10px;">
                                            <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc1" style="cursor:pointer">
                                                <option value="">Chuyển Phó phòng</option>
                                                {foreach $getAccountDeputyDirector as $pgd}
                                                <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$pgd.sHoTen}">{$pgd.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphophoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal3" style=""><b>Chọn PP phối hợp</b></button>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <select name="phongchutri[{$pl.PK_iMaVBDen}]" id="" class="form-control phongchutri1">
                                                <option value="">Chọn chuyên viên</option>
                                                {foreach $getDepartment as $cv}
                                                <option value="{$cv.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$cv.sHoTen}">{$cv.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" ><b>Chọn CV phối hợp</b></button>
                                        </div>
                                        <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="" id="{$pl.PK_iMaVBDen}" class="hidden mangphoihop1">
                                        <input type="text" name="mangphoihoppp[{$pl.PK_iMaVBDen}]" value="" id="{$pl.PK_iMaVBDen}--1" class="hidden mangphophongpp">
                                        <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden doc_id1">
										<input type="text" name="sKy_Hieu_{$pl.PK_iMaVBDen}" value="{$pl.sKy_Hieu}" class="hide">
                                        <input type="text" name="sNgay_Ky_{$pl.PK_iMaVBDen}" value="{$pl.sNgay_Ky}" class="hide">
                                        <input type="text" name="sMaCoQuan_{$pl.PK_iMaVBDen}" value="{$pl.sMaCoQuan}" class="hide">
                                        <input type="text" name="sTenCoQuan_{$pl.PK_iMaVBDen}" value="{$pl.sTenCoQuan}" class="hide">
										
										<p><input type="checkbox" name="vbdaura_tp{$pl.PK_iMaVBDen}" value="2" {if $pl.vbdaura_tp ==2}checked{/if} class="duyet"><span style=""> Văn bản để lưu</span></p>

										<!--<p><input type="checkbox" name="ppthammmuu[{$pl.PK_iMaVBDen}]" value="1"><span style="color: rgb(85, 85, 85);"> PP biết để đôn đốc</span>-->

										<p><span style=" padding-top:5px"><input type="checkbox" name="vanbanquantrong[{$pl.PK_iMaVBDen}]" value="1"></span> <span style="color: red;"> VB Quan trọng</span></p>
										
										{if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 6}
                                        <button type="button" name="abc"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#myModal1" >Từ chối</button>
										{/if}
										
                                    </td>
                                    <td class="chidao"  class="text-center">
                                        {if $vanban['iQuyenHan_DHNB'] == 11 && $pl.vbdaura_tp ==1}<span style="color: red"><b>Chi cục trưởng xác định văn bản yêu cầu có văn bản trả lời</b></span><br><br>{/if}
                                        <textarea name="chidaophogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}-1" class="form-control phogiamdoc" rows="2"></textarea><br>
                                        <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="5"></textarea><br>
										<button type="submit" class="btn btn-primary btn-xs pull-right" name="duyet" value="{$pl.PK_iMaVBDen}" style="margin-bottom: 10px" ><i class="fa fa-check"></i><b>Duyệt</b></button><br>
                                    </td>
                                   
                                </tr>
                                {/foreach}
                                </tbody>
                            </table>
							<div class="" style="margin-bottom:-20px; margin-top:-20px; text-align:center">{$phantrang}</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly_tp.js"></script>
<script>
    $(document).ready(function() {
        $("button[name=abc]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=maphongban]').val($(this).val());
            });
        });
		$("button[name=tctp]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=maphongbantp]').val($(this).val());
            });
        });
        $("button[name=dexuathan]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=mavbd]').val($(this).val());
                $('button[name=themhan]').text('Gửi Lãnh đạo ( '+$(this).attr("data-id")+' )');
                $('input[name=ldgui]').val($(this).attr("data-num"));
            });
        });
    });
</script>
