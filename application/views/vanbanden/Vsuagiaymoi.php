<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 class="font" style="text-align:center; font-size:30px;">
        </h3>
    </section>
		<h3 style="text-align:center; font-size:30px; margin-top:-30px">
            GIẤY MỜI
        </h3>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" action="">
                    <div class="row">
                        <div class="col-sm-12">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <select name="khuvuc" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn cấp ban hành --</option>
                                            {foreach layDuLieu(NULL,NULL,'tbl_khuvuc') as $kv}
                                            <option value="{$kv.sTenKV}" {!empty($getdAppointment) && ($getdAppointment[0]['sTenKV'] == $kv.sTenKV) ? selected : NULL }>{$kv.sTenKV}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="sokyhieu" class="form-control" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }">
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="4" type="text" class="loaivanban form-control ngayhan" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLVB'] : NULL }" name="loaivanban" autocomplete="off">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="3" name="noiguiden" tabindex="" type="text" class="donvi form-control" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }" autocomplete="off">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Ngày ký <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" class="form-control datepic datemask" id="" name="ngayky" placeholder="Ngày ký" value="{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : date('d/m/Y',time()) }" required>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="5" type="text" class="linhvuc form-control" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLV'] : NULL }" name="linhvuc" autocomplete="off">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
								
                            </div>
							
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký: </label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="7" type="text" name="nguoiky" class="nguoiky form-control" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenNguoiKy'] : NULL }" autocomplete="off">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="chucvu" class="form-control" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sChucVu'] : NULL }">
                                    </div>
                                </div>                              
                            </div>
							
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <textarea name="trichyeu" class="form-control" rows="2" style="border: 1px solid red;">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</textarea>
                                    </div>
                                </div>
                            </div>
							
							<div class="col-md-6">
                               
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Ngày nhận <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="ngaynhan" class="form-control datepic datemask" value="{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayNhan']) : NULL }">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                              
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <!--<input type="text" name="hangiaiquyet" class="form-control datepic datemask" value="{!empty($getdAppointment) &&($getdAppointment[0]['sHanGiaiQuyet'] == '0000-00-00') ? NULL : date_select($getdAppointment[0]['sHanGiaiQuyet']) }">-->
										<input type="text" name="hangiaiquyet" class="form-control" value="Tự động" readonly>
                                    </div>
                                </div>
                            </div>
							
							<div class="col-md-6">
								 <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"><b>Họp vào hồi:</b></label>

                                    <div class="col-sm-8">
                                        <input type="text" name="giohop" class="text-center" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiGio'] : NULL }">  
                                    </div>
                                </div>
								
								
							</div>
							
							<div class="col-md-6">
								
								 <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"><b>Ngày họp:</b></label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngayhop" class="datepic datemask" value="{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sGiayMoiNgay']) : NULL }" required>
                                    </div>
                                </div>
							</div>
							
							
							 <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Địa điểm:</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="diadiem" style="width: 100%;" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiDiaDiem'] : NULL }">
                                    </div>
                                </div>
                            </div>
							
							 <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nội dung họp:</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="noidung" class="form-control" id="" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sNoiDung'] : NULL }">
                                    </div>
                                </div>
                            </div>
							
								
								
                            <!--<div class="col-sm-12">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr class="info">
                                        <td><p class="col-sm-12 bootstrap-timepicker"><b>Họp vào hồi:</b> 
										<input type="text" name="giohop" class="text-center" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiGio'] : NULL }">  
										
										<span class="text-info">Ngày:</span>  <input type="text" name="ngayhop" class="datepic datemask" value="{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sGiayMoiNgay']) : NULL }"></p></td>
                                    </tr>
                                    <tr class="info">
                                        <td><p class="col-sm-7"><b>Địa điểm:</b>	 <input type="text" name="diadiem" style="width: 85%;" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiDiaDiem'] : NULL }">	<b></p><p class="col-sm-5">Chủ trì:</b>	 <input type="text" name="nguoichutri" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiChuTri'] : NULL }"  style="width: 77%;"></p></td>
                                    </tr>
                                    <tr class="info">
                                        <td>
                                            <label for="" class="col-sm-2 control-label">Nội dung họp: </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="noidung" class="form-control" id="" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sNoiDung'] : NULL }">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>-->
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label>	<input type="text" name="soden" value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }" class="form-control" style="color: red;font-size: 16px"></label>
                                    </div>
                                    <!--<label for="" class="col-sm-1 control-label">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <select name="domat" id="" class="form-control">
                                            <option value="1" {!empty($getdAppointment) && ($getdAppointment[0]['FK_iMaDM'] == 1) ? selected : NULL }>- Bình thường</option>
                                            <option value="2" {!empty($getdAppointment) && ($getdAppointment[0]['FK_iMaDM'] == 2) ? selected : NULL }>- Mật</option>
                                            <option value="3" {!empty($getdAppointment) && ($getdAppointment[0]['FK_iMaDM'] == 3) ? selected : NULL }>- Tối mật</option>
                                        </select>
                                    </div>

                                    <label for="" class="col-sm-1 control-label">Độ khẩn:</label>
                                    <div class="col-sm-2">
                                        <select name="dokhan" id="" class="form-control">
                                            <option value="1" {!empty($getdAppointment) && ($getdAppointment[0]['FK_iMaDK'] == 1) ? selected : NULL }>- Bình thường</option>
                                            <option value="2" {!empty($getdAppointment) && ($getdAppointment[0]['FK_iMaDK'] == 2) ? selected : NULL }>- Khẩn</option>
                                            <option value="3" {!empty($getdAppointment) && ($getdAppointment[0]['FK_iMaDK'] == 3) ? selected : NULL }>- Tối khẩn</option>
                                        </select>
                                    </div>-->
                                </div>
                            </div>
                            
                           
                           
                            {if !empty($idDocGo)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Ngày nhận</th>
                                            <th class="text-center">Chuyển từ</th>
                                            <th width="40%">Nội dung</th>
                                            <th class="text-center">Chuyển đến</th>
                                            <th class="text-center">Hạn xử lý</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $process as $pr}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_time($pr.sThoiGian)}</td>
                                            <td class="text-center">
                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                                                {$cb.sHoTen}
                                                {/if}
                                                {/foreach}
                                            </td>
                                            <td>{$pr.sMoTa}</td>
                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'  }{date_select($getdAppointment[0]['sHanGiaiQuyet'])}<br><i>{if {$pr.sThoiGianHetHan} != '0000-00-00'}(Hạn VB: {date_select($pr.sThoiGianHetHan)}){/if}{/if}</i></td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Tệp tin đính kèm:</label>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">

                                        <table class="table table-bordered" style = "width: 700px">

                                            <thead>

                                            <tr>

                                                <th class="text-center" style = "width: 50px">STT</th>

                                                <th>Tên tệp tin</th>

                                                <th class="text-center" style = "width: 150px">Tải về</th>

                                                <th class="text-center">Ngày nhập</th>

                                            </tr>

                                            </thead>

                                            <tbody>

                                            {$i=1}

                                            {foreach $Filedinhkem as $fi}

                                            <tr>

                                                <td class="text-center">{$i++}</td>

                                                <td>{$fi.sTenFile}</td>

                                                <td class="text-center"> <a class="tin1" href="{$fi.sDuongDan}" target="_blank">Xem</a> | <a class="tin1" href="{$fi.sDuongDan}" download>Download</a></td>

                                                <td class="text-center" width="15%">{date_select($fi.sThoiGian)}</td>

                                            </tr>

                                            {/foreach}

                                            </tbody>

                                        </table>

                                    </div>

                                </div>
                            </div>
                            {if !empty($resultPTL)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-2 control-label">Kết quả giải quyết:</label>

                                </div>

                                <div class="form-group">
                                    <form action="" method="post">
                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>
                                            <th>STT</th>
                                            <th>Cán bộ-đơn vị</th>
                                            <th>Nội dung</th>
                                            <th class="text-center">Tải về</th>
                                            <th class="text-center">Ngày nhập</th>
                                            <th class="text-center">Tác vụ</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $resultPTL as $tl}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td width="20%">{$tl.sHoTen} - {$tl.sTenPB}</td>
                                            <td>{$tl.sMoTa}</td>
                                            {if !empty($tl.sDuongDanFile)}
                                            <td width="8%" class="text-center"><a href="{$tl.sDuongDanFile}">[Tải tài liệu]</a></td>
                                            {else}
                                            <td class="text-center"> - </td>
                                            {/if}
                                            <td width="15%" class="text-center">{date_select($tl.date_file)}</td>
                                            {if $tl.active == 2}
                                                <td width="8%" class="text-center"><button type="submit" value="{$tl.PK_iMaKetQua}" name="xoafile" onclick="return confirm('Bạn muốn xóa {$tl.sMoTa} ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button></td>
                                            {else}
                                            <td class="text-center"> Đã duyệt</td>
                                            {/if}
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                    </form>
                                </div>

                            </div>

                            {/if}
                            {if !empty($VBDi)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Văn bản đi</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Số văn bản đi</th>
                                            <th class="text-center" >Ngày văn bản</th>
                                            <th class="text-center">Trích Yếu</th>
                                            <th class="text-center">Xem file</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $VBDi as $di}
                                        <tr class="success">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{$di.iSoVBDi}</td>
                                            <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                            <td class="text-center">{$di.sMoTa}</td>
                                            <td class="text-center" ><a href="{$di.sDuongDan}" target="_blank">[Xem file]</a></td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}

                            {/if}
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right" type="submit" name="luulai" value="Lưu lại">Cập nhật</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
    var data = {
        "timkiem": [
            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
            "{$lvb.sTenLVB}",
            {/foreach}
    ],
    "noiden": [
        {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
        "{$dv.sTenDV}",
        {/foreach}
    ],
    "linhvuc": [
        {foreach layDuLieu(NULL,NULL,'tbl_linhvuc') as $lv}
        "{$lv.sTenLV}",
        {/foreach}
    ],
    "nguoiky": [
        {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
        "{$nk.sHoTen}",
        {/foreach}
    ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".loaivanban",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".linhvuc",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            linhvuc: {
                data: data.linhvuc
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".donvi",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>