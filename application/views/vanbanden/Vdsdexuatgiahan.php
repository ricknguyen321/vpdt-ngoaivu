<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header" style="padding: 0px">
      <div class="row">
         <div class="col-sm-12 deletepadding1">
            <div class="col-sm-3">
               <h3 class="font">
                  Danh sách đề xuất gia hạn <b>({$count})</b>
               </h3>
            </div>
         </div>
      </div>
   </section>
   <div class="modal fade" id="myModal3" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel"><b>Lý do từ chối</b></h4>
            </div>
            <form action="" method="post">
               <div class="modal-body">
                  <div class="row" >
                     <div class="col-sm-12">
                        <label for="" class="col-sm-12">Lý do từ chối</label>
                        <div class="col-sm-12">
                           <textarea name="lydotuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập lý do...."></textarea>
                        </div>
                     </div>
                     <input type="text" name="mavbd" value="" class="hidden">
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Từ chối">Từ chối</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- Main content -->
   <section class="content">
      <!-- Default box -->
      <div class="box">
         <div class="box-body">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
               <div class="col-sm-8 deletepadding">
                  {$phantrang}
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <table id="" class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th width="50px" class="text-center">STT</th>
                              <th width="35%" class="text-center">Trích yếu</th>
                              <th width="" class="text-center">Duyệt hạn giải quyết</th>
                           </tr>
                        </thead>
                        <tbody>
                           {$i = 1}
                           {foreach $getDocGo as $vbd}
                           <tr>
                              <td class="text-center">{$i++}</td>
                              <td>
                                 <p><b><a style="color:black;" href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $vbd.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $vbd.sHanThongKe)}<span style="color: orange">{$vbd.sMoTa}</span>{elseif $vbd.sHanThongKe < date('Y-m-d',time()) && $vbd.sHanThongKe > '2017-01-01'}<span style="color: red">{$vbd.sMoTa}</span>{else}<span>{$vbd.sMoTa}</span>{/if}</a></b></p>
                                 <p>- Số ký hiệu: {$vbd.sKyHieu}<br>
                                    - Nơi gửi: {$vbd.sTenDV}<br>
                                    - Ngày nhận văn bản: {date_select($vbd.sNgayNhap)}<br>
                                    - Số đến: <b style="color: red; font-size: 18px">{$vbd.iSoDen}</b><br>
                                    - Hạn xử lý: <b style="color:red">{date_select($vbd.sHanThongKe)}</b>
                                 </p>
                                 <span><i>{if !empty($vbd.sNoiDung)}Nội dung*: {$vbd.sNoiDung}{/if}</i></span>
                                 <p><a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}">Tài liệu <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $vbd.sDuongDan} <a target="_blank" href="{$vbd.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($vbd.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}</p>
                              </td>
                              <td>
                                 <div style="    display: inline-table;">Hạn đề xuất: </div>
                                 <div style="    display: inline-table; margin-bottom:10px"><input type="text" name="handexuat[{$vbd.PK_iMaVBDen}]" value="{(!empty($vbd.sHanChoDuyet)&&($vbd.sHanChoDuyet > '2017-01-01') ? date_select($vbd.sHanChoDuyet) : NULL)}" class="form-control datepic datemask" placeholder="Nhập gia hạn...." style="width:100px"></div>
                                 <br>
                                 <p><b>Lý do đề xuất và quá trình gia hạn: </b><br>
                                    {$qtgiahan = layDuLieu('FK_iMaVBDen',$vbd.PK_iMaVBDen,'tbl_luuvet_giahan')}
                                    {$qtgiahan = array_reverse($qtgiahan)}
                                 <div style="max-height:280px;  overflow:auto">
                                    {$i=1}
                                    {foreach $qtgiahan as $gh}
										{if $gh.FK_iMaCB_Gui > 0}
											{$cb = layTTCB($gh.FK_iMaCB_Gui)}
											{$isld = 0}
										{else} 
											{$cb = layTTCB($gh.FK_iMaCB_Duyet)}
											{$isld = 1}
										{/if}
										<p {if $isld == 1}style="font-weight:bold"{/if}>"{if !empty($gh.noidungtuchoi)}
												{$gh.noidungtuchoi}
											{else}
												{if !empty($gh.noidunggiahan)}
													{$gh.noidunggiahan}
												{elseif $gh.iTrangThai_ThemHan == 2}
													Duyệt
												{elseif $gh.iTrangThai_ThemHan == 3}
													Từ chối
												{/if}
											{/if}"
										</p>
										<p style="text-align:right">
										{if $gh.sHanThongKe != '0000-00-00'}
											Hạn đề xuất: <b>{date_select($gh.sHanThongKe)}</b>
										{elseif $gh.iTrangThai_ThemHan == 3}
										   <span style="color:red; text-align:right">Từ chối</span>
										{/if} 
										<br> 
										{$cb[0].sHoTen} -
										{if !empty($gh.ngaygiahan)}
											{date_time2($gh.ngaygiahan)}
										{else}
											{if !empty($gh.ngayduyet)}
												{date_time2($gh.ngayduyet)}
											{/if}
										{/if}
										</p>
										<hr>
                                    {/foreach}
                                 </div>
                                 <!--<i>{$vbd.sNoiDungGiaHan}</i>--></p>										
                                 <input type="text" name="gheplydo[{$vbd.PK_iMaVBDen}]" value="{$vbd.sNoiDungGiaHan}" class="hidden">
                                 {if $vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5 || $vanban['iQuyenHan_DHNB'] == 3}
                                 {if $vanban['iQuyenHan_DHNB'] == 4}
                                 <p><label for="">Ý kiến chỉ đạo / Lý do từ chối</label>
                                    <textarea name="chidaoduyet[{$vbd.PK_iMaVBDen}]" id="" class="form-control lydo" rows="3" placeholder="Nhập ý kiến chỉ đạo thêm / Lý do từ chối gia hạn"></textarea>
                                 </p>
                                 <p>
                                    <button style="margin-left:20%; width: 20%" type="submit" name="duyethan" class="btn btn-primary btn-xs pull-left" value="{$vbd.PK_iMaVBDen}"><b>Duyệt Hạn</b></button>
                                    <button style="margin-right:20%; width: 15%" type="submit" name="tuchoihangd" class="btn btn-warning btn-xs tuchoihan pull-right" value="{$vbd.PK_iMaVBDen}">Từ chối</button>
                                 </p>
                                 {else}
                                 <p><button style="margin-left:20%; width: 20%" type="submit" name="duyethan" class="btn btn-primary btn-xs pull-left" value="{$vbd.PK_iMaVBDen}"><b>Duyệt Hạn</b></button>
                                    <button style="margin-right:20%; width: 15%" type="button" name="tuchoihan[{$vbd.PK_iMaVBDen}]" class="btn btn-warning btn-xs tuchoihan pull-right" value="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#myModal3" >Từ chối</button>
                                 </p>
                                 {/if}
                                 {/if}
                                 {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
                                 <br><br>
                                 <p><label for="">Đề xuất lên Lãnh đạo</label>
                                    <textarea name="noidungdexuat[{$vbd.PK_iMaVBDen}]" id="" class="form-control lydo" rows="3" placeholder="Nhập lý do đề xuất"></textarea>
                                 </p>
                                 {$gd = layTTGD()}
                                 {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
                                 <button type="submit" name="duyethancb" class="btn btn-primary btn-xs lanhdao" value="{$vbd.PK_iMaVBDen}">Gửi Lãnh đạo</button>
                                 {else}
                                 <button type="submit" name="duyethancb" class="btn btn-primary btn-xs hidden lanhdao" value="{$vbd.PK_iMaVBDen}">Gửi Lãnh đạo</button>
                                 <button type="button" name="luuao" class="btn btn-primary btn-xs lanhdao1" disabled>Gửi Lãnh đạo</button>
                                 {/if}
                                 {if $vanban['iQuyenHan_DHNB'] != 3}
                                 <button type="button" name="tuchoihan[{$vbd.PK_iMaVBDen}]" class="btn btn-warning btn-xs tuchoihan" value="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#myModal3" >Từ chối</button>
                                 {/if}
                                 <input type="hidden" name="idlanhdao[{$vbd.PK_iMaVBDen}]" value="{if !empty($vbd.PK_iMaLDCD)}{$vbd.PK_iMaLDCD}{else}{$gd[0].PK_iMaCB}{/if}">
                                 {/if}
                                 {if $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 11 || $vanban['iQuyenHan_DHNB'] == 10}
                                 <label for="">Ý kiến gửi Trưởng phòng</label>
                                 <textarea name="noidungdexuattp[{$vbd.PK_iMaVBDen}]" id="" class="form-control lydo" rows="5" placeholder="Nhập ý kiến gửi lên Trưởng phòng"></textarea>
                                 <br>
                                 <button type="submit" name="duyethanpp" class="btn btn-primary btn-xs hidden" value="{$vbd.PK_iMaVBDen}">{if $vanban['iQuyenHan_DHNB'] == 7}Gửi Trưởng phòng{else}Gửi Chi cục trưởng{/if}</button>
                                 <button type="button" name="luuao" class="btn btn-primary btn-xs" disabled>{if $vanban['iQuyenHan_DHNB'] == 7}Gửi Trưởng phòng{else}Gửi Chi cục trưởng{/if}</button>
                                 <button type="button" name="tuchoihan[{$vbd.PK_iMaVBDen}]" class="btn btn-warning btn-xs tuchoihan" value="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#myModal3" >Từ chối</button>
                                 <input type="hidden" name="idlanhdao[{$vbd.PK_iMaVBDen}]" value="{$vbd.PK_iMaLDCDPP}">
                                 {/if}
                              </td>
                           </tr>
                           {/foreach}
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="col-sm-12">
                  {$phantrang}
               </div>
            </form>
         </div>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</div>
<script>
   function myFunction() {
       window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
   }
</script>
<script>
   $(document).ready(function() {
       $(document).on('keyup','.lydo',function () {
           $(this).parent().find('.lanhdao').removeClass('hidden');
           $(this).parent().find('.lanhdao1').addClass('hidden');
       });
       $(document).on('keyup','.lydo',function () {
           $(this).parent().find('button[name=duyethanpp]').removeClass('hidden');
           $(this).parent().find('button[name=luuao]').addClass('hidden');
       });
   $(document).on('click','.tuchoihan',function () {
           $('input[name=mavbd]').val($(this).val());
       });
   });
</script>