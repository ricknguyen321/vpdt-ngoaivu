<style type="text/css">
    .border_main{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
    }
    .border_main1{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
    }
</style>
    <section class="content">
      <!-- Default box -->
        <div class="box" style="font-size: 16px">
            <div class="box-body">
				{$cb = layTTCB($id)}
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 100%;vertical-align: top; font-size: 16px">
                                    <span style="text-transform: uppercase;">
                                        <b>Danh sách văn bản {if $dagiaiquyet==1}đã giải quyết đúng hạn{/if}{if $dagiaiquyet == 0 && $chuagiaiquyet == ''}đã giải quyết quá hạn{/if} {if $chuagiaiquyet==1}chưa giải quyết trong hạn{/if}{if $chuagiaiquyet==0 && $dagiaiquyet == ''}chưa giải quyết quá hạn{/if} <br>
										{if $pb==0}của đồng chí {$cb[0].sHoTen}{/if}
										</b>
										<hr style="width:30%; padding-top:-10px">
                                    </span>
                                </td>                               
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12">                        
						<table id="" class="table table-bordered">                            
                            <tbody>
							<tr>
                                <td class="border_main" style="text-align: center; width:50px"><b>STT</b></td>
                                <td class="border_main" style="text-align: center; width:70%">
                                   <b>Trích yếu - Thông tin</b>								
								</td>
								<td class="border_main1" style="text-align: center; width:20%">
                                   <b>Ghi chú</b>								
								</td>
                            </tr>
                            {$i = 1}
                            {foreach $getDocGo as $vbd}
                            <tr>
                                <td class="border_main" style="text-align: center">{$i++}</td>
                                <td class="border_main">
                                    {if $vbd.iTrangThai == 1}
												<p><b>
												{if $vbd.sNgayGiaiQuyet > $vbd.sHanThongKe}
													<b style="color: red">{$vbd.sMoTa}</b>
												{else}
													<b>{$vbd.sMoTa}</b>
												{/if}</b></p>
											{else}
												<p>{if (date('Y-m-d',time()) <= $vbd.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $vbd.sHanThongKe)}<b style="color: orange;font-weight: bold;">{$vbd.sMoTa}</b>{elseif $vbd.sHanThongKe < date('Y-m-d',time()) && $vbd.sHanThongKe > '2017-01-01'}<b style="color: red">{$vbd.sMoTa}</b>{else}<b>{$vbd.sMoTa}</b>{/if}</p>
											{/if}
									- Số đến: <B>{$vbd.iSoDen}</b> - Ngày nhập: {date_select($vbd.sNgayNhap)}<Br>
									- Số ký hiệu: {$vbd.sKyHieu} - Nơi gửi: {$vbd.sTenDV}<br>
									- Chuyên viên xử lý: <b>{$vbd.sHoTen}</b><br>
                                    - Hạn xử lý: <b>{if $vbd.sHanThongKe > '2017-01-01'}{date_select($vbd.sHanThongKe)} {else}{date_select($vbd.sHanGiaiQuyet)}{/if} </b>									
								</td>	
								<td class="border_main1" style=""></td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
				
				
               
        </div>

    </section>