<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-4">
                    <h3 class="font">
                        Danh sách lịch họp phòng chủ trì nội dung ({$count})
                    </h3>
                </div>
               
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
						
                            <span class="pull-right">{$phantrang}</span>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    
                                    <th width="" class="text-center">Thông tin cuộc họp</th>
                                    <th width="17%" class="text-center">Lãnh đạo chủ trì/dự họp</th>
                                    <th width="40%" class="text-center">Chỉ đạo</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i=1}
                                {if !empty($lichhop)}

                                {foreach $lichhop as $lh}

                                <tr>

                                    <td class="text-center">{$i++}</td>

                                    <td>

                                        <p><b style="color:black"><a href="lichhopso" style="color:black">{$lh.sMoTa}</a></b></p>
										<p>- Thời gian: <b style="color:blue">{$lh.sGioMoi}, {date_select($lh.sNgayMoi)}</b><br>
										- Địa điểm: <b style="color:blue">{$lh.sDiaDiemMoi}</b><br>
										- Phòng chủ trì: {$lh.sPhongCT} {if $lh.sPhongPH != ''}<br>
										- Phòng phối hợp: {$lh.sPhongPH}{/if}
										</p>
										<p style="text-align:right"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></p>
											{$dsfile = layTLHop($lh.PK_iMaVBDi)}
											{foreach $dsfile as $f}
												{$cbtao = layTTCB($f.FK_iMaCB)}
												<p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{$f.sTenFile}</span></a> <i style="color: silver">({$cbtao[0].sHoTen} - {date_time2($f.sThoiGian)})</i></p>
											{/foreach}
										
                                    </td>

                                    <td class="text-center" width="15%">
										<b>{$cbduhop = layTTCB($lh.FK_iMaCB_Ky)}{$cbduhop[0].sHoTen}</b>
                                    </td>
                                    <td>
										{if !empty($lh.sChiDao)}
											<p><b style="color:red">{$lh.sChiDao}</b></p>
											<b class="pull-right">{$cbduhop = layTTCB($lh.FK_iMaCB_Ky)}{$cbduhop[0].sHoTen} - {date_time2($lh.sNgayNhap)}</b> 
										{/if}
									</td>
									
                                </tr>

                                {/foreach}

                                {/if}

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
