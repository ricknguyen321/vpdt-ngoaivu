<style type="text/css">
    .{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
    }
    .{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
    }
</style>
    <section class="content">
      <!-- Default box -->
        <div class="box" style="font-size: 16px">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="width: 60%;vertical-align: top; font-size: 16px;margin-bottom:20px">
                                    VĂN PHÒNG SỞ NGOẠI VỤ HÀ NỘI
                                </td>
                                <td colspan="13" style="text-align: center;width:40%">
                                   
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 100%;vertical-align: top; margin:5px">
                                    <span style="text-transform: uppercase;">
                                        <b>PHIẾU XỬ LÝ VĂN BẢN</b>
                                    </span>									
                                </td>
                            </tr>
							<tr style="margin-bottom:15px">
                                <td style="text-align: center; width: 100%;vertical-align: top; ">
                                    <span style="">
                                        <b>Kính gửi: Đ/c Giám đốc Sở</b>
                                    </span>									
                                </td>								
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr  >
                                <td style="width: 15%;vertical-align: top;" >
                                    <span style="">
                                        <b>Số đến:</b>
                                    </span>									
                                </td>
								<td style=";vertical-align: top;"  >
                                    <span style="">
                                        <b>{$getdAppointment[0]['iSoDen']}</b>
                                    </span>									
                                </td>
								<td style="width: 14%;vertical-align: top;" >
                                    <span style="">
                                        <b>Ngày đến:</b>
                                    </span>									
                                </td>
								<td style="width: 15%;vertical-align: top;"  >
                                    <span style="">
									   {date_select($getdAppointment[0]['sNgayNhan'])}
                                    </span>									
                                </td>
								<td style="width: 12%;vertical-align: top;" >
                                    <span style="">
                                        <b>Độ mật:</b>
                                    </span>									
                                </td>
								<td style="width: 12%;vertical-align: top;"  >
                                    <span style="">
										{$domat = layDoMat($getdAppointment[0]['FK_iMaDM'])}											
										{$domat[0].sTenDoMat}									   
                                    </span>									
                                </td>
                            </tr>
							<tr  >
                                <td style="width: 15%;vertical-align: top;" >
                                    <span style="">
                                        <b>Số ký hiệu:</b>
                                    </span>									
                                </td>
								<td style="vertical-align: top;"  >
                                    <span style="">
                                        {$getdAppointment[0]['sKyHieu']}
                                    </span>									
                                </td>
								<td style="width: 14%;vertical-align: top;" >
                                    <span style="">
                                        <b>Hạn xử lý:</b>
                                    </span>									
                                </td>
								<td style="width: 15%;vertical-align: top;"  >
                                    <span style="">
									   {date_select($getdAppointment[0]['sHanThongKe'])}
                                    </span>									
                                </td>
								<td style="width: 12%;vertical-align: top;" >
                                    <span style="">
                                        <b>Độ khẩn:</b>
                                    </span>									
                                </td>
								<td style="width: 12%;vertical-align: top;"  >
                                    <span style="">
										{$dokhan = layDoKhan($getdAppointment[0]['FK_iMaDK'])} 
										{$dokhan[0].sTenDoKhan}
                                    </span>									
                                </td>
                            </tr>
							
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="width: 100%;vertical-align: top;">
                                    <span style="">
                                        <p style="margin-bottom:5px"><b>Đơn vị gửi văn bản: {$getdAppointment[0]['sTenDV']}</b></p>
                                        <p style="margin-top:0px; margin-bottom:10px"><b>Trích yếu:&nbsp;&nbsp;&nbsp; {$getdAppointment[0]['sMoTa']}</b></p>
                                    </span>									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr>
                                <td style="width: 50%;vertical-align: top;" class=" text-center">
                                    <span style="">
										<b>1. Ý kiến đề xuất của Lãnh đạo Văn phòng</b>
										<BR><BR><BR><BR><BR><BR><BR><BR>
                                    </span>
									<p class="" style="text-align: left;">
										Ngày {date('d')} tháng {date('m')} năm {date('Y')}
									</p>
                                </td>
								<td style="width: 50%;vertical-align: top;text-align: center; " class=" text-center">
                                    <span style="">
										<B>Ý KIẾN CỦA LÃNH ĐẠO SỞ</B>
                                    </span>									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
               
        </div>
      <!-- /.box -->

    </section>
 


