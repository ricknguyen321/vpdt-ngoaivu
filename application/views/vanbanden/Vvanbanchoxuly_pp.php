<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Văn bản chờ xử lý <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6" style="margin-top: 10px">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Các loại văn bản:</label>

                                <div class="col-sm-7 sl2">
                                    <div class="row">
                                        <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="javascript:handleSelect(this)">
											<option value="dsvanbanden?id={$vanban['PK_iMaCB']}" >-- Tất cả --</option>
											<option value="vanbanchoxuly_pp" selected >Văn bản chờ xử lý</option>
											<option value="phophongphoihop" >Văn bản chờ phối hợp</option>
											<option value="vanbanchopheduyet?cacloaivanban=2">Văn bản đã xử lý</option>
                                        </select>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    function handleSelect(elm)
                                    {
                                        window.location = elm.value;
                                    }
                                </script>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a> </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal về trưởng phòng-->
        <div class="modal fade phongphoihop" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung từ chối...."></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập ghi chú</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="ghichu" class="form-control" placeholder="Nhập ghi chú ....">
                                    </div>
                                </div>
                                <input type="text" name="maphongbantp" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoitp" class="btn btn-primary btn-sm" value="Chuyển trưởng phòng">Chuyển trưởng phòng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!----- thêm hạn văn bản ------->
        <div class="modal fade" id="myModal5" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Đề xuất hạn giải quyết</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập hạn đề xuất</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="handexuat" class="form-control datepic datemask" placeholder="Nhập hạn đề xuất ....">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <label for="" class="col-sm-12">Lý do thêm hạn giải quyết</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập lý do...."></textarea>
                                    </div>
                                </div>
								<div class="col-sm-12">
									<div class="col-sm-6">
										{if $vanban['iQuyenHan_DHNB'] > 6}
										<br>
											<div class="form-group">
												<span style=""><b>Chọn Lãnh đạo phòng duyệt</b></span>
												<div style="margin-bottom:15px;margin-top:5px">
													{if !empty($dsldduyet)}
														<select name="ldduyet" style="width: 100%" id="" class="form-control select2">
															{foreach $dsldduyet as $ld}
																<option value="{$ld.PK_iMaCB}">{$ld.sHoTen}</option>
															{/foreach}
														</select>
													{/if}
												</div>
											</div>
										{/if}
									</div>
								</div>
                                <input type="text" name="mavbd" value="" class="hidden">
                                <input type="text" name="ldgui" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="themhan" class="btn btn-primary btn-sm" value="Chuyển chánh van phong">Gửi Lãnh đạo</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                              
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <br>
							<div class="col-sm-12 deletepadding" style="">
								<div class="col-sm-3">
									<p style="font-size: 15px"><b>Ghi chú màu Trích yếu:</b></p>
								</div>
								<div class="col-sm-3">
									<span><b style="color: red"><i class="fa fa-circle" aria-hidden="true"></i></b>: Quá hạn</span>
								</div>
								<div class="col-sm-3">
								<span><b style="color: orange"><i class="fa fa-circle" aria-hidden="true"></i></b>: Sắp đến hạn</span>
								</div>
								<div class="col-sm-3">
								<span><b style="color: black"><i class="fa fa-circle" aria-hidden="true"></i></b>: Trong hạn</span>
								</div>
							</div>
								<!-- <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>-->
							<div class="" style="text-align:center">{$phantrang}</div>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ngày đến</th>
                                    <th width="" class="text-center">Trích yếu - Thông tin</th>
                                    <th width="15%" ></th>
                                    <th width="25%" class="text-center">Chỉ đạo</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i=1}
                                {foreach $getDocAwait as $pl}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-center">{date_select($pl.sNgayNhap)}</td>
                                    <td>
                                        <p><b><a style="color: black;" href="{$url}chuyenvienchutrixuly?vbd={$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
                                        <input type="text" name="noidungvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sMoTa}">
										<input type="text" name="sohieu[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sKyHieu}">
										<input type="text" name="ngaynhapvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sNgayNhap}">
                                        - Số ký hiệu: {$pl.sKyHieu} <br>
                                        - Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> <br>
                                        <p>- Nơi gửi: <span style="color: black;">{$pl.sTenDV}</span><br>
										
										{$domat = layDoMat($pl.FK_iMaDM)} 											
										{if $pl.FK_iMaDM > 1} - Độ mật: <b style="color:red; font-size:16px">{$domat[0].sTenDoMat}</b><br>{else}{/if}
										
										{$dokhan = layDoKhan($pl.FK_iMaDK)} 
										{if $pl.FK_iMaDK > 1} - Độ khẩn: <b style="color:red; font-size:16px"> {$dokhan[0].sTenDoKhan} </b>{else}{/if}
										</p>
                                        {if !empty($pl.sNoiDung)}<br><p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
										{if $vanban['iQuyenHan_DHNB'] == 7}
                                        <span><span style="color: red">*</span> Chỉ đạo của lãnh đạo phòng:</b></span><br>
                                        <span><i>{$pl.smotald}</i></span><br>
										{/if}
										{if $pl.iSTCChuTri == 1 || $pl.iVanBanTBKL == 1 || $pl.iToCongTac == 1 || $pl.iSTCPhoiHop == 1 || $pl.iVanBanQPPL == 1}
                                        <h5>- Loại VB: <b style="color: blue"> <i>{($pl.iSTCChuTri == 1) ? 'SNV chủ trì.' : NULL }</i> <i>{($pl.iVanBanTBKL == 1) ? 'TBKL' : NULL }</i> <i>{($pl.iToCongTac == 1) ? 'TTHC' : NULL }</i> <i>{($pl.iSTCPhoiHop == 1) ? 'VB Đảng' : NULL }</i> <i>{($pl.iVanBanQPPL == 1) ? 'VB QPPL' : NULL }</i></b></h5>
                                        {/if}
                                        {if !empty($pl.sLyDoTuChoiVanBanDen)}<p><b class="text-danger">Lý do từ chối của {$pl.sCVTuChoi}: </b><br><i>"{$pl.sLyDoTuChoiVanBanDen}"</i></p>{/if}
										<p>
										<div style="text-align:right">
										{$dsfile=layDuLieu('FK_iMaVBDen',$pl.PK_iMaVBDen,'tbl_files_vbden')}
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" target="_blank"><B style="margin-bottom:3px">Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a><br>
											{foreach $dsfile as $f}
											<p style="margin-bottom:3px">
												<a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></p>
											{/foreach}
                                        </div>
										</p>
										<a href="chuyenvienchutrixuly?vbd={$pl.PK_iMaVBDen}&kx=word"><button class="btn btn-primary btn-xs" style="" type="button" value="">Tải phiếu trình</button></a> 	
										&nbsp;&nbsp;
										{if date('Y') == $year}
											<a href="vanban?isoden={$pl.iSoDen}&mvb={$pl.PK_iMaVBDen}"><button class="btn btn-primary btn-xs" style="" type="button" value="">Tạo VB đi</button></a> 	
											&nbsp;&nbsp;
										{/if}
                                    </td>
                                    <td class="ykien">
                                        <div style="margin-bottom: 10px">
                                            <select name="phongchutri[{$pl.PK_iMaVBDen}]" id="" class="form-control phongchutri1">
                                                <option value="">Chọn chuyên viên</option>
                                                {foreach $getDepartment as $cv}
                                                <option value="{$cv.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$cv.sHoTen}" {!empty($pl.PK_iMaCVCT)&&($pl.PK_iMaCVCT == $cv.PK_iMaCB) ? selected : NULL}>{$cv.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" >Chọn phối hợp</button>
                                        </div>
                                        <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCVPH)&&($pl.PK_iMaCVPH) ? $pl.PK_iMaCVPH : NULL }" id="{$pl.PK_iMaVBDen}" class="hidden mangphoihop1">
                                        <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden doc_id1">
                                        <input type="text" name="cvct[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCNCT)&&($pl.PK_iMaCNCT) ? $pl.PK_iMaCNCT : NULL }" class="hidden">
                                        <input type="text" name="vbpp[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCNPP)&&($pl.PK_iMaCNPP) ? $pl.PK_iMaCNPP : NULL }" class="hidden">
                                        <input type="text" name="vbpp1[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaPP)&&($pl.PK_iMaPP) ? $pl.PK_iMaPP : NULL }" class="hidden">
                                        <div style="">
                                            <p><input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{!empty($pl.sHanThongKe)&&($pl.sHanThongKe > '2017-01-01') ? date_select($pl.sHanThongKe) : NULL}" class="form-control  hangiaiquyet1" placeholder="Hạn xử lý" readonly="readonly" ></p>
                                        </div>

										<p>{if $pl.iTrangThai_ThemHan != 1}
                                        <button type="button" name="dexuathan"  value="{$pl.PK_iMaVBDen}" data-id="{if !empty($pl.ChiDao)}{$pl.ChiDao}{/if}" data-num="{if !empty($pl.PK_iMaLDCD)}{$pl.PK_iMaLDCD}{/if}" class="btn btn-success btn-xs" style="padding: 5px;" data-toggle="modal" data-target="#myModal5" >Đề xuất thêm hạn</button>
                                        {/if}</p>										
										{if $vanban['iQuyenHan_DHNB'] == 7}
                                        {if $pl.iTrangThai != 1 && $pl.iTrangThai_TruyenNhan == 6}
                                        <button type="button" name="tctp"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#myModal1" >Từ chối</button>
                                        {/if}
                                        {/if}
										<!--<p><a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.dem  > 0} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}</p>-->
										 
                                    </td>
                                    <td class="chidao">
                                        <!--<p style="text-align:center">{if $pl.vbdaura_tp == 1}<span style="color: red"><b>Lãnh đạo yêu cầu văn bản trả lời</b></span>{/if}</p>-->
                                        <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="6">{!empty($pl.MoTaCVCT)&&($pl.MoTaCVCT) ? $pl.MoTaCVCT : NULL}</textarea>
										<br>
                                        <button type="submit" class="btn btn-primary btn-xs pull-right" name="duyet" value="{$pl.PK_iMaVBDen}" style="margin-bottom: 10px" ><i class="fa fa-check"></i><b>Duyệt</b></button>
                                    </td>
                                    
                                </tr>
                                {/foreach}
                                </tbody>
                            </table>
							<div class="" style="margin-bottom:-20px; margin-top:-20px; text-align:center">{$phantrang}</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly_tp.js"></script>
<script>
    $(document).ready(function() {
        $("button[name=tctp]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=maphongbantp]').val($(this).val());
            });
        });
        $("button[name=dexuathan]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=mavbd]').val($(this).val());
                $('button[name=themhan]').text('Gửi Lãnh đạo ( '+$(this).attr("data-id")+' )');
                $('input[name=ldgui]').val($(this).attr("data-num"));
            });
        });
    });
</script>
