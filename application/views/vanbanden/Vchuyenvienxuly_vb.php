<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Phối hợp xử lý văn bản
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" action="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-3"><h4><label for="">VB QPPL:</label> <input type="checkbox" name="iVanBanQPPL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanQPPL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">SNV chủ trì:</label> <input type="checkbox" name="iSTCChuTri" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iSTCChuTri'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">TBKL:</label> <input type="checkbox" name="iVanBanTBKL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanTBKL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực: </label>
                                    <div class="col-sm-8">
                                        <label for=""></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLVB'] : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">  Ngày ký <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">  Lĩnh vực:</label>
                                    <div class="col-sm-8">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <i><b><p style="">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</p></b></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-info">*</span>: </label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenNguoiKy'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Ngày nhận <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayNhan']) : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Chức vụ <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sChucVu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Hạn giải quyết:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getdAppointment[0]['sHanGiaiQuyet']) : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }</label>
                                    </div>
                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p>
											
												{if $getdAppointment[0]['FK_iMaDM'] == 2} <b style="color:orange">Mật</b> {else}
													{if $getdAppointment[0]['FK_iMaDM'] == 3} <b style="color:red">Tối mật</b> {else}
													Bình thường {/if}{/if}
										</p>
                                    </div>
                                    <label for="" class="col-sm-1">Độ khẩn:</label>
                                    <div class="col-sm-2">
                                        <p>
											{if $getdAppointment[0]['FK_iMaDK'] == 2} <b style="color:orange"> Khẩn </b>{else}
													{if $getdAppointment[0]['FK_iMaDK'] == 3} <b style="color:red">Tối khẩn </b> {else}
													Bình thường {/if}{/if}
										</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2">Số trang:</label>
                                    <div class="col-sm-10">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoTrang'] : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"> Tham mưu:</label>
                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày phân loại:</label>
                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
							
							
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="width:50%">Tệp tin đính kèm văn bản đến:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered" style="width: 550px">
                                        <thead>
                                        <tr>
                                            <th class="text-center" style="width: 50px">STT</th>
                                            <th>Ghi chú tệp tin</th>
                                            <th class="text-center">Tải về</th>
                                            <th class="text-center" style="width:20%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {if !empty($Filedinhkem)}
                                        {foreach $Filedinhkem as $fi}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td>{$fi.sTenFile}</td>
                                            <td class="text-center"> 
											<a class="tin1" href="{$fi.sDuongDan}" target="_blank">Xem</a> | <a class="tin1" href="{$fi.sDuongDan}" download>Download</a>
											</td>
                                            <td class="text-center" width="15%">{date_select($fi.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
														
                            {if !empty($idDocGo)}
								<div class="col-sm-12">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>
										<label for="" class="col-sm-4 control-label" style="float:right; ">Hạn Lãnh đạo giao: <b style="font-size:16px; color:red">{date_select($getdAppointment[0]['sHanThongKe'])}</b></label>
									</div>
									<div class="form-group">
										<table class="table table-bordered table-striped">
											<thead>
											<tr>
												<th class="text-center" width="50px">STT</th>                                            
												<th class="text-center" width="15%">Chuyển từ</th>											
												<th>Nội dung</th>
												<th class="text-center" width="15%">Chuyển đến</th>
												<th class="text-center" width="12%">Thời gian</th>
											</tr>
											</thead>
											<tbody>
											{$i=1}
											{foreach $process as $pr}
											<tr class="info">
												<td class="text-center">{$i++}</td>
												<td class="text-center">
													{foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
														{if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
															{$cb.sHoTen}
														{/if}
													{/foreach}
												</td>
												<td>{$pr.sMoTa}</td>
												<td class="text-center">{$pr.sHoTen}</td>
												<td class="text-center">{date_time2($pr.sThoiGian)}</td>
											</tr>
											{/foreach}
											</tbody>
										</table>
									</div>
								</div>
								
								{if !empty($chidao)}
								kkkkkkkk
									<div class="col-sm-12">
										<div class="form-group">
											<label for="" class="col-sm-4 control-label" style="color:blue">Chỉ đạo của lãnh đạo Sở:</label>
										</div>
										<div class="form-group">
											<table class="table table-bordered table-striped" style="width: 88%">
												<thead>
												<tr style="background-color: orange !important">
													<th class="text-center" style="width:50px">STT</th>
													<th class="text-center" style="width:17%">Lãnh đạo</th>
													<th>Nội dung</th>
													<th class="text-center" style="width:17%">Thời gian</th>
												</tr>
												</thead>
												<tbody>
												{$i=1}
												
												{foreach $chidao as $cd}
												{$ld = layTTCB($cd.FK_iMaCB)}
												<tr class="info">
													<td class="text-center">{$i++}</td>                                            
													<td class="text-center">{$ld[0].sHoTen}</td>
													<td>{$cd.sNoiDung}</td>
													<td class="text-center">{date_time2($cd.sThoiGian)}</td>
												</tr>
												{/foreach}
												</tbody>
											</table>
										</div>
									</div>
								{/if}
								
								{if !empty($dexuat)}
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Đề xuất / ý kiến:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped" style="width: 88%">
                                        <thead>
                                        <tr style="background-color: orange !important">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="width:17%">Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th class="text-center" style="width:17%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										
                                        {foreach $dexuat as $dx}
										{$ld = layTTCB($dx.FK_iMaCB)}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>                                            
                                            <td class="text-center">{$ld[0].sHoTen}</td>
                                            <td>{$dx.sNoiDung}</td>
                                            <td class="text-center">{date_time2($dx.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							{/if}

								{if !empty($TrinhTuPhoiHop)}
								<div class="col-sm-12">
									<div class="form-group">
										<label for="" class="col-sm-12 control-label">Thông tin cán bộ phối hợp:</label>
									</div>
									<div class="form-group">
										<table class="table table-bordered table-striped">
											<thead>
											<tr class="phoihop">
												<th class="text-center" style="width: 50px">STT</th>
												
												<th class="text-center" style="width: 15%">Chuyển từ</th>
												<th>Nội dung</th>
												<th class="text-center"style="width: 15%">Chuyển đến</th>
												<th class="text-center" style="width: 12%">Ngày nhận</th>
											</tr>
											</thead>
											<tbody>
											{$i=1}
											{foreach $TrinhTuPhoiHop as $ph}
											<tr class="warning">
												<td class="text-center">{$i++}</td>
												
												<td class="text-center">
													{foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb1}
													{if $cb1.PK_iMaCB == $ph.input_per}
													{$cb1.sHoTen}
													{/if}
													{/foreach}
												</td>
												<td>{$ph.sMoTa}</td>
												<td class="text-center">{$ph.sHoTen}</td>
												<td class="text-center">{date_time2($ph.sThoiGian)}</td>
											</tr>
											{/foreach}
											</tbody>
										</table>
									</div>
								</div>
								{/if}
								
								{if !empty($resultPPH)}
								<div class="col-sm-12">
									<div class="form-group">
										<label for="" class="col-sm-10 control-label">Kết quả giải quyết - phòng phối hợp:</label>
									</div>
									<div class="form-group">
										<table class="table table-bordered">
											<thead>
											<tr>
												<th style="width: 50px">STT</th>
												<th>Cán bộ - Đơn vị</th>
												<th>Nội dung</th>
												<th class="text-center">Tài liệu</th>
												<th>Thời gian</th>
											</tr>
											</thead>
											<tbody>
											{$i=1}
											{foreach $resultPPH as $re}
											<tr >
												<td class="text-center">{$i++}</td>
												<td width="25%">{$re.sHoTen} - {$re.sTenPB}</td>
												<td>{$re.sMoTa}</td>
												<td width="8%" class="text-center"><a href="">-</a></td>
												<td width="15%" class="text-center">{date_time2($re.date_file)}</td>
											</tr>
											{/foreach}
											</tbody>
										</table>
									</div>
								</div>
								{/if}
								{if !empty($resultPTL)}
								<div class="col-sm-12">
									<div class="form-group">
										<label for="" class="col-sm-10 control-label">Kết quả giải quyết - phòng chủ trì:</label>
									</div>
									<div class="form-group">
										<table class="table table-bordered">
											<thead>
											<tr>
												<th style="width: 50px">STT</th>
												<th>Cán bộ - Đơn vị</th>
												<th>Nội dung</th>
												<th class="text-center">Kết quả</th>
												<th class="text-center">Thời gian</th>
											</tr>
											</thead>
											<tbody>
											{$i=1}
											{foreach $resultPTL as $tl}
											<tr >
												<td class="text-center" style="width:50px">{$i++}</td>
												<td style="width:25%">{$tl.sHoTen} - {$tl.sTenPB}</td>
												<td >{$tl.sMoTa}</td>
												<td width="8%" class="text-center">
												 {if $tl.sDuongDanFile !=''  && $tl.sDuongDanFile != 'doc_uploads/'}
														
													<a href="{$url}{$tl.sDuongDanFile}">Xem</a>
													{/if}
												</td>
												<td width="15%" class="text-center">{date_select($tl.date_file)}</td>
											</tr>
											{/foreach}
											</tbody>
										</table>
									</div>
								</div>
								{/if}
								 <form class="form-horizontal" method="post" action=""  enctype="multipart/form-data">
								<div class="col-sm-12">
									<div class="col-sm-6" style="margin-left:25%">
										<div class="form-group">
											<label for="" class="col-sm-12">Kết quả / nội dung phối hợp <br></label>
											<div class="col-sm-12" >
												<textarea name="ketqua" id="" class="form-control" rows="4">Lưu phối hợp.</textarea>
											</div>
										</div>
									</div>
									{if $getdAppointment[0]['sHanGiaiQuyet'] < date('Y-m-d',time())}
									<div class="col-sm-6">
										<div class="form-group">
											<label for="" class="col-sm-12">Lý do chưa kết thúc văn bản:<br> <span style="color: red"><i>(do phải thu thập thông tin từ các đơn vị khác ,...)</i></span></label>
											<div class="col-sm-12">
												<textarea name="chammuon" id="" class="form-control" rows="4"></textarea>
											</div>
										</div>
									</div>
									{/if}
									
									<!--<div class="col-sm-6" style="margin-top:40px" >
										<div class="form-group noidung">
											<div class="col-sm-6">
												<input type="file" name="files" required tabindex="6" class="form-control" readonly="" placeholder="Click vào đây để chọn file" >
											</div>
										</div> 
										<div class="col-md-5" style="width:100%">
												<div style="width:100%; padding-top: 7px; ">
											   (Dung lượng tối đa: 20MB; định dạng được chấp nhận: pdf, doc, docx, xls, xlsx, jpg, jpeg, png)
											   </p>
												</div>
										</div>
									</div>-->
									
								</div>
								</form>
                            {/if}
							
                            <div class="col-md-12" style="margin-left:44%">
                                <button class="btn btn-primary" type="submit" name="luulai" value="Lưu lại">Hoàn thành</button>
                                <input type="text" name="canbo" value="{if !empty($CBChiDao[0]['input_per'])}{$CBChiDao[0]['input_per']}{/if}" class="hidden"/>
                            </div>
                        </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $('.phoihop').css('background-color', '#FF9800')
    });
</script>