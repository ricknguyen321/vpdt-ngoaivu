<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post" enctype="multipart/form-data">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="30%">Thông tin văn bản</th>
                                    <th width="30%">Yêu cầu / Kết quả đề xuất</th>
                                    <th width="30%">Ý kiến gửi đi</th>
                                    <th width="5%" class="text-center">Gửi</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td >
                                            <p>Số đến : <b>{$dl.iSoDen}</b></p>
                                            <p>Trích yếu: 
                                                <a href="{$url}quatrinhxuly/{$dl.PK_iMaVBDen}" class="tin"> - {$dl.sMoTa}</a>
                                            </p>
                                            <p>Hạn văn bản:{(!empty($dl.sHanThongKe)&&($dl.sHanThongKe>'1970-01-01'))?date_select($dl.sHanThongKe):''}</p>
                                            <p>
                                                File văn bản: </a>{if !empty($dl.sDuongDan)}<a target="_blank" href="{$dl.sDuongDan}" id="{$dl.PK_iMaVBDen}" class="tin1 layma">Xem </a>{/if}
                                            </p>
                                        </td>
                                        <td >
                                            <p>Nội dung: <b>{$dl.sNoiDung}</b> {if $dl.CH_TL==2}{if !empty($dl.sFile_TraLoi)} <a target="_blank" href="{$url}{$dl.sFile_TraLoi}">Xem file</a> {/if}{else}{if !empty($dl.sFile_YeuCau)}<a target="_blank" href="{$url}{$dl.sFile_YeuCau}">Xem file</a>{/if}{/if}</p>
                                            <p>Người chuyển: {$dscanbo[$dl.FK_iMaNguoi_Gui]}</p>
                                            <p>Thời gian: {date_time($dl.sThoiGian_Chuyen)}</p>
                                        </td>
                                        <td class="text-center">
                                            <p {if $dl.CH_TL==2} class="hide" {/if}>
                                                <select name="canbophong_{$dl.PK_iMaVBDen}" style="width: 100%" id="{$dl.PK_iMaVBDen}" class="form-control select2 canbophong">
                                                    <option value="0">-- Chọn cán bộ giải quyết --</option>
                                                    {if !empty($canbophong)}
                                                        {foreach $canbophong as $cbp}
                                                            <option value="{$cbp.PK_iMaCB}">{$cbp.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <textarea name="ykien_{$dl.PK_iMaVBDen}" id="" rows="7" placeholder="Nhập ý kiến tại đây" class="form-control">{$dl.sNoiDung}</textarea>
                                                <input type="text" class="hide" name="file_{$dl.PK_iMaVBDen}" value="{$dl.sFile_YeuCau}">
                                                <input type="text" class="hide" name="fileTraloi_{$dl.PK_iMaVBDen}" value="{$dl.sFile_TraLoi}">
                                                <input type="text" class="hide" name="phongphoihop_{$dl.PK_iMaVBDen}" value="{$dl.FK_iMaPB_PH}">
                                            </p>
                                            <p>
                                                <input type="file" name="filetl_{$dl.PK_iMaVBDen}" readonly="" class="form-control">
                                            </p>
                                        </td>
                                        <td class="text-center">
                                            <p>
                                                {if $dl.CH_TL==1}
                                                <button name="guilen" value="{$dl.PK_iMaVBDen}" disabled="" id="gui_{$dl.PK_iMaVBDen}" class="btn btn-primary btn-xs">Gửi đi</button>
                                                {else}
                                                <button name="dongy" value="{$dl.PK_iMaVBDen}" id="gui_{$dl.PK_iMaVBDen}" class="btn btn-primary btn-xs">Đồng ý</button>
                                                {/if}
                                            </p>
                                            <p>
                                                {if $dl.CH_TL==1}
                                                <button name="tralai" value="{$dl.PK_iMaVBDen}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>
                                                {else}
                                                <button name="tralaiketqua" value="{$dl.PK_iMaVBDen}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>
                                                {/if}
                                            </p>
                                            <p>
                                                {if $dl.CH_TL==1}
                                                <button name="hoanthanh" value="{$dl.PK_iMaVBDen}" id="hoanthanh_{$dl.PK_iMaVBDen}" class="btn btn-primary btn-xs">Hoàn thành</button>
                                                {/if}
                                            </p>
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $(document).on('change','.canbophong',function(){
            var mavanban = $(this).attr('id');
            var canbo    = $(this).val();
            if(canbo>0)
            {
                $('#gui_'+mavanban).removeAttr('disabled');
            }
            else
            {
                $('#gui_'+mavanban).attr('disabled',true);
            }
        });
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>