<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{$url}giahanvanban" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{($sokyhieu)?$sokyhieu:''}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{($ngaynhap > '2017-01-01')?(date_select($ngaynhap)):''}" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Năm</label>

                                                    <div class="col-sm-8">
                                                        <select name="nam" id="" class="form-control">
                                                            <option value="">--- Chọn năm văn bản ---</option>
                                                            <option value="2017" {!empty($nam)&&($nam == 2017)? 'selected':''}>2017</option>
                                                            <option value="2018" {!empty($nam)&&($nam == 2018)? 'selected':''}>2018</option>
                                                            <option value="{date('Y')}" {!empty($nam)&&($nam)?($nam == date('Y'))?'selected':'':''}>{date('Y')}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-danger">*</span>:</label>

                                                    <div class="col-sm-8">
                                                        <div class="typeahead__container">
                                                            <div class="typeahead__field">
                                                                <span class="typeahead__query">
                                                                    <input tabindex="1" type="text" class="donvi form-control" value="{!empty($dv.sTenDV) &&($dv.sTenDV) ? $dv.sTenDV : NULL }" name="donvi" placeholder="Nhập nơi gửi đến">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{($ngayky > '2017-01-01')?(date_select($ngayky)):''}" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayden" value="{($ngayden > '2017-01-01')?(date_select($ngayden)):''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Phòng chủ trì</label>

                                                    <div class="col-sm-8">
                                                        <select name="sPhongChuTri" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn phòng chủ trì --</option>
                                                            {foreach layDuLieu(PK_iMaPB,11,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,12,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,13,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,14,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,15,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,17,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,19,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,20,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,21,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                            {foreach layDuLieu(PK_iMaPB,68,'tbl_phongban') as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {!empty($sPhongChuTri)&&($sPhongChuTri)?($sPhongChuTri==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                            {/foreach}

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <div class=" col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Trích yếu</label>
                                                        <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaymoi" class="form-control datepic datemask" id="" value="{($ngaymoi > '2017-01-01')?(date_select($ngaymoi)):''}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Người ký</label>

                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số đến</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="denngay" class="form-control datepic datemask" id="" value="{($denngay > '2017-01-01')?(date_select($denngay)):''}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Người nhập</label>

                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-12 deletepadding">
                    {$phantrang}
                </div>
                <div class="row">
                    <div class="col-md-12" style=" width: 100%;overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="6%" class="text-center">Ngày nhập</th>
                                    <th width="6%" class="text-center">Loại văn bản</th>
                                    <th width="35%" class="text-center">Trích yếu</th>
									
									<th width="" class="text-center">Hạn và lý do gia hạn</th>
                                </tr>
                            </thead>
                            <tbody>
                            {$i = 1}
                            {foreach $getDocGo as $vbd}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td>{date_select($vbd.sNgayNhap)}</td>
                                <td class="text-center">{$vbd.sTenLVB}</td>
                                <td>

                                        <b style="color: red">Số đến: {if $vbd.iSoDen <10}0{$vbd.iSoDen}{else}{$vbd.iSoDen}{/if}</b><br>

                                        <b style="color: blue">Số ký hiệu: {$vbd.sKyHieu}</b><br>

                                        {if $vbd.iGiayMoi == '1'}
                                        <a href="{$url}quytrinhgiaymoi/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a><br>
                                        (Vào hồi {$vbd.sGiayMoiGio} ngày {date_select($vbd.sGiayMoiNgay)}, tại {$vbd.sGiayMoiDiaDiem}) |
                                        (Số trang: {$vbd.iSoTrang}) |
                                        <b>{$vbd.sChuTri}</b><br>
                                        {else}
                                        <a href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a><br>
                                        (Số trang: {$vbd.iSoTrang})
                                        <b>{$vbd.sChuTri}</b><br>
                                        {/if}
                                    

                                    <p><i>Người nhập: {$vbd.sHoTen} </i></p>
                                    {if $vbd.sHanGiaiQuyet > '2017-01-01'}<p><b>Hạn nhập: {date_select($vbd.sHanGiaiQuyet)} </b></p>{/if}
                                    <span><i>{if !empty($vbd.sNoiDung)}Nội dung*:<br> <b>{$vbd.sNoiDung}<br>{/if}</b></i></span>
									<span style="float:right"><a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}"><i class="fa {if $vbd.iFile==0}fa-folder-open-o{else} fa-search{/if}" {if $vbd.iFile==0} style="color: gold;font-size: 20px;" {/if}></i></a>{if $vbd.iFile==1} <a target="_blank" href="{$vbd.sDuongDan}" class="tin1">{if $vbd.iMail == 1}Xem [file_pdf]{else}Xem{/if}</a>{/if}</span>

                                    <br>Nơi gửi : {$vbd.sTenDV}
                                    <br><b>Phòng chủ trì: {$vbd.sPhongChuTri}</b>
								</td>
  
								<td style="color: #000; font-size: 14px" class="text-left">
                                    <form action="{$url}giahanvanban" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-5 control-label">Hạn mới</label>

                                                <div class="col-sm-7">
                                                    <input type="text" name="ngaygiahan" class="form-control datepic datemask" id="" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Hạn cũ</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="hancu" class="form-control" id="" value="{if $vbd.iGiayMoi == 2}{($vbd.sHanGiaiQuyet <= $vbd.sHanThongKe && $vbd.iGiayMoi)?(date_select($vbd.sHanThongKe)):(date_select($vbd.sHanGiaiQuyet))}{/if}" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" col-md-12">
                                            <div class="form-group">
                                                <textarea name="lydogiahan" id="" class="form-control" rows="3" placeholder="Nhập lý do gia hạn."></textarea>
                                            </div>
                                        </div>
                                        <div class=" col-md-12">
                                            <div class="form-group">
                                               <!--  <input type="button" name="giahanvb_{$vbd.PK_iMaVBDen}" id="{$vbd.PK_iMaVBDen}" class="btn-warning" value="Gia hạn văn bản"> --> 
                                               <button type="submit" value="{$vbd.PK_iMaVBDen}" name="giahanvb">Gia hạn văn bản</button>
                                            </div>
                                        </div>   
                                    </form>

                                    <!-- <b>{if !empty($vbd.VBDi)}
                                        <input type="hidden" name="sNgayGiaiQuyet_{$vbd.PK_iMaVBDen}" value="{$vbd.sNgayGiaiQuyet}">

                                    <input type="button" name="danhgia" id="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#VPtraketqua" class="btn-warning" value="Trả lại">
                                    <br><br>

                                     có văn bản trả lời{/if}</b> -->
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <div><b>Tổng văn bản: {$count}</b></div>
                    </div>
                </div>
                <div class="col-sm-12 deletepadding">
                    {$phantrang}
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
function myFunction() {
    window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
}
</script>
<script>
    var data = {
    "noiden": [
        {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
        "{$dv.sTenDV}",
        {/foreach}
    ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".donvi",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });

    // trả lại văn bản để phòng thực hiện lại

    $(document).on('click','input[name=danhgia]',function(){
            var id = $(this).attr('id');
            var ngay_han = $('input[name=sNgayGiaiQuyet_'+id+']').val();
            //alert(id);alert(ngay_han);
            $('input[name=PK_iMaVBDen]').val(id);
            $('input[name=sNgayGiaiQuyet]').val(ngay_han);
        });
</script>
