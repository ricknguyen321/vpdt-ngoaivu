<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        <a href="javascript:void(0);" name="anhien">Tìm kiếm văn bản đến</a>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                <div class="col-sm-8">
                                    <select name="" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn nơi gửi đến--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class=" col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Trích yếu</label>
                                    <textarea name="" id="" class="form-control" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người ký</label>

                                <div class="col-sm-8">
                                    <select name="" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn người ký--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số đến</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Lãnh đạo chỉ đạo</label>

                                <div class="col-sm-8">
                                    <select name="" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn lạnh đạo --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                <div class="col-sm-8">
                                    <select name="" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn chức vụ --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người nhập</label>

                                <div class="col-sm-8">
                                    <select name="" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn người nhập --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Phòng ban thụ lý</label>

                                <div class="col-sm-8">
                                    <select name="" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn phòng ban thụ lý --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Loại văn bản</th>
                                    <th width="" class="text-center">Số đến</th>
                                    <th width="" class="text-center">Số ký hiệu</th>
                                    <th width="20%" class="text-center">Trích yếu</th>
                                    <th width="15%" class="text-center">Nơi gửi</th>
                                    <th width="" class="text-center">Tệp tin</th>
                                    <th class="text-center">Xóa</th>
                                    <th width="" class="text-center">Phòng chủ trì</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td>31-05-2017</td>
                                    <td class="text-center">Giấy mời</td>
                                    <td>13511</td>
                                    <td class="text-center">17</td>
                                    <td><a href="">V/v xin tạm ứng KP chi QLDA từ nguồn NS TP</a>  | (Số trang: 1) |   Chuyển đồng chí Hà Minh Hải - Giám đốc  

<p><i>Người nhập: Đào Quốc Hậu </i></p></td>
                                    <td>Trung tâm bảo tồn di sản Thăng Long-HN</td>
                                    <td class="text-center"><a href="" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xem tệp tin hoặc thêm"><i class="fa fa-search"></i></a></td>
                                    <td class="text-center"><button class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button></td>
                                    <td class="text-center"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
    	$(document).on('click','a[name=anhien]',function(){
    		$('.form-horizontal').toggle();
    	});
        $('.control-label').css('text-align', 'left');
        $('td').css('vertical-align', 'middle');
    });
</script>