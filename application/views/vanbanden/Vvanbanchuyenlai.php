<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Văn bản các phòng chuyển lại <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">

                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <span class="pull-right">{$phantrang}</span>
							<h5 style="width:100%; text-align:center"><a href="vanbanchoxuly"><b>NHẤN VÀO ĐÂY ĐỂ QUAY LẠI MỤC VĂN BẢN CHỜ PHÂN LOẠI</b></a></h5>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Trích yếu - Thông tin</th>
                                    <!--<th width="15%" class="text-center">Lý do</th>-->
                                    <!--<th width="100px" class="text-center">Ghi chú</th>-->
                                    <th width="12%" >Tài liệu</th>
                                    <th width="40%" class="text-center">Lý do từ chối</th>
                                </tr>
                                </thead>
                                <tbody>
                                {if $vanban['iQuyenHan_DHNB'] == 3}
                                {$i=1}
                                {foreach $Docawaitdir as $pl}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-center">{date_select($pl.sNgayNhap)}</td>
                                    <td>
                                        <p><b><a href="{$url}quatrinhxuly/{$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
                                        - Số ký hiệu: {$pl.sKyHieu} <br>
                                        - Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> <br>
                                        <p>- Nơi gửi: <span style="color: blue;">{$pl.sTenDV}</span></p>
                                        
                                        {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
										
										
										 
										
										
										
                                    </td>
                                    <!--<td></td>-->
                                    <!--<td class="text-center">{$pl.sGhiChu}</td>-->
                                    <td class="ykien">
										<p><a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> {if $pl.iFile==1}| <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}</p>
                                    </td>
                                    <td class="chidao">
                                         <p>{if !empty($pl.PhongChuyenLai['sTenPB'])}- Phòng chuyển lại: <b style="color:blue">{$pl.PhongChuyenLai['sTenPB']}</b>{/if}</p>
										- Lý do: <b style="color:red">{$pl.sLyDo}</b>
										
                                    </td>
                                    
                                </tr>
                                {/foreach}
                                {/if}
                                </tbody>
								
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly.js"></script>
