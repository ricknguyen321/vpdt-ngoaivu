<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách văn bản
                    </h3>
                </div>
                <div class="col-sm-9">
                </div>
            </div>
        </div>
    </section>

   

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{$url}thongkeVBDtheocanbo" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>


                                               <div class="form-group">
                                                   <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>
                                               
                                                   <div class="col-sm-8">
                                                       <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{($ngaynhap > '2017-01-01')?(date_select($ngaynhap)):''}" id="">
                                                   </div>
                                               </div> 
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-danger">*</span>:</label>

                                                    <div class="col-sm-8">
                                                        <div class="typeahead__container">
                                                            <div class="typeahead__field">
                                                                <span class="typeahead__query">
                                                                    <input tabindex="1" type="text" class="donvi form-control" value="{!empty($dv.sTenDV) &&($dv.sTenDV) ? $dv.sTenDV : NULL }" name="donvi" placeholder="Nhập nơi gửi đến">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayden" value="{($ngayden > '2017-01-01')?(date_select($ngayden)):''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>

                                              
                                            <div class="col-md-12">
                                                <button class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-12 deletepadding">
                    {$phantrang}
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="6%" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Loại văn bản</th>
                                    <th width="" class="text-center">Số đến</th>
                                    <th width="" class="text-center">Số ký hiệu</th>
                                    <th width="35%" class="text-center">Trích yếu</th>
                                    <th width="15%" class="text-center">Nơi gửi</th>
									{if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 9}
                                    <th width="5%" class="text-center">Tác vụ</th>
									{/if}
                                    <th width="" class="text-center">Phòng chủ trì</th>
									<th width="" class="text-center">Kết quả thực hiện</th>
                                </tr>
                            </thead>
                            <tbody>
                                <th>I</th>
                                <th colspan="10">I. CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ
                               
                                </th>
                            {$i = 1}
                            {foreach $getDocGo as $vbd}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td>{date_select($vbd.sNgayNhap)}</td>
                                <td class="text-center">{$vbd.sTenLVB}</td>
                                <td style="color: red; font-size: 16px" class="text-center"><b>{$vbd.iSoDen}</b></td>
                                <td class="text-center">{$vbd.sKyHieu}</td>
                                <td>
                                    {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9 || $vanban['iQuyenHan_DHNB'] == 3}
                                        {if $vbd.iGiayMoi == '1'}
                                        <a href="{$url}nhapgiaymoi/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a> <br>
                                        (Vào hồi {$vbd.sGiayMoiGio} ngày {date_select($vbd.sGiayMoiNgay)}, tại {$vbd.sGiayMoiDiaDiem}) |
                                        (Số trang: {$vbd.iSoTrang}) |
                                        <b>{$vbd.sChuTri}</b><br>
                                        {else}
                                        <a href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a> <br>
                                        (Số trang: {$vbd.iSoTrang})
                                            <b>{$vbd.sChuTri}</b><br>
                                        {/if}
                                    {else}
                                        {if $vbd.iGiayMoi == '1'}
                                        <a href="{$url}quytrinhgiaymoi/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a><br>
                                        (Vào hồi {$vbd.sGiayMoiGio} ngày {date_select($vbd.sGiayMoiNgay)}, tại {$vbd.sGiayMoiDiaDiem}) |
                                        (Số trang: {$vbd.iSoTrang}) |
                                        <b>{$vbd.sChuTri}</b><br>
                                        {else}
                                        <a href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a><br>
                                        (Số trang: {$vbd.iSoTrang})
                                        <b>{$vbd.sChuTri}</b><br>
                                        {/if}
                                    {/if}

                                    <p><i>Người nhập: {$vbd.sHoTen} </i></p>
                                    {if $vbd.sHanGiaiQuyet > '2017-01-01'}<p><b>Hạn nhập: {date_select($vbd.sHanGiaiQuyet)} </b></p>{/if}
                                    <span><i>{if !empty($vbd.sNoiDung)}Nội dung*:<br> <b>{$vbd.sNoiDung}<br>{/if}</b></i></span>
									<span style="float:right"><a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}"><i class="fa {if $vbd.iFile==0}fa-folder-open-o{else} fa-search{/if}" {if $vbd.iFile==0} style="color: gold;font-size: 20px;" {/if}></i></a>{if $vbd.iFile==1} <a target="_blank" href="{$vbd.sDuongDan}" class="tin1">{if $vbd.iMail == 1}Xem [file_pdf]{else}Xem{/if}</a>{/if}</span>
								</td>
                                <td class="text-center">{$vbd.sTenDV}</td>
								{if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 9}
                                <td class="text-center" >
                                    {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9}
                                    <button type="submit" value="{$vbd.PK_iMaVBDen}" name="xoavanban" onclick="return confirm('Bạn muốn xóa văn bản{$vbd.sMoTa} ?')" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Xóa" style="margin-top: 20px"><i class="fa fa-trash"></i></button>
                                    {/if}
                                    <a href="{if $vbd.iGiayMoi == '1'}{$url}nhapgiaymoi/{$vbd.PK_iMaVBDen}{else}{$url}vanbanden/{$vbd.PK_iMaVBDen}{/if}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="sửa văn bản" style="margin-top: 20px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </td>
								{/if}
                                <td class="text-center" style="padding-top: 20px">
                                    <b>{$vbd.sPhongChuTri}</b>
                                </td>
								<td style="color: blue; font-size: 14px" class="text-center">

                                    <b>{if !empty($vbd.VBDi)}
                                        <input type="hidden" name="sNgayGiaiQuyet_{$vbd.PK_iMaVBDen}" value="{$vbd.sNgayGiaiQuyet}">

                                    {$vbd.iSoVBDi}{$vbd.sKyHieuVBDi}
                                    {/if}</b></td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>

                        
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center" rowspan="2">STT</th>
                                    {if $iQuyenHan_DHNB < 7}
                                    <th width="8%" class="text-center" rowspan="2">Cán bộ thực hiện</th>
                                    {/if}
                                    <th width="8%" class="text-center" rowspan="2">Trách nhiệm giải quyết</th>
                                    <th width="8%" class="text-center" rowspan="2">Ngày nhận</th>
                                    <th width="6%" class="text-center" rowspan="2">Số ký hiệu</th>
                                    <th width="" class="text-center" rowspan="2">Nội dung</th>
                                    <th width="8%" class="text-center" rowspan="2">Hạn xử lý</th>
                                    <th width="15%" class="text-center" colspan="3">Kết quả thực hiện</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" >Hoàn Thành</th>
                                    <th width="5%" class="text-center">Đang thực hiện</th>
                                    <th width="5%" class="text-center">Tình trạng</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>II</th>
                                <th colspan="9">NHIỆM VỤ LÃNH ĐẠO GIAO, NHIỆM VỤ THƯỜNG XUYÊN, NHIỆM VỤ KHÁC
                               
                                </th>
                            </tr>
                       
                            {$i = 1}
                            {foreach $list_kh as $kh}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                {if $iQuyenHan_DHNB < 7}
                                <td width="8%" class="text-center">
                                    {if $kh.lanhdao_id != $kh.user_input && $kh.user_input != $kh.canbo_id}
                                    <b>Trưởng đơn vị giao việc</b><br>
                                    {/if}
                                    {foreach $canbo as $cb}
                                        {if $kh.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}
                                    {if $kh.canbo_id != $kh.user_input}
                                        {foreach $canbo as $cb}
                                            {if $kh.lanhdao_id == $cb.PK_iMaCB}
                                                <span style="color: blue"><i>(LĐ chỉ đạo {$cb.sHoTen})</i></span>
                                            {/if}
                                        {/foreach}
                                    {else}
                                        <span style="color: blue">(Cán bộ tự xây dựng kế hoạch)</span>
                                    {/if}

                                </td>
                                {/if}
                                <td>
                                    {if $kh.loai_kh ==5}
                                        Nhiệm vụ khác
                                    {/if}
                                    {if $kh.loai_kh ==3 or $kh.loai_kh ==4}
                                        Nhiệm vụ thường xuyên
                                    {/if}
                                    {if $kh.loai_kh ==2 && $iQuyenHan_DHNB == 8}
                                       {if $kh.canbo_id == $PK_iMaCB} Lãnh đạo giao {else} Phối hợp giải quyết {/if}
                                    {/if}
                                    {if $kh.loai_kh ==2 && $iQuyenHan_DHNB < 8}
                                       Lãnh đạo giao
                                    {/if}
                                    
                                    {if $iQuyenHan_DHNB >= 7}
                                    {if $kh.lanhdao_id != $kh.user_input && $kh.user_input != $kh.canbo_id}
                                    <b>Trưởng đơn vị giao việc</b><br>
                                    {/if}
                                    {if $kh.canbo_id != $kh.user_input}
                                        {foreach $canbo as $cb}
                                            {if $kh.lanhdao_id == $cb.PK_iMaCB}
                                                <span style="color: blue"><i>(LĐ chỉ đạo {$cb.sHoTen})</i></span>
                                            {/if}
                                        {/foreach}
                                    {else}
                                        {if $iQuyenHan_DHNB == 7 || $iQuyenHan_DHNB == 11 }
                                        <span style="color: blue"><br> (Cán bộ xây dựng kế hoạch:
                                            {foreach $canbo as $cb}
                                                {if $kh.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                            {/foreach})
                                        </span>
                                        {else}
                                        <span style="color: blue">(Cán bộ xây dựng kế hoạch)</span>
                                        {/if}
                                    {/if}
                                    {/if}
                                </td>
                                <td>{date_select($kh.ngay_nhan)}</td>
                                <td class="text-center">{$kh.vanban_skh}</td>
                                <td class="text-left" align="text-left">
                                    {if $PK_iMaCB == $kh.user_input && (($kh.active < 2 && $iQuyenHan_DHNB == 8)||($kh.active == 2 && $iQuyenHan_DHNB == 11)||($kh.active == 2 && $iQuyenHan_DHNB == 7) ||($kh.active == 3 && ($iQuyenHan_DHNB == 6 || $iQuyenHan_DHNB == 3)))}
                                    <a href="kehoachcongtac?id={$kh.kh_id}">{$kh.kh_noidung} - {date_time($kh.date_nhap)}
                                    {if $kh.pp_chidao != ''}<br>
                                    <b>Chỉ đạo của phó trưởng đơn vị:</b> {$kh.pp_chidao} - ({date_time($kh.pp_ngaygiao)})
                                    {/if}

                                    (Người nhận: {$dscanbo[$kh.canbo_id]})</a>
                                    
                                    {else}
                                    {$kh.kh_noidung} - {date_time($kh.date_nhap)}
                                    {if $kh.pp_chidao != ''}<br>
                                    <b>Chỉ đạo của phó trưởng đơn vị:</b> {$kh.pp_chidao} - ({date_time($kh.pp_ngaygiao)})
                                    {/if}
                                    (Người nhận: {$dscanbo[$kh.canbo_id]})
                                    {/if}

                                    {if $kh.ykien_pp !=''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                        <br><b>Ý kiến của {foreach $canbo as $cb}
                                                {if $kh.lanhdao_id == $cb.PK_iMaCB}{if $cb.iQuyenHan_DHNB ==7}phó phòng {else}trưởng phòng {/if} {$cb.sHoTen}{/if}
                                            {/foreach}:</b><br> <i>{$kh.ykien_pp} ({date_time($kh.ngay_ykien)})</i>
                                    {/if}
                                    {if $kh.ykien_tp !=''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                        <br><b>Ý kiến của lãnh đạo đơn vị {foreach $canbo as $cb}
                                                {if $truongdonvi == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                            {/foreach}:</b><br> <i>{$kh.ykien_tp} ({date_time($kh.ngay_danhgia)})</i>
                                    {/if}
                                    
                                    {if $kh.khokhan_vuongmac != ''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    <br>
                                    <label>Lý do chưa hoàn thành: </label>
                                    <span style="color: red">{$kh.khokhan_vuongmac} - {date_time($kh.ngay_hoanthanh)}</span>

                                        
                                    {/if}

                                    {if $kh.lydorahan != ''}
                                        <br><span style="color: blue">{$kh.lydorahan}</span>
                                    {/if}
                                    {if $kh.ngayhanmoi > '2018-01-01'}
                                        <br>Ngày gia hạn: <span style="color: blue">
                                        {date_select($kh.ngayhanmoi)}</span>
                                    {/if}
                                    {if $kh.lydorahan != '' || $kh.ngayhanmoi > '2018-01-01'}
                                        <br>Thời điểm gia hạn: <span style="color: blue">{date_time($kh.ngayrahanmoi)}</span>
                                    
                                    {/if}

                                    {if $kh.ket_qua_hoanthanh != ''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    <br>
                                    <label>Kết quả hoàn thành: </label>
                                    <span style="color: blue">{$kh.ket_qua_hoanthanh} - {date_time($kh.ngay_hoanthanh)}</span>
                                    {/if} 
                                    {if isset($kh.filename)}<br>
                                    <label>Tệp tin đính kèm: </label>
                                    {foreach $kh.filename as $file}
                                        {if $file !=''}<a href="kehoach_2019/{$file}"> [File] </a>{/if}
                                    {/foreach}
                                    {/if}
{$kiemtra=1}
{if isset($kh.file_phoihop)}
{if isset($kh.file_phoihop.0.canbo_id)}<br><i> Danh sách phối hợp:</i>{/if}
    {foreach $kh.file_phoihop as $phoihop}    
        <br> - <b>{$array_cb[$phoihop.canbo_id]}: </b>{$phoihop.lydo} &nbsp;&nbsp; 
        {if $phoihop.file_upload !=''}<a href="kehoach_2018/{$phoihop.file_upload}">[File] </a>{/if}

        {if $phoihop.canbo_id == $PK_iMaCB && $phoihop.active == 1 && $phoihop.chutri == 2}{$kiemtra=2}{/if}
    {/foreach}
{/if}
                                    {if $kh.ketluan_pp != ''}<hr style="margin-bottom: 6px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    <b>Nhận xét của {foreach $canbo as $cb}
                                                {if $kh.lanhdao_id == $cb.PK_iMaCB}{if $cb.iQuyenHan_DHNB ==7}phó phòng {else}trưởng phòng {/if} {$cb.sHoTen}{/if}
                                            {/foreach}:</b><br> 
                                    <span style="color: blue;">{$kh.ketluan_pp}</span>
                                    {/if}

                                    {if $kh.danh_gia != '' or ($kh.danh_gia == '' and $kh.active == 5) }<hr style="margin-bottom: 6px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    {if  $kh.active == 3 &&  $kh.chatluong == 0}
                                    <label style="color: red">Kết luật việc chưa hoàn thành của trưởng đơn vị: </label>
                                    {else}
                                    <label>Kết luận của trưởng đơn vị: </label>
                                    {/if}
                                    <span style="color: blue">{$kh.danh_gia} - {date_time($kh.ngay_danhgia)}</span>
                                    {if $kh.sangtao==2} 
                                        <br><span style="color: red"><b>Có tính sáng tạo</b></span>{/if}
                                    {/if}
                                    {if $kh.chatluong==2} 
                                        <br><span style="color: red"><b>Chất lượng chưa đảm bảo</b></span>
                                    {/if}

                                  
                                </td>
                                <td class="text-center">{if $kh.ngay_han >'2017-01-01'}{date_select($kh.ngay_han)}{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh.active == 5}X{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh.active < 5}X{/if}</td>
                                <td class="text-center" style="color: green"> 
                                    {if ($kh.ngay_han >= $date_for_friday && $kh.active < 5) || ($kh.ngay_han >= date_insert($kh.ngay_hoanthanh)  && $kh.active == 5) || $kh.ngay_han < '2017-01-01' }Trong hạn{/if}

                                    {if $kh.ngay_han < $date_for_friday && $kh.active < 5 && $kh.ngay_han >'2017-01-01'}<span style="color: red">Quá hạn </span>{/if}

                                </td>
                            </tr>
                            {/foreach}
                   
</tbody>
</table>



                        <div><b>Tổng văn bản: {$count+$count_kh}</b></div>
                    </div>
                </div>
                <div class="col-sm-12 deletepadding">
                    {$phantrang}
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
function myFunction() {
    window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
}
</script>
<script>
    var data = {
    "noiden": [
        {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
        "{$dv.sTenDV}",
        {/foreach}
    ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".donvi",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });

    // trả lại văn bản để phòng thực hiện lại

    $(document).on('click','input[name=danhgia]',function(){
            var id = $(this).attr('id');
            var ngay_han = $('input[name=sNgayGiaiQuyet_'+id+']').val();
            //alert(id);alert(ngay_han);
            $('input[name=PK_iMaVBDen]').val(id);
            $('input[name=sNgayGiaiQuyet]').val(ngay_han);
        });
</script>
