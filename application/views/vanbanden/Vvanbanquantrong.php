<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Văn bản quan trọng - Tổng số: <b>{$count}</b>
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label" style="float:left">Loại:</label>

                                <div class="col-sm-7 sl2">
                                    <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="this.form.submit()">
                                        <option value="1" {!empty($cacloaivanban)&&($cacloaivanban)?($cacloaivanban=="1")?'selected':'':''}>Chưa giải quyết</option>
                                        <option value="2" {!empty($cacloaivanban)&&($cacloaivanban)?($cacloaivanban=="2")?'selected':'':''}>Đã giải quyết</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{($sokyhieu)?$sokyhieu:''}">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="cacloaivanban" value="{($cacloaivanban)?$cacloaivanban:''}">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{(ngaynhap)?ngaynhap:''}" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Năm</label>

                                                    <div class="col-sm-8">
                                                        <select name="nam" id="" class="form-control">
                                                            <option value="">--- Chọn năm văn bản ---</option>
                                                            <option value="2017" {!empty($nam)&&($nam == 2017)? 'selected':''}>2017</option>
                                                            <option value="{date('Y')}" {!empty($nam)&&($nam)?($nam == date('Y'))?'selected':'':''}>{date('Y')}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayden" value="{($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class=" col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Trích yếu</label>
                                                        <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaymoi" class="form-control datepic datemask" id="" value="{($ngaymoi)?$ngaymoi:''}">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số đến</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="denngay" class="form-control datepic datemask" id="" value="{($denngay)?$denngay:''}">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Người nhập</label>

                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                    <div class="col-sm-8 deletepadding">
                        {$phantrang}
                    </div>
					<div class="col-sm-4 pull-right">
                        <button type="submit" class="btn btn-success pull-right btn-sm" name="boquantrong" value="Duyệt"><i class="fa fa-trash"></i> Bỏ quan trọng</button>
                    </div><br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Số đến</th>
                                    <th width="" class="text-center">Trích yếu</th>
                                    <th width="15%" class="text-center">Nơi gửi</th>
                                    <th width="15%" class="text-center">Phòng chủ trì</th>
                                    <th width="130px" class="text-center">Tác vụ</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i = 1}
                                {foreach $getDocGo as $vbd}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                     <td class="text-center">
										<p><b style="color: red; font-size: 18px" >{$vbd.iSoDen}</b></p>
										{date_select($vbd.sNgayNhap)}
									</td>
                                    <td>
                                        <p><b><a style="color:black;" href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}"><span>{$vbd.sMoTa}</span></a></b></p>
                                       
                                        <p>- Ký hiệu: {$vbd.sKyHieu}<br></p>
										<span><i>{if !empty($vbd.sNoiDung)}Nội dung*: {$vbd.sNoiDung}{/if}</i></span>
										
										<p style="text-align:right">
											<a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}" ><b>Tài liệu</b> <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $vbd.iFile==1} <a target="_blank" href="{$vbd.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($vbd.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}
											</p>
										</td>
                                    <td>{$vbd.sTenDV}</td>
                                    
                                    <td class="text-center" style="padding-top: 20px">
                                        <b>{$vbd.sPhongChuTri}</b>
                                    </td>
                                    <td class="text-center" >
                                        <span style="color: red;"><input type="checkbox" name="vanbanquantrong[{$vbd.PK_iMaCN}]" value="0"> Bỏ Quan trọng</span>
                                    </td>
                                </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        {$phantrang}
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    function myFunction() {
        window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
    }
</script>
