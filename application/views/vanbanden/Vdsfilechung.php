<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header" style="padding: 0px">
      <div class="row">
         <div class="col-sm-12 deletepadding1">
            <div class="col-sm-3">
               <h3 class="font">
                  Danh sách tài liệu, biểu mẫu chung <b>({$count})</b>
               </h3>
            </div>
            <div class="col-sm-9" style="">
               <div class="col-md-5" style="margin-top:20px">
                  <form action="" method="get">
                     <div class="form-group">
                        <input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm kiếm tổng hợp">
                     </div>
                  </form>
               </div>
               <div class="col-md-5" style="margin-top:20px">
                  <form action="" method="get">
                     <div class="form-group">
                        {if $vanban['PK_iMaCB'] == 705 || $vanban['iQuyenHan_DHNB'] <= 6 } 
                        <a  class="font" name="anhien" href="javascript:void(0);"><button tabindex="11" type="button"  class="btn btn-primary btn-sm">Upload biểu mẫu</button> </a>
                        {/if}
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Default box -->
      <div class="box">
         <div class="box-body">
            <form class="form-horizontal hide" method="post" enctype="multipart/form-data" autocomplete="off">
               <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="col-md-6">
                           <div class="form-group noidung">
                              <label for="" class="col-sm-3 control-label"  style="width:33.333333%">Chọn file *</label>
                              <div class="col-sm-9"  style="width:66.666666666%">
                                 <input type="file" name="files[]" placeholder="Click vào đây để chọn files" class="form-control" id="" multiple="">
                              </div>
                           </div>
                           <div class="themsau"></div>
                           <div style="text-align:right;margin-bottom:10px"><button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;text-align:right"><i class="fa fa-plus"></i></button><b class="text-danger"> Tải thêm</b>
						   </div>
						   <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="width:33.333333%; padding-right:0px; ">Chuyên mục</label>
                                    <div class="col-sm-8" style="width:66.66666666%">
                                        <input type="text"  class="form-control" tabindex="7" value="{if !empty($thongtin)}{$thongtin[0]['category']}{else}{if !empty($category)}{$category}{/if}{/if}" name="category" placeholder="">
                                    </div>
                                </div>
								
                        </div>
                        <div class="col-md-6">
							 <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Phòng ban</label>

                                    <div class="col-sm-8">
                                        <select name="phong"  id="" class="form-control kyhieu select2" style="width:100%">
                                            {if !empty($dsphong)}
                                                {foreach $dsphong as $d}
                                                    <option value="{$d.sTenPB}" {($thongtin)?($thongtin[0]['phong']=={$d.sTenPB})?'selected':'':($vanban['FK_iMaPhongHD']==$d.PK_iMaPB)?'selected':''}>{$d.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="width:33.333333%; padding-right:0px; ">Ghi chú</label>
                                    <div class="col-sm-8" style="width:66.66666666%">
                                        <input type="text" class="form-control" tabindex="7" value="{if !empty($thongtin)}{$thongtin[0]['note']}{else}{if !empty($note)}{$note}{/if}{/if}" name="note" placeholder="">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="width:33.333333%; padding-right:0px; ">Lĩnh vực</label> 
                                    <div class="col-sm-8" style="width:66.66666666%">
                                        <input type="text"  class="form-control" tabindex="7" value="{if !empty($thongtin)}{$thongtin[0]['tag']}{else}{if !empty($tag)}{$tag}{/if}{/if}" name="tag" placeholder="">
                                    </div>
                                </div>
                        </div>
                        <div class="form-group">
                           <label for=""class="col-sm-3"></label>
                           <div class="col-sm-12" style="text-align:center;margin-top:30px">
                              <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Upload'}</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
            <div class="row">
               <div class="col-md-12">
                  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                     <span class="pull-right">{$phantrang}</span>
					 <i></i>
                     <table id="" class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th width="50px" class="text-center">STT</th>
                              <th width="35%" class="text-center">Tên file</th>
                              <th width="15%" class="text-center">Chuyên mục</th>
                              <th width="15%" class="text-center">Phòng ban</th>
                              <th width="12%" class="text-center">Lĩnh vực</th>
                              <th width="" class="text-center">Người tải</th>
                           </tr>
                        </thead>
                        <tbody>
                           {$i=1}
                           {if !empty($dsfiles)}
                           {foreach $dsfiles as $f}
                           <tr>
                              <td class="text-center">{$i++}</td>
                              <td>
                                 <a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a>
                              </td>
                              <td class="text-center">
                                 {$f.category}
                              </td>
                              <td class="text-center">
                                 {$f.phong}
                              </td>
                              <td class="text-center">
                                 {$f.tag}
                              </td>
                              <td class="text-center">
                                <p>{$f.note}</p>
								<i>{$mangcb[$f.FK_iMaCB]} {date_time2($f.sThoiGian)}</i>
								{if $vanban['PK_iMaCB'] == 705 || $vanban['PK_iMaCB'] == $f.FK_iMaCB}
									<hr style="margin:5px">
									<a href="dsfilechung?id={$f.id}" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
									<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Bạn có chắc muốn xóa?');" name="xoa" value="{$f.id}"><i class="fa fa-trash"></i></button>

								{/if}
                              </td>
                           </tr>
                           {/foreach}
                           {/if}
                        </tbody>
                     </table>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</div>
<script>
   $(document).ready(function() {
       $(".formSentMsg").delay(5200).fadeOut(300);
       $('td').css('vertical-align', 'middle');
       $('th').css('vertical-align', 'middle');
       // $('td').css('text-align', 'justify');
       $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
       $(document).on('click','a[name=anhien]',function(){
           $('.form-horizontal').toggle();
           $('.form-horizontal').removeClass('hide');
       });
       $('.control-label').css('text-align', 'right');
   });
   
   $(document).ready(function() {
   $(document).on('click','button[name=themmoi]',function(){
           var noidung1 = $('.noidung')[0].outerHTML;
           var noidung = noidung1;
           $('.themsau').before(noidung);
       });
   });
   
</script>