<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="refresh" content="30"> 30 giây refesh trang 1 lần-->
  <link href="" rel="shortcut icon" type="image/x-icon" />
  <title>Giấy mời họp</title>
  <base href="">
  <meta name="author" content="Sở Ngoại Vụ">
  <meta name="copyright" content="Sở Ngoại Vụ">
  <meta name="description" content="LỊCH HỌP DO LÃNH ĐẠO SỞ CHỦ TRÌ ">
  <meta name="robots" content="noindex,nofollow">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">
  <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$url}assets/js/plugins/font-awesome/css/font-awesome.min.css">
  <style>
    .container {
        font-family:"Times New Roman", Times, serif;
        font-size:13pt;
    }
    .indam{
        font-weight: bold;
    }
  </style>
</head>

<body><br/>
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12" style="float:right">
                <form action="" method="get">
                    <!--<div class="col-md-3">
                        <select name="lanhdao" id="" class="form-control" onchange="this.form.submit()">
                            <option value="">-- Chọn lãnh đạo --</option>
                            {if !empty($lanhdao)}
                                {foreach $lanhdao as $ld}
                                    {if $ld.iQuyenHan_DHNB >3}
                                        <option value="{$ld.PK_iMaCB}" {($malanhdao)?($malanhdao==$ld.PK_iMaCB)?'selected':'':''}>{($ld.iQuyenHan_DHNB==4)?'GĐ':'PGĐ'}. {$ld.sHoTen}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                    </div>-->
					
					<div class="col-md-3">
                        <div class="form-group">
                             <h3 class="indam text-center"><i class="fa fa-calendar"></i> LỊCH HỌP</h3>
                        </div>
                    </div>
                    <div class="col-md-1" style="margin-right:10px">
                        <div class="form-group">
                            <a href="{$url}lichcongtac?tuan={$tuanhientai}" class="btn btn-primary"><i class="fa fa-calendar"></i> Tuần này </a>
                        </div>
                    </div>
                    <div class="col-md-1 text-center">
                        <div class="form-group">
                            <a href="{$url}lichcongtac?tuan={$tuantruoc}" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="tuan" id="" class="form-control select2" onchange="this.form.submit()">
                                {for $i=1;$i<54;$i++}
                                    <option value="{($i<10)?'0':''}{$i}" {($tuan==$i)?'selected':''}>Tuần {$i}</option>
                                {/for}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 text-center">
                        <div class="form-group">
                            <a href="{$url}lichcongtac?tuan={$tuansau}" class="btn btn-primary"><i class="fa fa-forward"></i></a>
                        </div>
                    </div>
                </form>
                <div class="col-md-3">
                   <!-- <a href="{$url}lichhoplanhdao" class="btn btn-success"> Lịch họp của Sở chủ trì</a>-->
                </div>
            </div>
        </div>
		<div class="col-md-12">
		Ghi chú màu trích yếu: <b style="color:#da8a0b">Lịch cán bộ các phòng nhập</b> &nbsp;&nbsp;&nbsp;&nbsp;<b style="color:#337ab7">Lịch họp từ giấy mời đến</b>
		<span class="pull-right">Lọc theo lãnh đạo: {foreach $bgd as $ld}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="lichcongtac?ldso={$ld.PK_iMaCB}&tuan={$tuan}"><b style="color:{if $ld.sHoTen == 'Ngô Minh Hoàng'} red {elseif $ld.sHoTen == 'Nguyễn Nam Hải'} green {else} blue {/if}">{$ld.sHoTen}</b></a>{/foreach} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="lichcongtac?tuan={$tuan}"><b style="color:black">Show all</b></a></span>
		</div>
        <div class="row">
        <form action="" method="post" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr style="background:#1373bf; color:white">
                        <th width="10%" class="text-center">Thời gian</th>
                        <th width="" class="text-center">Nội dung giấy mời / cuộc họp</th>
                        <th width="35%" class="text-center">LĐ chủ trì/dự họp; Phòng chủ trì nội dung</th>
                        <!--<th width="10%" class="text-center">Tài liệu</th>-->
                        <!--<th class="text-center">Soạn báo cáo</th>-->
                    </tr>
                    {if !empty($ngaytrongtuan)}
                        {foreach $ngaytrongtuan as $n}
                            <tr>
								
								{if {$n.0} == 'Thứ Hai'}
                                <td colspan="6" style="background-color:orange; color:white; padding-left: 10%">
                                    <b style=" font-size: 20px; text-shadow: 1px 1px 2px black; ">{$n.0} - {$n.1}</b>
                                </td>
								{elseif {$n.0} == 'Thứ Ba'}
								<td colspan="6" style="background-color:#1373bf; color:white; padding-left: 20%">
                                    <b style=" font-size: 20px; text-shadow: 1px 1px 2px black; ">{$n.0} - {$n.1}</b>
                                </td>
								{elseif {$n.0} == 'Thứ Tư'}
								<td colspan="6" style="background-color:orange; color:white; padding-left: 30%">
                                    <b style=" font-size: 20px; text-shadow: 1px 1px 2px black; ">{$n.0} - {$n.1}</b>
                                </td>
								{elseif {$n.0} == 'Thứ Năm'}
								<td colspan="6" style="background-color:#1373bf; color:white; padding-left: 40%">
                                    <b style=" font-size: 20px; text-shadow: 1px 1px 2px black; ">{$n.0} - {$n.1}</b>
                                </td>
								{elseif {$n.0} == 'Thứ Sáu'}
								<td colspan="6" style="background-color:orange; color:white; padding-left: 50%">
                                    <b style=" font-size: 20px; text-shadow: 1px 1px 2px black; ">{$n.0} - {$n.1}</b>
                                </td>
								{elseif {$n.0} == 'Thứ Bảy'}
								<td colspan="6" style="background-color:#1373bf; color:white; padding-left: 60%">
                                    <b style=" font-size: 20px; text-shadow: 1px 1px 2px black; ">{$n.0} - {$n.1}</b>
                                </td>
								{elseif {$n.0} == 'Chủ Nhật'}
								<td colspan="6" style="background-color:orange; color:white; padding-left: 70%">
                                    <b style=" font-size: 20px; text-shadow: 1px 1px 2px black; ">{$n.0} - {$n.1}</b>
                                </td>
								{/if}
                            </tr>
                            {if isset($n.sang)}
							
                            <!--<tr>
                                <td colspan="6" class="text-center" style="background:#0080622e;">
                                   
                                </td>
                            </tr>-->
                            
                            {foreach $n.sang.giaymoi as $sang}
								{$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
								{$dsld = explode(',', $sang.FK_iMaCB_Ky)}
								{$lddh = array()}
								{foreach $dsld as $ld}{$ld = trim($ld)}{$kkkk = array_push($lddh, $dscanbo[$ld])}{/foreach}
								{$duhop = implode(', ', $lddh)}								
								
                            {if isset($sang.PK_iMaVBDi)}
								{if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;font-size: 14px;" ><p style="font-weight:bold; font-size:25px; color:blue">{$sang.sGioMoi}</p><p>{$n.0}</p>{$n.1}</td>
                                    <td style="vertical-align: middle;padding-top: 20px;">
                                        
                                        <p><b style="color:#da8a0b; font-size:14px">{$sang.sMoTa}</b></p>
                                        <p>- Địa điểm: <b>{$sang.sDiaDiemMoi}</b></p>
										<p>- Cán bộ nhập lịch họp: {$dscanbo[$sang.FK_iMaCB_Nhap]} - <i style="color: silver">{date_time2($sang.sNgayNhap)}</i></p>
										<p> {if !empty($sang.sChiDao)} - Chỉ đạo của lãnh đạo:<br> "<span style="color:red">{$sang.sChiDao}</span>" - <b>{$dscanbo[$sang.FK_iMaCB_Nhap]}</b>
											{/if}
										</p>
                                        
										<p style="text-align:right"><a data-toggle="collapse" href="#collapseExample{$sang.PK_iMaVBDi}" aria-expanded="false" aria-controls="collapseExample{$sang.PK_iMaVBDi}" style=""><B>Tải tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a>
										  <div class="col-sm-12 collapse " id="collapseExample{$sang.PK_iMaVBDi}" style="">
											 <div class="col-sm-10">
												<label for="" class="col-sm-3 control-label"  style="">Chọn file *</label>
												<div class="col-sm-9"  style="">
													<input type="file" name="{$sang.PK_iMaVBDi}files[]" placeholder="Click vào đây để chọn files" class="form-control" id="" multiple="">
												</div>
											 </div>
											 <div class="col-sm-2">
												<button type="submit" name="luutl" value="{$sang.PK_iMaVBDi}" class="btn btn-primary btn-sm">Lưu</button>
											 </div>
										  </div>
										</p>
										
											{$dsfile = layTLHop($sang.PK_iMaVBDi)}
											<div style="max-height:250px;  overflow:auto">
												{foreach $dsfile as $f}
												
													<p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 90)}</span></a> <i style="color: silver">({$dscanbo[$f.FK_iMaCB]} - {date_time2($f.sThoiGian)})</i></p>
												
												{/foreach}
											</div>
                                    </td>
                                    <td>
									LĐ chủ trì/dự họp: <b style="font-size:15px;color:{if $duhop == 'Ngô Minh Hoàng'} red {elseif $duhop == 'Nguyễn Nam Hải'} green {else} blue {/if}">{$duhop}</b><br><hr>
									Chủ trì nội dung:<br>
									<p>{($sang.sPhongCT) ? $sang.sPhongCT: ''}{($sang.sPhongCT) ? ' chủ trì': ''}
									</p>
										{if $sang.sPhongPH != ''}
											Phòng phối hợp: {$sang.sPhongPH}
										{/if}	
										{if $sang.FK_iMaCB_Nhap == $macanbo || $macanbo == 735 || $macanbo == 696 || $macanbo == 705}

												<hr><p style="text-align:right">
													<a href="{$url}nhaplichhop?id={$sang.PK_iMaVBDi}" data-toggle="tooltip" data-placement="top" title="Sửa lịch họp" class="btn btn-default" target="_blank"><i class="fa fa-edit"></i></a>  <button type="submit" name="xoa" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" value="{$sang.PK_iMaVBDi}" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" class="btn btn-default"><i class="fa fa-trash-o"></i></button>
													</p>

										{/if}		
									</td>
                                </tr>
                            {else}
								{if isset($ldso) && $ldsang != $dscanbo[$ldso]}{continue}{/if}
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;font-size: 14px;"><p style="font-weight:bold; font-size:25px; color:blue">{$sang.sGiayMoiGio}</p><p>{$n.0}</p>{$n.1}</td>
                                    <td style="vertical-align: middle;padding-top: 20px;">
                                       
                                        <p><a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px">{$sang.sMoTa}</b></a></p>
										{if !empty($sang.sNoiDung)}
                                        <p>{$sang.sNoiDung}</p>
                                        {/if}
                                        <p>{$sang.sGhiChu}</p>
                                        - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
										- GM số: {$sang.sKyHieu}<br>
										- Nơi gửi: {$sang.sTenDV} <br>
										
										{$dschidao = laychidao($sang.PK_iMaVBDen)}
										{if !empty($dschidao)}
											- Chỉ đạo của Lãnh đạo:<br> 
											{foreach $dschidao as $cd}
												<p>
												<span style="color: red">"{$cd.sNoiDung}"</span> - <b>{$dscanbo[$cd.FK_iMaCB]}</b> - {date_time2($cd.sThoiGian)}</p>
											{/foreach}
										{/if}
										<p style="text-align:right"><a href="teptinden?id={$sang.PK_iMaVBDen}" target="_blank"><B>Tải tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p>
											{$dsfile = layTLHop2($sang.PK_iMaVBDen)}
											<div style="max-height:250px;  overflow:auto">
												{foreach $dsfile as $f}
												
													<p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 90)}</span></a> <i style="color: silver">({$dscanbo[$f.FK_iMaCB]} - {date_time2($f.sThoiGian)})</i></p>
												
												{/foreach}
											</div>
											
                                    </td>
                                    <td>
									
									LĐ chủ trì/dự họp: <b style="font-size:15px;color:{if $ldsang == 'Ngô Minh Hoàng'} red {elseif $ldsang == 'Nguyễn Nam Hải'} green {else} blue {/if}">{if $sang.PK_iMaVBDen == 0} Ngô Minh Hoàng, Nguyễn Nam Hải, Trần Nghĩa Hòa {else}{$ldsang}{/if}</b>
									<br><hr>
									Chủ trì nội dung:<br>
									{$sang.chidao}
                                        <p>{$demphs=0}
                                            {if $sang.phoihop[0]>0} Phòng phối hợp:
                                            {foreach $sang.phoihop as $phs}
                                            <!-- {$demphs++} -->
                                                {($demphs>1)?',':''}{$mangphongban[$phs]}
                                            {/foreach}
                                            {/if}
                                        </p>
                                    </td>
                                    <!--<td>
                                        {if !empty($sang.baocaohop)}
                                            {$so=0}
                                            {foreach $sang.baocaohop as $bc}
                                                {if $so>1}{if !empty($bc.sDuongDanFile) && $bc.sDuongDanFile!='doc_uploads/'}
                                                <hr style="border:1px dashed red">{/if}
                                                {/if}
                                                <p>{if !empty($bc.sDuongDanFile) && $bc.sDuongDanFile!='doc_uploads/'}<a target="_blank" href="{$bc.sDuongDanFile}">{($bc.sTenFiles)?$bc.sTenFiles:'File'}</a> - {$bc.sHoTen} - {$bc.sTenPB}{/if}</p>
                                            {/foreach}
                                        {/if}
                                        {if $macanbo==617}
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="text" name="tenfile[{$sang.PK_iMaVBDen}]" class="form-control" style="height: initial;padding: inherit;" placeholder="Nhập tên file">
                                            </div>
                                            <div class="form-group">
                                                <input type="file" name="files[{$sang.PK_iMaVBDen}]" class="form-control" style="height: initial;padding: inherit;">
                                            </div>
                                            <div class="form-group">
                                                <button name="luulai" value="{$sang.PK_iMaVBDen}" class="btn btn-primary btn-xs">Lưu lại</button>
                                            </div>
                                        </div> 
                                        {/if}                                                   
                                    </td>-->
                                    <!--<td><a href="{$url}soanbaocao/{$sang.PK_iMaVBDen}" target="_blank"  class="btn btn-primary btn-xs">Soạn báo cáo KQ họp</a></td>-->
                                </tr>
                                {/if}
                            {/foreach}
                            {/if}
                            {if isset($n.chieu)}
                            <!--<tr>
                                <td colspan="6" class="text-center" style="background:#0080622e;">
                                    
                                </td>
                            </tr>-->
                            
                            {foreach $n.chieu.giaymoi as $chieu}
                            {if isset($chieu.PK_iMaVBDi)}
								{if isset($ldso) && $chieu.sHoTen != $dscanbo[$ldso]}{continue}{/if}
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;font-size: 14px;"><p style="font-weight:bold; font-size:25px; color:#a94442">{$chieu.sGioMoi}</p><p>{$n.0}</p>{$n.1}</td>
                                    <td style="vertical-align: middle;padding-top: 20px;">
                                        <p><b style="color:#da8a0b; font-size:14px">{$chieu.sMoTa}</b></p>
                                        <p>- Địa điểm: <b>{$chieu.sDiaDiemMoi}</b></p>
										<p>- Cán bộ nhập lịch họp: {$dscanbo[$chieu.FK_iMaCB_Nhap]} - <i style="color: silver">{date_time2($chieu.sNgayNhap)}</i></p>
										<p> {if !empty($chieu.sChiDao)} - Chỉ đạo của lãnh đạo:<br> "<span style="color:red">{$chieu.sChiDao}</span>" - <b>{$dscanbo[$chieu.FK_iMaCB_Nhap]}</b>
											{/if}
										</p>
										
										<p style="text-align:right"><a data-toggle="collapse" href="#collapseExample{$chieu.PK_iMaVBDi}" aria-expanded="false" aria-controls="collapseExample{$chieu.PK_iMaVBDi}" style=""><B>Tải tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a>
										  <div class="col-sm-12 collapse " id="collapseExample{$chieu.PK_iMaVBDi}" style="">
											 <div class="col-sm-10">
												<label for="" class="col-sm-3 control-label"  style="">Chọn file *</label>
												<div class="col-sm-9"  style="">
													<input type="file" name="{$chieu.PK_iMaVBDi}files[]" placeholder="Click vào đây để chọn files" class="form-control" id="" multiple="">
												</div>
											 </div>
											 <div class="col-sm-2">
												<button type="submit" name="luutl" value="{$chieu.PK_iMaVBDi}" class="btn btn-primary btn-sm">Lưu</button>
											 </div>
										  </div>
										  </p>
										  
											{$dsfile = layTLHop($chieu.PK_iMaVBDi)}
											<div style="max-height:250px;  overflow:auto">
												{foreach $dsfile as $f}
												
													<p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 90)}</span></a> <i style="color: silver">({$dscanbo[$f.FK_iMaCB]} - {date_time2($f.sThoiGian)})</i></p>
												
												{/foreach}	
											</div>											
                                    </td>
                                    <td>
									LĐ chủ trì/dự họp: <b style="font-size:15px;color:{if $chieu.sHoTen == 'Ngô Minh Hoàng'} red {elseif $chieu.sHoTen == 'Nguyễn Nam Hải'} green {else} blue {/if}">{$chieu.sHoTen}</b><br><hr>
									Chủ trì nội dung:<br>
									<p>{($chieu.sPhongCT) ? $chieu.sPhongCT: ''} {($chieu.sPhongCT) ? ' chủ trì': ''} 
										
										</p>
										{if $chieu.sPhongPH != ''}
											Phòng phối hợp: {$chieu.sPhongPH} 
										{/if}
										{if $chieu.FK_iMaCB_Nhap == $macanbo || $macanbo == 735 || $macanbo == 696 || $macanbo == 705}
							
												<hr><p style="text-align:right">
													<a href="{$url}nhaplichhop?id={$chieu.PK_iMaVBDi}" data-toggle="tooltip" data-placement="top" title="Sửa lịch họp" class="btn btn-default" target="_blank"><i class="fa fa-edit"></i></a>  <button type="submit" name="xoa" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" value="{$chieu.PK_iMaVBDi}" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" class="btn btn-default"><i class="fa fa-trash-o"></i></button>
													</p>
					
										{/if}
									</td>
                                </tr>
                            {else}
								{if isset($ldso) && $chieu.lanhdaoduhop != $dscanbo[$ldso]}{continue}{/if}
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;font-size: 14px;" ><p style="font-weight:bold; font-size:25px; color:#a94442">{$chieu.sGiayMoiGio}</p><p>{$n.0}</p>{$n.1}</td>
                                    <td style="vertical-align: middle;padding-top: 20px;">
									
										<p><a href="quytrinhgiaymoi/{$chieu.PK_iMaVBDen}" target="_blank"><b style="font-size:14px">{$chieu.sMoTa}</b></a></p>
										{if !empty($chieu.sNoiDung)}
                                        <p>{$chieu.sNoiDung}</p>
                                        {/if}
                                        <p>{$chieu.sGhiChu}</p>
                                        - Địa điểm: <b>{$chieu.sGiayMoiDiaDiem}</b><br>
										- GM số: {$chieu.sKyHieu}<br>
										- Nơi gửi: {$chieu.sTenDV}<br>

										{$dschidao2 = laychidao($chieu.PK_iMaVBDen)}
										{if !empty($dschidao2)}
											- Chỉ đạo của Lãnh đạo:<br> 
											{foreach $dschidao2 as $cd2}
												<p>
												<span style="color: red">"{$cd2.sNoiDung}"</span> - <b>{$dscanbo[$cd2.FK_iMaCB]}</b> - {date_time2($cd2.sThoiGian)}</p>
											{/foreach}
										{/if}
										
										<p style="text-align:right"><a href="teptinden?id={$chieu.PK_iMaVBDen}" target="_blank"><B>Tải tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p>
											{$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
											<div style="max-height:250px;  overflow:auto">
												{foreach $dsfile as $f}												
														<p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 90)}</span></a> <i style="color: silver">({$dscanbo[$f.FK_iMaCB]} - {date_time2($f.sThoiGian)})</i></p>
													
												{/foreach}
											</div>
                                    </td>
                                    <td>
									LĐ chủ trì/dự họp: <b style="font-size:15px;color:{if $chieu.lanhdaoduhop == 'Ngô Minh Hoàng'} red {elseif $chieu.lanhdaoduhop == 'Nguyễn Nam Hải'} green {else} blue {/if}">{$chieu.lanhdaoduhop}</b><br><hr>
									Chủ trì nội dung:<br>
									{$chieu.chidao}
                                        <p>{$demphc=0}
                                            {if $chieu.phoihop[0]>0} Phòng phối hợp:
                                            {foreach $chieu.phoihop as $phc}
                                            <!-- {$demphc++} -->
                                                {($demphc>1)?',':''}{$mangphongban[$phc]}
                                            {/foreach}
                                            {/if}
                                        </p>
                                    </td>
                                    <!--<td>
                                        {if !empty($chieu.baocaohop)}
                                            {$so=0}
                                            {foreach $chieu.baocaohop as $bc}
                                                
                                                {if $so>1}{if !empty($bc.sDuongDanFile) && $bc.sDuongDanFile!='doc_uploads/'}
                                                <hr style="border:1px dashed red">{/if}
                                                {/if}
                                                <p>{if !empty($bc.sDuongDanFile) && $bc.sDuongDanFile!='doc_uploads/'}<a target="_blank" href="{$bc.sDuongDanFile}">{($bc.sTenFiles)?$bc.sTenFiles:'File'}</a> - {$bc.sHoTen} - {$bc.sTenPB}{/if}</p>
                                            {/foreach}
                                        {/if}
                                        {if $macanbo==617}
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="text" name="tenfile[{$chieu.PK_iMaVBDen}]" class="form-control" style="height: initial;padding: inherit;" placeholder="Nhập tên file">
                                            </div>
                                            <div class="form-group">
                                                <input type="file" name="files[{$chieu.PK_iMaVBDen}]" class="form-control" style="height: initial;padding: inherit;">
                                            </div>
                                            <div class="form-group">
                                                <button name="luulai" value="{$chieu.PK_iMaVBDen}" class="btn btn-primary btn-xs">Lưu lại</button>
                                            </div>
                                        </div> 
                                        {/if}                                                   
                                    </td>-->
                                    <!--<td><a href="{$url}soanbaocao/{$chieu.PK_iMaVBDen}" target="_blank"  class="btn btn-primary btn-xs">Soạn báo cáo KQ họp</a></td>-->
                                </tr>
                            {/if}
                            {/foreach}
                            {/if}
                            
                        {/foreach}
                    {/if}
                    </tbody>    
                </table>
            </div>
        </form>
        </div>
        <style>
            .open-small-chat1 {
                height: 38px;
                width: 38px;
                display: block;
                background: #3c8dbc;
                padding: 9px 8px;
                text-align: center;
                color: #fff;
                border-radius: 50%;
            }
        </style>
        
    <style>
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #0c0b0b;
        }
        hr{
            border-top: 1px solid #9E9E9E;
            margin: 10px 0px;
        }
        h2, h3 {
            margin-top: 0px;
            margin-bottom: 20px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('td').css('vertical-align', 'middle');
            $('th').css('vertical-align', 'middle');
        });
    </script>

</body>
</html>
