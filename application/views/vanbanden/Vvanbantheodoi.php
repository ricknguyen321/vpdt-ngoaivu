<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Văn bản GĐ giao chỉ đạo, đôn đốc <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a> </label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" class="Checkbox"> {$de.sTenPB}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4"> Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
					<br>
					<div class="col-sm-12 deletepadding" style="margin-bottom: 0px;">
							<div class="col-sm-3">
								<p style="font-size: 15px"><b>Màu trích yếu:</b></p>
							</div>
							<div class="col-sm-3">
								<span><b style="color: red"><i class="fa fa-circle" aria-hidden="true"></i></b>: Quá hạn</span>
							</div>
							<div class="col-sm-3">
							<span><b style="color: orange"><i class="fa fa-circle" aria-hidden="true"></i></b>: Sắp đến hạn</span>
							</div>
							<div class="col-sm-3">
							<span><b style="color: black"><i class="fa fa-circle" aria-hidden="true"></i></b>: Trong hạn</span>
							</div>
						</div>
						
                        <div class="col-sm-12" style="text-align:center">
                            {$phantrang}
                        </div>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="40%" class="text-center">Trích yếu - Thông tin</th>
                                <th width="10%" >Chủ trì</th>
                                <th width="" class="text-center">Chỉ đạo của GĐ</th>
                                {if $vanban['PK_iMaCB'] == 534}
                                <th width="" class="text-center">Duyệt</th>
                                {/if}
                            </tr>
                            </thead>
                            <tbody>
                            {if $vanban['iQuyenHan_DHNB'] == 5}
                            {$i=1}
                            {foreach $getDocAwait as $pl}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td>
                                    <p><b><a style="color:black;" href="{$url}quatrinhxuly/{$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
                                    - Số ký hiệu: {$pl.sKyHieu} <br>
                                    - Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> <br>
                                    - Nơi gửi: <span style="color: black;">{$pl.sTenDV}</span><br>
									- Ngày nhận văn bản: {date_select($pl.sNgayNhap)}<br>
                                    {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
									
									<hr style="margin:10px">
										{$dsfile=layDuLieu('FK_iMaVBDen',$pl.PK_iMaVBDen,'tbl_files_vbden')}
										<p><a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p>
											{foreach $dsfile as $f}
												<p style="margin-bottom:3px"><a href="{$f.sDuongDan}">{$f.sTenFile}</a></p>
											{/foreach}
                                </td>
                                <td width="18%" class="ykien text-center">
                                    
                                    <div style="margin-bottom: 10px; text-align:center">
                                      
                                            {foreach $getDepartment as $de}
												<b>{!empty($pl.PK_iMaPhongCT[6])&&($pl.PK_iMaPhongCT[6] == $de.PK_iMaPB) ? $de.sTenPB : NULL}</b>
											{/foreach}
                                        
                                    </div>
									Hạn giải quyết:
                                    <div style="margin-bottom: 10px; text-align:center">
									
										<b>
										{if $pl.sHanThongKe > '2017-01-01'}
											{date_select($pl.sHanThongKe)}
										{else}
											{date_select($pl.sHanGiaiQuyet)}
										{/if}
										</b>
                                    </div>
									<!--<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}-->
                                   
                                </td>
                                <td width="35%" class="chidao">
                                    <!--<textarea name="chidaogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}gd" class="form-control giamdoc" rows="2">{!empty($pl.ChuThich[4])&&($pl.ChuThich) ? $pl.ChuThich[4] : NULL}</textarea><br>-->
                                    <p>{!empty($pl.ChuThich[5])&&($pl.ChuThich) ? $pl.ChuThich[5] : NULL}</p>
                                    {!empty($pl.ChuThich[6])&&($pl.ChuThich) ? $pl.ChuThich[6] : NULL}<br>
                                    {if $vanban['PK_iMaCB'] == 534}
                                    <textarea name="ykienchidao[{$pl.PK_iMaVBDen}]" class="form-control" rows="4" placeholder="Nhập ý kiến chỉ đạo phòng...">{if $pl.iTrangThai_DonDoc == 1}{$pl.sNoiDungChiDaoDonDoc_PG}{/if}</textarea>
                                    {/if}
                                </td>
                                {if $vanban['PK_iMaCB'] == 534}
                                <td class="text-center">
                                    <button type="submit" class="btn btn-default1 btn-xs pull-center" name="chidaoykiem" value="{$pl.PK_iMaVBDen}" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Chỉ đạo</button>
                                    <br>{if $pl.iTrangThai_DonDoc == 1}(<b style="color: green"><i>Đã chỉ đạo</i></b>){/if}
                                </td>
                                {/if}
                            </tr>
                            {/foreach}
                            {/if}
                            </tbody>
                        </table>
                        <div class="col-sm-12" style="text-align:center">
                            {$phantrang}
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly.js"></script>
