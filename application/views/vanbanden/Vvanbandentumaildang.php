<script src="{$url}assets/js/vanbanden/vanbanden.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Nhập mới văn bản đến
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-sm-12 hidden">
                    <div class="col-md-8 noidunggiaymoi">
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Nội dung họp *</label>

                            <div class="col-sm-9">
                                <textarea name="noidunggiaymoi[]" class="form-control" rows="3" style="border: 1px solid red;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Địa điểm họp</label>

                            <div class="col-sm-9">
                                <input type="text" name="diadiemhop[]" value="" class="form-control" style="border: 1px solid red;">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 noidunggiaymoi">
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label" required>Giờ họp</label>

                            <div class="col-sm-8">
                                <input type="text" name="giohop[]" class="form-control" value="" placeholder="Ví dụ: {date('h:i',time())}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Ngày họp</label>

                            <div class="col-sm-8">
                                <input type="text" name="ngayhop[]" value="" class="form-control datepic datemask" id="" placeholder="Ví dụ: {date('d/m/Y',time())}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 hidden">
                    <div class="noidung">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nội dung</label>

                                <div class="col-sm-9">
                                    <textarea name="noidunghop[]" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Hạn giải quyết </label>

                                <div class="col-sm-8">
                                    <input tabindex="8" type="text" value="" name="hannoidung[]" class="form-control datepic datemask" placeholder="dd/mm/yyyy">
                                </div>
                                
                            </div>
                        </div>
                        <script>
                            $(document).ready(function() {
                                //Date picker
                                $('.datepic').datepicker({
                                    autoclose: true
                                });
                                //Datemask dd/mm/yyyy
                                $(".datemask").inputmask("dd/mm/yyyy");
                            });
                        </script>
                    </div>
                </div>
                <form class="form-horizontal" method="post" action="" autocomplete="on">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-2"><h5><label for="">VB QPPL:</label> <input name="qppl" value="1" type="checkbox"></h5></div>
                            <!--<div class="col-md-2"><h5><label for="">SNV chủ trì:</label> <input name="stc" value="1" type="checkbox"></h5></div>-->
                            <div class="col-md-2"><h5><label for="">TBKL:</label> <input name="tbkl" value="1" type="checkbox"></h5></div>
                            <!--<div class="col-md-2"><h5><label for="">Văn bản mật:</label> <input name="vbm" value="1" type="checkbox"></h5></div>-->
                            <div class="col-md-2"><h5><label for="">TTHC:</label> <input type="checkbox" name="iToCongTac" value="1"></h5></div>
                            <div class="col-md-2"><h5><label for="">VB Đảng:</label> <input type="checkbox" name="iSTCPhoiHop" value="1"></h5></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <select name="khuvuc" id="" class="form-control select2" style="width:100%;color: black;">
                                            <option value="">--- Chọn khu vực ---</option>
                                            {if !empty($khuvuc)}
                                                {foreach $khuvuc as $kv}
                                                    <option value="{$kv.sTenKV}">{$kv.sTenKV}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="2" style="color: black;" type="text" required name="sokyhieu" class="form-control" value="{$a = explode('/', $xml->STRKYHIEU)}{$a[0]}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" required="" id="" class="form-control select2 ngayhan" style="width:100%">
                                            <option value="">-- Chọn loại văn bản --</option>
                                                {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                    <option value="{$lvb.sTenLVB}" {($xml->STRLOAIVANBAN)?($xml->STRLOAIVANBAN==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                {/foreach}
                                        </select>
                                    </div>
                                    <!-- <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="4" style="color: black;" required type="text" class="loaivanban form-control ngayhan" value="{$xml->STRLOAIVANBAN}" name="loaivanban">
                                                </span>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="1" style="color: black;" type="text" required class="noiguiden form-control" value="{(!empty($xml->STRNOIGUI))?$xml->STRNOIGUI:$noigui}" name="noiguiden">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" style="color: black;" type="text" name="ngayky" required class="form-control datepic datemask" value="{kiemtrangay($xml->STRNGAYKY)}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">   Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="5" style="color: black;" type="text" class="linhvuc form-control" value="" name="linhvuc" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <textarea tabindex="6" style="color: black;" name="trichyeu" class="form-control" rows="2">{(!empty($xml->STRTRICHYEU))?$xml->STRTRICHYEU:$trichyeu}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 loaicongvan {if isset($xml->STRLOAIVANBAN) && $xml->STRLOAIVANBAN =='Giấy mời'}hide{/if}">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung </label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop[]" class="form-control" rows="3" placeholder="Nhập nội dung"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 loaicongvan {if isset($xml->STRLOAIVANBAN) && $xml->STRLOAIVANBAN =='Giấy mời'}hide{/if}">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"><== Hạn giải quyết của nội dung </label>

                                    <div class="col-sm-8">
                                        <input type="text" value="" name="hannoidung[]" class="form-control datepic datemask" placeholder="dd/mm/yyyy">
                                    </div>
                                </div>
                            </div>
                            <div class="themsau"></div>
                            <div class="col-md-12 loaicongvan text-right {if isset($xml->STRLOAIVANBAN) && $xml->STRLOAIVANBAN=='Giấy mời'}hide{/if}">
                                <div class="col-sm-12" style="margin-left:-60px; margin-top:-36px">
                                    <button type="button" name="themmoi" value="themmoi" class="btn btn-success" ><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                </div>
                            </div>
							
							
								<div class="col-md-6 {if $xml->STRLOAIVANBAN!='Giấy mời'}hide{/if}">
									
									<div class="form-group">
										<label for="" class="col-sm-4 control-label" style="color: blue">Giờ họp: (VD: 08:30)</label>
										<div class="col-sm-8">
											<input style="color: black;" type="text" name="giohop1" class="text-center" value="{if isset($xml->STRTHOIGIANHOP)}{$xml->STRTHOIGIANHOP}{/if}">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="col-sm-4 control-label" style="color: blue">Địa điểm:</label>
										<div class="col-sm-8">
											<input type="text" style="color: black; width: 100%;" name="diadiemhop1" style="" value="{if isset($xml->STRDIADIEM)}{$xml->STRDIADIEM}{/if}">
										</div>
									</div>
								</div>
								
								<div class="col-md-6 {if $xml->STRLOAIVANBAN!='Giấy mời'}hide{/if}">
									
									<div class="form-group">
										<label for="" class="col-sm-4 control-label" style="color: blue">Ngày họp:</label>
										<div class="col-sm-8">
											<input type="text" name="ngayhop1" class="datepic datemask" value="{if isset($xml->STRNGAYHOP)}{$xml->STRNGAYHOP}{/if}">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="col-sm-4 control-label">Chủ trì:</label>
										<div class="col-sm-8">
											<input type="text" style="color: black; width: 100%;" name="nguoichutri" value=""  style="">
										</div>
									</div>
								</div>								
								
                                <!--<table class="table table-bordered ">
                                    <tbody>
                                    <tr class="info">
                                        <td><p class="col-sm-12"><b>Họp vào hồi:</b> <input style="color: black;" type="text" name="giohop1" class="text-center" value="{if isset($xml->STRTHOIGIANHOP)}{$xml->STRTHOIGIANHOP}{/if}">  <span class="text-info">Ngày:</span>  <input type="text" name="ngayhop1" class="datepic datemask" value="{if isset($xml->STRNGAYHOP)}{$xml->STRNGAYHOP}{/if}">    (Nhập giờ theo dạng hh:mm - Ví dụ: 8 giờ 30 -> 08:30)</p></td>
                                    </tr>
                                    <tr class="info">
                                        <td><p class="col-sm-6"><b>Địa điểm:</b>
                                            <input type="text" style="color: black;" name="diadiemhop1" style="width: 100%;" value="{if isset($xml->STRDIADIEM)}{$xml->STRDIADIEM}{/if}"> </p>
                                        <p class="col-sm-6"><b>Người chủ trì:</b>
                                            <input type="text" style="color: black;" name="nguoichutri" value=""  style="width: 68%;"></p></td>
                                    </tr>
                                    </tbody>
                                </table>-->
								
								
                            <div class="col-md-8 loaigiaymoi {if $xml->STRLOAIVANBAN!='Giấy mời'}hide{/if}">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung họp</label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunggiaymoi[]" class="form-control" rows="2" style="" placeholder="Nhập nội dung "></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Địa điểm họp</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="diadiemhop[]" class="form-control" value="" style="" placeholder="Nhập địa điểm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 loaigiaymoi {if $xml->STRLOAIVANBAN!='Giấy mời' }hide{/if}">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" required>Giờ họp</label>

                                    <div class="col-sm-8 bootstrap-timepicker">
                                        <input type="text" name="giohop[]" class="form-control" value="" id="" placeholder="Ví dụ: {date('h:i',time())}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày họp</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngayhop[]" class="form-control" value="" id="" placeholder="Ví dụ: {date('d/m/Y',time())}">
                                    </div>
                                </div>
                            </div>
                            <div class="themmoigiaymoi"></div>
                            <div class="col-md-12 loaigiaymoi text-right {if $xml->STRLOAIVANBAN!='Giấy mời'}hide{/if}">
                                <div class="col-sm-8">
                                    <button type="button" name="themmoigiaymoi" value="themmoigiaymoi" class="btn btn-success"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                </div>
                            </div>
                            <div class="col-sm-12 {if $xml->STRLOAIVANBAN!='Giấy mời'}hide{/if}">
                                <hr style="border: 1px dashed #3c8dbc">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký: </label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="7" style="color: black;" type="text" name="nguoiky" class="nguoiky form-control" value="{$xml->STRNGUOIKY}">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Ngày nhận:</label>
                                    <div class="col-sm-8">
                                        <input tabindex="9" style="color: black;" type="text" required name="ngaynhan" class="form-control datepic datemask ngayhan" value="{date('d/m/Y')}">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-info"></span>:</label>
                                    <div class="col-sm-8">
                                        <input tabindex="8" autocomplete="on" type="text" class="form-control" name="chucvu" value="{if isset($xml->STRCHUCDANH)}{$xml->STRCHUCDANH}{/if}" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết *:</label>

                                    <div class="col-sm-5">
										{if $xml->STRLOAIVANBAN=='Giấy mời'}
											<input type="text" name="" required class="form-control" value="Tự động lấy theo ngày họp" readonly>
										{/if}
                                        <input type="text" name="hangiaiquyet" required class="form-control datepic datemask {if $xml->STRLOAIVANBAN=='Giấy mời'}hide{/if}" value="" >
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="checkbox" name="iDeadline" value="1"> &nbsp;&nbsp;<b> VB có thời hạn </b>
										<!--<input type="checkbox" name="hanvb" value="1"> &nbsp;&nbsp;<b> Hạn VB </b>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="soden" value="{$soden}" class="form-control" style="color: red;font-size: 22px;font-weight: bold">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Độ mật:</label>
                                    <div class="col-sm-8">
                                        <select name="domat" id="" class="form-control select2" style="width:100%">
                                            {if !empty($domat)}
                                                {foreach $domat as $dm}
                                                    <option value="{$dm.PK_iMaDM}">{$dm.sTenDoMat}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="sotrang" class="form-control" value="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Độ khẩn:</label>
                                    <div class="col-sm-8">
                                        <select name="dokhan" id="" class="form-control select2" style="width:100%">
                                        {if !empty($dokhan)}
                                            {foreach $dokhan as $dk}
                                                <option value="{$dk.PK_iMaDK}">{$dk.sTenDoKhan}</option>
                                            {/foreach}
                                        {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Thời hạn theo QT ISO:</label>
                                    <div class="col-sm-6">
                                        <select name="quytrinh" class="form-control select2 ngayhan" style="width:100%" id="">
                                            {if !empty($quytrinh)}
                                                {foreach $quytrinh as $qt}
                                                    <option value="{$qt.iSoNgay}">{$qt.sTenQuyTrinh}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                    <!--<label for="" class="col-sm-4 control-label" style="font-size:15px">{if !empty($ktpdf)}{if !empty($pdf)}<a target="_blank" class="tin1" href="{$url}{$pdf}">Xem file đính kèm</a>{/if}{/if}</label>-->
									<input type="text" class="hide" name="mail_files" value="{$mail_files}">
									<input type="text" class="hide" name="mail_date" value="{$mail_date}">
                                    <input type="text" class="hide" name="duongdanfile" value="{if !empty($ktpdf)}{if !empty($pdf)}{$url}{$pdf}{/if}{/if}">
										{$tenfile}
                                </div>
                            </div>
							<div class="col-sm-12">
								<div class="form-group pull-right" >
								<b class="pull-right">Đính kèm</b><br>
								{$dsfiles = explode(';', $mail_files)}
												{foreach $dsfiles as $file}
													{$file = trim($file, ' ')}
													{if strpos($file, '.sdk') == false}
														<p style="margin-bottom:3px"><a href="emaildangFile_2020/{$file}" target="_blank" style="color:brown">{$file}</a></p>
													{/if}
												{/foreach}
								</div>
							</div>
                            <div class="col-md-12" style="text-align:center; margin-top:10px">
                                <button tabindex="10" class="btn btn-primary" type="submit" name="luulai" value="Lưu lại">Lưu lại</button><!--<a href="{$url}readmail?trangthai=0" class="btn btn-default">Quay lại >></a> <label for="">Người nhập văn bản này là: {!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['FK_iMaNguoiNhap'] : $vanban['sHoTen'] }</label>-->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
		$('input').css('font-size', '14px');
        $('textarea').css('font-size', '14px');
        $(document).on('change','select[name=loaivanban]',function(){
            var loaivanban =  $('select[name=loaivanban]').val();
           
            if(loaivanban=='Giấy mời')
            {
                $('.loaigiaymoi').removeClass('hide');
                $('.loaicongvan').addClass('hide');
            }
            else
            {
                $('.loaigiaymoi').addClass('hide');
                $('.loaicongvan').removeClass('hide');
            }
        });
        $('input.form-control').css('height','32px');
        $('.control-label').css('text-align', 'left');
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
        $(document).on('click','button[name=themmoigiaymoi]',function(){
            var noidunggiaymoi1 = $('.noidunggiaymoi')[0].outerHTML;
            var noidunggiaymoi2 = $('.noidunggiaymoi')[1].outerHTML;
            var noidunggm = noidunggiaymoi1+noidunggiaymoi2;
            $('.themmoigiaymoi').before(noidunggm);
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {foreach $loaivanban as $lvb}
                "{$lvb.sTenLVB}",
            {/foreach}
        ],
        "noiden": [
            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                "{$dv.sTenDV}",
            {/foreach}
        ],
        "linhvuc": [
            {foreach $linhvuc as $lv}
                "{$lv.sTenLV}",
            {/foreach}
        ],
        "nguoiky": [
            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                "{$nk.sHoTen}",
            {/foreach}
        ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".loaivanban",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".linhvuc",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            linhvuc: {
                data: data.linhvuc
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".noiguiden",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>