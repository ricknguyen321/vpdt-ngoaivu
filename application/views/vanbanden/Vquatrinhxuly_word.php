<style type="text/css">
    .border_main{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
    }
    .border_main1{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
    }
</style>
    <section class="content">
      <!-- Default box -->
        <div class="box" style="font-size: 18px">
            <div class="box-body" style="font-size: 18px">
                <div class="row" style="font-size: 18px">
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 35%;vertical-align: top; font-size: 18px">
                                    SỞ NGOẠI VỤ HÀ NỘI<BR>
                                    <span style="text-transform: uppercase;">
                                        <b>{$phonghd[0].sTenPB}</b>
										<hr style="width:30%; padding-top:-10px">
                                    </span>
                                </td>
                                <td colspan="13" style="text-align: center;width: 65%;font-size: 18px">
                                    <B>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM </B><BR>
                                    <b><span style="padding: 30px 0 30px 0">Độc lập - Tự do - Hạnh phúc</span></b>
									<hr style="width:30%; padding-top:-10px">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 100%;vertical-align: top; font-size: 18px; margin:5px">
                                    <span style="text-transform: uppercase;">
                                        <b>PHIẾU TRÌNH XỬ LÝ CÔNG VIỆC</b>
                                    </span>									
                                </td>
                            </tr>
							<tr style="margin-bottom:15px">
                                <td style="text-align: center; width: 40%;vertical-align: top; font-size: 18px" >
                                    <span style="">
                                        Kính gửi:
                                    </span>
									
                                </td>
								<td style="width: 60%;vertical-align: top; font-size: 18px" >
                                    <span style="">
                                        - Đ/c Giám đốc Sở <br>
										- Đ/c Phó Giám đốc Sở 
                                    </span>									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr  class="border_main">
                                <td style="width: 20%;vertical-align: top;font-size: 18px" class="border_main">
                                    <span style="">
                                        <b>Thông tin văn bản đến:</b>
                                    </span>									
                                </td>
								<td style="width: 80%;vertical-align: top;font-size: 18px"  class="border_main1">
                                    <span style="">
                                       <p>{$getdAppointment[0]['sMoTa']}</p>
									   - Số đến: {$getdAppointment[0]['iSoDen']} &nbsp;
									   - Ký hiệu: {$getdAppointment[0]['sKyHieu']} &nbsp;
									   - Nơi gửi: {$getdAppointment[0]['sTenDV']}<br>
									   - Ngày nhận: {date_select($getdAppointment[0]['sNgayNhan'])}  &nbsp; - Hạn xử lý: {date_select($getdAppointment[0]['sHanThongKe'])}
                                    </span>									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr class="border_main" style="height:40px;">
                                <td style="width: 33%;vertical-align: top;font-size: 18px" class="border_main">
                                    <span style="">
                                        Ngày nhận văn bản: ..../....
                                    </span>									
                                </td>
								<td style="width: 33%;vertical-align: top;font-size: 18px" class="border_main">
                                    <span style="">
                                       Ngày chuyển phòng chuyên môn: ..../....
                                    </span>									
                                </td>
								<td style="width: 33%;vertical-align: top;font-size: 18px" class="border_main1">
                                    <span style="">
                                       Ngày chuyên viên xử lý: ..../....
                                    </span>									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr  class="border_main">
                                <td style="width: 20%;vertical-align: top;font-size: 18px" class="border_main">
                                    <span style="">
                                        <b>Hồ sơ kèm theo:</b>
                                    </span>
                                </td>
								<td style="width: 80%;vertical-align: top;font-size: 18px" class="border_main1">
                                    <span style="">
                                       - Phiếu trình xử lý văn bản đến số {$getdAppointment[0]['iSoDen']}<br>
									   - <br>
									   - {$getdAppointment[0]['sTenLVB']} {$getdAppointment[0]['sKyHieu']} -  {limit_word($getdAppointment[0]['sMoTa'], 25)}
                                    </span>									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr class="border_main" style="height:80px;">
                                <td style="vertical-align: top; border-right: solid 1px;font-size: 18px" class="border_main">
                                    <span style="">
                                        <b>Báo cáo, đề xuất của {($vanban['FK_iMaPhongHD'] == 11) ? 'Văn phòng':'phòng chuyên môn'}:</b><br>
										-<br>
										-<br>
										-<br><br>
										Kính trình Lãnh đạo xem xét, phê duyệt./.
                                    </span>
                                </td>
								
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr  class="border_main">
                                <td style="width: 45%;vertical-align: top;text-align: center; font-size: 18px" class="border_main text-center">
                                    <span style="">
                                        <b>Cán bộ xử lý</b> ..../....<br>
										<br><br><br><br>
										{$vanban['sHoTen']}
                                    </span>
                                </td>
								<td style="width: 55%;vertical-align: top;text-align: center; font-size: 18px" class="border_main1 text-center">
                                    <span style="">
                                       <b>Ý kiến của Trưởng phòng</b> ..../....<br>
									   <br><br><br><br>
									   {if $vanban['FK_iMaPhongHD'] != 11}
											{$tp['sHoTen']}
										{/if}
                                    </span>									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				<div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%" class="table table-bordered table-striped">
							<tr class="border_main" style="height:80px;">
                                <td style="vertical-align: top; border-right: solid 1px; border-bottom: solid 1px;font-size: 18px" class="border_main">
                                    <span style="">
                                        <b>Ý kiến của Phó giám đốc:</b> ..../....
										<br><br><br><br><br><br>
										<b>Ý kiến của Giám đốc:</b> ..../....
										<br><br><br><br><br><br><br>
                                    </span>
                                </td>
								
                            </tr>
                        </table>
                    </div>
                </div>
				
                <br><br>
            </div>
        </div>
      <!-- /.box -->

    </section>
 


