<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách Giấy mời - <b>({$count}) </b>
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font {if $vanban['iQuyenHan_DHNB']==9 || $vanban['iQuyenHan_DHNB']==2} btn btn-primary btn-xs {/if}" {if $vanban['iQuyenHan_DHNB']==9 || $vanban['iQuyenHan_DHNB']==2} style="font-size: 12px!important" {/if} data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a> </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{$url}dsgiaymoi" target="_blank" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{($sokyhieu)?$sokyhieu:''}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{($ngaynhap > '2017-01-01')?(date_select($ngaynhap)):''}" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Năm</label>

                                                    <div class="col-sm-8">
                                                        <select name="nam" id="" class="form-control">
                                                            <option value="">--- Chọn năm văn bản ---</option>
                                                            <option value="2017" {!empty($nam)&&($nam == 2017)? 'selected':''}>2017</option>
                                                            <option value="{date('Y')}" {!empty($nam)&&($nam)?($nam == date('Y'))?'selected':'':''}>{date('Y')}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{($ngayky > '2017-01-01')?(date_select($ngayky)):''}" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayden" value="{($ngayden > '2017-01-01')?(date_select($ngayden)):''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class=" col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Trích yếu</label>
                                                        <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaymoi" class="form-control datepic datemask" id="" value="{($ngaymoi > '2017-01-01')?(date_select($ngaymoi)):''}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số đến</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="denngay" class="form-control datepic datemask" id="" value="{($denngay > '2017-01-01')?(date_select($denngay)):''}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Người nhập</label>

                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                    <div class="col-sm-12 deletepadding">
                        {$phantrang}
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="8%" class="text-center">Ngày nhận</th>
                                    <th width="8%" class="text-center">Số đến</th>
                                    <th width="" class="text-center">Trích yếu</th>
                                    <th width="15%" class="text-center">Phòng chủ trì</th>
									<th width="6%" class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i = 1}
                                {foreach $getDocGo as $vbd}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td>{date_select($vbd.sNgayNhap)}</td>
                                    <td style="color: red; font-size: 18px" class="text-center"><b>{$vbd.iSoDen}</b></td>
                                    <td>
                                       <p> <a href="{$url}quytrinhgiaymoi/{$vbd.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a></p>
									   
                                        - Thời gian: <b style="color:blue">{$vbd.sGiayMoiGio}</b> ngày <b style="color:blue">{date_select($vbd.sGiayMoiNgay)}</b>, tại <b style="color:blue">{$vbd.sGiayMoiDiaDiem}</b><br>
                                        <br>
                                        <p>- Ký hiệu: {$vbd.sKyHieu}<br>
										- Nơi gửi đến: {$vbd.sTenDV}</p>
										
										<span style="float:right"><a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}">Tài liệu <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a>{if $vbd.iFile==1} | <a target="_blank" href="{$vbd.sDuongDan}" class="tin1">{if $vbd.iMail == 1} Đính kèm{else} Đính kèm{/if}</a>{/if}</span>
									
									</td>
									
                                    <td class="text-center" style="padding-top: 20px">
                                        <b>{$vbd.sPhongChuTri}</b>
                                    </td>
									
									{if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 9 || $vanban['PK_iMaCB'] == 705 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731}
									<td class="text-center" >
										{if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['PK_iMaCB'] == 705 || empty($vbd.sPhongChuTri)}
										<button type="submit" value="{$vbd.PK_iMaVBDen}" name="xoavanban" onclick="return confirm('Bạn muốn xóa văn bản đến số {$vbd.iSoDen} ?')" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Xóa" style="margin-top: 20px"><i class="fa fa-trash"></i></button>
										{/if}
										<p><a href="{if $vbd.iGiayMoi == '1'}{$url}nhapgiaymoi/{$vbd.PK_iMaVBDen}{else}{$url}vanbanden/{$vbd.PK_iMaVBDen}{/if}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="sửa văn bản" style="margin-top: 20px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></p>
									</td>
									{/if}
								
                                </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        {$phantrang}
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    function myFunction() {
        window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
    }
</script>
