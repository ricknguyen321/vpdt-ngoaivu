<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        {$title}
                    </h3>
                </div>
                <div class="col-sm-9">
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <!-- Default box -->
        <div class="box">
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="" class="text-center">Ngày nhập</th>
                                <th class="text-center">Số đến</th>
                               
                                <th width="" class="text-center">Trích yếu - Thông tin</th>
                            </tr>
                            </thead>
                            <tbody>
                            {$i=1}
                            {foreach $getDocAwait as $pl}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td class="text-center">{date_select($pl.sNgayNhap)}</td>
                                <td class="text-center"><b style="color: red; font-size: 16px">{$pl.iSoDen}</b></td>
                                <td>
                                    <p><a style="color: black;" href="{$url}giaymoichuyenvienchutrixuly/{$pl.PK_iMaVBDen}"><b>{$pl.sMoTa}
                                    {if $pl.sMoTa == ''}Xem chi tiết{/if}
                                    </b></a></p>
									
                                    <p>Vào hồi {$pl.sGiayMoiGio} ngày {date_select($pl.sGiayMoiNgay)}, tại {$pl.sGiayMoiDiaDiem}</p>
									
									- Ký hiệu: {$pl.sKyHieu}<br>
									- Nơi gửi: {$pl.sTenDV}<br>
                                    <p>- Hạn giải quyết : <b>{date_select($pl.sGiayMoiNgay)} </b></p>
                                    <br>{if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>

<script>
    $(document).ready(function() {
        $(document).on('click','button[name=tctp]',function () {
            $('input[name=maphongbantp]').val($(this).val());
            $('button[name=tuchoitp]').text('Gửi Lãnh đạo ( '+$(this).attr("data-id")+' )');
            $('input[name=capdo]').val(parseInt($(this).attr("data-num")) - 1);
        });
    });
</script>

