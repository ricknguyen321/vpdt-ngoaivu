<script src="{$url}assets/js/plugins/getDataDoc.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Phân loại văn bản <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản </a></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
										{if $de.PK_iMaPB != 78}
											<input type="checkbox" name="chonphong" value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" class="Checkbox"> {$de.sTenPB}<br>
										{/if}
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-lg" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Thêm nội dung</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidunghop" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung ...." required></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập hạn nội dung</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="hannoidung" class="form-control datepic datemask" placeholder="Nhập hạn nếu có ....">
                                    </div>
                                </div>
                                <input type="text" name="khuvuc" value="" class="hidden">
								<input type="text" name="sokyhieu" value="" class="hidden">
								<input type="text" name="donvi" value="" class="hidden">
								<input type="text" name="ngayky" value="" class="hidden">
                                <input type="text" name="linhvuc" value="" class="hidden">
								<input type="text" name="trichyeu" value="" class="hidden">
								<input type="text" name="nguoiky" value="" class="hidden">
								<input type="text" name="ngaynhan" value="" class="hidden">
                                <input type="text" name="soden" value="" class="hidden">
								<input type="text" name="chucvu" value="" class="hidden">
								<input type="text" name="hangiaiquyet1" value="" class="hidden">
								<input type="text" name="sotrang" value="" class="hidden">
								<input type="text" name="domat" value="" class="hidden">
								<input type="text" name="dokhan" value="" class="hidden">
                                <input type="text" name="loaivanban" value="" class="hidden">
								<input type="text" name="duongdan" value="" class="hidden">
								<input type="text" name="tenfile" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="luunoidung" class="btn btn-primary btn-sm" value="Luu">Thêm nội dung</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="{$url}dsvanbanden" method="get" target="_blank" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-sm-12" style="text-align:center">
                                <span>{$phantrang}</span>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                                <button type="button" id="duyet" class="hidden">Duyet</button>
                            </div>
                        </div>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="50px" class="text-center">STT</th>
                                <th width="" class="text-center">Trích yếu - Thông tin</th>
                                <th width="17%" >Tác vụ</th>
                                <th width="25%" class="text-center">Tham mưu BGĐ</th>
                            </tr>
                            </thead>
                            <tbody>
                            {if $vanban['iQuyenHan_DHNB'] == 3}
                            {$j=1}
                            {foreach $getDocAwait as $pl}
                            <tr class="thongbao">
                                <td class="text-center">{$j++ + $pagei}</td>
                                <td>
									<p><a style="color: black;" href="{$url}quatrinhxuly/{$pl.PK_iMaVBDen}{if !empty($nam)}/?nam={$nam}{/if}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<b style="color: orange;font-weight: bold;">{$pl.sMoTa}</b>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<b style="color: red">{$pl.sMoTa}</b>{else}<b>{$pl.sMoTa}</b>{/if}</a></p>
									
									- Số đến: <b style="color: red; font-size: 20px">{$pl.iSoDen}</b> 
									- Ngày nhập: {date_time2($pl.sNgayNhap)} 
                                    - Số ký hiệu: {$pl.sKyHieu} <br>
                                    <p>- Nơi gửi: <span style="color: blue;">{$pl.sTenDV}</span><br>
									
									{if $pl.FK_iMaDM > 1}
										- Độ mật: {if $pl.FK_iMaDM == 2} <b style="color:red; font-size:16px">MẬT</b> {/if}
											{if $pl.FK_iMaDM == 3} <b style="color:red; font-size:16px">TỐI MẬT</b> {/if}
											{if $pl.FK_iMaDM == 4} <b style="color:red; font-size:16px">TUYỆT MẬT</b> {/if}
										
									{/if}
									{if $pl.FK_iMaDK > 1}
										- Độ khẩn: {if $pl.FK_iMaDK == 2} <b style="color:red; font-size:16px">KHẨN</b> {/if}
											{if $pl.FK_iMaDK == 3} <b style="color:red; font-size:16px">TỐI KHẨN</b> {/if}
											{if $pl.FK_iMaDK == 5} <b style="color:red; font-size:16px">HỎA TỐC</b> {/if}
										
									{/if}
										</p>			
													
                                    {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
									{if !empty($pl.PhongChuyenLai['sTenPB'])}- Phòng gửi lại: <i><b style="color:red">{$pl.PhongChuyenLai['sTenPB']}</b></i>{/if}
                                    {if !empty($pl.sLyDo)}<p><b class="text-danger">- Nội dung từ chối*:</b> <i>{$pl.sLyDo}</i></p>{/if}
                                    
									
									<p><span style="margin-right: 15px;"><input type="checkbox" name="iSTCChuTri_{$pl.PK_iMaVBDen}" value="1" {($pl.iSTCChuTri == 1) ? checked : NULL }><label for=""> &nbsp;Sở chủ trì </label> </span>
                                    <span style="margin-right: 15px;"><input type="checkbox" name="iVanBanTBKL_{$pl.PK_iMaVBDen}" value="1" {($pl.iVanBanTBKL == 1) ? checked : NULL }><label for=""> &nbsp;TBKL  </label> </span>
                                    <span style="margin-right: 15px;"><input type="checkbox" name="iToCongTac_{$pl.PK_iMaVBDen}" value="1" {($pl.iToCongTac == 1) ? checked : NULL }><label for=""> &nbsp;TTHC </label> </span>
                                    <span><input type="checkbox" name="iSTCPhoiHop_{$pl.PK_iMaVBDen}" value="1" {($pl.iSTCPhoiHop == 1) ? checked : NULL }><label for=""> &nbsp;VB Đảng </label> </span></p>

									 <span><input type="checkbox" name="vanbanquantrong[]" value="1"></span> <span style="color: red;">&nbsp;VB Quan trọng</span>
									<br>
									<span class="pull-right" style="text-align:right">
										{$dsfile=layDuLieu('FK_iMaVBDen',$pl.PK_iMaVBDen,'tbl_files_vbden')}
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a><br>
											{foreach $dsfile as $f}
											<p style="margin-top:3px;margin-bottom:3px">
												<a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></p>
											{/foreach}
                                        </span>
									<!--<P class="pull-right"><a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}</p>-->
                                </td>
                                <td class="ykien">
                                    <div style="margin-bottom: 10px" class="giamdoc1">
                                        <select name="giamdoc[]" id="" class="form-control">
                                            <option value="">Chọn Giám đốc</option>
                                            {foreach $getAccountDirec as $gd}
                                            <option value="{$gd.PK_iMaCB}" data-id="{$gd.sHoTen}">{$gd.sHoTen}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px" class="phogiamdoc1">
                                        <select name="phogiamdoc[]" id="" class="form-control">
                                            <option value="">Chọn PGĐ</option>
                                            {foreach $getAccountDeputyDirector as $pgd}
                                            <option value="{$pgd.PK_iMaCB}" data-id="{$pgd.sHoTen}" data-toggle="{$pl.sMoTa}">{$pgd.sHoTen}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px" class="phongchutri1">
                                        <select name="phongchutri[]" id="" class="form-control">
                                            <option value="">Chọn phòng chủ trì</option>
                                            {foreach $getDepartment as $de}
												{if $de.PK_iMaPB != 78}
													<option value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" data-num="{$de.PK_iMaCB}" data-collapse="{$pl.sMoTa}">{$de.sTenPB}</option>
												{/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px">
                                        <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" disabled>Chọn phối hợp</button>
                                    </div>

                                    <input type="text" name="mangphoihop[]" value="" id="{$pl.PK_iMaVBDen}" class="hidden">
                                    <input type="text" name="doc_id[]" value="{$pl.PK_iMaVBDen}" class="hidden">
									<input type="text" name="sKy_Hieu_{$pl.PK_iMaVBDen}" value="{$pl.sKy_Hieu}" class="hide">
                                    <input type="text" name="sNgay_Ky_{$pl.PK_iMaVBDen}" value="{$pl.sNgay_Ky}" class="hide">
                                    <input type="text" name="sMaCoQuan_{$pl.PK_iMaVBDen}" value="{$pl.sMaCoQuan}" class="hide">
                                    <input type="text" name="sTenCoQuan_{$pl.PK_iMaVBDen}" value="{$pl.sTenCoQuan}" class="hide">
                                    <div style="margin-bottom: 10px">
                                        <input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{($pl.sHanThongKe > '2017-01-01') ? date_select($pl.sHanThongKe) :(($pl.sHanGiaiQuyet > '2017-01-01') ? date_select($pl.sHanGiaiQuyet) : NULL) }" class="form-control datepic quytrinh datemask" placeholder="Hạn giải quyết">
                                    </div>
									
									
									<div class="form-group">
                                    <label for="" class="control-label">Thời hạn theo QT ISO:</label>

                                        <select name="quytrinh[]" id="" class="form-control">
										<option value="">Chọn thời hạn</option>
                                            {if !empty($dsquytrinh)}{$i=1}
                                                {foreach $dsquytrinh as $qt}
                                                    <option value="{$qt.iSoNgay}" data-id="{$qt.iSoNgay}" data-ma="{$pl.PK_iMaVBDen}" data-ngaynhan="{$pl.sNgayNhan}">{$qt.sTenQuyTrinh}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
									</div>
									
                                   

									<input type="checkbox" name="iDeadline_{$pl.PK_iMaVBDen}" value="1" {if $pl.iDeadline == 1} checked {/if} ><span style="{if $pl.iDeadline == 1}color:blue{/if}"><label> VB có thời hạn </label> </span>
									
                                </td>
                                <td class="chidao">
                                   <p> <textarea name="chidaogiamdoc[]" id="" class="form-control giamdoc" rows="2"></textarea><br>
                                    <textarea name="chidaophogiamdoc[]" id="" class="form-control phogiamdoc" rows="2"></textarea><br>
                                    <textarea name="chidaophongchutri[]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="4"></textarea></p>
									<a href="quatrinhxuly/{$pl.PK_iMaVBDen}?kx=word"><button type="button" name=""  value="" class="btn btn-primary btn-xs">Tải phiếu xử lý</button></a>
									<button type="button" name="themnoidung"  value="{$pl.PK_iMaVBDen}" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#myModal1" >Thêm nội dung</button>
                                </td>
                              
                            </tr>
                            {/foreach}
                            {/if}
                            </tbody>
                        </table>
						<div class="col-sm-12">
                                <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="" ><i class="fa fa-check"></i> Duyệt</button>
                            </div>
						<div class="col-sm-12" style="text-align:center">
                                <span>{$phantrang}</span>
                            </div>
                        <p id="idhmh" class="hidden">{$getAccountDirec[0]['PK_iMaCB']}</p>
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>

<script>
	
	var url = window.location.href;
	
	$(document).on('change','select[name="quytrinh[]"]',function () {
		var songay     = $(this).find(':selected').attr('data-id');
		var mavb     = $(this).find(':selected').attr('data-ma');
		var ngaynhan     = $(this).find(':selected').attr('data-ngaynhan');
		var suc = 0;
		$.ajax({
				url:url,
				type:'POST',
				data:{
					'action':'layNgayHetHan',
					'songay':songay,
					'ngaynhan':ngaynhan
				},
				success: function(repon)
				{				
					
					var result = JSON.parse(repon);
					$('input[name="hangiaiquyet['+mavb+']"]').val(result);
					//alert(result);
					
				},
				dataType: 'text'
		});	
	});
	
</script>

<script src="{$url}assets/js/plugins/vanbanchoxuly_cvp.js"></script>
<script src="{$url}assets/js/plugins/message.js"></script>
