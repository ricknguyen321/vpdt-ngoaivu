<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="font">
            Danh sách văn bản
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <a class="btn btn-primary btn-xs" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="    margin-bottom: 10px;">
                                <i class="fa fa-search"></i> Tìm kiếm văn bản
                            </a>
                            <div class="collapse" id="collapseExample">
                                <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="Nhập loại văn bản">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="Nhập số ký hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="Nhập nơi gủi đến">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="Nhập ngày ký">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" value="" name="" class="form-control" placeholder="ngày nhập">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" value="" name="" class="form-control" placeholder="đến ngày">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="" id="" class="form-control" rows="4" placeholder="Nhập trích yếu"></textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="Nhập người ký ">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="Nhập số đến">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="nhập chức vụ">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" value="" name="" class="form-control" placeholder="Người nhập">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button type="submit" class="btn btn-primary" name="" value=""><i class="fa fa-search"></i> Tìm kiếm</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="post">
                                <button type="submit" class="btn btn-success pull-right" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                                <br>
                                <table id="" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="5%" class="text-center">STT</th>
                                        <th width="" class="text-center">Ngày nhập</th>
                                        <th width="" class="text-center">Trích yếu - Thông tin</th>
                                        <th width="" class="text-center">Tệp tin</th>
                                        <th width="" >Ý kiến</th>
                                        <th width="" class="text-center">Chỉ đạo</th>
                                        <th width="" class="text-center">Duyệt</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                    {foreach $getDocAwait as $pl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td>{$pl.sNgayNhap}</td>
                                        <td>
                                            <a href="">{$pl.sMoTa}</a>
                                            <p>- Số ký hiệu: {$pl.sKyHieu} </p>
                                            <p>- Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> </p>
                                            <p>- Nơi gửi: <span style="color: blue;">{$pl.sTenDV}</span></p>
                                        </td>
                                        <td width="8%" class="text-center"><a href="{$url}teptin?ma={$pl.PK_iMaVBDen}"><h2 style="margin-top: 20px"><i class="fa fa-folder-open"></i></h2></a> Xem file</a></td>
                                        <td width="18%" class="ykien">
                                            <div style="margin-bottom: 10px">
                                                <select name="phogiamdoc[]" id="" class="form-control">
                                                    <option value="">Chuyển Phó phòng</option>
                                                    {foreach $getAccountDeputyDirector as $pgd}
                                                    <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$pgd.sHoTen}">{$pgd.sHoTen}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                            <div style="margin-bottom: 10px">
                                                <select name="phongchutri[]" id="" class="form-control">
                                                    <option value="">Chọn chuyên viên</option>
                                                    {foreach $getDepartment as $cv}
                                                    <option value="{$cv.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$cv.sHoTen}">{$cv.sHoTen}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                            <div style="margin-bottom: 10px">
                                                <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal" >Chọn phối hợp</button>
                                            </div>

                                            <input type="text" name="mangphoihop[]" value="" id="{$pl.PK_iMaVBDen}" class="hidden">
                                            <input type="text" name="doc_id[]" value="{$pl.PK_iMaVBDen}" class="hidden">
                                            <div style="margin-bottom: 10px">
                                                <input type="text" name="hangiaiquyet[]" value="{date_select($pl.sHanGiaiQuyet)}" class="form-control">
                                            </div>
                                        </td>
                                        <td width="35%" class="chidao">
                                            <textarea name="chidaophogiamdoc[]" id="{$pl.PK_iMaVBDen}" class="form-control phogiamdoc" rows="2"></textarea><br>
                                            <textarea name="chidaophongchutri[]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="4"></textarea>
                                        </td>
                                        <td class="text-center">
                                            <br><input type="checkbox" name="duyet[]" value="1"><br><br><br>
                                            <!--                                            <span style="color: red;"> VB Quan trọng:</span><br><input type="checkbox" name="vanbanquantrong" value="1">-->
                                        </td>
                                    </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly_tp.js"></script>
