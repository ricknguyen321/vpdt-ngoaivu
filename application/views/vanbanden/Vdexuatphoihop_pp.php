<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post" enctype="multipart/form-data">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="30%">Thông tin văn bản</th>
                                    <th width="30%">Yêu cầu đề xuất</th>
                                    <th width="30%">Ý kiến gửi đi</th>
                                    <th width="5%" class="text-center">Gửi</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td >
                                            <p>Số đến : <b>{$dl.iSoDen}</b></p>
                                            <p>Trích yếu: 
                                                <a href="{$url}chuyenvienchutrixuly?vbd={$dl.PK_iMaVBDen}" class="tin"> - {$dl.sMoTa}</a>
                                            </p>
                                            <p>Hạn văn bản:{(!empty($dl.sHanThongKe)&&($dl.sHanThongKe>'1970-01-01'))?date_select($dl.sHanThongKe):''}</p>
                                            <p>
                                                File văn bản: </a>{if !empty($dl.sDuongDan)}<a target="_blank" href="{$dl.sDuongDan}" id="{$dl.PK_iMaVBDen}" class="tin1 layma">Xem </a>{/if}
                                            </p>
                                        </td>
                                        <td >
                                            <p>Nội dung: <b>{$dl.sNoiDung}</b> {if !empty($dl.sFile_YeuCau)}<a target="_blank" href="{$url}{$dl.sFile_YeuCau}">Xem file</a>{/if}</p>
                                            <p>Người chuyển: {$dscanbo[$dl.FK_iMaNguoi_Gui]}</p>
                                            <p>Thời gian: {date_time($dl.sThoiGian_Chuyen)}</p>
                                        </td>
                                        <td class="text-center">
                                            <p class="hide">
                                                <input type="text" name="truongphong_{$dl.PK_iMaVBDen}" value="{$truongphong[0]['PK_iMaCB']}">
                                                <select name="truongphong" required="" style="width: 100%" id="" class="form-control select2">
                                                    {if !empty($truongphong)}
                                                        {foreach $truongphong as $tp}
                                                            <option value="{$tp.PK_iMaCB}" selected="">{$tp.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <input type="file" name="filetl_{$dl.PK_iMaVBDen}" readonly="" class="form-control">
                                            </p>
                                            <p>
                                                <textarea name="ykien_{$dl.PK_iMaVBDen}" id="" rows="7" placeholder="Nhập ý kiến tại đây" class="form-control">{$dl.sNoiDung}</textarea>
                                                <input type="text" class="hide" name="file_{$dl.PK_iMaVBDen}" value="{$dl.sFile_YeuCau}">
                                                <input type="text" class="hide" name="phongphoihop_{$dl.PK_iMaVBDen}" value="{$dl.FK_iMaPB_PH}">
                                            </p>
                                        </td>
                                        <td class="text-center">
                                            <p>
                                                <button name="guilen" value="{$dl.PK_iMaVBDen}" class="btn btn-primary btn-xs">Gửi lên</button>
                                            </p>
                                            <p>
                                                <button name="tralai" value="{$dl.PK_iMaVBDen}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>
                                            </p>
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>