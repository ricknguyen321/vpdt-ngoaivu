<script src="{$url}assets/js/plugins/getDataGM.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Phân loại giấy mời <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a> </label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" class="Checkbox"> {$de.sTenPB}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade bs-example-modal-lg" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Thêm nội dung</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidunghop" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung .... " required></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <br>
                                    <label for="" class="col-sm-4">Giờ họp</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="giohop" class="form-control" placeholder="Nhập giờ họp nếu có .... Ví dụ: 10:00">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <br>
                                    <label for="" class="col-sm-4">Ngày họp</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="ngayhop" class="form-control datepic mask" placeholder="Ví dụ: {date('d/m/Y',time())}">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <label for="" class="col-sm-2">Địa điểm</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="diadiemhop" class="form-control" placeholder="Nhập địa điểm nếu có .....">
                                    </div>
                                </div>
                                <input type="text" name="khuvuc" value="" class="hidden"><input type="text" name="sokyhieu" value="" class="hidden"><input type="text" name="donvi" value="" class="hidden"><input type="text" name="ngayky" value="" class="hidden">
                                <input type="text" name="linhvuc" value="" class="hidden"><input type="text" name="trichyeu" value="" class="hidden"><input type="text" name="nguoiky" value="" class="hidden"><input type="text" name="ngaynhan" value="" class="hidden">
                                <input type="text" name="soden" value="" class="hidden"><input type="text" name="chucvu" value="" class="hidden"><input type="text" name="hangiaiquyet1" value="" class="hidden"><input type="text" name="sotrang" value="" class="hidden">
                                <input type="text" name="loaivanban" value="" class="hidden"> <input type="text" name="giohopcu" value="" class="hidden"><input type="text" name="ngayhopcu" value="" class="hidden"><input type="text" name="diachihopcu" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="luunoidung" class="btn btn-primary btn-sm" value="Luu">Thêm nội dung</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="{$url}dsvanbanden" method="get" target="_blank" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="4" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                <!--<div class="form-group">
                                                    <label for="" class="col-sm-4">Người ký:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>-->

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <!--<div class="form-group">
                                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>-->

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span>{$phantrang}</span>
                                </div>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                                </div>
                            </div>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Trích yếu - Thông tin</th>
                                    <th width="15%" >Ý kiến</th>
                                    <th width="25%" class="text-center">Chỉ đạo</th>
                                    <th width="10%" class="text-center">Duyệt</th>
                                </tr>
                                </thead>
                                <tbody>
                                {if $vanban['iQuyenHan_DHNB'] == 3}
                                {$i=1}
                                {if !empty($getDocAwait)}
                                {foreach $getDocAwait as $pl}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td>{date_select($pl.sNgayNhap)}</td>
                                    <td>
                                        <p><b><a style="color: black;" href="{$url}quytrinhgiaymoi/{$pl.PK_iMaVBDen}">{$pl.sMoTa}</a></b></p>
                                        <p>- Thời gian: <b style="color:blue">{$pl.sGiayMoiGio}</b> ngày <b style="color:blue">{date_select($pl.sGiayMoiNgay)}</b>, tại <b style="color:blue">{$pl.sGiayMoiDiaDiem}</b></p>

                                        <span>- Số ký hiệu: {$pl.sKyHieu} </span><br>
                                        <span>- Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> </span><br>
                                        <span>- Nơi gửi: <span style="color: blue;">{$pl.sTenDV}</span></span><br>
                                        <br>
                                        {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
                                        {if !empty($pl.sLyDoTuChoiVanBanDen)}
                                        <br>
                                        <span style="color: red">Lý do từ chối: {$pl.sLyDoTuChoiVanBanDen}</span>
                                        {/if}
                                    </td>
                                    <td >
                                        <div style="margin-bottom: 10px">
                                            <select name="giamdoc[]" id="" class="form-control">
                                                <option value="">Chuyển Giám đốc</option>
                                                {foreach $getAccountDirec as $gd}
                                                <option value="{$gd.PK_iMaCB}" data-id="{$gd.sHoTen}">{$gd.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <select name="phogiamdoc[]" id="" class="form-control">
                                                <option value="">Chuyển P.Giám đốc</option>
                                                {foreach $getAccountDeputyDirector as $pgd}
                                                <option value="{$pgd.PK_iMaCB}" data-id="{$pgd.sHoTen}">{$pgd.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <select name="phongchutri[]" id="" class="form-control">
                                                <option value="">Chọn phòng chủ trì</option>
                                                {foreach $getDepartment as $de}
                                                <option value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}">{$de.sTenPB}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" disabled>Chọn phối hợp</button>
                                        </div>

                                        <input type="text" name="mangphoihop[]" value="" id="{$pl.PK_iMaVBDen}" class="hidden">
                                        <input type="text" name="doc_id[]" value="{$pl.PK_iMaVBDen}" class="hidden">
                                        <div style="margin-bottom: 10px">
                                            <input type="text" name="hangiaiquyet[]" value="{($pl.sHanGiaiQuyet > '2017-01-01') ? date_select($pl.sHanGiaiQuyet) :NULL }" class="form-control datepic" placeholder="Hạn giải quyết">
                                        </div>
										
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" target="_blank">Quản lý file <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}
									</td>
                                    <td class="chidao">
                                        <textarea name="chidaogiamdoc[]" id="" class="form-control giamdoc" rows="2"></textarea><br>
                                        <textarea name="chidaophogiamdoc[]" id="" class="form-control phogiamdoc" rows="2"></textarea><br>
                                        <textarea name="chidaophongchutri[]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="4"></textarea>
                                    </td>
                                    <td width="5%">
                                        <p><span style="color: red;"><input type="checkbox" name="giamdocduhop[]" value="1"> GĐ dự họp</span></p>
										<p><span style="color: red;"><input type="checkbox" name="pgdduhop[]" value="1"> PGĐ dự họp</span></p>
                                        <p><span style="color: red;"><input type="checkbox" name="phongduhop[]" value="1"> Phòng dự họp</span></p>
										
                                        <button type="button" name="themnoidung"  value="{$pl.PK_iMaVBDen}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal1" >Thêm nội dung</button>
                                    </td>
                                </tr>
                                {/foreach}
                                {/if}
                                {/if}
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly_cvp.js"></script>
