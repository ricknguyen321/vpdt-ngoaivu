<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Văn bản chờ Giám đốc xử lý <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a> </label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
									{if $de.PK_iMaPB != 78} <!-- skip Ban GĐ -->
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" class="Checkbox"> {$de.sTenPB}<br>
									{/if}
										
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                              

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade bs-example-modal-lg" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><b>Thêm nội dung</b></h4>
                        </div>
                        <form action="" method="post">
                            <div class="modal-body">
                                <div class="row" >
                                    <div class="col-sm-12">
                                        <label for="" class="col-sm-12">Nhập nội dung</label>
                                        <div class="col-sm-12">
                                            <textarea name="noidunghop" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung ...." required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="" class="col-sm-12">Nhập hạn nội dung</label>
                                        <div class="col-sm-12">
                                            <input type="text" name="hannoidung" class="form-control datepic datemask" placeholder="Nhập hạn nếu có ....">
                                        </div>
                                    </div>
                                    <input type="text" name="khuvuc" value="" class="hidden"><input type="text" name="sokyhieu" value="" class="hidden"><input type="text" name="donvi" value="" class="hidden"><input type="text" name="ngayky" value="" class="hidden">
                                    <input type="text" name="linhvuc" value="" class="hidden"><input type="text" name="trichyeu" value="" class="hidden"><input type="text" name="nguoiky" value="" class="hidden"><input type="text" name="ngaynhan" value="" class="hidden">
                                    <input type="text" name="soden" value="" class="hidden"><input type="text" name="chucvu" value="" class="hidden"><input type="text" name="hangiaiquyet1" value="" class="hidden"><input type="text" name="sotrang" value="" class="hidden">
                                    <input type="text" name="loaivanban" value="" class="hidden">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="luunoidung" class="btn btn-primary btn-sm" value="Luu">Thêm nội dung</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <div class="" style="text-align:center">{$phantrang}</div>
                        <button type="button" id="duyet" class="hidden">Duyet</button>
                        <br>
						<div class="col-sm-12 deletepadding" style="margin-bottom:5px">
							<div class="col-sm-2">
								<p style="font-size: 15px"><b>Màu Trích yếu:</b></p>
							</div>
							<div class="col-sm-2">
								<span><b style="color: red"><i class="fa fa-circle" aria-hidden="true"></i></b>: Quá hạn</span>
							</div>
							<div class="col-sm-2">
							<span><b style="color: orange"><i class="fa fa-circle" aria-hidden="true"></i></b>: Sắp đến hạn</span>
							</div>
							<div class="col-sm-2">
							<span><b style="color: black"><i class="fa fa-circle" aria-hidden="true"></i></b>: Trong hạn</span>
							</div>
							<div class="col-sm-4" style="text-align:right">
							<span class="pull-right"><button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="" ><i class="fa fa-check"></i> Duyệt</button></span>
							</div>
						</div>
						
						
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="50px" class="text-center">STT</th>
                                <th width="" class="text-center">Trích yếu - Thông tin</th>
                                <th width="15%" >Tác vụ</th>
                                <th width="25%" class="text-center">Chỉ đạo</th>
                                <th width="10%" class="text-center"><input type="checkbox" id="select_all"> Chọn hết</th>
                            </tr>
                            </thead>
                            <tbody>
                            {if $vanban['iQuyenHan_DHNB'] == 4}
							{$gd = layTTGD()}
                            {$j=1}
                            {foreach $getDocAwait as $pl}
                            <tr class="thongbao">
                                <td class="text-center">{$j++ +$pagei}</td>
                                <td>
                                    <p><b><a style="color: black;" href="{$url}quatrinhxuly/{$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
                                    - Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> 
									- Ngày nhập: {date_time2($pl.sNgayNhap)} 
                                    - Số ký hiệu: {$pl.sKyHieu} <br>
                                   <p> - Nơi gửi: <span style="color: black;">{$pl.sTenDV}</span><br>
									{if $pl.FK_iMaDM > 1}
										- Độ mật: {if $pl.FK_iMaDM == 2} <b style="color:red; font-size:16px">Mật</b> {/if}
											{if $pl.FK_iMaDM == 3} <b style="color:red; font-size:16px">Tối mật</b> {/if}
											{if $pl.FK_iMaDM == 4} <b style="color:red; font-size:16px">Tuyệt mật</b> {/if}
										
									{/if}
									{if $pl.FK_iMaDK > 1}
										- Độ mật: {if $pl.FK_iMaDK == 2} <b style="color:red; font-size:16px">Khẩn</b> {/if}
											{if $pl.FK_iMaDK == 3} <b style="color:red; font-size:16px">Tối khẩn</b> {/if}
											{if $pl.FK_iMaDK == 5} <b style="color:red; font-size:16px">Hỏa tốc</b> {/if}
										
									{/if}
									</p>
													
                                    {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
									
									<p><span style="margin-right: 15px;"><input type="checkbox" name="iSTCChuTri_{$pl.PK_iMaVBDen}" id="iSTCChuTri_{$pl.PK_iMaVBDen}" value="1" {($pl.iSTCChuTri == 1) ? checked : NULL }><label for="iSTCChuTri_{$pl.PK_iMaVBDen}"> &nbsp; Sở chủ trì </label> </span>
                                    <span style="margin-right: 15px;"><input type="checkbox" name="iVanBanTBKL_{$pl.PK_iMaVBDen}" id="iVanBanTBKL_{$pl.PK_iMaVBDen}" value="1" {($pl.iVanBanTBKL == 1) ? checked : NULL }><label for="iVanBanTBKL_{$pl.PK_iMaVBDen}"> &nbsp; TBKL  </label> </span>
                                    <span style="margin-right: 15px;"><input type="checkbox" name="iToCongTac_{$pl.PK_iMaVBDen}" id="iToCongTac_{$pl.PK_iMaVBDen}" value="1" {($pl.iToCongTac == 1) ? checked : NULL }><label for="iToCongTac_{$pl.PK_iMaVBDen}"> &nbsp; TTHC </label> </span>
                                    <span><input type="checkbox" name="iSTCPhoiHop_{$pl.PK_iMaVBDen}" id="iSTCPhoiHop_{$pl.PK_iMaVBDen}" value="1" {($pl.iSTCPhoiHop == 1) ? checked : NULL }><label for="iSTCPhoiHop_{$pl.PK_iMaVBDen}"> &nbsp; VB Đảng </label> </span></p>
									
                                    <span><input type="checkbox" name="vanbanquantrong[{$pl.PK_iMaVBDen}]" value="1" id="quantrong{$pl.PK_iMaVBDen}"></span> <span style="color: red;"> <label for="quantrong{$pl.PK_iMaVBDen}"> &nbsp; VB Quan trọng</label></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="iDeadline_{$pl.PK_iMaVBDen}" value="1" {if $pl.iDeadline == 1} checked {/if} id="thoihan{$pl.PK_iMaVBDen}"><span style="{if $pl.iDeadline == 1}color:blue{/if}"><label for="thoihan{$pl.PK_iMaVBDen}"> &nbsp; VB có thời hạn </label> </span>                                
									
									<br>
									<span style="text-align:right">
										{$dsfile=layDuLieu('FK_iMaVBDen',$pl.PK_iMaVBDen,'tbl_files_vbden')}
										<a class="tin1 pull-right" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B class="">Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a><br>
											{foreach $dsfile as $f}
												<p style="margin-bottom:3px">
													<a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a>
												</p>
											{/foreach}
                                        </span>
										
									<!--<p class="pull-right"><a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}</p>-->
									 
                                </td>
                                <td class="ykien">
                                    <div style="margin-bottom: 10px" class="phogiamdoc">
                                        <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc1">
											
                                            <option quyen="{$gd[0].iQuyenHan_DHNB}" value="{$gd[0].PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$gd[0].sHoTen}" data-toggle="{$pl.sMoTa}" selected>{$gd[0].sHoTen}</option>
                                            {foreach $getAccountDeputyDirector as $pgd}
                                            <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$pgd.sHoTen}" data-toggle="{$pl.sMoTa}" {!empty($pl.PK_iMaCBNhan[5])&&($pl.PK_iMaCBNhan[5] == $pgd.PK_iMaCB) ? selected : NULL}>{$pgd.sHoTen}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px">
                                        <select name="phongchutri[{$pl.PK_iMaVBDen}]" id="" class="form-control phongchutri1">
                                            <option value="">Chọn phòng chủ trì</option>
                                            {foreach $getDepartment as $de}
												{if $de.PK_iMaPB != 78}
													<option value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" data-num="{$de.PK_iMaCB}" data-collapse="{$pl.sMoTa}" {!empty($pl.PK_iMaPhongCT[6])&&($pl.PK_iMaPhongCT[6] == $de.PK_iMaPB) ? selected : NULL}>{$de.sTenPB}</option>
												{/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px">
                                        <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">Chọn phòng phối hợp</button>
                                    </div>

                                    <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="{($pl.PK_iMaPhongPH) ? $pl.PK_iMaPhongPH[7] : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden mangphoihop1">
                                    <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden doc_id1">
                                    <div style="margin-bottom: 10px">
                                        <input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{($pl.sHanThongKe > '2017-01-01') ? date_select($pl.sHanThongKe) : (($pl.sHanGiaiQuyet > '2017-01-01') ? date_select($pl.sHanGiaiQuyet) : NULL)}" class="form-control datepic quytrinh" placeholder="Hạn giải quyết">
                                    </div>
									
									<div class="form-group">
                                    <label for="" class="control-label">Thời hạn theo QT ISO:</label>

                                        <select name="quytrinh[]" id="" class="form-control">
										<option value="">Chọn thời hạn</option>
                                            {if !empty($dsquytrinh)}{$i=1}												
                                                {foreach $dsquytrinh as $qt}												
                                                    <option value="{$qt.iSoNgay}" data-id="{$qt.iSoNgay}" data-ma="{$pl.PK_iMaVBDen}" data-ngaynhan="{$pl.sNgayNhan}">{$qt.sTenQuyTrinh}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
									</div>
									   
                                </td>
                                <td class="chidao">
                                    <!--<textarea name="chidaogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}gd" class="form-control giamdoc" rows="2">{!empty($pl.ChuThich[4])&&($pl.ChuThich) ? $pl.ChuThich[4] : NULL}</textarea><br>-->
                                    <textarea name="chidaophogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}" class="form-control phogiamdoc" rows="2">{if !empty($pl.ChuThich[5])&&($pl.ChuThich)} {$pl.ChuThich[5]} {else}Giám đốc {$gd[0].sHoTen} trực tiếp chỉ đạo./.{/if}</textarea><br>
                                    <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="6">{!empty($pl.ChuThich[6])&&($pl.ChuThich) ? $pl.ChuThich[6] : NULL}</textarea>
									<br>{if !empty($pl.sLyDo)}<p><b class="text-danger">Nội dung từ chối*:</b> <i>{$pl.sLyDo}</i></p>{/if}
                                    {if !empty($pl.PhongChuyenLai['sTenPB'])}(<b>Phòng gửi lại: </b><i>{$pl.PhongChuyenLai['sTenPB']}</i>){/if}
								</td>
                                <td >
                                    <p><input type="checkbox" name="duyet[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="duyet"><span style="color: red;"> Chọn duyệt</span></p>
										{$pgdnhan = 0}
										{foreach $getAccountDeputyDirector as $pgd}
											{if !empty($pl.PK_iMaCBNhan[5])&&($pl.PK_iMaCBNhan[5] == $pgd.PK_iMaCB)}{$pgdnhan = 1}{/if}
                                        {/foreach}
                                    <p><input type="checkbox" name="pgdthammmuu[{$pl.PK_iMaVBDen}]" value="1" id="dondoc" checked ><span style="color: rgb(85, 85, 85);"> Đến thẳng phòng</span></p>
                                    
                                </td>
                            </tr>
							
                            {/foreach}
                            {/if}
							
                            </tbody>
                        </table>
						<button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="" ><i class="fa fa-check"></i> Duyệt</button>
						<div class="" style="text-align:center">{$phantrang}</div>
                        
                        
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly.js"></script>
<script>
    $(document).on('click','button[name=duyetvanban]',function(){
        $('#duyet').click();
    });
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector("#duyet").addEventListener("click", function(e){
            $(".thongbao").each(function () {
                var phogiamdoc = $(this).find('.ykien').find('.phogiamdoc :selected').val();
                var tieudepgd = $(this).find('.ykien').find('.phogiamdoc :selected').attr('data-toggle');
                var phongchutri = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-num');
                var tieude = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-collapse');
                var checkDoc = $(this).find('.duyet').is(':checked');
				$(this).find('.duyet').attr("checked", "checked")
                if (checkDoc == true) {
                    if (phogiamdoc != "") {
                        var notification1 = {
                            'is_notification': false,
                            'id_cb': phogiamdoc,
                            'base_url': 'vanbanchoxuly_pgd',
                            'notification': tieudepgd
                        };
                        notificationsRef.push(notification1);
                    }
                    if (phogiamdoc == "") {
                        if (phongchutri != undefined) {
                            var notification2 = {
                                'is_notification': false,
                                'id_cb': phongchutri,
                                'base_url': 'vanbanchoxuly_tp',
                                'notification': tieude
                            };
                            notificationsRef.push(notification2);
                        }
                    }
                }
            });
        });
    });
	
	
	var url = window.location.href;
	
	$(document).on('change','select[name="quytrinh[]"]',function () {
		var songay     = $(this).find(':selected').attr('data-id');
		var mavb     = $(this).find(':selected').attr('data-ma');
		var ngaynhan     = $(this).find(':selected').attr('data-ngaynhan');
		var suc = 0;
		$.ajax({
				url:url,
				type:'POST',
				data:{
					'action':'layNgayHetHan',
					'songay':songay,
					'ngaynhan':ngaynhan
				},
				success: function(repon)
				{				
					
					var result = JSON.parse(repon);
					$('input[name="hangiaiquyet['+mavb+']"]').val(result);
					//alert(result);
					
				},
				dataType: 'text'
		});	
	});
	
</script>
