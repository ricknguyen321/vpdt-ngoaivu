<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Thông tin văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <form class="form-horizontal">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Khu vực:</label>
                                    <div class="col-md-8">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Số ký hiệu:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?$thongtin[0]['sKyHieu']:''}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Loại văn bản:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?$thongtin[0]['sTenLVB']:''}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Nơi gửi đến:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?$thongtin[0]['sTenDV']:''}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Ngày ký:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?date_select($thongtin[0]['sNgayKy']):''}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Lĩnh vực:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?$thongtin[0]['sTenLV']:''}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Trích yếu</label>
                                    <div>{($thongtin)?$thongtin[0]['sMoTa']:''}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Người ký:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?$thongtin[0]['sTenNguoiKy']:''}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Ngày nhận:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?date_select($thongtin[0]['sNgayNhan']):''}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Chức vụ:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?$thongtin[0]['sChucVu']:''}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4">Hạn giải quyết:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?date_select($thongtin[0]['sHanGiaiQuyet']):''}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Số văn bản:</label>
                                    <div class="col-md-8">
                                        {($thongtin)?$thongtin[0]['iSoDen']:''}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Độ mật:</label>
                                    <div class="col-md-8">
                                        Bình thường
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Độ khẩn:</label>
                                    <div class="col-md-8">
                                        Bình thường
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Người chỉ đạo:</label>
                                    <div class="col-md-8">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4">Ngày phân loại:</label>
                                    <div class="col-md-8">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Trình tự giải quyết:</label>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th class="text-center" width="5%">STT</th>
                                                <th class="text-center" width="10%">Ngày nhập</th>
                                                <th class="text-center" width="10%">Chuyển từ</th>
                                                <th class="text-center" width="30%">Nội dung</th>
                                                <th class="text-center" width="10%">Chuyển đến</th>
                                                <th class="text-center" width="10%">Hạn xử lý</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {if !empty($process)}
                                                {$i=1}
                                                {foreach $process as $pr}
                                                <tr class="success">
                                                    <td class="text-center">{$i++}</td>
                                                    <td class="text-center">{date_time($pr.sThoiGian)}</td>
                                                    <td class="text-center">
                                                        {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                        {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                                                        {$cb.sHoTen}
                                                        {/if}
                                                        {/foreach}
                                                    </td>
                                                    <td>{$pr.sMoTa}</td>
                                                    <td class="text-center">{$pr.sHoTen}</td>
                                                    <td class="text-center">-</td>
                                                </tr>
                                                {/foreach}
                                            {/if}
                                        </tbody>
                                    </table>
                                    <label for="">Kết quả giải quyết:</label>
                                    <div class="panel panel-info">
                                        <div class="panel-body">- Văn bản trả lời: {if !empty($filesdi)}<a href="{$filesdi['sDuongDan']}">{$filesdi['sTenFile']}</a>{/if}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Tệp tin văn bản đến:</label>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th class="text-center" width="10%">STT</th>
                                                <th class="text-center" width="50%">Tên tệp tin</th>
                                                <th class="text-center" width="20%">Tải về</th>
                                                <th class="text-center" width="20%">Ngày nhập</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {if !empty($files)}{$i=1}
                                        {foreach $files as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td >{($f.sTenFile)?$f.sTenFile:'Chưa đặt tên'}</td>
                                                <td class="text-center"><a href="{$f.sDuongDan}" download>[Tải tài liệu]</a></td>
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
                                            </tr>
                                        {/foreach}
                                        {/if}
                                        </tbody>
                                    </table><br>
                                    <a href="javascript: history.go(-1)" id="backLink" class="btn btn-default">Quay lại danh sách >></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>