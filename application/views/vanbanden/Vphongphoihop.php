<style>
    .btn-success:hover {
        color: #fff !important;
        background-color: #449d44;
        border-color: #398439;
    }
    .btn-success:hover, .btn-success:active, .btn-success.hover {
        background-color: #008d4c;
        color: #FFF !important;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Phòng phối hợp chờ xử lý <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn chuyên viên phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade phophoihop" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phó phòng phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_B" method="POST">
                                {foreach $getAccountDeputyDirector as $pp}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$pp.PK_iMaCB}" data-id="{$pp.sHoTen}" class="Checkbox"> {$pp.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphophongban" value="" class="hidden" >
                                <input type="text" name="phochutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade phophoihop" id="myModal10" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn trưởng phòng phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="BHN" method="POST">
                                {foreach $getAccountDeputyDirector1 as $pp}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong1" value="{$pp.PK_iMaCB}" data-num="{$pp.sHoTen}" class="Checkbox"> {$pp.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphophongban1" value="" class="hidden" >
                                <input type="text" name="phochutri2" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai1" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!--- từ chối -->
        <div class="modal fade" id="myModal3" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nội dung từ chối...." required></textarea>
                                    </div>
                                </div>
                                <input type="text" name="maphongban" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Chuyển chánh van phong">Từ chối</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4"> Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                              
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <div class="col-sm-10 no-padding" style=" margin-top: -25px; margin-bottom: -20px;">
                                {$phantrang}
                            </div>
                            <div class="col-sm-2 no-padding" style=" margin-top: 0px; margin-bottom: 15px;float:right;">
                                {if $vanban['PK_iMaCB'] == 655}
                                <button type="submit" class="btn btn-default1 btn-xs pull-center" name="duyetnhieu" value="duyetnhieu"  ><i class="fa fa-check"></i> Duyệt Nhiều Văn Bản</button>
                                {/if}
                            </div>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="" class="text-center">Ngày nhập</th>
                                    <th width="30%" class="text-center">Trích yếu - Thông tin</th>
                                    <th width="" >Ý kiến</th>
                                    <th width="" class="text-center">Chỉ đạo</th>
                                    {if $vanban['iQuyenHan_DHNB'] == 7}
                                    <th width="" class="text-center">Trình tự xử lý</th>
                                    {/if}
                                    <th width="" class="text-center">Duyệt</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i=1}
                                {if !empty($getDocAwaitPPH)}
                                {foreach $getDocAwaitPPH as $pl}
                                {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11 }
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td>{date_select($pl.sNgayNhap)}</td>
                                    <td>
                                        <p><b><a href="{$url}chuyenvienxuly?vbd={$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span style="color: black">{$pl.sMoTa}</span>{/if}</a></b></p>
                                        <input type="text" name="noidungvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sMoTa}">
                                        <input type="text" name="sohieu[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sKyHieu}">
                                        <input type="text" name="ngaynhapvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sNgayNhap}">
                                        <br><p>- Số ký hiệu: {$pl.sKyHieu} </p>
                                        <p>- Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> </p>
                                        <p>- Nơi gửi: <span style="color: blue;">{$pl.sTenDV}</span></p><br>
                                        {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
                                    </td>
                                    <td width="18%" class="ykien">
                                        {if $vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] != 15 || $vanban['iQuyenHan_DHNB'] == 11 || $vanban['iQuyenHan_DHNB'] == 3}
                                        <div style="margin-bottom: 10px">
                                            <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc1 phophong">
                                                <option value="">Chuyển Phó phòng chủ trì</option>
                                                {foreach $getAccountDeputyDirector as $pgd}
                                                <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$pgd.sHoTen}" data-fruit="{$pl.sGiayMoiGio}" data-leaves="{$pl.sGiayMoiNgay}">{$pgd.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphophoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal1" style=""><b>Chọn PP phối hợp</b></button>
                                        </div>
                                        {else}
                                        <div style="margin-bottom: 10px">
                                            <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc2">
                                                <option value="">Chuyển Trưởng phòng chủ trì</option>
                                                {foreach $getAccountDeputyDirector1 as $pgd}
                                                <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$pgd.sHoTen}" data-fruit="{$pl.sGiayMoiGio}" data-leaves="{$pl.sGiayMoiNgay}">{$pgd.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphophoihop_cc" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal10" style="color: #cb0104;padding-right: 12px;"><b>Chọn trưởng phòng phối hợp</b></button>
                                        </div>
                                        {/if}
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" ><b>Chọn CV phối hợp</b></button>
                                        </div>
                                        <input type="text" name="mangphoihoppp[{$pl.PK_iMaVBDen}]" value="" id="{$pl.PK_iMaVBDen}-1" class="hidden">
                                        <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="" id="{$pl.PK_iMaVBDen}" class="hidden">
                                        <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden">
                                        <input type="text" name="id_phoihop[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaPhongPH}" class="hidden">
                                        <div style="margin-bottom: 10px">
                                            <input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{($pl.sHanThongKe > '2017-01-01') ? date_select($pl.sHanThongKe) : NULL}" class="form-control datepic" placeholder="Hạn giải quyết">
                                        </div>
										
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" target="_blank"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}
										
                                    </td>
                                    <td width="35%" class="chidao">
                                        <textarea name="chidaophogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}-1" class="form-control phogiamdoc" rows="2"></textarea><br>
                                        <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="4"></textarea>
                                    </td>
                                    <td class="text-center">
                                        <p><button tabindex="10" type="button" class="btn btn-success btn-xs hidden" name="luuao" disabled><i class="fa fa-check"></i> Duyệt</button><button type="submit" class="btn btn-success btn-xs pull-center duyet_ph" name="duyet" value="{$pl.PK_iMaVBDen}" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button></p>
										 <button type="button" name="abc"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal3" >Từ chối</button>
                                    </td>
                                </tr>
                                {else}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td>{date_select($pl.sNgayNhap)}</td>
                                    <td>
                                        </p><b><a href="{$url}chuyenvienxuly?vbd={$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
                                        <input type="text" name="noidungvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sMoTa}">
                                        <input type="text" name="sohieu[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sKyHieu}">
                                        <input type="text" name="ngaynhapvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sNgayNhap}">
                                        <p>- Số ký hiệu: {$pl.sKyHieu} </p>
                                        <p>- Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> </p>
                                        <p>- Nơi gửi: <span style="color: blue;">{$pl.sTenDV}</span></p><br>
                                        {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
                                    </td>
                                    <td width="18%" class="ykien">
                                        <div style="margin-bottom: 10px">
                                            <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc1 phophong" readonly="readonly">
                                                <option value="">Chuyển Phó phòng chủ trì</option>
                                                {foreach $getAccountDeputyDirector as $pgd}
                                                <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$pgd.sHoTen}" data-fruit="{$pl.sGiayMoiGio}" data-leaves="{$pl.sGiayMoiNgay}" {if $pgd.PK_iMaCB == $vanban['PK_iMaCB']}selected{/if}>{$pgd.sHoTen}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 10px">
                                            <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" >Chọn chuyên viên phối hợp</button>
                                        </div>
                                        <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCB)&&($pl.PK_iMaCB)? $pl.PK_iMaCB : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden">
                                        <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden">
                                        <input type="text" name="id_phoihop[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaPhongPH}" class="hidden">
                                        <div style="margin-bottom: 10px">
                                            <input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{($pl.sHanThongKe > '2017-01-01') ? date_select($pl.sHanThongKe) : NULL}" class="form-control datepic" placeholder="Hạn giải quyết">
                                        </div>
                                        <br>
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" target="_blank"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}
										
                                    </td>
                                    <td width="32%" class="chidao">
                                        <textarea name="chidaophogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}-1" class="form-control phogiamdoc" rows="2"  readonly="readonly">{!empty($pl.PPPHCT)&&($pl.PPPHCT)? $pl.PPPHCT : NULL}</textarea><br>
                                        <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="3"></textarea>
                                    </td>
                                    <td width="18%">
                                        {$a=2}
                                        <p><b>1.{$pl.HoTenTP}</b></p>
                                        {foreach $pl.TrinhTu as $pr}
                                        <p>{$a++}. {$pr}</p>
                                        {/foreach}
                                    </td>
                                    <td class="text-center">
                                        <br><button tabindex="10" type="button" class="btn btn-success btn-xs hidden" name="luuao" disabled><i class="fa fa-check"></i> Duyệt</button><button type="submit" class="btn btn-success btn-xs pull-center duyet_ph" name="duyet" value="{$pl.PK_iMaVBDen}" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                                        {if $vanban['iQuyenHan_DHNB'] == 5}
                                        <button type="button" name="abc"  value="{$pl.PK_iMaPhongPH}" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal3" >Từ chối</button>
<!--                                        <input type="text" name="ma_cb" value="{$pl.PK_iMaPhongPH}">-->
                                        {/if}
                                    </td>
                                </tr>
                                {/if}
                                {/foreach}
                                {else}
                                <tr class="text-center danger"> <td colspan="7">Hiện tại chưa có văn bản phối hợp</td></tr>
                                {/if}
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/phongphoihop.js"></script>
<script>
    $(document).ready(function() {
        $("button[name=abc]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=maphongban]').val($(this).val());
            });
        });
        $(document).on('click','.duyet_ph',function () {
            $('.duyet_ph').addClass('hidden');
            $('button[name=luuao]').removeClass('hidden');
        });
    });
</script>
