<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Chuyên viên phối hợp xử lý giấy mời
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-3"><h4><label for="">Là VB QPPL:</label> <input type="checkbox" name="iVanBanQPPL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanQPPL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">SNV chủ trì:</label> <input type="checkbox" name="iSTCChuTri" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iSTCChuTri'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">TBKL:</label> <input type="checkbox" name="iVanBanTBKL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanTBKL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenKV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLVB'] : NULL }</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Ngày ký <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLV'] : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <p><b>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr class="info">
                                        <td><p class="col-sm-12"><b>Thời gian:</b> <b style="color:blue">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiGio'] : NULL }</b>  <span class="text-info">ngày:</span> <b style="color:blue">{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sGiayMoiNgay']) : NULL }</b></p></td>
                                    </tr>
                                    <tr class="info">
                                        <td><p class="col-sm-7"><b>Địa điểm:</b> <b style="color:blue">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiDiaDiem'] : NULL } </b>	<b></p><p class="col-sm-5">Chủ trì:</b>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiChuTri'] : NULL } </p></td>
                                    </tr>
                                    <tr class="info">
                                        <td>
                                            <label for="" class="col-sm-2 control-label">Nội dung</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="" required value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sNoiDung'] : NULL } ">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-info">*</span>: </label>

                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenNguoiKy'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Ngày nhận <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>	29-05-2017</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sChucVu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getdAppointment[0]['sHanGiaiQuyet']) : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }</label>
                                    </div>
                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>

                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Số trang:</label>
                                    <div class="col-sm-10">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoTrang'] : NULL } (Trang)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"> Tham mưu:</label>

                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày phân loại:</label>

                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
							
							<div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-2 control-label">Tệp tin đính kèm:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered" style="width: 550px">

                                        <thead>

                                        <tr>

                                           <th class="text-center" style="width: 50px">STT</th>

                                            <th style="">Ghi chú tệp tin</th>

                                            <th class="text-center" style="width: 100px">Đính kèm</th>

                                            <th class="text-center" style="width:20%">Thời gian</th>


                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}
                                        {if !empty($Filedinhkem)}
                                        {foreach $Filedinhkem as $fi}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td>{$fi.sTenFile}</td>
                                            <td class="text-center"> <a class="tin1" href="{$fi.sDuongDan}" download>Download</a></td>
                                            <td class="text-center" width="15%">{date_select($fi.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}
                                        {/if}
                                        </tbody>

                                    </table>

                                </div>

                            </div>
							
                            {if !empty($idDocGo)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered table-striped">

                                        <thead>

                                        <tr>

                                            <th class="text-center" width="50px">STT</th>

                                            <th class="text-center" width="15%">Ngày nhận</th>

                                            <th class="text-center" width="15%">Chuyển từ</th>

                                            <th>Nội dung</th>

                                            <th class="text-center" width="15%">Chuyển đến</th>

                                            <th class="text-center" width="10%">Hạn xử lý</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $process as $pr}

                                        <tr class="info">

                                            <td class="text-center">{$i++}</td>

                                            <td class="text-center">{date_time($pr.sThoiGian)}</td>

                                            <td class="text-center">

                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}

                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}

                                                {$cb.sHoTen}

                                                {/if}

                                                {/foreach}

                                            </td>

                                            <td>{$pr.sMoTa}</td>

                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanGiaiQuyet'])}{/if}<br><i>{if $pr.sThoiGianHetHan > '2017-01-01'}{/if}</i></td>
                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>
							
							{if !empty($chidao)}
									<div class="col-sm-12">
										<div class="form-group">
											<label for="" class="col-sm-4 control-label" style="color:blue">Chỉ đạo của lãnh đạo Sở:</label>
										</div>
										<div class="form-group">
											<table class="table table-bordered table-striped" style="width: 88%">
												<thead>
												<tr style="background-color: orange !important">
													<th class="text-center" style="width:50px">STT</th>
													<th class="text-center" style="width:17%">Lãnh đạo</th>
													<th>Nội dung</th>
													<th class="text-center" style="width:17%">Thời gian</th>
												</tr>
												</thead>
												<tbody>
												{$i=1}
												
												{foreach $chidao as $cd}
												{$ld = layTTCB($cd.FK_iMaCB)}
												<tr class="info">
													<td class="text-center">{$i++}</td>                                            
													<td class="text-center"><span style="color: red"> {$ld[0].sHoTen}</span></td>
													<td><span style="color: red"> {$cd.sNoiDung} </span></td>
													<td class="text-center">{date_time2($cd.sThoiGian)}</td>
												</tr>
												{/foreach}
												</tbody>
											</table>
										</div>
									</div>
								{/if}
								
								
							{if !empty($dexuat)}
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Đề xuất / ý kiến:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped" style="width: 88%">
                                        <thead>
                                        <tr style="background-color: orange !important">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="width:17%">Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th class="text-center" style="width:17%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										
                                        {foreach $dexuat as $dx}
										{$ld = layTTCB($dx.FK_iMaCB)}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>                                            
                                            <td class="text-center">{$ld[0].sHoTen}</td>
                                            <td>{$dx.sNoiDung}</td>
                                            <td class="text-center">{date_time2($dx.sThoiGian)} &nbsp; {if $dx.FK_iMaCB == $vanban['PK_iMaCB']}<button type="submit" value="{$dx.id}" name="xoadexuat" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							{/if}

                            {if !empty($TrinhTuPhoiHop)}
                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-12 control-label">Trình tự giải quyết Phòng phối hợp:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered table-striped">

                                        <thead>

                                        <tr class="phoihop">

                                            <th class="text-center" width="50px">STT</th>

                                            <th class="text-center" width="15%">Ngày nhận</th>

                                            <th class="text-center" width="15%">Chuyển từ</th>

                                            <th>Nội dung</th>

                                            <th class="text-center" width="15%">Chuyển đến</th>

                                            <th class="text-center" width="10%">Hạn xử lý</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $TrinhTuPhoiHop as $ph}

                                        <tr class="warning">

                                            <td class="text-center">{$i++}</td>

                                            <td class="text-center">{date_time($ph.sThoiGian)}</td>

                                            <td class="text-center">

                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb1}

                                                {if $cb1.PK_iMaCB == $ph.input_per}

                                                {$cb1.sHoTen}

                                                {/if}

                                                {/foreach}

                                            </td>

                                            <td>{$ph.sMoTa}</td>

                                            <td class="text-center">{$ph.sHoTen}</td>

                                            <td class="text-center">{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanGiaiQuyet'])}{/if}</td>


                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>
                            {/if}

                            {if !empty($resultPPH)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng phối hợp:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th width="50px">STT</th>

                                            <th width="25%">Cán bộ - Đơn vị</th>

                                            <th>Nội dung</th>

                                           
                                            <th width="15%">Thời gian</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $resultPPH as $re}

                                        <tr >

                                            <td class="text-center">{$i++}</td>

                                            <td width="20%">{$re.sHoTen} - {$re.sTenPB}</td>

                                            <td>{$re.sMoTa}</td>

                                        

                                            <td width="15%" class="text-center">{date_select($re.date_file)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            {/if}
                            {if !empty($resultPTL)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng chủ trì:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th width="50px">STT</th>

                                            <th width="25%">Cán bộ - Đơn vị</th>

                                            <th>Nội dung</th>

                                            <th class="text-center" width="10%">Tải về</th>

                                            <th class="text-center" width="10%">Thời gian</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $resultPTL as $tl}

                                        <tr >

                                            <td class="text-center">{$i++}</td>

                                            <td width="20%">{$tl.sHoTen} - {$tl.sTenPB}</td>

                                            <td>{$tl.sMoTa}</td>

                                            <td width="8%" class="text-center"><a href="{$tl.sDuongDanFile}">Download</a></td>

                                            <td width="15%" class="text-center">{date_select($tl.date_file)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            {/if}



                            <div class="col-sm-12">

                                <div class="col-sm-6">

                                    <div class="form-group">

                                        <label for="" class="col-sm-12">Kết quả giải quyết <br><span style="color: red">  <i>(chỉ nhập khi có kết quả giải quyết)</i></span></label>

                                        <div class="col-sm-12">

                                            <textarea name="ketqua" id="" class="form-control" rows="4"></textarea>

                                        </div>

                                    </div>

                                </div>

                                <!--{if $getdAppointment[0]['sGiayMoiNgay'] < date('Y-m-d',time())}

                                <div class="col-sm-6">

                                    <div class="form-group">

                                        <div class="col-sm-12">
                 
                                            <div class="form-group">
                                                <label for="" class="col-sm-12 control-label">
                                                    Tệp tin VB giải quyết: <span class="text-info">*</span></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="fileketqua" class="form-control" placeholder="Tên file">
                                                </div>
                                                <div class="col-sm-5">
                                                    <input type="file" name="files">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/if}-->
                            </div>
                            {/if}
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="luulai" value="Lưu lại">Hoàn thành</button>
                                <input type="text" name="canbo" value="{if !empty($CBChiDao[0]['input_per'])}{$CBChiDao[0]['input_per']}{/if}" class="hidden"/>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $('.phoihop').css('background-color', '#FF9800');
    });
</script>