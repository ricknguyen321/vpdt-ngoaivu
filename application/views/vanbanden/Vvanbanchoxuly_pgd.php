<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-4">
                    <h3 class="font">
                        Văn bản chờ xử lý  <b>({$count+count($getDocAwaitPH)})</b>
                    </h3>
                </div>
                <div class="col-md-4 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
										{if $de.PK_iMaPB != 78}
											<input type="checkbox" name="chonphong" value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" class="Checkbox"> {$de.sTenPB}<br>
										{/if}
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row"><br>
                        <div class="col-sm-12 text-center">
                            <div class="collapse" id="collapseExample">
                                <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                    <div class="row">
                                        <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
													<br>
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                               
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="post">
                                <div class="col-sm-8">
                                    {$phantrang}
                                </div>
                                
								
								<div class="col-sm-12 deletepadding" style="margin-bottom: 0px;">
									<div class="col-sm-3">
										<p style="font-size: 15px"><b>Ghi chú màu Trích yếu:</b></p>
									</div>
									<div class="col-sm-2">
										<span><b style="color: red"><i class="fa fa-circle" aria-hidden="true"></i></b>: Quá hạn</span>
									</div>
									<div class="col-sm-2">
									<span><b style="color: orange"><i class="fa fa-circle" aria-hidden="true"></i></b>: Sắp đến hạn</span>
									</div>
									<div class="col-sm-2">
									<span><b style="color: black"><i class="fa fa-circle" aria-hidden="true"></i></b>: Trong hạn</span>
									</div>
									<div class="col-sm-3">
									<button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                                <button type="button" id="duyet" class="hidden">Duyet</button>
									</div>
								</div>
								
								
								
								
                                <table id="" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="5%" class="text-center">STT</th>
                                        <th width="" class="text-center">Ngày nhập</th>
                                        <th width="30%" class="text-center">Trích yếu - Thông tin</th>
                                        <th width="" >Ý kiến</th>
                                        <th width="" class="text-center">Chỉ đạo</th>
                                        <th width="" class="text-center">Duyệt <br> <input type="checkbox" id="select_all"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {if $vanban['iQuyenHan_DHNB'] == 5}
                                    {$i=1}
                        
                                    {foreach $getDocAwait as $pl}
                                    <tr class="thongbao">
                                        <td class="text-center">{$i++}</td>
                                        <td>{date_select($pl.sNgayNhap)}</td>
                                        <td>
                                            <p><b><a style="color: black;" href="{$url}quatrinhxuly/{$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: orange">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a></b></p>
                                            - Số ký hiệu: {$pl.sKyHieu} <br>
                                            - Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> <br>
                                            <p>- Nơi gửi: <span style="color: black;">{$pl.sTenDV}</span></p>
                                         
                                            {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
											{if $pl.iSTCChuTri == 1 || $pl.iVanBanTBKL == 1 || $pl.iToCongTac == 1 || $pl.iSTCPhoiHop == 1 || $pl.iVanBanQPPL == 1}
											<h5>- Loại VB:<b style="color: blue"> <i>{($pl.iSTCChuTri == 1) ? 'SNV chủ trì.' : NULL }</i> <i>{($pl.iVanBanTBKL == 1) ? 'TBKL' : NULL }</i> <i>{($pl.iToCongTac == 1) ? 'TTHC' : NULL }</i> <i>{($pl.iSTCPhoiHop == 1) ? 'VB Đảng' : NULL }</i> <i>{($pl.iVanBanQPPL == 1) ? 'VB QPPL' : NULL }</i></b></h5>
											{/if}
                                        </td>
										<td width="18%" class="ykien">
                                            <div style="margin-bottom: 10px">
                                                <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc1">
                                                    <option value="">Chuyền P.Giám đốc</option>
                                                    {foreach $getAccountDeputyDirector as $pgd}
                                                    <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-toggle="{$pl.sMoTa}" data-id="{$pgd.sHoTen}" {!empty($pl.PK_iMaCBNhan[5])&&($pl.PK_iMaCBNhan[5] == $pgd.PK_iMaCB) ? selected : NULL}>{$pgd.sHoTen}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                            <div style="margin-bottom: 10px">
                                                <select name="phongchutri[{$pl.PK_iMaVBDen}]" id="" class="form-control phongchutri1">
                                                    <option value="">Chọn chủ trì</option>
                                                    {foreach $getDepartment as $de}
														{if $de.PK_iMaPB != 78}
															<option value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" data-num="{$de.PK_iMaCB}" data-collapse="{$pl.sMoTa}" {!empty($pl.PK_iMaPhongCT[6])&&($pl.PK_iMaPhongCT[6] == $de.PK_iMaPB) ? selected : NULL}>{$de.sTenPB}</option>
														{/if}
                                                    {/foreach}
                                                </select>
                                            </div>
                                            <div style="margin-bottom: 10px">
                                                <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">Chọn phối hợp</button>
                                            </div>

                                            <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="{($pl.PK_iMaPhongPH) ? $pl.PK_iMaPhongPH[7] : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden mangphoihop1">
                                            <input type="text" name="idpgd[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCN1[5])&&($pl.PK_iMaCN1[5]) ? $pl.PK_iMaCN1[5] : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden">
                                            <input type="text" name="idpcc[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCN[6])&&($pl.PK_iMaCN[6]) ? $pl.PK_iMaCN[6] : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden">
                                            <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden doc_id1">
                                            <div style="margin-bottom: 10px">
                                                <input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{($pl.sThoiGianHetHan[5] > '2017-01-01') ? date_select($pl.sThoiGianHetHan[5]) : (($pl.sHanGiaiQuyet > '2017-01-01') ? date_select($pl.sHanGiaiQuyet) : NULL)}" class="form-control datepic hangiaiquyet1" placeholder="Hạn giải quyết">
                                            </div>
											<br>
											
											<p><a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}</p>
											
											
											 <span><input type="checkbox" name="vanbanquantrong[{$pl.PK_iMaVBDen}]" value="1"></span> <span style="color: red;"> VB Quan trọng</span>
                                        </td>
                                        <td width="35%" class="chidao">
                                            <textarea name="chidaophogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}" class="form-control phogiamdoc" rows="2">{!empty($pl.ChuThich[5])&&($pl.ChuThich) ? $pl.ChuThich[5] : NULL}</textarea><br>
                                            <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="4">{!empty($pl.ChuThich[6])&&($pl.ChuThich) ? $pl.ChuThich[6] : NULL}</textarea>
											<br>{if !empty($pl.sLyDo)}<p><b class="text-danger">Nội dung từ chối*:</b> <i>{$pl.sLyDo}</i></p>{/if}
											{if !empty($pl.PhongChuyenLai['sTenPB'])}(<b>Phòng gửi lại: </b><i>{$pl.PhongChuyenLai['sTenPB']}</i>){/if}
										</td>
                                        <td class="text-center">
                                            <br><span style="color: red;"> Chọn duyệt:</span><br><input type="checkbox" name="duyet[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="duyet">
                                        </td>
                                    </tr>
                                    {/foreach}

                                    {if !empty($getDocAwaitPH)}
                                    {foreach $getDocAwaitPH as $bgd}
                                    <tr class="thongbao">
                                        <td class="text-center">{$i++}</td>
                                        <td>{date_select($bgd.sNgayNhap)}</td>
                                        <td>
											
											<p><b><a style="color: black;" href="{$url}quatrinhxuly/{$bgd.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $bgd.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $bgd.sHanThongKe)}<span style="color: orange">{$bgd.sMoTa}</span>{elseif $bgd.sHanThongKe < date('Y-m-d',time()) && $bgd.sHanThongKe > '2017-01-01'}<span style="color: red">{$bgd.sMoTa}</span>{else}<span>{$bgd.sMoTa}</span>{/if}</a></b></p>
											
                                            - Số ký hiệu: {$bgd.sKyHieu} <br>
                                            - Số đến: <b style="color: red; font-size: 16px">{$bgd.iSoDen}</b> <br>
                                            <p>- Nơi gửi: <span style="color: black;">{$bgd.sTenDV}</span></p>
                                            
                                            {if !empty($bgd.sNoiDung)}<p>* Nội dung: <i><b>{$bgd.sNoiDung}</b></i></p>{/if}
                                            {if $bgd.iSTCChuTri == 1 || $bgd.iVanBanTBKL == 1 || $bgd.iToCongTac == 1 || $bgd.iSTCPhoiHop == 1 || $bgd.iVanBanQPPL == 1}
                                            <h5><b style="color: blue">Văn bản thuộc loại:</b> <i>{($bgd.iSTCChuTri == 1) ? 'SNV chủ trì.' : NULL }</i> <i>{($bgd.iVanBanTBKL == 1) ? 'Thông báo kết luận.' : NULL }</i> <i>{($bgd.iToCongTac == 1) ? 'TTHC.' : NULL }</i> <i>{($bgd.iSTCPhoiHop == 1) ? 'SNV phối hợp.' : NULL }</i> <i>{($bgd.iVanBanQPPL == 1) ? 'VB QPPL.' : NULL }</i></h5>
                                            {/if}

                                            <p><b style="color: red">Lưu ý:</b> <b><i>Văn bản chuyển đến Ban Giám đốc Sở để biết</i></b></p>
                                        </td>
                                        <td width="18%" class="ykien">
                                            <div style="margin-bottom: 10px">
                                                <select name="phongchutri[{$bgd.PK_iMaVBDen}]" id="" class="form-control phongchutri1" disabled>
                                                    <option value="">Chọn chủ trì</option>
                                                </select>
                                            </div>
                                            <div style="margin-bottom: 10px">
                                                <button type="button" name="chonphoihop" value="{$bgd.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" disabled>Chọn phối hợp</button>
                                            </div>
                                            <input type="text" name="doc_id[{$bgd.PK_iMaVBDen}]" value="{$bgd.PK_iMaVBDen}" class="hidden doc_id1">
                                            <div style="margin-bottom: 10px">
                                                <p><input type="text" name="hangiaiquyet[{$bgd.PK_iMaVBDen}]" value="{($bgd.sHanThongKe > '2017-01-01') ? date_select($bgd.sHanThongKe) : NULL}" class="form-control datepic hangiaiquyet1" placeholder="Hạn giải quyết"></p>
                                            </div>
											<a class="tin1" href="{$url}teptinden?id={$bgd.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> | {if $bgd.iFile==1} <a target="_blank" href="{$bgd.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($bgd.sDuongDan)=='pdf'}Xem file{else}Xem{/if}</a>{/if}
                                        </td>
                                        <td width="35%" class="chidao">
                                            <p>{$bgd.mieuta}</p>
                                        </td>
                                        <td class="text-center">
                                           
                                        </td>
                                    </tr>
                                    {/foreach}
                                    {/if}
                                    {/if}
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
							
                                <div class="col-sm-8">
                                    {$phantrang}
                                </div>
                                <p class="hidden" id="id_cb">{$vanban['PK_iMaCB']}</p>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly.js"></script>
<script>
    $(document).on('click','button[name=duyetvanban]',function(){
        $('#duyet').click();
    });
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector("#duyet").addEventListener("click", function(e){
            var id = $('#id_cb').text();
            $(".thongbao").each(function () {
                var phogiamdoc = $(this).find('.ykien').find('.phogiamdoc1 :selected').val();
                var tieudepgd = $(this).find('.ykien').find('.phogiamdoc1 :selected').attr('data-toggle');
                var phongchutri = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-num');
                var tieude = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-collapse');
                var checkDoc = $(this).find('.duyet').is(':checked');
                if (checkDoc == true) {
                    if (phogiamdoc != id) {
//                    console.log(giamdoc);
                        var notification1 = {
                            'is_notification': false,
                            'id_cb': phogiamdoc,
                            'base_url': 'vanbanchoxuly_pgd',
                            'notification': tieudepgd
                        };
                        notificationsRef.push(notification1);
                    }
                    if (phogiamdoc == id) {
                        if (phongchutri != undefined) {
                            var notification2 = {
                                'is_notification': false,
                                'id_cb': phongchutri,
                                'base_url': 'vanbanchoxuly_tp',
                                'notification': tieude
                            };
                            notificationsRef.push(notification2);
                        }
                    }
                }
            });
        });
    });
</script>
