<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="refresh" content="30"> 30 giây refesh trang 1 lần-->
  <link href="" rel="shortcut icon" type="image/x-icon" />
  <title>Giấy mời họp</title>
  <base href="">
  <meta name="author" content="Sở Ngoại Vụ">
  <meta name="copyright" content="Sở Ngoại Vụ">
  <meta name="description" content="LỊCH HỌP DO LÃNH ĐẠO SỞ CHỦ TRÌ ">
  <meta name="robots" content="noindex,nofollow">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">
  <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$url}assets/js/plugins/font-awesome/css/font-awesome.min.css">
  <style>
    .container {
        font-family:"Times New Roman", Times, serif;
        font-size:13pt;
    }
    .indam{
        font-weight: bold;
    }
  </style>
</head>

<body><br/>
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12" style="float:right">
                <form action="" method="get">

                    <div class="col-md-1">
                        <div class="form-group">
                             <h3 class="indam text-center"><a href="http://103.205.100.97/vpdtsongoaivu/welcome.html" class="btn btn-primary" style="color: #FFF;" data-original-title="" title=""><i class="fa fa-home" aria-hidden="true"></i> <b>TRANG CHỦ</b></a></h3>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group"></div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                             <h3 class="indam text-center"><i class="fa fa-calendar"></i> LỊCH HỌP LĐ SỞ</h3>
                        </div>
                    </div>
                    <div class="col-md-1" style="margin-right:10px">
                        <div class="form-group">
                            <a href="{$url}lichtuanlanhdao?tuan={$tuanhientai}" class="btn btn-primary"><i class="fa fa-calendar"></i> Tuần này </a>
                        </div>
                    </div>
                    <div class="col-md-1 text-center">
                        <div class="form-group">
                            <a href="{$url}lichtuanlanhdao?tuan={$tuantruoc}" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="tuan" id="" class="form-control select2" onchange="this.form.submit()">
                                {for $i=1;$i<54;$i++}
                                    <option value="{($i<10)?'0':''}{$i}" {($tuan==$i)?'selected':''}>Tuần {$i}</option>
                                {/for}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 text-center">
                        <div class="form-group">
                            <a href="{$url}lichtuanlanhdao?tuan={$tuansau}" class="btn btn-primary"><i class="fa fa-forward"></i></a>
                        </div>
                    </div>
                </form>
                <div class="col-md-3">
                   <!-- <a href="{$url}lichhoplanhdao" class="btn btn-success"> Lịch họp của Sở chủ trì</a>-->
                </div>
            </div>
        </div>
    <div class="col-md-12">
    <!-- Ghi chú màu trích yếu: <b style="color:#da8a0b">Lịch cán bộ các phòng nhập</b> &nbsp;&nbsp;&nbsp;&nbsp;<b style="color:#337ab7">Lịch họp từ giấy mời đến</b> -->
    <!-- <span class="pull-right">Lọc theo lãnh đạo: {foreach $bgd as $ld}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="lichcongtac?ldso={$ld.PK_iMaCB}&tuan={$tuan}"><b style="color:{if $ld.sHoTen == 'Ngô Minh Hoàng'} red {elseif $ld.sHoTen == 'Nguyễn Nam Hải'} green {else} blue {/if}">{$ld.sHoTen}</b></a>{/foreach} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="lichcongtac?tuan={$tuan}"><b style="color:black">Show all</b></a></span> -->
    </div>
        <div class="row">
        <form action="" method="post" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            <div class="col-md-12">
              <table id="" class="table table-bordered table-hover">
                <tr style="background-color: rgb(60, 141, 188); color: rgb(255, 255, 255);">
                  <th width="8%" class="text-center">Lãnh đạo Sở</th>
                  <th width="3%" class="text-center">Buổi</th>
                  <th width="14%" class="text-center">
                      <b>{$ngaytuan[0][0]}<b><br>
                      <i>{$ngaytuan[0][1]}</i>
                  </th>
                   <th width="14%" class="text-center">
                      <b>{$ngaytuan[1][0]}<b><br>
                      <i>{$ngaytuan[1][1]}</i>
                  </th>
                   <th width="14%" class="text-center">
                      <b>{$ngaytuan[2][0]}<b><br>
                      <i>{$ngaytuan[2][1]}</i>
                  </th>
                   <th width="14%" class="text-center">
                      <b>{$ngaytuan[3][0]}<b><br>
                      <i>{$ngaytuan[3][1]}</i>
                  </th>
                   <th width="14%" class="text-center">
                      <b>{$ngaytuan[4][0]}<b><br>
                      <i>{$ngaytuan[4][1]}</i>
                  </th>
                   <th width="7%" class="text-center">
                      <b>{$ngaytuan[5][0]}<b><br>
                      <i>{$ngaytuan[5][1]}</i>
                  </th>
                   <th width="7%" class="text-center">
                      <b>{$ngaytuan[6][0]}<b><br>
                      <i>{$ngaytuan[6][1]}</i>
                  </th>                       
              </tr>
                                              <tr>
                              <td rowspan="2"><b style="color: blue">{$bgd[0]['sHoTen']}</b></td>
                              <td>sáng</td>
                              <!-- sáng thứ 2 -->
                              <td>
                                {if isset($ngaytrongtuan[0]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[0]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iDuHop == 1}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 3 -->
                              <td>
                                {if isset($ngaytrongtuan[1]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[1]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iDuHop == 1}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 4 -->
                              <td>
                                {if isset($ngaytrongtuan[2]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[2]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iDuHop == 1}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 5 -->
                              <td>
                                {if isset($ngaytrongtuan[3]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[3]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iDuHop == 1}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 6 -->
                              <td>
                                {if isset($ngaytrongtuan[4]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[4]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iDuHop == 1}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 7 -->
                              <td>
                                {if isset($ngaytrongtuan[5]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[5]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iDuHop == 1}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng chủ nhật -->
                              <td>
                                {if isset($ngaytrongtuan[6]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[6]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iDuHop == 1}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 
                            </tr>

                            <!-- BUỔI CHIỀU -->
                            </tr>
                            <tr>
                              <td>Chiều</td>
                              <!-- chiều thứ 2 -->
                              <td>
                                {if isset($ngaytrongtuan[0]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[0]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iDuHop == 1}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- chiều thứ 3 -->
                              <td>
                                {if isset($ngaytrongtuan[1]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[1]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iDuHop == 1}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 4 -->
                              <td>
                                {if isset($ngaytrongtuan[2]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[2]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iDuHop == 1}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 5 -->
                              <td>
                                {if isset($ngaytrongtuan[3]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[3]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iDuHop == 1}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 6 -->
                              <td>
                                {if isset($ngaytrongtuan[4]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[4]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iDuHop == 1}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 7 -->
                              <td>
                                {if isset($ngaytrongtuan[5]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[5]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iDuHop == 1}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều chủ nhật -->
                              <td>
                                {if isset($ngaytrongtuan[6]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[6]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[0]['PK_iMaCB']}
                                    
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iDuHop == 1}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>
                            </tr>

                            <!-- PHÓ GIÁM ĐỐC 1 -->
                                                        <tr>
                              <td rowspan="2"><b style="color: blue">{$bgd[1]['sHoTen']}</b></td>
                              <td>sáng</td>
                              <!-- sáng thứ 2 -->
                              <td>
                                {if isset($ngaytrongtuan[0]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[0]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 3 -->
                              <td>
                                {if isset($ngaytrongtuan[1]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[1]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 4 -->
                              <td>
                                {if isset($ngaytrongtuan[2]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[2]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 5 -->
                              <td>
                                {if isset($ngaytrongtuan[3]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[3]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 6 -->
                              <td>
                                {if isset($ngaytrongtuan[4]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[4]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 7 -->
                              <td>
                                {if isset($ngaytrongtuan[5]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[5]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng chủ nhật -->
                              <td>
                                {if isset($ngaytrongtuan[6]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[6]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 
                            </tr>

                            <!-- BUỔI CHIỀU -->
                            </tr>
                            <tr>
                              <td>Chiều</td>
                              <!-- chiều thứ 2 -->
                              <td>
                                {if isset($ngaytrongtuan[0]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[0]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- chiều thứ 3 -->
                              <td>
                                {if isset($ngaytrongtuan[1]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[1]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 4 -->
                              <td>
                                {if isset($ngaytrongtuan[2]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[2]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 5 -->
                              <td>
                                {if isset($ngaytrongtuan[3]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[3]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 6 -->
                              <td>
                                {if isset($ngaytrongtuan[4]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[4]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 7 -->
                              <td>
                                {if isset($ngaytrongtuan[5]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[5]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều chủ nhật -->
                              <td>
                                {if isset($ngaytrongtuan[6]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[6]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[1]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[1]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>
                            </tr>

                            <!-- PHÓ GIÁM ĐỐC 2 -->
                                                        <tr>
                              <td rowspan="2"><b style="color: blue">{$bgd[2]['sHoTen']}</b></td>
                              <td>sáng</td>
                              <!-- sáng thứ 2 -->
                              <td>
                                {if isset($ngaytrongtuan[0]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[0]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 3 -->
                              <td>
                                {if isset($ngaytrongtuan[1]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[1]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 4 -->
                              <td>
                                {if isset($ngaytrongtuan[2]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[2]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1  && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 5 -->
                              <td>
                                {if isset($ngaytrongtuan[3]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[3]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 6 -->
                              <td>
                                {if isset($ngaytrongtuan[4]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[4]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng thứ 7 -->
                              <td>
                                {if isset($ngaytrongtuan[5]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[5]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- sáng chủ nhật -->
                              <td>
                                {if isset($ngaytrongtuan[6]['sang']['giaymoi'])}
                                {foreach $ngaytrongtuan[6]['sang']['giaymoi'] as $sang}
                                  {$ldsang = str_replace('- PGĐ','',$sang.lanhdaoduhop)}
                                  {$dsld = explode(',', $sang.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($sang.PK_iMaVBDi)}
                                    {if $sang.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$sang.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$sang.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($sang.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $sang.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$sang.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($sang.sMoTa) >0} style="color:red"{/if}>{catKyTu1($sang.sMoTa)}</span></b></a> <i>(GM số: {$sang.sKyHieu})</i><br>
                                    {if !empty($sang.sNoiDung)}
                                      {$sang.sNoiDung}<br>
                                    {/if}
                                    {if !empty($sang.sGhiChu)}{$sang.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$sang.sTenDV} <br>
                              
                                    {$dschidao = laychidao($sang.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($sang.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 
                            </tr>

                            <!-- BUỔI CHIỀU -->
                            </tr>
                            <tr>
                              <td>Chiều</td>
                              <!-- chiều thứ 2 -->
                              <td>
                                {if isset($ngaytrongtuan[0]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[0]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td> 

                              <!-- chiều thứ 3 -->
                              <td>
                                {if isset($ngaytrongtuan[1]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[1]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 4 -->
                              <td>
                                {if isset($ngaytrongtuan[2]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[2]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 5 -->
                              <td>
                                {if isset($ngaytrongtuan[3]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[3]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 6 -->
                              <td>
                                {if isset($ngaytrongtuan[4]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[4]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}

                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều thứ 7 -->
                              <td>
                                {if isset($ngaytrongtuan[5]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[5]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>

                              <!-- chiều chủ nhật -->
                              <td>
                                {if isset($ngaytrongtuan[6]['chieu']['giaymoi'])}
                                {foreach $ngaytrongtuan[6]['chieu']['giaymoi'] as $chieu}
                                  {$ldsang = str_replace('- PGĐ','',$chieu.lanhdaoduhop)}
                                  {$dsld = explode(',', $chieu.FK_iMaCB_Ky)}
                                  {$lddh = array()}
                                  {foreach $dsld as $ld}{$ld = trim($ld)}
                                    {$kkkk = array_push($lddh, $dscanbo[$ld])}
                                  {/foreach}
                                  {$duhop = implode(', ', $lddh)}
                                  <!-- nếu là văn bản đi -->
                                  {if isset($chieu.PK_iMaVBDi)}
                                    {if $chieu.FK_iMaCB_Ky == $bgd[2]['PK_iMaCB']}
                                    {if isset($ldso) && $duhop != $dscanbo[$ldso]}{continue}{/if}
                                      <b><u>{$chieu.sGioMoi}</u>: </b>
                                        <b style="color:#da8a0b; font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b><br>
                                        - Địa điểm: <b>{$chieu.sDiaDiemMoi}</b><br>
                                        {$dsfile = layTLHop($chieu.PK_iMaVBDi)}
                                        <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                          {foreach $dsfile as $f}
                                            <p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a></p>
                                          
                                          {/foreach}
                                        </div>
                                    {/if}
                                  <!-- trái lại là văn bản đến -->
                                  {else}
                                    {if $chieu.iPGDDuHop == 1 && strpos($ldsang,$bgd[2]['sHoTen']) !== false}
                                    
                                    <b><u>{$chieu.sGiayMoiGio}</u>: </b>
                                    <a href="quytrinhgiaymoi/{$sang.PK_iMaVBDen}" target="_blank"><b style="font-size:14px"><span {if timkiemkytu($chieu.sMoTa) >0} style="color:red"{/if}>{catKyTu1($chieu.sMoTa)}</span></b></a> <i>(GM số: {$chieu.sKyHieu})</i><br>
                                    {if !empty($chieu.sNoiDung)}
                                      {$chieu.sNoiDung}<br>
                                    {/if}
                                    {if !empty($chieu.sGhiChu)}{$chieu.sGhiChu}<br>{/if}
                                    - Địa điểm: <b>{$sang.sGiayMoiDiaDiem}</b><br>
                                    - Nơi gửi: {$chieu.sTenDV} <br>
                              
                                    {$dschidao = laychidao($chieu.PK_iMaVBDen)}
                                    
                                      {$dsfile = layTLHop2($chieu.PK_iMaVBDen)}
                                      <div style="max-height:100px;  overflow:auto; margin-top:10px;">
                                        {foreach $dsfile as $f}
                                          <a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{substr($f.sTenFile, 0, 30)}</span></a><br>     
                                        {/foreach}
                                      </div>
                                  {/if}
                                {/if}
                                <!-- kết thúc văn bản đến đi -->
                                {/foreach}
                                {/if}
                              </td>
                            </tr>


                    </tbody>    
                </table>
            </div>
        </form>
        </div>
        <style>
            .open-small-chat1 {
                height: 38px;
                width: 38px;
                display: block;
                background: #3c8dbc;
                padding: 9px 8px;
                text-align: center;
                color: #fff;
                border-radius: 50%;
            }
        </style>
        
    <style>
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #0c0b0b;
        }
        hr{
            border-top: 1px solid #9E9E9E;
            margin: 10px 0px;
        }
        h2, h3 {
            margin-top: 0px;
            margin-bottom: 20px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('td').css('vertical-align', 'top');
            $('th').css('vertical-align', 'middle');
        });
    </script>

</body>
</html>
