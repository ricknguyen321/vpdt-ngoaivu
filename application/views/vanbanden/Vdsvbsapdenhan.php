<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                 <div class="col-sm-3">
                    <h3 class="font">
                        Văn bản sắp đến hạn xử lý <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Tìm kiếm</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                    <div class="modal-body">
                        <div class="row" >
                            <div class="col-sm-12">
                                <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                <div class="col-sm-12">
                                    <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung từ chối...."></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label for="" class="col-sm-12">Nhập ghi chú</label>
                                <div class="col-sm-12">
                                    <input type="text" name="ghichu" class="form-control" placeholder="Nhập ghi chú ....">
                                </div>
                            </div>
                            <input type="text" name="maphongban" value="" class="hidden">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Chuyển chánh van phong">Chuyển chánh văn phòng</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal về trưởng phòng-->
        <div class="modal fade phongphoihop" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung từ chối...."></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập ghi chú</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="ghichu" class="form-control" placeholder="Nhập ghi chú ....">
                                    </div>
                                </div>
                                <input type="text" name="maphongbantp" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoitp" class="btn btn-primary btn-sm" value="Chuyển trưởng phòng">{if $vanban['iQuyenHan_DHNB'] == 11}Chuyển Chi Cục Trưởng{else}Chuyển trưởng phòng{/if}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!----- thêm hạn văn bản ------->
        <div class="modal fade" id="myModal3" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Đề xuất hạn giải quyết</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập hạn đề xuất</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="handexuat" class="form-control datepic datemask" placeholder="Nhập hạn đề xuất ....">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <label for="" class="col-sm-12">Lý do thêm hạn giải quyết</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập lý do...."></textarea>
                                    </div>
                                </div>
                                <input type="text" name="mavbd" value="" class="hidden">
                                <input type="text" name="ldgui" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="themhan" class="btn btn-primary btn-sm" value="Chuyển chánh van phong">Gửi Lãnh đạo</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="2" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <span class="pull-right h1no">{$phantrang}</span>
                            <br>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    <th width="10%" width="" class="text-center">Ngày nhập</th>
                                    <th width="8%" class="text-center">Số đến</th>
                                    <th width="" class="text-center">Trích yếu - Thông tin</th>
                                    <th width="22%" class="text-center">Trình tự xử lý</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i=1}
                                {foreach $getDocAwait as $pl}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-center">{date_select($pl.sNgayNhap)}</td>
                                    <td class="text-center"><b style="color: red; font-size: 16px">{$pl.iSoDen}</b></td>
                                    <td>
                                        {if $pl.iGiayMoi == '1'}
											<p><b><a href="{$url}quytrinhgiaymoi/{$pl.PK_iMaVBDen}" style="color: #0c0b0b">{$pl.sMoTa}</a></b></p>
											<p>- Thời gian: <b style="color:blue">{$pl.sGiayMoiGio}</b> ngày <b style="color:blue">{date_select($pl.sGiayMoiNgay)}</b>, tại <b style="color:blue">{$pl.sGiayMoiDiaDiem}</b></p>
											
											<!--<b>{$pl.sChuTri}</b><br>-->
											- Nơi gửi: {$pl.sTenDV}<br>
											<p style="">- <b>Hạn xử lý : </b>
											
											{if $pl.sHanThongKe >'1970-01-01'} {date_select($pl.sHanThongKe)}
											{else}
												{if $pl.sThoiGianHetHan > '2017-01-01'}{date_select($pl.sThoiGianHetHan)}
													{else}
														{date_select($pl.sHanGiaiQuyet)}
													
												{/if}
											{/if}</p>
                                       
									   {else}
											<p>											
											<a href="{$url}quatrinhxuly/{$pl.PK_iMaVBDen}" style="color: orange"><b>{$pl.sMoTa}</b></a>
											</p>
											- Nơi gửi: {$pl.sTenDV}<br>
											- Ngày ký: <b>{date_select($pl.sNgayKy)}</b><br>
											<b style=""><b>- Hạn xử lý : </b>{if $pl.sHanThongKe >'1970-01-01'}{date_select($pl.sHanThongKe)}{else}{date_select($pl.sHanGiaiQuyet)}{/if}</b>
                                        {/if}
										<br>
										<div class="pull-right" style="text-align:right;">
										{$dsfile=layDuLieu('FK_iMaVBDen',$pl.PK_iMaVBDen,'tbl_files_vbden')}
										<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" ><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a><br>
											{foreach $dsfile as $f}
											<p style="margin-bottom:3px">
												<a style="color:brown" href="{$f.sDuongDan}">{$f.sTenFile}</a></p>
											{/foreach}
                                        </div>
										<div>
                                        <p style="margin-top:20px; text-align:center;">
                                        {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
                                        {if $pl.iTrangThai_ThemHan != 1 && $pl.iTrangThai !=1}
										
										<br><br>
                                        <button type="button" name="dexuathan"  value="{$pl.PK_iMaVBDen}" data-id="{if !empty($pl.ChiDao)}{$pl.ChiDao}{/if}" data-num="{if !empty($pl.PK_iMaLDCD)}{$pl.PK_iMaLDCD}{/if}" class="btn btn-primary btn-xs" style="padding: 2px 5px;" data-toggle="modal" data-target="#myModal3" >Đề xuất thêm hạn</button>
                                        {/if}
                                        {/if}
                                        {if $vanban['iQuyenHan_DHNB'] == 7}
                                        {if $pl.iTrangThai_ThemHan != 1 && $pl.iTrangThai !=1}
										
										<br><br>
                                        <button type="button" name="dexuathantp"  value="{$pl.PK_iMaVBDen}" data-id="{if !empty($pl.ChiDaoPP)}{$pl.ChiDaoPP}{/if}" data-num="{if !empty($pl.PK_iMaLDCDTP)}{$pl.PK_iMaLDCDTP}{/if}" class="btn btn-primary btn-xs" style="padding: 2px 5px;" data-toggle="modal" data-target="#myModal3" >Đề xuất thêm hạn</button>
                                        {/if}
                                        {/if}
										<!--
										&nbsp;&nbsp;&nbsp;&nbsp;
										{if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
                                        {if $pl.iTrangThai != 1}
                                        <button type="button" name="abc"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" >Từ chối</button>
                                        {/if}
                                        {/if}
                                        {if $vanban['iQuyenHan_DHNB'] == 7}
                                        {if $pl.iTrangThai != 1 && $pl.iTrangThai_TruyenNhan == 6 || $pl.iTrangThai != 1 && $pl.iTrangThai_TruyenNhan == 7}
                                        <button type="button" name="tctp"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal1" >Từ chối</button>
                                        {/if}
                                        {/if}
										{if $vanban['iQuyenHan_DHNB'] == 11}
                                        {if $pl.iTrangThai != 1}
                                        <button type="button" name="tctp"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal1" >Từ chối</button>
                                        {/if}
                                        {/if}
										-->
										</p>
										</div>
                                    </td>
                                    <td >
                                        {$a=1}
                                        {foreach $pl.TrinhTu as $pr}
                                        <p>{$a++}. {$pr}</p>
                                        <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                        {/foreach}
                                    </td>
                                    
                                </tr>
                                {/foreach}

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $("button[name=abc]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=maphongban]').val($(this).val());
            });
        });
        $("button[name=tctp]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=maphongbantp]').val($(this).val());
            });
        });
        $("button[name=dexuathan]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=mavbd]').val($(this).val());
                $('button[name=themhan]').text('Gửi Lãnh đạo ( '+$(this).attr("data-id")+' )');
                $('input[name=ldgui]').val($(this).attr("data-num"));
            });
        });
        $("button[name=dexuathantp]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=mavbd]').val($(this).val());
                $('button[name=themhan]').text('Gửi Lãnh đạo ( '+$(this).attr("data-id")+' )');
                $('input[name=ldgui]').val($(this).attr("data-num"));
            });
        });
    });
</script>
