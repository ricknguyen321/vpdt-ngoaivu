<script src="{$url}assets/js/vanbanden/vanbanden.js"></script>
<script src="{$url}assets/js/plugins/checksoden.js"></script>
<!-- <script src="https://cdn.firebase.com/js/client/2.2.3/firebase.js"></script> -->
<style>
    .alert-warning {
        color: #8a6d3b !important;
        background-color: #fcf8e3 !important;
        border-color: #faebcc !important;
    }
    .alert {
        padding: 15px !important;
        margin-bottom: 20px !important;
        border: 1px solid transparent !important;
        border-radius: 4px !important;
    }
    .alert-dismissable .close {
        position: relative;
        top: -2px;
        right: 0px;
        color: inherit;
    }
    button.close {
        -webkit-appearance: none;
        padding: 0;
        cursor: pointer;
        background: 0 0;
        border: 0;
    }
    .close {
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        filter: alpha(opacity=20);
        opacity: .2;
    }
</style>
<script>
    $(document).ready(function() {
        $('#soden').css('color', 'red');
		$('input').css('font-size', '14px');
        $('textarea').css('font-size', '14px');
    });
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px;padding-left: 15px; ">
        <div class="row" style="    margin-top: -10px;">
            <div class="col-sm-3">
                <a href="{$url}dsvanbanden"><h1 style="font-size: 13px; font-weight: 600">TÌM KIẾM VĂN BẢN ĐẾN </h1></a>
            </div>

            <div class="col-sm-3">
                <a href="{$url}dsvanban"><h1 style="font-size: 13px; font-weight: 600">TÌM KIẾM VĂN BẢN ĐI</h1></a>
            </div>
			<!--<div class="col-sm-6" style="margin-top:15px">
                {$slduyet = laysoluongdaduyet()}	
				{if $slduyet[0].count > 0}
					<a href="dsvanbanchoso"><marquee scrolldelay="300"><b style="color:red"><b style="font-size:16px">{$slduyet[0].count}</b> văn bản chờ số đã được duyệt, đề nghị cán bộ văn thư cấp số và phát hành văn bản./.</b></marquee></a>
				{/if}
            </div>-->
        </div>
    </section>

    <!-- Main content -->
    <section class="content" style="padding-top: 0px">
        <div class="col-sm-12 hidden">
            <div class="noidung">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Nội dung *</label>

                        <div class="col-sm-9">
                            <textarea name="noidunghop[]" class="form-control" rows="2" placeholder="Nhập nội dung"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="col-sm-4 control-label">Hạn giải quyết </label>

                        <div class="col-sm-8">
                            <input tabindex="8" type="text" value="" name="hannoidung[]" class="form-control datepic datemask" placeholder="Hạn giải quyết của nội dung">
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        //Date picker
                        $('.datepic').datepicker({
                            autoclose: true
                        });
                        //Datemask dd/mm/yyyy
                        $(".datemask").inputmask("dd/mm/yyyy");
                    });
                </script>
            </div>
        </div>

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {if isset($content.class)&& $content.class == info}
                <div class="alert alert-warning alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <label for="" style="color:black;">Thêm thành công số đến là: <b style="font-size: 20px;color:red;" id="thongbaoden"></b></label>
                </div>
                {/if}
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="on">
                    <div class="row">
                        <div class="col-sm-12">
                                    <div class="col-md-2"><h5> <input type="checkbox" name="iVanBanQPPL" value="1" {!empty($getDocGo) &&($getDocGo[0]['iVanBanQPPL'] == '1') ? checked : NULL }><label for="">VB QPPL</label></h5></div>
                                    <div class="col-md-2"><h5> <input type="checkbox" name="iSTCChuTri" value="1" {!empty($getDocGo) &&($getDocGo[0]['iSTCChuTri'] == '1') ? checked : NULL }><label for="">SNV chủ trì</label></h5></div>
                                    <div class="col-md-2"><h5> <input type="checkbox" name="iVanBanTBKL" value="1" {!empty($getDocGo) &&($getDocGo[0]['iVanBanTBKL'] == '1') ? checked : NULL }><label for="">TBKL</label></h5></div>
                                    <!--<div class="col-md-2"><h5><label for="">Văn bản mật:</label> <input type="checkbox" name="iVanBanMat" value="1" {!empty($getDocGo) &&($getDocGo[0]['iVanBanMat'] == '1') ? checked : NULL }></h5></div>-->
                                    <div class="col-md-2"><h5> <input type="checkbox" name="iToCongTac" value="1" {!empty($getDocGo) &&($getDocGo[0]['iToCongTac'] == '1') ? checked : NULL }><label for="">TTHC</label></h5></div>
                                    <div class="col-md-2"><h5> <input type="checkbox" name="iSTCPhoiHop" value="1" {!empty($getDocGo) &&($getDocGo[0]['iSTCPhoiHop'] == '1') ? checked : NULL }><label for="">VB Đảng</label></h5></div>
									<div class="col-md-2"><h5>
									<input type="checkbox" name="iDeadline" value="1" {!empty($getDocGo) &&($getDocGo[0]['iDeadline'] == '1') ? checked : NULL } ><span style="{if !empty($getDocGo) && ($getDocGo[0]['iDeadline'] == '1')}color:blue{/if}"><label> VB có thời hạn </label> </span></h5></div>
									
									
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực:</label>

                                    <div class="col-sm-8">
                                        <select name="khuvuc" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn cấp ban hành --</option>
                                            {foreach layDuLieu(NULL,NULL,'tbl_khuvuc') as $kv}
                                            <option value="{$kv.PK_iMaKV}" {!empty($getDocGo) && ($getDocGo[0]['sTenKV'] == $kv.PK_iMaKV) ? selected : NULL }>{$kv.sTenKV}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Số ký hiệu <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <!-- <input tabindex="2" type="text" name="sokyhieu" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sKyHieu'] : NULL }" class="form-control" placeholder="Nhập số hiệu"> -->
                                        <input type="text" tabindex="2" class="form-control" name="sokyhieu" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sKyHieu'] : NULL }" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Loại văn bản <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <select name="loaivanban" required="" id="" class="form-control select2 ngayhan" style="width:100%">
                                            <option value="">-- Chọn loại văn bản --</option>
                                                {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                    <option value="{$lvb.sTenLVB}" {!empty($getDocGo) &&($getDocGo) ? ($getDocGo[0]['sTenLVB'] ==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                {/foreach}
                                        </select>
                                    </div>
                                    <!-- <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="4" type="text" class="loaivanban form-control ngayhan" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenLVB'] : NULL }" name="loaivanban" autocomplete="off" placeholder="Nhập loại văn bản" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="1" type="text" class="donvi form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenDV'] : NULL }" name="donvi" placeholder="Nhập nơi gửi đến">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" class="form-control datepic datemask" id="" name="ngayky" placeholder="Ngày ký" value="{!empty($getDocGo) &&($getDocGo) ? date_select($getDocGo[0]['sNgayKy']) : date('d/m/Y',time()) }" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="5" type="text" class="linhvuc form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenLV'] : NULL }" name="linhvuc" placeholder="Nhập lĩnh vực">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                      
							
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color:blue">Trích yếu <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-10">
                                       <textarea tabindex="6" name="trichyeu" id="trichyeu" class="form-control" rows="2" required placeholder="Nhập trích yếu">{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sMoTa'] : NULL }</textarea>
                                        <p id="baoloi"></p>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung</label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop[]" class="form-control" rows="2"  placeholder="Nhập nội dung">{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sNoiDung'] : NULL }</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"><== Hạn xử lý</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{if !empty($getDocGo[0]['sHanNoiDung']) && $getDocGo[0]['sHanNoiDung'] > '2017-01-01'}{!empty($getDocGo) &&($getDocGo) ? date_select($getDocGo[0]['sHanNoiDung']) : NULL }{/if}" name="hannoidung[]" class="form-control datepic datemask" placeholder="Hạn xử lý">
                                    </div>
                                </div>
                            </div>
                            <div class="themsau"></div>
                            {if empty($idDocGo)}
                            <div class="col-md-12 text-right">
                                <div class="col-sm-12" style="margin-left: -185px; margin-top: -35px;">
                                    <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                </div>
                            </div>
                            {/if}
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-danger"></span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="7" type="text" name="nguoiky" class="nguoiky form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenNguoiKy'] : NULL }">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày nhận:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="9" type="text" value="{!empty($getDocGo) &&($getDocGo) ? date_select($getDocGo[0]['sNgayNhan']) : date('d/m/Y',time()) }" name="ngaynhan" class="form-control ngayhan datepic datemask" placeholder="Nhập ngày nhận">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số đến:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['iSoDen'] : $soden }" name="soden" id="soden" class="form-control" placeholder="Nhập số đến" style="font-size: 16px; color: red; font-weight: 600">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Độ mật:</label>
                                    <div class="col-sm-8">
                                        <select name="domat" id="" class="form-control select2" style="width:100%">
                                            {if !empty($domat)}
                                                {foreach $domat as $dm}
                                                    <option value="{$dm.PK_iMaDM}" {if !empty($getDocGo) && $getDocGo[0]['FK_iMaDM'] == $dm.PK_iMaDM} selected{/if}>{$dm.sTenDoMat}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-danger"></span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="8" class="form-control" name="chucvu" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sChucVu'] : NULL }" id="" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết VB:</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="hangiaiquyet" value="{!empty($getDocGo) &&($getDocGo[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getDocGo[0]['sHanGiaiQuyet']) : ''}" class="form-control datepic datemask" required>
                                    </div>

                                    <div class="col-sm-3">
                                        <input type="checkbox" name="hanvb" value="1" {if !empty($getDocGo) && $getDocGo[0]['hanvb'] == 1} 'checked': ''{/if}> &nbsp;&nbsp;<b> Hạn VB </b>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang:</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="10" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['iSoTrang'] : 1 }" name="sotrang" class="form-control col-sm-4" placeholder="nhập số trang">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Độ khẩn:</label>
                                    <div class="col-sm-8">
                                        <select name="dokhan" id="" class="form-control select2" style="width:100%">
                                        {if !empty($dokhan)}
                                            {foreach $dokhan as $dk}
                                                <option value="{$dk.PK_iMaDK}" {if !empty($getDocGo) && $getDocGo[0]['FK_iMaDK'] == $dk.PK_iMaDK} selected{/if}>{$dk.sTenDoKhan}</option>
                                            {/foreach}
                                        {/if}
                                        </select>
                                    </div>
                                </div>
								
                            </div>
							
							<div class="col-md-6">
								<form method="post" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Đính kèm:</label>
                                    <div class="col-sm-8">                                        
										<input type="file" name="files[]" readonly="" placeholder="Click vào đây để chọn files" class="form-control" id="" multiple="" required="required">
                                    </div>
                                </div>
								</form>
                            </div>
							
                            <div class="col-md-6 {!empty($getDocGo) &&($getDocGo) ? 'hide':''}">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Thời hạn theo QT ISO:</label>

                                    <div class="col-sm-8">
                                        <select name="quytrinh" id="" class="form-control ngayhan select2" style="width: 100%;">
                                            {if !empty($dsquytrinh)}{$i=1}
                                                {foreach $dsquytrinh as $qt}
                                                    <option value="{$qt.iSoNgay}">{$qt.sTenQuyTrinh}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
							</div>
							
							
                            {if !empty($idDocGo)}
                            <!--<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">STT</th>
                                                <th class="text-center">Ngày nhận</th>
                                                <th class="text-center">Chuyển từ</th>
                                                <th width="40%">Nội dung</th>
                                                <th class="text-center">Chuyển đến</th>
                                                <th class="text-center">Hạn xử lý</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $process as $pr}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_time($pr.sThoiGian)}</td>
                                            <td class="text-center">
                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                                                {$cb.sHoTen}
                                                {/if}
                                                {/foreach}
                                            </td>
                                            <td>{$pr.sMoTa}</td>
                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">{if ($getDocGo[0]['sHanGiaiQuyet']) > '2017-01-01'}{date_select($getDocGo[0]['sHanGiaiQuyet'])}{/if}<br><i>{if ($pr.sThoiGianHetHan) > '2017-01-01'}(Hạn VB: {date_select($pr.sThoiGianHetHan)}){/if}</i></td>                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>-->

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Tệp tin đính kèm:</label>
                                </div>
                                <div class="form-group">
                                        <table class="table table-bordered" style="width:75%">

                                            <thead>

                                            <tr>

                                                <th class="text-center" style="width:50px">STT</th>

                                                <th>Tên tệp tin</th>

                                                <th class="text-center" style="width:20%"></th>

                                                <th class="text-center" style="20%">Thời gian</th>

                                            </tr>

                                            </thead>

                                            <tbody>

                                            {$i=1}

                                            {foreach $Filedinhkem as $fi}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td>{$fi.sTenFile}</td>
                                                <td class="text-center"> <a class="tin1" href="{$fi.sDuongDan}" target="_blank">Xem</a> | <a class="tin1" href="{$fi.sDuongDan}" download>Download</a></td>
                                                <td class="text-center" width="15%">{date_time2($fi.sThoiGian)}</td>
                                            </tr>
                                            {/foreach}

                                            </tbody>

                                        </table>
                                </div>
                            </div>
                            
                            

                            {/if}
                            <div class="col-md-12" style="text-align:center; margin-top:10px">
                                <!--<button tabindex="10" type="button" class="btn btn-primary btn-sm hidden" name="luuao" disabled>Lưu lại</button>--> <button tabindex="10" type="submit" class="btn btn-primary btn-sm" name="luulai" value="Lưu lại">Lưu lại</button> <!--<label for="">Người nhập văn bản này là: {!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sHoTen'] : $vanban['sHoTen'] }</label>-->
                                <button class="hidden" name="thongbao" id="notification-button">thongbao</button>
                                {if empty($idDocGo)}
                                <div ng-app="myModule" ng-controller="mtController" class="hidden">
                                    <button ng-click="checkSoDen()" name="checksoden">Kiểm tra số đến</button>
                                </div>
                                {/if}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'right');
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
        $(document).on('click','button[name=luulai]',function(){
            $('button[name=checksoden]').click();
            $('button[name=thongbao]').click();
        });
        $('#thongbaoden').text(parseInt($.trim($('#soden').val())) - 1);
		$(document).on('click','button[name=luulai]',function () {
            $('button[name=luulai]').addClass('hidden');
			$('button[name=luuao]').removeClass('hidden');
        });
		$(document).on('keyup','input',function () {
            $('button[name=luulai]').removeClass('hidden');
            $('button[name=luuao]').addClass('hidden');
        });
    });
</script>
{if isset($content)}
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        if (Notification.permission !== "granted"){
            Notification.requestPermission();
        }
        window.onload = function(e){
            e.preventDefault();
            if($('#soden').val()==''){
                $('#soden').focus();
                return false;
            }
            // Save notification
            var notification = {
                'is_notification' : false,
                'id_user' : 3,
                'base_url' : 'vanbanchoxuly',
                'notification' : parseInt($.trim($('#soden').val())) - 1
            };
            notificationsRef.push(notification);
//            $('#soden').val('');
            return false;
        }
    });
</script>
{/if}
<script>
    var data = {
        // "timkiem": [
        //     {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
        //         "{$lvb.sTenLVB}",
        //     {/foreach}
        // ],
        "noiden": [
            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                "{$dv.sTenDV}",
            {/foreach}
        ],
        "linhvuc": [
            {foreach layDuLieu(NULL,NULL,'tbl_linhvuc') as $lv}
                "{$lv.sTenLV}",
            {/foreach}
        ],
        "nguoiky": [
            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                "{$nk.sHoTen}",
            {/foreach}
        ]

    };
    // typeof $.typeahead === 'function' && $.typeahead({
    //     input: ".loaivanban",
    //     minLength: 1,
    //     maxItem: 10,
    //     order: "asc",
    //     hint: true,
    //     backdrop: {
    //         "background-color": "#fff"
    //     },
    //     source: {
    //         timkiem: {
    //             data: data.timkiem
    //         }
    //     },
    //     debug: false
    // });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".linhvuc",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            linhvuc: {
                data: data.linhvuc
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".donvi",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>
<script>

</script>
