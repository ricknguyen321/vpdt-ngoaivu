<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 class="font" style="text-align:center; font-size:30px;">
        </h3>
    </section>
		<h3 style="text-align:center; font-size:30px; margin-top:-30px">
            GIẤY MỜI
        </h3>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenKV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }</p>
                                    </div>
                                </div>
                               

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến:</label>
                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Ngày ký:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : NULL }</p>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trích yếu:</label>
                                    <div class="col-sm-10">
                                        <p><b style=" font-size:16px">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</b></p>
                                    </div>
                                </div>
                            </div>
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Thời gian họp:</label>
                                    <div class="col-sm-10">
                                        <b style="color:blue; font-size:16px">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiGio'] : NULL }</b>  <span class="text-info" style="color:black">ngày </span> <b style="color:blue; font-size:16px">{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sGiayMoiNgay']) : NULL }</b>
                                    </div>
                                </div>
                            </div>
							
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Địa điểm:</label>

                                    <div class="col-sm-8">
                                        <p><b style="color:blue; font-size:16px">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiDiaDiem'] : NULL } </b></p>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chủ trì:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiChuTri'] : NULL }</p>
                                    </div>
                                </div>
                            </div>
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nội dung:</label>
                                    <div class="col-sm-10">
                                        <p>{$getdAppointment[0]['sNoiDung']}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký: </label>

                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenNguoiKy'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Ngày nhận:</label>
                                    <div class="col-sm-8">
                                        <p> {!empty($getdAppointment) &&($getdAppointment) ? ($getdAppointment[0]['sNgayNhan'] > '1970-01-01')?date_select($getdAppointment[0]['sNgayNhan']):'' : NULL }</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ:</label>

                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sChucVu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn nhập:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getdAppointment[0]['sHanGiaiQuyet']) : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }</label>
                                    </div>
                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
										<p>
												{if $getdAppointment[0]['FK_iMaDM'] == 2} <b style="color:red; font-size:16px">Mật</b> {else}
													{if $getdAppointment[0]['FK_iMaDM'] == 3} <b style="color:red; font-size:16px">Tối mật</b> {else}
													{if $getdAppointment[0]['FK_iMaDM'] == 4} <b style="color:red; font-size:16px">Tuyệt mật</b> {else}
													Bình thường {/if}{/if}{/if}
										</p>
                                    </div>

                                    <label for="" class="col-sm-1">Độ khẩn:</label>
                                    <div class="col-sm-2">
                                        <p>
											{if $getdAppointment[0]['FK_iMaDK'] == 2} <b style="color:orange; font-size:16px"> Khẩn </b>{else}
													{if $getdAppointment[0]['FK_iMaDK'] == 3} <b style="color:red; font-size:16px">Tối khẩn </b> {else}
													Bình thường {/if}{/if}

										</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Số trang:</label>
                                    <div class="col-sm-10">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoTrang'] : NULL } </p>
                                    </div>
                                </div>
                            </div>
							
							<div class="col-sm-12">
                                <div class="form-group">
                                    
                                </div>
                                <div class="form-group">
								<label for="" class="col-sm-2 control-label" style="width:50%; font-size:16px"><a href="teptinden?id={$getdAppointment[0]['PK_iMaVBDen']}"><b>Tài liệu <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></b></a> &larr;Tải lên tài liệu phục vụ họp</label>
								 </div>
									<div style="max-height:250px;  overflow:auto; margin-bottom:10px">
                                    <table class="table table-bordered" style="width: 70%">
                                        <thead>
										
										<tr>
                                            <th class="text-center" style="width: 50px">STT</th>
                                            <th style=""></th>
                                            <th class="text-center" style="width:15%">Thời gian</th>
											<th class="text-center" style="width:17.5%">Cán bộ</th>
                                        </tr>
										
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $Filedinhkem as $fi}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td><a style="color:brown" href="{$fi.sDuongDan}" target="_blank">			{substr($fi.sTenFile, 0, 90)}
											</a></td>
                                            <td class="text-center" >{date_time2($fi.sThoiGian)}</td>
											<td class="text-center">{$cb = layTTCB($fi.FK_iMaCB)}{$cb[0].sHoTen}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
									</div>
                               
                            </div>
                            
                            {if !empty($idDocGo)}
							
							<div class="col-sm-12">
                                <div class="form-group">
                                   <b style="margin-left:20px"> Trình tự giải quyết:</b>
										{if !empty($process)}{$i=1}
								{foreach $process as $pr}
                                    <b style="font-size:14px; {if $i == count($process)} color:blue {/if}"> {$pr.sHoTen} </b> {if $i <count($process)} 	&rarr; {/if}
									{$i = $i + 1}
                                {/foreach}
                               
								{else} <p>----</p> {/if}
									
									{if !empty($getdAppointment[0]['iDeadline']) && $getdAppointment[0]['iDeadline'] == 1}
										<label for="" class="col-sm-4 control-label" style="float:right; "><b style=" color:blue">Lãnh đạo giao có thời hạn</b>: <b style="font-size:16px; color:red">{date_select($getdAppointment[0]['sHanThongKe'])}</b></label>
										
									{else}
										<label for="" class="col-sm-4 control-label" style="float:right; ">Hạn xử lý: <b style="font-size:16px; color:red">{if !empty($getdAppointment[0]['sHanThongKe'])}
												{date_select($getdAppointment[0]['sHanThongKe'])}
											{else}
												{if $pr.sThoiGianHetHan > '2017-01-01'}{date_select($pr.sThoiGianHetHan)} {else}
													{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanGiaiQuyet'])}{/if}
												{/if}
											{/if}</b></label>
									{/if}
									
                                </div>
								
								
								{if !empty($process)}{$i=1}								
                                
								{else} <p>----</p> {/if}
                            </div>

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <table class="table table-bordered table-striped">

                                        <thead>

                                        <tr>

                                            <th class="text-center"  width="50px">STT</th>


                                            <th class="text-center" width="15%">Chuyển từ</th>

                                            <th width="">Chỉ đạo của lãnh đạo</th>

                                            <th class="text-center"  width="15%">Chuyển đến</th>

                                            <th class="text-center"  width="15%"><b style="font-size:16px;">Hạn LĐ giao</b></th>
											
											<th class="text-center"  width="15%">Ngày nhận</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $process as $pr}

                                        <tr class="info">

                                            <td class="text-center">{$i++}</td>

                                            <td class="text-center">

                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}

                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}

                                                {$cb.sHoTen}

                                                {/if}

                                                {/foreach}

                                            </td>

                                            <td>{$pr.sMoTa}</td>

                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">											
											<b style="font-size:16px; color:red">
											{if !empty($getdAppointment[0]['sHanThongKe'])}
												{date_select($getdAppointment[0]['sHanThongKe'])}
											{else}
												{if $pr.sThoiGianHetHan > '2017-01-01'}{date_select($pr.sThoiGianHetHan)} {else}
													{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanGiaiQuyet'])}{/if}
												{/if}
											{/if}
											</b>
											</td>
											 <td class="text-center">{date_time($pr.sThoiGian)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>
							
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Trình tự chuyển nhận - lưu vết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th  width="50px" class="text-center">STT</th>
                                            
                                            <th  width="15%" class="text-center">Chuyển từ</th>
                                            <th width="">Nội dung</th>
                                            <th  width="15%" class="text-center">Chuyển đến</th>
											<th  width="15%" class="text-center">Ngày nhận</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										{$cbx = ''}
                                        {foreach $luuvetchuyennhan as $lvet}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>
                                           
                                            <td class="text-center">{if $cbx != $dscanbo[$lvet.FK_iMaCB_Chuyen]}
												{if $lvet.FK_iMaCB_Chuyen == 735}
																<b>{$dscanbo[$lvet.FK_iMaCB_Chuyen]}</b>
															{else}
																{$dscanbo[$lvet.FK_iMaCB_Chuyen]}
															{/if}
											{/if}</td>
                                            <td>
											{if $lvet.FK_iMaCB_Chuyen == 735}
																<b>{$lvet.sNoiDung}</b>
															{else}
																{$lvet.sNoiDung}
															{/if}</td>
                                            <td class="text-center">{$dscanbo[$lvet.FK_iMaCB_Nhan]}</td>
											<td class="text-center">{date_time($lvet.sThoiGian)}</td>
                                        </tr>
										{$cbx = $dscanbo[$lvet.FK_iMaCB_Chuyen]}
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							
							
							{if !empty($chidao)}
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue; font-size:15px">Chỉ đạo của lãnh đạo:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped" style="width: 88%">
                                        <thead>
                                        <tr style="background-color: orange !important">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="width:17%">Lãnh đạo</th>
                                            <th>Nội dung</th>
                                            <th class="text-center" style="width:17%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										
                                        {foreach $chidao as $cd}
										{$ld = layTTCB($cd.FK_iMaCB)}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>                                            
                                            <td class="text-center"><span style="color: red"> {$ld[0].sHoTen} </span></td>
                                            <td><span style="color: red"> {$cd.sNoiDung}</span></td>
                                            <td class="text-center">{date_time2($cd.sThoiGian)}&nbsp; {if $cd.FK_iMaCB == $vanban['PK_iMaCB']}<button type="submit" value="{$cd.id}" name="xoachidao" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							{/if}
							
							{if !empty($dexuat)}
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue; font-size:15px">Đề xuất / ý kiến:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped" style="width: 88%">
                                        <thead>
                                        <tr style="background-color: orange !important">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="width:17%">Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th class="text-center" style="width:17%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										
                                        {foreach $dexuat as $dx}
										{$ld = layTTCB($dx.FK_iMaCB)}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>                                            
                                            <td class="text-center">{$ld[0].sHoTen}</td>
                                            <td>{$dx.sNoiDung}</td>
                                            <td class="text-center">{date_time2($dx.sThoiGian)} &nbsp; {if $dx.FK_iMaCB == $vanban['PK_iMaCB']}<button type="submit" value="{$dx.id}" name="xoadexuat" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							{/if}
							

                            {if !empty($resultPPH)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng phối hợp:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th width="50px">STT</th>

                                            <th width="25%">Cán bộ - Đơn vị</th>

                                            <th>Nội dung</th>

                                            <th class="text-center"  width="8%">Tài liệu</th>

                                            <th  width="15%">Thời gian</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $resultPPH as $re}

                                        <tr >

                                            <td class="text-center">{$i++}</td>

                                            <td >{$re.sHoTen}</td>

                                            <td>{$re.sMoTa}</td>

                                            <td  class="text-center">
												{if $tl.sDuongDanFile !=''  && $tl.sDuongDanFile != 'doc_uploads/'}
                                                <a href="{$url}{$tl.sDuongDanFile}">Xem</a>
												{/if}											</td>

                                            <td  class="text-center">{date_select($re.date_file)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            {/if}
                            {if !empty($resultPTL)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng chủ trì:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th width="50px">STT</th>

                                            <th width="15%">Cán bộ</th>

                                            <th>Nội dung</th>

                                            <th class="text-center"  width="8%">Tài liệu</th>

                                            <th  width="15%">Thời gian</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $resultPTL as $tl}

                                        <tr >

                                            <td class="text-center">{$i++}</td>

                                            <td class="text-center">{$tl.sHoTen}</td>

                                            <td>{$tl.sMoTa}</td>

                                            <td class="text-center">
											
												{if $tl.sDuongDanFile !=''  && $tl.sDuongDanFile != 'doc_uploads/'}
                                                <a href="{$url}{$tl.sDuongDanFile}">Xem</a>
												{/if}
											</td>

                                            <td class="text-center">{date_time($tl.date_file)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            {/if}
							
							 <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-12 control-label">Trình tự hoàn thành công việc - lưu vết:</label>

                                </div>

                                <div class="form-group">
                                    <form action="" method="post">
                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

											<th  width="50px" class="text-center">STT</th>
                                            <th  width="15%" class="text-center">Ngày nhận</th>
                                            <th  width="20%" class="text-center">Chuyển từ</th>
                                            <th width="">Nội dung</th>
                                            <th  width="15%" class="text-center">Chuyển đến</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        
                                        {if !empty($luuvetduyet)}{$i=1}
                                        {foreach $luuvetduyet as $lvd}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_time($lvd.sThoiGian)}</td>
                                            <td class="text-center">{$dscanbo[$lvd.FK_iMaCB_Chuyen]}</td>
                                            <td>{$lvd.sNoiDung}</td>
                                            <td class="text-center">{($lvd.FK_iMaCB_Nhan>0)?$dscanbo[$lvd.FK_iMaCB_Nhan]:''}</td>
                                        </tr>
                                        {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                    </form>
                                </div>

                            </div>
                            {if !empty($VBDi)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Văn bản đi</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="50px">STT</th>
                                            <th class="text-center" width="6%">Số văn bản đi</th>
                                            <th class="text-center" width="10%">Ngày văn bản</th>
                                            <th class="text-center" >Trích Yếu</th>
                                            <th class="text-center" width="8%">Tài liệu</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $VBDi as $di}
                                        <tr class="success">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{$di.iSoVBDi}</td>
                                            <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                            <td class="text-center">{$di.sMoTa}</td>
                                            <td class="text-center">
												{if $di.sDuongDan !=''  && $di.sDuongDan != 'doc_uploads/'}
                                                <a href="{$url}{$di.sDuongDan}">Xem</a>
												{/if}
											</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}
							
							 <div class="col-md-12" style = "text-align:center">							
                            {if $getdAppointment[0]['iTrangThai'] != 1  && $vanban['iQuyenHan_DHNB'] < 6}
								
								<div class="col-sm-12" style="margin-bottom:20px; margin-top: 10px">
										<div class="col-sm-8" style="margin-left: 16%">
											<div class="form-group">
												<label for="" class="col-sm-12">Lãnh đạo có ý kiến chỉ đạo: </label>
												
												<div class="col-sm-12" style="display: inline-flex">
													<textarea name="chidao" id="" class="form-control" rows="3" placeholder="Ý kiến chỉ đạo"></textarea>
													<button class="btn btn-primary" type="submit" name="luulai" value="Chỉ đạo" style="margin-left: 5px">Gửi</button>
												</div>
												
											</div>
										</div>
																															   
								</div>
									
							{else}
								{if $getdAppointment[0]['iTrangThai'] != 1}
							
									<div class="col-sm-12" style="margin-bottom:20px; margin-top: 10px">
										<div class="col-sm-8" style="margin-left: 16%">
											<div class="form-group">
												<label for="" class="col-sm-12">Ý kiến / đề xuất </label>
												
												<div class="col-sm-12" style="display: inline-flex">
													<textarea name="dexuat" id="" class="form-control" rows="3" placeholder="Nhập ý kiến / đề xuất"></textarea>
													<button class="btn btn-primary" type="submit" name="luuykien" value="Lưu lại" style="margin-left: 5px">Gửi</button>
												</div>
												
											</div>
										</div>
																															   
									</div>
								{/if}
							{/if}							
                            </div>
							
							

                            {/if}
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>