<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Nhập mới văn bản đến
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-3"><h4><label for="">Là VB QPPL:</label> <input type="checkbox"></h4></div>
                            <div class="col-md-3"><h4><label for="">SNV chủ trì:</label> <input type="checkbox"></h4></div>
                            <div class="col-md-3"><h4><label for="">TBKL:</label> <input type="checkbox"></h4></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <p>Chưa có dữ liệu</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>440</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>Giấy mời</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>	Văn phòng UBND TPHN</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Ngày ký <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>	29-05-2017</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <p>Dự HN trực tuyến sơ kết công tác CCHC 6 tháng đầu năm 2017 và công bố Chỉ số CCHC năm 2016</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr class="info">
                                            <td><p class="col-sm-12"><b>Họp vào hồi:</b> 	 14:00   <span class="text-info">Ngày:</span>  30-05-2017	(Nhập giờ theo dạng hh:mm - Ví dụ: 8 giờ 30 phút -> 08:30)</p></td>
                                        </tr>
                                        <tr class="info">
                                            <td><p class="col-sm-7"><b>Địa điểm:</b>	 P701, Số 12 Lê Lai, Hoàn Kiếm 	<b></p><p class="col-sm-5">Người chủ trì:</b>	 Lê Hồng Sơn</p></td>
                                        </tr>
                                        <tr class="info">
                                            <td>
                                                <label for="" class="col-sm-2 control-label">Nội dung</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="" required>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-info">*</span>: </label>

                                    <div class="col-sm-8">
                                        <p>	Phạm Quí Tiên</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Ngày nhận <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>	29-05-2017</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>	Chánh Văn phòng</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết::</label>

                                    <div class="col-sm-8">
                                        <p>	29-05-2017</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">	13249</label>
                                    </div>
                                    <label for="" class="col-sm-2 control-label">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p></p>
                                    </div>

                                    <label for="" class="col-sm-2 control-label">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Số trang:</label>
                                    <div class="col-sm-10">
                                        <p>	1(Trang)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"> Tham mưu:</label>

                                    <div class="col-sm-8">
                                        <select name="" id="" class="form-control">
                                            <option value="">chọn người nhận</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày phân loại:</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Ngày nhận</th>
                                                <th>Chuyển từ</th>
                                                <th>Nội dung</th>
                                                <th>Chuyển đến</th>
                                                <th>Hạn xử lý</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Tệp tin đính kèm:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên tệp tin</th>
                                            <th>Tải về</th>
                                            <th>Ngày nhập</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-10 control-label">Kết quả giải quyết - phòng phối hợp:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Cán bộ-đơn vị</th>
                                            <th>Nội dung</th>
                                            <th>Tải về</th>
                                            <th>Ngày nhập</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-10 control-label">Kết quả giải quyết - phòng thụ lý chính:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Nội dung</th>
                                            <th>Tải về</th>
                                            <th>Ngày nhập</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="" class="col-sm-12">Kết quả giải quyết <br><span style="color: red">	<i>(chỉ nhập khi có kết quả giải quyết)</i></span></label>
                                        <div class="col-sm-12">
                                            <textarea name="" id="" class="form-control" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <label for="" class="col-sm-12">Lý do chậm muộn <br> <span style="color: red"><i>(trường hợp chưa kết thúc được văn bản thì nhập lý do, tiến độ giải quyết-sau đó click vào ô "hoàn thành công việc")</i></span></label>
                                        <div class="col-sm-12">
                                            <textarea name="" id="" class="form-control" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary">Lưu lại</button> <label for="">Người nhập văn bản này là: Nguyễn văn A</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>