<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Thống kê văn bản đến theo phòng
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
				<div class="row">
                    <form action="" method="get" class="form-horizontal">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>
                                <div class="col-sm-8">
                                    <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{!empty($ngaynhap)&&($ngaynhap > '2017-01-01')&&($ngaynhap)?(date_select($ngaynhap)): NULL}" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Đến ngày</label>
                                <div class="col-sm-8">
                                    <input type="text" name="ngayden" class="form-control datepic datemask" value="{!empty($ngaynhap)&&($ngayden > '2017-01-01')&&($ngaynhap)?(date_select($ngayden)):NULL}" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary btn-sm">Tìm kiếm</button>
							
							&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-sm" name="xuat_excel" value="xuat_excel">&#8594; Excel</button>
                        </div>
                    </form>
                    </div>
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th rowspan="2" class="text-center" width="5%">STT</th>
                                <th rowspan="2" class="text-center" width="15%">Họ và tên</th>
                                <th rowspan="2" class="text-center" width="10%">1. Tổng số VB đến</th>
                                <th colspan="4" class="text-center" width="30%">2. Đã giải quyết</th>
                                <th colspan="3" class="text-center" width="25%">3. Chưa giải quyết</th>
                            </tr>
                            <tr>
                                <th class="text-center">Tổng</th>
                                <th class="text-center">Đúng hạn</th>
                                <th class="text-center">Quá hạn</th>
                                <th class="text-center">Sáng tạo</th>
                                <th class="text-center">Tổng</th>
                                <th class="text-center">Trong hạn</th>
                                <th class="text-center">Quá hạn</th>
                            </tr>
                        </thead>
                        <tbody>
						{$tong1 = 0}{$tong2 = 0}{$tong3 = 0}{$tong4 = 0}
						{$tong5 = 0}{$tong6 = 0}{$tong7 = 0}{$tong8 = 0}
                        {if !empty($phongban)}{$i=1}
                            {foreach $phongban as $pb}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td><a href="{$url}baocaodauvieccanhan?tp={$pb.PK_iMaPB}{if !empty($id)}&id={$id}{/if}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin"><b>{$pb.sTenPB}</b></a></td>
                                <td class="text-center">{if isset($mang_tongdauviec[$pb.PK_iMaPB])} {$mang_dagiaiquyet_quahan[$pb.PK_iMaPB]+$mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]+$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]+$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}								
								{$tong1 = $tong1 + $mang_dagiaiquyet_quahan[$pb.PK_iMaPB]+$mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]+$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]+$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}
								{else}0{/if}</td>
                                <td class="text-center">{if isset($mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]) && isset($mang_dagiaiquyet_quahan[$pb.PK_iMaPB])}{$mang_dagiaiquyet_quahan[$pb.PK_iMaPB]+$mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]}
								{$tong2 = $tong2 + $mang_dagiaiquyet_quahan[$pb.PK_iMaPB]+$mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]}{elseif isset($mang_dagiaiquyet_quahan[$pb.PK_iMaPB])}{$mang_dagiaiquyet_quahan[$pb.PK_iMaPB]}{$tong2 = $tong2 + $mang_dagiaiquyet_quahan[$pb.PK_iMaPB]}{elseif isset($mang_dagiaiquyet_dunghan[$pb.PK_iMaPB])}{$mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]}{$tong2 = $tong2+ $mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]}{else}0{/if}</td>
                                <td class="text-center">{if isset($mang_dagiaiquyet_dunghan[$pb.PK_iMaPB])}{$mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]}{$tong3 = $tong3 + $mang_dagiaiquyet_dunghan[$pb.PK_iMaPB]}{else}0{/if}</td>
                                <td class="text-center">{if isset($mang_dagiaiquyet_quahan[$pb.PK_iMaPB])}{$mang_dagiaiquyet_quahan[$pb.PK_iMaPB]}{$tong4 = $tong4 + $mang_dagiaiquyet_quahan[$pb.PK_iMaPB]}{else}0{/if}</td>
                                <td class="text-center">{if isset($mang_sangtao[$pb.PK_iMaPB])}{$mang_sangtao[$pb.PK_iMaPB]}{$tong5 = $tong5 + $mang_sangtao[$pb.PK_iMaPB]}{else}0{/if}</td>
                                <td class="text-center">{if isset($mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]) && isset($mang_chuagiaiquyet_quahan[$pb.PK_iMaPB])}{$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]+$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}{$tong6 = $tong6+$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]+$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}{elseif isset($mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB])}{$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]}{$tong6=$tong6+$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]}{elseif isset($mang_chuagiaiquyet_quahan[$pb.PK_iMaPB])}{$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}{$tong6=$tong6+$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}{else}0{/if}</td>
                                <td class="text-center">{if isset($mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB])} {$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]}{$tong7=$tong7+$mang_chuagiaiquyet_tronghan[$pb.PK_iMaPB]}{else}0{/if}</td>
                                <td class="text-center">{if isset($mang_chuagiaiquyet_quahan[$pb.PK_iMaPB])}{$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}{$tong8 = $tong8+$mang_chuagiaiquyet_quahan[$pb.PK_iMaPB]}{else}0{/if}</td>
                              
                            </tr>
                            {/foreach}
                        {/if}
						
							<tr>
                                <td class="text-center">{$i++}</td>
                                <td>Tổng</td>
                                <td class="text-center">{$tong1}</td>
								<td class="text-center">{$tong2}</td>
								<td class="text-center">{$tong3}</td>
								<td class="text-center">{$tong4}</td>
								<td class="text-center">{$tong5}</td>
								<td class="text-center">{$tong6}</td>
								<td class="text-center">{$tong7}</td>
								<td class="text-center">{$tong8}</td>
                            </tr>
							
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
</script>