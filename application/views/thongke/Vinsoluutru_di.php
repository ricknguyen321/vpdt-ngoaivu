<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        In sổ lưu trữ
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <form action="" class="form-horizontal" method="get">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label">Số đi:</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="soditu" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="{($thongtin)?($thongtin['soditu']):''}" placeholder="Từ">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="sodiden" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="{($thongtin)?($thongtin['sodiden']):''}" placeholder="Đến">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-2" style="padding:0px" >
                                                <label for="" class="control-label" >Ngày nhập:</label>
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" name="ngaytu" value="{($thongtin)?($thongtin['ngaytu']):''}" class="form-control datepic datemask" placeholder="Từ">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" name="ngayden" value="{($thongtin)?($thongtin['ngayden']):''}" class="form-control datepic datemask" placeholder="Đến">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label">Loại sổ:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select name="loaiso" style="width:100%" id="" class="form-control select2">
                                                    <option value="0">Sổ thường</option>
                                                    <option value="1" {($thongtin)?($thongtin['loaiso']==1)?'selected':'':''}>Sổ giấy mời</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-2" style="padding:0px" >
                                                <label for="" class="control-label" >Ngày mời:</label>
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" name="giaymoitu" value="{($thongtin)?($thongtin['giaymoitu']):''}" class="form-control datepic datemask" placeholder="Từ">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" name="giaymoiden" value="{($thongtin)?($thongtin['giaymoiden']):''}" class="form-control datepic datemask" placeholder="Đến">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 {($vanban['iQuyenHan_DHNB'] != 2 && $vanban['iQuyenHan_DHNB'] != 9)?'hide':''}">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label">Phòng ban:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select name="phongban" style="width:100%" id="" class="form-control select2">
                                                    <option value="">-- Chọn phòng ban --</option>
                                                    {if !empty($dsphongban)}
                                                        {foreach $dsphongban as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {($thongtin)?($thongtin['phongban']==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button class="btn btn-primary" name="loc" value="loc">Lọc</button>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button class="btn btn-primary  pull-right" name="thongkeword" id="thongkeword" value="">Kết xuất word</button>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button class="btn btn-primary  pull-right" name="thongkeexcel" id="thongkeexcel" value="" >Kết xuất excel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                                <table id="" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Số ký hiệu</th>
                                            <th>Ngày ký</th>
                                            <th>Loại văn bản</th>
                                            <th width="30%">Trích yếu</th>
                                            <th>Người ký</th>
                                            <th>Đơn vị ban hành</th>
                                            <th>Nơi nhận</th>
                                            <th class="hide">Người nhập</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($danhsach)}{$i=1}
                                            {foreach $danhsach as $dl}
                                                <tr>
                                                    <td>{$i++}</td>
                                                    <td>{$dl.sKyHieu}</td>
                                                    <td>{date_select($dl.sNgayVBDi)}</td>
                                                    <td>{$dl.sTenLVB}</td>
                                                    <td>{$dl.sMoTa}</td>
                                                    <td>{$dl.sHoTen}</td>
                                                    <td>{$dl.sTenPB}</td>
                                                    <td>{$dl.sNoiNhan}</td>
                                                    <td class="hide"></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                                <div class="pull-right">{$thongtin['phantrang']}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });


    $(document).ready(function() {
        $(document).on('click','button[name=thongkeword]',function(){
            var soditu = $("input[name=soditu]").val();
            var sodiden = $("input[name=sodiden]").val();
            var ngaytu = $("input[name=ngaytu]").val();
            var ngayden = $("input[name=ngayden]").val();
            var loaiso = $("input[name=loaiso]").val();
            var giaymoitu = $("input[name=giaymoitu]").val();
            var giaymoiden = $("input[name=giaymoiden]").val();
            var phongban = $("input[name=phongban]").val();
            var url      = window.location.href+"&ketxuatword" ;
            window.open(url);
        });
        $(document).on('click','button[name=thongkeexcel]',function(){
            var soditu = $("input[name=soditu]").val();
            var sodiden = $("input[name=sodiden]").val();
            var ngaytu = $("input[name=ngaytu]").val();
            var ngayden = $("input[name=ngayden]").val();
            var loaiso = $("input[name=loaiso]").val();
            var giaymoitu = $("input[name=giaymoitu]").val();
            var giaymoiden = $("input[name=giaymoiden]").val();
            var phongban = $("input[name=phongban]").val();
            var url      = window.location.href+"&ketxuatexcel";//alert(url);
            window.open(url);
        });
    });
</script>