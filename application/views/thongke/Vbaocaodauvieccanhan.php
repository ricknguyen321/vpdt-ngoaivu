<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Thống kê văn bản đến theo cá nhân
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <div class="row">
                        <form action="" method="get" class="form-horizontal">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="ngaynhaptu" class="form-control datepic datemask" value="{!empty($ngaynhaptu)&&($ngaynhaptu > '2017-01-01')&&($ngaynhaptu)?(date_select($ngaynhaptu)): NULL}" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Đến ngày</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="ngaynhapden" class="form-control datepic datemask" value="{!empty($ngaynhapden)&&($ngaynhapden > '2017-01-01')&&($ngaynhapden)?(date_select($ngaynhapden)):NULL}" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
								<input type="text" name="tp" class="hide" value="{$tp}" id="{$tp}">
                                <button type="submit" class="btn btn-primary btn-sm">Tìm kiếm</button>
								
									&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-sm" name="xuat_excel" value="xuat_excel">&#8594; Excel</button>
								
                            </div>
                        </form>
                    </div>
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th rowspan="2" class="text-center" width="5%">STT</th>
                            <th rowspan="2" class="text-center" width="15%">Họ và tên</th>
                            <th rowspan="2" class="text-center" width="10%">1. Tổng số VB đến</th>
                            <th colspan="4" class="text-center" width="30%">2. Đã giải quyết</th>
                            <th colspan="3" class="text-center" width="25%">3. Chưa giải quyết</th>
                        </tr>
                        <tr>
                            <th class="text-center">Tổng</th>
                            <th class="text-center">Đúng hạn</th>
                            <th class="text-center">Quá hạn</th>
                            <th class="text-center">Sáng tạo</th>
                            <th class="text-center">Tổng</th>
                            <th class="text-center">Trong hạn</th>
                            <th class="text-center">Quá hạn</th>
                        </tr>
                        </thead>
                        <tbody>
                        {if !empty($canbo)}{$i=0}
                        {foreach $canbo as $cb}
                        {if $cb.iQuyenHan_DHNB==6 || $cb.iQuyenHan_DHNB==3}
                        <!--<tr>
                            <td class="text-center">{$i++}</td>
							
                            <td>{$cb.sHoTen}</td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
							<td class="text-center">-</td>
							
                            <td class="text-center">{if isset($tongdauviecTP['tong'])}<a href="{$url}danhsachchitiet?tp={if !empty($tp)}{$tp}{else}{$vanban['FK_iMaPhongHD']}{/if}{if !empty($id)}&idld={$id}{/if}{if !empty($idld)}{/if}{if !empty($ngaynhaptu)}&ngaynhaptu={date_select($ngaynhaptu)}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$dagiaiquyet_dunghan_TP['tong']+$dagiaiquyet_quahan_TP['tong']+$chuagiaiquyet_tronghan_TP['tong']+$chuagiaiquyet_quahan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($dagiaiquyet_dunghan_TP['tong']) && isset($dagiaiquyet_quahan_TP['tong'])}<a href="javascript:void(0)" class="tin1">{$dagiaiquyet_dunghan_TP['tong']+$dagiaiquyet_quahan_TP['tong']}</a>{elseif isset($dagiaiquyet_dunghan_TP['tong'])}<a href="" class="tin">{$dagiaiquyet_dunghan_TP['tong']}</a>{elseif isset($dagiaiquyet_quahan_TP['tong'])}<a href="" class="tin">{$dagiaiquyet_quahan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($dagiaiquyet_dunghan_TP['tong'])}<a href="{$url}danhsachchitiet?tp={if !empty($tp)}{$tp}{else}{$vanban['FK_iMaPhongHD']}{/if}{if !empty($id)}&idld={$id}{/if}&dagiaiquyet=1{if !empty($ngaynhaptu)}&ngaynhaptu={date_select($ngaynhaptu)}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$dagiaiquyet_dunghan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($dagiaiquyet_quahan_TP['tong'])}<a href="{$url}danhsachchitiet?tp={if !empty($tp)}{$tp}{else}{$vanban['FK_iMaPhongHD']}{/if}{if !empty($id)}&idld={$id}{/if}&dagiaiquyet=0{if !empty($ngaynhaptu)}&ngaynhaptu={date_select($ngaynhaptu)}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$dagiaiquyet_quahan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">0</td>
							
                            <td class="text-center">{if isset($chuagiaiquyet_tronghan_TP['tong']) && isset($chuagiaiquyet_quahan_TP['tong'])}<a href="javascript:void(0)" class="tin1">{$chuagiaiquyet_tronghan_TP['tong']+$chuagiaiquyet_quahan_TP['tong']}</a>{elseif isset($chuagiaiquyet_tronghan_TP['tong'])}<a href="" class="tin">{$chuagiaiquyet_tronghan_TP['tong']}</a>{elseif isset($chuagiaiquyet_quahan_TP['tong'])}<a href="" class="tin">{$chuagiaiquyet_quahan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($chuagiaiquyet_tronghan_TP['tong'])}<a href="{$url}danhsachchitiet?tp={if !empty($tp)}{$tp}{else}{$vanban['FK_iMaPhongHD']}{/if}{if !empty($id)}&idld={$id}{/if}&chuagiaiquyet=1{if !empty($ngaynhaptu)}&ngaynhaptu={date_select($ngaynhaptu)}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$chuagiaiquyet_tronghan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if !empty($chuagiaiquyet_quahan_TP['tong'])}<a href="{$url}danhsachchitiet?tp={if !empty($tp)}{$tp}{else}{$vanban['FK_iMaPhongHD']}{/if}{if !empty($id)}&idld={$id}{/if}&chuagiaiquyet=0{if !empty($ngaynhaptu)}&ngaynhaptu={date_select($ngaynhaptu)}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$chuagiaiquyet_quahan_TP['tong']}</a>{else}0{/if}</td>
							

                        </tr>-->
						
                        {else}{/if}
                        {if empty($id)}
                        <tr>
                            <td class="text-center">{$i++}</td>
							
                            <td>{$cb.sHoTen}</td>
							
                            <td class="text-center">{if isset($mang_tongdauviec[$cb.PK_iMaCB])} {$mang_dagiaiquyet_quahan[$cb.PK_iMaCB]+$mang_dagiaiquyet_dunghan[$cb.PK_iMaCB]+$mang_chuagiaiquyet_tronghan[$cb.PK_iMaCB]+$mang_chuagiaiquyet_quahan[$cb.PK_iMaCB]}{else}0{/if}</td>
							
                            <td class="text-center">{if isset($mang_dagiaiquyet_dunghan[$cb.PK_iMaCB]) && isset($mang_dagiaiquyet_quahan[$cb.PK_iMaCB])}{$mang_dagiaiquyet_quahan[$cb.PK_iMaCB]+$mang_dagiaiquyet_dunghan[$cb.PK_iMaCB]}</a>{elseif isset($mang_dagiaiquyet_quahan[$cb.PK_iMaCB])}{$mang_dagiaiquyet_quahan[$cb.PK_iMaCB]}{elseif isset($mang_dagiaiquyet_dunghan[$cb.PK_iMaCB])}{$mang_dagiaiquyet_dunghan[$cb.PK_iMaCB]}{else}0{/if}</td>
							
                            <td class="text-center">{if isset($mang_dagiaiquyet_dunghan[$cb.PK_iMaCB])}<a href="{$url}danhsachchitiet?id={$cb.PK_iMaCB}&dagiaiquyet=1{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$mang_dagiaiquyet_dunghan[$cb.PK_iMaCB]}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($mang_dagiaiquyet_quahan[$cb.PK_iMaCB])}<a href="{$url}danhsachchitiet?id={$cb.PK_iMaCB}&dagiaiquyet=0{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$mang_dagiaiquyet_quahan[$cb.PK_iMaCB]}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($mang_sangtao[$cb.PK_iMaCB])}<a href="{$url}danhsachchitiet?id={$cb.PK_iMaCB}&sangtao=1{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$mang_sangtao[$cb.PK_iMaCB]}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($mang_chuagiaiquyet_tronghan[$cb.PK_iMaCB]) && isset($mang_chuagiaiquyet_quahan[$cb.PK_iMaCB])}{$mang_chuagiaiquyet_tronghan[$cb.PK_iMaCB]+$mang_chuagiaiquyet_quahan[$cb.PK_iMaCB]}{elseif isset($mang_chuagiaiquyet_tronghan[$cb.PK_iMaCB])}{$mang_chuagiaiquyet_tronghan[$cb.PK_iMaCB]}{elseif isset($mang_chuagiaiquyet_quahan[$cb.PK_iMaCB])}{$mang_chuagiaiquyet_quahan[$cb.PK_iMaCB]}{else}0{/if}</td>
							
                            <td class="text-center">{if isset($mang_chuagiaiquyet_tronghan[$cb.PK_iMaCB])} <a href="{$url}danhsachchitiet?id={$cb.PK_iMaCB}&chuagiaiquyet=1{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$mang_chuagiaiquyet_tronghan[$cb.PK_iMaCB]}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($mang_chuagiaiquyet_quahan[$cb.PK_iMaCB])}<a href="{$url}danhsachchitiet?id={$cb.PK_iMaCB}&chuagiaiquyet=0{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$mang_chuagiaiquyet_quahan[$cb.PK_iMaCB]}</a>{else}0{/if}</td>

                        </tr>
                        {/if}
                        
                        {/foreach}
                        {/if}
						
						<tr>
                            <td class="text-center"></td>
							
                            <td><B>TỔNG</B></td>
							
                            <td class="text-center">{if isset($tongdauviecTP['tong'])}{$dagiaiquyet_dunghan_TP['tong']+$dagiaiquyet_quahan_TP['tong']+$chuagiaiquyet_tronghan_TP['tong']+$chuagiaiquyet_quahan_TP['tong']}{else}0{/if}</td>
							
                            <td class="text-center">{if isset($dagiaiquyet_dunghan_TP['tong']) && isset($dagiaiquyet_quahan_TP['tong'])}{$dagiaiquyet_dunghan_TP['tong']+$dagiaiquyet_quahan_TP['tong']}</a>{elseif isset($dagiaiquyet_dunghan_TP['tong'])}{$dagiaiquyet_dunghan_TP['tong']}{elseif isset($dagiaiquyet_quahan_TP['tong'])}{$dagiaiquyet_quahan_TP['tong']}{else}0{/if}</td>
							
                            <td class="text-center">{if isset($dagiaiquyet_dunghan_TP['tong'])}<a href="{$url}danhsachchitiet?pb={$tp}&dagiaiquyet=1{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$dagiaiquyet_dunghan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if isset($dagiaiquyet_quahan_TP['tong'])}<a href="{$url}danhsachchitiet?pb={$tp}&dagiaiquyet=0{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$dagiaiquyet_quahan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">0</td>
							
                            <td class="text-center">{if isset($chuagiaiquyet_tronghan_TP['tong']) && isset($chuagiaiquyet_quahan_TP['tong'])}{$chuagiaiquyet_tronghan_TP['tong']+$chuagiaiquyet_quahan_TP['tong']}{elseif isset($chuagiaiquyet_tronghan_TP['tong'])}{$chuagiaiquyet_tronghan_TP['tong']}{elseif isset($chuagiaiquyet_quahan_TP['tong'])}{$chuagiaiquyet_quahan_TP['tong']}{else}0{/if}</td>
							
                            <td class="text-center">{if isset($chuagiaiquyet_tronghan_TP['tong'])}<a href="{$url}danhsachchitiet?pb={$tp}&chuagiaiquyet=1{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$chuagiaiquyet_tronghan_TP['tong']}</a>{else}0{/if}</td>
							
                            <td class="text-center">{if !empty($chuagiaiquyet_quahan_TP['tong'])}<a href="{$url}danhsachchitiet?pb={$tp}&chuagiaiquyet=0{if !empty($ngaynhaptu)}&ngaynhaptu={str_replace("/","%2F",date_select($ngaynhaptu))}{/if}{if !empty($ngaynhapden)}&ngaynhapden={str_replace("/","%2F",date_select($ngaynhapden))}{/if}" class="tin">{$chuagiaiquyet_quahan_TP['tong']}</a>{else}0{/if}</td>

                        </tr>
						
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
</script>