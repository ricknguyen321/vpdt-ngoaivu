<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="30%">{($phogiamdoc)?'Phó giám đốc':'Phòng ban'}</th>
                                    <th class="external-event bg-green">Tổng đang xử lý</th>
                                    <th class="external-event bg-aqua">Đang xử lý đúng hạn</th>
                                    <th class="external-event bg-red">Đang xử lý quá hạn</th>
                                    <th class="external-event bg-green">Tổng đã xử lý</th>
                                    <th class="external-event bg-aqua">Đã xử lý đúng hạn</th>
                                    <th class="external-event bg-red">Đã xử lý quá hạn</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">{if !empty($phogiamdoc)}{$phogiamdoc[0]['sHoTen']}{/if}{if !empty($phongban)}{$phongban[0]['sTenPB']}{/if}</td>
                                    <td class="text-center">{$chualam_quahan+$chualam_tronghan}</td>
                                    <td class="text-center"><a target="_blank" href="{$url}danhsachvanban?phongban={$dl_phongban}&phogiamdoc={$dl_phogiamdoc}&giamdoc={$dl_giamdoc}&chucnang=layVB_ChuaLam_TrongHan&chucnangdem=demVB_ChuaLam_TrongHan">{$chualam_tronghan}</a></td>
                                    <td class="text-center"><a target="_blank" href="{$url}danhsachvanban?phongban={$dl_phongban}&phogiamdoc={$dl_phogiamdoc}&giamdoc={$dl_giamdoc}&chucnang=layVB_ChuaLam_QuaHan&chucnangdem=demVB_ChuaLam_QuaHan">{$chualam_quahan}</a></td>
                                    <td class="text-center">{$dalam_tronghan+$dalam_quahan}</td>
                                    <td class="text-center"><a target="_blank" href="{$url}danhsachvanban?phongban={$dl_phongban}&phogiamdoc={$dl_phogiamdoc}&giamdoc={$dl_giamdoc}&chucnang=layVB_DaLam_TrongHan&chucnangdem=demVB_DaLam_TrongHan">{$dalam_tronghan}</a></td>
                                    <td class="text-center"><a target="_blank" href="{$url}danhsachvanban?phongban={$dl_phongban}&phogiamdoc={$dl_phogiamdoc}&giamdoc={$dl_giamdoc}&chucnang=layVB_DaLam_QuaHan&chucnangdem=demVB_DaLam_QuaHan">{$dalam_quahan}</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>