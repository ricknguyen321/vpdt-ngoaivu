<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                        <table id="" class="table table-bordered table-hover">
                            <tbody>
                                {if !empty($danhsach)}
                                    {foreach $danhsach as $dl}
                                        <tr>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{date_select($dl.sNgayNhan)}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$dl.iSoDen}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$dl.sTenDV}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$dl.sKyHieu}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{date_select($dl.sNgayKy)}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$dl.sTenLVB}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$dl.sMoTa}</td>
                                            
                                            {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9}
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$dl.sHoTen}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$dl.sTenPB}</td>
                                            <td style="border-top: 1px solid #000;border-left: 1px solid #000; text-align: center;border-right: 1px solid #000;">{$dl.nguoigiaiquyet}</td>
                                            {else}
                                            <td class="text-center">{($dl.sHanGiaiQuyet >'1970-01-01' && !empty($dl.sHanGiaiQuyet))?date_select($dl.sHanGiaiQuyet):'-'}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;">{($dl.iTrangThai==1)?'Đã giải quyết':'Đang xử lý'}</td>
                                            <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-left: 1px solid #000;text-align: center;border-right: 1px solid #000;">{if $vanban['iQuyenHan_DHNB']==8}{$vanban['sHoTen']}{else}{$dl.nguoigiaiquyet}{/if}</td>
                                            {/if}
                                        </tr>
                                    {/foreach}
                                    <tr><td colspan="10" style="border-top: 1px solid #000; "></td></tr>
                                {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">
                            {($thongtin)?$thongtin['phantrang']:''}
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>