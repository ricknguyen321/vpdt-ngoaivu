<table>
    <thead>
        <tr>
            <th style="border-top: 0.5pt solid #000; width: 40pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: center; vertical-align:middle">STT</th>
            <th style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: center; vertical-align:middle">Số ký hiệu</th>
            <th style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: center; vertical-align:middle">Ngày ký</th>
            <th style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: center; vertical-align:middle">Loại văn bản</th>
            <th style="border-top: 0.5pt solid #000; width: 400pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: center; vertical-align:middle">Trích yếu</th>
            <th style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: center; vertical-align:middle">Người ký</th>
            <th style="border-top: 0.5pt solid #000;border-right: 0.5pt solid #000;  width: 160pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: center; vertical-align:middle">Đơn vị ban hành</th>                                            
        </tr>
    </thead>
    <tbody>
        {if !empty($danhsach)}{$i=1}
            {foreach $danhsach as $dl}
                <tr>
                    <td style="border-top: 0.5pt solid #000; width: 40pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: left; vertical-align:middle;padding-left: 5px">{$i++}</td>
                    <td style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: left; vertical-align:middle;padding-left: 5px">{$dl.sKyHieu}</td>
                    <td style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: left; vertical-align:middle;padding-left: 5px">{date_select($dl.sNgayVBDi)}</td>
                    <td style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: left; vertical-align:middle;padding-left: 5px">{$dl.sTenLVB}</td>
                    <td style="border-top: 0.5pt solid #000; width: 400pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: left; vertical-align:middle;padding-left: 5px">{$dl.sMoTa}</td>
                    <td style="border-top: 0.5pt solid #000; width: 80pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: left; vertical-align:middle;padding-left: 5px">{$dl.sHoTen}</td>
                    <td style="border-top: 0.5pt solid #000;border-right: 0.5pt solid #000; width: 160pt; font-size: 14pt;font-family: 'Times New Roman', Times, serif; border-left: 0.5pt solid #000;text-align: left; vertical-align:middle;padding-left: 5px">{$dl.sTenPB}</td>
                   
                </tr>
            {/foreach}
            <tr><td colspan="7" style="border-top: 0.5pt solid #000;" ></td>
        {/if}
    </tbody>
</table>
                            
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
</script>