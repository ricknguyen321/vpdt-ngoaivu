<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <!--<div class="row">
                    <form action="" method="get">
                        <div class="col-md-4">
                            <select name="malanhdao" id="" style="width: 100%" class="form-control select2">
                                <option value="">-- Chọn phó giám đốc --</option>
                                {if !empty($phogiamdoc)}
                                    {foreach $phogiamdoc as $pgd}
                                        <option value="{$pgd.PK_iMaCB}" {($malanhdao)?($malanhdao==$pgd.PK_iMaCB)?'selected':'':''}>{$pgd.sHoTen}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary btn-xs">Tìm kiếm</button>
                        </div>
                    </form>
                </div>-->
				<br>
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
                        {if !empty($vanbanphogiamdoc)}{$i=1}
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="50%">Phó Giám Đốc</th>
                                    <th>Số lượng văn bản đã chỉ đạo trực tiếp</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $vanbanphogiamdoc as $vbpgd}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td><a target="_blank" href="{$url}thongkevanbantheophong?giamdoc={$taikhoan}&phogiamdoc={$vbpgd.PK_iMaCBNhan}">{$vbpgd.sHoTen}</a></td>
                                        <td>{$vbpgd.tongvanban}</td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                        {/if}
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="50%">Phòng ban</th>
                                    <th>Số lượng văn bản đã chỉ đạo trực tiếp</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($vanbanphongban)}{$i=1}
                                    {foreach $vanbanphongban as $vbpb}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td><a target="_blank" href="{$url}thongkevanbantheophong?phongban={$vbpb.PK_iMaPhongCT}&{$chucvu}={$taikhoan}">{$vbpb.sTenPB}</a></td>
                                            <td>{$vbpb.tongvanban}</td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>