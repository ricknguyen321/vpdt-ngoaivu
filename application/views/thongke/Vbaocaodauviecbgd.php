<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Báo cáo theo đầu công việc theo phong
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <div class="row">
                        <form action="" method="get" class="form-horizontal">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{!empty($ngaynhap)&&($ngaynhap > '2017-01-01')?(date_select($ngaynhap)):''}" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Đến ngày</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="ngayden" class="form-control datepic datemask" value="{!empty($ngayden)&&($ngayden > '2017-01-01')?(date_select($ngayden)):''}" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary btn-sm">Tìm kiếm</button>
                            </div>
                        </form>
                    </div>
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th rowspan="2" class="text-center" width="5%">STT</th>
                            <th rowspan="2" class="text-center" width="15%">Họ và tên</th>
                            <th rowspan="2" class="text-center" width="10%">1. Số việc được giao</th>
                            <th colspan="4" class="text-center" width="30%">2. Đã giải quyết</th>
                            <th colspan="3" class="text-center" width="25%">3. Số việc chưa giải quyết</th>
                        </tr>
                        <tr>
                            <th class="text-center">Tổng số</th>
                            <th class="text-center">Đúng hạn</th>
                            <th class="text-center">Quá hạn</th>
                            <th class="text-center">Có tính sáng tạo</th>
                            <th class="text-center">Tổng số</th>
                            <th class="text-center">Trong hạn</th>
                            <th class="text-center">Quá hạn</th>
                        </tr>
                        </thead>
                        <tbody>
                        {if !empty($phongban)}{$i=1}
                        {foreach $bangiamdoc as $gd}
                        <tr>
                            <td class="text-center">{$i++}</td>
                            <td>{$gd.sHoTen}</td>
                            <td class="text-center">{if isset($mang_tongdauviec_bgd[$gd.PK_iMaCB])}
                                <a href="{$url}baocaodauviecphong?id={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">
                                    {$mang_dagiaiquyet_quahan_bgd[$gd.PK_iMaCB]+$mang_dagiaiquyet_dunghan_bgd[$gd.PK_iMaCB]+$mang_chuagiaiquyet_tronghan_bgd[$gd.PK_iMaCB]+$mang_chuagiaiquyet_quahan_bgd[$gd.PK_iMaCB]}</a>{else}0{/if}</td>
                            <td class="text-center">{if isset($mang_dagiaiquyet_dunghan_bgd[$gd.PK_iMaCB]) && isset($mang_dagiaiquyet_quahan_bgd[$gd.PK_iMaCB])}
                                <a href="{$url}baocaodauviecphong?id={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">
                                    {$mang_dagiaiquyet_quahan_bgd[$gd.PK_iMaCB]+$mang_dagiaiquyet_dunghan_bgd[$gd.PK_iMaCB]}
                                </a>
                                {elseif isset($mang_dagiaiquyet_quahan_bgd[$gd.PK_iMaCB])}
                                <a href="" class="tin">{$mang_dagiaiquyet_quahan_bgd[$gd.PK_iMaCB]}</a>
                                {elseif isset($mang_dagiaiquyet_dunghan_bgd[$gd.PK_iMaCB])}
                                <a href="" class="tin">{$mang_dagiaiquyet_dunghan_bgd[$gd.PK_iMaCB]}</a>
                                {else}0{/if}</td>
                            <td class="text-center">{if isset($mang_dagiaiquyet_dunghan_bgd[$gd.PK_iMaCB])}<a href="{$url}baocaodauviecphong?id={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">{$mang_dagiaiquyet_dunghan_bgd[$gd.PK_iMaCB]}</a>{else}0{/if}</td>
                            <td class="text-center">{if isset($mang_dagiaiquyet_quahan_bgd[$gd.PK_iMaCB])}<a href="{$url}baocaodauviecphong?id={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">{$mang_dagiaiquyet_quahan_bgd[$gd.PK_iMaCB]}</a>{else}0{/if}</td>
                            <td class="text-center">{if isset($mang_sangtao_bgd[$gd.PK_iMaCB])}<a href="{$url}baocaodauviecphong?id={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">{$mang_sangtao_bgd[$gd.PK_iMaCB]}</a>{else}0{/if}</td>
                            <td class="text-center">{if isset($mang_chuagiaiquyet_tronghan_bgd[$gd.PK_iMaCB]) && isset($mang_chuagiaiquyet_quahan_bgd[$gd.PK_iMaCB])}<a href="{$url}baocaodauvieccanhan?tp={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">{$mang_chuagiaiquyet_tronghan_bgd[$gd.PK_iMaCB]+$mang_chuagiaiquyet_quahan_bgd[$gd.PK_iMaCB]}</a>{elseif isset($mang_chuagiaiquyet_tronghan_bgd[$gd.PK_iMaCB])}<a href="" class="tin">{$mang_chuagiaiquyet_tronghan_bgd[$gd.PK_iMaCB]}</a>{elseif isset($mang_chuagiaiquyet_quahan_bgd[$gd.PK_iMaCB])}<a href="" class="tin">{$mang_chuagiaiquyet_quahan_bgd[$gd.PK_iMaCB]}</a>{else}0{/if}</td>
                            <td class="text-center">{if isset($mang_chuagiaiquyet_tronghan_bgd[$gd.PK_iMaCB])} <a href="{$url}baocaodauviecphong?id={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">{$mang_chuagiaiquyet_tronghan_bgd[$gd.PK_iMaCB]}</a>{else}0{/if}</td>
                            <td class="text-center">{if isset($mang_chuagiaiquyet_quahan_bgd[$gd.PK_iMaCB])}<a href="{$url}baocaodauviecphong?id={$gd.PK_iMaCB}{if !empty($ngaynhap)}&ngaynhap={date_select($ngaynhap)}{/if}{if !empty($ngayden)}&ngayden={date_select($ngayden)}{/if}" class="tin">{$mang_chuagiaiquyet_quahan_bgd[$gd.PK_iMaCB]}</a>{else}0{/if}</td>
                        </tr>
                        {/foreach}
                        {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
</script>