<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        In sổ lưu trữ
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <form action="" class="form-horizontal" autocomplete="off" method="get">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label" >Ngày nhập:</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="ngaynhaptu" value="{($thongtin)?($thongtin['ngaynhaptu']):''}" class="form-control datepic datemask" placeholder="Từ">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="ngaynhapden" value="{($thongtin)?($thongtin['ngaynhapden']):''}" class="form-control datepic datemask" placeholder="Đến">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label">Số đến:</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="soditu" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="{($thongtin)?($thongtin['soditu']):''}" placeholder="Từ">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="sodiden" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="{($thongtin)?($thongtin['sodiden']):''}" placeholder="Đến">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 {($vanban['iQuyenHan_DHNB'] != 2 && $vanban['iQuyenHan_DHNB'] != 9)?'hide':''}">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label">Nơi gửi đến:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="typeahead__container">
                                                    <div class="typeahead__field">
                                                        <span class="typeahead__query">
                                                            <input class="js-typeahead noiguiden a form-control" value="{($thongtin)?$thongtin['noiguiden']:''}" name="noiguiden" type="search" autocomplete="off">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label">Loại văn bản:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="typeahead__container">
                                                    <div class="typeahead__field">
                                                        <span class="typeahead__query">
                                                            <input class="js-typeahead loaivanban form-control" value="{($thongtin)?$thongtin['loaivanban']:''}" name="loaivanban" type="search" autocomplete="off">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 {($vanban['iQuyenHan_DHNB'] != 2 && $vanban['iQuyenHan_DHNB'] != 9)?'hide':''}">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label" >Ngày mời:</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="ngaymoitu" value="{($thongtin)?($thongtin['ngaymoitu']):''}" class="form-control datepic datemask" placeholder="Từ">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="ngaymoiden" value="{($thongtin)?($thongtin['ngaymoiden']):''}" class="form-control datepic datemask" placeholder="Đến">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label" >Lãnh đạo:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select name="lanhdao" style="width:100%" id="" class="form-control select2">
                                                    <option value="">-- Chọn lãnh đạo --</option>
                                                    {if !empty($nguoiky)}
                                                        {foreach $nguoiky as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {($thongtin)?($thongtin['lanhdao']==$nn.PK_iMaCB)?'selected':'':''}>{$nn.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 {($vanban['iQuyenHan_DHNB'] != 2 && $vanban['iQuyenHan_DHNB'] != 9)?'hide':''}">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="control-label" >Phòng ban:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select name="phongban" style="width:100%" id="" class="form-control select2">
                                                    <option value="">-- Chọn phòng ban --</option>
                                                    {if !empty($dsphongban)}
                                                        {foreach $dsphongban as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {($thongtin)?($thongtin['phongban']==$pb.PK_iMaPB)?'selected':'':''} >{$pb.sTenPB}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <button class="btn btn-primary" name="loc" value="loc">Lọc</button>
                                            </div>
                                            <div class="col-md-3">
                                                <button class="btn btn-primary" name="download" value="download">download</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                                <table id="" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Ngày nhận</th>
                                            <th>Số đến</th>
                                            <th>Cơ quan ban hành</th>
                                            <th>Số ký hiệu</th>
                                            <th>Ngày ký</th>
                                            <th>Loại văn bản</th>
                                            <th width="30%">Trích yếu</th>
                                            
                                            {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9}
                                            <th>LĐ chỉ đạo</th>
                                            <th>ĐV giải quyết</th>
                                            <th>CB giải quyết</th>
                                            {else}
                                            <th>Hạn giải quyết</th>
                                            <th>Tình trạng</th>
                                            <th>Trực tiếp giải quyết</th>
                                            {/if}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($danhsach)}
                                            {foreach $danhsach as $dl}
                                                <tr>
                                                    <td>{date_select($dl.sNgayNhan)}</td>
                                                    <td>{$dl.iSoDen}</td>
                                                    <td>{$dl.sTenDV}</td>
                                                    <td>{$dl.sKyHieu}</td>
                                                    <td>{date_select($dl.sNgayKy)}</td>
                                                    <td>{$dl.sTenLVB}</td>
                                                    <td>{$dl.sMoTa}</td>
                                                    
                                                    {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9}
                                                    <td>{$dl.sHoTen}</td>
                                                    <td>{$dl.sTenPB}</td>
                                                    <td>{$dl.nguoigiaiquyet}</td>
                                                    {else}
                                                    <td class="text-center">{($dl.sHanGiaiQuyet >'1970-01-01' && !empty($dl.sHanGiaiQuyet))?date_select($dl.sHanGiaiQuyet):'-'}</td>
                                                    <td>{($dl.iTrangThai==1)?'Đã giải quyết':'Đang xử lý'}</td>
                                                    <td>{if $vanban['iQuyenHan_DHNB']==8}{$vanban['sHoTen']}{else}{$dl.nguoigiaiquyet}{/if}</td>
                                                    {/if}
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                                <div class="pull-right">
                                    {($thongtin)?$thongtin['phantrang']:''}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    var data = {
        "loaivanban": [
            {if !empty($loaivanban)}
                {foreach $loaivanban as $l}
                    "{$l.sTenLVB}",
                {/foreach}
            {/if}
        ],
        "noiguiden": [
            {if !empty($noiguiden)}
                {foreach $noiguiden as $ng}
                    "{$ng.sTenDV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".loaivanban",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            loaivanban: {
                data: data.loaivanban
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".noiguiden",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiguiden: {
                data: data.noiguiden
            }
        },
        debug: false
    });
</script>