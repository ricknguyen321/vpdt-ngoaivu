<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản thống kê theo phòng ban xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th rowspan="3" class="text-center" width="5%">STT</th>
                                <th rowspan="3" class="text-center" width="20%">Tên đơn vị</th>
                                <th colspan="6" class="text-center" width="50%">Văn bản chủ trì</th>
                                <th colspan="3" class="text-center" width="25%">Văn bản phối hợp</th>
                            </tr>
                            <tr>
                                <th colspan="3" class="text-center">Văn bản đang xử lý</th>
                                <th colspan="3" class="text-center">Văn bản đã hoàn thành</th>
                                <th rowspan="2" class="text-center">Tổng</th>
                                <th rowspan="2" class="text-center">Văn bản đang xử lý</th>
                                <th rowspan="2" class="text-center">Văn bản đã hoàn thành</th>
                            </tr>
                            <tr>
                                <th class="text-center">Tổng</th>
                                <th class="text-center">Trong hạn</th>
                                <th class="text-center">Quá hạn</th>
                                <th class="text-center">Tổng</th>
                                <th class="text-center">Trong hạn</th>
                                <th class="text-center">Quá hạn</th>                         
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
</script>