<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        {$title}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Loại văn bản</th>
                                    <th>Số đến</th>
                                    <th>Số ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th>Nơi gửi</th>
                                    <th>Hạn văn bản</th>
                                    <th>Ngày giải quyết</th>
                            </thead>
                            <tbody>
                                {if !empty($danhsach)}{$i=1}
                                    {foreach $danhsach as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{$dl.sTenLVB}</td>
                                            <td class="text-center" style="color:red; font-size: 18px; font-weight: bold;">{$dl.iSoDen}</td>
                                            <td class="text-center">{$dl.sKyHieu}</td>
                                            <td><a {if $dl.iGiayMoi==2} href="{$url}quatrinhxuly/{$dl.PK_iMaVBDen}" {else} href="{$url}quytrinhgiaymoi/{$dl.PK_iMaVBDen}" {/if} style="color: #0c0b0b"><b>{$dl.sMoTa}</b></a>
                                                <p>Người nhập:{$hotenvanthu[$dl.FK_iMaNguoiNhap]}</p>
                                                <p>Ngày nhập: {date_time($dl.sNgayNhap)}</p>
                                            </td>
                                            <td>{$dl.sTenDV}</td>
                                            <td class="text-center">{if !empty($dl.sHanThongKe) && $dl.sHanThongKe > '1970-01-01'}{date_select($dl.sHanThongKe)}{/if}</td>
                                            <td class="text-center">{if !empty($dl.sNgayGiaiQuyet) && $dl.sNgayGiaiQuyet > '1970-01-01'}{date_select($dl.sNgayGiaiQuyet)}{/if}</td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">{$phantrang}</div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
    });
</script>