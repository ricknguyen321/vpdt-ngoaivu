<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Báo cáo tổng hợp
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <form action="" method="post" class="form-horizontal" autocomplete="off">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="" class="control-label">NGÀY THÁNG:</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-md-6" style="padding-left:0px"><input name="ngaybd" required="" value="{if !empty($ngaybd)}{$ngaybd}{/if}" type="text" placeholder="Từ ngày" class="form-control datepic datemask"></div>
                                            <div class="col-md-6" style="padding-left:0px"><input name="ngaykt" required="" value="{if !empty($ngaykt)}{$ngaykt}{/if}" type="text" placeholder="Đến ngày" class="form-control datepic datemask"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                <input type="checkbox" class="flat-red" name="vbtt" value="1" {if !empty($vbtt)}checked{/if}> <label for="" class="control-label">VĂN BẢN THÔNG THƯỜNG</label> 
                                            </div>
                                            <div class="col-md-6">
                                                <input type="checkbox" class="flat-red" name="vbstc" value="1" {if !empty($vbstc)}checked{/if}> <label for="" class="control-label">VĂN BẢN SNV CHỦ TRÌ</label> 
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="" class="control-label">PHÒNG/BAN:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <select name="phongban" id="" class="form-control select2" {if !empty($required)}{$required}{/if}>
                                                    <option value="">-- {$tenluachon} --</option>
                                                    {if !empty($dsphongban)}
                                                        {foreach $dsphongban as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {($phongban)?($phongban==$pb.PK_iMaPB)?'selected':'':''}>{$pb.sTenPB}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <button class="btn btn-primary btn-sm" type="submit" name="loc" value="loc">Lọc</button>
                                            <button type="submit" class="btn btn-primary btn-sm" name="xuat_excel" value="xuat_excel">Xuất Excel</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12" style="overflow-x:auto;">
                    <table id="" class="table table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2" class="text-center" width="">STT</th>
                                <th rowspan="2" class="text-center width="">Số đến</th>
                                <th rowspan="2" class="text-center" width="">Số SKH</th>
                                <th rowspan="2" class="text-center" width="">Ngày ban hành</th>
                                <th rowspan="2" class="text-center" width="">Nội dung văn bản <br> <i>(Nhấp chuột vào nội dung để xem chi tiết)</i></th>
                                <th colspan="3" class="text-center" width="">Lãnh đạo Sở giao</th>
                                <th rowspan="2" class="text-center" width="">Thời hạn hoàn thành</th>
                                <th rowspan="2" class="text-center" width="">Tình hình triển khai và kết quả đạt được</th>
                                <th colspan="5" class="text-center" width="">Tiến độ</th>
                                <th rowspan="2" class="text-center" width="">Khó khăn, vướng mắc</th>
                                <!-- <th rowspan="2" class="text-center" width="">Đề xuất hướng xử lý</th> -->
                                <th rowspan="2" class="text-center" width="">Ghi Chú</th>
                            </tr>
                            <tr>
                                <th class="text-center">Ngày giao</th>
                                <th class="text-center">Phòng, ban, đơn vị chủ trì</th>
                                <th class="text-center">Phòng, ban, đơn vị phối hợp</th>
                                <th class="text-center">Tổng số</th>
                                <th class="text-center">Đang triển khai (Chưa đến hạn)</th>
                                <th class="text-center">Đang triển khai (Đã quá hạn)</th>
                                <th class="text-center">Hoàn thành (Trong hạn)</th>
                                <th class="text-center">Hoàn thành (Quá hạn)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="17"><b>NHIỆM VỤ HOÀN THÀNH TRONG KỲ</b></td>
                            </tr>
                            {if !empty($dsphongban)}
                                {foreach $dsphongban as $pb}
                                    <tr style="background:#e4e4e4" {if !isset($tonghoanthanh[$pb.PK_iMaPB])} class="hide"{/if}>
                                        <td class="colpan" colspan="10">{$pb.sTenPB}</td>
                                        <td class="text-center">{if isset($tonghoanthanh[$pb.PK_iMaPB])}{$tonghoanthanh[$pb.PK_iMaPB]}{else}0{/if}</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">{if isset($tonghoanthanhdung[$pb.PK_iMaPB])}{$tonghoanthanhdung[$pb.PK_iMaPB]}{else}0{/if}</td>
                                        <td class="text-center">{if isset($tonghoanthanhqua[$pb.PK_iMaPB])}{$tonghoanthanhqua[$pb.PK_iMaPB]}{else}0{/if}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    {if !empty($hoanthanh)}{$i=1}
                                        {foreach $hoanthanh as $dl}
                                            {if $dl.PK_iMaPhongCT == $pb.PK_iMaPB}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td class="text-center">{$dl.iSoDen}</td>
                                                    <td class="text-center">{$dl.sKyHieu}</td>
                                                    <td>{date_select($dl.sNgayKy)}</td>
                                                    <td><a target="_blank" style="color:black" href="{$url}thongtindaura?id={$dl.PK_iMaVBDen}">{$dl.sMoTa} </a><br>
                                                    {if !empty($dl.file)} <a target="_blank" class="tin1" href="{$dl.file.sDuongDan}">Xem file</a>{/if}
                                                    </td>
                                                    <td>{date_select($dl.sThoiGian)}</td>
                                                    <td>{$dl.sVietTat}</td>
                                                    <td>
                                                    {if !empty($dl.PK_iMaPhongPH)} {$dem=0}
                                                        {$mangten = explode(',',$dl.PK_iMaPhongPH)}
                                                        {foreach $mangten as $vt}
                                                        <!-- {$dem++} -->
                                                            {($dem>1)?',':''}
                                                            {$phongviettat[$vt]}
                                                        {/foreach}
                                                    {/if}
                                                    </td>
                                                    <td class="text-center">{($dl.sHanGiaiQuyet >'1970-01-01' && !empty($dl.sHanGiaiQuyet) )?date_select($dl.sHanGiaiQuyet):'-'}</td>
                                                    <td> 
                                                        {if !empty($dl.ketqua)}
                                                        <p>{$dl.ketqua.sMoTa}</p>
                                                        <p>
                                                            {if !empty($dl.ketqua.sDuongDanFile) && $dl.ketqua.sDuongDanFile!='doc_uploads/'}
                                                            <a href="{$url}{$dl.ketqua.sDuongDanFile}" target="_blank" class="tin1">Xem file</a>
                                                            {/if}
                                                        </p>
                                                        {/if}
                                                    </td>
                                                    <td></td>
                                                    <td class="text-center"></td>
                                                    <td class="text-center"></td>
                                                    <td class="text-center">{if $dl.sNgayGiaiQuyet <= $dl.sHanGiaiQuyet || $dl.sHanGiaiQuyet <= "1970-01-01"}x{/if}</td>
                                                    <td class="text-center">{if $dl.sNgayGiaiQuyet > $dl.sHanGiaiQuyet && $dl.sHanGiaiQuyet > '1970-01-01'}x{/if}</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            {/if}
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            {/if}
                            <tr>
                                <td colspan="17"><b>NHIỆM VỤ ĐANG DỞ DANG</b></td>
                            </tr>
                            {if !empty($dsphongban)}
                                {foreach $dsphongban as $pb}
                                    <tr style="background:#e4e4e4" {if !isset($tongchuahoanthanh[$pb.PK_iMaPB])} class="hide"{/if}>
                                        <td class="colpan" colspan="10">{$pb.sTenPB}</td>
                                        <td class="text-center">{if isset($tongchuahoanthanh[$pb.PK_iMaPB])}{$tongchuahoanthanh[$pb.PK_iMaPB]}{else}0{/if}</td>
                                        <td class="text-center">{if isset($tongchuahoanthanhdung[$pb.PK_iMaPB])}{$tongchuahoanthanhdung[$pb.PK_iMaPB]}{else}0{/if}</td>
                                        <td class="text-center">{if isset($tongchuahoanthanhqua[$pb.PK_iMaPB])}{$tongchuahoanthanhqua[$pb.PK_iMaPB]}{else}0{/if}</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    {if !empty($chuahoanthanh)}{$i=1}
                                        {foreach $chuahoanthanh as $dl}
                                            {if $dl.PK_iMaPhongCT == $pb.PK_iMaPB}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td class="text-center">{$dl.iSoDen}</td>
                                                    <td class="text-center">{$dl.sKyHieu}</td>
                                                    <td>{date_select($dl.sNgayKy)}</td>
                                                    <td><a target="_blank" style="color:black" href="{$url}thongtindaura?id={$dl.PK_iMaVBDen}">{$dl.sMoTa} </a><br>
                                                    {if !empty($dl.file)} <a target="_blank" class="tin1" href="{$dl.file.sDuongDan}">Xem file</a>{/if}
                                                    </td>
                                                    <td>{date_select($dl.sThoiGian)}</td>
                                                    <td>{$dl.sVietTat}</td>
                                                    <td>
                                                    {if !empty($dl.PK_iMaPhongPH)} {$dem=0}
                                                        {$mangten = explode(',',$dl.PK_iMaPhongPH)}
                                                        {foreach $mangten as $vt}
                                                        <!-- {$dem++} -->
                                                            {($dem>1)?',':''}
                                                            {$phongviettat[$vt]}
                                                        {/foreach}
                                                    {/if}
                                                    </td>
                                                    <td class="text-center">{($dl.sHanGiaiQuyet >'1970-01-01' && !empty($dl.sHanGiaiQuyet) )?date_select($dl.sHanGiaiQuyet):'-'}</td>
                                                    <td>
                                                        {if !empty($dl.ketqua)}
                                                        <p>{$dl.ketqua.sMoTa}</p>
                                                        <p>
                                                            {if !empty($dl.ketqua.sDuongDanFile) && $dl.ketqua.sDuongDanFile!='doc_uploads/'}
                                                            <a href="{$url}{$dl.ketqua.sDuongDanFile}" target="_blank" class="tin1">Xem file</a>
                                                            {/if}
                                                        </p>
                                                        {/if}
                                                    </td>
                                                    <td></td>
                                                    <td class="text-center">{if $dl.sHanGiaiQuyet <= '1970-01-01' || empty($dl.sHanGiaiQuyet) || $dl.sHanGiaiQuyet >= date_insert($ngaykt)}x{/if}</td>
                                                    <td class="text-center">{if $dl.sHanGiaiQuyet < date_insert($ngaykt)  && $dl.sHanGiaiQuyet > '1970-01-01'}x{/if}</td>
                                                    <td class="text-center"></td>
                                                    <td class="text-center"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            {/if}
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            {/if}
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>