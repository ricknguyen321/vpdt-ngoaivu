<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Báo cáo tổng hợp
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <form action="" method="post" class="form-horizontal" autocomplete="off">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="checkbox" class="minimal" name="all" value="1" checked=""> <label for="" class="text-danger">(1)</label><label for="">TẤT CẢ VĂN BẢN CỦA NĂM</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group vbtt">
                                                <input type="checkbox" class="minimal" name="vbtt" {($thongtin)?($thongtin['vbtt'])?'checked':'':''} value="1"> <label for="" class="text-danger">(1.1)</label><label for="">VĂN BẢN THÔNG THƯỜNG</label>
                                            </div>
                                            <div class="form-group vbstc">
                                                <input type="checkbox" class="minimal" name="vbstc" {($thongtin)?($thongtin['vbstc'])?'checked':'':''} value="1"> <label for="" class="text-danger">(1.2)</label><label for="">VĂN BẢN SNV CHỦ TRÌ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group {($thongtin)?($thongtin['vbtt'])?'':'hide':'hide'} vbtt_tbkt">
                                                <input type="checkbox" class="minimal" name="vbtt_tbkt" {($thongtin)?($thongtin['vbtt_tbkt'])?'checked':'':''} value="1"> <label for="" class="text-danger">(1.1.1)</label><label for="">THÔNG BÁO - KẾT LUẬN</label>
                                            </div>
                                            <div class="form-group {($thongtin)?($thongtin['vbtt'])?'':'hide':'hide'} vbtt_khac">
                                                <input type="checkbox" class="minimal" name="vbtt_khac" {($thongtin)?($thongtin['vbtt_khac'])?'checked':'':''} value="1"> <label for="" class="text-danger">(1.1.2)</label><label for="">VĂN BẢN KHÁC</label>
                                            </div>
                                            <div class="form-group {($thongtin)?($thongtin['vbstc'])?'':'hide':'hide'} vbstc_tbkt">
                                                <input type="checkbox" class="minimal" name="vbstc_tbkt" {($thongtin)?($thongtin['vbstc_tbkt'])?'checked':'':''} value="1"> <label for="" class="text-danger">(1.2.1)</label><label for="">THÔNG BÁO - KẾT LUẬN</label>
                                            </div>
                                            <div class="form-group {($thongtin)?($thongtin['vbstc'])?'':'hide':'hide'} vbstc_khac">
                                                <input type="checkbox" class="minimal" name="vbstc_khac" {($thongtin)?($thongtin['vbstc_khac'])?'checked':'':''} value="1"> <label for="" class="text-danger">(1.2.2)</label><label for="">VĂN BẢN KHÁC</label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="margin-top:0px">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="">DẠNG VĂN BẢN:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="formg-roup">
                                                <input type="checkbox" class="minimal" name="khongthoihan" {($thongtin)?($thongtin['khongthoihan'])?'checked':'':''} value="1"> <label for="">KHÔNG THỜI HẠN (Mặc định 5 ngày giải quyết)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" class="minimal" name="cothoihan" {($thongtin)?($thongtin['cothoihan'])?'checked':'':''} value="1"> <label for="">CÓ THỜI HẠN (Hạn giải quyết theo ý kiến chỉ đạo)</label>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="" class="control-label">NGÀY THÁNG:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-6" style="padding-left:0px"><input name="ngay_ngaybd" value="{($thongtin)?($thongtin['ngay_ngaybd'])?$thongtin['ngay_ngaybd']:'':''}" type="text" placeholder="Từ ngày" class="form-control datepic datemask"></div>
                                            <div class="col-md-6" style="padding-left:0px"><input name="ngay_ngaykt" value="{($thongtin)?($thongtin['ngay_ngaykt'])?$thongtin['ngay_ngaykt']:'':''}" type="text" placeholder="Đến ngày" class="form-control datepic datemask"></div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-2" style="padding:0px;">
                                                <label for="" class="control-label">SỐ ĐẾN:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="col-md-6" style="padding-left:0px"><input name="soden_tu" value="{($thongtin)?($thongtin['soden_tu'])?$thongtin['soden_tu']:'':''}" type="text" placeholder="Từ" class="form-control"></div>
                                                <div class="col-md-6" style="padding-left:0px"><input name="soden_den" value="{($thongtin)?($thongtin['soden_den'])?$thongtin['soden_den']:'':''}" type="text" placeholder="Đến" class="form-control"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="" class="">TÌNH TRẠNG:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="checkbox" class="minimal" name="cgq" {($thongtin)?($thongtin['cgq'])?'checked':'':''} value="1"> <label for=""> CHƯA GIẢI QUYẾT</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group {($thongtin)?($thongtin['cgq'])?'':'hide':'hide'} cgq_cdh">
                                                    <input type="checkbox" class="minimal" name="cgq_cdh" {($thongtin)?($thongtin['cgq_cdh'])?'checked':'':''} value="1"> <label for=""> CHƯA ĐẾN HẠN</label>
                                                </div>
                                                <div class="form-group {($thongtin)?($thongtin['cgq'])?'':'hide':'hide'} cgq_sdh">
                                                    <input type="checkbox" class="minimal" name="cgq_sdh" {($thongtin)?($thongtin['cgq_sdh'])?'checked':'':''} value="1"> <label for=""> SẮP ĐẾN HẠN</label>
                                                </div>
                                                <div class="form-group {($thongtin)?($thongtin['cgq'])?'':'hide':'hide'} cgq_dqh">
                                                    <input type="checkbox" class="minimal" name="cgq_dqh" {($thongtin)?($thongtin['cgq_dqh'])?'checked':'':''} value="1"> <label for=""> ĐÃ QUÁ HẠN</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="checkbox" class="minimal" name="dgq" {($thongtin)?($thongtin['dgq'])?'checked':'':''} value="1"> <label for=""> ĐÃ GIẢI QUYẾT</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group dgq_dth {($thongtin)?($thongtin['dgq'])?'':'hide':'hide'}">
                                                    <input type="checkbox" class="minimal" name="dgq_dth" {($thongtin)?($thongtin['dgq_dth'])?'checked':'':''} value="1"> <label for=""> ĐÚNG THỜI HẠN</label>
                                                </div>
                                                <div class="form-group dgq_qth {($thongtin)?($thongtin['dgq'])?'':'hide':'hide'}">
                                                    <input type="checkbox" class="minimal" name="dgq_qth" {($thongtin)?($thongtin['dgq_qth'])?'checked':'':''} value="1"> <label for=""> ĐÃ QUÁ HẠN</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="margin-top:0px">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="" class="control-label">PHÒNG/BAN:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <select name="phongban" id="" class="form-control select2">
                                                    <option value="">-- {$tenluachon} --</option>
                                                    {if !empty($dsphongban)}
                                                        {foreach $dsphongban as $pb}
                                                            <option value="{$pb.PK_iMaPB}" {($thongtin)?($thongtin['phongban'])?($thongtin['phongban']==$pb.PK_iMaPB)?'selected':'':'':''}>{$pb.sTenPB}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <button class="btn btn-primary btn-sm" type="submit" name="loc" value="loc">Lọc</button>  <button type="submit" class="btn btn-primary btn-sm" name="xuat_excel" value="xuat_excel">Xuất Excel</button> 
                                            <input type="checkbox" class="minimal" name="hiensoden" value="1" {($thongtin)?($thongtin['hiensoden'])?'checked':'':''}><label for="">Hiện số đến</label> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12" style="overflow-x:auto;">
                    <table id="" class="table table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2" class="text-center" width="">STT</th>
                                <th rowspan="2" class="text-center hiensoden {($thongtin)?($thongtin['hiensoden'])?'':'hide':'hide'}" width="">Số đến</th>
                                <th rowspan="2" class="text-center" width="">Số SKH</th>
                                <th rowspan="2" class="text-center" width="">Ngày ban hành</th>
                                <th rowspan="2" class="text-center" width="">Nội dung văn bản <br> <i>(Nhấp chuột vào nội dung để xem chi tiết)</i></th>
                                <th colspan="3" class="text-center" width="">Lãnh đạo Sở giao</th>
                                <th rowspan="2" class="text-center" width="">Thời hạn hoàn thành</th>
                                <th rowspan="2" class="text-center" width="">Tình hình triển khai và kết quả đạt được</th>
                                <th colspan="5" class="text-center" width="">Tiến độ</th>
                                <th rowspan="2" class="text-center" width="">Khó khăn, vướng mắc</th>
                                <th rowspan="2" class="text-center" width="">Đề xuất hướng xử lý</th>
                                <th rowspan="2" class="text-center" width="">Ghi Chú</th>
                            </tr>
                            <tr>
                                <th class="text-center">Ngày giao</th>
                                <th class="text-center">Phòng, ban, đơn vị chủ trì</th>
                                <th class="text-center">Phòng, ban, đơn vị phối hợp</th>
                                <th class="text-center">Tổng số</th>
                                <th class="text-center">Đang triển khai (Chưa đến hạn)</th>
                                <th class="text-center">Đang triển khai (Đã quá hạn)</th>
                                <th class="text-center">Hoàn thành (Trong hạn)</th>
                                <th class="text-center">Hoàn thành (Quá hạn)</th>
                            </tr>
                        </thead>
                        <tbody>
                            {if $vanban['iQuyenHan_DHNB']==2 || $vanban['iQuyenHan_DHNB']==9 || $vanban['iQuyenHan_DHNB']==3|| $vanban['iQuyenHan_DHNB']==4|| $vanban['iQuyenHan_DHNB']==5}
                                {if !empty($dsphongban)}
                                    {foreach $dsphongban as $pb}
                                        <tr style="background:#e4e4e4" class="{if $pb.tong==0}hide{/if}">
                                            <td class="colpan" colspan="{($thongtin)?($thongtin['hiensoden'])?'10':'9':'9'}">{$pb.sTenPB}</td>
                                            <td class="text-center">{$pb.tong}</td>
                                            <td class="text-center">{if isset($pb.danggiaiquyet_tronghan)}{$pb.danggiaiquyet_tronghan}{else}0{/if}</td>
                                            <td class="text-center">{if isset($pb.danggiaiquyet_quahan)}{$pb.danggiaiquyet_quahan}{else}0{/if}</td>
                                            <td class="text-center">{if isset($pb.dagiaiquyet_tronghan)}{$pb.dagiaiquyet_tronghan}{else}0{/if}</td>
                                            <td class="text-center">{if isset($pb.dagiaiquyet_quahan)}{$pb.dagiaiquyet_quahan}{else}0{/if}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                {if $dl.PK_iMaPhongCT == $pb.PK_iMaPB}
                                                    <tr>
                                                        <td class="text-center">{$i++}</td>
                                                        <td class="text-center {($thongtin)?($thongtin['hiensoden'])?'':'hide':'hide'} hiensoden">{$dl.iSoDen}</td>
                                                        <td class="text-center">{$dl.sKyHieu} <br><i>({$dl.iSoDen})</i></td>
                                                        <td>{date_select($dl.sNgayKy)}</td>
                                                        <td><a target="_blank" style="color:black" href="{$url}thongtindaura?id={$dl.PK_iMaVBDen}">{$dl.sMoTa} </a><br>
                                                        {if !empty($dl.sDuongDan)} <a target="_blank" class="tin1" href="{$dl.sDuongDan}">Xem file</a>{/if}
                                                        </td>
                                                        <td>{date_select($dl.sThoiGian)}</td>
                                                        <td>{$dl.sVietTat}</td>
                                                        <td>
                                                        {if !empty($dl.phongphoihop)} {$dem=0}
                                                            {foreach $dl.phongphoihop as $ph}
                                                            <!-- {$dem++} -->
                                                                {($dem>1)?',':''}
                                                                {$ph.sVietTat}
                                                            {/foreach}
                                                        {/if}
                                                        </td>
                                                        <td class="text-center">{($dl.sHanGiaiQuyet >'1970-01-01' && !empty($dl.sHanGiaiQuyet) )?date_select($dl.sHanGiaiQuyet):'-'}</td>
                                                        <td>
                                                        {$dl.sGhiChu}
                                                        <p class="tin1">{if isset($mangVBDI[$dl.iSoDen])}(Văn bản trả lời: 
                                                            <a class="tin1" target="_blank" href="{$url}{$mangVBDI[$dl.iSoDen][1]}">{$mangVBDI[$dl.iSoDen][0]}</a>
                                                            {if isset($mangVBDI[$dl.iSoDen][2])}, <a class="tin1" target="_blank" href="{$url}{$mangVBDI[$dl.iSoDen][3]}">{$mangVBDI[$dl.iSoDen][2]}</a>{/if}
                                                            {if isset($mangVBDI[$dl.iSoDen][4])}, <a class="tin1" target="_blank" href="{$url}{$mangVBDI[$dl.iSoDen][5]}">{$mangVBDI[$dl.iSoDen][4]}</a>{/if}){/if}</p>
                                                       <!--  {if !empty($dl.duongdan)}<br>
                                                            {if !empty($dl.duongdan.sDuongDan)}
                                                            (Văn bản trả lời:<a target="_blank" href="{$dl.duongdan.sDuongDan}">{$dl.duongdan.sTenFile}</a>)
                                                            {/if}
                                                        {/if} -->
                                                        </td>
                                                        <td></td>
                                                        <td class="text-center">{if $dl.iTrangThai==0 && $dl.sHanGiaiQuyet >= date('Y-m-d')}x{/if}</td>
                                                        <td class="text-center">{if $dl.iTrangThai==0 && $dl.sHanGiaiQuyet < date('Y-m-d')}x{/if}</td>
                                                        <td class="text-center">{if $dl.iTrangThai==1 && ($dl.sHanGiaiQuyet >= $dl.sNgayGiaiQuyet || $dl.sHanGiaiQuyet <= '1970-01-01')}x{/if}</td>
                                                        <td class="text-center">{if $dl.iTrangThai==1 && $dl.sHanGiaiQuyet > '1970-01-01' && $dl.sHanGiaiQuyet < $dl.sNgayGiaiQuyet}x{/if}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                {/if}
                            {else}
                                {if !empty($dsdulieu)}{$i=1}
                                    <tr style="background:#e4e4e4">
                                        <td class="colpan" colspan="{($thongtin)?($thongtin['hiensoden'])?'10':'9':'9'}">{$vanban['sTenPB']}</td>
                                        <td class="text-center">{$pb.tong}</td>
                                        <td class="text-center">{if isset($pb.danggiaiquyet_tronghan)}{$pb.danggiaiquyet_tronghan}{else}0{/if}</td>
                                        <td class="text-center">{if isset($pb.danggiaiquyet_quahan)}{$pb.danggiaiquyet_quahan}{else}0{/if}</td>
                                        <td class="text-center">{if isset($pb.dagiaiquyet_tronghan)}{$pb.dagiaiquyet_tronghan}{else}0{/if}</td>
                                        <td class="text-center">{if isset($pb.dagiaiquyet_quahan)}{$pb.dagiaiquyet_quahan}{else}0{/if}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    {foreach $dsdulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center {($thongtin)?($thongtin['hiensoden'])?'':'hide':'hide'} hiensoden">{$dl.iSoDen}</td>
                                            <td class="text-center">{$dl.sKyHieu} <br><i>({$dl.iSoDen})</i></td>
                                            <td>{date_select($dl.sNgayKy)}</td>
                                            <td><a target="_blank" style="color:black" href="{$url}thongtindaura?id={$dl.PK_iMaVBDen}">{$dl.sMoTa} </a><br>
                                            {if !empty($dl.sDuongDan)} <a target="_blank" class="tin1" href="{$dl.sDuongDan}">Xem file</a>{/if}
                                            </td>
                                            <td>{date_select($dl.sThoiGian)}</td>
                                            <td>{$dl.sVietTat}</td>
                                            <td>
                                            {if !empty($dl.phongphoihop)} {$dem=0}
                                                {foreach $dl.phongphoihop as $ph}
                                                <!-- {$dem++} -->
                                                    {($dem>1)?',':''}
                                                    {$ph.sVietTat}
                                                {/foreach}
                                            {/if}
                                            </td>
                                            <td class="text-center">{($dl.sHanGiaiQuyet >'1970-01-01' && !empty($dl.sHanGiaiQuyet) )?date_select($dl.sHanGiaiQuyet):'-'}</td>
                                            <td>
                                            {$dl.sGhiChu}
                                            <p class="tin1">{if isset($mangVBDI[$dl.iSoDen])}(Văn bản trả lời: 
                                                            <a class="tin1" target="_blank" href="{$url}{$mangVBDI[$dl.iSoDen][1]}">{$mangVBDI[$dl.iSoDen][0]}</a>
                                                            {if isset($mangVBDI[$dl.iSoDen][2])}, <a class="tin1" target="_blank" href="{$url}{$mangVBDI[$dl.iSoDen][3]}">{$mangVBDI[$dl.iSoDen][2]}</a>{/if}
                                                            {if isset($mangVBDI[$dl.iSoDen][4])}, <a class="tin1" target="_blank" href="{$url}{$mangVBDI[$dl.iSoDen][5]}">{$mangVBDI[$dl.iSoDen][4]}</a>{/if}){/if}</p>
                                            <!-- {if !empty($dl.duongdan)}<br>
                                                {if !empty($dl.duongdan.sDuongDan)}
                                                (Văn bản trả lời:<a target="_blank" href="{$dl.duongdan.sDuongDan}">{$dl.duongdan.sTenFile}</a>)
                                                {/if}
                                            {/if} -->
                                            </td>
                                            <td></td>
                                            <td class="text-center">{if $dl.iTrangThai==0 && $dl.sHanGiaiQuyet >= date('Y-m-d')}x{/if}</td>
                                            <td class="text-center">{if $dl.iTrangThai==0 && $dl.sHanGiaiQuyet < date('Y-m-d')}x{/if}</td>
                                            <td class="text-center">{if $dl.iTrangThai==1 && $dl.sHanGiaiQuyet >= $dl.sNgayGiaiQuyet}x{/if}</td>
                                            <td class="text-center">{if $dl.iTrangThai==1 && $dl.sHanGiaiQuyet < $dl.sNgayGiaiQuyet}x{/if}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            {/if}
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('input[name=hiensoden]').on('ifChanged', function (event){ 
            var hiensoden = $('input[name=hiensoden]:checked').length;
            if(hiensoden==0)
            {
                $('.hiensoden').addClass('hide');
                $('.colpan').attr('colspan', '9');
            }
            else{ 
                $('.hiensoden').removeClass('hide');
                $('.colpan').attr('colspan', '10');
            }
        });
        $('input[name=all]').on('ifChanged', function (event){ 
            var all = $('input[name=all]:checked').length;
            if(all==0)
            {
                $('.vbtt').addClass('hide');
                $('.vbstc').addClass('hide');
            }
            else{
                $('.vbtt').removeClass('hide');
                $('.vbstc').removeClass('hide');
            }
        });
        $('input[name=vbtt]').on('ifChanged', function (event){ 
            var vbtt = $('input[name=vbtt]:checked').length;
            if(vbtt==0)
            {
                $('.vbtt_tbkt').addClass('hide');
                $('.vbtt_khac').addClass('hide');
            }
            else{
                $('.vbtt_tbkt').removeClass('hide');
                $('.vbtt_khac').removeClass('hide');
            }
        });
        $('input[name=vbstc]').on('ifChanged', function (event){ 
            var vbSNV = $('input[name=vbstc]:checked').length;
            if(vbstc==0)
            {
                $('.vbstc_tbkt').addClass('hide');
                $('.vbstc_khac').addClass('hide');
            }
            else{
                $('.vbstc_tbkt').removeClass('hide');
                $('.vbstc_khac').removeClass('hide');
            }
        });
        $('input[name=cgq]').on('ifChanged', function (event){ 
            var cgq = $('input[name=cgq]:checked').length;
            if(cgq==0)
            {
                $('.cgq_cdh').addClass('hide');
                $('.cgq_sdh').addClass('hide');
                $('.cgq_dqh').addClass('hide');
            }
            else{
                $('.cgq_cdh').removeClass('hide');
                $('.cgq_sdh').removeClass('hide');
                $('.cgq_dqh').removeClass('hide');
            }
        });
        $('input[name=dgq]').on('ifChanged', function (event){ 
            var dgq = $('input[name=dgq]:checked').length;
            if(dgq==0)
            {
                $('.dgq_dth').addClass('hide');
                $('.dgq_qth').addClass('hide');
            }
            else{
                $('.dgq_dth').removeClass('hide');
                $('.dgq_qth').removeClass('hide');
            }
        });
    });
</script>