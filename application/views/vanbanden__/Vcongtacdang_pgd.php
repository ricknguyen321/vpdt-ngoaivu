<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Phó giám đốc xử lý
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a> <b>({$count} văn bản)</b></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" class="Checkbox"> {$de.sTenPB}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row"><br>
                    <div class="col-sm-12 text-center">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="4" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người ký:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <div class="col-sm-12 text-center deletepadding" >
                            <p style="color: red">Khi tích chọn <b>"VB quan trọng"</b> thì văn bản sẽ được thêm vào <b>danh sách văn bản quan trọng ở trang chủ</b>
                                <br><i>(đây là danh sách quan trọng dành riêng cho người dùng)</i></p>
                        </div>
                        <div class="col-sm-8">
                            {$phantrang}
                        </div>
                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                        <button type="button" id="duyet" class="hidden">Duyet</button>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="" class="text-center">Ngày nhập</th>
                                <th width="30%" class="text-center">Trích yếu - Thông tin</th>
                                <th width="" >Ý kiến</th>
                                <th width="" class="text-center">Chỉ đạo</th>
                                <th width="" class="text-center">Duyệt <br> <input type="checkbox" id="select_all"></th>
                            </tr>
                            </thead>
                            <tbody>
                            {if $vanban['iQuyenHan_DHNB'] == 5}
                            {$i=1}
                            {foreach $getDocAwait as $pl}
                            <tr class="thongbao">
                                <td class="text-center">{$i++}</td>
                                <td>{date_select($pl.sNgayNhap)}</td>
                                <td>
                                    <a style="color: black;" href="{$url}chuyenvienchutrixuly?vbd={$pl.PK_iMaVBDen}">{if (date('Y-m-d',time()) <= $pl.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $pl.sHanThongKe)}<span style="color: gold">{$pl.sMoTa}</span>{elseif $pl.sHanThongKe < date('Y-m-d',time()) && $pl.sHanThongKe > '2017-01-01'}<span style="color: red">{$pl.sMoTa}</span>{else}<span>{$pl.sMoTa}</span>{/if}</a>
                                    <p>- Số ký hiệu: {$pl.sKyHieu} </p>
                                    <p>- Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> </p>
                                    <p>- Nơi gửi: <span style="color: black;">{$pl.sTenDV}</span></p>
                                    <br>
                                    {if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
                                </td>
                                <td width="18%" class="ykien">
                                    <div style="margin-bottom: 10px">
                                        <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc1">
                                            <option value="">Chuyền P.Giám đốc</option>
                                            {foreach $getAccountDeputyDirector as $pgd}
                                            <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-toggle="{$pl.sMoTa}" data-id="{$pgd.sHoTen}" {!empty($pl.PK_iMaCBNhan[5])&&($pl.PK_iMaCBNhan[5] == $pgd.PK_iMaCB) ? selected : NULL}>{$pgd.sHoTen}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px">
                                        <select name="phongchutri[{$pl.PK_iMaVBDen}]" id="" class="form-control phongchutri1">
                                            <option value="">Chọn chủ trì</option>
                                            {foreach $getDepartment as $de}
                                            <option value="{$de.PK_iMaPB}" data-id="{$de.sTenPB}" data-num="{$de.PK_iMaCB}" data-collapse="{$pl.sMoTa}" {!empty($pl.PK_iMaPhongCT[6])&&($pl.PK_iMaPhongCT[6] == $de.PK_iMaPB) ? selected : NULL}>{$de.sTenPB}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px">
                                        <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">Chọn phối hợp</button>
                                    </div>

                                    <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="{($pl.PK_iMaPhongPH) ? $pl.PK_iMaPhongPH[7] : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden mangphoihop1">
                                    <input type="text" name="idpgd[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCN[5])&&($pl.PK_iMaCN[5]) ? $pl.PK_iMaCN[5] : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden">
                                    <input type="text" name="idpcc[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCN[6])&&($pl.PK_iMaCN[6]) ? $pl.PK_iMaCN[6] : NULL}" id="{$pl.PK_iMaVBDen}" class="hidden">
                                    <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden doc_id1">
                                    <div style="margin-bottom: 10px">
                                        <input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{($pl.sThoiGianHetHan[5] > '2017-01-01') ? date_select($pl.sThoiGianHetHan[5]) : (($pl.sHanGiaiQuyet > '2017-01-01') ? date_select($pl.sHanGiaiQuyet) : NULL)}" class="form-control datepic hangiaiquyet1" placeholder="Hạn giải quyết">
                                    </div>
                                    <br>
                                    <a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" target="_blank"><i class="fa {if $pl.iFile==0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $pl.iFile==1} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}
                                    <br>
                                    <span><input type="checkbox" name="vanbanquantrong[{$pl.PK_iMaVBDen}]" value="1"></span> <span style="color: red;"> VB Quan trọng</span>
                                </td>
                                <td width="35%" class="chidao">
                                    <textarea name="chidaophogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}" class="form-control phogiamdoc" rows="2">{!empty($pl.ChuThich[5])&&($pl.ChuThich) ? $pl.ChuThich[5] : NULL}</textarea><br>
                                    <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="4">{!empty($pl.ChuThich[6])&&($pl.ChuThich) ? $pl.ChuThich[6] : NULL}</textarea>
                                    <br>{if !empty($pl.sLyDo)}<p>Nội dung từ chối*: <b><i>{$pl.sLyDo}</i></b></p>{/if}
                                </td>
                                <td class="text-center">
                                    <br><span style="color: red;"> Chọn duyệt:</span><br><input type="checkbox" name="duyet[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="duyet">
                                </td>
                            </tr>
                            {/foreach}
                            {/if}
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                        <div class="col-sm-8">
                            {$phantrang}
                        </div>
                        <p class="hidden" id="id_cb">{$vanban['PK_iMaCB']}</p>
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly.js"></script>
<script>
    $(document).on('click','button[name=duyetvanban]',function(){
        $('#duyet').click();
    });
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector("#duyet").addEventListener("click", function(e){
            var id = $('#id_cb').text();
            $(".thongbao").each(function () {
                var phogiamdoc = $(this).find('.ykien').find('.phogiamdoc1 :selected').val();
                var tieudepgd = $(this).find('.ykien').find('.phogiamdoc1 :selected').attr('data-toggle');
                var phongchutri = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-num');
                var tieude = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-collapse');
                var checkDoc = $(this).find('.duyet').is(':checked');
                if (checkDoc == true) {
                    if (phogiamdoc != id) {
//                    console.log(giamdoc);
                        var notification1 = {
                            'is_notification': false,
                            'id_cb': phogiamdoc,
                            'base_url': 'vanbanchoxuly_pgd',
                            'notification': tieudepgd
                        };
                        notificationsRef.push(notification1);
                    }
                    if (phogiamdoc == id) {
                        if (phongchutri != undefined) {
                            var notification2 = {
                                'is_notification': false,
                                'id_cb': phongchutri,
                                'base_url': 'vanbanchoxuly_tp',
                                'notification': tieude
                            };
                            notificationsRef.push(notification2);
                        }
                    }
                }
            });
        });
    });
</script>
