<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách Email đến
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="" class="form-horizontal" method="get">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-3"><a href="{$url}readmail?trangthai=1" class="btn btn-default">Email đã xem</a></div>
                                    <div class="col-md-3"><a href="{$url}readmail?trangthai=0" class="btn btn-default">Email chưa xem</a></div>
                                    <div class="col-md-4">
                                        <input type="text" class="hide" name="trangthai" value="{($trangthai)?$trangthai:0}">
                                        <input name="chude" type="text" value="{($chude)?$chude:''}" class="form-control" placeholder="Nhập chủ đề tìm kiếm">
                                    </div>
                                    <div class="col-md-1"><button class="btn btn-primary">Tìm kiếm</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%" class="text-center">STT</th>
                                    <th width="35%">Chủ đề</th>
                                    <th width="20%">Người gửi</th>
                                    <th width="10%" class="text-center" >Thời gian</th>
                                    <th width="15%">Tạo văn bản từ mail</th>
                                    <th width="10%" class="text-center" >Tình trạng</th>
                                    <th width="5%" class="text-center" ><button name="xoanhieu" value="xoanhieu" class="btn btn-xs btn-danger">Xóa</button>
                                    <input type="checkbox" name="select_all"  id="select_all"></th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsmail)}{$i=1}
                                    {foreach $dsmail as $m}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td>{$m.mail_subject}</td>
                                            <td>{$m.mail_from}</td>
                                            <td class="text-center">{date_time($m.mail_date)}</td>
                                            <td class="text-center"><a href="{$url}vanbandentumail?id={$m.mail_id}&xml={$m.mail_attachment}&pdf={$m.mail_pdf}" class="tin">XMLOutput.xml</a></td>
                                            <td class="text-center"><span class="label label-{($trangthai)?'success':'warning'}">{($trangthai)?'Đã xem':'Chưa xem'}</span></td>
                                            <td class="text-center"><button type="submit" name="xoa" value="{$m.mail_id}" onclick="return confirm('Bạn có muốn xóa email này?')" class="btn btn-default btn-xs"><i class="fa fa-trash-o"></i></button>
                                                <input type="checkbox" name="mangxoa[]" value="{$m.mail_id}">
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('#select_all').change(function() {
            var checkboxes = $(this).closest('form').find(':checkbox');
            checkboxes.prop('checked', $(this).is(':checked'));
        });
    });
</script>