<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Giấy mời đã hoàn thành
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Loại văn bản:</label>

                                <div class="col-sm-7 sl2">
                                    <div class="row">
                                        <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="javascript:handleSelect(this)">
                                            <option value="dsphongchutri" >-- Tất cả --</option>
                                            <option value="{if $vanban['iQuyenHan_DHNB'] == 7}vanbanchoxuly_pp{/if}{if $vanban['iQuyenHan_DHNB'] == 8}vanbanchoxuly_cv{/if}{if $vanban['iQuyenHan_DHNB'] == 6}vanbanchoxuly_tp{/if}" {!empty($urlcv)&&($urlcv)?($urlcv=="vanbanchoxuly_cv")?'selected':'':''}>Văn bản mới nhận</option>
                                            <option value="dsphoihop">Phối hợp giải quyết</option>
                                            <option value="dsphongchutri?cacloaivanban=1">Đã chỉ đạo chưa giải quyết</option>
                                            <option value="dsvanbanphongdaxuly">Đã giải quyết</option>
                                        </select>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    function handleSelect(elm)
                                    {
                                        window.location = elm.value;
                                    }
                                </script>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a> <b>({$count} văn bản)</b></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade phophoihop" id="myModal3" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phó phòng phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_B" method="POST">
                                {foreach $getAccountDeputyDirector as $pp}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$pp.PK_iMaCB}" data-id="{$pp.sHoTen}" class="Checkbox"> {$pp.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphophongban" value="" class="hidden" >
                                <input type="text" name="phochutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="4" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người ký:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-sm-6 deletepadding1">
                                <span>{$phantrang}</span>
                            </div>
                            <div class="col-sm-6">
<!--                                <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>-->
                            </div>
                        </div>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="" class="text-center">Ngày nhập</th>
                                <th width="30%" class="text-center">Trích yếu - Thông tin</th>
                                <th width="" >Ý kiến</th>
                                <th width="" class="text-center">Chỉ đạo</th>
                                <th width="" class="text-center">Duyệt</th>
                            </tr>
                            </thead>
                            <tbody>
                            {$i=1}
                            {foreach $getDocAwait as $pl}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td>{date_select($pl.sNgayNhap)}</td>
                                <td>
                                    <a style="color:black;" href="{$url}giaymoichuyenvienchutrixuly/{$pl.PK_iMaVBDen}">{$pl.sMoTa}</a>
                                    <input type="text" name="noidungvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sMoTa}">
									<input type="text" name="sohieu[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sKyHieu}">
                                    <input type="text" name="ngaynhapvb[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sNgayNhap}">
                                    <input type="text" name="ngaymoi[{$pl.PK_iMaVBDen}]" class="hidden" value="{$pl.sGiayMoiNgay}">
                                    <br>(Vào hồi {$pl.sGiayMoiGio} ngày {date_select($pl.sGiayMoiNgay)}, tại {$pl.sGiayMoiDiaDiem})
                                    <p>- Số ký hiệu: {$pl.sKyHieu} </p>
                                    <p>- Số đến: <b style="color: red; font-size: 16px">{$pl.iSoDen}</b> </p>
                                    <p>- Nơi gửi: <span style="color: black;">{$pl.sTenDV}</span></p>
                                    <br>{if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
                                </td>
                                <td width="18%" class="ykien">
                                    {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
                                    <div style="margin-bottom: 10px">
                                        <select name="phogiamdoc[{$pl.PK_iMaVBDen}]" id="" class="form-control phogiamdoc1">
                                            <option value="">Chuyển Phó phòng</option>
                                            {foreach $getAccountDeputyDirector as $pgd}
                                            <option value="{$pgd.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$pgd.sHoTen}" {!empty($pl.PK_iMaPP)&&($pl.PK_iMaPP == $pgd.PK_iMaCB) ? selected : NULL}>{$pgd.sHoTen}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px">
                                        <button type="button" name="chonphophoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal3" style="color: #cb0104;padding-right: 12px;"><b>Chọn phó phòng phối hợp</b></button>
                                    </div>
                                    {/if}
                                    <div style="margin-bottom: 10px">
                                        <select name="phongchutri[{$pl.PK_iMaVBDen}]" id="" class="form-control phongchutri1">
                                            <option value="">Chọn chuyên viên</option>
                                            {foreach $getDepartment as $cv}
                                            <option value="{$cv.PK_iMaCB}" data-num="{$pl.PK_iMaVBDen}" data-id="{$cv.sHoTen}" {!empty($pl.PK_iMaCVCT)&&($pl.PK_iMaCVCT == $cv.PK_iMaCB) ? selected : NULL}>{$cv.sHoTen}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <div style="margin-bottom: 10px">
                                        <button type="button" name="chonphoihop" value="{$pl.PK_iMaVBDen}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" >Chọn phối hợp</button>
                                    </div>
                                    <input type="text" name="mangphoihop[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCVPH)&&($pl.PK_iMaCVPH) ? $pl.PK_iMaCVPH : NULL }" id="{$pl.PK_iMaVBDen}" class="hidden mangphoihop1">
                                    <input type="text" name="mangphoihoppp[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaPPPH)&&($pl.PK_iMaPPPH) ? $pl.PK_iMaPPPH : NULL }" id="{$pl.PK_iMaVBDen}--1" class="hidden mangphophongpp">
                                    <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden doc_id1">
                                    <input type="text" name="cvct[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCNCT)&&($pl.PK_iMaCNCT) ? $pl.PK_iMaCNCT : NULL }" class="hidden">
                                    <input type="text" name="vbpp[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaCNPP)&&($pl.PK_iMaCNPP) ? $pl.PK_iMaCNPP : NULL }" class="hidden">
                                    <input type="text" name="vbpp1[{$pl.PK_iMaVBDen}]" value="{!empty($pl.PK_iMaPP)&&($pl.PK_iMaPP) ? $pl.PK_iMaPP : NULL }" class="hidden">
                                    <div style="margin-bottom: 10px">
                                        <input type="text" name="hangiaiquyet[{$pl.PK_iMaVBDen}]" value="{if $vanban['iQuyenHan_DHNB'] == 6}{!empty($pl.sHanChoPP)&&($pl.sHanChoPP > '2017-01-01') ? date_select($pl.sHanChoPP) : (!empty($pl.sHanChoCV)&&($pl.sHanChoCV > '2017-01-01') ? date_select($pl.sHanChoCV) : NULL)}{elseif $vanban['iQuyenHan_DHNB'] == 7}{(!empty($pl.sHanChoCV)&&($pl.sHanChoCV > '2017-01-01') ? date_select($pl.sHanChoCV) : NULL)}{/if}" class="form-control datepic hangiaiquyet1" placeholder="Hạn xử lý">
                                    </div>
									<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" target="_blank"><i class="fa {if $pl.dem == 0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $pl.dem  > 0} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}
                                </td>
                                <td width="35%" class="chidao">
                                    {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
                                    <textarea name="chidaophogiamdoc[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}-1" class="form-control phogiamdoc" rows="2">{!empty($pl.MoTaPP)&&($pl.MoTaPP) ? $pl.MoTaPP : NULL}</textarea><br>
                                    {/if}
                                    <textarea name="chidaophongchutri[{$pl.PK_iMaVBDen}]" id="{$pl.PK_iMaVBDen}-{$pl.PK_iMaVBDen}" class="form-control phongchutri" rows="4">{!empty($pl.MoTaCVCT)&&($pl.MoTaCVCT) ? $pl.MoTaCVCT : NULL}</textarea>
                                </td>
                                <td class="text-center">
                                    <button type="submit" class="btn btn-success btn-xs pull-center" name="duyet" value="{$pl.PK_iMaVBDen}" style="margin-bottom: 10px" disabled><i class="fa fa-check"></i> Duyệt</button>
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
<!--                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>-->
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script src="{$url}assets/js/plugins/vanbanchoxuly_tp.js"></script>
