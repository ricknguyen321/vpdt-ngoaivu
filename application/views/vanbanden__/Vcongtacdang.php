<script src="{$url}assets/js/vanbanden/vanbanden.js"></script>
<script src="{$url}assets/js/plugins/checksoden.js"></script>
<script src="https://cdn.firebase.com/js/client/2.2.3/firebase.js"></script>
<style>
    .alert-warning {
        color: #8a6d3b !important;
        background-color: #fcf8e3 !important;
        border-color: #faebcc !important;
    }
    .alert {
        padding: 15px !important;
        margin-bottom: 20px !important;
        border: 1px solid transparent !important;
        border-radius: 4px !important;
    }
    .alert-dismissable .close {
        position: relative;
        top: -2px;
        right: 0px;
        color: inherit;
    }
    button.close {
        -webkit-appearance: none;
        padding: 0;
        cursor: pointer;
        background: 0 0;
        border: 0;
    }
    .close {
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        filter: alpha(opacity=20);
        opacity: .2;
    }
</style>
<script>
    $(document).ready(function() {
        $('input').css('border-color', 'black');
        $('#soden').css('color', 'red');
        $('textarea').css('border-color', 'black');
        $('input').css('font-size', '15px');
        $('textarea').css('font-size', '15px');
    });
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px;padding-left: 15px; ">
        <div class="row" style="    margin-top: -10px;">
            <div class="col-sm-6">
                <a href="{$url}danhsachcongtacdang"><h1 style="font-size: 13px; font-weight: 600">TÌM KIẾM VĂN BẢN CÔNG TÁC ĐẢNG </h1></a>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content" style="padding-top: 0px">
        <div class="col-sm-12 hidden">
            <div class="noidung">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Nội dung *</label>

                        <div class="col-sm-9">
                            <textarea name="noidunghop[]" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="col-sm-4 control-label">Hạn giải quyết </label>

                        <div class="col-sm-8">
                            <input tabindex="8" type="text" value="" name="hannoidung[]" class="form-control datepic datemask" placeholder="Hạn giải quyết của nội dung">
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        //Date picker
                        $('.datepic').datepicker({
                            autoclose: true
                        });
                        //Datemask dd/mm/yyyy
                        $(".datemask").inputmask("dd/mm/yyyy");
                    });
                </script>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {if isset($content)}
                <div class="alert alert-warning alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <label for="" style="color:black;">Thêm thành công số đến là: <b style="font-size: 20px;color:red;" id="thongbaoden"></b></label>
                </div>
                {/if}
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="on">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-3"><h5><label for="">Là VB QPPL:</label> <input type="checkbox" name="iVanBanQPPL" value="1" {!empty($getDocGo) &&($getDocGo[0]['iVanBanQPPL'] == '1') ? checked : NULL }></h5></div>
                            <div class="col-md-3"><h5><label for="">SNV chủ trì:</label> <input type="checkbox" name="iSTCChuTri" value="1" {!empty($getDocGo) &&($getDocGo[0]['iSTCChuTri'] == '1') ? checked : NULL }></h5></div>
                            <div class="col-md-3"><h5><label for="">TBKL:</label> <input type="checkbox" name="iVanBanTBKL" value="1" {!empty($getDocGo) &&($getDocGo[0]['iVanBanTBKL'] == '1') ? checked : NULL }></h5></div>
                            <div class="col-md-3"><h5><label for="">Văn bản mật:</label> <input type="checkbox" name="iVanBanMat" value="1" {!empty($getDocGo) &&($getDocGo[0]['iVanBanMat'] == '1') ? checked : NULL }></h5></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực:</label>

                                    <div class="col-sm-8">
                                        <select name="khuvuc" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn cấp ban hành --</option>
                                            {foreach layDuLieu(NULL,NULL,'tbl_khuvuc') as $kv}
                                            <option value="{$kv.PK_iMaKV}" {!empty($getDocGo) && ($getDocGo[0]['sTenKV'] == $kv.PK_iMaKV) ? selected : NULL }>{$kv.sTenKV}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <!-- <input tabindex="2" type="text" name="sokyhieu" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sKyHieu'] : NULL }" class="form-control" placeholder="Nhập số hiệu"> -->
                                        <input type="text" tabindex="2" class="form-control" name="sokyhieu" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sKyHieu'] : NULL }" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-danger">*</span>:</label>

                                    <!-- <div class="col-sm-8">
                                        <select name="loaivanban" id="" class="form-control select2 ngayhan" style="width:100%">
                                            <option value="">-- Chọn loại văn bản --</option>
                                                {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                    <option value="{$lvb.sTenLVB}">{$lvb.sTenLVB}</option>
                                                {/foreach}
                                        </select>
                                    </div> -->
                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="4" type="text" class="loaivanban form-control ngayhan" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenLVB'] : NULL }" name="loaivanban" autocomplete="off" placeholder="Nhập loại văn bản" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="1" type="text" class="donvi form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenDV'] : NULL }" name="donvi" placeholder="Nhập nơi gửi đến">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" class="form-control datepic datemask" id="" name="ngayky" placeholder="Ngày ký" value="{!empty($getDocGo) &&($getDocGo) ? date_select($getDocGo[0]['sNgayKy']) : date('d/m/Y',time()) }" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="5" type="text" class="linhvuc form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenLV'] : NULL }" name="linhvuc" placeholder="Nhập lĩnh vực">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class=" col-md-12">
                                    <div class="form-group" style="margin-top: -16px;">
                                        <label for="" class="control-label">Trích yếu <span class="text-danger">*</span></label>
                                        <textarea tabindex="6" name="trichyeu" id="trichyeu" class="form-control ngayhan" rows="2" required placeholder="Nhập trích yếu">{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sMoTa'] : NULL }</textarea>
                                        <p id="baoloi"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="    margin: -30px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Nội dung *</label>

                                    <div class="col-sm-9">
                                        <textarea name="noidunghop[]" class="form-control" rows="3" style="border: 1px solid red;" placeholder="Nhập nội dung">{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sNoiDung'] : NULL }</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết </label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{if !empty($getDocGo[0]['sHanNoiDung']) && $getDocGo[0]['sHanNoiDung'] > '2017-01-01'}{!empty($getDocGo) &&($getDocGo) ? date_select($getDocGo[0]['sHanNoiDung']) : NULL }{/if}" name="hannoidung[]" class="form-control datepic datemask" placeholder="Hạn giải quyết của nội dung">
                                    </div>
                                </div>
                            </div>
                            <div class="themsau"></div>
                            {if empty($idDocGo)}
                            <div class="col-md-12 text-right">
                                <div class="col-sm-12" style="    margin-top: -40px;">
                                    <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm nội dung</b>
                                </div>
                            </div>
                            {/if}
                            <div class="col-sm-12" style="    margin: -25px;margin-left: 0px;">
                                <hr style="border: 0.5px solid #3c8dbc">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="7" type="text" name="nguoiky" class="nguoiky form-control" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sTenNguoiKy'] : NULL }">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày nhận:</label>

                                    <div class="col-sm-8">
                                        <input tabindex="9" type="text" value="{!empty($getDocGo) &&($getDocGo) ? date_select($getDocGo[0]['sNgayNhan']) : date('d/m/Y',time()) }" name="ngaynhan" class="form-control ngayhan datepic datemask" placeholder="Nhập ngày nhận">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số đến:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['iSoDen'] : $soden }" name="soden" id="soden" class="form-control" placeholder="Nhập số đến" style="font-size: 16px; color: red; font-weight: 600">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="8" class="form-control" name="chucvu" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sChucVu'] : NULL }" id="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="hangiaiquyet" value="{!empty($getDocGo) &&($getDocGo[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getDocGo[0]['sHanGiaiQuyet']) : ''}" class="form-control datepic datemask" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang:</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="10" value="{!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['iSoTrang'] : 1 }" name="sotrang" class="form-control col-sm-4" placeholder="nhập số trang">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 {!empty($getDocGo) &&($getDocGo) ? 'hide':''}">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label" style="padding-right: 0px;">Thời hạn theo QT ISO:</label>

                                    <div class="col-sm-10">
                                        <select name="quytrinh" id="" class="form-control ngayhan select2" style="width: 100%;">
                                            <option value="5">-- Lựa chọn quy trình --</option>
                                            {if !empty($dsquytrinh)}{$i=1}
                                            {foreach $dsquytrinh as $qt}
                                            <option value="{$qt.iSoNgay}">{$qt.sTenQuyTrinh}</option>
                                            {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {if !empty($idDocGo)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Ngày nhận</th>
                                            <th class="text-center">Chuyển từ</th>
                                            <th width="40%">Nội dung</th>
                                            <th class="text-center">Chuyển đến</th>
                                            <th class="text-center">Hạn xử lý</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $process as $pr}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_time($pr.sThoiGian)}</td>
                                            <td class="text-center">
                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                                                {$cb.sHoTen}
                                                {/if}
                                                {/foreach}
                                            </td>
                                            <td>{$pr.sMoTa}</td>
                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">{if ($pr.sThoiGianHetHan) > '2017-01-01'}{date_select($getDocGo[0]['sHanGiaiQuyet'])}{/if}<br><i>{if ($pr.sThoiGianHetHan) > '2017-01-01'}(Hạn VB: {date_select($pr.sThoiGianHetHan)}){/if}</i></td>                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Tệp tin đính kèm:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th class="text-center">STT</th>

                                            <th>Tên tệp tin</th>

                                            <th class="text-center">Tải về</th>

                                            <th class="text-center">Ngày nhập</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $Filedinhkem as $fi}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td>{$fi.sTenFile}</td>
                                            <td class="text-center"> <a class="tin1" href="{$url}doc_uploads_2017/{$fi.sTenFile}" download>[Tải về]</a></td>
                                            <td class="text-center" width="15%">{date_select($fi.sThoiGian)}</td>
                                        </tr>
                                        {/foreach}

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            {if !empty($resultPTL)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-2 control-label">Kết quả giải quyết:</label>

                                </div>

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        <textarea name="ketqua" id="" class="form-control" rows="4" readonly>{foreach $resultPTL as $re}{$re.sMoTa}&#13;&#10;{/foreach}</textarea>

                                    </div>

                                </div>

                            </div>

                            {/if}
                            {if !empty($VBDi)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Văn bản đi</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Số văn bản đi</th>
                                            <th class="text-center" >Ngày văn bản</th>
                                            <th class="text-center">Trích Yếu</th>
                                            <th class="text-center">Xem file</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $VBDi as $di}
                                        <tr class="success">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{$di.iSoVBDi}</td>
                                            <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                            <td>{$di.sMoTa}</td>
                                            <td class="text-center"><a href="{$di.sDuongDan}" target="_blank">[Xem file]</a></td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}

                            {/if}
                            <div class="col-md-12">
                                <button tabindex="10" type="button" class="btn btn-primary btn-sm hidden" name="luuao" disabled>Lưu lại</button> <button tabindex="10" type="submit" class="btn btn-primary btn-sm" name="luulai" value="Lưu lại">Lưu lại</button> <label for="">Người nhập văn bản này là: {!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['sHoTen'] : $vanban['sHoTen'] }</label>
                                <button class="hidden" name="thongbao" id="notification-button">thongbao</button>
                                {if empty($idDocGo)}
                                <div ng-app="myModule" ng-controller="mtController" class="hidden">
                                    <button ng-click="checkSoDen()" name="checksoden">kiem tra so den</button>
                                </div>
                                {/if}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
        $(document).on('click','button[name=luulai]',function(){
            $('button[name=checksoden]').click();
            $('button[name=thongbao]').click();
        });
        $('#thongbaoden').text(parseInt($.trim($('#soden').val())) - 1);
        $(document).on('click','button[name=luulai]',function () {
            $('button[name=luulai]').addClass('hidden');
            $('button[name=luuao]').removeClass('hidden');
        });
        $(document).on('keyup','input',function () {
            $('button[name=luulai]').removeClass('hidden');
            $('button[name=luuao]').addClass('hidden');
        });
    });
</script>
{if isset($content)}
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        if (Notification.permission !== "granted"){
            Notification.requestPermission();
        }
        window.onload = function(e){
            e.preventDefault();
            if($('#soden').val()==''){
                $('#soden').focus();
                return false;
            }
            // Save notification
            var notification = {
                'is_notification' : false,
                'id_user' : 3,
                'base_url' : 'vanbanchoxuly',
                'notification' : parseInt($.trim($('#soden').val())) - 1
            };
            notificationsRef.push(notification);
//            $('#soden').val('');
            return false;
        }
    });
</script>
{/if}
<script>
    var data = {
        "timkiem": [
            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
            "{$lvb.sTenLVB}",
            {/foreach}
    ],
    "noiden": [
        {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
        "{$dv.sTenDV}",
        {/foreach}
    ],
    "linhvuc": [
        {foreach layDuLieu(NULL,NULL,'tbl_linhvuc') as $lv}
        "{$lv.sTenLV}",
        {/foreach}
    ],
    "nguoiky": [
        {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
        "{$nk.sHoTen}",
        {/foreach}
    ]

    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".loaivanban",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".linhvuc",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            linhvuc: {
                data: data.linhvuc
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".donvi",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            noiden: {
                data: data.noiden
            }
        },
        debug: false
    });
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".nguoiky",
        minLength: 1,
        maxItem: 10,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            nguoiky: {
                data: data.nguoiky
            }
        },
        debug: false
    });
</script>
<script>

</script>
