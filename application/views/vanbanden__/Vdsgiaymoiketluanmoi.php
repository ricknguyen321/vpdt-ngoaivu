<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Báo cáo kết quả cuộc họp
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Loại văn bản:</label>

                                <div class="col-sm-7 sl2">
                                    <div class="row">
                                        <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="javascript:handleSelect(this)">
                                            <option value="dsphongchutri" >-- Tất cả --</option>
                                            <option value="vanbanchoxuly_cv" {!empty($urlcv)&&($urlcv)?($urlcv=="vanbanchoxuly_cv")?'selected':'':''}>Văn bản mới nhận</option>
                                            <option value="dsphoihop">Phối hợp giải quyết</option>
                                            <option value="dsphongchutri?cacloaivanban=1">Đã chỉ đạo chưa giải quyết</option>
                                            <option value="dsvanbanphongdaxuly">Đã giải quyết</option>
                                        </select>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    function handleSelect(elm)
                                    {
                                        window.location = elm.value;
                                    }
                                </script>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a><b>({$count} văn bản)</b></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bs-example-modal-lg-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title text-center">NỘI DUNG CHI TIẾT</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="">- Đề xuất của phòng</label>
                                    <div id="noidungdexuat"></div>
                                </div>
                                <div class="col-sm-12">
                                    <label for="">- Kết luận cuộc họp</label>
                                    <div id="noidungchitiet"></div>
                                </div>
                                <div class="col-sm-12">
                                    <div id="baocao"></div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <label for="">- Ý kiến giám đốc</label>
                                    <div id="ykienlanhdao"></div>
                                </div>
                                <div id="mang"></div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
        <div class="modal fade phongphoihop" id="myModal1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chuyển tiếp báo cáo cuộc họp</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Chọn cán bộ nhận báo cáo</label>
                                    <div class="col-sm-12">
                                        <select name="nguoinhan[]" multiple="" data-placeholder="Chọn người nhận" id="" class="form-control select2" style="width: 100%">
                                            {foreach $getAccountDeputyDirector as $a}
                                            <option value="{$a.PK_iMaCB}">{$a.sHoTen}</option>
                                            {/foreach}
                                            {foreach $getDepartment as $b}
                                            <option value="{$b.PK_iMaCB}">{$b.sHoTen}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <label for="" class="col-sm-12">Nhập nội dung chuyển tiếp</label>
                                    <div class="col-sm-12">
                                        <textarea name="sNoiDungChuyenTiepTP" id="" cols="30" rows="3" class="form-control" placeholder="Nhập nội dung từ chối...." required></textarea>
                                    </div>
                                </div>
                                <input type="text" name="maphongbantp" value="" class="hidden">
                                <input type="text" name="idNoiDung" id="idND" class="hidden" value="">
                                <input type="text" name="idPhong" id="idPNhap" class="hidden" value="">
                                <input type="text" name="FileBaoCao" id="BaoCao" class="hidden" value="">
                                <input type="text" name="idCBNhap1" id="idCBNhap" class="hidden" value="">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="chuyentiepcanbo" class="btn btn-primary btn-sm" value="Chuyển trưởng phòng">Gửi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="4" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người ký:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <span class="pull-right">{$phantrang}</span>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="" class="text-center">Ngày nhập</th>
                                <th class="text-center">Số đến</th>
                                <th class="text-center">Số ký hiệu</th>
                                <th class="text-center">Nơi gửi</th>
                                <th width="" class="text-center">Trích yếu - Thông tin</th>
                                <th width="" class="text-center">Trình tự xử lý</th>
                                <th width="" class="text-center">Chi tiết</th>
                            </tr>
                            </thead>
                            <tbody>
                            {$i=1}
                            {foreach $getDocAwait as $pl}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td class="text-center">{date_select($pl.sNgayNhap)}</td>
                                <td class="text-center"><b style="color: red; font-size: 16px">{$pl.iSoDen}</b></td>
                                <td class="text-center">{$pl.sKyHieu}</td>
                                <td class="text-center">{$pl.sTenDV}</td>
                                <td class="mieuta">
                                    <a style="color: black;" href="{$url}quytrinhgiaymoi/{$pl.PK_iMaVBDen}"><b>{$pl.sMoTa}</b></a><br>
                                    <p>(Vào hồi {$pl.sGiayMoiGio} ngày {date_select($pl.sGiayMoiNgay)}, tại {$pl.sGiayMoiDiaDiem})</p>
                                    <br>{if !empty($pl.sNoiDung)}<p>* Nội dung: <i><b>{$pl.sNoiDung}</b></i></p>{/if}
                                    <br>{if !empty($pl.sNoiDungChuyenTiepTP)}<p>* Nội dung chuyển: <i><b>{$pl.sNoiDungChuyenTiepTP}</b></i></p>{/if}
									<textarea name="noidungmieuta" id="" cols="30" rows="10" class="hidden">{$pl.smieuta}</textarea>
                                    <textarea name="noidungdexuat" id="" cols="30" rows="10" class="hidden">{$pl.sNoiDungDeXuat}</textarea>
                                    <textarea name="ykien" id="" cols="30" rows="10" class="hidden">{$pl.sYKien}</textarea>
                                    <input type="hidden" name="filesbc" value="{$pl.sFile}">
                                    <input type="hidden" name="idVBD" value="{$pl.PK_iMaVBDen}">
                                    <input type="hidden" name="idND" value="{$pl.PK_iMaNoiDung}">
                                    <input type="hidden" name="idPNhap" value="{$pl.PK_iMaPhongNhap}">
                                    <input type="hidden" name="idCBNhap" value="{$pl.PK_iMaCBNhap}">
                                    <input type="hidden" name="pre" value="{$vanban['iQuyenHan_DHNB']}">
                                    <label for="">*Báo cáo gửi các phòng:</label><br>
                                    <i>
                                        {$b=1}
                                        {foreach $pl.TenPhong as $tp}
                                        {$b++}. {$tp}<br>
                                        {/foreach}
                                    </i>
                                </td>
                                <td class="chidao">
                                    {$a=1}
                                    {foreach $pl.TrinhTu as $pr}
                                    <p>{$a++}. {$pr}</p>
                                    <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                    {/foreach}
                                </td>
                                <td class="text-center">
                                    <a href="" data-toggle="modal" data-target=".bs-example-modal-lg-1" class="xem" data-num="{$pl.PK_iMaSoanBaoCao}">Xem chi tiết</a>
                                    <br><br>
                                    {if $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 10 || $vanban['iQuyenHan_DHNB'] == 11}
                                    <button type="button" name="chuyentiep" value="{$pl.PK_iMaVBDen}" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal1"><i class="fa fa-share-square-o" aria-hidden="true"></i></button>
                                    {/if}
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script>
    $(document).ready(function () {
        var url = window.location.href;
       $(document).on('click','.xem',function () {
         var detail = $(this).parent().parent().find('textarea[name=noidungmieuta]').text();
           var detail1 = $(this).parent().parent().find('textarea[name=noidungdexuat]').text();
           var ykien = $(this).parent().parent().find('textarea[name=ykien]').text();
           var idDoc = $(this).parent().parent().find('input[name=idVBD]').val();
           var idContent = $(this).parent().parent().find('input[name=idND]').val();
           var baocao = $(this).parent().parent().find('input[name=filesbc]').val();
           var pre = $(this).parent().parent().find('input[name=pre]').val();
           if(baocao != ''){
               $('#baocao').html('<a href="https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/'+baocao+'" target="_blank">[Xem báo cáo]</a>');
           }
           $('#noidungchitiet').html(detail);
           $('#noidungdexuat').html(detail1);
           $('#ykienlanhdao').html(ykien);
           if(pre == 4){
               $('#mang').html('<div class="col-sm-8">\n' +
                   '<br>\n' +
                   '<label for="">Ý kiến của lãnh đạo</label>\n' +
                   '<input type="text" name="data[sYKien]" value="" class="form-control" placeholder="Nhập ý kiến" required>\n' +
                   '</div>\n' +
                   '<div class="col-sm-4" style="padding-top: 4px;" id="mang">\n' +
                   '<br><br>\n' +
                   '<input type="text" name="idNoiDung" id="idNoiDung" class="hidden" value="'+idContent+'">\n' +
                   '<input type="text" name="idVanBanDen" id="idVanBanDen" class="hidden" value="'+idDoc+'">\n' +
                   '<button type="submit" name="duyetbc" class="btn btn-primary btn-sm" value="Duyệt">Duyệt</button>\n' +
                   '<button type="submit" name="tuchoibc" class="btn btn-danger btn-sm" value="Từ chối">Từ chối</button>\n' +
                   '</div>');
           }
           $.ajax({
               url: url,
               type: 'POST',
               data: {
                   'action' : 'getData',
                   'idTypeOf' : $(this).attr('data-num')
               },
               success: function(response){
                   var result = JSON.parse(response);
//                   console.log(response);
               }
           });// end ajax
       });
        $(document).on('click','button[name=chuyentiep]',function () {
            $('input[name=maphongbantp]').val($(this).val());
            var idContent = $(this).parent().parent().find('input[name=idND]').val();
            $('#idND').val(idContent);
            var idPNhap = $(this).parent().parent().find('input[name=idPNhap]').val();
            $('#idPNhap').val(idPNhap);
            var idCBNhap = $(this).parent().parent().find('input[name=idCBNhap]').val();
            $('#idCBNhap').val(idCBNhap);
            var baocao = $(this).parent().parent().find('input[name=filesbc]').val();
            $('#BaoCao').val(baocao);
        });
    });
</script>