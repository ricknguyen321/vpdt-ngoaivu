<div class="content-wrapper">
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Văn bản chờ phê duyệt
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Văn bản :</label>

                                <div class="col-sm-8 sl2">
                                    <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="this.form.submit()">
                                        <option value="1" {!empty($cacloaivanban)&&($cacloaivanban)?($cacloaivanban=="1")?'selected':'':''}>Chưa phê duyệt</option>
                                        <option value="2" {!empty($cacloaivanban)&&($cacloaivanban)?($cacloaivanban=="2")?'selected':'':''}>Đã phê duyệt</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a><b>({$count} văn bản)</b></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Modal -->
        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" >
                            <form role="form" action="" id="Q_A" method="POST">
                                {foreach $getDepartment as $de}
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>
                                    </div>
                                </div>
                                {/foreach}
                                <input type="text" name="mangphongban" value="" class="hidden" >
                                <input type="text" name="phongchutri1" value="" class="hidden" >
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade phongphoihop" id="myModal4" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung từ chối...."></textarea>
                                    </div>
                                </div>
                                <input type="text" name="iddoc" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Từ chối kết quả">Từ chối kết quả</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="4" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người ký:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post">
                        <span class="pull-left">{$phantrang}</span>
                        {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11}
                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                        {/if}
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">STT</th>
                                <th width="" class="text-center">Ngày nhập</th>
                                <th class="text-center">Số đến</th>
                                <th class="text-center">Số ký hiệu</th>
                                <th class="text-center">Nơi gửi</th>
                                <th width="30%" class="text-center">Trích yếu - Thông tin</th>
                                <th width="" class="text-center">Trình tự xử lý</th>
                                {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11}
                                <th width="" class="text-center">Duyệt <br> <input type="checkbox" id="select_all"></th>
                                {else}
                                <th width="" class="text-center">Trạng thái</th>
                                {/if}
                            </tr>
                            </thead>
                            <tbody>
                            {$i=1}
                            {foreach $getDocAwait as $pl}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td class="text-center">{date_select($pl.sNgayNhap)}</td>
                                <td class="text-center"><b style="color: red; font-size: 16px">{$pl.iSoDen}</b></td>
                                <td class="text-center">{$pl.sKyHieu}</td>
                                <td>{$pl.sTenDV}</td>
                                <td>
                                    <a style="color:black;" href="{$url}{if $pl.iToCongTac == 1}chuyenvienxuly_tct?vbd={$pl.PK_iMaVBDen}{else}quatrinhxuly/{$pl.PK_iMaVBDen}{/if}">{$pl.sMoTa}</a>
                                    <p>{if $pl.sHanGiaiQuyet > '2017-01-01'}(<b>Hạn giải quyết : </b>{date_select($pl.sHanGiaiQuyet)}){/if}</p>
									<br>
									<a class="tin1" href="{$url}teptinden?id={$pl.PK_iMaVBDen}" target="_blank"><i class="fa {if $pl.dem == 0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $pl.dem  > 0} <a target="_blank" href="{$pl.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}
									<br><br>
                                    <p>*Nội dung hoàn thành của đ/c <b>{$pl.Ten_NHT}</b>: <br><i>{$pl.MoTa_NHT}</i></p>
									{if !empty($pl.lydochammuon)}<p><b>Lý do chưa kết thúc văn bản</b>: <i>{$pl.lydochammuon}</i></p>{/if}
								</td>
                                <td width="20%">
                                    {$a=1}
                                    {foreach $pl.TrinhTu as $pr}
                                    <p>{$a++}. {$pr}</p>
                                    <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                    {/foreach}
                                    <input type="text" name="doc_id[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="hidden">
                                </td>
                                <td class="text-center" width="8%">
                                    {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11}
                                    <br><span style="color: red;"> Chọn duyệt: <br><input type="checkbox" name="duyet[{$pl.PK_iMaVBDen}]" value="{$pl.PK_iMaVBDen}" class="duyet"><br>
                                        <br><span style="color: red;"> Có sáng tạo: <br><input type="checkbox" name="cosangtao[{$pl.PK_iMaVBDen}]" value="1"><br>
                                            <br><span style="color: red;"> VB chỉ để lưu kho: <br><input type="checkbox" name="luukho[{$pl.PK_iMaVBDen}]" value="1" {($pl.sLuuVaoKho == 1) ? checked : NULL}><br>
											<input type="hidden" name="canbo" value="{$pl.PK_iMaCBHoanThanh}"><br>
                                                 <button type="button" name="tuchoikq"  value="{$pl.PK_iMaVBDen}" class="btn btn-warning btn-xs"  data-toggle="modal" data-target="#myModal4"><i class="fa fa-reply" aria-hidden="true"></i> Từ chối kết quả</button>
                                    {else}
                                            {if $pl.iTrangThai == 1}
                                            <a href="javascript:void(0)" class="btn btn-success btn-xs" value="Duyệt" style="margin-bottom: 10px"><i class="fa fa-check"></i> Đã duyệt</a>
                                            {if $pl.iCoSangTao == 1}
                                            <br><span style="color: red;"> Có sáng tạo: <br><input type="checkbox" name="" value="1" ($pl.iCoSangTao == 1) ? checked : NULL><br>
                                            {/if}
                                             {if $pl.sLuuVaoKho == 1}
                                            <br><span style="color: red;"> VB lưu kho: <br><input type="checkbox" name="" value="1" ($pl.sLuuVaoKho == 1) ? checked : NULL><br>
                                            {/if}
                                            {elseif $pl.iTrangThai == 2}
											{if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11}
                                             <a href="javascript:void(0)" class="btn btn-warning btn-xs" value="Duyệt" style="margin-bottom: 10px"><i class="fa fa-check"></i> Chưa duyệt</a>
											 {else}
											 <a href="javascript:void(0)" class="btn btn-warning btn-xs" value="Duyệt" style="margin-bottom: 10px"><i class="fa fa-check"></i> LĐ phòng <br>chưa duyệt</a>
											 {/if}
                                             {if $pl.sLuuVaoKho == 1}
                                            <br><span style="color: red;"> VB lưu kho: <br><input type="checkbox" name="" value="1" ($pl.sLuuVaoKho == 1) ? checked : NULL><br>
                                            {/if}
                                            {/if}
                                    {/if}
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 11}
                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetvanban" value="Duyệt" style="margin-bottom: 10px" ><i class="fa fa-check"></i> Duyệt</button>
                        {/if}
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script>
    $("#select_all").click(function(){
        var checked_status = this.checked;
        $(".duyet").each(function(){
            this.checked = checked_status;
        });
    });
    $("button[name=tuchoikq]").each(function (i, e) {
        $(this).click(function () {
            $('input[name=iddoc]').val($(this).val());
        });
    });
</script>
