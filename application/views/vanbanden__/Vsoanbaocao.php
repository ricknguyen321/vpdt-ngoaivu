<style>
    .btn {
        display: inline-block;
        padding: 5px 10px;
        margin-bottom: 0;
        font-size: 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Soạn báo cáo kết quả cuộc họp
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-md-12 control-label text-danger">KẾT LUẬN CUỘC HỌP </label>
                                    <div class="col-md-12 ">
                                        <textarea name="noidung" class="form-control textarea" id="" cols="" rows="20" placeholder="Nhập nội dung" style="width: 100%">{($thongtin)?$thongtin[0]['sNoiDung']:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-md-12 control-label text-danger">ĐỀ XUẤT CỦA PHÒNG </label>
                                    <div class="col-md-12 ">
                                        <textarea name="noidungdexuat" class="form-control textarea" id="" cols="" rows="20" placeholder="Nhập nội dung" style="width: 100%">{($thongtin)?$thongtin[0]['sNoiDungDeXuat']:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-12 control-label">File đính kèm </label>
                                <div class="col-md-12">
                                    <input type="file" name="files" value="">
                                    {if !empty($thongtin[0]['sFile'])}
                                    <ul>
                                        <li class="text-info"><a href="{$url}{($thongtin)?$thongtin[0]['sFile']:''}" target="_blank"><b>Xem file</b></a></li>
                                        <input type="hidden" name="luufiles" value="{($thongtin)?$thongtin[0]['sFile']:''}">
                                    </ul>
                                    {/if}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label for=""> DANH SÁCH CÁC ĐƠN VỊ CẦN NHẬN VĂN BẢN:</label>
                            <table id="phongban" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%" class="text-center"><input type="checkbox" name="checkallpb"></th>
                                    <th width="50%">Tên phòng ban</th>
                                    <th width="35%">Người nhận</th>
                                    <!--                                    <th width="25%">Ghi chú</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                {if !empty($dsphongban)}
                                {foreach $dsphongban as $pb}
                                <tr>
                                    <td class="text-center" width="5%"><input type="checkbox" name="phongban[]" value="{$pb.PK_iMaPB}"
                                                                   {if !empty($dsphong)}
                                                                   {foreach $dsphong as $p}
                                                                   {if $pb.PK_iMaPB == $p.PK_iMaPB}checked{/if}
                                        {/foreach}
                                        {/if}></td>
                                    <td width="50%">{$pb.sTenPB}</td>
                                    <td width="35%">
                                        {if $pb.PK_iMaPB == 73}
                                        <select name="nguoinhan[{$pb.PK_iMaPB}][]" multiple="" data-placeholder="Chọn người nhận" id="" class="form-control select2" style="width:100%;">
                                            {if !empty($dsnguoinhanpb)}
                                            {foreach $dsnguoinhanpb as $ngpb}
                                            {if $ngpb.FK_iMaPhongHD == $pb.PK_iMaPB}
                                            <option value="{$ngpb.PK_iMaCB}"
                                                    {if !empty($dscanbo)}
                                                    {foreach $dscanbo as $c}
                                                    {if $ngpb.PK_iMaCB == $c.PK_iMaCB}selected{/if}
                                            {/foreach}
                                            {/if}
                                            >{$ngpb.sHoTen}</option>
                                            {/if}
                                            {/foreach}
                                            {/if}
                                        </select>
                                        {/if}
                                    </td>
                                    <!--                                    <td><input name="ghichu[{$pb.PK_iMaPB}][]" type="text" placeholder="Nhập ghi chú tại đây" value="{if !empty($dsphong)}{foreach $dsphong as $p}{if $pb.PK_iMaPB == $p.PK_iMaPB}{$p.sGhiChu}{/if}{/foreach}{/if}" class="form-control">-->
                                    <!--                                    </td>-->
                                </tr>
                                {/foreach}
                                {/if}
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                            <input type="hidden" name="idvbden" value="{($thongtin)?$thongtin[0]['PK_iMaVBDen']:''}">
                            <a href="javascript: history.back(1)" class="btn btn-default btn-sm">Quay lại >></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('change',"input[name=checkallpb]",function () {
            $(this).parents('#phongban').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $(document).on('change',"input[name=checkallqh]",function () {
            $(this).parents('#quanhuyen').find("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $('td').css('vertical-align', 'middle');
    });
</script>

