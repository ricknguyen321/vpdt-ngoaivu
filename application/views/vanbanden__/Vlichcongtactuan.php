<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="refresh" content="30"> 30 giây refesh trang 1 lần-->
  <link href="" rel="shortcut icon" type="image/x-icon" />
  <title>Lịch công tác tuần</title>
  <base href="">
  <meta name="author" content="Nguyễn Xuân Hải">
  <meta name="copyright" content="Sở Ngoại Vụ">
  <meta name="description" content="LỊCH HỌP DO LÃNH ĐẠO SỞ CHỦ TRÌ ">
  <meta name="robots" content="noindex,nofollow">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">
  <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
  <style>
    .container {
        font-family:"Times New Roman", Times, serif;
        font-size:13pt;
    }
    .indam{
        font-weight: bold;
    }
  </style>
</head>

<body><br/>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="indam text-center">LỊCH HỌP CÔNG TÁC TUẦN: {$thuhai} - {$thubay}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr style="background:#ccc;">
                        <th width="5%" class="text-center">STT</th>
                        <th width="35%" class="text-center">Nội dung</th>
                        <th width="10%" class="text-center">Thời gian</th>
                        <th width="20%" class="text-center">Địa điểm</th>
                        <th width="15%" class="text-center">Lãnh đạo chỉ đạo</th>
                        <th width="15%" class="text-center">Đơn vị giải quyết</th>
                    </tr>
                    <tbody>
                    {if !empty($danhsach)}{$i=1}
                        {foreach $danhsach as $dl}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td>
                                    <p><b>- {$dl.sTenDV}</b></p>
                                    <p>{$dl.sMoTa}</p>
                                </td>
                                <td>{$dl.sGiayMoiGio}, {date_select($dl.sGiayMoiNgay)}</td>
                                <td>{$dl.sGiayMoiDiaDiem}</td>
                                <td></td>
                                <td>{$dl.phonggiaiquyet}</td>
                            </tr>
                        {/foreach}
                    {/if}
                    </tbody>    
                </table>
            </div>
        </div>
    <style>
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #0c0b0b;
        }
        hr{
            border-top: 1px solid #9E9E9E;
            margin: 10px 0px;
        }
        h2, h3 {
            margin-top: 0px;
            margin-bottom: 20px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('td').css('vertical-align', 'middle');
            $('th').css('vertical-align', 'middle');
        });
    </script>
</body>
</html>
