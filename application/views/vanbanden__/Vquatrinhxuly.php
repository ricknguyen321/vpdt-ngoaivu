<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Thông tin văn bản
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" action="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-3"><h4><label for="">Là VB QPPL:</label> <input type="checkbox" name="iVanBanQPPL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanQPPL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">SNV chủ trì:</label> <input type="checkbox" name="iSTCChuTri" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iSTCChuTri'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">TBKL:</label> <input type="checkbox" name="iVanBanTBKL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanTBKL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h5><label for="">TTHC:</label> <input type="checkbox" name="iToCongTac" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iToCongTac'] == '1') ? checked : NULL }></h5></div>
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <label for=""></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLVB'] : NULL }</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Ngày ký <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">	Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <p><b><i>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</i></b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-info">*</span>: </label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenNguoiKy'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Ngày nhận <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayNhan']) : NULL }</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sChucVu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getdAppointment[0]['sHanGiaiQuyet']) : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2" style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }</label>
                                    </div>
                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>

                                    <label for="" class="col-sm-1">Độ khẩn:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2">Số trang:</label>
                                    <div class="col-sm-10">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoTrang'] : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label"> Tham mưu:</label>

                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày phân loại:</label>

                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
                            {if !empty($idDocGo)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Ngày nhận</th>
                                            <th class="text-center">Chuyển từ</th>
                                            <th width="40%">Nội dung</th>
                                            <th class="text-center">Chuyển đến</th>
                                            <th class="text-center">Hạn xử lý</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $process as $pr}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_time($pr.sThoiGian)}</td>
                                            <td class="text-center">
                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                                                {$cb.sHoTen}
                                                {/if}
                                                {/foreach}
                                            </td>
                                            <td>{$pr.sMoTa}</td>
                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">{if $getdAppointment[0]['sHanThongKe'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanThongKe'])}{/if}<br><i>{if $pr.sThoiGianHetHan > '2017-01-01'}(Hạn VB: {date_select($pr.sThoiGianHetHan)}){/if}</i></td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Tệp tin đính kèm:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th>Tên tệp tin</th>
                                            <th class="text-center">Tải về</th>
                                            <th class="text-center">Ngày nhập</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}

                                        {foreach $Filedinhkem as $fi}

                                        <tr>

                                            <td class="text-center">{$i++}</td>

                                            <td>{$fi.sTenFile}</td>

                                            <td class="text-center"> <a class="tin1" href="{$fi.sDuongDan}" download>[Tải về]</a></td>

                                            <td class="text-center" width="15%">{date_select($fi.sThoiGian)}</td>

                                        </tr>

                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							 {if !empty($resultPTL)}
							<div class="col-sm-12">

                                <div class="form-group">

                                    <label for="">Files văn bản liên quan:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th>STT</th>

                                            <th>Cán bộ-đơn vị</th>

                                            <th>Tên file</th>

                                            <th class="text-center">Tải về</th>

                                            <th class="text-center">Ngày nhập</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $resultPTL as $tl}
                                        {if empty($tl.sMoTa)}
                                        <tr >

                                            <td class="text-center">{$i++}</td>

                                            <td width="20%">{$tl.sHoTen} - {$tl.sTenPB}</td>

                                            <td>{$tl.sTenFiles}</td>

                                            <td width="8%" class="text-center"><a href="{$tl.sDuongDanFile}">[Tải tài liệu]</a></td>

                                            <td width="15%" class="text-center">{date_select($tl.date_file)}</td>

                                        </tr>
                                        {/if}
                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>
							{/if}
                            {if !empty($resultPTL)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-2 control-label">Kết quả giải quyết:</label>

                                </div>

                                <div class="form-group">
                                    <form action="" method="post">
                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>
                                            <th>STT</th>
                                            <th>Cán bộ-đơn vị</th>
                                            <th>Nội dung</th>
                                            <th class="text-center">Ngày nhập</th>
                                            <th class="text-center">Tác vụ</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $resultPTL as $tl}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td width="20%">{$tl.sHoTen} - {$tl.sTenPB}</td>
                                            <td>{$tl.sMoTa}</td>
                                            <td width="15%" class="text-center">{date_select($tl.date_file)}</td>
                                            {if $tl.active == 2}
                                                <td width="8%" class="text-center"><button type="submit" value="{$tl.PK_iMaKetQua}" name="xoafile" onclick="return confirm('Bạn muốn xóa {$tl.sMoTa} ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button></td>
                                            {else}
                                            <td class="text-center"></td>
                                            {/if}
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                    </form>
                                </div>

                            </div>

                            {/if}
                            {if !empty($VBDi)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Văn bản đi</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Số văn bản đi</th>
                                            <th class="text-center" >Ngày văn bản</th>
                                            <th class="text-center">Trích Yếu</th>
                                            <th class="text-center">Xem file</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $VBDi as $di}
                                        <tr class="success">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{$di.iSoVBDi}</td>
                                            <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                            <td class="text-center">{$di.sMoTa}</td>
                                            <td class="text-center" ><a href="{$di.sDuongDan}" target="_blank">[Xem file]</a></td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/if}

                            {/if}
                            <div class="col-md-12">
<!--                                <button class="btn btn-primary" type="submit" name="luulai" value="Lưu lại">Hoàn thành công việc</button> <label for="">Người nhập văn bản này là: {!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['FK_iMaNguoiNhap'] : $vanban['sHoTen'] }</label>-->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>