<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Thông tin văn bản
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Thêm mới/Cập nhật</div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Tên tệp tin *</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="tenteptin" value="{($thongtin)?$thongtin[0]['sTenFile']:''}" required class="form-control" id="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Chọn tệp tin *</label>
                                            <div class="col-sm-9">
                                                <input type="file" name="files[]" {($thongtin)?'disabled':'required'} readonly="" placeholder="Click vào đây để chọn files" class="form-control" id="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" style="padding-right:0px;" class="col-sm-3">Thuộc văn bản</label>
                                            <div class="col-sm-9">
                                                {($kyhieu)?$kyhieu[0]['sKyHieu']:''}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for=""class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <a href="{$url}teptindi?id={$mavanban}" class="btn btn-default">Hủy bỏ</a> <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <form action="" method="post">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Tệp tin đính kèm:</label>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" width="10%">STT</th>
                                            <th class="text-center" width="30%">Tên tệp tin</th>
                                            <th class="text-center" width="20%">Tải về</th>
                                            <th class="text-center" width="20%">Ngày nhập</th>
                                            <th class="text-center" width="20%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if !empty($dsfile)}{$i=1}
                                        {foreach $dsfile as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td ><a class="tin" href="{$url}teptinden?id={$mavanban}&mafile={$f.PK_iMaFileDen}">{$f.sTenFile}</a></td>
                                                <td class="text-center"><a class="tin1" href="{$f.sDuongDan}" download>[Tải tài liệu]</a></td>
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
                                                <td class="text-center">
                                                    <button type="submit" class="btn btn-default" onclick="return confirm('Bạn có chắc muốn xóa tệp tin này?');" name="xoa" value="{$f.sDuongDan}"><i class="fa fa-trash"></i></button>
                                                    <a class="tin btn btn-default" href="{$url}teptinden?id={$mavanban}&mafile={$f.PK_iMaFileDen}"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    {/if}
                                    </tbody>
                                </table><br>
                                <a class="btn btn-default" href="javascript: history.back(1)" id="backLink">Quay lại >></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>