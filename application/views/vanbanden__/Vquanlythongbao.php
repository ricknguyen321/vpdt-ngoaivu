<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Quản lý thông báo
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="" method="post" class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nội dung<span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <textarea tabindex="1" name="noidung" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung ...">{!empty($getMessageId) &&($getMessageId) ? $getMessageId[0]['sNoiDung'] : NULL }</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày kết thúc<span class="text-danger">*</span>:</label>
                                    <div class="col-sm-8">
                                        <input tabindex="2" type="text" name="ngayketthuc" value="{!empty($getMessageId) &&($getMessageId) ? date_select($getMessageId[0]['sNgayKetThuc']) : NULL }" class="form-control datepic datemask" placeholder="Ngày kết thúc">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ẩn/Hiện nội dung<span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="radio" value="0" name="an" checked {!empty($getMessageId) &&($getMessageId[0]['iTrangThai'] == 0) ? checked : '' }> Hiện nội dung
                                        <input type="radio" value="1" name="an" {!empty($getMessageId) &&($getMessageId[0]['iTrangThai'] == 1) ? checked : '' }> Ẩn nội dung
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button type="submit" name="luu" value="luu" class="btn btn-primary btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Nội dung</th>
                                            <th>Ngày kết thúc</th>
                                            <th>Ẩn/hiện</th>
                                            <th width="10%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {$i=1}
                                        {foreach $getMessage as $tb}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td>{$tb.sNoiDung}</td>
                                            <td class="text-center">{date_select($tb.sNgayKetThuc)}</td>
                                            <td class="text-center">{($tb.iTrangThai == 0) ? 'Hiện' : 'Ẩn'}</td>
                                            <td class="text-center">
                                                <a href="{$url}quanlythongbao/{$tb.PK_iMaThongBao}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                                <button type="submit" value="{$tb.PK_iMaThongBao}" name="xoavanban" onclick="return confirm('Bạn muốn xóa văn bản{$tb.sNoiDung}?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    function myFunction() {
        window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
    }
</script>
