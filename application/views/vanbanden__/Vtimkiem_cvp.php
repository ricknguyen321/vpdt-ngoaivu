<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách văn bản {if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs" onclick="myFunction()">Tải nhiều tệp tin</button>{/if}
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="well" style="background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{$url}ketquatimkiem_cvp" target="_blank" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                                <div class="col-sm-8">
                                                    <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                        <option value="">-- Chọn loại văn bản --</option>
                                                        {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                        <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="sokyhieu" class="form-control" id="" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="ngaynhap" class="form-control datepic datemask" value="" id="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                                <div class="col-sm-8">
                                                    <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                        <option value="">-- Chọn nơi gửi đến --</option>
                                                        {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                        <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="ngayky" class="form-control datepic datemask" value="" id="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="ngayden" value="" class="form-control datepic datemask" id="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class=" col-md-12">
                                                <div class="form-group">
                                                    <label for="" class="control-label">Trích yếu</label>
                                                    <textarea name="trichyeu" id="" class="form-control" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="ngaymoi" class="form-control datepic datemask" id="" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Người ký</label>

                                                <div class="col-sm-8">
                                                    <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                        <option value="">-- Chọn người ký --</option>
                                                        {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                        <option value="{$nk.sHoTen}">{$nk.sHoTen}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Số đến</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="soden" class="form-control" id="" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                                <div class="col-sm-8">
                                                    <input type="text" name="denngay" class="form-control datepic datemask" id="" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                                <div class="col-sm-8">
                                                    <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                        <option value="">-- Chọn chức vụ --</option>
                                                        {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                        <option value="{$cv.sTenCV}" >{$cv.sTenCV}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Người nhập</label>

                                                <div class="col-sm-8">
                                                    <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                        <option value="">-- Chọn người nhập --</option>
                                                        {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                        <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button class="btn btn-primary">Tìm kiếm</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
