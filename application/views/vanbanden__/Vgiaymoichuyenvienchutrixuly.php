<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
        <h1 class="font">
            Chuyên viên xử lý
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-3"><h4><label for="">Là VB QPPL:</label> <input type="checkbox" name="iVanBanQPPL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanQPPL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">SNV chủ trì:</label> <input type="checkbox" name="iSTCChuTri" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iSTCChuTri'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-3"><h4><label for="">TBKL:</label> <input type="checkbox" name="iVanBanTBKL" value="1" {!empty($getdAppointment) &&($getdAppointment[0]['iVanBanTBKL'] == '1') ? checked : NULL }></h4></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Khu vực: </label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenKV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Số ký hiệu <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sKyHieu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Loại văn bản <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLVB'] : NULL }</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Nơi gửi đến <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenDV'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">	Ngày ký <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayKy']) : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">	Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenLV'] : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 ">Trích yếu <span class="text-info">*</span>:</label>
                                    <div class="col-sm-10">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sMoTa'] : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr class="info">
                                        <td><p class="col-sm-12"><b>Họp vào hồi:</b> {!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiGio'] : NULL }  <span class="text-info">Ngày:</span> {!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sGiayMoiNgay']) : NULL }	(Nhập giờ theo dạng hh:mm - Ví dụ: 8 giờ 30 phút -> 08:30)</p></td>
                                    </tr>
                                    <tr class="info">
                                        <td><p class="col-sm-7"><b>Địa điểm:</b>	 {!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiDiaDiem'] : NULL } 	<b></p><p class="col-sm-5">Người chủ trì:</b>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sGiayMoiChuTri'] : NULL } </p></td>
                                    </tr>
                                    <tr class="info">
                                        <td>
                                            <label for="" class="col-sm-2 ">Nội dung</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="" required value="{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sNoiDung'] : NULL } ">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Người ký <span class="text-info">*</span>: </label>

                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sTenNguoiKy'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">
                                        Ngày nhận <span class="text-info">*</span>:</label>
                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment) ? date_select($getdAppointment[0]['sNgayNhan']) : NULL }</p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Chức vụ <span class="text-info">*</span>:</label>

                                    <div class="col-sm-8">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['sChucVu'] : NULL }</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <p>{!empty($getdAppointment) &&($getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01') ? date_select($getdAppointment[0]['sHanGiaiQuyet']) : NULL }</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 " style="color: red; font-size: 16px">Số đến:</label>
                                    <div class="col-sm-2">
                                        <label style="color: red;font-size: 16px">	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoDen'] : NULL }</label>
                                    </div>
                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>

                                    <label for="" class="col-sm-1">Độ mật:</label>
                                    <div class="col-sm-2">
                                        <p>Bình thường</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 ">Số trang:</label>
                                    <div class="col-sm-10">
                                        <p>	{!empty($getdAppointment) &&($getdAppointment) ? $getdAppointment[0]['iSoTrang'] : NULL } (Trang)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 "> Tham mưu:</label>

                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Ngày phân loại:</label>

                                    <div class="col-sm-8">
                                    </div>
                                </div>
                            </div>
                            {if !empty($idDocGo)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-2 ">Trình tự giải quyết:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered table-striped">

                                        <thead>

                                        <tr>

                                            <th class="text-center">STT</th>

                                            <th class="text-center">Ngày nhận</th>

                                            <th class="text-center">Chuyển từ</th>

                                            <th width="40%">Nội dung</th>

                                            <th class="text-center">Chuyển đến</th>

                                            <th class="text-center">Hạn xử lý</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}
                                        {foreach $process as $pr}
                                        <tr class="success">
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_time($pr.sThoiGian)}</td>
                                            <td class="text-center">
                                                {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                                                {$cb.sHoTen}
                                                {/if}
                                                {/foreach}
                                            </td>
                                            <td>{$pr.sMoTa}</td>
                                            <td class="text-center">{$pr.sHoTen}</td>
                                            <td class="text-center">{if $getdAppointment[0]['sHanGiaiQuyet'] > '2017-01-01'}{date_select($getdAppointment[0]['sHanGiaiQuyet'])}{/if}<br><i>{if $pr.sThoiGianHetHan > '2017-01-01'}(Hạn VB: {date_select($pr.sThoiGianHetHan)}){/if}</i></td>
                                        </tr>
                                        {/foreach}
                                        </tbody>

                                    </table>

                                </div>

                            </div>



                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-2 ">Tệp tin đính kèm:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th class="text-center">STT</th>

                                            <th>Tên tệp tin</th>

                                            <th class="text-center">Tải về</th>

                                            <th class="text-center">Ngày nhập</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}
                                        {if !empty($Filedinhkem)}
                                        {foreach $Filedinhkem as $fi}

                                        <tr>

                                            <td class="text-center">{$i++}</td>

                                            <td>{$fi.sTenFile}</td>

                                            <td class="text-center"> <a class="tin1" href="{$fi.sDuongDan}" download>[Tải về]</a></td>

                                            <td class="text-center" width="15%">{date_select($fi.sThoiGian)}</td>

                                        </tr>

                                        {/foreach}
                                        {/if}
                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            {if !empty($resultPPH)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng phối hợp:</label>

                                </div>

                                <div class="form-group">

                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>

                                            <th>STT</th>

                                            <th>Cán bộ-đơn vị</th>

                                            <th>Nội dung</th>

                                            <th class="text-center">Tải về</th>

                                            <th>Ngày nhập</th>

                                        </tr>

                                        </thead>

                                        <tbody>

                                        {$i=1}

                                        {foreach $resultPPH as $re}

                                        <tr >

                                            <td class="text-center">{$i++}</td>

                                            <td width="20%">{$re.sHoTen} - {$re.sTenPB}</td>

                                            <td>{$re.sMoTa}</td>

                                            <td width="8%" class="text-center"><a href="{$re.sDuongDanFile}">[Tải tài liệu]</a></td>

                                            <td width="15%" class="text-center">{date_select($re.date_file)}</td>

                                        </tr>

                                        {/foreach}

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            {/if}
                            {if !empty($resultPTL)}

                            <div class="col-sm-12">

                                <div class="form-group">

                                    <label for="" class="col-sm-10 ">Kết quả giải quyết - phòng thụ lý:</label>

                                </div>

                                <div class="form-group">
                                    <form action="" method="post">
                                    <table class="table table-bordered">

                                        <thead>

                                        <tr>
                                            <th>STT</th>
                                            <th>Cán bộ-đơn vị</th>
                                            <th>Nội dung</th>
                                            <th class="text-center">Tải về</th>
                                            <th class="text-center">Ngày nhập</th>
                                            <th class="text-center">Tác vụ</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $resultPTL as $tl}
                                        <tr >
                                            <td class="text-center">{$i++}</td>
                                            <td width="20%">{$tl.sHoTen} - {$tl.sTenPB}</td>
                                            <td>{$tl.sMoTa}</td>
											{if !empty($tl.sDuongDanFile)}
                                            <td width="8%" class="text-center"><a href="{$tl.sDuongDanFile}">[Tải tài liệu]</a></td>
											{else}
											<td class="text-center"> - </td>
											{/if}
                                            <td width="15%" class="text-center">{date_select($tl.date_file)}</td>
                                            {if $tl.active == 2}
                                                <td width="8%" class="text-center"><button type="submit" value="{$tl.PK_iMaKetQua}" name="xoafile" onclick="return confirm('Bạn muốn xóa {$tl.sMoTa} ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button></td>
											{else}
											<td class="text-center"> Đã duyệt</td>
											{/if}
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                    </form>
                                </div>

                            </div>

                            {/if}
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                            {if $getdAppointment[0]['iTrangThai'] != 1  || $getdAppointment[0]['PK_iMaCBHoanThanh'] == $vanban['PK_iMaCB']}
                            <div class="col-sm-12">

                                <div class="col-sm-6">

                                    <div class="form-group">

                                        <label for="" class="col-sm-12">Kết quả giải quyết <br><span style="color: red">  <i>(chỉ nhập khi có kết quả giải quyết)</i></span></label>

                                        <div class="col-sm-12">

                                            <textarea name="ketqua" id="" class="form-control" rows="4"></textarea>

                                        </div>

                                    </div>

                                </div>
                                {if $getdAppointment[0]['sHanGiaiQuyet'] <= date('Y-m-d',time())}
                                <br><br>
                                <!--<div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="form-group" id="themtailieu">
                                                <label for="" class="col-sm-12">
                                                    Chọn lãnh đạo chỉ đạo: <span class="text-danger">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="lanhdao" id="" class="form-control phogiamdoc1" style="cursor:pointer" required>
                                                        <option value=""> --- Chọn Lãnh đạo đã chỉ đạo văn bản ---</option>
                                                        <optgroup label="Chọn Trưởng phòng">
                                                            <option value="{$truongphong.PK_iMaCB}">{$truongphong.sHoTen}</option>
                                                        </optgroup>
                                                        <optgroup label="Chọn Phó phòng">
                                                            {foreach $getAccountDeputyDirector as $pgd}
                                                            <option value="{$pgd.PK_iMaCB}">{$pgd.sHoTen}</option>
                                                            {/foreach}
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-sm-6">
                                    <div class="form-group">
<!--                                        <label for="" class="col-sm-12">Lý do chưa hoàn thành <br> <span style="color: red"><i>(trường hợp chưa kết thúc được văn bản thì nhập lý do, tiến độ giải quyết-sau đó click vào ô "hoàn thành công việc")</i></span></label>-->
                                        <div class="col-sm-12">
<!--                                            <textarea name="chammuon" id="" class="form-control" rows="4"></textarea>-->
                                            <div class="form-group">
                                                <label for="" class="col-sm-12">
                                                    Báo cáo phục vụ họp: <span class="text-danger">*</span></label>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6">
                                                        <input type="text" name="fileketqua" class="form-control" placeholder="Tên file" value="Báo cáo">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="file" name="files" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="form-group noidung" id="themtailieu">
                                                <label for="" class="col-sm-12">
                                                    Đính thêm tài liệu: <span class="text-danger">*</span></label>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6">
                                                        <input type="text" name="filetailieu[]" class="form-control" placeholder="Nhập số ký hiệu" value=""><br>
                                                        <input type="text" name="ngaythangnam[]" class="form-control datepic datemask" placeholder="Nhập dd/mm/yyyy" value="{date('d/m/Y',time())}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="file" name="files1[]" value="" multiple>
                                                    </div>
                                                </div>
                                                <script>
                                                    $(document).ready(function() {
                                                        //Date picker
                                                        $('.datepic').datepicker({
                                                            autoclose: true
                                                        });
                                                        //Datemask dd/mm/yyyy
                                                        $(".datemask").inputmask("dd/mm/yyyy");
                                                    });
                                                </script>
                                            </div>
                                            <div class="themsau">
                                            </div>
                                            <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;"><i class="fa fa-plus"></i></button><b class="text-danger"> Thêm tài liệu đính kèm</b>
                                        </div>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            {/if}
                            {/if}
                            {if $getdAppointment[0]['iTrangThai'] != 1  || $getdAppointment[0]['PK_iMaCBHoanThanh'] == $vanban['PK_iMaCB']}
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-sm" type="submit" name="luulai" value="Lưu lại">Hoàn thành công việc</button>
								<a href="{$url}soanbaocao/{$getdAppointment[0]['PK_iMaVBDen']}" target="_blank"  class="btn btn-danger btn-xs">Soạn báo cáo kết quả họp</a>
                            </div>
                            {/if}
                            </form>
                        </div>
                    </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
        $('.').css('text-align', 'left');
    });
</script>