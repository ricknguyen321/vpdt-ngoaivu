<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách phòng phối hợp
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Loại văn bản:</label>

                                <div class="col-sm-7 sl2">
                                    <div class="row">
                                        <select name="cacloaivanban" id="" class="form-control select2" style="width:100%" onchange="javascript:handleSelect(this)">
                                            <option value="dsphongchutri" >-- Tất cả --</option>
                                            <option value="{if $vanban['iQuyenHan_DHNB'] == 7}vanbanchoxuly_pp{/if}{if $vanban['iQuyenHan_DHNB'] == 8}vanbanchoxuly_cv{/if}{if $vanban['iQuyenHan_DHNB'] == 6}vanbanchoxuly_tp{/if}" {!empty($urlcv)&&($urlcv)?($urlcv=="vanbanchoxuly_cv")?'selected':'':''}>Văn bản mới nhận</option>
                                            <option value="dsphoihop" {!empty($urlcv)&&($urlcv)?($urlcv=="dsphoihop")?'selected':'':''}>Phối hợp giải quyết</option>
                                            <option value="dsphongchutri?cacloaivanban=1">Đã chỉ đạo chưa giải quyết</option>
                                            <option value="dsvanbanphongdaxuly">Đã giải quyết</option>
                                        </select>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    function handleSelect(elm)
                                    {
                                        window.location = elm.value;
                                    }
                                </script>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Tìm kiếm văn bản</a><b>({$count} văn bản)</b></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Main content -->

    <section class="content">

        <!-- Modal -->

        <div class="modal fade phongphoihop" id="myModal" role="dialog" aria-labelledby="myModalLabel">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        <h4 class="modal-title" id="myModalLabel"><b>Chọn phòng ban phối hợp</b></h4>

                    </div>

                    <div class="modal-body">

                        <div class="row" >

                            <form role="form" action="" id="Q_A" method="POST">

                                {foreach $getDepartment as $de}

                                <div class="col-sm-6">

                                    <div class="col-sm-12">

                                        <input type="checkbox" name="chonphong" value="{$de.PK_iMaCB}" data-id="{$de.sHoTen}" class="Checkbox"> {$de.sHoTen}<br>

                                    </div>

                                </div>

                                {/foreach}

                                <input type="text" name="mangphongban" value="" class="hidden" >

                                <input type="text" name="phongchutri1" value="" class="hidden" >

                            </form>

                        </div>

                    </div>

                    <div class="modal-footer">

                        <button type="button" name="ghilai" class="btn btn-primary" data-dismiss="modal">Ghi lại</button>

                    </div>

                </div>

            </div>

        </div>
        <div class="modal fade" id="myModal3" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Từ chối văn bản</b></h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <label for="" class="col-sm-12">Nhập nội dung từ chối</label>
                                    <div class="col-sm-12">
                                        <textarea name="noidungtuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập nội dung từ chối...." required></textarea>
                                    </div>
                                </div>
                                <input type="text" name="maphongban" value="" class="hidden">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Chuyển chánh van phong">Chuyển về Trưởng phòng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Default box -->

        <div class="box">

            <div class="box-body">
                <div class="row">

                    <div class="col-sm-12">

                        <div class="collapse" id="collapseExample">

                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">

                                <div class="row">

                                    <form action="" method="get" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Loại văn bản:</label>
                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số ký hiệu:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{!empty($sokyhieu)&&($sokyhieu)?$sokyhieu:''}" placeholder="Nhập số kí hiệu">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Trích yếu:</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Nơi gửi đến:</label>
                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">	Ngày ký:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{!empty($ngayky)&&($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Ngày nhập từ:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngaynhap" value="{!empty($ngaynhap)&&($ngaynhap)?$ngaynhap:''}" class="form-control datepic datemask" id="">
                                                    </div>

                                                    <label for="" class="col-sm-2">đến:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="ngayden" value="{!empty($ngayden)&&($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea name="trichyeu" id="" class="form-control" rows="4" placeholder="Nhập trích yếu">{!empty($trichyeu)&&($trichyeu)?$trichyeu:''}</textarea><br/>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người ký:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Số đến:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{!empty($soden)&&($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Chức vụ:</label>
                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4">Người nhập:</label>
                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-primary">Tìm kiếm</button>
                                                </div>
                                            </div>
                                    </form>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12">

                        <form action="" method="post">
                            <span class="pull-right deletepadding1" style="    margin-right: 0px;">{$phantrang}</span>
                            <br>

                            <table id="" class="table table-bordered table-striped">

                                <thead>

                                <tr>

                                    <th width="5%" class="text-center">STT</th>

                                    <th width="" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Loại văn bản</th>

                                    <th class="text-center">Số đến</th>

                                    <th class="text-center">Số ký hiệu</th>

                                    <th class="text-center">Nơi gửi</th>

                                    <th width="" class="text-center">Trích yếu - Thông tin</th>

                                    <th width="20%" class="text-center">Trình tự xử lý</th>

                                    <th width="" class="text-center">Tệp tin</th>

                                </tr>

                                </thead>

                                <tbody>

                                {$i=1}

                                {if !empty($getDocAwaitPPH)}

                                {foreach $getDocAwaitPPH as $pph}

                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td>{date_select($pph.sNgayNhap)}</td>
                                    <td class="text-center">{$pph.sTenLVB}</td>
                                    <td class="text-center"><b style="color: red; font-size: 16px">{$pph.iSoDen}</b></td>
                                    <td class="text-center">{$pph.sKyHieu}</td>
                                    <td>{$pph.sTenDV}</td>
                                    <td width="30%">
                                        {if $pph.iGiayMoi == '1'}
                                        <a href="{$url}giaymoichuyenvienxuly/{$pph.PK_iMaVBDen}" style="color: #0c0b0b"><b>{if !empty($pph.sNoiDung)}{$pph.sNoiDung}{else}{$pph.sMoTa}{/if}</b></a><br>
                                        (Vào hồi {$pph.sGiayMoiGio} ngày {date_select($pph.sGiayMoiNgay)}, tại {$pph.sGiayMoiDiaDiem}) |
                                        (Số trang: {$pph.iSoTrang})
                                        {else}
                                        <a href="{$url}chuyenvienxuly?vbd={$pph.PK_iMaVBDen}" style="color: #0c0b0b"><b>{$pph.sMoTa}</b></a><br>
                                        (Số trang: {$pph.iSoTrang})
                                        <b>{if !empty($pph.TrinhTu)}{$pph.TrinhTu}{/if}</b>
                                        {/if}
<!--                                        <span>Chỉ đạo của phòng: Đồng chí</span> <i>{$pph.mieuta}</i>-->
                                    </td>
                                    <td class="30%">
                                        {$a=1}
                                        {foreach $pph.TrinhTuPH as $pr}
                                        <p>{$a++}. {$pr}</p>
                                        <hr style="border-bottom: 1px dashed gold; margin: 5px;border-top:0px">
                                        {/foreach}
                                    </td>
                                    <td class="text-center">
                                        <a class="tin1" href="{$url}teptinden?id={$pph.PK_iMaVBDen}" target="_blank"><i class="fa {if $pph.demtin == 0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $pph.demtin  > 0} <br> <a target="_blank" href="{$pph.sDuongDan}" target="_blank" class="tin1">{if layDuoiFile($pph.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}
                                        <br>{if $vanban['iQuyenHan_DHNB'] == 7}
                                        <button type="button" name="abc"  value="{$pph.PK_iMaVBDen}" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal3" >Từ chối về TP</button>
                                        <!--                                        <input type="text" name="ma_cb" value="{$pl.PK_iMaPhongPH}">-->
                                        {/if}
                                    </td>
                                </tr>
                                {/foreach}
                                {/if}
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $("button[name=abc]").each(function (i, e) {
            $(this).click(function () {
                $('input[name=maphongban]').val($(this).val());
            });
        });
    });
</script>
