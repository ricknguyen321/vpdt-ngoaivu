<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách văn bản
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="myModal3" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><b>Lý do từ chối</b></h4>
                </div>
                <form action="" method="post">
                    <div class="modal-body">
                        <div class="row" >
                            <div class="col-sm-12">
                                <label for="" class="col-sm-12">Lý do từ chối</label>
                                <div class="col-sm-12">
                                    <textarea name="lydotuchoi" id="" cols="30" rows="5" class="form-control" placeholder="Nhập lý do...."></textarea>
                                </div>
                            </div>
                            <input type="text" name="mavbd" value="" class="hidden">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="tuchoi" class="btn btn-primary btn-sm" value="Từ chối">Từ chối</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="" class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>

                                                    <div class="col-sm-8">
                                                        <select name="loaivanban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="">-- Chọn loại văn bản --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_loaivanban') as $lvb}
                                                            <option value="{$lvb.sTenLVB}" {!empty($loaivanban)&&($loaivanban)?($loaivanban==$lvb.sTenLVB)?'selected':'':''}>{$lvb.sTenLVB}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="sokyhieu" class="form-control" id="" value="{($sokyhieu)?$sokyhieu:''}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày nhập từ</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaynhap" class="form-control datepic datemask" value="{(ngaynhap)?ngaynhap:''}" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến</label>

                                                    <div class="col-sm-8">
                                                        <select name="donvi" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn nơi gửi đến --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_donvi') as $dv}
                                                            <option value="{$dv.sTenDV}" {!empty($donvi)&&($donvi)?($donvi==$dv.sTenDV)?'selected':'':''}>{$dv.sTenDV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Ngày ký</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayky" class="form-control datepic datemask" value="{($ngayky)?$ngayky:''}" id="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Nhập đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngayden" value="{($ngayden)?$ngayden:''}" class="form-control datepic datemask" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class=" col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Trích yếu</label>
                                                        <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Giấy mời từ ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="ngaymoi" class="form-control datepic datemask" id="" value="{($ngaymoi)?$ngaymoi:''}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Người ký</label>

                                                    <div class="col-sm-8">
                                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn người ký --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_nguoiky') as $nk}
                                                            <option value="{$nk.sHoTen}" {!empty($nguoiky)&&($nguoiky)?($nguoiky==$nk.sHoTen)?'selected':'':''}>{$nk.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Số đến</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="soden" class="form-control" id="" value="{($soden)?$soden:''}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                                    <div class="col-sm-8">
                                                        <input type="text" name="denngay" class="form-control datepic datemask" id="" value="{($denngay)?$denngay:''}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                                    <div class="col-sm-8">
                                                        <select name="chucvu" id="" class="form-control select2" style="width:100%" >
                                                            <option value="">-- Chọn chức vụ --</option>
                                                            {foreach layDuLieu(NULL,NULL,'tbl_chucvu') as $cv}
                                                            <option value="{$cv.sTenCV}" {!empty($chucvu)&&($chucvu)?($chucvu==$cv.sTenCV)?'selected':'':''}>{$cv.sTenCV}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Người nhập</label>

                                                    <div class="col-sm-8">
                                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                                            <option value="">-- Chọn người nhập --</option>
                                                            {foreach layDuLieu('iQuyenHan_DHNB','9','tbl_canbo') as $nn}
                                                            <option value="{$nn.PK_iMaCB}" {!empty($nguoinhap)&&($nguoinhap)?($nguoinhap==$nn.PK_iMaCB)?'selected':'':''} >{$nn.sHoTen}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                    <div class="col-sm-8 deletepadding">
                        {$phantrang}
                    </div>
                    <b>{$count} văn bản.</b>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="6%" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Loại văn bản</th>
                                    <th width="" class="text-center">Số đến</th>
                                    <th width="30%" class="text-center">Trích yếu</th>
                                    <th width="15%" class="text-center">Nơi gửi</th>
                                    <th width="" class="text-center">Phòng chủ trì</th>
                                    <th width="25%" class="text-center">Duyệt hạn giải quyết</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i = 1}
                                {foreach $getDocGo as $vbd}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td>{date_select($vbd.sNgayNhap)}</td>
                                    <td style="color: red; font-size: 18px" class="text-center"><b>{$vbd.iSoDen}</b></td>
                                    <td class="text-center">{$vbd.sKyHieu}</td>
                                    <td>
                                        <a style="color:black;" href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}"><span><b>{$vbd.sMoTa}</b></span></a>
                                        <br>
                                        <p><i>Người nhập: {$vbd.sHoTen} </i></p><span><i>{if !empty($vbd.sNoiDung)}Nội dung*: {$vbd.sNoiDung}{/if}</i></span>
                                        <p><a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}"><i class="fa {if $vbd.iFile==0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $vbd.iFile==1} <a target="_blank" href="{$vbd.sDuongDan}" class="tin1">{if layDuoiFile($vbd.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}</p>
                                    </td>
                                    <td>{$vbd.sTenDV}</td>
                                    <td class="text-center" style="padding-top: 20px">
                                        <b>{$vbd.sPhongChuTri}</b>
                                    </td>
                                    <td>
                                        <label for="">Hạn đề xuất</label>
                                        <input type="text" name="handexuat[{$vbd.PK_iMaVBDen}]" value="{(!empty($vbd.sHanChoDuyet)&&($vbd.sHanChoDuyet > '2017-01-01') ? date_select($vbd.sHanChoDuyet) : NULL)}" class="form-control datepic datemask" placeholder="Nhập gia hạn...." required><br>
                                        <p><b>Lý do đề xuất: </b><br><i>{$vbd.sNoiDungGiaHan}</i></p>
                                        <input type="text" name="gheplydo[{$vbd.PK_iMaVBDen}]" value="{$vbd.sNoiDungGiaHan}" class="hidden">
                                        {if $vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5 || $vanban['iQuyenHan_DHNB'] == 3}
                                        <button type="submit" name="duyethan" class="btn btn-primary btn-xs" value="{$vbd.PK_iMaVBDen}">Duyệt Hạn</button>
                                        <button type="button" name="tuchoihan[{$vbd.PK_iMaVBDen}]" class="btn btn-warning btn-xs tuchoihan" value="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#myModal3" >Từ chối hạn</button>
                                        {/if}
                                        {if $vanban['iQuyenHan_DHNB'] == 6 || $vanban['iQuyenHan_DHNB'] == 3}
                                        <label for="">Lý do đề xuất lên Lãnh đạo</label>
                                        <textarea name="noidungdexuat[{$vbd.PK_iMaVBDen}]" id="" class="form-control lydo" rows="5" placeholder="Nhập lý do đề xuất"></textarea><br>
										{if $vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] == 15}
                                        <button type="submit" name="duyethancb" class="btn btn-primary btn-xs lanhdao" value="{$vbd.PK_iMaVBDen}">Gửi Lãnh đạo ({$vbd.ChiDao})</button>
										{else}
										<button type="submit" name="duyethancb" class="btn btn-primary btn-xs hidden lanhdao" value="{$vbd.PK_iMaVBDen}">Gửi Lãnh đạo ({$vbd.ChiDao})</button>
                                        <button type="button" name="luuao" class="btn btn-primary btn-xs lanhdao1" disabled>Gửi Lãnh đạo ({$vbd.ChiDao})</button>
										{/if}
                                        <button type="button" name="tuchoihan[{$vbd.PK_iMaVBDen}]" class="btn btn-warning btn-xs tuchoihan" value="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#myModal3" >Từ chối hạn</button>
                                        <input type="hidden" name="idlanhdao[{$vbd.PK_iMaVBDen}]" value="{$vbd.PK_iMaLDCD}">
                                        {/if}
                                        {if $vanban['iQuyenHan_DHNB'] == 7 || $vanban['iQuyenHan_DHNB'] == 11}
                                        <label for="">Lý do đề xuất lên Trưởng phòng</label>
                                        <textarea name="noidungdexuattp[{$vbd.PK_iMaVBDen}]" id="" class="form-control lydo" rows="5" placeholder="Nhập lý do đề xuất"></textarea><br>
                                        <button type="submit" name="duyethanpp" class="btn btn-primary btn-xs hidden" value="{$vbd.PK_iMaVBDen}">{if $vanban['iQuyenHan_DHNB'] == 7}Gửi Trưởng phòng{else}Gửi Chi cục trưởng{/if}</button>
                                        <button type="button" name="luuao" class="btn btn-primary btn-xs" disabled>{if $vanban['iQuyenHan_DHNB'] == 7}Gửi Trưởng phòng{else}Gửi Chi cục trưởng{/if}</button>
                                        <button type="button" name="tuchoihan[{$vbd.PK_iMaVBDen}]" class="btn btn-warning btn-xs tuchoihan" value="{$vbd.PK_iMaVBDen}" data-toggle="modal" data-target="#myModal3" >Từ chối hạn</button>
                                        <input type="hidden" name="idlanhdao[{$vbd.PK_iMaVBDen}]" value="{$vbd.PK_iMaLDCDPP}">
                                        {/if}
                                    </td>
                                </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        {$phantrang}
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    function myFunction() {
        window.open("{$url}upload_files_vbden","mywindow","menubar=1,resizable=1,width=550,height=320");
    }
</script>
<script>
    $(document).ready(function() {
        $(document).on('keyup','.lydo',function () {
            $(this).parent().find('.lanhdao').removeClass('hidden');
            $(this).parent().find('.lanhdao1').addClass('hidden');
        });
        $(document).on('keyup','.lydo',function () {
            $(this).parent().find('button[name=duyethanpp]').removeClass('hidden');
            $(this).parent().find('button[name=luuao]').addClass('hidden');
        });
		$(document).on('click','.tuchoihan',function () {
            $('input[name=mavbd]').val($(this).val());
        });
    });
</script>
