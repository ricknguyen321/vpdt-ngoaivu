<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách văn bản tìm kiếm
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">  
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-12 deletepadding">
                    {$phantrang}
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="6%" class="text-center">Ngày nhập</th>
                                    <th width="" class="text-center">Loại văn bản</th>
                                    <th width="" class="text-center">Số đến</th>
                                    <th width="" class="text-center">Số ký hiệu</th>
                                    <th width="30%" class="text-center">Trích yếu</th>
                                    <th width="15%" class="text-center">Nơi gửi</th>
                                    <th width="5%" class="text-center">Tệp tin</th>
                                    <th width="5%" class="text-center">Xóa</th>
                                    <th width="" class="text-center">Phòng chủ trì</th>
                                    <th width="" class="text-center">Kết quả thực hiện</th>
                                </tr>
                            </thead>
                            <tbody>
                            {$i = 1}
                            {foreach $getDocGo as $vbd}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td>{date_select($vbd.sNgayNhap)}</td>
                                <td class="text-center">{$vbd.sTenLVB}</td>
                                <td style="color: red; font-size: 16px" class="text-center"><b>{$vbd.iSoDen}</b></td>
                                <td class="text-center">{$vbd.sKyHieu}</td>
                                <td>
                                    {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9 || $vanban['iQuyenHan_DHNB'] == 3}
                                        {if $vbd.iGiayMoi == '1'}
                                        <a href="{$url}nhapgiaymoi/{$vbd.PK_iMaVBDen}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a> <br>
                                        (Vào hồi {$vbd.sGiayMoiGio} ngày {date_select($vbd.sGiayMoiNgay)}, tại {$vbd.sGiayMoiDiaDiem}) |
                                        (Số trang: {$vbd.iSoTrang}) |
                                        <b>{$vbd.sChuTri}</b><br>
                                        {else}
                                        <a href="{$url}vanbanden/{$vbd.PK_iMaVBDen}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a> <br>
                                        (Số trang: {$vbd.iSoTrang})
                                            <b>{$vbd.sChuTri}</b><br>
                                        {/if}
                                    {else}
                                        {if $vbd.iGiayMoi == '1'}
                                        <a href="{$url}quytrinhgiaymoi/{$vbd.PK_iMaVBDen}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a><br>
                                        (Vào hồi {$vbd.sGiayMoiGio} ngày {date_select($vbd.sGiayMoiNgay)}, tại {$vbd.sGiayMoiDiaDiem}) |
                                        (Số trang: {$vbd.iSoTrang}) |
                                        <b>{$vbd.sChuTri}</b><br>
                                        {else}
                                        <a href="{$url}quatrinhxuly/{$vbd.PK_iMaVBDen}" style="color: #0c0b0b"><b>{$vbd.sMoTa}</b></a><br>
                                        (Số trang: {$vbd.iSoTrang})
                                        <b>{$vbd.sChuTri}</b><br>
                                        {/if}
                                    {/if}
                                    <p><i>Người nhập: {$vbd.sHoTen} </i></p><span><i>{if !empty($vbd.sNoiDung)}Nội dung*: {$vbd.sNoiDung}{/if}</i></span><br></td>
                                <td>{$vbd.sTenDV}</td>
                                <td class="text-center"><a class="tin1" href="{$url}teptinden?id={$vbd.PK_iMaVBDen}"><i class="fa {if $vbd.iFile==0}fa-folder-open-o{else} fa-search{/if}"></i></a>{if $vbd.iFile==1} <br> <a target="_blank" href="{$vbd.sDuongDan}" class="tin1">{if layDuoiFile($vbd.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if}</td>
                                <td class="text-center" >
                                    {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 3 || $vanban['iQuyenHan_DHNB'] == 9}
                                    {if $vanban['iQuyenHan_DHNB'] == 2 || $vanban['iQuyenHan_DHNB'] == 9}
                                    <button type="submit" value="{$vbd.PK_iMaVBDen}" name="xoavanban" onclick="return confirm('Bạn muốn xóa văn bản{$vbd.sMoTa} ?')" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Xóa" style="margin-top: 20px"><i class="fa fa-trash"></i></button>
                                    {/if}
                                    <a href="{if $vbd.iGiayMoi == '1'}{$url}nhapgiaymoi/{$vbd.PK_iMaVBDen}{else}{$url}vanbanden/{$vbd.PK_iMaVBDen}{/if}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="sửa văn bản" style="margin-top: 20px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    {/if}
                                </td>
                                <td class="text-center" style="padding-top: 20px">
                                    <b>{$vbd.sPhongChuTri}</b>
                                </td>
                                <td style="color: blue; font-size: 14px" class="text-center"><b>{if !empty($vbd.VBDi)} có văn bản trả lời{/if}</b></td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <div><b>Tổng văn bản: {$count}</b></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    {$phantrang}
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
