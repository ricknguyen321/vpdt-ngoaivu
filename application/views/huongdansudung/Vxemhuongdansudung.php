<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Hướng dẫn sử dụng
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        {if !empty($quyenhan)}{$i=1}
                            {foreach $quyenhan as $key => $qh}
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="{$qh.PK_iMaQuyen}">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{$key}_{$qh.PK_iMaQuyen}" aria-expanded="false" aria-controls="{$key}_{$qh.PK_iMaQuyen}">
                                               {$i++}.{$qh.sTenQuyen}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="{$key}_{$qh.PK_iMaQuyen}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{$qh.PK_iMaQuyen}">
                                        <div class="panel-body">
                                        {if !empty($dsdulieu)}{$ii=1}
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            {foreach $dsdulieu as $k => $dl}
                                                {if $qh.PK_iMaQuyen==$dl.FK_iMaQuyen}
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading" role="tab">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" role="button"  data-target="#{$k}1">
                                                                    {$ii++}.{$dl.sTenChucNang}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="{$k}1" class="panel-collapse collapse" role="tabpanel">
                                                            <div class="panel-body">
                                                                {$dl.sHuongDan}
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/if}
                                            {/foreach}
                                            </div>
                                        {/if}
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                </div>
            </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('img').css('max-width', '100%');
    });
</script>