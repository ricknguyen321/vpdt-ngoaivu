<script type="text/javascript" src="{$url}assets/js/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{$url}ckfinder/ckfinder.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Viết hướng dẫn sử dụng
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Tên chức năng <label class="required" for="">*</label></label>
                                <input type="text" name="chucnang" class="form-control" placeholder="Nhập tên chức năng" required value="{($thongtin)?($thongtin[0]['sTenChucNang']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="">Quyền hạn <label class="required" for="">*</label></label>
                                <select name="quyenhan" id="" class="form-control select2" required style="width: 100%">
                                    <option value="">-- Chọn quyền hạn --</option>
                                    {if !empty($quyenhan)}
                                        {foreach $quyenhan as $qh}
                                            <option value="{$qh.PK_iMaQuyen}" {($thongtin)?($thongtin[0]['FK_iMaQuyen']==$qh.PK_iMaQuyen)?'selected':'':''}>{$qh.sTenQuyen}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Hướng dẫn sử dụng</label>
                               <textarea name="huongdan" class="" required id="editor1" rows="4">{($thongtin)?($thongtin[0]['sHuongDan']):NULL}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="1" class="flat-red" checked>  Hiển thị
                                </label>
                                <label style="padding-left:25px">
                                    <input type="radio" name="trangthai" value="2" {if !empty($thongtin)} {if $thongtin[0]['iTrangThai']==2}checked{/if}{/if} class="flat-red">  Không hiển thị
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật':'Thêm mới'}</button>
                                {if !empty($thongtin)}<a href="{$url}viethuongdansudung" class="btn btn-default pull-right" >Quay lại >></a>{/if}
                            </div>
                        </div>
                    </div>
                </div>
                <br><hr style="border-bottom: 5px solid #438796">
                <div class="row">
                    <div class="col-md-12"  style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" class="text-center">STT</th>
                                    <th width="50%">Tên chức năng</th>
                                    <th width="20%">Tên quyền</th>
                                    <th width="20%">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsdulieu)}{$i=1}
                                    {foreach $dsdulieu as $dl}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td><a href="{$url}viethuongdansudung?id={$dl.PK_iMaChucNang}">{$dl.sTenChucNang}</a></td>
                                            <td>{$dl.sTenQuyen}</td>
                                            <td><label for="" class="label {($dl.iTrangThai==1)?'label-success':'label-warning'}">{($dl.iTrangThai==1)?'Hiển thị':'Không hiển thị'}</label></td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    var editor = CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl: '/dieuhanhnoibo/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '/dieuhanhnoibo/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
    } );
</script>