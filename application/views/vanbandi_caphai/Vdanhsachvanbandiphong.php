<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Danh sách văn bản đi
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a  class="font" name="anhien">Tìm kiếm văn bản</a></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
<!--                <label for=""><a style="font-size:20px;" name="anhien" href="javascript:void(0);">Tìm kiếm văn bản</a></label>-->
                <form class="form-horizontal hide" method="get" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn loại văn bản --</option>
                                            {if !empty($dsloaivanban)}
                                                {foreach $dsloaivanban as $l}
                                                <option value="{$l.PK_iMaLVB}" {($loaivanban)?($l.PK_iMaLVB==$loaivanban)?'selected':'':''}>{$l.sTenLVB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số đi</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi dự thảo</label>

                                    <div class="col-sm-8">
                                        <select name="noiduthao" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn nơi dự thảo --</option>
                                            {if !empty($dsduthao)}
                                                {foreach $dsduthao as $d}
                                                    <option value="{$d.PK_iMaPB}" {($noiduthao)?($noiduthao==$d.PK_iMaPB)?'selected':'':''}>{$d.sTenPhong}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày văn bản</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngaythang" value="{($ngaythang)?$ngaythang:''}" class="form-control datepic datemask" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class=" col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Trích yếu</label>
                                        <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký</label>

                                    <div class="col-sm-8">
                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%;">
                                            <option value="">-- Chọn người ký --</option>
                                            {if !empty($nguoiky)}
                                                {foreach $nguoiky as $nk}
                                                    <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6 hide">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người nhập</label>

                                    <div class="col-sm-8">
                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn người nhập --</option>
                                            {if !empty($dsnguoinhap)}
                                                {foreach $dsnguoinhap as $nn}
                                                    <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end quyền bằng 2 là văn thư form-->
                <div class="col-md-12">
                    {if isset($thongbaoso)}
                    <h3 class="formSentMsg" style="background:#b2dde0; padding:18px;">Số vừa duyệt là: <b style="color:red;">{$thongbaoso}</b></h3>
                    {/if}
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%">Loại văn bản</th>
                                    <th width="10%">Số đi</th>
                                    <th width="10%">Số ký hiệu</th>
                                    <th width="10%" class="text-center" >Ngày tháng</th>
                                    <th width="28%">Trích yếu</th>
                                    <th width="17%">Nơi nhận</th>
                                    <th width="6%">Tệp tin</th>
                                    <th width="5%">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sTenLVB}</td>
                                        <td class="text-center" style="font-size:18px;color: red"><b>{$dl.iSoVBDi}</b></td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td class="text-center">{date_select($dl.sNgayVBDi)}</td>
                                        <td ><a href="{$url}thongtinvanbandi?id={$dl.PK_iMaVBDi}" class="tin" style="color: black !important;"><b>{$dl.sMoTa}</b></a>{if !empty($dl.iSoDen)}<br> <b>Trả lời cho văn bản đến số:</b><b style="color:red;">{$dl.iSoDen}</b>{/if}<br><i>(Người ký: {$dl.sHoTen})</i><p><b>(Ngày nhập: {date_select($dl.sNgayNhap)})</p></b></td>
                                        <td>{$dl.sNoiNhan}</td>
                                        <td class="text-center"><a class="tin1" href="{$url}teptindi_caphai?id={$dl.PK_iMaVBDi}"><i class="fa {if empty($dl.sDuongDan)}fa-folder-open-o{else} fa-search{/if}"></i></a> <br>
                                            {if !empty($dl.sDuongDan)}
                                            <a target="_blank" href="{$dl.sDuongDan}" class="tin1">{if layDuoiFile($dl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>
                                            {/if}
                                        </td>
                                        <td class="text-center">
                                            
                                            -
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">{$phantrang}</div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(".formSentMsg").delay(5200).fadeOut(300);
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.form-horizontal').toggle();
            $('.form-horizontal').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>