<script src="{$url}assets/js/vanbandi/vanbandicaphai.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới văn bản đi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số văn bản</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="sovanban" value="{($thongtin)?($thongtin[0]['iSoVBDi'])?$thongtin[0]['iSoVBDi']:'':''}" placeholder="VĂN THƯ CẤP SỐ" {if !empty($thongtin)}{if $vanban['quyenhan_caphai']!=6}readonly=""{/if}{/if} class="form-control" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản *</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" id="" required class="form-control  kyhieu select2" style="width:100%">
                                            <option value="">-- Chọn loại văn bản --</option>
                                            {if !empty($dsloaivanban)}
                                                {foreach $dsloaivanban as $l}
                                                <option value="{$l.PK_iMaLVB}" {($thongtin)?($thongtin[0]['FK_iMaLVB']==$l.PK_iMaLVB)?'selected':'':''}>{$l.sTenLVB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày tháng *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control datepic datemask"  required id="" value="{($thongtin)?date_select($thongtin[0]['sNgayVBDi']):date('d/m/Y')}" name="ngaythang">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi dự thảo *</label>

                                    <div class="col-sm-8">
                                        <select name="noiduthao" required id="" class="form-control select2" style="width:100%">
                                            {if !empty($dsduthao)}
                                                {foreach $dsduthao as $d}
                                                    <option value="{$d.PK_iMaPB}" {if !empty($thongtin)}{($thongtin[0]['FK_iMaPB']=={$d.PK_iMaPB})?'selected':''}{else}{($vanban['phong_caphai']==$d.PK_iMaPB)?'selected':''}{/if}>{$d.sTenPhong}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Trích yếu *</label>
                                        <textarea name="mota" id="" required class="form-control" rows="3">{($thongtin)?$thongtin[0]['sMoTa']:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Nơi nhận* </label>
                                        <input type="text" class="form-control" name="noinhan" value="{$thongtin['0']['sNoiNhan']}" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group an {if empty($loai) || $loai!='giaymoi'}hide{/if}">
                                    <label for="" class="col-sm-4 control-label">Ngày mời</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngaymoi" tabindex="8" value="{($thongtin)?($thongtin[0]['sNgayMoi']!=0000-00-00)?date_select($thongtin[0]['sNgayMoi']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" class="form-control" value="{($thongtin)?$thongtin[0]['iSoTrang']:''}" id="" name="sotrang">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký *</label>

                                    <div class="col-sm-8">
                                        <select name="nguoiky" id="" required class="form-control select2">
                                            <option value="">-- Chọn người ký --</option>
                                            {if !empty($nguoiky)}
                                                {foreach $nguoiky as $nk}
                                                    <option value="{$nk.PK_iMaCB}" {($thongtin)?($thongtin[0]['FK_iMaCB_Ky']==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group an {if empty($loai) || $loai!='giaymoi'}hide{/if}">
                                    <label for="" class="col-sm-4 control-label">Giời mời</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="giomoi" tabindex="9" value="{($thongtin)?($thongtin[0]['sGioMoi']):''}" class="form-control req" id="" placeholder="VD 08:30">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu *</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="4" class="form-control" value="{($thongtin)?$thongtin[0]['sKyHieu']:''}" id="" readonly="" name="sokyhieu">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{($thongtin)?$thongtin[0]['sChucVu']:''}" readonly="" id="" name="chucvu">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group an {if empty($loai) || $loai!='giaymoi'}hide{/if}">
                                    <label for="" class="col-sm-4 control-label">Địa điểm</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="diadiem" tabindex="10" value="{($thongtin)?$thongtin[0]['sDiaDiemMoi']:''}" class="form-control req" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="5" class="js-typeahead a form-control" value="{($thongtin)?$thongtin[0]['sTenLV']:''}" name="linhvuc" type="search" autocomplete="off">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="" class="col-sm-3">Tệp tin</label>

                                    <div class="col-sm-9">
                                        <input type="file" name="files[]" required class="form-control" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Trả lời cho vb đến số</label>
                                    <div class="col-sm-8">
                                        <input type="text" required class="form-control" tabindex="7" value="{($thongtin)?$thongtin[0]['iSoDen']:''}" name="sotraloi" placeholder="Nếu có nhiều số thì. VD: 14659,14995">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</button> <label for="">Người nhập văn bản này là: {if !empty($thongtin)}{$mangcb[$thongtin[0]['FK_iMaCB']]}{else}{$vanban['sHoTen']}{/if}</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
    $(document).on('change','select[name=noinhan]',function(){
        var ten = $('.noinhan option:selected').map(function() {
            return this.text;
        }).get().join(',');
        $('input[name=tennoinhan]').val(ten);
    });
    $(document).on('change','.nguoinhan',function(){
            var nguoinhan1  = $('select[name=nguoinhan1]').val();
            var nguoinhan2 = $('select[name=nguoinhan2]').val();
            if(nguoinhan1.length>0 || nguoinhan2.length>0)
            {
                $('button[name=luudulieu]').attr('type','submit');
            }
            else{
                $('button[name=luudulieu]').attr('type','button');
            }
        });
</script>

