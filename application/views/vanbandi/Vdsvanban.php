<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3" style="width:20%">
                    <h3 class="font">
                        Danh sách văn bản đi <b> ({$count})</b>
						<!--{if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs" onclick="myFunction()">Gửi nhiều email cùng lúc</button>{/if}-->
                    </h3>
                </div>
                <div class="col-sm-9" style="width:80%">
					<div class="col-md-5" style="margin-top:20px">
						<form action="" method="get">    
							<div class="form-group">
									<input type="text" name="tonghop" class="form-control" id="tonghop" value="{($tonghop)?$tonghop:''}" placeholder="Tìm tổng hợp theo số đi, trích yếu, lĩnh vực">
                               
                            </div>		
						</form>
					</div>
					<!--<div class="col-md-2" style="margin-top:20px">
						<form action="" method="get">
							<div class="form-group">
								<input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="" placeholder="Tìm theo số đi">
                            </div>
						</form>
					</div>
					<div class="col-md-3" style="margin-top:20px">
						<form action="" method="get">
							<div class="form-group">
								<input type="text" name="trichyeu" value="{($trichyeu)?$trichyeu:''}" class="form-control" id="" placeholder="Tìm theo trích yếu">
                            </div>
						</form>
					</div>-->
					<div class="col-md-3"  style="margin-top:10px">
                        <form action="" method="get">
                            <div class="form-group">
                                <div class="col-md-9">
                                    <select name="loaivanban" id="" class="form-control select2" style="width:100%" onchange="this.form.submit()">
										<option value="">Tất cả</option>
										<option value="1"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="1")?'selected':'':''}>Công văn</a></option>
                                        <option value="2"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="2")?'selected':'':''}>Quyết định</option>
                                        <option value="3"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="3")?'selected':'':''}>Công hàm</option>
										<option value="5"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="5")?'selected':'':''}>Thông báo</option>
										<option value="6"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="6")?'selected':'':''}>Báo cáo</option>
										<option value="9"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="9")?'selected':'':''}>Tờ trình</option>
										<option value="10"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="10")?'selected':'':''}>Giấy mời</option>
										<option value="12"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="12")?'selected':'':''}>Công thư</option>
										<option value="41"{!empty($loaivanban)&&($loaivanban)?($loaivanban=="12")?'selected':'':''}>Kế hoạch</option>
                                    </select>
                                </div>
                            </div>
                        </form>
						
                    </div>
					
                    <div class="col-md-3 abc" >
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a  class="font" name="anhien" href="javascript:void(0);"><i class="fa fa-search"></i> Nâng cao</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal hide anhien" method="get" >
				<div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn loại văn bản --</option>
                                            {if !empty($dsloaivanban)}
                                                {foreach $dsloaivanban as $l}
                                                <option value="{$l.PK_iMaLVB}" {($loaivanban)?($l.PK_iMaLVB==$loaivanban)?'selected':'':''}>{$l.sTenLVB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số đi</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi dự thảo</label>

                                    <div class="col-sm-8">
                                        <select name="noiduthao" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn nơi dự thảo --</option>
                                            {if !empty($dsduthao)}
                                                {foreach $dsduthao as $d}
                                                    <option value="{$d.PK_iMaPB}" {($noiduthao)?($noiduthao==$d.PK_iMaPB)?'selected':'':''}>{$d.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" style="display:flex">
                                    <label for="" class="col-sm-4 control-label">Ngày văn bản từ</label>

                                    <div class="col-sm-4">
                                        <input type="text" name="tungay" value="{($tungay)?$tungay:''}" class="form-control datepic datemask" id="">
                                    </div> <b>&#8594;</b>
									<div class="col-sm-4">
                                        <input type="text" name="denngay" value="{($denngay)?$denngay:''}" class="form-control datepic datemask" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class=" col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Trích yếu</label>
                                        <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký</label>

                                    <div class="col-sm-8">
                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn người ký --</option>
                                            {if !empty($nguoiky)}
                                                {foreach $nguoiky as $nk}
                                                    <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
							
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="linhvuc" value="{($linhvuc)?$linhvuc:''}" class="form-control" id="">
                                    </div>
                                </div>
                                
                            </div>
							
                            <div class="col-md-6 hide">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người nhập</label>

                                    <div class="col-sm-8">
                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn người nhập --</option>
                                            {if !empty($dsnguoinhap)}
                                                {foreach $dsnguoinhap as $nn}
                                                    <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align:center; margin-top: 10px">
                                <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
				</div>
                </form>
				<div class="col-sm-6 deletepadding">
                    {$phantrang} 
					
                </div>
				<div class="col-sm-6 pull-right" style>
					<a href="dsvanban" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;" >ALL</button></a>
					<a href="dsvanban?domat=2&tungay={$tungay}&denngay={$denngay}&tonghop={$tonghop}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px" >VB MẬT</button></a>
					<a href="dsvanban?loaivanban=1&tungay={$tungay}&denngay={$denngay}&tonghop={$tonghop}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px" >Công văn</button></a>
					<a href="dsvanban?loaivanban=9&tungay={$tungay}&denngay={$denngay}&tonghop={$tonghop}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px" >Tờ trình</button></a>
					<a href="dsvanban?loaivanban=2&tungay={$tungay}&denngay={$denngay}&tonghop={$tonghop}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px" >Quyết định</button></a>
					<a href="dsvanban?loaivanban=5&tungay={$tungay}&denngay={$denngay}&tonghop={$tonghop}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px" >Thông báo</button></a>
					{$today = date('Y-m-d', time())}
					<a href="dsvanban?tungay={$today}&dengay={$today}" target=""><button class="btn btn-primary btn-xs pull-right" style="margin-bottom:10px;margin-right:10px" >TODAY</button></a>					
					
                </div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                    {if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs pull-right hide" type="submit" name="guimail" value="guimail">Gửi Mail lên Thành Phố</button>{/if}
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="width:50px" class="text-center">STT</th>
                                    <th width="8%" class="text-center" >Ngày tháng</th>
                                    <th width="8%">Số đi</th>
                                    <th width="">Trích yếu</th>
                                    <th width="23%">Nơi nhận</th>
                                    <th width="8%" class="text-center">Tệp tin</th>
                                    {if $vanban['iQuyenHan_DHNB']==9 || $vanban['PK_iMaCB'] == 705 || $vanban['iQuyenHan_DHNB']==3 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731}
                                    <th width="8%">Tác vụ</th>
                                    <th class="hide">Email</th>
                                    {/if}
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($vanbandi)} 
								{$i=1}
								{$i = $i + $page}
								{$dsdonvi = layemaildonvi()}
                                {foreach $vanbandi as $di}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
										 <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                        <td class="text-center" ><b style="font-size:20px;color: red;">{$di.iSoVBDi}</b> <br>{$di.sTenLVB}</td>
                                       
                                       
                                        {$qt = layKQChuyenDi($di.PK_iMaVBDi)}
										{if $qt[0].iTrangThai == 3}<td style="background: url(/vpdtsongoaivu/files/images/approved.png) bottom right no-repeat;vertical-align: middle;background-size: 15%;">{else} <td>{/if}
										
										<p><a href="{$url}thongtinvanban?id={$di.PK_iMaVBDi}" class="tin" style="color: black !important;"><b>{$di.sMoTa}</b></a></p>
										{if $di.sTenLVB == "Giấy mời"}
											<p>- Thời gian: <b style="color: blue">{$di.sGioMoi}</b> ngày <b style="color: blue">{$di.sNgayMoi}</b><br>
											- Địa điểm: <b style="color: blue">{$di.sDiaDiemMoi} </b>
											</p>
										{else}
											{$mangsoden = explode(',' , $di.iSoDen)}
											
											- Trả lời VB đến số: {foreach $mangsoden as $sd} 
												{$sd = trim($sd)}
												{if is_numeric($sd) && $sd > 0}
													<a href="dsvanbanden?soden={$sd}" target="_blank"><b style="color:red; font-size: 14px">{$sd}</b></a>, 
												{else}
													{$sd}, 
												{/if}
											{/foreach}<br>
										{/if}
										- Ký hiệu: {$di.iSoVBDi}/{$di.sKyHieu} {if $di.sTenLV != ''}&nbsp;&nbsp;- Lĩnh vực: {$di.sTenLV} {/if}<br>
										
										{if $di.FK_iMaDM > 1} {$domat = layDoMat($di.FK_iMaDM)}  - Độ mật: <b style="color:red; font-size:16px">{$domat[0].sTenDoMat}</b><br>{/if}
													
									
										{if $di.FK_iMaDK > 1} {$dokhan = layDoKhan($di.FK_iMaDK)}  - Độ khẩn:<b style="color:red; font-size:16px">{$dokhan[0].sTenDoKhan}</b><br>{/if}
										
											{$cbtao = layTTCB($di.FK_iMaCB_Nhap)}
											
											<p>- Người tạo: {$cbtao[0].sHoTen}</p>
											
										- Người ký: <b>{$di.sHoTen}</b><br>
										</td>
										
										{if $di.iVBDang == 1}
										<td style="background: url(/vpdtsongoaivu/files/images/codang.png) bottom right no-repeat;vertical-align: middle;background-size: 25%;">{else} <td width="23%">{/if}
                                        
										<div style="max-height:150px;  overflow:auto">
										{$dsemail = explode( ',', $di.sNoiNhan)}
										{foreach $dsemail as $email}
											{$email = trim($email)}
											{$k = 0}
												{foreach $dsdonvi as $donvi}
													{if $donvi.sEmail == $email}
														{$k = 1}
														{$donvi.sTenDV};<br>{break}
													{/if}
												{/foreach}
												{if $k == 0}{$email};<br>{/if}
										{/foreach}
										<br>
                                            {if !empty($di.FK_iMaDV_Ngoai)}
												{layDV_Ngoai($di.FK_iMaDV_Ngoai)}
                                            {/if}
										</div>
										</td>
                                        <td class="text-center">

											<p><a class="tin1" href="{$url}teptindi?id={$di.PK_iMaVBDi}"><b>Tài liệu</b> <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p> 
											{if $di.iGuiMail==1}
												{$dsfile = layDSFileDi($di.PK_iMaVBDi)}													
												{foreach $dsfile as $fidi}
													{if (substr($fidi.sDuongDan, 28, 6) == '_vbdi_' && layDuoiFile($fidi.sDuongDan)=='pdf' )}
														<p><a target="_blank" href="{$fidi.sDuongDan}" target="_blank" class="tin1">
															{substr($fidi.sDuongDan, 28)}
														</a></p>
													{/if}
												{/foreach}
											{/if}
											<!--<p>{if $di.iFile==1} <a target="_blank" href="{$di.sDuongDan}" target="_blank" class="tin1">
											{if layDuoiFile($di.sDuongDan)=='pdf'}
												{substr($di.sDuongDan, 28)}
											{else}
											Xem dự thảo{/if}</a>{/if}
											</p>-->
											 <p>
												<!-- ricknguyen321 -->
												{if (substr($di.sDuongDan, 28, 6) == '_vbdi_' && layDuoiFile($di.sDuongDan)=='pdf') && $di.iGuiMail == 1}{/if} 
												{if $di.iGuiMail == 1}
													<span class="label label-success" style="padding:7px;">Đã gửi</span>
												{else}
													<span class="label label-warning" style="padding:7px;">Chưa gửi</span>
												{/if}
											 </p>
											 
											 {if $vanban['iQuyenHan_DHNB']==9 || $vanban['iQuyenHan_DHNB']==3 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731 || $vanban['PK_iMaCB'] == 705}
												{if $di.iGuiMail != 1}
													<button class="btn btn-primary btn-xs" onclick="myFunction({$di.PK_iMaVBDi}, {$di.FK_iMaLVB}, {$di.iSoVBDi})" id="">Gửi email</button>
													
												{/if}
											{/if}
										
										
										</td>
                                        {if $vanban['iQuyenHan_DHNB']==9 || $vanban['PK_iMaCB'] == 705 || $vanban['iQuyenHan_DHNB']==3 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731}
                                        <td class="text-center">
											{if date('Y') == $year} 
												<p><a href="{$url}vanban?id={$di.PK_iMaVBDi}&loai=di" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a></p>
												
												{if $vanban['PK_iMaCB'] == 705 || $vanban['iQuyenHan_DHNB']==3}
													<p><button type="submit" name="xoa" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" value="{$di.PK_iMaVBDi}" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button></p>
												{/if}
													
												{if ($vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731) && $di.iGuiMail == 0}
													<p><button type="submit" name="xoa" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" value="{$di.PK_iMaVBDi}" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button></p>
												{/if}
											{/if}											
                                        </td>
                                        <td class="text-center hide"><input type="checkbox" name="mavanban[{$di.PK_iMaVBDi}]"></td>
                                        {/if}
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
function myFunction($ma, $loai, $sodi) {	
	window.open("{$url}upload_files_vbdi?ma=" + $ma + "&loai=" + $loai + "&isodi=" + $sodi,"phathanh","menubar=1,location=no,resizable=0,width=550,height=320, top=180, left=400");
}
</script>