<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                        Danh sách văn bản đi của phòng <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-md-6 col-sm-offser-3 abc">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label"><a class="font" name="anhien" ><i class="fa fa-search"></i> Tìm kiếm</a></label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
<!--                <label for=""><a style="font-size:20px;" name="anhien" href="javascript:void(0);"><i class="fa fa-search"></i> Tìm kiếm</a></label>-->
                <form class="form-horizontal hide" method="get" enctype="multipart/form-data" autocomplete="off">
                    <div class="row"><br>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn loại văn bản --</option>
                                            {if !empty($dsloaivanban)}
                                                {foreach $dsloaivanban as $l}
                                                <option value="{$l.PK_iMaLVB}" {($loaivanban)?($l.PK_iMaLVB==$loaivanban)?'selected':'':''}>{$l.sTenLVB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi dự thảo</label>

                                    <div class="col-sm-8">
                                        <select name="noiduthao" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn nơi dự thảo --</option>
                                            {if !empty($dsduthao)}
                                                {foreach $dsduthao as $d}
                                                    <option value="{$d.PK_iMaPB}" {($noiduthao)?($noiduthao==$d.PK_iMaPB)?'selected':'':''}>{$d.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                               
								
								<div class="form-group">
                                    <label for="" class="col-sm-4">Ngày đi từ:</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="tu" value="{!empty($tu)&&($tu)?$tu:''}" class="form-control datepic datemask" id="">
                                    </div>

                                    <label for="" class="col-sm-2">đến:</label>
                                    <div class="col-sm-3">
                                         <input type="text" name="den" value="{!empty($den)&&($den)?$den:''}" class="form-control datepic datemask" id="">
                                    </div>
                                </div>
												
												
                            </div>
                            <div class="col-md-12">
                                <div class=" col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Trích yếu</label>
                                        <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký</label>

                                    <div class="col-sm-8">
                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn người ký --</option>
                                            {if !empty($nguoiky)}
                                                {foreach $nguoiky as $nk}
                                                    <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6 hide">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người nhập</label>

                                    <div class="col-sm-8">
                                        <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn người nhập --</option>
                                            {if !empty($dsnguoinhap)}
                                                {foreach $dsnguoinhap as $nn}
                                                    <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
				<div class="pull-right">{$phantrang}</div>
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
								    <th style="width:50px" class="text-center">STT</th>
                                    <!--<th width="10%">Loại văn bản</th>-->
                                    <th width="6%">Số đi</th>
                                    <!--<th width="10%">Số ký hiệu</th>-->
                                    <th width="10%" class="text-center" >Ngày tháng</th>
                                    <th >Trích yếu</th>
                                    <th width="25%">Nơi nhận</th>
                                    <th class="text-center" width="8%">VB đến số</th>
                                    <th width="10%" class="text-center">Tệp tin</th>
                            </tr>
                        </thead>
                        <tbody>
                        {if !empty($vanbandi)} {$i=1}
                            {foreach $vanbandi as $di}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <!--<td class="text-center">{$di.sTenLVB}</td>-->
                                    <td class="text-center"><b style="color: red; font-size: 16px">{$di.iSoVBDi}</b></td>
                                    <!--<td class="text-center">{$di.sKyHieu}</td>-->
                                    <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                    {$qt = layKQChuyenDi($di.PK_iMaVBDi)}
										{if $qt[0].iTrangThai == 3}<td style="background: url(/vpdtsongoaivu/files/images/approved.png) bottom right no-repeat;vertical-align: middle;background-size: 20%;">{else} <td>{/if}
										<p><a href="{$url}thongtinvanban?id={$di.PK_iMaVBDi}" class="tin" style="color: black !important;"><b>{$di.sMoTa}</b></a></p>
										- Người tạo: {$cbtao = layTTCB($di.FK_iMaCB_Nhap)} {$cbtao[0].sHoTen} <br>
										<p>- Ngày tạo: {date_select($di.sNgayNhap)}</p>
										- Người ký: <b>{$di.sHoTen}</b>
									</td>
                                    <td >
										<textarea name="" id="" class="form-control" rows="5" readonly="readonly" style="background: none; border: none;">{$di.sNoiNhan}
										</textarea>
									</td>
                                    <td class="text-center" ><a href="dsvanbanden?soden={str_replace(",",", ",$di.iSoDen)}"><b style="color:red; font-size: 16px">{str_replace(",",", ",$di.iSoDen)}</a></b></td>
                                    <td class="text-center">

										<p><a class="tin1" href="{$url}teptindi?id={$di.PK_iMaVBDi}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p> 
										<p>{if $di.iFile==1} <a target="_blank" href="{$di.sDuongDan}" class="tin1">{if layDuoiFile($di.sDuongDan)=='pdf'}Xem file đã gửi{else}Xem dự thảo{/if}</a>{/if}</p>										
										<p><span class="label label-{(layDuoiFile($di.sDuongDan)=='pdf')?'success':'warning'}" style="padding:7px;">{(layDuoiFile($di.sDuongDan)=='pdf')?'Đã gửi':'Chưa gửi'}</span></p>
									
									</td>
                                </tr>
                            {/foreach}
                        {/if}
                        </tbody>
                    </table>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.form-horizontal').toggle();
            $('.form-horizontal').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>