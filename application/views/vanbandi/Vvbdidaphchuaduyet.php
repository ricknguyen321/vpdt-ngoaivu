<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản đi đã phát hành chưa duyệt <b>({$count})</b>
      </h1>
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
               <i>Để đảm bảo đúng quy trình xử lý văn bản trên hệ thống văn phòng điện tử (toàn bộ văn bản phát hành đi đều được duyệt trên hệ thống), kính đề nghị lãnh đạo kiểm tra, rà soát văn bản đã được phát hành, sau đó nhấn nút "Duyệt" để duyệt văn bản.</i>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form class="form-horizontal" method="post" action=""  enctype="multipart/form-data">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:50px" class="text-center">STT</th>
                                   
                                    <th width="">Trích yếu văn bản đi</th>
									
                                    <th width="20%"></th>
									
                                    <!--<th width="6%" class="text-center"></th>-->
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr  id ="{$dl.PK_iMaVBDi}">
                                        <td class="text-center">{$i++}</td>
                                        
                                        <td >
											<p><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin" ><b style="color:black">{$dl.sMoTa} </b></a></p>
											- Ký hiệu: {$dl.sKyHieu}<br>
											- Người tạo: {$dscanbo[$dl.FK_iMaCB_Nhap]}<br>											
											- Người ký: <b>{$cbky = layTTCB($dl.FK_iMaCB_Ky)} {$cbky[0].sHoTen}</b> <br>
											
											<p style="text-align:right"><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p>
											{$dsfile = layDSFile($dl.PK_iMaVBDi)}
											{foreach $dsfile as $f}
												{$cbtao = layTTCB($f.FK_iMaCB)}
												<p><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{$f.sTenFile}</span></a> <i style="color: silver">({$cbtao[0].sHoTen} - {date_time2($f.sThoiGian)})</i></p>
											{/foreach}
										
										</td>
                                        
                                        
                                        <td class="text-center">
										 <input type="text" name="yearup" value="{$year}" class="hidden" >
										{if $trangthai==1 && $cbky[0].sHoTen == $vanban['sHoTen']}                                       
											
											<p>
												<br>
                                                <button name="dongy" id="id_{$dl.PK_iMaVBDi}" value="{$dl.PK_iMaVBDi}" class="btn btn-primary btn-xs" style="width: 50%" >Duyệt</button>&nbsp;&nbsp;&nbsp;                                            
												<!--{if $vanban['PK_iMaCB'] == 693}
                                                <button name="tralai" value="{$dl.PK_iMaVBDi}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>{/if}-->
                                            
                                            </p>
											{/if}
                                        </td>

                                    </tr>

                                {/foreach}

                            {/if}

                            </tbody>

                        </table>

                    </form>

                    <div class="pull-right">{$phantrang}</div>

                </div>

            </div>

        </div>

      <!-- /.box -->



    </section>

    <!-- /.content -->

</div>

<script>

    $(document).ready(function() {

        var url = window.location.href;

        $('td').css('vertical-align', 'middle');

        $('th').css('vertical-align', 'middle');

        // $('td').css('text-align', 'justify');

        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');

        $('.control-label').css('text-align', 'left');

    });

</script>

<script type="text/javascript">
        
        function callback(rv) {
            var obj = JSON.parse(rv);
            if (obj.Status == 0) {
                document.getElementById("file2").value = obj.FileServer;
            } else {
                document.getElementById("file2").value = obj.Message;
            }

            $('#LicenseDetailModal').modal('toggle');
        }


        function exc_sign_sim(url,maVB) { 
            $.post("vanbandichoxuly_ld",
            {
                kydientu:1,
                FileName: url,
                PK_iMaVBDi : maVB
            },
            function (data, status) {
                console.log("Data: " + data + "\nStatus: " + status);
                callback(data);
            });
        }

        /* function exc_sign_sim() {
            var test;
            $.post("PdfSign.ashx",
            {
                FileName: document.getElementById("file1").value,
                LanhDao: document.getElementById("_lanhdao").value,
                Phone: document.getElementById("_phone").value
            },
            function (data, status) {
                console.log("Data: " + data + "\nStatus: " + status);
                callback(data);
            });
        }*/

        function openfile() {
            window.open(document.getElementById("file2").value, 'popUpWindow', 'height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no');
            return false;
        }

        function showSignForm() {
            $("#LicenseDetailModal").modal('show');
        }
        $(document).on('click','#_lanhdaoPheduyet',function(){
            var id = $(this).val();
            $('#'+id).addClass('hide');
        });

        function exc_sign_approved(url,ci_session) {
            var prms = {};

            prms["FileUploadHandler"] = "https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/lanhdaoky";
            prms["SessionId"] = "ci_sessions="+ci_session+";kiemtra=ok";            
            prms["FileName"] = url; 

            var json_prms = JSON.stringify(prms);
            console.log(json_prms);
            vgca_sign_approved(json_prms, SignFileCallBack1);

        }

         function SignFileCallBack1(rv) {
            var received_msg = JSON.parse(rv);
            console.log(received_msg);
            if (received_msg.Status == 0) {
                console.log(received_msg);
                document.getElementById("_signature").value = received_msg.FileName + ":" + received_msg.FileServer + ":" + received_msg.DocumentNumber + ":" + received_msg.DocumentDate;
                document.getElementById("file1").value = received_msg.FileServer;
                document.getElementById("file2").value = received_msg.FileServer;
            } else {
                document.getElementById("_signature").value = received_msg.Message;
            }
        }

    </script>