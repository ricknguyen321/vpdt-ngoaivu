<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-6">
                    <h3 class="font">
                       Lịch họp Sở Ngoại vụ
                    </h3>
                </div>
                
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
<!--               
                <!--end quyền bằng 2 là văn thư form-->
               
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="get">
						
						<iframe src="lichcongtac" style="width: 100%; height:3600px;" frameborder="0"></iframe>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $(".formSentMsg").delay(5200).fadeOut(300);
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.form-horizontal').toggle();
            $('.form-horizontal').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });

    
    $(document).on('click','#_vanthuphathanh',function(){
            var id = $(this).val();
            $('#'+id).addClass('hide');
        });

    function exc_sign_issued(ci_session,file,socv,ngayphathanh) {
        var prms = {};

        prms["FileUploadHandler"] = "https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/vanthuphathanh";
        prms["SessionId"] = "ci_sessions="+ci_session+";kiemtra=ok";
        prms["FileName"] = file;
        prms["DocNumber"] = socv;
        prms["IssuedDate"] = ngayphathanh;

        var json_prms = JSON.stringify(prms);
        vgca_sign_issued(json_prms, SignFileCallBack1);

    }
    function SignFileCallBack1(rv) {
        var received_msg = JSON.parse(rv);
        console.log(received_msg);
        if (received_msg.Status == 0) {
            console.log(received_msg);
            document.getElementById("_signature").value = received_msg.FileName + ":" + received_msg.FileServer + ":" + received_msg.DocumentNumber + ":" + received_msg.DocumentDate;
            document.getElementById("file1").value = received_msg.FileServer;
            document.getElementById("file2").value = received_msg.FileServer;
        } else {
            document.getElementById("_signature").value = received_msg.Message;
        }
    }
       
</script>