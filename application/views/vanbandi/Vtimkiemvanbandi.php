<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 style="font-size: 18px">
        <a href="javascript:void(0);" name="anhien">Tìm kiếm văn bản đi</a>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="get" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                                <div class="col-sm-8">
                                    <select name="loaivanban" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn loại văn bản --</option>
                                        {if !empty($dsloaivanban)}
                                            {foreach $dsloaivanban as $l}
                                            <option value="{$l.PK_iMaLVB}" {($loaivanban)?($l.PK_iMaLVB==$loaivanban)?'selected':'':''}>{$l.sTenLVB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                <div class="col-sm-8">
                                    <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Nơi dự thảo</label>

                                <div class="col-sm-8">
                                    <select name="noiduthao" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn nơi dự thảo --</option>
                                        {if !empty($dsduthao)}
                                            {foreach $dsduthao as $d}
                                                <option value="{$d.PK_iMaPB}" {($noiduthao)?($noiduthao==$d.PK_iMaPB)?'selected':'':''}>{$d.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Ngày văn bản</label>

                                <div class="col-sm-8">
                                    <input type="text" name="ngaythang" value="{($ngaythang)?$ngaythang:''}" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class=" col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Trích yếu</label>
                                    <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người ký</label>

                                <div class="col-sm-8">
                                    <select name="nguoiky" id="" class="form-control select2">
                                        <option value="">-- Chọn người ký --</option>
                                        {if !empty($nguoiky)}
                                            {foreach $nguoiky as $nk}
                                                <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6 hide">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người nhập</label>

                                <div class="col-sm-8">
                                    <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn người nhập --</option>
                                        {if !empty($dsnguoinhap)}
                                            {foreach $dsnguoinhap as $nn}
                                                <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%">Loại văn bản</th>
                                    <th width="6%">Số đi</th>
                                    <th width="14%">Số ký hiệu</th>
                                    <th width="10%" class="text-center" >Ngày tháng</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="18%">Nơi nhận</th>
                                    <th width="6%" class="text-center">Tệp tin</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)} {$i=1}
                                {foreach $dsvanban as $di}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$di.sTenLVB}</td>
                                        <td class="text-center">{$di.iSoVBDi}</td>
                                        <td class="text-center">{$di.sKyHieu}</td>
                                        <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                        <td ><a href="{$url}thongtinvanban?id={$di.PK_iMaVBDi}" class="tin">{$di.sMoTa}</a><br><i>(Người ký: {$di.sHoTen})</i></td>
                                        <td>{$di.sNoiNhan}</td>
                                        <td class="text-center"><a class="tin" data-toggle="tooltip" data-placement="top" title="Xem tệp tin" href="{$url}teptin?id={$di.PK_iMaVBDi}"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">{$phantrang}</div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.form-horizontal').toggle();
        });
        $('.control-label').css('text-align', 'left');
        $('td').css('vertical-align', 'middle');
    });
</script>