<script src="{$url}assets/js/vanbandi/vanbanscan.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Văn bản đi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tệp tin văn bản</label>

                                <div class="col-sm-9">
                                    <input type="file" name="files[]" multiple class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label" style="color:red;">Số văn bản *</label>

                                <div class="col-sm-9">
                                    <input type="text" style="color:red;" name="sodi" readonly="" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Loại văn bản *</label>

                                <div class="col-sm-9">
                                    <select name="loaivanban" id="" class="form-control select2 laykyhieu">
                                        <option value="">-- Chọn loại văn bản --</option>
                                        {if !empty($dsloaivanban)}
                                            {foreach $dsloaivanban as $vb}
                                                <option value="{$vb.PK_iMaLVB}">{$vb.sTenLVB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tên tệp tin</label>
                                <div class="col-sm-9">
                                    <input type="text" name="tenteptin" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Ngày tháng *</label>
                                <div class="col-sm-9">
                                    <input type="text" name="ngaythang" value="{date('d/m/Y')}" class="form-control datepic datemask" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nơi dự thảo *</label>

                                <div class="col-sm-9">
                                    <select name="duthao" requred id="" class="form-control select2 laykyhieu">
                                        {if !empty($dsduthao)}
                                            {foreach $dsduthao as $dt}
                                                <option value="{$dt.PK_iMaPB}">{$dt.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Trích yếu *</label>
                                    <textarea name="trichyeu" id="" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số trang</label>

                                <div class="col-sm-8">
                                    <input type="text" name="sotrang" class="form-control" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                <div class="col-sm-8">
                                    <input type="text" name="kyhieu" readonly="" class="form-control" id="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người ký *</label>

                                <div class="col-sm-8">
                                    <select name="nguoiky" id="" required class="form-control select2">
                                        <option value="">-- Chọn người ký --</option>
                                        {if !empty($nguoiky)}
                                            {foreach $nguoiky as $nk}
                                                <option value="{$nk.PK_iMaCB}">{$nk.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Độ mật</label>

                                <div class="col-sm-8">
                                    <select name="domat" id="" class="form-control select2">
                                        {if !empty($dsdomat)}
                                            {foreach $dsdomat as $dm}
                                                <option value="{$dm.PK_iMaDM}">{$dm.sTenDoMat}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Lĩnh vực</label>

                                <div class="col-sm-8">
                                    <select name="linhvuc" id="" class="form-control select2">
                                        <option value="">-- Chọn lĩnh vực --</option>
                                        {if !empty($dslinhvuc)}
                                            {foreach $dslinhvuc as $lv}
                                                <option value="{$lv.sTenLV}">{$lv.sTenLV}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" name="chucvu" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Độ khẩn</label>

                                <div class="col-sm-8">
                                    <select name="dokhan" id="" class="form-control select2">
                                        {if !empty($dsdomat)}
                                            {foreach $dsdokhan as $dk}
                                                <option value="{$dk.PK_iMaDK}">{$dk.sTenDoKhan}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Nơi nhận</label>
                                    <textarea name="noinhan" id="" class="form-control" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Chú thích</label>
                                    <textarea name="ghichu" id="" class="form-control" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 hide" style="padding-left:30px;">
                            <div class="col-md-12" style="padding-left:30px;">
                                <div class="form-group">
                                    <input type="checkbox"> CHUYỂN EMAIL LÊN UBND THÀNH PHỐ
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-3">Gửi tới</label>

                                    <div class="col-sm-9">
                                        <input type="checkbox"> Cổng thông tin TP Hà Nội (conggiaotiepdientu@hanoi.gov.vn) <br>
                                        <input type="checkbox"> Văn phòng UBND thành phố (vanthu@hanoi.gov.vn)
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <input type="checkbox">THTK <br>
                                        <input type="checkbox"> UBND thành phố Hà Nội (vanthu1@hanoi.gov.vn)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Trả lời cho văn bản đến số</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"  name="soden">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit" name="luudulieu" value="luudulieu">Thêm mới</button> <button class="btn btn-default">Quay lại >></button> <label for="">Người nhập văn bản này là: {$vanban['sHoTen']}</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>