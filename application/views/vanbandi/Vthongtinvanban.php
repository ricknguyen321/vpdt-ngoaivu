<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
	{if $thongtin[0]['sTenLVB'] == "Giấy mời"}
		<h3 class="" style="text-align:center">
			GIẤY MỜI
		</h3>
	{else}
		{if $thongtin[0]['iVBDang'] == 1}
			<h1 class="font" style="text-align:center">
				<img src="files/images/codang.png" style="height:50px">
			</h1>
		{else}
			<h1 class="font">
				Thông tin văn bản đi
			</h1>
		{/if}
	{/if}
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
					<form class="form-horizontal" method="post" action="">
					
					<a class="pull-right" data-toggle="collapse" href="#collapseExample{$thongtin[0]['PK_iMaVBDi']}" aria-expanded="false" aria-controls="collapseExample{$thongtin[0]['PK_iMaVBDi']}" style="margin-right:40px"><b style="color:black"> <button class="btn btn-primary btn-xs" style="">Thêm vào hồ sơ</button></b></a>
                  <div class="col-sm-8 collapse pull-right" id="collapseExample{$thongtin[0]['PK_iMaVBDi']}" style="margin-bottom: 20px; ">
                     <div class="col-sm-10">												
                        <select name="manghs{$thongtin[0]['PK_iMaVBDi']}[]" multiple="" id="" data-placeholder="" class="form-control select2" style="width:100%">
                        {if !empty($dshoso)}
                        {foreach $dshoso as $hs}
                        <option value="{$hs.PK_iMaHS}" {if in_array($hs.PK_iMaHS, $dshosovb)}selected{/if}>{$hs.sTenHS}</option>
                        {/foreach}
                        {/if}
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <button type="submit" name="luuhoso" value="{$thongtin[0]['PK_iMaVBDi']}" class="btn btn-primary btn-sm">Lưu</button>
                     </div>
                  </div>
				  
				  
					{$approved = 0}
					{foreach $dschuyen as $ch}
						{if $ch.iTrangThai==3}  
							{$approved = 1} 
							<div class="col-md-12" style="background: url(/vpdtsongoaivu/files/images/approved.png) no-repeat; background-position-x: right; background-size: 10%;">
						{/if}
                    {/foreach}
                        {if $approved == 0}  <div class="col-md-12"> {/if}
                            <div class="col-md-6">
                               
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Số văn bản:</label>
                                    <div class="col-md-8">
									{if $thongtin[0]['iSoVBDi'] == '0' || $thongtin[0]['iSoVBDi'] == '00'} 
											Chưa cấp số 
											{else}
										<b style="color: red;font-size: 20px">{$thongtin[0]['iSoVBDi']}</b>     {/if}                                   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Loại văn bản: </label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['sTenLVB']}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Số ký hiệu: </label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['iSoVBDi']}/{$thongtin[0]['sKyHieu']}
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Ngày ký:</label>
                                    <div class="col-md-8">
                                        {date_select($thongtin[0]['sNgayVBDi'])}
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="">
                                    <label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;" >Trích yếu</label>
                                    <div class="col-md-8"  style="padding-top:7px">
										<b>{$thongtin[0]['sMoTa']}</b>
									</div>
                                </div>
                            </div>
							
							
							{if $thongtin[0]['sTenLVB'] == "Giấy mời"}
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Giờ mời:</label>
                                    <div class="col-md-8">
                                        <b style="color:blue; font-size: 16px">{$thongtin[0]['sGioMoi']}</b>
                                    </div>
                                </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Ngày mời:</label>
                                    <div class="col-md-8">
                                        <b style="color:blue; font-size: 16px">{date_select($thongtin[0]['sNgayMoi'])}</b>
                                    </div>
                                </div>
							</div>
							<div class="col-md-12">
                                <div class="form-group" style="">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;" style="width: 16.7%;">Địa điểm</label>
                                    <div class="col-md-8">
										<b><i>{$thongtin[0]['sDiaDiemMoi']}</i></b>
									</div>
                                </div>
                            </div>
							{/if}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Người ký:</label>
                                    <div class="col-md-8">
                                        <b style="color:blue">{$thongtin[0]['sHoTen']}</b>
                                    </div>
                                </div>
								
                            </div>
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Lĩnh vực/Hashtag:</label>
                                    <div class="col-md-8">
                                        {$thongtin[0]['sTenLV']}
                                    </div>
                                </div>
							</div>
							
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Độ mật:</label>
                                    <div class="col-md-8">
                                        {$domat = layDoMat($thongtin[0]['FK_iMaDM'])}											
										{if $thongtin[0]['FK_iMaDM'] > 1} <b style="color:red; font-size:16px">{$domat[0].sTenDoMat}</b>{else}{$domat[0].sTenDoMat}{/if}
										</p>
                                    </div>
                                </div>
								
                            </div>
							<div class="col-md-6">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Độ khẩn:</label>
                                    <div class="col-md-8">
                                        {$dokhan = layDoKhan($thongtin[0]['FK_iMaDK'])} 
										{if $thongtin[0]['FK_iMaDK'] > 1} <b style="color:red; font-size:16px"> {$dokhan[0].sTenDoKhan} </b> {else}{$dokhan[0].sTenDoKhan}{/if}
                                    </div>
                                </div>
							</div>
							
							<div class="col-md-12">
								{if !empty($thongtin[0]['sNoiNhan'])}
										{$dsdonvi = layemaildonvi()}
										<div class="form-group">
											<label for="" class="col-sm-2 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Nơi nhận:</label>
											 <div class="col-md-8" style="padding-top:7px">
													{$dsemail = explode( ',', $thongtin[0]['sNoiNhan'])}
													{foreach $dsemail as $email}
														{$email = trim($email)}
														{$k = 0}
															{foreach $dsdonvi as $donvi}
																{if $donvi.sEmail == $email}
																	{$k = 1}
																	{$donvi.sTenDV}; &nbsp;&nbsp; {break}
																{/if}
															{/foreach}
															{if $k == 0}{$email}; &nbsp;&nbsp; {/if}
													{/foreach}
													<br>
														{if !empty($dl.FK_iMaDV_Ngoai)}
															{layDV_Ngoai($dl.FK_iMaDV_Ngoai)}
														{/if}
											</div>
										</div>
								  {/if}
								
                                <div class="form-group" style="padding-left:15px;">
									<p style="font-size: 16px; margin-top:20px"><a class="tin1" href="{$url}teptindi?id={$thongtin[0]['PK_iMaVBDi']}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 16px;"></i></a></p>
									
									{if !empty($dsfile)}
                                    <table class="table table-bordered table-striped" style="width:73%">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th  width="50px">STT</th>
                                                <th  width="">Tên file / ghi chú</th>
                                                <th  width="15%">Thời gian</th>
                                                <th  width="20%">Người gửi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
										{$i=1}
                                            {foreach $dsfile as $f}
												{if $f.sTenFile != '.'}
                                                <tr>
                                                    <td class="text-center">{$i++}</td>
                                                    <td ><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></td>
                                                    <td class="text-center">{date_time2($f.sThoiGian)}</td>
                                                    <td class="text-center">{if !empty($f.FK_iMaCB)}{$dscanbo[$f.FK_iMaCB]}{/if}</td>
                                                </tr>
												{/if}
                                            {/foreach}
                                        
                                        </tbody>
                                    </table>
                                    {else}<br>----{/if}
                                </div>
                            </div>
							
							{if !empty($chidao)}
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue; font-size:15px">Chỉ đạo của lãnh đạo:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped" style="width: 88%; margin-left: 15px;">
                                        <thead>
                                        <tr style="background-color: orange !important">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="width:17%">Lãnh đạo</th>
                                            <th>Nội dung</th>
                                            <th class="text-center" style="width:17%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										
                                        {foreach $chidao as $cd}
										{$ld = layTTCB($cd.FK_iMaCB)}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>                                            
                                            <td class="text-center"><span style="color: red"> {$ld[0].sHoTen} </span></td>
                                            <td><span style="color: red"> {$cd.sNoiDung}</span></td>
                                            <td class="text-center">{date_time2($cd.sThoiGian)}&nbsp; {if $cd.FK_iMaCB == $vanban['PK_iMaCB']}<button type="submit" value="{$cd.id}" name="xoachidao" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							{/if}
							
							{if !empty($dexuat)}
							<div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue; font-size:15px">Đề xuất / ý kiến:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped" style="width: 88%">
                                        <thead>
                                        <tr style="background-color: orange !important">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="width:17%">Cán bộ</th>
                                            <th>Nội dung</th>
                                            <th class="text-center" style="width:17%">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
										
                                        {foreach $dexuat as $dx}
										{$ld = layTTCB($dx.FK_iMaCB)}
                                        <tr class="info">
                                            <td class="text-center">{$i++}</td>                                            
                                            <td class="text-center">{$ld[0].sHoTen}</td>
                                            <td>{$dx.sNoiDung}</td>
                                            <td class="text-center">{date_time2($dx.sThoiGian)} &nbsp; {if $dx.FK_iMaCB == $vanban['PK_iMaCB']}<button type="submit" value="{$dx.id}" name="xoadexuat" onclick="return confirm('Bạn chắc chắn muốn xóa ?')" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="fa fa-trash"></i></button>{/if}</td>
                                        </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							{/if}
								
							
                            {if !empty($thongtin[0]['iSoDen'])}								
                                <div class="col-md-12">
								<div class="form-group" style="padding-left:15px;">
                                <label>Là văn bản đầu ra của văn bản đến số:</label>
                                {if !empty($dssoden)}{$i=1}
                                <table class="table table-bordered table-striped">
								
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th width="50px" >STT</th>
                                            <th width="7%" >Số đến</th>
                                            <th width="10%" >Ký hiệu</th>
                                            <th width="20%" >Nơi gửi đến</th>
                                            <th >Trích yếu</th>
                                            <th width="15%" >Người ký</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                        {foreach $dssoden as $so}
										{$dsfileden = layDuLieu('FK_iMaVBDen',$so.PK_iMaVBDen,'tbl_files_vbden')}
                                            <tr>
                                                <td class="text-center" >{$i++}</td>
                                                <td class="text-center"><a class="tin1" href="{$url}quatrinhxuly/{$so.PK_iMaVBDen}"><b style="color: red;font-size: 16px">{$so.iSoDen}</b></a></td>
												<!--<td ><a class="tin1" href="{$url}thongtindaura?id={$so.PK_iMaVBDen}">{$so.iSoDen}</a></td>-->
                                                <td class="text-center">{$so.sKyHieu}</td>
                                                <td class="text-center">{$so.sTenDV}</td>
                                                <td><p><a class="tin1" href="{$url}quatrinhxuly/{$so.PK_iMaVBDen}">{$so.sMoTa}</p>
												{foreach $dsfileden as $f}
													<p style="text-align:right; margin-top:3px"><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</p>
												{/foreach}
												</a></td>
                                                <td class="text-center"><p>{$so.sTenNguoiKy}</p>{date_select($so.sNgayKy)}</td>
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
									
                                </table>{else}<p>----</p>{/if}
								</div>
                                </div>
                            {/if}
                            <div class="col-md-6 hide" >
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="background: rgb(225, 248, 251); padding: 7px 5px 7px 15px;">Người nhập:</label>
                                    <div class="col-md-8">
                                        {$nguoinhap['sHoTen']}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Quá trình xử lý văn bản chính:</label>
									{if !empty($dschuyen)}
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th  width="50px%">STT</th>
                                                
                                                <th  width="15%">Người gửi</th>
                                                <th  width="">Nội dung</th>
                                                <th  width="15%">Người nhận</th>
												<th  width="12%">Thời gian</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
										{$i=1}
											{$ykien=''}
                                            {foreach $dschuyen as $ch}
                                                <tr>
                                                    <td  class="text-center">{$i++}</td>
                                                    <td class="text-center">
														{if $ch.FK_iMaCB_Gui < 696} 
																<span style="color: red">{$dscanbo[$ch.FK_iMaCB_Gui]} </span>
															{else}
																{$dscanbo[$ch.FK_iMaCB_Gui]}
															{/if}
															
													</td>
                                                    <td > {if $ch.FK_iMaCB_Gui < 696} 
																<span style="color: red"> {if $ch.sYKien != $ykien}{$ch.sYKien} {/if}</span>
															{else}
																{if $ch.sYKien != $ykien}{$ch.sYKien} {/if}
															{/if}
													</td>
													
                                                    <td class="text-center">{$dscanbo[$ch.FK_iMaCB_Nhan]} <!--{if $ch.iTrangThai==3} <span class="label label-success" style="padding:5px">Duyệt</span>{/if}-->
													</td>
													<td class="text-center">{date_time2($ch.sThoiGian)}</td>
                                                </tr>
												{$ykien=$ch.sYKien}
                                            {/foreach}
                                        
                                        </tbody>
                                    </table>
									{else} <p>----</p> {/if}
                                </div>
                            </div>
                            <div class="col-md-12 hide">
                                <div class="form-group" style="padding-left:15px;">
                                    <label for="">Phối hợp xử lý:</label>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="background: #ccc;">
                                                <th  width="50px">STT</th>
                                                <th  width="15%">Thời gian</th>
                                                <th  width="20%">Người gửi</th>
                                                <th  width="20%">Người nhận</th>
                                                <th  width="">Nội dung xử lý</th>
                                                <th  width="10%">Tệp đính kèm</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {if !empty($dschuyenPH)}{$i=1}
                                            {foreach $dschuyenPH as $ph}
                                                <tr>
                                                    <td  class="text-center">{$i++}</td>
                                                    <td class="text-center">{date_time2($ph.sThoiGian)}</td>
                                                    <td class="text-center">{$dscanbo[$ph.FK_iMaCB_Gui]}</td>
                                                    <td class="text-center">{$dscanbo[$ph.FK_iMaCB_Nhan]}</td>
                                                    <td class="">{$ph.noidung_xuly}</td>
                                                    <td class="text-center">{if !empty($ph.sFile)}<a href="{$url}{$ph.sFile}" download>Download</a>{/if}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							 <div class="col-md-12" style = "text-align:center">							
                            {if ($thongtin[0]['iGuiMail'] != 1 || $approved == 0)  && ($vanban['iQuyenHan_DHNB'] < 6 && $vanban['iQuyenHan_DHNB'] != 3)}
								
								<div class="col-sm-12" style="margin-bottom:20px; margin-top: 10px">
										<div class="col-sm-8" style="margin-left: 16%">
											<div class="form-group">
												<label for="" class="col-sm-12">Lãnh đạo có ý kiến chỉ đạo: </label>
												
												<div class="col-sm-12" style="display: inline-flex">
													<textarea name="chidao" id="" class="form-control" rows="3" placeholder="Ý kiến chỉ đạo"></textarea>
													<button class="btn btn-primary" type="submit" name="luulai" value="Chỉ đạo" style="margin-left: 5px">Gửi</button>
												</div>
												
											</div>
										</div>
																															   
								</div>
									
							{/if}
								{if ($thongtin[0]['iGuiMail'] != 1 || $approved == 0)  && ($vanban['iQuyenHan_DHNB'] > 5  || $vanban['iQuyenHan_DHNB'] == 3)}
							
									<div class="col-sm-12" style="margin-bottom:20px; margin-top: 10px">
										<div class="col-sm-8" style="margin-left: 16%">
											<div class="form-group">
												<label for="" class="col-sm-12">Ý kiến / đề xuất </label>
												
												<div class="col-sm-12" style="display: inline-flex">
													<textarea name="dexuat" id="" class="form-control" rows="3" placeholder="Nhập ý kiến / đề xuất"></textarea>
													<button class="btn btn-primary" type="submit" name="luuykien" value="Lưu lại" style="margin-left: 5px">Gửi</button>
												</div>
												
											</div>
										</div>
																															   
									</div>
								{/if}							
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
		$('.form-group').css('margin-bottom', '0px');
    });
</script>