<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới lịch họp
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body" >
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row" style="width:100%; justify-content: center;">
                        <div class="col-md-12" style="margin-top: 10px; margin-left:10%; width:80%">

							
							<div class="col-md-12" style="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label" style="color:blue" style="text-align:center">Nội dung cuộc họp *</label>
                                        <textarea name="mota" id="" required class="form-control" placeholder="Nội dung cuộc họp" rows="4">{($thongtin)?$thongtin[0]['sMoTa']:''}</textarea>
                                    </div>
                                </div>
                            </div>
							
							{if $vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5}
								<div class="col-md-12" style="">
									<div class="col-md-12">
										<div class="form-group">
											<label for="" class="control-label" style="" style="text-align:center">Ý kiến chỉ đạo:</label>
											
											<textarea name="chidao" id="" class="form-control" placeholder="Ý kiến chỉ đạo của lãnh đạo (optional)" rows="3">{($thongtin)?$thongtin[0]['sChiDao']:''}</textarea>
										</div>
									</div>
								</div>
							{/if}                   
							
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Ngày họp *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngaymoi" tabindex="8" value="{($thongtin)?($thongtin[0]['sNgayMoi']!=0000-00-00)?date_select($thongtin[0]['sNgayMoi']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>                                
								
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">LĐ chủ trì *</label>

                                    <div class="col-sm-8">
                                        <select name="nguoiky[]" id="" multiple="" required class="form-control select2">
                                            {if !empty($nguoiky)}
                                                {foreach $nguoiky as $nk}
                                                    <option value="{$nk.PK_iMaCB}" {if !empty($mangld)}{if in_array($nk.PK_iMaCB, $mangld)}selected{/if}{/if}>{$nk.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Phòng chủ trì *</label>

                                    <div class="col-sm-8">
										<select name="phongct" id="" class="form-control select2" style="width:100%">
										<option value="" >-- Chọn phòng chủ trì --</option>
                                            {if !empty($phongban)}
                                                {foreach $phongban as $pb}
                                                    <option value="{$pb.sTenPB}" {($thongtin)?($thongtin[0]['sPhongCT']=={$pb.sTenPB})?'selected':'':($vanban['FK_iMaPhongHD']==$pb.PK_iMaPB)?'selected':''}>{$pb.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
									</div>
								</div>
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Phòng phối hợp</label>
                                    <div class="col-sm-8">
										<select name="phongph[]" multiple="" id="" data-placeholder="" class="form-control select2" style="width:100%">
                                            {if !empty($phongban)}
                                                {foreach $phongban as $pb}
													{if $vanban['FK_iMaPhongHD'] != $pb.PK_iMaPB}
														<option value="{$pb.sTenPB}" {if in_array($pb.sTenPB,$dsph)} selected {/if}>{$pb.sTenPB}</option>
													{/if}
                                                {/foreach}
                                            {/if}
                                        </select>
									</div>
								</div>
                            </div>							
							
							<div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Giờ họp *</label>

                                    <div class="col-sm-8">
										<div style="display: flex">
											<select name="gio" id="" required class="form-control " style="width:25%">
												<option value="00" {($gio == 00 || $gio == 0) ? 'selected': ''}>00</option>
												<option value="01" {($gio == 01 || $gio == 1) ? 'selected': ''}>01</option> 
												<option value="02" {($gio == 02 || $gio == 2) ? 'selected': ''}>02</option>
												<option value="03" {($gio == 03 || $gio == 3) ? 'selected': ''}>03</option>
												<option value="04" {($gio == 04 || $gio == 4) ? 'selected': ''}>04</option>
												<option value="05" {($gio == 05 || $gio == 5) ? 'selected': ''}>05</option>
												<option value="06" {($gio == 06 || $gio == 6) ? 'selected': ''}>06</option>
												<option value="07" {($gio == 07 || $gio == 7) ? 'selected': ''}>07</option>
												<option value="08" {($gio == 08 || $gio == 8) ? 'selected': ''}>08</option>
												<option value="09" {($gio == 09 || $gio == 9) ? 'selected': ''}>09</option>
												<option value="10" {($gio == 10) ? 'selected': ''}>10</option>
												<option value="11" {($gio == 11) ? 'selected': ''}>11</option>
												<option value="12" {($gio == 12) ? 'selected': ''}>12</option>
												<option value="13" {($gio == 13) ? 'selected': ''}>13</option>
												<option value="14" {($gio == 14) ? 'selected': ''}>14</option>
												<option value="15" {($gio == 15) ? 'selected': ''}>15</option>
												<option value="16" {($gio == 16) ? 'selected': ''}>16</option>
												<option value="17" {($gio == 17) ? 'selected': ''}>17</option>
												<option value="18" {($gio == 18) ? 'selected': ''}>18</option>
												<option value="19" {($gio == 19) ? 'selected': ''}>19</option>
												<option value="20" {($gio == 20) ? 'selected': ''}>20</option>
												<option value="21" {($gio == 21) ? 'selected': ''}>21</option>
												<option value="22" {($gio == 22) ? 'selected': ''}>22</option>
												<option value="23" {($gio == 23) ? 'selected': ''}>23</option>
											
											</select>
											
											&nbsp;:&nbsp;
										
											<select name="phut" id="" required class="form-control" style="width:25%">
												
												<option value="00" {($phut == 00 || $phut == 0) ? 'selected': ''}>00</option>
												<option value="05" {($phut == 05 || $phut == 5) ? 'selected': ''}>05</option> 
												<option value="10" {($phut == 10) ? 'selected': ''}>10</option>
												<option value="15" {($phut == 15) ? 'selected': ''}>15</option>
												<option value="20" {($phut == 20) ? 'selected': ''}>20</option>
												<option value="25" {($phut == 25) ? 'selected': ''}>25</option>
												<option value="30" {($phut == 30) ? 'selected': ''}>30</option>
												<option value="35" {($phut == 35) ? 'selected': ''}>35</option>
												<option value="40" {($phut == 40) ? 'selected': ''}>40</option>
												<option value="45" {($phut == 45) ? 'selected': ''}>45</option>
												<option value="50" {($phut == 50) ? 'selected': ''}>50</option>
												<option value="55" {($phut == 55) ? 'selected': ''}>55</option>
												
											</select>
										</div>
                                        <!--<input type="text" name="giomoi" tabindex="9" value="{($thongtin)?($thongtin[0]['sGioMoi']):''}" class="form-control req" id="" placeholder="hh:mm ví dụ: 08:30">-->
                                    </div>										
                                </div>
								
								
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Địa điểm *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="diadiem" tabindex="10" value="{($thongtin)?$thongtin[0]['sDiaDiemMoi']:'Hội trường tầng 1 Sở Ngoại vụ'}" class="form-control req" id="" required>
                                    </div>
									
                                    <!--<div class="col-sm-8">
										<select name="diadiem" required id="" class="form-control select2" style="width:100%">
                                       
                                            <option value="{($thongtin[0]['sDiaDiemMoi'])?$thongtin[0]['sDiaDiemMoi']:'Hội trường tầng 1 Sở Ngoại vụ'}" {($thongtin[0]['sDiaDiemMoi']=="Hội trường tầng 1 Sở Ngoại vụ")?'selected':''}>Hội trường tầng 1 Sở Ngoại vụ</option>
                                  
                                        </select>
									</div>-->
									
                                </div>
                                
                                <div class="form-group noidung">
									<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                    <label for="" class="col-sm-3" style="width:33.333333%; padding-top: 7px;">Tài liệu họp</label>
									
                                    <div class="col-sm-9" style="width:66.666666666%">
                                        <input type="file" name="files[]" multiple="" tabindex="6" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                    </div>
									</form>
                                </div>
								
								<div class="themsau"></div>
                              <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px; float:right"><i class="fa fa-plus"></i></button>
                            </div>
							
							
								<!--{$vanban['iQuyenHan_DHNB']}-->
                            
                            
                        </div>
						
						{if !empty($tailieu)}
							<div class="col-md-12" style="text-align: -webkit-center;">
                            <div class="form-group">
                                <label for="">Tệp tin đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="">Tên file / ghi chú </th>
                                            <th class="text-center" width="15%"></th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                            <th class="text-center" width="8%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {$i=1}
                                        {foreach $tailieu as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td >{$f.sTenFile}</td>
												<td class="text-center">
													<a class="tin1" href="{$f.sDuongDan}" target="_blank">Xem</a> / <a class="tin1" href="{$f.sDuongDan}" download>Download</a>
												</td>
												
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
												
                                                <td class="text-center">
														<button type="submit" class="btn btn-default" onclick="return confirm('Bạn có chắc muốn xóa tệp tin này?');" name="xoa" value="{$f.sDuongDan}"><i class="fa fa-trash"></i></button>
																							
                                                </td>
                                            </tr>
                                        {/foreach}
                                    
                                    </tbody>
                                </table><br>
                                
                            </div>
                        </div>
							{/if}
							
							<div class="col-md-12" style="margin-top:20px; text-align: center;">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</button> 
                            </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
   
</script>

