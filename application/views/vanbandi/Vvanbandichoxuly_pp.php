<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản trình ký
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form class="form-horizontal" method="post" action=""  enctype="multipart/form-data">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="">Trích yếu văn bản</th>
                                    <th width="35%">Ý kiến người gửi</th>
                                    <th width="25%">Ý kiến gửi đi/trả lại</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td >
										<p><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin" ><b style="color:black">{$dl.sMoTa} </b></a></p>
										{if $dl.FK_iMaLVB == 10}
											<p><b class="text-center" style="color:blue">GIẤY MỜI</b><br>
											- Thời gian: <b style="color: blue">{$dl.sGioMoi}</b> ngày <b style="color: blue">{$dl.sNgayMoi}</b><br>
											- Địa điểm: <b style="color: blue">{$dl.sDiaDiemMoi} </b>
											</p>
										{else}
											{if $dl.iSoVBDi > 0}- Số văn bản: <b style="color:red">{$dl.iSoVBDi}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---{/if}
											- Trả lời VB đến số: 
												{$mangsoden = explode(',' , $dl.iSoDen)}
												{foreach $mangsoden as $sd} 
													{$sd = trim($sd)}
													{if is_numeric($sd) && $sd > 0}
														<a href="dsvanbanden?soden={$sd}" target="_blank"><b style="color:red; font-size: 14px">{$sd}</b></a>, 
													{else}
														{$sd}, 
													{/if}
												{/foreach}
												<br>
										{/if}
											- Ký hiệu: {$dl.sKyHieu}<br>
											- Người tạo: {$dscanbo[$dl.FK_iMaCB_Nhap]}<br>
											- Người ký: <b>{$dscanbo[$dl.FK_iMaCB_Ky]}</b><br>
											- Nơi nhận:
											{if !empty($dl.sNoiNhan)}
												{$dsdonvi = layemaildonvi()}
													{$dsemail = explode( ',', $dl.sNoiNhan)}
													{foreach $dsemail as $email}
														{$email = trim($email)}
														{$k = 0}
															{foreach $dsdonvi as $donvi}
																{if $donvi.sEmail == $email}
																	{$k = 1}
																	{$donvi.sTenDV}; &nbsp; {break}
																{/if}
															{/foreach}
															{if $k == 0}{$email}; &nbsp;{/if}
													{/foreach}
													<br>
														{if !empty($dl.FK_iMaDV_Ngoai)}
															{layDV_Ngoai($dl.FK_iMaDV_Ngoai)}
														{/if}
											{/if}
											<p style="text-align:right"><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p>
											{$dsfile = layDSFile($dl.PK_iMaVBDi)}
											<div style="max-height:250px;  overflow:auto">
												{foreach $dsfile as $f}
												{$cbtao = layTTCB($f.FK_iMaCB)}
													<p style="margin-bottom:5px; border-bottom:dashed 1px gray"><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{$f.sTenFile}</span></a> <i style="color: silver">({$cbtao[0].sHoTen} - {date_time2($f.sThoiGian)})</i></p>
												{/foreach}
											</div>
										
										</td>
                                        {if $dl.iVBDang == 1}<td style="background: url(/vpdtsongoaivu/files/images/codang.png) bottom right no-repeat;vertical-align: middle;background-size: 20%;">{else} <td >{/if}
                                           {$luuvet = array_reverse(layDuLieu('FK_iMaVBDi',$dl.PK_iMaVBDi,'tbl_luuvet_vbdi'))}											
											{$ykien=''}
											<div style="max-height:300px;  overflow:auto">
											{foreach $luuvet as $lv}
												"{if $lv.sYKien != $ykien}{$lv.sYKien} {/if}" <br> <span style="float:right"><b>{$dscanbo[$lv.FK_iMaCB_Gui]}</b> &rarr; {$dscanbo[$lv.FK_iMaCB_Nhan]} - {date_time2($lv.sThoiGian)}</span><br><hr style="border-bottom: 1px dashed gray; margin: 10px;">
												{$ykien=$lv.sYKien}
											{/foreach}
											</div>
                                        </td>

                                        <td class="text-center">
                                            <p>
                                                <select name="truongphong_{$dl.PK_iMaVBDi}" required="" style="width: 100%" id="" class="form-control select2">
                                                    {if !empty($truongphong)}
                                                        {foreach $truongphong as $tp}
                                                            <option value="{$tp.PK_iMaCB}" selected="">{$tp.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <textarea name="ykien_{$dl.PK_iMaVBDi}" id="" rows="4" placeholder="Ý kiến gửi lên lãnh đạo/Ý kiến trả lại chuyên viên" class="form-control"></textarea>
                                            </p>
											<p><input type="file" name="files{$dl.PK_iMaVBDi}[]" placeholder="Click vào đây để chọn files" id="" multiple=""></p>
											
											<br>
											<p>
                                                <button name="guilen" value="{$dl.PK_iMaVBDi}" class="btn btn-primary btn-xs" style="width: 50%" >Gửi lên</button>
                                            
												{if $dl.FK_iMaCB_Nhap != $vanban['PK_iMaCB']}
                                                <button name="tralai" value="{$dl.PK_iMaVBDi}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>{/if}
												{if $dl.FK_iMaCB_Nhap == $vanban['PK_iMaCB']}
													<button name="sualai" id="" value="" class="btn btn-primary btn-xs"  style="background-color: lavender;"><a href="vanban?id={$dl.PK_iMaVBDi}">Sửa</a></button>
													
													<button type="submit" name="xoa" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" value="{$dl.PK_iMaVBDi}" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" class="btn btn-default"><i class="fa fa-trash-o"></i></button>
												
												{/if}
                                            </p>
											
                                        </td>
                                        
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>