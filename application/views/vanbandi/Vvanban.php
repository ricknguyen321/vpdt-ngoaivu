<script src="{$url}assets/js/vanbandi/vanbandi.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Nhập mới văn bản đi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số văn bản</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="sovanban" value="{($thongtin)?($thongtin[0]['iSoVBDi'])?$thongtin[0]['iSoVBDi']:'':''}" placeholder="Tự động / Văn thư cấp số" {if $vanban['PK_iMaCB'] != 730 && $vanban['PK_iMaCB'] != 731 && $vanban['PK_iMaCB'] != 705 && $vanban['PK_iMaCB'] != 696} readonly="" {/if} class="form-control kyhieu" id="">
	 
                                    </div>
                                </div>
                                
                                <div class="form-group {if $vanban['iQuyenHan_DHNB'] ==9}hide{/if}">
                                    <label for="" class="col-sm-4 control-label">Lãnh đạo *</label>
                                    <div class="col-sm-8">
                                        <select name="lanhdaophong" id="" required class="form-control select2 nguoinhan" style="width:100%" {if $id > 0} disabled="disabled"{/if}>
                                            <option value="" id="">-- Chọn lãnh đạo --</option>
											{$tp=1}
                                            {if !empty($lanhdaophong)}
                                                {foreach $lanhdaophong as $ldp}
                                                <option value="{$ldp.PK_iMaCB}" {if $tp==1} selected {else} {/if}>{$ldp.sHoTen}</option>
												
												{$tp = $tp+1}
												
                                                {/foreach}
                                            {/if}
											
                                        </select>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Loại văn bản *</label>
                                    <div class="col-sm-8">
                                        <select name="loaivanban" id="" required class="form-control kyhieu select2" style="width:100%">
                                            <option value="">Đề nghị chọn đúng loại VB để hệ thống tự động cấp số được chính xác, xin cảm ơn./. --</option>
                                            {if !empty($dsloaivanban)}
                                                {foreach $dsloaivanban as $l}
                                                <option value="{$l.PK_iMaLVB}" {($thongtin)?($thongtin[0]['FK_iMaLVB']==$l.PK_iMaLVB)?'selected':'':''}>{$l.sTenLVB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>										
                                    </div>
                                </div>
								
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày tháng *</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control datepic datemask"  required id="" value="{($thongtin)?date_select($thongtin[0]['sNgayVBDi']):date('d/m/Y')}" name="ngaythang">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi dự thảo *</label>

                                    <div class="col-sm-8">
                                        <select name="noiduthao" required id="" class="form-control kyhieu select2" style="width:100%">
                                            {if !empty($dsduthao)}
                                                {foreach $dsduthao as $d}
                                                    <option value="{$d.PK_iMaPB}" {($thongtin)?($thongtin[0]['FK_iMaPB']=={$d.PK_iMaPB})?'selected':'':($vanban['FK_iMaPhongHD']==$d.PK_iMaPB)?'selected':''}>{$d.sTenPB}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-sm-4 control-label">

									<input type="checkbox" value="1" {if $thongtin[0]['iVBDang'] == 1}checked{/if} name="iVBDang" > <b style="">Văn bản Đảng</b>
										
									</div>
									
                                </div>
								
                            </div>
							
							<div class="col-md-12" style="width:50.1%">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label" style="color:blue">Trích yếu văn bản đi *</label>
                                        <textarea name="mota" id="" required class="form-control" placeholder="Vui lòng nhập chính xác trích yếu văn bản đi, trích yếu sẽ trở thành tiêu đề của email khi văn bản được phát hành" {if $vanban['FK_iMaPhongHD']==76}rows="5"{else} rows="4" {/if}>{if !empty($thongtin)}{$thongtin[0]['sMoTa']}{else}{if !empty($ttvbd)}{$ttvbd[0]['sMoTa']}{/if}{/if}</textarea>
                                    </div>
                                </div>
                            </div>
							
                            <div class="col-md-12" style="width:49.9%">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Ý kiến gửi lên lãnh đạo</label>
                                        <textarea name="ykien" id="" {if $vanban['iQuyenHan_DHNB'] == 19}required{/if} class="form-control" placeholder="..." {if $vanban['FK_iMaPhongHD']==76}rows="5"{else} rows="4" {/if} {if $id > 0} readonly="readonly"{/if}>{($thongtin)?$thongtin[0]['ykien_chuyen']:''}Kính trình lãnh đạo./.</textarea>
                                    </div>
                                </div>
                            </div>                            
							
							<div class="col-md-4">
                                <div class="form-group an {if empty($loaivanban) || $loaivanban!=10}hide{/if}">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Ngày mời *</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="ngaymoi" tabindex="8" value="{($thongtin)?($thongtin[0]['sNgayMoi']!=0000-00-00)?date_select($thongtin[0]['sNgayMoi']):'':''}" class="form-control datepic datemask req" id="">
                                    </div>
                                </div>                                
								
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="width:33.333333%; padding-right:0px; color:blue">VB đến số *</label>
                                    <div class="col-sm-8" style="width:66.66666666%">
                                        <input type="text" required class="form-control" tabindex="7" value="{if !empty($thongtin)}{$thongtin[0]['iSoDen']}{else}{if !empty($isoden)}{$isoden}{/if}{/if}" name="sotraloi" placeholder="465, 495... không có nhập: 00">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Người ký *</label>

                                    <div class="col-sm-8">
                                        <select name="nguoiky" id="" required class="form-control select2">
                                            <option value="">-- Chọn người ký --</option>
                                            {if !empty($nguoiky)}
                                                {foreach $nguoiky as $nk}
                                                    <option value="{$nk.PK_iMaCB}" {($thongtin)?($thongtin[0]['FK_iMaCB_Ky']==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>							
							
							<div class="col-md-4">
                                <div class="form-group an {if empty($loaivanban) || $loaivanban!=10}hide{/if}">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Giờ mời *</label>
									
									<div class="col-sm-8">
										<div style="display: flex">
											<select name="gio" id="" required class="form-control " style="width:25%">
												<option value="00" {($gio == 00 || $gio == 0) ? 'selected': ''}>00</option>
												<option value="01" {($gio == 01 || $gio == 1) ? 'selected': ''}>01</option> 
												<option value="02" {($gio == 02 || $gio == 2) ? 'selected': ''}>02</option>
												<option value="03" {($gio == 03 || $gio == 3) ? 'selected': ''}>03</option>
												<option value="04" {($gio == 04 || $gio == 4) ? 'selected': ''}>04</option>
												<option value="05" {($gio == 05 || $gio == 5) ? 'selected': ''}>05</option>
												<option value="06" {($gio == 06 || $gio == 6) ? 'selected': ''}>06</option>
												<option value="07" {($gio == 07 || $gio == 7) ? 'selected': ''}>07</option>
												<option value="08" {($gio == 08 || $gio == 8) ? 'selected': ''}>08</option>
												<option value="09" {($gio == 09 || $gio == 9) ? 'selected': ''}>09</option>
												<option value="10" {($gio == 10) ? 'selected': ''}>10</option>
												<option value="11" {($gio == 11) ? 'selected': ''}>11</option>
												<option value="12" {($gio == 12) ? 'selected': ''}>12</option>
												<option value="13" {($gio == 13) ? 'selected': ''}>13</option>
												<option value="14" {($gio == 14) ? 'selected': ''}>14</option>
												<option value="15" {($gio == 15) ? 'selected': ''}>15</option>
												<option value="16" {($gio == 16) ? 'selected': ''}>16</option>
												<option value="17" {($gio == 17) ? 'selected': ''}>17</option>
												<option value="18" {($gio == 18) ? 'selected': ''}>18</option>
												<option value="19" {($gio == 19) ? 'selected': ''}>19</option>
												<option value="20" {($gio == 20) ? 'selected': ''}>20</option>
												<option value="21" {($gio == 21) ? 'selected': ''}>21</option>
												<option value="22" {($gio == 22) ? 'selected': ''}>22</option>
												<option value="23" {($gio == 23) ? 'selected': ''}>23</option>
											
											</select>
											
											&nbsp;:&nbsp;
										
											<select name="phut" id="" required class="form-control" style="width:25%">
												
												<option value="00" {($phut == 00 || $phut == 0) ? 'selected': ''}>00</option>
												<option value="05" {($phut == 05 || $phut == 5) ? 'selected': ''}>05</option> 
												<option value="10" {($phut == 10) ? 'selected': ''}>10</option>
												<option value="15" {($phut == 15) ? 'selected': ''}>15</option>
												<option value="20" {($phut == 20) ? 'selected': ''}>20</option>
												<option value="25" {($phut == 25) ? 'selected': ''}>25</option>
												<option value="30" {($phut == 30) ? 'selected': ''}>30</option>
												<option value="35" {($phut == 35) ? 'selected': ''}>35</option>
												<option value="40" {($phut == 40) ? 'selected': ''}>40</option>
												<option value="45" {($phut == 45) ? 'selected': ''}>45</option>
												<option value="50" {($phut == 50) ? 'selected': ''}>50</option>
												<option value="55" {($phut == 55) ? 'selected': ''}>55</option>
												
											</select>
										</div>
                                    </div>
									
									<!--<div class="col-sm-8">
                                        <input type="text" name="giomoi" tabindex="9" value="{($thongtin)?($thongtin[0]['sGioMoi']):''}" class="form-control req" id="" placeholder="VD 08:30">
                                    </div>-->
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                    <div class="col-sm-8">
                                        <input type="text" tabindex="4" class="form-control" value="{($thongtin)?$thongtin[0]['sKyHieu']:''}" id="" name="sokyhieu" placeholder="Tự động" {($thongtin)?'':'readonly="readonly"'} {if $id > 0} readonly="readonly"{/if}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{($thongtin)?$thongtin[0]['sTenCV']:''}" readonly="" id="" name="chucvu" placeholder="Tự động">
                                    </div>
                                </div>
                            </div>
														
							<div class="col-md-4">
                                <div class="form-group an {if empty($loaivanban) || $loaivanban!=10}hide{/if}">
                                    <label for="" class="col-sm-4 control-label" style="color:blue">Địa điểm*</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="diadiem" tabindex="10" value="{($thongtin)?$thongtin[0]['sDiaDiemMoi']:'Hội trường tầng 1 Sở Ngoại vụ'}" class="form-control req" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hashtag</label>

                                    <div class="col-sm-8">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <span class="typeahead__query">
                                                    <input tabindex="5" class="js-typeahead a form-control" value="{($thongtin)?$thongtin[0]['sTenLV']:''}" name="linhvuc" type="search" autocomplete="off"  placeholder="Tùy chọn">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
								 <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang</label>

                                    <div class="col-sm-8">
                                        <input tabindex="3" type="text" class="form-control" value="{($thongtin)?$thongtin[0]['iSoTrang']:'1'}" id="" name="sotrang">
                                    </div>
                                </div>
								
								
								
                            </div>
							
							
							<div class="col-md-5" style="width:33.333333%;margin-bottom: 20px;border-bottom: solid 2px;padding-bottom: 10px;">
								
                                <div class="form-group noidung">
                                    <label for="" class="col-sm-3" style="width:33.333333%; padding-top: 7px; color:blue">Tệp tin *</label>

                                    <div class="col-sm-9" style="width:66.666666666%">
                                        <input type="file" name="files[]" multiple="" tabindex="6" class="form-control" readonly="" placeholder="Click vào đây để chọn file">
                                    </div>
									
                                </div>
								
								<div class="themsau"></div>
								<button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;"><i class="fa fa-plus"></i></button><b class="text-danger"> Tải thêm</b> <span style="font-size:11px">(Tối đa: 20MB; có thể chọn nhiều tập tin; định dạng được chấp nhận: pdf, doc, docx, xls, xlsx, jpg, jpeg, png)</span>
			
							 </div>
							   
							
							<div class="col-md-5" style="width:33.3333%">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Độ mật</label>

                                    <div class="col-sm-8">                                        
                                       <select name="domat" id="" class="form-control select2" style="width:100%">
                                            {if !empty($domat)}
                                                {foreach $domat as $dm}
                                                    <option value="{$dm.PK_iMaDM}" {if !empty($thongtin) && $thongtin[0]['FK_iMaDM'] == $dm.PK_iMaDM} selected{/if}>{$dm.sTenDoMat}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
							</div>
							
							<div class="col-md-5" style="width:33.333%">
								<div class="form-group">
                                    <label for="" class="col-sm-4 control-label" style="">Độ khẩn</label>

                                    <div class="col-sm-8">
                                        <select name="dokhan" id="" class="form-control select2" style="width:100%">
                                        {if !empty($dokhan)}
                                            {foreach $dokhan as $dk}
                                                <option value="{$dk.PK_iMaDK}" {if !empty($thongtin) && $thongtin[0]['FK_iMaDK'] == $dk.PK_iMaDK} selected{/if}>{$dk.sTenDoKhan}</option>
                                            {/foreach}
                                        {/if}
                                        </select>
                                    </div>
                                </div>
							</div>
								
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Email nhận ngoài VPĐT: </label> (<i>Mỗi email cách nhau bởi dấu phẩy</i>)
                                        <input type="text" class="form-control" name="nhanmailngoai" value="{if $thongtin['0']['sNoiNhanNgoai']}{$thongtin['0']['sNoiNhanNgoai']} {elseif $vanban['FK_iMaPhongHD'] == 77}phongletan.dfa.hn@gmail.com{/if}" placeholder="Ví dụ: ricknguyen321@gmail.com, nguyenvanbinh_songv@hanoi.gov.vn">
                                    </div>
                                </div>
                            </div>
							
							<div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Email nhận: </label> (<i>Tìm kiếm và chọn nơi nhận tại đây hoặc tích chọn nơi nhận tại bảng phía dưới</i>)
										<select name="emailnhan[]" multiple="" id="" data-placeholder="" class="form-control select2" style="width:100%">
                                            {if !empty($dsemailnhan)}
                                                {foreach $dsemailnhan as $e}
													<option value="{$e.sEmail}" {if !empty($mangnguoinhan)}{if in_array($e.sEmail, $mangnguoinhan)}selected{/if}{/if}>{$e.sTenDV}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
										
                                    </div>
                                </div>
                            </div>
							
							<div class="col-md-12" style="max-height:350px;overflow: scroll;">

							<table id="" class="table table-bordered table-striped">
								<thead>
									<tr style="background-color: rgb(19, 115, 191);">
										<th>Quận huyện:</th>
										<th>Sở Ban Ngành:</th>
										<th>Đơn vị khác:</th>
									</tr>
								</thead>
								<tbody>
									{$k = 0}
									{if !empty($donvi)} {foreach $donvi as $dv}
									<tr>
										<td style="padding: 0px 10px;">
											{if !empty($quanhuyen[$k])} 
												<input type="checkbox" value="{$quanhuyen[$k].sEmail}" name="noinhanmail[]" id="{$quanhuyen[$k].sEmail}">
												<label for="{$quanhuyen[$k].sEmail}" style="font-weight:400; font-size:14px"> {$quanhuyen[$k].sTenDV} </label>
											{/if}
										</td>
										<td style="padding: 0px 10px;">
											{if !empty($sobannganh[$k])}
												<input type="checkbox" value="{$sobannganh[$k].sEmail}" name="noinhanmail[]" id="{$sobannganh[$k].sEmail}">
												<label for="{$sobannganh[$k].sEmail}" style="font-weight:400; font-size:14px"> {$sobannganh[$k].sTenDV} </label>
											{/if}
										</td>
										<td style="padding: 0px 10px;">	
											{if !empty($dv)} 
												<input type="checkbox" value="{$dv.sEmail}" name="noinhanmail[]" id="{$dv.sEmail}">
												<label for="{$dv.sEmail}" style="font-weight:400; font-size:14px"> {$dv.sTenDV} </label>
											{/if}
										</td>
									</tr>
									{$k = $k +1}
									{/foreach} {/if}
								</tbody>
							</table>
							</div>
							 
							
                            <!--<div class="col-md-12" style="max-height:350px;overflow: scroll;">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p><label for="" class="control-label">Quận huyện:</label></p>
                                        {if !empty($quanhuyen)}
                                            {foreach $quanhuyen as $qh}
                                                <input type="checkbox" value="{$qh.sEmail}" {if !empty($mangnguoinhan)}{if in_array($qh.sEmail, $mangnguoinhan)}checked{/if}{/if} name="noinhanmail[]" id="{$qh.sEmail}"> <label for="{$qh.sEmail}" style="font-weight:400; font-size:14px"> {$qh.sTenDV} </label><br>
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p><label for="" class="control-label">Sở Ban Ngành:</label></p>
                                        {if !empty($sobannganh)}
                                            {foreach $sobannganh as $sn}
                                                <input type="checkbox" value="{$sn.sEmail}" {if !empty($mangnguoinhan)}{if in_array($sn.sEmail, $mangnguoinhan)}checked{/if}{/if} name="noinhanmail[]" id="{$sn.sEmail}"> <label for="{$sn.sEmail}" style="font-weight:400; font-size:14px"> {$sn.sTenDV} </label> <br>
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                       <p> <label for="" class="control-label">Đơn vị khác:</label></p>
                                        {if !empty($donvi)}
                                            {foreach $donvi as $dv}
                                                <input type="checkbox" value="{$dv.sEmail}" {if !empty($mangnguoinhan)}{if in_array($dv.sEmail, $mangnguoinhan)}checked{/if}{/if} name="noinhanmail[]" id="{$dv.sEmail}"> <label for="{$dv.sEmail}" style="font-weight:400; font-size:14px"> {$dv.sTenDV} </label> <br>
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
                            </div>-->
						<!--	<div class="col-md-12"  style="max-height:250px;overflow: scroll;">
                                <label for="" class="control-label">Nơi nhận ngoài thành phố hà nội</label><br>
                                {if !empty($donvi_ngoai)}
                                {foreach $donvi_ngoai as $dvn}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="checkbox" value="{$dvn.PK_iMaTT}" name="donvingoai[]"> {$dvn.sTenDV} <br>
                                    </div>
                                </div>
                                {/foreach}
                                {/if}
                            </div> -->
							
							 <div class="col-md-7" style="width:33.333333%">
                                
                            </div>
														
                                <div class="form-group" style="display:none">
                                    <div class="col-md-12" style="width:100%; text-align: center;">
                                        <input type="radio" name="duthao" value="1" required> <label style="padding-right:15px;" for="" class="control-label">Có dự thảo UB TP</label>
                                        <input type="radio" name="duthao" value="2" checked> <label style="" for="" class="control-label">Không có dự thảo UB TP</label>
                                    </div>
                                </div>

								<!--{$vanban['iQuyenHan_DHNB']}-->
                            
                            <div class="col-md-12" style="margin-top:20px; text-align: center;">
                                <button tabindex="11" type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">{($thongtin)?'Cập nhật':'Thêm mới<i></i>'}</button> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');

		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>
<script>
    var data = {
        "timkiem": [
            {if !empty($dslinhvuc)}
                {foreach $dslinhvuc as $l}
                    "{$l.sTenLV}",
                {/foreach}
            {/if}
        ]
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 15,
        order: "asc",
        hint: true,
        backdrop: {
            "background-color": "#fff"
        },
        source: {
            timkiem: {
                data: data.timkiem
            }
        },
        debug: false
    });
    $(document).on('change','select[name=noinhan]',function(){
        var ten = $('.noinhan option:selected').map(function() {
            return this.text;
        }).get().join(',');
        $('input[name=tennoinhan]').val(ten);
    });
    $(document).on('change','.nguoinhan',function(){
            var nguoinhan1  = $('select[name=lanhdaophong]').val();
            if(nguoinhan1.length>0)
            {
                $('button[name=luudulieu]').attr('type','submit');
            }
            else{
                $('button[name=luudulieu]').attr('type','button');
            }
        });
</script>

