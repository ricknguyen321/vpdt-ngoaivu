<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản đi đã xử lý
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="20%">Ngày nhập</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="30%">Nơi nhận</th>
                                    <th width="6%">Tệp tin</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td class="text-center">{date_time($dl.sNgayNhap)}</td>
                                        <td ><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">{$dl.sMoTa}</a> <br>-Người nhập: {$dl.sHoTen}</td>
                                        <td>
											<textarea name="" id="" class="form-control" rows="6" readonly="readonly" style="background: none; border: none;">{$dl.sNoiNhan}
											</textarea>
										</td>
                                        <td class="text-center">
											
											<p><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> </p>
										
											<p>{if $dl.iFile==1} <a target="_blank" href="{$dl.sDuongDan}" class="tin1">{if layDuoiFile($dl.sDuongDan)=='pdf'}Xem tệp tin{else}Xem dự thảo{/if}</a>{/if}</p>
										 
										</td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>