<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản người dùng tạo <b>({$count})</b>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="20%">Nơi nhận</th>
                                    <th width="6%">Tệp tin</th>
                                    <th width="5%" class="text-center">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
								{$dsdonvi = layemaildonvi()}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.iSoVBDi}/{$dl.sKyHieu}</td>
										
										{$qt = layKQChuyenDi($dl.PK_iMaVBDi)}
										{if $qt[0].iTrangThai == 3}<td style="background: url(/vpdtsongoaivu/files/images/approved.png) bottom right no-repeat;vertical-align: middle;background-size: 15%;">{else} <td>{/if}
										<p><b><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">
                                            {$dl.sMoTa}</a> </b></p>
                                            - Người tạo: {$dscanbo[$dl.FK_iMaCB_Nhap]} <br>
                                            - Ngày tạo: {date_time($dl.sNgayNhap)}
                                        </td>
                                        {if $dl.iVBDang == 1}<td style="background: url(/vpdtsongoaivu/files/images/codang.png) bottom right no-repeat;vertical-align: middle;background-size: 20%;">{else} <td >{/if}
											<div style="max-height:150px;  overflow:auto">
											
												{$dsemail = explode( ',', $dl.sNoiNhan)}
												{foreach $dsemail as $email}
													{$email = trim($email)}
													{$k = 0}
														{foreach $dsdonvi as $donvi}
															{if $donvi.sEmail == $email}
																{$k = 1}
																{$donvi.sTenDV};<br>{break}
															{/if}
														{/foreach}
														{if $k == 0}{$email};<br>{/if}
												{/foreach}
												<br>
													{if !empty($dl.FK_iMaDV_Ngoai)}
														{layDV_Ngoai($dl.FK_iMaDV_Ngoai)}
													{/if}
											</div>
                                        </td>
                                        <td class="text-center">
									
											<p><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> </p>
											
											<p>{if !empty($dl.sDuongDan)} <a target="_blank" href="{$dl.sDuongDan}" id="{$dl.PK_iMaVBDi}" class="tin1 layma">{if $dl.iSoVBDi>0}Xem văn bản đi{else}Xem dự thảo{/if}</a>{/if}</p>
											
											 <span class="label label-{($dl.iSoVBDi>0)?'success':'warning'}" style="font-size: 14px; padding: 5px;">{($dl.iSoVBDi>0)?'Đã cấp số':'Chưa cấp số'}</span>
										 
                                        </td>
                                        <td class="text-center">
                                            {if $qt[0].iTrangThai != 3 && $vanban['PK_iMaCB']==$dl.FK_iMaCB_Nhap}
											<p><a href="{$url}vanban?id={$dl.PK_iMaVBDi}&loai=choso" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default"><i class="fa fa-edit"></i></a> </p>
											
                                            <button type="submit" name="xoa" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" value="{$dl.PK_iMaVBDi}" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" class="btn btn-default"><i class="fa fa-trash-o"></i></button>
                                            {else}
                                            -
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>