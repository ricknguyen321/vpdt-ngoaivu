<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách giấy mời
                    </h3>
                </div>
                <div class="col-sm-9">
                    <div class="col-md-6 tieude">
                        <form action="" method="get">
                            
                        </form>
                    </div>
                    <div class="col-md-6 abc">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label"><a class="font" name="anhien" href="javascript:void(0);"><i class="fa fa-search"></i> Tìm kiếm</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form class="form-horizontal anhien hide" method="get" enctype="multipart/form-data" autocomplete="off">
                <div class="row"><br>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label"></label>
                                <div class="col-sm-8">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Số ký hiệu</label>

                                <div class="col-sm-8">
                                    <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Nơi dự thảo</label>

                                <div class="col-sm-8">
                                    <select name="noiduthao" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn nơi dự thảo --</option>
                                        {if !empty($dsduthao)}
                                            {foreach $dsduthao as $d}
                                                <option value="{$d.PK_iMaPB}" {($noiduthao)?($noiduthao==$d.PK_iMaPB)?'selected':'':''}>{$d.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Ngày văn bản</label>

                                <div class="col-sm-8">
                                    <input type="text" name="ngaythang" value="{($ngaythang)?$ngaythang:''}" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class=" col-md-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Trích yếu</label>
                                    <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người ký</label>

                                <div class="col-sm-8">
                                    <select name="nguoiky" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn người ký --</option>
                                        {if !empty($nguoiky)}
                                            {foreach $nguoiky as $nk}
                                                <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6 hide">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Người nhập</label>

                                <div class="col-sm-8">
                                    <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn người nhập --</option>
                                        {if !empty($dsnguoinhap)}
                                            {foreach $dsnguoinhap as $nn}
                                                <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button name="timkiem" value="tk" class="btn btn-primary">Tìm kiếm</button>
                        </div>
                    </div>
                </div>
            </form>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-md-12 table-responsive">
                    {if $vanban['iQuyenHan_DHNB']==9}<button class="btn btn-primary btn-xs pull-right hide" type="submit" name="guimail" value="guimail">Gửi Mail lên Thành Phố</button>{/if}
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="6%" class="text-center">Ngày nhập</th>
                                    <th width="6%" class="text-center">Số đi</th>
                                    <th width="35%">Trích yếu</th>
                                    <th width="17%" class="text-center visible-lg">Nơi nhận</th>
                                    {if $vanban['iQuyenHan_DHNB']==9 ||  $vanban['PK_iMaCB']== 705}
										<th width="8%">Tác vụ</th>
                                    {/if}
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($dsvanban)} {$i=1}
                                    {foreach $dsvanban as $di}
                                        <tr>
                                            <td class="text-center">{$i++}</td>
                                            <td class="text-center">{date_select($di.sNgayVBDi)}</td>
                                            <td class="text-center" style="font-size:18px;color: red"><b>{$di.iSoVBDi}</b></td>
                                            {$qt = layKQChuyenDi($di.PK_iMaVBDi)}
											{if $qt[0].iTrangThai == 3}<td style="background: url(/vpdtsongoaivu/files/images/approved.png) bottom right no-repeat;vertical-align: middle;background-size: 12%;">{else} <td>{/if}
											<p><a href="{$url}thongtinvanban?id={$di.PK_iMaVBDi}" class="tin" style="color: black !important;"><b>{$di.sMoTa}</b></a></p>
											- Giờ mời: <b style="color: blue">{$di.sGioMoi}</b><br>
											- Ngày mời: <b style="color: blue">{date_select($di.sNgayMoi)}</b><br>
											- Địa điểm: <b style="color: blue">{$di.sDiaDiemMoi}</b><br>
											- Người ký: <b>{$di.sHoTen}</b>
											
											</td>
                                            <td class="visible-lg">{$di.sNoiNhan}
											<br>
                                            {if !empty($di.FK_iMaDV_Ngoai)}
                                            {layDV_Ngoai($di.FK_iMaDV_Ngoai)}
                                            {/if}
											</td>
                                            {if $vanban['iQuyenHan_DHNB']==9 ||  $vanban['PK_iMaCB']== 705}
                                            <td class="text-center">
                                                <p><a href="{$url}vanban?id={$di.PK_iMaVBDi}&loai=giaymoi" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default"><i class="fa fa-edit"></i></a> </p>
                                                <button type="submit" name="xoa" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu"  value="{$di.PK_iMaVBDi}" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" class="btn btn-default"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                            <td class="text-center hide"><input type="checkbox" name="mavanban[{$di.PK_iMaVBDi}]"></td>
                                            {/if}
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                        <div class="pull-right">{$phantrang}</div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
        $('td').css('vertical-align', 'middle');
        $('.anhien').css('background', 'rgba(60, 141, 188, 0.07)');
        $(document).on('click','a[name=anhien]',function(){
            $('.anhien').toggle();
            $('.anhien').removeClass('hide');
        });
        $('.control-label').css('text-align', 'left');
    });
</script>
<script>
function myFunction() {
    window.open("{$url}upload_files_vbdi_gm","mywindow","menubar=1,resizable=1,width=550,height=320");
}
</script>
