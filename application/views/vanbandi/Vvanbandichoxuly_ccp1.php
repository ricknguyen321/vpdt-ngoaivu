<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản trình ký
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:4%" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="20%">Ý kiến</th>
                                    <th width="6%">Tệp tin</th>
                                    <th width="30%">Ý kiến gửi đi</th>
                                    <th width="5%" class="text-center">Gửi</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td ><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">{$dl.sMoTa}</a> <br>-Người nhập: {$dscanbo[$dl.FK_iMaCB_Nhap]}</td>
                                        <td >
                                            <b>{$dl.sYKien}</b><br>
                                            - {$dscanbo[$dl.FK_iMaCB_Gui]} <br>
                                            - {date_time($dl.sThoiGian)}
                                        </td>
                                        <td class="text-center"><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><span class="label label-success">Tải file</span></a>{if !empty($dl.sDuongDan)} <br><a target="_blank" href="{$dl.sDuongDan}" id="{$dl.PK_iMaVBDi}" class="tin1 layma">{if layDuoiFile($dl.sDuongDan)=='pdf'}Xem [file_pdf]{else}Xem{/if}</a>{/if} 
                                            <p><span class="label label-{($dl.iSoVBDi>0)?'success':'warning'}">{($dl.iSoVBDi>0)?'Đã cấp số':'Chưa cấp số'}</span></p>
                                        </td>
                                        <td class="text-center">
                                            <p>
                                                <select name="lanhdaoso_{$dl.PK_iMaVBDi}" id="{$dl.PK_iMaVBDi}" style="width: 100%" class="form-control select2">
                                                    {if !empty($lanhdaoso)}
                                                        {foreach $lanhdaoso as $ld}
                                                            <option value="{$ld.PK_iMaCB}">{$ld.sHoTen}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </p>
                                            <p>
                                                <textarea name="ykien_{$dl.PK_iMaVBDi}" id="" rows="3" placeholder="Nhập ý kiến tại đây" class="form-control"></textarea>
                                            </p>
                                        </td>
                                        <td class="text-center">
                                            <p>
                                                <button type="submit" name="guilen" id="id_{$dl.PK_iMaVBDi}" value="{$dl.PK_iMaVBDi}" class="btn btn-primary btn-xs">Gửi lên</button>
                                            </p>
                                            <p>
                                                <button type="submit" name="tralai" value="{$dl.PK_iMaVBDi}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>
                                            </p>
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>