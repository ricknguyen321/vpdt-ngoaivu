<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản trình ký
      </h1>
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="panel-body">
                    <div class="col-md-2"><a href="{$url}vanbandichoxuly_ld?trangthai=1" class="btn btn-primary btn-xs">Văn bản trình ký</a> ({$count1})</div>
                    <div class="col-md-2"><a href="{$url}vanbandichoxuly_ld?trangthai=2" class="btn btn-danger btn-xs">Đã trả lại</a> ({$count2})</div>
                    <div class="col-md-2"><a href="{$url}vanbandichoxuly_ld?trangthai=3" class="btn btn-info btn-xs">Đã duyệt</a> ({$count3})</div>
                </div>
				<div class="pull-right" style="margin-top: -20px; margin-bottom: -15px">{$phantrang}</div>
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form class="form-horizontal" method="post" action=""  enctype="multipart/form-data">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>                                   
                                    <th width="">Trích yếu văn bản đi</th>
									{if $trangthai==1}
                                    <th width="35%">Ý kiến người gửi</th>
                                    <th width="30%">Ý kiến lãnh đạo</th>
									{else}
									 <th width="35%">Ý kiến người gửi</th>
                                    <th width="5%"></th>
										{/if}
                                    <!--<th width="6%" class="text-center"></th>-->
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
								
                                {foreach $dsvanban as $dl}
                                    <tr  id ="{$dl.PK_iMaVBDi}">                                        
                                        <td >
											<p><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin" ><b style="color:black">{$dl.sMoTa} </b></a></p>
											{if $dl.FK_iMaLVB == 10}
												<p><b class="text-center" style="color:blue">GIẤY MỜI</b><br>
												- Thời gian: <b style="color: blue">{$dl.sGioMoi}</b> ngày <b style="color: blue">{$dl.sNgayMoi}</b><br>
												- Địa điểm: <b style="color: blue">{$dl.sDiaDiemMoi} </b>
												</p>
											{else}
												{if $dl.iSoVBDi > 0}- Số văn bản: <b style="color:red">{$dl.iSoVBDi}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---{/if}
												- Trả lời VB đến số: 
												{$mangsoden = explode(',' , $dl.iSoDen)}
												{foreach $mangsoden as $sd} 
													{$sd = trim($sd)}
													{if is_numeric($sd) && $sd > 0}
														<a href="dsvanbanden?soden={$sd}" target="_blank"><b style="color:red; font-size: 14px">{$sd}</b></a>, 
													{else}
														{$sd}, 
													{/if}
												{/foreach}
												<br>
											{/if}
											- Người tạo: {$dscanbo[$dl.FK_iMaCB_Nhap]}<br>
											- Nơi nhận:
											{if !empty($dl.sNoiNhan)}
												{$dsdonvi = layemaildonvi()}
													{$dsemail = explode( ',', $dl.sNoiNhan)}
													{foreach $dsemail as $email}
														{$email = trim($email)}
														{$k = 0}
															{foreach $dsdonvi as $donvi}
																{if $donvi.sEmail == $email}
																	{$k = 1}
																	{$donvi.sTenDV}; &nbsp; {break}
																{/if}
															{/foreach}
															{if $k == 0}{$email}; &nbsp;{/if}
													{/foreach}
													<br>
														{if !empty($dl.FK_iMaDV_Ngoai)}
															{layDV_Ngoai($dl.FK_iMaDV_Ngoai)}
														{/if}
											{/if}
											<p style="text-align:right"><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a></p>
											{$dsfile = layDSFile($dl.PK_iMaVBDi)}
											<div style="max-height:250px;  overflow:auto">
												{foreach $dsfile as $f}
													{$cbtao = layTTCB($f.FK_iMaCB)}
													<p style="margin-bottom:5px; border-bottom:dashed 1px gray"><a class="tin1" href="{$f.sDuongDan}" target="_blank"><span style="color:brown">{$f.sTenFile}</span></a> <i style="color: silver">({$cbtao[0].sHoTen} - {date_time2($f.sThoiGian)})</i>
													</p>
												{/foreach}
											</div>
										</td>
                                        {if $dl.iVBDang == 1}<td style="background: url(/vpdtsongoaivu/files/images/codang.png) bottom right no-repeat;vertical-align: middle;background-size: 20%;">{else} <td >{/if}
											{$luuvet = array_reverse(layDuLieu('FK_iMaVBDi',$dl.PK_iMaVBDi,'tbl_luuvet_vbdi'))}			{$ykien=''}
											<div style="max-height:300px;  overflow:auto">
											{foreach $luuvet as $lv}
												"{if $lv.sYKien != $ykien}{$lv.sYKien} {/if}" <br> <span style="float:right"><b>{$dscanbo[$lv.FK_iMaCB_Gui]}</b> &rarr; {$dscanbo[$lv.FK_iMaCB_Nhan]} - {date_time2($lv.sThoiGian)}</span><br><hr style="border-bottom: 1px dashed gray; margin: 10px;">
												{$ykien=$lv.sYKien}
											{/foreach}
                                            </div>
                                        </td>
                                        
                                        <td class="text-center">
										<input type="text" name="yearup" value="{$year}" class="hidden" >
										{if $trangthai==1}
                                            <p>
                                                <textarea name="ykien_{$dl.PK_iMaVBDi}" id="" rows="5" placeholder="Ý kiến phê duyệt/trả lại..." class="form-control"></textarea>
                                            </p>
											<i>Chọn dự thảo lãnh đạo đã sửa</i>
											<p>
												<input type="file" name="files{$dl.PK_iMaVBDi}[]" placeholder="Click vào đây để chọn files" id="" multiple="" class="form-control">
											
											</p>
											<p>
												<br>
                                                <button name="dongy" id="id_{$dl.PK_iMaVBDi}" value="{$dl.PK_iMaVBDi}" class="btn btn-primary btn-xs" style="width: 50%" >Duyệt</button>                                            
												{if $dl.FK_iMaCB_Nhap != $vanban['PK_iMaCB']}
                                                <button name="tralai" value="{$dl.PK_iMaVBDi}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>{/if}
                                            
                                            </p>
											{/if}
                                        </td>
                                        <!--<td class="text-center">
                                            {if $trangthai==1}
                                            <p>-->
                                           
                                           <!-- {if $giaodienmobie != 1}
                                            <button name="kydientu" type="button" id="_lanhdaoPheduyet" class="btn" style="background-color: #008d4c;color: #fff" onclick="exc_sign_approved('{$url}{$dl.sDuongDan}','{$ci_session}');" value="{$dl.PK_iMaVBDi}">Ký Văn Bản<br>Qua ToKen</button>
                                            <br><br>
                                            <button name="kydientu" type="button" id="_lanhdaoPheduyet" class="btn" style="background-color: #008d4c;color: #fff" onclick="exc_sign_sim('{$dl.sDuongDan}','{$dl.PK_iMaVBDi}');" value="{$dl.PK_iMaVBDi}">Ký Văn Bản<br>Qua Sim</button>
                                            {/if} 
                                            
                                            {if $giaodienmobie == 1}
                                            <button name="kydientu" type="button" id="_lanhdaoPheduyet" class="btn" style="background-color: #008d4c;color: #fff" onclick="exc_sign_sim('{$dl.sDuongDan}','{$dl.PK_iMaVBDi}');" value="{$dl.PK_iMaVBDi}">Ký Văn Bản</button>
                                            {/if}-->

                                            
                                            <!--<button name="dongy" id="id_{$dl.PK_iMaVBDi}" value="{$dl.PK_iMaVBDi}" class="btn btn-primary btn-xs">Duyệt</button>
                                            
                                              

                                            </p>
                                            

                                            <p>

                                                <button name="tralai" value="{$dl.PK_iMaVBDi}" onclick="return confirm('Bạn muốn trả lại?');" class="btn btn-danger btn-xs">Trả lại</button>

                                            </p>

                                            {else}

                                            -

                                            {/if}

                                        </td>-->

                                    </tr>

                                {/foreach}

                            {/if}

                            </tbody>

                        </table>

                    </form>

                    <div class="pull-right">{$phantrang}</div>

                </div>

            </div>

        </div>

      <!-- /.box -->



    </section>

    <!-- /.content -->

</div>

<script>

    $(document).ready(function() {

        var url = window.location.href;

        $('td').css('vertical-align', 'middle');

        $('th').css('vertical-align', 'middle');

        // $('td').css('text-align', 'justify');

        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');

        $('.control-label').css('text-align', 'left');

    });

</script>

<script type="text/javascript">
        
        function callback(rv) {
            var obj = JSON.parse(rv);
            if (obj.Status == 0) {
                document.getElementById("file2").value = obj.FileServer;
            } else {
                document.getElementById("file2").value = obj.Message;
            }

            $('#LicenseDetailModal').modal('toggle');
        }


        function exc_sign_sim(url,maVB) { 
            $.post("vanbandichoxuly_ld",
            {
                kydientu:1,
                FileName: url,
                PK_iMaVBDi : maVB
            },
            function (data, status) {
                console.log("Data: " + data + "\nStatus: " + status);
                callback(data);
            });
        }

        /* function exc_sign_sim() {
            var test;
            $.post("PdfSign.ashx",
            {
                FileName: document.getElementById("file1").value,
                LanhDao: document.getElementById("_lanhdao").value,
                Phone: document.getElementById("_phone").value
            },
            function (data, status) {
                console.log("Data: " + data + "\nStatus: " + status);
                callback(data);
            });
        }*/

        function openfile() {
            window.open(document.getElementById("file2").value, 'popUpWindow', 'height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no');
            return false;
        }

        function showSignForm() {
            $("#LicenseDetailModal").modal('show');
        }
        $(document).on('click','#_lanhdaoPheduyet',function(){
            var id = $(this).val();
            $('#'+id).addClass('hide');
        });

        function exc_sign_approved(url,ci_session) {
            var prms = {};

            prms["FileUploadHandler"] = "https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/lanhdaoky";
            prms["SessionId"] = "ci_sessions="+ci_session+";kiemtra=ok";            
            prms["FileName"] = url; 

            var json_prms = JSON.stringify(prms);
            console.log(json_prms);
            vgca_sign_approved(json_prms, SignFileCallBack1);

        }

         function SignFileCallBack1(rv) {
            var received_msg = JSON.parse(rv);
            console.log(received_msg);
            if (received_msg.Status == 0) {
                console.log(received_msg);
                document.getElementById("_signature").value = received_msg.FileName + ":" + received_msg.FileServer + ":" + received_msg.DocumentNumber + ":" + received_msg.DocumentDate;
                document.getElementById("file1").value = received_msg.FileServer;
                document.getElementById("file2").value = received_msg.FileServer;
            } else {
                document.getElementById("_signature").value = received_msg.Message;
            }
        }

    </script>