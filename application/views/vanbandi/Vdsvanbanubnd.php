<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách các văn bản Sở Ngoại Vụ đã tham mưu trình UBND thành phố 
    
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <div class="row">
                <div class="col-md-12" >
                    <form class="form-horizontal anhien" style="padding-top:10px;" method="get" autocomplete="off">
                        <div class="col-md-4 anhien">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Từ ngày</label>
                                <div class="col-sm-8">
                                    <input type="text" name="tungay" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 anhien">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Đến ngày</label>

                                <div class="col-sm-8">
                                    <input type="text"  name="denngay" class="form-control datepic datemask" id="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 anhien">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label" style="padding:7px 0px;">Phòng ban</label>

                                <div class="col-sm-9">
                                    <select name="phongban" id="" class="form-control select2" style="width:100%">
                                        <option value="">-- Chọn phòng ban --</option>
                                        {if !empty($dsphongban)}
                                            {foreach $dsphongban as $pb}
                                            <option value="{$pb.PK_iMaPB}">{$pb.sTenPB}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 anhien">
                            <button type="submit" name="loc" value="loc" class="btn btn-primary btn-sm">Lọc</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">
                        <form action="" method="post">
                            <button type="submit" name="export" value="export" style="margin-top: -51px;margin-left: 60px;" class="btn btn-primary btn-sm">Xuất Excel <i class="fa fa-download"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <table id="" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="4%" class="text-center">STT</th>
                                <th width="10%" class="text-center">Số ký hiệu</th>
                                <th width="10%" class="text-center">Ngày ký</th>
                                <th width="12%" class="text-center">Loại văn bản</th>
                                <th width="32%" class="text-center">Trích yếu</th>
                                <th width="12%" class="text-center">Người ký</th>
                                <th width="20%" class="text-center">Đơn vị ban hành</th>
                            </tr>
                        </thead>
                        <tbody>
                        {if !empty($dsphongsl)}{$i=1}
                            {foreach $dsphongsl as $pb}
                                <tr style="background:#ecf0f5;">
                                    <th class="text-center">{intToRoman($i++)}</th>
                                    <th colspan="6">{$pb.sTenPB} - Tống số: {$pb.tong} Văn Bản</th>
                                </tr>
                                {if !empty($dsthongtinvb)}{$stt=1}
                                    {foreach $dsthongtinvb as $tt}
                                        {if $tt.FK_iMaPB==$pb.FK_iMaPB}
                                            <tr>
                                                <td class="text-center">{$stt++}</td>
                                                <td class="text-center">
                                                <p style="color:red;">{$tt.iSoVBDi}</p>
                                                {$tt.sKyHieu} </td>
                                                <td>{date_select($tt.sNgayVBDi)}</td>
                                                <td class="text-center">{$tt.sTenLVB}</td>
                                                <td><b style="color: black">{$tt.sMoTa}</b></td>
                                                <td>{$tt.sHoTen}</td>
                                                <td class="text-center">{$tt.sTenPB}</td>
                                            </tr>
                                        {/if}
                                    {/foreach}
                                {/if}
                            {/foreach}
                        {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('th').css('vertical-align', 'middle');
        $('td').css('vertical-align', 'middle');
        $('.anhien').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>