<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-3">
                    <h3 class="font">
                        Danh sách chỉ đạo của lãnh đạo <b>({$count})</b>
                    </h3>
                </div>
                <div class="col-sm-9">
                    
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
						<span class="pull-right">Để đánh dấu đã xem, nhấn nút "Đã xem".</span>
                            <span class="">{$phantrang}</span>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="50px" class="text-center">STT</th>
                                    
                                    <th width="" class="text-center">Trích yếu - Thông tin</th>
                                    <th width="15%" class="text-center">Lãnh đạo</th>
                                    <th width="35%" class="text-center">Chỉ đạo</th>
									<th width="10%" class="text-center">Thời gian</th>
									<th width="6%" class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {$i=1}
                                {if !empty($dschidao)}

                                {foreach $dschidao as $cd}

                                <tr>

                                    <td class="text-center">{$i++}</td>

                                    <td>

                                        <p><b><a style="color: black;" href="{$url}thongtinvanban?id={$cd.FK_iMaVBDi}">{if (date('Y-m-d',time()) <= $cd.sHanThongKe) && (date('Y-m-d',time()+86400*3) >= $cd.sHanThongKe)}<span style="color: #FF9800;font-weight: bold;">{$cd.sMoTa}</span>{elseif $cd.sHanThongKe < date('Y-m-d',time()) && $cd.sHanThongKe > '2017-01-01'}<span style="color: red">{$cd.sMoTa}</span>{else}<span>{$cd.sMoTa}</span>{/if}</a></b></p>
										
                                        <p>- Số VB đi: <b style="color:red">{$cd.iSoVBDi}</b><br>
										- Người tạo: {$cbtao = layTTCB($cd.FK_iMaCB_Nhap)}{$cbtao[0].sHoTen} <br>
										- Người ký: {$cbky = layTTCB($cd.FK_iMaCB_Ky)}{$cbky[0].sHoTen} <br>
										</p>

                                    </td>

                                    <td class="text-center" width="15%">
										<b>{$cd.sHoTen}</b>
                                    </td>
                                    <td>
										<p>{if $cd.iDaXem != 1}
											<b style="color:blue">{$cd.sNoiDung}</b>
										{else}
											{$cd.sNoiDung}
										{/if}</p>
									</td>
									<td class="text-center">{date_time2($cd.sThoiGian)}</td>
									<td class="text-center">
										{if $cd.iDaXem != 1}
											<button type="submit" name="daxem" class="btn btn-primary btn-xs" value="{$cd.id}">Đã xem</button>
										{/if}
									</td>
                                </tr>

                                {/foreach}

                                {/if}

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
