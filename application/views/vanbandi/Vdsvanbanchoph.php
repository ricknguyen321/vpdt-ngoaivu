<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header" style="padding: 0px">
      <div class="row">
         <div class="col-sm-12 deletepadding1">
            <div class="col-sm-4">
               <h3 class="font">
                  Danh sách văn bản đã duyệt phát hành 
               </h3>
            </div>
            <div class="col-md-8 abc" style="display:inline-flex">
               <form action="" method="get" style="width: 30%">
                  <div class="form-group">
                     <input type="text" name="trichyeu" value="{($trichyeu)?$trichyeu:''}" class="form-control" id="" placeholder="Tìm theo trích yếu">
                  </div>
               </form>
               <div class="form-group" style="width: 70%">
                  <label for="" class="col-sm-12 control-label"><a  class="font" name="anhien" href="javascript:void(0);"><i class="fa fa-search"></i> Tìm kiếm</a></label>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Default box -->
      <div class="box">
         <div class="box-body">
            <!--                <label for=""><a style="font-size:20px;" name="anhien" href="javascript:void(0);"><i class="fa fa-search"></i> Tìm kiếm</a></label>-->
            <form class="form-horizontal hide" method="get" enctype="multipart/form-data" autocomplete="off">
               <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="" class="col-sm-4 control-label">Loại văn bản</label>
                              <div class="col-sm-8">
                                 <select name="loaivanban" id="" class="form-control select2" style="width:100%">
                                    <option value="">-- Chọn loại văn bản --</option>
                                    {if !empty($dsloaivanban)}
                                    {foreach $dsloaivanban as $l}
                                    <option value="{$l.PK_iMaLVB}" {($loaivanban)?($l.PK_iMaLVB==$loaivanban)?'selected':'':''}>{$l.sTenLVB}</option>
                                    {/foreach}
                                    {/if}
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="" class="col-sm-4 control-label">Số ký hiệu</label>
                              <div class="col-sm-8">
                                 <input type="text" name="kyhieu" value="{($kyhieu)?$kyhieu:''}" class="form-control" id="">
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="" class="col-sm-4 control-label">Nơi dự thảo</label>
                              <div class="col-sm-8">
                                 <select name="noiduthao" id="" class="form-control select2" style="width:100%">
                                    <option value="">-- Chọn nơi dự thảo --</option>
                                    {if !empty($dsduthao)}
                                    {foreach $dsduthao as $d}
                                    <option value="{$d.PK_iMaPB}" {($noiduthao)?($noiduthao==$d.PK_iMaPB)?'selected':'':''}>{$d.sTenPB}</option>
                                    {/foreach}
                                    {/if}
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="" class="col-sm-4 control-label">Ngày văn bản</label>
                              <div class="col-sm-8">
                                 <input type="text" name="ngaythang" value="{($ngaythang)?$ngaythang:''}" class="form-control datepic datemask" id="">
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class=" col-md-12">
                              <div class="form-group">
                                 <label for="" class="control-label">Trích yếu</label>
                                 <textarea name="trichyeu" id="" class="form-control" rows="2">{($trichyeu)?$trichyeu:''}</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="" class="col-sm-4 control-label">Người ký</label>
                              <div class="col-sm-8">
                                 <select name="nguoiky" id="" class="form-control select2" style="width:100%;">
                                    <option value="">-- Chọn người ký --</option>
                                    {if !empty($nguoiky)}
                                    {foreach $nguoiky as $nk}
                                    <option value="{$nk.PK_iMaCB}" {($nguoikyvb)?($nguoikyvb==$nk.PK_iMaCB)?'selected':'':''}>{$nk.sHoTen}</option>
                                    {/foreach}
                                    {/if}
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 hide">
                           <div class="form-group">
                              <label for="" class="col-sm-4 control-label">Người nhập</label>
                              <div class="col-sm-8">
                                 <select name="nguoinhap" id="" class="form-control select2" style="width:100%">
                                    <option value="">-- Chọn người nhập --</option>
                                    {if !empty($dsnguoinhap)}
                                    {foreach $dsnguoinhap as $nn}
                                    <option value="{$nn.PK_iMaCB}">{$nn.sHoTen}</option>
                                    {/foreach}
                                    {/if}
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12" style="text-align:center; margin-top: 10px">
                           <button name="timkiem" value="tk" class="btn btn-primary btn-sm">Tìm kiếm</button>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
            <!--end quyền bằng 2 là văn thư form-->
            <div class="col-md-12">
               {if isset($thongbaoso)}
               <h3 class="formSentMsg" style="background:#b2dde0; padding:18px;">Số vừa duyệt là: <b style="color:red;">{$thongbaoso}</b></h3>
               {/if}
            </div>
            <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
               <form action="" method="post">
                  <table id="" class="table table-bordered table-hover">
                     <thead>
                        <tr>
                           <th style="width:50px" class="text-center">STT</th>
                           <!--<th width="10%">Loại văn bản</th>
                              <th width="10%">Số ký hiệu</th>
                              <th width="9%" class="text-center" >Ngày tháng</th>-->
                           <th width="">Trích yếu</th>
                           <th width="25%" >Nơi nhận</th>
                           <th width="10%">Tệp tin</th>
                           <th width="8%">Tác vụ</th>
                           <th width="7%" class="text-center">Cấp số</th>
                        </tr>
                     </thead>
                     <tbody>
                        {if !empty($dsvanban)}{$i=1}
                        {$dsdonvi = layemaildonvi()}
                        {foreach $dsvanban as $dl}
                        {$qt = layKQChuyenDi($dl.PK_iMaVBDi)}
                        {if $qt[0].iTrangThai == 3}
                        <tr>
                           <td class="text-center">{$i++}</td>
                           <!--<td class="text-center">{$dl.sTenLVB}</td>
                              <td class="text-center">{$dl.sKyHieu}</td>-->
                           <!-- ricknguyen321 -->
                           <td style="background: url(/vpdtsongoaivu/files/images/approved.png) bottom right no-repeat;vertical-align: middle;background-size: 15%;">
                              <p><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin" style="color: black !important;"><b>{$dl.sMoTa}</b></a></p>
                              {if $dl.sTenLVB == "Giấy mời"}
                              <p>- Thời gian: <b style="color: blue">{$dl.sGioMoi}</b> ngày <b style="color: blue">{$dl.sNgayMoi}</b><br>
                                 - Địa điểm: <b style="color: blue">{$dl.sDiaDiemMoi} </b>
                              </p>
                              {else}
                              - Loại: <b>{$dl.sTenLVB}</b> {if !empty($dl.iSoDen)}
                              {$mangsoden = explode(',' , $dl.iSoDen)}
                              ---- Trả lời VB đến số: {foreach $mangsoden as $sd} 
                              {$sd = trim($sd)}
                              {if is_numeric($sd) && $sd > 0}
                              <a href="dsvanbanden?soden={$sd}" target="_blank"><b style="color:red; font-size: 14px">{$sd}</b></a>, 
                              {else}
                              {$sd}, 
                              {/if}
                              {/foreach}
                              {/if}<br>
                              {/if}											
                              {if $dl.FK_iMaDM > 1} {$domat = layDoMat($dl.FK_iMaDM)}  - Độ mật: <b style="color:red; font-size:16px">{$domat[0].sTenDoMat}</b><br>{/if}
                              {if $dl.FK_iMaDK > 1} {$dokhan = layDoKhan($dl.FK_iMaDK)}  - Độ khẩn:<b style="color:red; font-size:16px">{$dokhan[0].sTenDoKhan}</b><br>{/if}
                              <p>- Người tạo: {$cbtao = layTTCB($dl.FK_iMaCB_Nhap)}{$cbtao[0].sHoTen} ({date_time2($dl.sNgayNhap)})</p>
                              <p>- Người ký: <b>{$dl.sHoTen}</b></p>
                              <input class="hidden" type="text" name="ngaythang[{$dl.PK_iMaVBDi}][]" style="width:100px" value="{date('d/m/Y')}">
                           </td>
                           {if $dl.iVBDang == 1}
                           <td style="background: url(/vpdtsongoaivu/files/images/codang.png) bottom right no-repeat;vertical-align: middle;background-size: 30%;">{else} 
                           <td>
                              {/if}
                              <div style="max-height:150px;  overflow:auto">
                                 {$dsemail = explode( ',', $dl.sNoiNhan)}
                                 {foreach $dsemail as $email}
                                 {$email = trim($email)}
                                 {$k = 0}
                                 {foreach $dsdonvi as $donvi}
                                 {if $donvi.sEmail == $email}
                                 {$k = 1}
                                 {$donvi.sTenDV};<br>{break}
                                 {/if}
                                 {/foreach}
                                 {if $k == 0}{$email};<br>{/if}
                                 {/foreach}
                                 <br>
                                 {if !empty($dl.FK_iMaDV_Ngoai)}
                                 {layDV_Ngoai($dl.FK_iMaDV_Ngoai)}
                                 {/if}
                              </div>
                           </td>
                           <td class="text-center">
                              <p><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><B>Tài liệu </B><i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> </p>
                              <!--{if $dl.iFile==1}<a target="_blank" href="{$dl.sDuongDan}" class="tin1">{if layDuoiFile($dl.sDuongDan)=='pdf'}Xem văn bản đi{else}Xem dự thảo{/if}</a>{/if}-->
                           </td>
                           <td class="text-center">
                              {if $vanban['iQuyenHan_DHNB']==9 || $dl.FK_iMaCB_Nhap==$vanban['PK_iMaCB'] || $vanban['iQuyenHan_DHNB']==3 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731 || $vanban['PK_iMaCB'] == 705}
                              <p><a href="{$url}vanban?id={$dl.PK_iMaVBDi}&loai=choso" data-toggle="tooltip" data-placement="top" title="Sửa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a> </p>
                              {if $vanban['iQuyenHan_DHNB']==3 || $vanban['PK_iMaCB'] == 705}
                              <button type="submit" name="xoa" onclick="return confirm('Bạn có muốn xóa văn bản này không?')" value="{$dl.PK_iMaVBDi}" data-toggle="tooltip" data-placement="top" title="Xóa dữ liệu" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                              {/if}
                              {else}
                              <!--<button type="button" id="_vanthuphathanh" onclick="exc_sign_issued('{$ci_session}','{$url}{$dl.sDuongDan}','{$sovanbandi}','{$datetimeios8601}');" value="{$dl.PK_iMaVBDi}">Đóng dấu PH</button>--> 
                              -
                              {/if}
                           </td>
                           <td class="text-center">
                              <!--   {if $vanban['iQuyenHan_DHNB']==9}<button type="submit" name="duyet" value="{$dl.PK_iMaVBDi}" ata-toggle="tooltip" data-placement="top" title="" onclick="return confirm('Bạn có chắc muốn duyệt văn bản này?');" data-original-title="Duyệt văn bản" class="btn btn-default"><i class="fa  fa-check-square-o"></i></button>{else}-
                                 {/if}  -->
                              {if $vanban['iQuyenHan_DHNB']== 9 || $vanban['iQuyenHan_DHNB']== 3 || $vanban['PK_iMaCB'] == 730 || $vanban['PK_iMaCB'] == 731}
                              {if $dl.FK_iMaLVB == 10}
                              {$sovanbandi = $solonnhatGM}
                              {else}
                              {$sovanbandi = $solonnhatCV}
                              {/if}
                              {if layDuoiFile($dl.sDuongDan)=='pdf-123'}
                              <button type="button" id="_vanthuphathanh" onclick="exc_sign_issued('{$ci_session}','{$url}{$dl.sDuongDan}','{$sovanbandi}','{$datetimeios8601}');" value="{$dl.PK_iMaVBDi}">Đóng dấu phát hành</button>
                              {else}
                              <button type="submit" name="duyet" value="{$dl.PK_iMaVBDi}" ata-toggle="tooltip" data-placement="top" title="" onclick="return confirm('Bạn có chắc muốn cấp số cho văn bản này?');" data-original-title="Cấp số văn bản" class="btn btn-primary btn-xs">Cấp số</button>
                              {/if}
                              {else}
                              -
                              {/if}
                           </td>
                        </tr>
                        {/if}
                        {/foreach}
                        {/if}
                     </tbody>
                  </table>
                  <div class="pull-right">{$phantrang}</div>
               </form>
            </div>
         </div>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</div>
<script>
   $(document).ready(function() {
       $(".formSentMsg").delay(5200).fadeOut(300);
       $('td').css('vertical-align', 'middle');
       $('th').css('vertical-align', 'middle');
       // $('td').css('text-align', 'justify');
       $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
       $(document).on('click','a[name=anhien]',function(){
           $('.form-horizontal').toggle();
           $('.form-horizontal').removeClass('hide');
       });
       $('.control-label').css('text-align', 'left');
   });
   
   
   $(document).on('click','#_vanthuphathanh',function(){
           var id = $(this).val();
           $('#'+id).addClass('hide');
       });
   
   function exc_sign_issued(ci_session,file,socv,ngayphathanh) {
       var prms = {};
   
       prms["FileUploadHandler"] = "https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/vanthuphathanh";
       prms["SessionId"] = "ci_sessions="+ci_session+";kiemtra=ok";
       prms["FileName"] = file;
       prms["DocNumber"] = socv;
       prms["IssuedDate"] = ngayphathanh;
   
       var json_prms = JSON.stringify(prms);
       vgca_sign_issued(json_prms, SignFileCallBack1);
   
   }
   function SignFileCallBack1(rv) {
       var received_msg = JSON.parse(rv);
       console.log(received_msg);
       if (received_msg.Status == 0) {
           console.log(received_msg);
           document.getElementById("_signature").value = received_msg.FileName + ":" + received_msg.FileServer + ":" + received_msg.DocumentNumber + ":" + received_msg.DocumentDate;
           document.getElementById("file1").value = received_msg.FileServer;
           document.getElementById("file2").value = received_msg.FileServer;
       } else {
           document.getElementById("_signature").value = received_msg.Message;
       }
   }
      
</script>