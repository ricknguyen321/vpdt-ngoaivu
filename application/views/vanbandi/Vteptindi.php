<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Quản lý file văn bản đi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="panel panel-info">
                                    <div class="panel-heading"><b>Thêm mới / Cập nhật file</b></div>
                                    <div class="panel-body">
									<div class="form-group">
										<div style="margin-left:15px; padding-right:10px; ">
                                           <p><b style="color:red;"></b>
										   (Dung lượng tối đa: 20MB; có thể chọn nhiều tập tin; định dạng được chấp nhận: pdf, doc, docx, xls, xlsx, jpg, jpeg, png)
										   </p>
                                        </div>
										
										
										<!--<label for="" class="col-sm-3 control-label" style="width:33%">Tên file / ghi chú</label>
                                        <div class="col-sm-9"  style="width:66%">
                                                <input type="text" name="tenteptin" value="{($thongtin)?$thongtin[0]['sTenFile']:''}" class="form-control" id="">
                                        </div>-->
									</div>
                                        <div class="form-group noidung">

											 <label for="" class="col-sm-3 control-label"  style="width:33%">Chọn file *</label>
                                            <div class="col-sm-9"  style="width:66%">
                                                <input type="file" name="files[]" placeholder="Click vào đây để chọn files" class="form-control" id="" multiple="">
                                            </div>
                                        </div>

										
										<div class="themsau"></div>
                                    <button type="button" name="themmoi" value="themmoi" class="btn btn-success btn-xs" style="background: green;color: white;padding: 4px 10px;"><i class="fa fa-plus"></i></button><b class="text-danger"> Tải thêm</b>

                                        <div class="form-group">
                                            <label for=""class="col-sm-3"></label>
                                            <div class="col-sm-9" style="text-align:center">
                                                <a href="{$url}teptindi?id={$mavanban}" class="btn btn-default">Cancel</a> <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <form action="" method="post">
                        <div class="col-md-12" style="text-align: -webkit-center;">
                            <div class="form-group">
                                <label for="">Tệp tin đính kèm:</label>
                                <table class="table table-bordered table-striped " style="width:75%">
                                    <thead>
                                        <tr style="background: #ccc;">
                                            <th class="text-center" style="width:50px">STT</th>
                                            <th class="text-center" style="">Tên file / ghi chú </th>
                                            <th class="text-center" width="20%">Ngày tháng</th>
                                            <th class="text-center" width="8%">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if !empty($dsfile)}{$i=1}
                                        {foreach $dsfile as $f}
                                            <tr>
                                                <td class="text-center">{$i++}</td>
                                                <td ><a style="color:brown" href="{$f.sDuongDan}" target="_blank">{$f.sTenFile}</a></td>												
                                                <td class="text-center">{date_time($f.sThoiGian)}</td>
												
                                                <td class="text-center">
													{if $f.FK_iMaCB == $vanban['PK_iMaCB']}
														{if substr($f.sDuongDan, 28, 6) != '_vbdi_'}
															<button type="submit" class="btn btn-default" onclick="return confirm('Bạn có chắc muốn xóa tệp tin này?');" name="xoa" value="{$f.sDuongDan}"><i class="fa fa-trash"></i></button>
															<!--<a class="tin btn btn-default" href="{$url}teptindi?id={$mavanban}&mafile={$f.PK_iMaFileDi}"><i class="fa fa-edit"></i></a>-->
														{else}											
															-													
														{/if}	
														
													{else}											
														-													
													{/if}																								
                                                </td>
                                            </tr>
                                        {/foreach}
                                    {/if}
                                    </tbody>
                                </table><br>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
		
		$(document).on('click','button[name=themmoi]',function(){
            var noidung1 = $('.noidung')[0].outerHTML;
            var noidung = noidung1;
            $('.themsau').before(noidung);
        });
    });
</script>