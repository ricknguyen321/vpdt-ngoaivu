<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="refresh" content="30"> 30 giây refesh trang 1 lần-->
  <link href="" rel="shortcut icon" type="image/x-icon" />
  <title>Lịch họp do lãnh đạo Sở chủ trì</title>
  <base href="">
  <meta name="author" content="Nguyễn Xuân Hải">
  <meta name="copyright" content="Sở Ngoại Vụ">
  <meta name="description" content="LỊCH HỌP DO LÃNH ĐẠO SỞ CHỦ TRÌ ">
  <meta name="robots" content="noindex,nofollow">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{$url}assets/js/plugins/font-awesome/css/font-awesome.min.css">
  <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
  <style>
	.container {
	    font-family:"Times New Roman", Times, serif;
        font-size:13pt;
    }
	.indam{
		font-weight: bold;
	}
  </style>
</head>

<body><br/>
    <div class="container">
    	<div class="row">
            <div class="col-md-12 col-md-offset-0">
                <a href="{$url}lichcongtac" class="btn btn-success pull-right">  Xem lịch công tác của lãnh đạo</a>
                <table class="table table-bordered">
                    <tr>
                        <th colspan="3" class="text-center" style="font-size: 15pt;">LỊCH HỌP DO SỞ CHỦ TRÌ <br>Tuần từ: {$ngaybd} - {$ngaykt}</th>
                    </tr>
                    <tr style="background:#ccc;">
                        <th width="10%">THỨ/NGÀY</th>
                        <th width="45%" class="text-center">SÁNG</th>
                        <th width="45%" class="text-center">CHIỀU</th>
                    </tr>
                    {if !empty($ngaytrongtuan)}
                        {foreach $ngaytrongtuan as $n}
                            <tr>
                                <th class="text-center">{$n.0} {$n.1}</th>
                                <td>
                                    {if !empty($lichsang)}{$sang=0}
                                        {foreach $lichsang as $ls}
                                            {if date_select($ls.sNgayMoi) == $n.1}
                                            <!-- {$sang++} -->
                                            {if $sang>1}
                                                <hr>
                                            {/if}
                                            <p class="indam">{if $ls.PK_iMaVBDi == 32092 or $ls.PK_iMaVBDi == 32093}- UBND Thành phố chủ trì {else}+  Đ/C {$ls.sHoTen} chủ trì:{/if}</p>
                                            <p>+ {$ls.sMoTa}</p>
                                             {if $ls.PK_iMaVBDi == 32092} (Đ/c Mai Xuân Vinh Phó Giám đốc Sở ký){/if}</p>
                                            <p>{if $ls.PK_iMaVBDi == 32092}
                                            <b>Thành phần:<br></b>
                                            - Đ/c Lãnh đạo UBND Thành phố <br>
                                            - Đ/c Lãnh đạo sở và lãnh đạo phòng tổ chức biên chế, Sở Nội vụ <br>

                                            {/if}</p>
                                       

                                            {if !empty({$ls.sDuongDan})}<p><a target="_blank" href="{$url}{$ls.sDuongDan}">[Xem file]</a></p>{/if}
                                            <p>+ {$ls.sGioMoi}, ngày {date_select($ls.sNgayMoi)} tại {$ls.sDiaDiemMoi}</p>
                                            <p>
                                            {if $ls.PK_iMaVBDi == 32092 or $ls.PK_iMaVBDi == 32093}- Sở Ngoại Vụ {else}+ {$ls.sTenPB}{/if}
                                            {/if}
                                            </p>
                                        {/foreach}
                                    {/if}
                                </td>
                                <td>
                                    {if !empty($lichchieu)}{$chieu=0}
                                        {foreach $lichchieu as $lc}
                                            {if date_select($lc.sNgayMoi) == $n.1}
                                            <!-- {$chieu++} -->
                                            {if $chieu>1}
                                            <hr>
                                            {/if}
                                            <p class="indam">- Đ/c {$lc.sHoTen} chủ trì:</p>
                                            <p><b>+ Nội dung cuộc họp: </b>{$lc.sMoTa}</p>
                                            
											{if !empty({$lc.sDuongDan})}<p style="width:100%"><a target="_blank" href="{$url}{$lc.sDuongDan}"><i style="float:right">Đính kèm</i></a></p>{/if}<br>
											
                                            <p>+ <b style="color:blue">{$lc.sGioMoi}</b> ngày <b style="color:blue">{date_select($lc.sNgayMoi)}</b> tại <b style="color:blue">{$lc.sDiaDiemMoi}</b></p>
                                            <p>+ Phòng nhập lịch: {$lc.sTenPB}</p>
                                            {/if}
                                        {/foreach}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                    {/if}
                    </tbody>    
                </table>
            </div>
    	</div>
    </div>
    <style>
        .open-small-chat1 {
            height: 38px;
            width: 38px;
            display: block;
            background: #3c8dbc;
            padding: 9px 8px;
            text-align: center;
            color: #fff;
            border-radius: 50%;
        }
    </style>
    <!--<div id="small-chat" style="    position: fixed;bottom: 20px;right: 70px;z-index: 100;">
        <a href="{$url}welcome.html" class="open-small-chat1" style="cursor:pointer;" data-toggle="tooltip" data-placement="top" title="Về Trang Chủ">
            <i class="fa fa-home" style="font-size: 20px;"></i>
        </a>
    </div>-->
    <style>
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #0c0b0b;
        }
        hr{
            border-top: 1px solid #9E9E9E;
            margin: 10px 0px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('td').css('vertical-align', 'middle');
            $('th').css('vertical-align', 'middle');
        });
    </script>
</body>
</html>
