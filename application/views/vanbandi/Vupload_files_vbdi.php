<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="refresh" content="30"> 30 giây refesh trang 1 lần-->
  <link href="" rel="shortcut icon" type="image/x-icon" />
  <title>{$title}</title>
  <base href="">
  <meta name="author" content="">
  <meta name="copyright" content="Sở Ngoại Vụ">
  <meta name="description" content="">
  <meta name="robots" content="noindex,nofollow">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{$url}assets/js/plugins/bootstrap/css/bootstrap.min.css">
  <script src="{$url}assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
  <style>
    .container {
        font-family:"Times New Roman", Times, serif;
        font-size:13pt;
    }
    .indam{
        font-weight: bold;
    }
  </style>
</head>

<body><br/>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top:-30px">
                {if !empty($content)}
                    <h2 class="{if $content['class']=='info'}text-primary{else}text-danger{/if}">{$content['content']} {if $content['class']=='info'} &nbsp;&nbsp;&nbsp; <button class="btn btn-primary" onclick="closeWin()" style="text-align:center">Đóng lại</button>{/if}</h2> 
                {/if}
				<h3 class="indam" style="">Chú ý trước khi phát hành văn bản số: {$isodi}</h3>
                <h4>1. File phải được scan dưới định dạng pdf.</h4>
				<h4>2. File phải được ký số trước khi gửi.</h4>
                <h4>3. Đổi tên file thành: <b style="color:blue">{$isodi}.pdf</b></h4>
                <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="form-group">
                        <input type="file" name="files[]" class="form-control" readonly="" style="width: 50%; float: left;">
                    </div>
                    <div class="form-group" style="text-align:center">
                        <button type="submit" name="upload" value="upload" class="btn btn-primary"><b>Gửi email</b></button>
                    </div>
                </form>
				<h5><i>Lưu ý: Hệ thống cần thời gian upload file lên server, quá trình này có thể kéo dài 20s - 30s tùy thuộc dung lượng file, vui lòng không tắt cửa sổ này và đợi đến khi có thông báo.</i></h5>
            </div>
        </div>
    </div>
    <script>
        function closeWin() {
            window.close();
        }
    </script>
</body>
</html>
