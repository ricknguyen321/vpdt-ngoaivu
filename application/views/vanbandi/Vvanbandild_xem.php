<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Danh sách văn bản để xem
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"  style="overflow-x:auto;padding:0px;">
                    <form action="" method="post">
                        <table id="" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:50px" class="text-center">STT</th>
                                    <th width="10%" class="text-center">Ký hiệu</th>
                                    <th width="30%">Trích yếu</th>
                                    <th width="">Ý kiến người gửi</th>
                                    <th width="6%">Tệp tin</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($dsvanban)}{$i=1}
                                {foreach $dsvanban as $dl}
                                    <tr>
                                        <td class="text-center">{$i++}</td>
                                        <td class="text-center">{$dl.sKyHieu}</td>
                                        <td ><p><b><a href="{$url}thongtinvanban?id={$dl.PK_iMaVBDi}" class="tin">{$dl.sMoTa}</a> </b></p>
											- Người nhập: {$dscanbo[$dl.FK_iMaCB_Nhap]}</td>
                                        <td >
                                            <p><b>{$dl.sYKien}</b></p>
                                            - {$dscanbo[$dl.FK_iMaCB_Gui]} <br>
                                            - {date_time($dl.sThoiGian)}
                                        </td>
                                        <td class="text-center">

											<p><a class="tin1" href="{$url}teptindi?id={$dl.PK_iMaVBDi}"><b>Tài liệu</b> <i class="fa fa-folder-open-o" style="color: gold;font-size: 15px;"></i></a> </p>
										
											<p>{if !empty($dl.sDuongDan)} <a target="_blank" href="{$dl.sDuongDan}" id="{$dl.PK_iMaVBDi}" class="tin1 layma">Xem file</a>{/if}</p>
										
											<span class="label label-{($dl.iSoVBDi>0)?'success':'warning'}" style="font-size: 14px; padding: 5px;">{($dl.iSoVBDi>0)?'Đã cấp số':'Chưa cấp số'}</span>
											
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </form>
                    <div class="pull-right">{$phantrang}</div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        var url = window.location.href;
        $('td').css('vertical-align', 'middle');
        $('th').css('vertical-align', 'middle');
        // $('td').css('text-align', 'justify');
        $('.form-horizontal').css('background', 'rgba(60, 141, 188, 0.07)');
        $('.control-label').css('text-align', 'left');
    });
</script>