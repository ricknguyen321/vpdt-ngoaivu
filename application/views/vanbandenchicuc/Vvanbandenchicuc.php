<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Nhập mới văn bản đến
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Khu vực:</label>

                                    <div class="col-sm-8">
                                        <select name="khuvuc" id="" class="form-control select2" style="width:100%">
                                            <option value="">-- Chọn khu vực --</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số ký hiệu <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="sokyhieu" value="" placeholder="Nhập số ký hiệu" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Loại văn bản <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <select name="loaivanban" id="" class="form-control select2" style="width:100%" required>
                                            <option value="">-- Chọn loại văn bản --</option>
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nơi gửi đến <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <select name="donvi" id="" class="form-control select2" style="width:100%" required>
                                            <option value="">-- Chọn nơi gửi đến --</option>
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control datepic datemask" id="" name="ngayky" placeholder="Ngày ký" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Lĩnh vực:</label>

                                    <div class="col-sm-8">
                                        <select name="linhvuc" id="" class="form-control select2" style="width:100%" >
                                            <option value="">-- Chọn lĩnh vực --</option>
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class=" col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Trích yếu <span class="text-danger">*</span></label>
                                        <textarea name="trichyeu" id="" class="form-control" rows="4" required placeholder="Nhập trích yếu"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Người ký <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <select name="nguoiky" id="" class="form-control select2" style="width:100%" >
                                            <option value="">-- Chọn người ký --</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Ngày nhận:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="" name="ngaynhan" class="form-control datepic datemask" placeholder="Nhập ngày nhận">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số đến:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="" name="soden" class="form-control" placeholder="Nhập số đến" style="font-size: 16px; color: red; font-weight: 600">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Chức vụ <span class="text-danger">*</span>:</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="chucvu" value="" id="" placeholder="Nhập chức vụ">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Hạn giải quyết:</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="hangiaiquyet" value="" class="form-control datepic datemask" placeholder="Nhập hạn giải quyết">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Số trang:</label>

                                    <div class="col-sm-8">
                                        <input type="text" value="" name="sotrang" class="form-control col-sm-4" placeholder="nhập số trang">
                                    </div>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label for="" class="col-sm-4 control-label">Thời hạn theo QT ISO:</label>-->
<!---->
<!--                                    <div class="col-sm-8">-->
<!--                                        <select name="" id="" class="form-control select2">-->
<!--                                            <option value="">-- Lựa chọn quy trình --</option>-->
<!--                                        </select>-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                            {if !empty($idDocGo)}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Trình tự giải quyết:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">STT</th>
                                                <th class="text-center">Ngày nhận</th>
                                                <th class="text-center">Chuyển từ</th>
                                                <th width="40%">Nội dung</th>
                                                <th class="text-center">Chuyển đến</th>
                                                <th class="text-center">Hạn xử lý</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {$i=1}
                                        {foreach $process as $pr}
                                            <tr class="success">
                                                <td class="text-center">{$i++}</td>
                                                <td class="text-center">{$pr.sThoiGian}</td>
                                                <td class="text-center">
                                                    {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                    {if $cb.PK_iMaCB == $pr.PK_iMaCBChuyen}
                                                    {$cb.sHoTen}
                                                    {/if}
                                                    {/foreach}
                                                </td>
                                                <td>{$pr.sMoTa}</td>
                                                <td class="text-center">
                                                    {foreach layDuLieu(NULL,NULL,'tbl_canbo') as $cb}
                                                    {if $cb.PK_iMaCB == $pr.PK_iMaCBNhan}
                                                    {$cb.sHoTen}
                                                    {/if}
                                                    {/foreach}
                                                    {foreach layDuLieu(NULL,NULL,'tbl_phongban') as $pb}
                                                    {if $pb.PK_iMaPB == $pr.PK_iMaPhongCT}
                                                    {$pb.sTenPB}
                                                    {/if}
                                                    {/foreach}
                                                </td>
                                                <td class="text-center">{date_select($pr.sThoiGianHetHan)}<br><i>(Hạn VB: {date_select($pr.sThoiGianHetHan)})</i></td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Tệp tin đính kèm:</label>
                                </div>
                                <div class="form-group">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên tệp tin</th>
                                            <th>Tải về</th>
                                            <th>Ngày nhập</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            {/if}
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="luulai" value="Lưu lại">Lưu lại</button> <label for="">Người nhập văn bản này là: {!empty($getDocGo) &&($getDocGo) ? $getDocGo[0]['FK_iMaNguoiNhap'] : $vanban['sHoTen'] }</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
        $('.control-label').css('text-align', 'left');
    });
</script>