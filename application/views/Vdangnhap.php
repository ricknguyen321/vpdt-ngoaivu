<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>VĂN PHÒNG ĐIỆN TỬ - SỞ NGOẠI VỤ HÀ NỘI</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="assets/js/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script src="{$url}assets/js/plugins/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../vpdtsongoaivu/assets/css/snow.css">
	{if date('m') == 12}
		<style type="text/css">
        body {
            font-family:arial,tahoma,verdana,helvetica,sans-serif;
            font-size:11px;
            text-align:center;
            background:#e1e1e1 url(assets/img/bg-noel.jpg) repeat-x;
			background-size: cover;
            line-height:22px;
            color:#000000;
            font-weight:bold;
            margin-left: 0px;
            margin-top: 77px;
            margin-right: 0px;
            margin-bottom: 0px;
			
        }
		#logo{
			background: url(assets/img/login/Group2-noel.png) no-repeat left;
			background-size: 125px 125px;
            width:100%;
            height:165px;
			margin-left: 58px;
        }
		</style>
	{else}
		<style type="text/css">
        body {
            font-family:arial,tahoma,verdana,helvetica,sans-serif;
            font-size:11px;
            text-align:center;
            background:#e1e1e1 url(assets/img/bg.png) repeat-x;
            line-height:22px;
            color:#000000;
            font-weight:bold;
            margin-left: 0px;
            margin-top: 77px;
            margin-right: 0px;
            margin-bottom: 0px;
			
        }
		#logo{
			background: url(assets/img/login/Group2.png) no-repeat left;
			background-size: 125px 125px;
            width:100%;
            height:165px;
			margin-left: 58px;
        }
		</style>
	{/if}
    <style type="text/css">
        html{
            margin: 0 auto;
        }
        
        #clear{
            clear: both;
            height:5px;
        }
        #table_main{
            margin: 0 auto;
            width:270px;
            height:297px;
            text-align:left;
			background: white;
			border-radius: 20px;
			border: 1px dashed #bad4eb;
			box-shadow: 0px 0px 11px 2px rgba(0, 0, 0, 0.48);
			float: right;
        }
        #left{
            width:15px;
            height:297px;
            float:left;
        }
        #right{
            width:15px;
            height:297px;
            float:right;
        }
        #center{
            height:298px;
            margin-left:15px;
            vertical-align:top;

        }
        #img_left{
			
			
		}
        #content{
            width:100%;
        }
        
        #text{
            width:100%;
            margin-top: -10px;
        }
        .fro{
            width:180px;
        }
        /*.msgerr
         {
            background: url(images/error-icon-16.gif) no-repeat left top;
             padding-left: 25px;
             margin-left: 5px;
             list-style: none;
             vertical-align: middle;
             text-align: left;
             color: #df2267;
             height:16px;
         }*/

        .errmsg
        {
            color:#ff0000;
        }
    </style>
    <style>
        .loader,
        .loader:after {
            /*border-radius: 50%;*/
            width: 18em;
            height: 0em;
            margin: auto;
            padding: 10px;
        }
        .loader {
            /*margin: 785px;*/
            /*font-size: 5px;*/
            /*position: fixed;*/
            /*text-indent: -9999em;*/
            background-image: url("assets/img/ajax-loader.gif");
        }
        #loadingDiv {
            position:fixed;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background:#ffffff;
        }
		
    </style>
    <script>
        $(document).ready(function () {
            $(document).on('click','#dangnhap',function () {
//                    alert(1);
                $('body').append('<div style="" id="loadingDiv"><div class="loader" style="margin-top: 200px;">Đang tải dữ liệu</div></div>');
            });
        });
    </script>
</head>
<body>
<table width="1000"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" height="91%" border="0" cellpadding="0" cellspacing="0">
                <tr>
				<td style="color:white; text-align: right; width: 71%; padding-top: 110px;">
					<h1 style="font-size:25px;background: rgba(0, 0, 0, 0.14);padding: 15px;">
					<b style="text-shadow: 1px 1px 2px black;">VĂN PHÒNG ĐIỆN TỬ - SỞ NGOẠI VỤ HÀ NỘI </b><br>
					</h1>
					<h3>
					Số 10 Lê Lai, Quận Hoàn Kiếm, Hà Nội<br>
					<a href="https://dfa.hanoi.gov.vn/" target="_blank" style="color:white">https://dfa.hanoi.gov.vn/</a><br>
					</h3>
					<h5>
					Trình duyệt được khuyên dùng: <a href="https://www.google.com/chrome/" target="_blank" style="color:white">Google Chrome</a><br>
					<!--Hỗ trợ liên quan đến VPĐT: Bình - 0989945321-->
					</h5>
				</td>	
		
		
                    <td>
						<div id="table_main">
                            <div id="left"> </div>
                            <div id="right"> </div>
                            <div id="center">
                                <div id="content">
                                    <div id="logo"> </div>
                                    <div id="text">
                                        <form action="" method="post" autocomplete="off">
                                            <input type="hidden" name="login" value="yes">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    
                                                    <td colspan="3" style="text-align: center;" height="35">
                                                        <input name="taikhoan" type="text" id="user_name"  value="" size="25" style="font-family:Verdana; font-size:12px; border:1px solid #CCCCCC; text-align:center" autofocus="autofocus" placeholder="username">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td colspan="3" style="text-align: center;">
                                                        <input name="matkhau" type="password" id="user_pass" size="25" style="font-family:Verdana; font-size:12px; border:1px solid #CCCCCC; text-align:center" placeholder="password">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
												<p style="text-align: center; margin-top: -5px; margin-bottom: 0px;">Vui lòng nhập username và password</p>
                                                    {if $ketqua=='failed'}<p style="color:red; text-align: center; margin-top: -5px; margin-bottom: 0px;">Username hoặc password không đúng.</p>{/if}
													
                                                </tr>
                                                <tr>
                                                    <td colspan="5" style="text-align: center;" height="35">
                                                        <input name="dangnhap" type="submit" id="dangnhap" value="Enter" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                                <div id="img_left"> </div>
                            </div>
						</div>
					</td>
                </tr>
            </table>
        </td>
		
    </tr>
</table>
{if date('m') == 12}

	<div class="snowflakes" aria-hidden="true" style="color:white">
	  <div class="snowflake">•</div>	  <div class="snowflake">●</div> 	  <div class="snowflake">❅</div>	  <div class="snowflake">❆</div>
	  <div class="snowflake">•</div>	  <div class="snowflake">●</div>	  <div class="snowflake">❅</div>	  <div class="snowflake">❆</div>
	  <div class="snowflake">•</div>	  <div class="snowflake">●</div>	  <div class="snowflake">❅</div>	  <div class="snowflake">❆</div>	  
	</div>
	{if $is_mobile == 0}
		<script src='../vpdtsongoaivu/etc/xmasb.js'></script>
		<!--<audio src='../vpdtsongoaivu/etc/xmas.mp3' autoplay ></audio>	-->
	{/if}
{/if}

</body>
