    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 20%;vertical-align: top; font-size: 14px">
                                    Sở Ngoại Vụ HÀ NỘI<BR>
                                    <span style="text-transform: uppercase;">
                                        <b>{$phongban[$phong_id]}</b>
                                    </span>
                                </td>
                                <td colspan="13" style="text-align: center;width: 80%">
                                    <B>TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ, CHẤM ĐIỂM MỨC ĐỘ HOÀN THÀNH NHIỆM VỤ HÀNG THÁNG</B><BR>
                                    <b><span style="padding: 30px 0 30px 0">Tháng:{$month}</span></b>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">STT</th>
                                    <th class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Người thực hiện</th>
                                    <th width="6%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Tổng số việc theo kế hoạch</th>
                                    <th width="30%" class="text-center" colspan="6" style="border-top: 1px solid #000; border-left: 1px solid #000;">Công việc đã hoàn thành trong tháng</th>
                                    <th width="18%" class="text-center" colspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Công việc còn tồn cuối tháng</th>
                                    <th width="7%" class="text-center" rowspan="3"  style="border-top: 1px solid #000; border-left: 1px solid #000;">Tỉ lệ hoàn thành (%)</th>
                                    <th width="6%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Điểm trung bình của tháng</th>
                                    <th width="9%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;">Kết quả phân loại mức độ hoàn thành nhiệm vụ</th>
                                </tr>
                                <tr>
                                    <th width="6%" class="text-center" rowspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Số lượng</th>
                                    <th width="12%" class="text-center" colspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Thời gian</th>
                                    <th width="12%" class="text-center" colspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;" >Chất lượng</th>
                                    <th width="6%" class="text-center" rowspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="6%" class="text-center" rowspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Số lượng</th>
                                    <th width="6%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;" rowspan="2">Trong hạn</th>
                                    <th width="6%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;" rowspan="2">Chậm tiến độ</th>
                                    
                                </tr>
                                <tr>
                                    <th width="6%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Trong hạn</th>
                                    <th width="6%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Quá hạn</th>
                                    <th width="6%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Đảm bảo</th>
                                    <th width="6%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>I</b></td>
                                    <td colspan="14" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000; text-align: left;padding-left: 5px"><b>Lãnh đạo phòng, đơn vị</b></td>
                                </tr>

                                {$jj=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key <= 10}
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$jj++}</td>
                                    <td style="border-top: 1px solid #000;border-left: 1px solid #000; text-align: left;padding-left: 5px">{$giatri[11]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[0]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[1]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[2]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;">{$giatri[3]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[4]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[5]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[6]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[7]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[8]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;">{$giatri[9]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: right;padding-right: 15px">{$giatri[10]} %</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$giatri[13]['diem']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;border-right: 1px solid #000;">
                                        {if $giatri[14]['danhgia_tp'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==5}<span style="color:#00FF00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==4}<span style="color:#00CC66">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                        
                                    </td>
                                    

                                </tr>
                                {/if}
                                {/foreach}

                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>II</b></td>
                                    <td colspan="14" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000; text-align: left;padding-left: 5px"><b>Công chức, viên chức, LĐHĐ</b></td>
                                </tr>
                                {$ii=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key > 10}
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$ii++}</td>
                                    <td style="border-top: 1px solid #000;border-left: 1px solid #000; text-align: left;padding-left: 5px">{$giatri[11]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[0]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[1]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[2]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;">{$giatri[3]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[4]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[5]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[6]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[7]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$giatri[8]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;">{$giatri[9]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: right;padding-right: 15px">{$giatri[10]} %</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$giatri[13]['diem']}
                                    </td>  
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;border-right: 1px solid #000;">
                                        {if $giatri[14]['danhgia_tp'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==5}<span style="color:#00FF00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==4}<span style="color:#00CC66">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgia_tp'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                    </td>                              
                                </tr>
                                {/if}
                                {/foreach}
                                
                                <tr>
                                    <td colspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>Tổng cộng</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[0]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[1]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[2]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;"><b>{$count_arr1[3]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[4]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[5]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[6]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[7]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{$count_arr1[8]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;"><b>{$count_arr1[9]}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: right;padding-right: 15px"><b>{$count_arr1[10]} %</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>
                                         {if isset($diem_tb)} {$diem_tb} {/if}
                                    </b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;border-right: 1px solid #000;"><b>
                                         
                                    </b></td>
                                </tr>

                                <tr><td colspan="15" style="border-top: 1px solid #000; "></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 15px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="3" style="text-align: center;vertical-align: top;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b> Giám đốc sở</b><br><br><br><br>
                                    
                                </td>
                                <td colspan="6" style="text-align: center;vertical-align: top;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b>Phó giám đốc sở</b><br><br><br><br>
                                    
                                </td>
                                <td colspan="6" style="text-align: center;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b>Trưởng phòng, đơn vị</b><br>
                                    <i>(ký, ghi rõ họ tên)</i><br><br><br><br>

                         
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>                
            </div>
        </div>
      <!-- /.box -->

    </section>
 


