<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-9">
                    <h3 class="font">
                        Thống kê báo cáo đánh giá kết quả thực hiện nhiệm vụ công tác tháng
                    </h3>
                </div>
                <div class="col-sm-3">
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{$url}thongkebaocaothangIn" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                                <div class="form-group">
                                                <div class="col-md-4" >
                                                    <label for="" class="col-sm-4 control-label">Chọn tháng: </label>
                                                    <div class="col-sm-8">
                                                        <select id="thangbd1" class="form-control ngayhan select2" style="width:100%" onchange="getvalue(this.value)">
                                                            <option value="0" {if $thangbd == 0}selected ="selected"{/if}>-- Chọn tháng bắt đầu --</option>
                                                            {for $i=1;$i<=12;$i++}
                                                                <option value="{$i}"  {if $thangbd == $i}selected ="selected"{/if}> Tháng {$i}</option>
                                                            {/for}
                                                        </select>
                                                    </div>
                                                </div>
                                            
                                            <div class="col-md-2" >
                                                <button class="btn btn-primary  pull-right" name="thongkeword" id="thongkeword" value="">Kết xuất word</button>
                                            </div>
                                            <div class="col-md-2" >
                                                <button class="btn btn-primary  pull-right" name="thongkeexcel" id="thongkeexcel" value="" >Kết xuất excel</button>
                                            </div>
                                            <div class="col-md-4" >
                    <span style="color: red">(Người dùng chọn tháng sau đó click vào nút "Kết xuất" để lưu file word)</span>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

<script>
    $(document).ready(function() {
        $(document).on('click','button[name=thongkeword]',function(){
            var thang = $("#thangbd1").val();
            var url      = window.location.href + "?ketxuatword&thangbd="+thang;
            window.open(url);
        });
        $(document).on('click','button[name=thongkeexcel]',function(){
            var thang = $("#thangbd1").val();
            var url      = window.location.href + "?thangbd="+thang;
            window.open(url);
        });
    });
    
</script>
