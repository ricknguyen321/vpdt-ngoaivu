    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 20%;vertical-align: top; font-size: 14px">
                                    Sở Ngoại Vụ HÀ NỘI<BR>
                                    <span style="text-transform: uppercase;">{$phongban[$list_diem[0]['phong_id']]}</span>
                                </td>
                                <td colspan="13" style="text-align: center;width: 80%">
                                    <B>BIỂU ĐÁNH GIÁ, CHẤM ĐIỂM MỨC ĐỘ HOÀN THÀNH NHIỆM VỤ HÀNG TUẦN CỦA CÁ NHÂN</B><BR>
                                    <b><span style="padding: 30px 0 30px 0">Tuần:{$tuan} ({$arr})</span></b><br>
                                    Họ và tên: {$canbophong[$list_diem[0]['canbo_id']]}; Chức vụ: 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">STT</th>
                                    <th width="30%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Nội dung</th>
                                    <th width="5%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Tổng số việc được giao</th>
                                    <th width="25%" class="text-center" colspan="6" style="border-top: 1px solid #000; border-left: 1px solid #000;">Công việc đã hoàn thành trong tuần</th>
                                    <th width="10%" class="text-center" colspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Công việc còn tồn cuối tuần</th>
                                    <th width="5%" class="text-center" rowspan="3"  style="border-top: 1px solid #000; border-left: 1px solid #000;">Tỉ lệ hoàn thành (%)</th>
                                    <th width="5%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000;">Cá nhân tự chấm điểm</th>
                                    <th width="5%" class="text-center" rowspan="3" style="border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;">Trưởng đơn vị chấm điểm</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;" >Chất lượng</th>
                                    <th width="5%" class="text-center" rowspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2" style="border-top: 1px solid #000; border-left: 1px solid #000;">Thời gian</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Trong hạn</th>
                                    <th width="5%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Quá hạn</th>
                                    <th width="5%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Đảm bảo</th>
                                    <th width="5%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Chưa đảm bảo</th>
                                    <th width="5%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;" >Trong hạn</th>
                                    <th width="5%" class="text-center" style="border-top: 1px solid #000; border-left: 1px solid #000;">Quá hạn</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>A</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;padding-left: 3px;text-align: left;"><b>KẾT QUẢ THỰC HIỆN NHIỆM VỤ THEO KẾ HOẠCH CÔNG TÁC TUẦN ĐƯỢC DUYỆT <br>(Tối đa 65 điểm)</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[0]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[1]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[2]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;">{$count_arr[3]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[4]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[5]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[6]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[7]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[8]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;color: red;text-align: center;">{$count_arr[9]}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$count_arr[10]}%</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                    {if $quyen >= 7}
                                        {(!empty($list_diem) && $list_diem[0]['diem_1']>0)?$list_diem[0]['diem_1']:''}
                                    {/if}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; text-align: center;">                                   
                                    {if $list_diem[0].active ==2}
                                        {$list_diem[0]['diem_1_tp']}
                                    {else}
                                        -
                                    {/if}
                                    </td>

                                </tr>
                                <tr>
                                    <th style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">I</th>
                                    <th colspan="14" style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;border-right: 1px solid #000;padding-left: 3px;">NHIỆM VỤ LÃNH ĐẠO GIAO, NHIỆM VỤ THƯỜNG XUYÊN, NHIỆM VỤ KHÁC</th>
                                </tr> 
                                {$i=1}
                                {foreach $list_data1 as $key => $value1}
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$i++}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">{$value1.vanban_skh} &nbsp;{$value1.kh_noidung}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">1</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value1.active ==3}1{else}0{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value1.active == 3 && (date_insert($value1.ngay_hoanthanh) <= $value1.ngay_han || $value1.ngay_han <'2017-01-01' )}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: red">{if $value1.active == 3  && date_insert($value1.ngay_hoanthanh) > $value1.ngay_han && $value1.ngay_han >'2017-01-01'}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value1.active == 3 && $value1.chatluong == 1}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value1.active == 3 && $value1.chatluong == 2}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value1.active == 3 && $value1.sangtao == 2}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value1.active < 3}1{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if ($value1.active == 2  && date_insert($value1.ngay_hoanthanh) <= $value1.ngay_han) || ($value1.active == 1 &&$value1.ngay_han <'2017-01-01' || date('Y-m-d') <= $value1.ngay_han)}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: red">{if ($value1.active == 2  && date_insert($value1.ngay_hoanthanh) > $value1.ngay_han && $value.ngay_han > '2017-01-01') || ($value1.active == 1  && date('Y-m-d') > $value1.ngay_han && $value1.ngay_han > '2017-01-01')}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue;border-right: 1px solid #000"></td>
                                </tr> 
                                {/foreach}
                                <tr>
                                    <th style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">II</th>
                                    <th colspan="14" style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;border-right: 1px solid #000;padding-left: 3px;">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ</th>
                                </tr>
                                {foreach $list_data as $key => $value}
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">{$i++}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px">{$value.vanban_skh} &nbsp;{$value.kh_noidung}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">1</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value.active ==3}1{else}0{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value.active == 3  && ($value.ngay_hoanthanh <= $value.ngay_han || $value.ngay_han <'2017-01-01' )}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: red">{if $value.active == 3  && $value.ngay_hoanthanh > $value.ngay_han && $value.ngay_han >'2017-01-01'}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value.active == 3  && $value.chatluong == 1}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value.active == 3  && $value.chatluong == 2}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value.active == 3  && $value.sangtao == 2}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if $value.active < 3}1{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue">{if ($value.active == 2  && $value.ngay_hoanthanh <= $value.ngay_han) || ($value.active == 1 &&$value.ngay_han <'2017-01-01' || date('Y-m-d') <= $value.ngay_han)}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: red">{if ($value.active == 2  && $value.ngay_hoanthanh > $value.ngay_han && $value.ngay_han > '2017-01-01') || ($value.active == 1  && date('Y-m-d') > $value.ngay_han && $value.ngay_han > '2017-01-01')}X{/if}</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;color: blue"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;border-right: 1px solid #000;color: blue"></td>
                                </tr> 
                                {/foreach}

                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>B</b></td>
                                    <td  colspan="12" style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 3px;"><b>Ý thức tổ chức kỷ luật (tối đa 20 điểm)</b></td>
                                    
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>{($list_diem)?$list_diem[0]['diem_2']:''}</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;"><b>{($list_diem)?$list_diem[0]['diem_2_tp']:''}</b></td>
                                </tr> 
                                <tr>
                                    <td  style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">1</td>
                                    <td  style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Chấp hành tốt đường lối, chủ trương của Đảng; chính sách,pháp luật của nhà nước <br><b>- Điểm tối đa: 4</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                    {$list_diem[0]['diem_3']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2}
                                        {$list_diem[0]['diem_3_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">2</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Chấp hành nghiêm nội quy, quy chế làm việc của cơ quan, đơn vị, quy tắc ứng xử của cán bộ, công chức, viên chức, người lao động <i>(ngoài các tiêu chí nêu ở các mục phần này)</i> <br><b>- Điểm tối đa: 4</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                    {$list_diem[0]['diem_4']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_4_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">3</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Có phẩm chất chính trị, đạo đức tốt; có lối sống lành mạnh, có tác phong lề lối làm việc chuẩn mực; tận tụy trách nhiệm trong công việc; Giữ gìn đoàn kết nội bộ, thực hiện nguyên tắc tập trung dân chủ; có tinh thần cầu thị, lắng nghe cộng tác, giúp đỡ đồng nghiệp<br><b>- Điểm tối đa: 3</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_5']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_5_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">4</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Có thái độ phục vụ nhân dân đúng mực; Không hách dịch, cửa quyền, sách nhiễu, gây phiền hà, tiêu cực trong thực hiện nhiệm vụ, Không hẹn gặp giải quyết công việc bên ngoài cơ quan và ngoài giờ làm việc<br><b>- Điểm tối đa: 3</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_6']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_6_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">5</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Thực hiện tốt văn hóa nơi công sở; Mặc trang phục lịch sự, đầu tóc gọn gàng, đúng quy định; Đeo thẻ công chức, viên chức, nhân viên trong giờ làm việc; Tư thế cử chỉ nghiêm túc; Thái độ khiêm tốn lịch sự; Ngôn ngữ chuẩn mực, rõ ràng, hòa nhã; Không hút thuốc tại cơ quan, phòng làm việc; Không sử dụng đồ uống có cồn trong giờ làm việc; Sắp xếp nơi làm việc ngăn nắp,gọn gàng, sạch sẽ  <br><b>- Điểm tối đa: 3</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_7']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_7_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">6</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Xây dựng hình ảnh, giữ gìn uy tín cho bản thân, cơ quan, đơn vị và đồng nghiệp; giữ gìn bí mật cơ quan, đơn vị và thực hiện ngiêm kỷ luật phát ngôn <br><b>- Điểm tối đa: 2</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_8']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_8_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">7</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Chấp hành nghiêm quy định về thời gian làm việc của nhà nước, của cơ quan; sắp xếp, sử dụng thời gianlàm việc khoa học và hiệu quả<br><b>- Điểm tối đa: 1</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_9']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_9_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 

                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"><b>C</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;"><b>Năng lực và kỹ năng thực hiện nhiệm vụ (tối đa 10 điểm)</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_10']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_10_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">1</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Chủ động học tập, rèn luyện, nâng cao trình độ, kỹ năng, năng lực chuyên môn nghiệp vụ, kiến thức ngoại ngữ, tin học; tham mưu đầy đủ, có chất lượng các văn bản phục vụ công tác chỉ đạo, điều hành của đơn vị/bộ phận theo chỉ đạo của lãnh đạo và kế hoạch công tác <br><b>- Điểm tối đa: 2</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_11']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_11_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">2</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Xây dựng kế hoạch công tác của đơn vị theo lĩnh vực được phân công và kế hoạch công tác của cá nhân rõ nội dung, tiến độ<br><b>- Điểm tối đa: 1</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_12']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_12_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">3</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Chỉ đạo, điều hành, kiểm soát việc thực hiện nhiệm vụ của đơn vị, bộ phận đảm bảo kịp thời, không bỏ sót nhiệm vụ. Phân cong, chỉ đạo giải quyết công việc linh hoạt, có định hướng, đúng quy trình<br><b>- Điểm tối đa: 2</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_13']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_13_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">4</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Kiểm tra, bao quát, đôn đốc việc thực hiện nhiệm vụ của CBCCVC trong đơn vị/bộ phận và giải quyết kịp thời những khó khăn, vướng mắc theo thẩm quyền<br><b>- Điểm tối đa: 1</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_14']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_14_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">5</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Có năng lực tập hợp CBCCVC, xây dựng đơn vị/bộ phận đoàn kết, thống nhất; phối hợp, tạo lập mối quan hệ tốt với cá nhân, tổ chức có liên quan trong thực hiện nhiệm vụ<br><b>- Điểm tối đa: 1</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_15']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_15_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">6</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Sử dụng thành thạo các phần mềm, ứng dụng CNTT đáp ứng yêu cầu công việc <br><b>- Điểm tối đa: 2</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_16']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_16_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                               
                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">7</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 5px;">Thiết lập hồ sơ công việc đầy đủ theo các đầu mục công việc được phân công; lưu trữ hồ sơ, tài liệu đúng nguyên tắc; xây dựng kho dữ liệu cá nhân làm cơ sở để xây dựng kho dữ liệu dùng chung của cơ quan, đơn vị<br><b>- Điểm tối đa: 1</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_18']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_18_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr>  

                                <tr>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">D</td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: left;padding-left: 3px;"><b>Điểm thưởng (điểm tối đa: 5)</b></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;"></td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;text-align: center;">
                                        {$list_diem[0]['diem_19']}
                                    </td>
                                    <td style="border-top: 1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000 ;text-align: center;">
                                    {if $list_diem[0].active ==2 }
                                        {$list_diem[0]['diem_19_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr> 
                                <tr><td colspan="15" style="border-top: 1px solid #000; "></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 15px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="13" style="text-align: left;padding-left: 35px;width: 75%;vertical-align: top;">
                                    <b> Nhận xét của trưởng đơn vị:</b><br>
                                    {$list_diem[0]['nhan_xet']}
                                </td>
                                <td colspan="2" style="text-align: center;width: 25%">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b>Cán bộ thực hiện</b><br>
                                    <i>(ký, ghi rõ họ tên)</i><br><br><br><br>

                           {$canbophong[$list_diem[0]['canbo_id']]}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>                
            </div>
        </div>
      <!-- /.box -->

    </section>
 


