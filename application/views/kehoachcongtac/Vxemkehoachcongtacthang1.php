<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-8">
                <span class="font">
                    Đánh giá kết quả kế hoạch công tác tháng: 

                </span>
                     <!-- {$month}  -->
                     <select name="month" style="width: 50px" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <b><a href="thongkecongviec/{$month}/{$cb_id}" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết" target="_blank">[Xem chi tiết công việc trong tháng]</a></b>
            </div>
            <div class="col-md-3 sl3" style="padding-bottom: 3px;">
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
            </div>  
            <div class="col-md-1 sl3">
                Biểu số 3
            </div>         
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12" style="overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3">STT</th>
                                    <th width="15%" class="text-center" rowspan="3">Nội dung</th>
                                    <th width="5%" class="text-center" rowspan="3">Tổng số công việc theo kế hoạch</th>
                                    <th width="25%" class="text-center" colspan="6">Công việc đã hoàn thành trong tháng</th>
                                    <th width="10%" class="text-center" colspan="3">Công việc còn tồn cuối tháng</th>
                                    <th width="5%" class="text-center" rowspan="3"  style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Tỉ lệ hoàn thành (%)</th>
                                    <th width="5%" class="text-center" rowspan="3">Số điểm đạt được hàng tuần</th>
                                    <th width="15%" class="text-center" rowspan="3">Nhận xét của lãnh đạo đơn vị</th>
                                    <th width="5%" class="text-center" rowspan="3">Điểm bình quân của tháng</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2">Chất lượng</th>
                                    <th width="5%" class="text-center" rowspan="2" style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="5%" class="text-center" rowspan="2">Trong hạn</th>
                                    <th width="5%" class="text-center" rowspan="2">Quá hạn</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center">Trong hạn</th>
                                    <th width="5%" class="text-center">Quá hạn</th>
                                    <th width="5%" class="text-center">Đảm bảo</th>
                                    <th width="5%" class="text-center">Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>                                
                                <tr>
                                    <td class="text-center"><b>I</b></td>
                                    <td class="text-left"><b>Kết quả thực hiện nhiệm vụ trong tháng <br>(tối đa 60 điểm)</b></td>
                                    <td class="text-center">{$count_arr[0]}</td>
                                    <td class="text-center">{$count_arr[1]}</td>
                                    <td class="text-center">{$count_arr[2]}</td>
                                    <td class="text-center" style="color: red">{$count_arr[3]}</td>
                                    <td class="text-center">{$count_arr[4]}</td>
                                    <td class="text-center">{$count_arr[5]}</td>
                                    <td class="text-center">{$count_arr[6]}</td>
                                    <td class="text-center">{$count_arr[7]}</td>
                                    <td class="text-center">{$count_arr[8]}</td>
                                    <td class="text-center" style="color: red">{$count_arr[9]}</td>
                                    <td class="text-center">{$count_arr[10]}%</td>
                                    <td class="text-center">
                                    </td>
                                    <td class="text-center">
                                    </td>
                                    <td class="text-center">
                                    <b>{$trungbinhthang1}</b>
                                    </td>

                                </tr>

                                {$i=1}
                                {foreach $list_data as $key => $value}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tuần {$arr_tuan[$j]}</td>
                                    <td class="text-center" style="color: blue">{$value[0]}</td>
                                    <td class="text-center" style="color: blue">{$value[1]}</td>
                                    <td class="text-center" style="color: blue">{$value[2]}</td>
                                    <td class="text-center" style="color: red">{$value[3]}</td>
                                    <td class="text-center" style="color: blue">{$value[4]}</td>
                                    <td class="text-center" style="color: blue">{$value[5]}</td>
                                    <td class="text-center" style="color: blue">{$value[6]}</td>
                                    <td class="text-center" style="color: blue">{$value[7]}</td>
                                    <td class="text-center" style="color: blue">{$value[8]}</td>
                                    <td class="text-center" style="color: red">{$value[9]}</td>
                                    <td class="text-center" style="color: blue">{$value[10]} %</td>
                                    <td class="text-center" style="color: blue">{$kyluat[$key]['0']['diem_1_tp']}</td>
                                    <td class="text-left" style="color: blue" colspan="2">{$nhanxetpp[$j]['nhan_xet_1']}<br>{if $nhanxettp[$j]['nhan_xet'] !=''}<b>Trưởng đơn vị: </b>{$nhanxettp[$j]['nhan_xet']}{/if}</td>
                                </tr> 
                                {/foreach}
                                
                                <tr>
                                    <td class="text-center"><b>II</b></td>
                                    <td class="text-left"><b>Ý thức tổ chức kỷ luật (tối đa 20 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b></b></td>
                                    <td class="text-center" colspan="2"><b>{$trungbinhthang2}</b></td>
                                </tr> 
                                {$i=1}
                                {foreach $kyluat as $key => $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tuần {$arr_tuan[$j]}</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$kl['1']['diem_2_tp']}</b></td>
                                    <td class="text-center" colspan="2"><b></b></td>
                                </tr> 
                                {/foreach}
                                
                                <tr>
                                    <td class="text-center"><b>III</b></td>
                                    <td class="text-left"><b>Năng lực và kỹ năng thực hiện nhiệm vụ (tối đa 10 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b></b></td>
                                    <td class="text-center" colspan="2"><b>{$trungbinhthang3}</b></td>

                                </tr> 
                                {$i=1}
                                {foreach $kyluat as $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tuần {$arr_tuan[$j]}</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$kl['2']['diem_10_tp']}</b></td>
                                    <td class="text-center" colspan="2"><b></b></td>
                                </tr> 
                                {/foreach}
                                <tr>
                                    <th class="text-center">IV</th>
                                    <td class="text-left"><b>Điểm thưởng (điểm tối đa: 10)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center" colspan="2"><b>{$trungbinhthang4}</b></td>
                                </tr>
                                {$i=1}
                                {foreach $kyluat as $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tuần {$arr_tuan[$j]}</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$kl['3']['diem_19_tp']}</b></td>
                                    <td class="text-center" colspan="2"><b></b></td>
                                </tr> 
                                {/foreach} 
                                <!-- tổng cộng điểm của các tuần-->
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-left"><b>Tổng cộng (I+II+III+IV)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b></b></td>
                                    <td class="text-center" colspan="2"><b>{$tongdiemthang}</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {if $quyen <= 17}
                <div class="row" style="height: 5px">
                    <div class="row">
                    <div class="col-md-12" style="margin-left: 12px;">
                        <label>Cán bộ tự,đánh giá bản thân và nhận mức phân loại tháng: <span style="color: red">(đ/c {$ten_cb})</span><br></label>
                        {if $giatri['nhanxet_cv'] !=''} {$giatri['nhanxet_cv']}<br>{/if}

                            <i style="color: red">
                            {if isset($giatri) && $giatri['danhgia_cv'] == 6}Hoàn thành xuất sắc nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_cv'] == 5}Hoàn thành tốt nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_cv'] == 4}Hoàn thành nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_cv'] == 3}Hoàn thành nhiệm vụ, còn hạn chế về năng lực{/if}
                            {if isset($giatri) && $giatri['danhgia_cv'] == 2}Không hoàn thành nhiệm vụ{/if}
                        </i>
                       {if $giatri['nhanxet_pp'] !=''} {$giatri['nhanxet_pp']}<br>{/if}
                        {if $giatri['nhanxet'] !=''}<br><br><b>Đánh giá của lãnh đạo đơn vị:</b> {$giatri['nhanxet']}<br>{/if}
                        <i style="color: red">
                            {if isset($giatri) && $giatri['danhgia_tp'] == 6}Hoàn thành xuất sắc nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_tp'] == 5}Hoàn thành tốt nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_tp'] == 4}Hoàn thành nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_tp'] == 3}Hoàn thành nhiệm vụ, còn hạn chế về năng lực{/if}
                            {if isset($giatri) && $giatri['danhgia_tp'] == 2}Không hoàn thành nhiệm vụ{/if}
                        </i>

                        {if $giatri['danhgia_gd']>0 && $quyen != 4}
                            <br><br><b>Đánh giá của giám đốc sở:</b> {$giatri['nhanxet_gd']}<br>
                            <i style="color: red">
                            {if isset($giatri) && $giatri['danhgia_gd'] == 6}Hoàn thành xuất sắc nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
                            {if isset($giatri) && $giatri['danhgia_gd'] == 3}Hoàn thành nhiệm vụ, còn hạn chế về năng lực{/if}
                            {if isset($giatri) && $giatri['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
                        </i>
                    </div>
                        {/if}
                    {if $quyen==4}
                    <div class="row">
                    <div class="col-md-12">
                        <br><br><b>Giám đốc sở đánh giá kết quả tháng:<br></b>
                        <textarea name="nhanxet_gd" style="font-size: 15px;" id="" required="" rows="10" placeholder="Nội dung nhận xét" class="form-control">{$giatri['nhanxet_gd']}</textarea>
                        </div>
                        <input type="hidden" name="key_id" value="{$giatri['id']}">
                        <input type="hidden" name="id_cb" value="{$giatri['id_cb']}">
                {if $giatri['danhgia_gd'] >0}
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="8" {if isset($giatri) && $giatri['danhgia_gd'] == 8}checked{/if}>
                        <b> Hoàn thành xuất sắc tiêu biểu</b>
                    </div>
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="6" {if isset($giatri) && $giatri['danhgia_gd'] == 6}checked{/if}>
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="5" {if isset($giatri) && $giatri['danhgia_gd'] == 5}checked{/if}>
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="4" {if isset($giatri) && $giatri['danhgia_gd'] == 4}checked{/if}>
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <!-- <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="3" {if isset($giatri) && $giatri['danhgia_gd'] == 3}checked{/if}>
                        <b> Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</b>
                    </div> -->
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="2" {if isset($giatri) && $giatri['danhgia_gd'] == 2}checked{/if}>
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                {else}
                <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="8" {if isset($giatri) && $giatri['danhgia_gd'] == 8}checked{/if}>
                        <b> Hoàn thành xuất sắc tiêu biểu</b>
                    </div>
                <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="6" {if isset($giatri) && $giatri['danhgia_tp'] == 6}checked{/if}>
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="5" {if isset($giatri) && $giatri['danhgia_tp'] == 5}checked{/if}>
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="4" {if isset($giatri) && $giatri['danhgia_tp'] == 4}checked{/if}>
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <!-- <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="3" {if isset($giatri) && $giatri['danhgia_tp'] == 3}checked{/if}>
                        <b> Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</b>
                    </div> -->
                    <div class="col-md-2">
                        <input type="radio" name="danhgia_gd" value="2" {if isset($giatri) && $giatri['danhgia_tp'] == 2}checked{/if}>
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                {/if}
                    

                    <div class="col-md-12">
                        <button type="submit" name="luudulieugd" value="luudulieugd" class="btn btn-primary">Cập nhật đánh giá tháng {$month}</button>
                    </div>
                </div>
                {/if}
                    
                    
                </div>
                </div>

                <div class="row" style="height: 5px"></div>
                {/if}
                {if $phong_id ==  $phong_nguoidung} <!--&& $handanhgiathang >= $ngayhientai -->
                <div class="row" style="height: 10px"></div>
                <div class="row {if ($phongban==12&&$quyenhan==11&&$macanbo!=$taikhoan)||($phongban!=12&&$quyenhan==7&&$macanbo!=$taikhoan)}hide{/if}">
                    <div class="col-md-2">
                        {if $quyen >=7}<label>CÁ NHÂN TỰ PHÂN LOẠI:</label>{else}
                        <label>Lãnh đạo phân loại:</label>
                        {/if}
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiathang" value="8" {if isset($giatri) && $giatri['danhgia_cv'] == 8}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="8" {if isset($giatri) && $giatri['danhgia_tp'] == 8}checked{/if}>
                        {/if}
                        <b> Hoàn thành xuất sắc tiêu biểu</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiathang" value="6" {if isset($giatri) && $giatri['danhgia_cv'] == 6}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="6" {if isset($giatri) && $giatri['danhgia_tp'] == 6}checked{/if}>
                        {/if}
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiathang" value="5" {if isset($giatri) && $giatri['danhgia_cv'] == 5}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="5" {if isset($giatri) && $giatri['danhgia_tp'] == 5}checked{/if}>
                        {/if}
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiathang" value="4" {if isset($giatri) && $giatri['danhgia_cv'] == 4}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="4" {if isset($giatri) && $giatri['danhgia_tp'] == 4}checked{/if}>
                        {/if}
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <!-- <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiathang" value="3" {if isset($giatri) && $giatri['danhgia_cv'] == 3}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="3" {if isset($giatri) && $giatri['danhgia_tp'] == 3}checked{/if}>
                        {/if}
                        <b> Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</b>
                    </div> -->
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiathang" value="2" {if isset($giatri) && $giatri['danhgia_cv'] == 2}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiathang" value="2" {if isset($giatri) && $giatri['danhgia_tp'] == 2}checked{/if}>
                        {/if}
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                    
                </div>  
                <div class="row {if $giatri['acitve']==2 && $giatri['danhgia_gd'] > 0}hide{/if}" >
                    <div class="col-md-12">
                        <textarea name="noidungnhanxet" style="font-size: 15px;" id="" required="" rows="15" class="form-control">{if $macanbo==$taikhoan && !empty($giatri)}
						{$giatri['nhanxet_cv']}{else}{if $quyen==8}- Chấp hành nghiêm chủ trương, đường lối, nghị quyết của Đảng, chính sách pháp luật của Nhà nước; 
- Thực hiện tốt 02 bộ Quy tắc ứng xử của Thành phố và nội quy, quy chế làm việc của cơ quan.
- Có ý thức xây dựng phòng, đơn vị đoàn kết, thống nhất. 
- Các công việc được triển khai thực hiện trong tháng:…………………………….

{/if}{if $quyen==7 || $quyen==10 || $quyen==11 }- Chấp hành nghiêm chủ trương, đường lối, nghị quyết của Đảng, chính sách pháp luật của Nhà nước; 
- Thực hiện tốt 02 bộ Quy tắc ứng xử của Thành phố và nội quy, quy chế làm việc của cơ quan.
- Có ý thức xây dựng phòng, đơn vị đoàn kết, thống nhất. 
- Các công việc được triển khai thực hiện trong tháng:…………………………….
{/if}{/if}{if $quyenhan==6 || ($quyenhan==3 && $chucvu==6)}{$giatri['nhanxet']}{/if}
						{if !empty($nhanxet_phophong)}{$nhanxet_phophong['sNhanXet']}{/if}</textarea>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-12" style="color: blue">Lưu ý tiêu chí<br> - Hoàn thành xuất sắc nhiệm vụ là: trung bình từ 90 điểm trở lên (phải có điểm thưởng) <br>- Hoàn thành tốt nhiệm vụ là: trung bình từ 70 điểm trở lên và dưới 90 điểm 
                    <br>- Hoàn thành nhiệm vụ là: trung bình từ 50 điểm trở lên và dưới 70 điểm 
                    <br>- Không hoàn thành nhiệm vụ là: trung bình dưới 50 điểm</div>
                </div>
                <div class="row"><div class="col-md-12" style="height: 5px;">&nbsp;</div></div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="quyenhan" value="{$quyen}">
                        <input type="hidden" name="canbo_id" value="{$cb_id}">
                        <input type="hidden" name="thang" value="{$month}">
                        <input type="hidden" name="diem" value="{$tongdiemthang}">
                        
						{if !isset($giatri) || $giatri['acitve'] ==1} 
						    <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật đánh giá tháng {$month}</button>
                        {elseif ($quyenhan==6 && $giatri['acitve'] ==2 || ($quyenhan==3&&$chucvu==6&&$giatri['acitve'] ==2))}
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật đánh giá tháng {$month}</button>
                        {/if} 

                        {if !empty($mocham) && $mocham[0]['canbo'] == $cb_id && $month == $mocham[0]['thang'] && $giatri['acitve'] > 2}
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật đánh giá tháng {$month}</button>
                        {/if}                       
						
                    </div>
                </div> 
                {/if}                               
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

