<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-3">
                <span class="font">
                    Danh sách kế hoạch công tác quý
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn quý công tác: 
                </span>
                <label class="8 sl2">
                    <select name="quy" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=4;$i++}
                                <option value="{$i}" {if $quy == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                </label>
            </div>
            <div class="col-md-4 sl3">
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
            </div>
            <div class="col-md-1 sl3">Biểu số 6</div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3" >STT</th>
                                    <th class="text-center" rowspan="3" >Người thực hiện</th>
                                    <th width="5%" class="text-center" rowspan="3" >Tổng số việc theo kế hoạch</th>
                                    <th width="30%" class="text-center" colspan="6" >Công việc đã hoàn thành trong quý</th>
                                    <th width="15%" class="text-center" colspan="3" >Công việc còn tồn cuối quý</th>
                                    <th width="8%" class="text-center" style="border-left: 1px solid #FFF" rowspan="3"  >Tỉ lệ hoàn thành (%)</th>
                                    <th width="6%" class="text-center" rowspan="3" >Điểm trung bình của quý</th>
                                    <th width="10%" class="text-center" rowspan="3" >Kết quả phân loại mức độ hoàn thành nhiệm vụ</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2" >Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2"  >Chất lượng</th>
                                    <th width="5%" class="text-center" style="border-left: 1px solid #FFF" rowspan="2" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="5%" class="text-center"  rowspan="2">Trong hạn</th>
                                    <th width="5%" class="text-center"  rowspan="2">Chậm tiến độ</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" >Trong hạn</th>
                                    <th width="5%" class="text-center" >Quá hạn</th>
                                    <th width="5%" class="text-center" >Đảm bảo</th>
                                    <th width="5%" class="text-center" >Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>I</b></td>
                                    <td style=" text-align: left;padding-left: 5px"><b>Lãnh đạo phòng, đơn vị</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: left;padding-left: 5px"></td>
                                    <td>
                                    </td>
                                    <td >
                                    </td>
                                </tr>

                                {$jj=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key <= 10}
                                <tr>
                                    <td>{$jj++}</td>
                                    <td style=" text-align: left;padding-left: 5px">
                                        {if $quyen ==3 || $quyen >=6}
                                            <a href="xemkehoachcongtacquy/{$giatri[12]}/{$quy}">{$giatri[11]}</a></td>
                                        {else}
                                            {$giatri[11]}
                                        {/if}
                                    </td>
                                    <td>{$giatri[0]}</td>
                                    <td>{$giatri[1]}</td>
                                    <td>{$giatri[2]}</td>
                                    <td>{$giatri[3]}</td>
                                    <td>{$giatri[4]}</td>
                                    <td>{$giatri[5]}</td>
                                    <td>{$giatri[6]}</td>
                                    <td>{$giatri[7]}</td>
                                    <td>{$giatri[8]}</td>
                                    <td>{$giatri[9]}</td>
                                    <td>{$giatri[10]} %</td>
                                    <td >
                                        {$giatri[13]['diem']}
                                    </td>
                                    <td >{if $giatri[14]['danhgiaquy_ld'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==5}<span style="color:#00EE00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==4}<span style="color:#00FF00">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                    </td>

                                </tr>
                                {/if}
                                {/foreach}

                                <tr>
                                    <td><b>II</b></td>
                                    <td style=" text-align: left;padding-left: 5px"><b>Công chức, viên chức, LĐHĐ</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: left;padding-left: 5px"></td>
                                    <td > </td>
                                    <td></td>

                                </tr>
                                {$ii=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key > 10}
                                <tr>
                                    <td>{$ii++}</td>
                                    <td style=" text-align: left;padding-left: 5px">
                                        {if $quyen ==3 || $quyen >=6}
                                            <a href="xemkehoachcongtacquy/{$giatri[12]}/{$quy}">{$giatri[11]}</a></td>
                                        {else}
                                            {$giatri[11]}
                                        {/if}
                                    <td>{$giatri[0]}</td>
                                    <td>{$giatri[1]}</td>
                                    <td>{$giatri[2]}</td>
                                    <td>{$giatri[3]}</td>
                                    <td>{$giatri[4]}</td>
                                    <td>{$giatri[5]}</td>
                                    <td>{$giatri[6]}</td>
                                    <td>{$giatri[7]}</td>
                                    <td>{$giatri[8]}</td>
                                    <td>{$giatri[9]}</td>
                                    <td>{$giatri[10]} %</td>
                                    <td >
                                        {$giatri[13]['diem']}
                                    </td>
                                    <td >{if $giatri[14]['danhgiaquy_ld'] ==6}<span style="color:#0000FF
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==5}<span style="color:#00FF00">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==4}<span style="color:#00CC66">Hoàn thành nhiệm vụ</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if}
                                        {if $giatri[14]['danhgiaquy_ld'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}
                                    </td>                              
                                </tr>
                                {/if}
                                {/foreach}
                                
                                <tr>
                                    <td colspan="2" style="text-align: center;"><b>Tổng cộng</b></td>
                                    <td><b>{$count_arr1[0]}</b></td>
                                    <td><b>{$count_arr1[1]}</b></td>
                                    <td><b>{$count_arr1[2]}</b></td>
                                    <td><b>{$count_arr1[3]}</b></td>
                                    <td><b>{$count_arr1[4]}</b></td>
                                    <td><b>{$count_arr1[5]}</b></td>
                                    <td><b>{$count_arr1[6]}</b></td>
                                    <td><b>{$count_arr1[7]}</b></td>
                                    <td><b>{$count_arr1[8]}</b></td>
                                    <td><b>{$count_arr1[9]}</b></td>
                                    <td><b>{$count_arr1[10]} %</b></td>
                                    <td ><b>
                                         {if isset($diem_tb)} {$diem_tb} {/if}
                                    </b></td>
                                    <td ><b>
                                    </b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              
                </div>   
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','top');
    });
</script>

