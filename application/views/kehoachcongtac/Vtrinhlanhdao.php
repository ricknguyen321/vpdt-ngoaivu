<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">  
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-12">
                <span class="font">
                    Thêm mới nhiệm vụ giao phòng - kế hoạch phòng trình lãnh đạo
                </span>
            </div>
             <!--<div class="col-md-7 sl3">
                <span class="4 font" style="font-weight: 550">
                    Tuần công tác: 
                </span>
                <label class="8 sl2">
                    <select name="tuan" style="" class="form-control select2" onchange="this.form.submit()">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </label>
            </div>
            <div class="col-md-1">
                <span class="font">
                   
                </span>
            </div> -->          
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-2">Nội dung công việc <span style="color: red">*</span></div>
                        <div class="col-sm-10">
                            <textarea name="kh_noidung" class="form-control textarea" rows="6" style="border: 1px solid #3c8dbc;" placeholder="Nhập nội dung công việc">{($giatri)?$giatri[0]['kh_noidung']:''}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12" style="height: 7px"></div>

                    <div class="col-md-12">
                        {if $iQuyenHan_DHNB == 4 or $iQuyenHan_DHNB == 5}
                        <div class="col-sm-2" style="margin-top: 5px;">Phòng chủ trì <span style="color: red">*</span></div>
                        <div class="col-sm-3" style="margin-top: 5px;">
                            <select name="phong_id" required class="form-control select2" style="width:100%;border:1px solid #3c8dbc;" >
                            <option value="">-- Chọn phòng chủ trì --</option>
                                {foreach $phong_ph as $phongct}
                                    <option value="{$phongct.PK_iMaPB}" {($giatri)?($giatri[0]["phong_id"]==$phongct.PK_iMaPB)?"selected":"":""}> {$phongct.sTenPB}</option>
                                {/foreach}
                            </select>
                        </div>
                        {/if}

                        <div class="col-sm-2" style="padding-top: 3px;">
                            Phòng phối hợp:
                        </div> 
                        <div class="col-sm-2" style="padding-top: 3px;">
                            <input type="button" name="themphoihop"  value="Chọn phòng phối hợp" data-toggle="modal" data-target="#themphoihop" class="btn btn-primary">&nbsp;&nbsp;                
                        </div>
                        <input type="hidden" name="phongban_ph" value="{($giatri)?$giatri[0]['phong_ph']:''}"> 
                        {if $iQuyenHan_DHNB < 4 or $iQuyenHan_DHNB > 5}
                        <div class="col-sm-2" style="margin-top: 5px;">Chuyển lãnh đạo duyệt <span style="color: red">*</span></div>
                        <div class="col-sm-3" style="margin-top: 5px;">
                            <select name="lanhdao_id" required class="form-control select2" style="width:100%;border:1px solid #3c8dbc;" >
                            <option value="">-- Chọn lãnh đạo --</option>
                                {foreach $canbo as $cb}
                                    <option value="{$cb.PK_iMaCB}" {($giatri)?($giatri[0]["lanhdao_id"]==$cb.PK_iMaCB)?"selected":"":""}> {$cb.sHoTen}</option>
                                {/foreach}
                            </select>
                        </div>
                        {/if}
                        <div class="col-sm-1" style="margin-top: 5px;">Hạn xử lý </div>
                        <div class="col-sm-2" style="padding-left: 42px;margin-top: 5px;">
                           <input type="text" class="form-control datepic datemask" id="ngay_han" value="{if !empty($giatri) && $giatri[0]['ngay_han'] >'2017-01-01'}{date_select($giatri[0]['ngay_han'])}{/if}" name="ngay_han" style="border: 1px solid #3c8dbc;" >
                        </div>
                        
                    </div>  
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row">
                    <div class="col-md-12">                                            
                        <div class="col-sm-1">
                            <input type="hidden" name="kh_id" value="{$kh_id}">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </div> 
                </div>
                </form>
                <div class="row" style="height: 12px">
                    <div class="modal fade" id="themphoihop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header" style="padding: 8px;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1">Chọn phòng phối hợp</h4>
                          </div>
                          <div class="modal-body">
                              <div class="col-md-12">
                                    {foreach $phong_ph as $phong}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="checkbox" name="phongban" value="{$phong.PK_iMaPB}" id="{$phong.PK_iMaPB}"> {$phong.sTenPB} 
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ghilai()">Ghi lại</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="modal fade" id="chuahoanthanh" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1"></h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="phongphoihop">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="canbo_id">
                                    <input type="text" class="hide" name="ngay_han">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Nội dung:</label>
                                    <textarea name="lydo" class="form-control" rows="4" ></textarea>
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Chọn file:</label>
                                    <input type="file" class="form-control" name="files[]">
                                  </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luuchuahoanthanh" value="Chưa hoàn thành" class="btn btn-primary">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                      </div>
                    </div>
                </div>
                
                <div class="modal fade" id="LD_danhgia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title1" >Đánh giá kết quả công việc</h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Đánh giá:</label>
                                    <textarea name="lydo" class="form-control" rows="4" ></textarea>
                                  </div>
                                  <div class="form-group">
                                        <input type="radio" name="chatluong" value="1" checked="checked"> Chất lượng đảm bảo &nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="chatluong" value="2"> Không đảm bảo chất lượng <br/>
                                        <input type="radio" name="chatluong" value="3"> Có tính sáng tạo, đổi mới, hiệu quả &nbsp;
                                  </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="LDdanhgia" value="lanhdaodanhgia" class="btn btn-primary">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                      </div>
                </div>

               

                <div class="modal fade" id="LD_duyet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title2" ></h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Lý do:</label>
                                    <textarea name="lydo" class="form-control" rows="4" ></textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="ldduyet" value="Chưa hoàn thành" class="btn btn-primary">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" style="font-size: 14px;font-weight: bold;">Kế hoạch công việc-kết quả giải quyết công việc
                        <!--{foreach $arr as $key => $value}
                            {$giatri = $key+1}
                            {if $week == $giatri} {$week} ({$value}) {/if}                            
                        {/foreach}-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center" rowspan="2">STT</th>
                                    <th width="8%" class="text-center" rowspan="2">Lãnh đạo chỉ đạo</th>
                                    <th width="8%" class="text-center" rowspan="2">Phòng giải quyết chính</th>
                                    <th width="8%" class="text-center" rowspan="2">Phòng phối hợp</th>
                                    <th width="" class="text-center" rowspan="2">Nội dung kế hoạch</th>
                                    <th width="8%" class="text-center" rowspan="2">Ngày nhận</th>
                                    <th width="8%" class="text-center" rowspan="2">Hạn xử lý</th>
                                    <th width="15%" class="text-center" colspan="3">Kết quả thực hiện</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" >Hoàn Thành</th>
                                    <th width="5%" class="text-center">Đang thực hiện</th>
                                    <th width="5%" class="text-center">Tình trạng</th>
                                </tr>
                            </thead>
                            <tbody>
                           
                            {$i = 1}
                            {foreach $list_kh as $kh}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                <td width="8%" class="text-center">
                                    {foreach $canbo as $cb}
                                        {if $kh.lanhdao_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}
                                </td>
                               
                                <td>
                                    {foreach $phong_ph as $phong}
                                        {if $kh.phong_id == $phong.PK_iMaPB} {$phong.sTenPB}{/if}
                                    {/foreach}
                                </td>
                                <td>
                                   {$kh.phong_phoihop}
                                </td>
                                <td class="text-left" align="text-left">
                                {if ($kh.active < 3 and $kh.user_input == $PK_iMaCB and ($iQuyenHan_DHNB < 4 or $iQuyenHan_DHNB > 5 )) or ($kh.active == 3 and $kh.user_input == $PK_iMaCB and ($iQuyenHan_DHNB == 4 or $iQuyenHan_DHNB == 5 ))}
                                <a href="trinhlanhdao/{$kh.kh_id}"> 
                                {/if}   
                                    {$kh.kh_noidung}  
                                {if ($kh.active < 3 and $kh.user_input == $PK_iMaCB and ($iQuyenHan_DHNB < 4 or $iQuyenHan_DHNB > 5 )) or ($kh.active == 3 and $kh.user_input == $PK_iMaCB and ($iQuyenHan_DHNB == 4 or $iQuyenHan_DHNB == 5 ))}
                                </a> 
                                {/if}

                                {if $kh.active == 2}
                                <span style="color: red;font-weight:800; ">Lãnh đạo chưa duyệt:</span> {$kh.lydo_khongduyet} (<b>{date_time($kh.ngay_ld_khongduyet)}</b>)
                                {/if}

                                {if $kh.active >= 3 && $kh.lydo_duyet !=''}
                                <br><span style="color: blue;font-weight:800; ">Lãnh đạo duyệt:</span> {$kh.lydo_duyet} (<b>{date_time($kh.ngay_ld_duyet)}</b>)
                                {/if}

                                {if $kh.khokhan_vuongmac != ''}<br>
                                    <label>Lý do chưa hoàn thành: </label>
                                    <span style="color: red">{$kh.khokhan_vuongmac} ({date_time($kh.ngay_hoanthanh)}) </span>
                                {/if}

                                {if $kh.ketqua_hoanthanh != ''}<br>
                                    <label>Kết quả hoàn thành: </label>
                                    <span style="color: blue">{$kh.ketqua_hoanthanh}  ({date_time($kh.ngay_hoanthanh)})</span>
                                {/if} 
                                {if $kh.file_chutri !=''}<br>
                                    <label>Tệp tin đính kèm: </label>
                                        <a href="kehoach_2018/{$kh.file_chutri}"> [File] </a>
                                {/if}

                                {if $kh.file_phoihop !=''}<br><i> Danh sách phối hợp:</i>
                                    {foreach $kh.file_phoihop as $phoihop}    
                                        <br> - <b>{$array_pb[$phoihop.phong_id]}: </b>{$phoihop.lydo} &nbsp;&nbsp;<i>({date_time($phoihop.thoigianluu)})</i> 
                                        {if $phoihop.file_upload !=''}<a href="kehoach_2018/{$phoihop.file_upload}">[File] </a>{/if}
                                    {/foreach}
                                    <br>
                                {/if}


                                {if $kh.active ==1 and $kh.lanhdao_id == $PK_iMaCB}
                                     <input type="button" name="khongduyet" id="{$kh.kh_id}" data-toggle="modal" data-target="#LD_duyet" value="Không duyệt">&nbsp;&nbsp;                                    
                                    <input type="button" name="duyetviec" id="{$kh.kh_id}" data-toggle="modal" data-target="#LD_duyet" value="Duyệt">
                                {/if}

                                {if ($kh.active == 3 or $kh.active == 4)and $kh.phong_id == $PK_iMaPB}
                                    <input type="button" name="hoanthanh" id="{$kh.kh_id}" value="Hoàn thành" data-toggle="modal" data-target="#chuahoanthanh">&nbsp;&nbsp;
                                    <input type="button" name="chuahoanthanh" id="{$kh.kh_id}" data-toggle="modal" data-target="#chuahoanthanh" value="Chưa hoàn thành">
                                    <input type="hidden" name="thulychinh_{$kh.kh_id}" value="1">
                                {/if}  

                                {if $kh.arr_phoihop !=''}
                                    {foreach $kh.arr_phoihop as $ph}
                                        {if $PK_iMaPB == $ph && $kh.active_phoihop != 1 }
                                        <input type="button" name="hoanthanh" id="{$kh.kh_id}" value="Hoàn thành" data-toggle="modal" data-target="#chuahoanthanh">&nbsp;&nbsp;
                                        <input type="button" name="chuahoanthanh" id="{$kh.kh_id}" data-toggle="modal" data-target="#chuahoanthanh" value="Chưa hoàn thành">
                                        <input type="hidden" name="thulychinh_{$kh.kh_id}" value="2">
                                        {/if}
                                    {/foreach}
                                {/if}

                                {if $kh.active == 5 and $kh.lanhdao_id == $PK_iMaCB and $kh.lanhdao_danhgia =='' }
                                    <br> <input type="button" name="danhgia" id="{$kh.kh_id}" data-toggle="modal" data-target="#LD_danhgia" value="Đánh giá kết quả công việc">&nbsp;&nbsp;                                    
                                {/if}

                                {if $kh.active == 5 and $kh.lanhdao_danhgia !='' }
                                    <hr style="display: block; margin-top: 0.5em; margin-bottom: 0.5em; margin-left: auto; margin-right: auto;  border-style: inset;border-width: 1px;">
                                    <b>Nhận xét của lãnh đạo:</b> "{$kh.lanhdao_danhgia}"
                                    <br> <b>Chất lượng công việc:</b>
                                    {if $kh.chat_luong == 1} <span style="color:blue">CHẤT LƯỢNG ĐẢM BẢO </span>{/if} 
                                    {if $kh.chat_luong == 2} <span style="color:red">CHẤT LƯỢNG KHÔNG ĐẢM BẢO </span>{/if} 
                                    {if $kh.chat_luong == 3} <span style="color:blue; font-weight:600; ">CÓ TÍNH SÁNG TẠO, ĐỔI MỚI, HIỆU QUẢ{/if}                                    
                                {/if}

                                </td>
                                <td>{date_select($kh.date_nhap)}</td>
                                <td class="text-left" align="text-left">
                                    {date_select($kh.ngay_han)} 
                                    <input type="hidden" name="han_xl_{$kh.kh_id}" value="{$kh.ngay_han}"> 
                                    <input type="hidden" name="{$kh.kh_id}" value="{$kh.kh_id}">                            
                                </td>
                                <td class="text-center" align="text-center" style="color: blue">
                                    {if $kh.active == 5} X {/if}                                 
                                </td>
                                <td class="text-center" align="text-center" style="color: blue">
                                    {if $kh.active < 5} X {/if}                             
                                </td>
                                <td class="text-left" align="text-left">
                                {if date_insert($kh.ngay_hoanthanh) > $kh.ngay_han && $kh.ngay_han > '2018-01-01'}<span style="color: red">Quá Hạn</span>{/if} 

                                {if $kh.active < 5 && $kh.ngay_han > '2018-01-01' && $kh.ngay_han < date('Y-m-d')}<span style="color: red">Quá Hạn</span>{/if} 

                                {if $kh.ngay_han < '2018-01-01' or (date_insert($kh.ngay_hoanthanh) <= $kh.ngay_han) or ($kh.ngay_han >= date('Y-m-d')) }<span style="color: blue">Trong hạn</span>{/if}                            
                                </td>
                               
                            </tr>
                            {/foreach}
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var url = window.location.href;
        $(document).on('click','input[name=hoanthanh]',function(){
            var id = $(this).attr('id');
            var canbo = $('input[name='+id+']').val();
            var thulychinh = $('input[name=thulychinh_'+id+']').val();
            var ngay_han = $('input[name=han_xl_'+id+']').val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=ngay_han]').val(ngay_han);
            $('input[name=trangthai]').val(1);
            $('input[name=phongphoihop]').val(thulychinh);
            $('.modal-title').text("Hoàn thành công việc");
        });

        $(document).on('click','input[name=chuahoanthanh]',function(){
            var id = $(this).attr('id');
            var canbo = $('input[name='+id+']').val();
            var thulychinh = $('input[name=thulychinh_'+id+']').val();
            var ngay_han = $('input[name=han_xl_'+id+']').val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=ngay_han]').val(ngay_han);
            $('input[name=trangthai]').val(2);
            $('input[name=phongphoihop]').val(thulychinh);
            $('.modal-title').text("Lý do chưa hoàn thành công việc");
            
        });

        $(document).on('click','input[name=danhgia]',function(){
            var id = $(this).attr('id');
            $('input[name=kehoach_id]').val(id);
        });

         $(document).on('click','input[name=duyetviec]',function(){
            var id = $(this).attr('id');
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(1);
            $('.modal-title2').text("Duyệt đề xuất của phòng");
        });
         $(document).on('click','input[name=khongduyet]',function(){
            var id = $(this).attr('id');
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(2);
            $('.modal-title2').text("Không duyệt đề xuất của phòng");
        });

        $(document).on('click','input[name=themphoihop]',function(){
            var str1 = $('input[name=phongban_ph]').val();
            if(str1 !=''){
                $(':checkbox').each(function(i){
                    if(str1.indexOf($(this).val()) != -1){
                        $('#'+$(this).val()).attr('checked','checked');
                    }
                });
            }  
        });
    });

    function ghilai(){ 
        var str_id ='';
        $.each($("input[name='phongban']:checked"), function(){
            str_id = str_id + $(this).val() + ",";
        });
         str_id = str_id.substring(0, str_id.length - 1);
        $('input[name=phongban_ph]').val(str_id);
    }   

 

 
</script>
