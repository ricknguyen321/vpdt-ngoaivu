<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-9">
                    <h3 class="font">
                        Thống kê báo cáo đánh giá kết quả thực hiện nhiệm vụ công tác tháng
                    </h3>
                </div>
                <div class="col-sm-3">
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{$url}thongkebaocaothang" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Tháng bắt đầu  </label>

                                                    <div class="col-sm-8">
                                                        <select name="thangbd" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $thangbd == 0}selected ="selected"{/if}>-- Chọn tháng bắt đầu --</option>
                                                            {for $i=1;$i<=12;$i++}
                                                                <option value="{$i}"  {if $thangbd == $i}selected ="selected"{/if}> Tháng {$i}</option>
                                                            {/for}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Tháng kết thúc</label>

                                                    <div class="col-sm-8">
                                                        <select name="thangkt" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $thangkt == 0}selected ="selected"{/if}>-- Chọn tháng bắt đầu --</option>
                                                            {for $i=1;$i<=12;$i++}
                                                                <option value="{$i}"  {if $thangkt == $i}selected ="selected"{/if}> Tháng {$i}</option>
                                                            {/for}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Phòng, đơn vị</label>

                                                    <div class="col-sm-8">
                                                        <select name="phongban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $phong == 0}selected ="selected"{/if}>-- Chọn phòng, đơn vị --</option>
                                                            <option value="73" {if $phong == 73}selected ="selected"{/if}>Ban Giám đốc Sở</option>
                                                             {foreach $phongban as $key_phong => $value_phong}
                                                                <option value="{$key_phong}" {if $phong == $key_phong}selected ="selected"{/if}> {$value_phong}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Cán bộ</label>

                                                    <div class="col-sm-8">
                                                        <select name="canbo" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" >-- Chọn cán bộ --</option>
                                                            {foreach $danhsachcbphong as $value_canbo}
                                                                <option value="{$value_canbo.PK_iMaCB}" {if $canbo == $value_canbo.PK_iMaCB }selected ="selected"{/if}> {$value_canbo.sHoTen}</option>
                                                            {/foreach}
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Mức đánh giá :</label>

                                                    <div class="col-sm-8">
                                                        <select name="danhgia" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $loai == 0}selected ="selected"{/if}>-- Chọn mức đánh giá --</option>
                                                            <option value="4" {if $loai == 4}selected ="selected"{/if}>-- Hoàn thành xuất sắc tiêu biểu --</option>
                                                            <option value="1"  {if $loai == 1}selected ="selected"{/if}>-- Hoàn thành xuất sắc nhiệm vụ --</option>
                                                            <option value="2"  {if $loai == 2}selected ="selected"{/if}>-- Hoàn thành tốt nhiệm vụ --</option>
                                                            <option value="3"  {if $loai == 3}selected ="selected"{/if}>-- Hoàn thành nhiệm vụ --</option>
                                                            <!-- <option value="4" {if $loai == 4}selected ="selected"{/if}>-- Hoàn thành còn hạn chế về năng lực --</option> -->
                                                            <option value="5" {if $loai == 5}selected ="selected"{/if}>-- Không hoàn thành nhiệm vụ --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> 
                                            
                                            <div class="col-md-12" >
                                                <button class="btn btn-primary  pull-right" name="thongke" value="thongkebaocaothang" >Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-12 deletepadding">
                   <!--  {$phantrang} -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center align-middle">STT</th>
                                    <th width="12%" class="text-center align-middle">Cán bộ</th>
                                    <th width="12%" class="text-center align-middle">phòng</th>
                                    <th width="5%" class="text-center align-middle">Tháng</th>
                                    <th width="8%" class="text-center align-middle">Số điểm</th>
                                    <th width="" class="text-center align-middle">Đánh giá của lãnh đạo đơn vị</th>
									<th width="12%" class="text-center align-middle">Kết quả phân loại</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-left"><b>I</b></td>
                                <td class="text-left" colspan="6"><b>Kết quả thực hiện nhiệm vụ tháng của lãnh đạo sở</b></td>
                            </tr>
                            {$j =1}
                            {foreach $kehoach_gd as $kh_gd}
                            <tr>
                               <td class="text-left" rowspan="3">{$j++}</td>
                               <td rowspan="3">
                                 <a href="{$url}xemkehoachcongtac_GD/{$kh_gd.canbo_id}/{$kh_gd.thang}" target="_blank" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết">
                                    {$tencanbo[$kh_gd.canbo_id]}
                                </a>
                                </td>
                               <td class="text-left" rowspan="3">{$phongban[$kh_gd.phong_id]}</td>
                               <td class="text-center" rowspan="3"><b>{$kh_gd.thang}</b></td>
                               <td class="text-left" rowspan="3">{$kh_gd.diem_tong_tp}</td>
                                <td><b>Cấp có thẩm quyền nhận xét, đánh giá: </b><br><br><br><br></td>
                               <td><b>Cấp có thẩm quyền quyết định: </b><br><br><br><br></td>
                               
                           </tr>
                           <tr>
                                <td class="text-left">
                                    {if $kh_gd.danhgia_ld !=''}
                                    
                                    <b>Tập thể Ban Giám đốc Sở đánh giá: </b>
                                    {if $kh_gd.active >= 3}{$kh_gd.nhanxet_bgd}{/if}
                               </td>
                               <td class="text-left">
                                
                                <b>Tập thể Ban Giám đốc Sở đề xuất mức phân loại: </b>
                            {if $kh_gd.active >= 3}
                                {if $kh_gd.danhgia_bgd == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                {if $kh_gd.danhgia_bgd == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh_gd.danhgia_bgd == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh_gd.danhgia_bgd == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh_gd.danhgia_bgd == 3}
                                        <span style="color: #110000">Hoàn thành nhiệm vụ, còn hạn chế về năng lực</span>
                                    {/if}
                                    {if $kh_gd.danhgia_bgd == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                                {/if}
                                <br><br>
                                
                               </td>
                           </tr>
                           <tr>
                               <td><b>Cá nhân tự nhận xét, đánh giá: </b>{$kh_gd.danhgia_ld}{/if}</td>
                               <td>
                                   <b>Cá nhân tự nhận mức phân loại: </b><br>
                                {if $kh_gd.danhgia_lds == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                {if $kh_gd.danhgia_lds == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh_gd.danhgia_lds == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh_gd.danhgia_lds == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh_gd.danhgia_lds == 3}
                                        <span style="color: #110000">Hoàn thành nhiệm vụ, còn hạn chế về năng lực</span>
                                    {/if}
                                    {if $kh_gd.danhgia_lds == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                               </td>

                           </tr>
                           {/foreach}

                            {foreach $kehoach_ld as $kh_ld}
                            {if $kh_ld.phong_id == 73}
                            <tr>
                               <td class="text-left">{$j++}</td>
                               <td>
                                <a href="{$url}xemkehoachcongtac_lanhdao/{$kh_ld.canbo_id}/{$kh_ld.thang}" target="_blank" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết">
                               {$tencanbo[$kh_ld.canbo_id]}
                                </a></td>
                               <td class="text-left">{$phongban[$kh_ld.phong_id]}</td>
                               <td class="text-center"><b>{$kh_ld.thang}</b></td>
                               <td class="text-left">{$kh_ld.diem_tong_tp}</td>
                               <td class="text-left">
                                    {if $kh_ld.danhgia_ld !=''}<b>Nhận xét của Giám đốc Sở: </b>{$kh_ld.danhgia_ld}{/if}
                               </td>
                               <td class="text-left">
                                <b>Giám đốc Sở quyết định: </b><br>
                                {if $kh_ld.danhgia_lds == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                {if $kh_ld.danhgia_lds == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh_ld.danhgia_lds == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh_ld.danhgia_lds == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh_ld.danhgia_lds == 3}
                                        <span style="color: #110000">Hoàn thành nhiệm vụ, còn hạn chế về năng lực</span>
                                    {/if}
                                    {if $kh_ld.danhgia_lds == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                               </td>
                           </tr>
                           {/if}
                           {/foreach}

                           <tr>
                                <td class="text-left"><b>II</b></td>
                                <td class="text-left" colspan="6"><b>Kết quả thực hiện nhiệm vụ tháng của lãnh đạo Phòng</b></td>
                            </tr>
                            {$jj = 1}
                            {foreach $kehoach_ld as $kh_ld}
                            {if $kh_ld.phong_id != 73}
                            <tr>
                               <td class="text-left">{$jj++}</td>
                               <td>
                                <a href="{$url}xemkehoachcongtac_lanhdao/{$kh_ld.canbo_id}/{$kh_ld.thang}" target="_blank" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết">
                               {$tencanbo[$kh_ld.canbo_id]}
                                </a></td>
                               <td class="text-left">{$phongban[$kh_ld.phong_id]}</td>
                               <td class="text-center"><b>{$kh_ld.thang}</b></td>
                               <td class="text-left">{$kh_ld.diem_tong_tp}</td>
                               <td class="text-left">
                                    {if $kh_ld.danhgia_ld !=''}<b>Nhận xét của Giám đốc Sở: </b>{$kh_ld.danhgia_ld}{/if}
                               </td>
                               <td class="text-left">
                                <b>Giám đốc sở quyết định: </b><br>
                                {if $kh_ld.danhgia_lds == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                {if $kh_ld.danhgia_lds == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh_ld.danhgia_lds == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh_ld.danhgia_lds == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh_ld.danhgia_lds == 3}
                                        <span style="color: #110000">Hoàn thành nhiệm vụ, còn hạn chế về năng lực</span>
                                    {/if}
                                    {if $kh_ld.danhgia_lds == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                               </td>
                           </tr>
                           {/if}
                           {/foreach}

                            <tr>
                                <td class="text-left"><b>III</b></td>
                                <td class="text-left" colspan="6"><b>Kết quả thực hiện nhiệm vụ tháng của phó phòng, chuyên viên</b></td>
                            </tr>
                            {$i = 1}
                            {foreach $kehoach as $kh}
                            <tr>
                               <td class="text-left" rowspan="2">{$i++}</td>
                               <td rowspan="2">
                                <a href="{$url}xemkehoachcongtacthang/{$kh.id_cb}/{$kh.thang}" target="_blank">
                               {$tencanbo[$kh.id_cb]}
                                </a></td>
                               <td class="text-left" rowspan="2">{$phongban[$kh.id_phong]}</td>
                               <td class="text-center" rowspan="2"><b>{$kh.thang}</b></td>
                               <td class="text-left" rowspan="2">{$kh.diem}</td>
                               <td>
                                    {if $kh.acitve == 5}<b>Nhận xét của Giám đốc Sở: </b>{$kh.nhanxet_gd}<br><br>{/if}
                                </td>
                               <td>{if $kh.acitve == 5}
                                <b>Giám đốc sở quyết định: </b>
                                    {if $kh.danhgia_gd == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                    {if $kh.danhgia_gd == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh.danhgia_gd == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_gd == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_gd == 3}
                                        <span style="color: #110000">Hoàn thành nhiệm vụ, còn hạn chế về năng lực</span>
                                    {/if}
                                    {if $kh.danhgia_gd == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                                    <br><br>
                                {/if}</td>
                               
                           </tr>
                           <tr>
                               <td class="text-left">
                                    
                                    
                                    {if $kh.nhanxet !=''}<b>Đánh giá của LĐ phòng:</b>{$kh.nhanxet}{/if}
                               </td>
                               <td class="text-left">
                                <b>LĐ đơn vị: </b>
                                    {if $kh.danhgia_tp == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                    {if $kh.danhgia_tp == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh.danhgia_tp == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_tp == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_tp == 3}
                                        <span style="color: #110000">Hoàn thành nhiệm vụ, còn hạn chế về năng lực</span>
                                    {/if}
                                    {if $kh.danhgia_tp == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                               </td>
                           </tr>
                           {/foreach}
                            </tbody>
                        </table>
                        <!-- <div><b>Tổng văn bản: {$count}</b></div> -->
                    </div>
                </div>
                <div class="col-sm-12 deletepadding">
                   <!--  {$phantrang} -->
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

<script>
    $("#an_hien").hide();
    $("#an_hien1").hide();
    $(document).ready(function() {
        $(document).on('click','button[name=thongke]',function(){
            $(this).addClass('hide');
        });
    });
    var url = window.location.href;
    $(document).on('change','select[name=phongban]',function(){
    var maphong = $(this).val();
    $.ajax({
        url:url,
        type:'post',
        data:{
            action:'getmaphong',
            maphong:maphong
        },
        success:function(respon){
            var result = JSON.parse(respon);
            var tong = result.length;
            var html = '<option value="0">-- Chọn cán bộ --</option>';
            for(var i=0;i<tong;i++)
            {
                html+="<option value='"+result[i]['PK_iMaCB']+"'>"+result[i]['sHoTen']+"</option>";
            }
            $('select[name=canbo]').html(html);
            $('.select2').select2();
        }

    });
   });

function an_hien(){
    if(document.getElementById('lanhdaodonvi').checked) {
        $("#an_hien").show();
        $("#an_hien1").show();
    }else {
        $("#an_hien").hide();
        $("#an_hien1").hide();
    }
}
</script>
