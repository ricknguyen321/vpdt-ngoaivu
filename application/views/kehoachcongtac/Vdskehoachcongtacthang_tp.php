<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-3">
                <span class="font">
                    Danh sách kế hoạch công tác tháng
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn Tháng công tác: 
                </span>
                <label class="8 sl2">
                    <select name="month" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                </label>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3" >STT</th>
                                    <th class="text-center" rowspan="3" >Người thực hiện</th>
                                    <th width="5%" class="text-center" rowspan="3" >Tổng số việc theo kế hoạch</th>
                                    <th width="24%" class="text-center" colspan="6" >Công việc đã hoàn thành trong tháng</th>
                                    <th width="12%" class="text-center" colspan="3" >Công việc còn tồn cuối tháng</th>
                                    <th width="4%" class="text-center" style="border-left: 1px solid #FFF" rowspan="3"  >Tỉ lệ hoàn thành (%)</th>
                                    <th width="4%" class="text-center" style="border-left: 1px solid #FFF" rowspan="3"  >Tỉ lệ hoàn thành đúng hạn (%)</th>
                                    <th width="4%" class="text-center" style="border-left: 1px solid #FFF" rowspan="3"  >Tỉ lệ hoàn thành quá hạn (%)</th>
                                    <th width="4%" class="text-center" rowspan="3" >Điểm tự động</th>
                                    <th width="4%" class="text-center" rowspan="3" >Điểm cá nhân</th>
                                    <th width="4%" class="text-center" rowspan="3" >Điểm LĐ sở</th>
                                    <th width="12%" class="text-center" rowspan="3" >Cán bộ tự nhận xét về ưu điểm, hạn chế</th>
                                    <th width="18%" class="text-center" rowspan="3" >Nhận xét của lãnh đạo sở</th>
                                </tr>
                                <tr>
                                    <th width="4%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="8%" class="text-center" colspan="2" >Thời gian</th>
                                    <th width="8%" class="text-center" colspan="2"  >Chất lượng</th>
                                    <th width="4%" class="text-center" style="border-left: 1px solid #FFF" rowspan="2" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="4%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="4%" class="text-center"  rowspan="2">Trong hạn</th>
                                    <th width="4%" class="text-center"  rowspan="2">Chậm tiến độ</th>
                                    
                                </tr>
                                <tr>
                                    <th width="4%" class="text-center" >Trong hạn</th>
                                    <th width="4%" class="text-center" >Quá hạn</th>
                                    <th width="4%" class="text-center" >Đảm bảo</th>
                                    <th width="4%" class="text-center" >Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                

                                <tr>
                                    <td><b></b></td>
                                    <td colspan="19"  style=" text-align: left;padding-left: 5px"><b>Lãnh đạo phòng</b></td>
                                    
                                </tr>
                                {$ii=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key > 10}
                                <tr>
                                    <td style="vertical-align: middle;">{$ii++}</td>
                                    <td style="vertical-align: middle; text-align: left;padding-left: 5px">
                                        <a href="xemkehoachcongtac_lanhdao/{$giatri[12]}/{$month}" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết">{$giatri[11]}</a></td>
                                    <td style="vertical-align: middle;">{$giatri[0]['tong']}</td>
                                    <td style="vertical-align: middle;">{$giatri[1]['tong']}</td>
                                    <td style="vertical-align: middle;">{$giatri[2]['tong']}</td>
                                    <td style="vertical-align: middle;color: red"><a href="{$url}kehoachcongtac_quahan?listkh={$giatri[23]}" style="color: red" data-content="Click chuột để xem chi tiết" title="Click để xem chi tiết">{$giatri[3]['tong']}</a></td>
                                    <td style="vertical-align: middle;">{$giatri[4]['tong']}</td>
                                    <td style="vertical-align: middle;">{$giatri[5]['tong']}</td>
                                    <td style="vertical-align: middle;">{$giatri[6]['tong']}</td>
                                    <td style="vertical-align: middle;">
                                    {$giatri[7]['tong']}</td>
                                    <td style="vertical-align: middle;">{$giatri[8]['tong']}</td>
                                    <td style="vertical-align: middle;"><a href="{$url}kehoachcongtac_quahan?listkh={$giatri[24]}" style="color: red" data-content="Click chuột để xem chi tiết" title="Click để xem chi tiết">{$giatri[9]['tong']}</a></td>
                                    <td style="vertical-align: middle;">{$giatri[10]} %</td>
                                    <td style="vertical-align: middle;">{$giatri[20]} %</td>
                                    <td style="vertical-align: middle;color: red">{$giatri[21]} %</td>
                                    <td style="vertical-align: middle;"><b>{$giatri[22]['diemtudong']}</b></td>
                                    <td style="vertical-align: middle;" >
                                        {$giatri[13]['diem_tong']}
                                    </td>
                                    <td  style="vertical-align: middle;">
                                        {$giatri[16]['diem_tong_tp']}
                                    </td>
                                    <td style="vertical-align: middle;" class="text-center">{if !empty($giatri[15]['danhgia'])} <button type="button" class="btn btn-default btn-xs" data-trigger="hover" data-container="body" data-toggle="popover" data-html="true"  data-placement="top" data-content="{$giatri[15]['danhgia']}">Cá nhân tự <br> nhận xét</button>{/if}
                                    
                                    {if $giatri[25]['danhgia_canhan'] ==8}<span style="color:red
">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                        {if $giatri[25]['danhgia_canhan'] ==6}<span style="color:red
">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                        {if $giatri[25]['danhgia_canhan'] ==5}<span style="color:blue">Hoàn thành tốt nhiệm vụ</span>{/if}
                                        {if $giatri[25]['danhgia_canhan'] ==4}<span style="color:#00FF00">Hoàn thành nhiệm vụ</span>{/if}
{if $giatri[25]['danhgia_canhan'] ==2}<span style="color:#FF0000">Không hoàn thành nhiệm vụ</span>{/if}

                                    </td>
                                    <td  style="vertical-align: middle;" class="text-center">
                                        {if ($quyen == 4 or $quyen ==3) &&  $giatri[18]['danhgia_cvp'] !=''}<!--
                                            <button type="button" class="btn btn-default btn-xs" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{strip_tags($giatri[18]['danhgia_cvp'])}">Tham mưu <br>của CVP</button><br> -->
                                        {/if}
                                        {if $giatri[19]['danhgia_pgd'] !=''}
                                            <button type="button" class="btn btn-default btn-xs" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="{$giatri[19]['danhgia_pgd']}">Nhận xét <br>của PGĐ</button><br>
                                        {/if}

                                        {if $giatri[14]['danhgia_ld'] !=''}
                                        <button type="button" class="btn btn-default btn-xs" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true"  data-content="{$giatri[14]['danhgia_ld']}">Nhận xét <br>của GĐ</button><br>
                                        {/if}
                                    {if $giatri[17]['danhgia_lds'] ==8 &&  $giatri[14]['danhgia_ld'] !=''}<span style="color:red
                                   ">(Hoàn thành xuất sắc tiêu biểu)</span>{/if}
                                    {if $giatri[17]['danhgia_lds'] ==6 &&  $giatri[14]['danhgia_ld'] !=''}<span style="color:red
                                   ">(Hoàn thành xuất sắc nhiệm vụ)</span>{/if}
                                       {if $giatri[17]['danhgia_lds'] ==5 &&  $giatri[14]['danhgia_ld'] !=''}<span style="color:blue">(Hoàn thành tốt nhiệm vụ)</span>{/if}
                                       {if $giatri[17]['danhgia_lds'] ==4 &&  $giatri[14]['danhgia_ld'] !=''}<span style="color:#00CC66">(Hoàn thành nhiệm vụ)</span>{/if}
                                       <!-- {if $giatri[17]['danhgia_lds'] ==3}<span style="color:#EE0000">Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</span>{/if} -->
                                       {if $giatri[17]['danhgia_lds'] ==2 &&  $giatri[14]['danhgia_ld'] !=''}<span style="color:#FF0000">(Không hoàn thành nhiệm vụ)</span>{/if}
                                    </td>
                                                                 
                                </tr>
                                {/if}
                                {/foreach}
                                <!--
                                <tr>
                                    <td colspan="2" style="text-align: center;"><b>Tổng </b></td>
                                    <td><b>{$count_arr1[0]}</b></td>
                                    <td><b>{$count_arr1[1]}</b></td>
                                    <td><b>{$count_arr1[2]}</b></td>
                                    <td><b>{$count_arr1[3]}</b></td>
                                    <td><b>{$count_arr1[4]}</b></td>
                                    <td><b>{$count_arr1[5]}</b></td>
                                    <td><b>{$count_arr1[6]}</b></td>
                                    <td><b>{$count_arr1[7]}</b></td>
                                    <td><b>{$count_arr1[8]}</b></td>
                                    <td><b>{$count_arr1[9]}</b></td>
                                    <td><b>{$count_arr1[10]} %</b></td>
                                    <td ><b>
                                         {if isset($diem_tb)} {$diem_tb} {/if}
                                    </b></td>
                                    <td ><b>
                                    </b></td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
              
                </div>   
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','middle');
    });

    $(document).ready(function(){
        var url = window.location.href;
        $(document).on('click','button[name=chamdiem]',function(){
            var danhgia_id = $(this).val();
            var nhan_xet  = $('textarea[name="nhanxet_' + danhgia_id + '"]').val();
            var diem_1_tp  = $('select[name="diem_1_tp_' + danhgia_id + '"] option:selected').val();
            var diem_3_tp  = $('select[name="diem_3_tp_' + danhgia_id + '"] option:selected').val();
            var diem_4_tp  = $('select[name="diem_4_tp_' + danhgia_id + '"] option:selected').val();
            var diem_5_tp  = $('select[name="diem_5_tp_' + danhgia_id + '"] option:selected').val();
            var diem_6_tp  = $('select[name="diem_6_tp_' + danhgia_id + '"] option:selected').val();
            var diem_7_tp  = $('select[name="diem_7_tp_' + danhgia_id + '"] option:selected').val();
            var diem_8_tp  = $('select[name="diem_8_tp_' + danhgia_id + '"] option:selected').val();
            var diem_9_tp  = $('select[name="diem_9_tp_' + danhgia_id + '"] option:selected').val();
            var diem_11_tp  = $('select[name="diem_11_tp_' + danhgia_id + '"] option:selected').val();
            var diem_12_tp  = $('select[name="diem_12_tp_' + danhgia_id + '"] option:selected').val();
            var diem_13_tp  = $('select[name="diem_13_tp_' + danhgia_id + '"] option:selected').val();
            var diem_14_tp  = $('select[name="diem_14_tp_' + danhgia_id + '"] option:selected').val();
            var diem_15_tp  = $('select[name="diem_15_tp_' + danhgia_id + '"] option:selected').val();
            var diem_16_tp  = $('select[name="diem_16_tp_' + danhgia_id + '"] option:selected').val();
            var diem_18_tp  = $('select[name="diem_18_tp_' + danhgia_id + '"] option:selected').val();
            var diem_19_tp  = $('select[name="diem_19_tp_' + danhgia_id + '"] option:selected').val();
            var diem_2_tp = parseFloat(diem_3_tp)+parseFloat(diem_4_tp)+parseFloat(diem_5_tp)+parseFloat(diem_6_tp)+parseFloat(diem_7_tp)+parseFloat(diem_8_tp)+parseFloat(diem_9_tp);
            var diem_10_tp = parseFloat(diem_11_tp)+parseFloat(diem_12_tp)+parseFloat(diem_13_tp)+parseFloat(diem_14_tp)+parseFloat(diem_15_tp)+parseFloat(diem_16_tp)+parseFloat(diem_18_tp);
            var diem_tong_tp = parseFloat(diem_1_tp)+parseFloat(diem_2_tp)+parseFloat(diem_10_tp)+parseFloat(diem_19_tp);
            $.ajax({
                url:url,
                type:'POST',
                data:{
                    action:'chamdiem',
                    danhgia_id:danhgia_id,
                    diem_1_tp:diem_1_tp,
                    diem_2_tp:diem_2_tp,
                    diem_3_tp:diem_3_tp,
                    diem_4_tp:diem_4_tp,
                    diem_5_tp:diem_5_tp,
                    diem_6_tp:diem_6_tp,
                    diem_7_tp:diem_7_tp,
                    diem_8_tp:diem_8_tp,
                    diem_9_tp:diem_9_tp,
                    diem_10_tp:diem_10_tp,
                    diem_11_tp:diem_11_tp,
                    diem_12_tp:diem_12_tp,
                    diem_13_tp:diem_13_tp,
                    diem_14_tp:diem_14_tp,
                    diem_15_tp:diem_15_tp,
                    diem_16_tp:diem_16_tp,
                    diem_18_tp:diem_18_tp,
                    diem_19_tp:diem_19_tp,
                    diem_tong_tp:diem_tong_tp,
                    nhan_xet:nhan_xet
                },
                success:function(repon){ 
                    var result = JSON.parse(repon);
                    if(result>0)
                    {   
                        location.reload();
                        showMessage('Bạn đã chấm điểm thành công','info');
                    }


                }
            });
        });
    });
</script>
<style>
    .popover-content{
        text-align: justify!important;
    }
</style>
