<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-7">
                <span class="font">
                    Đánh giá kết quả kế hoạch công tác quý: 
                </span>
                <span class="4" style="font-weight: 550">
                     {$quy} 
                </span>
            </div>
            <div class="col-md-4 sl3" style="padding-bottom: 3px;">
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
            </div>  
            <div class="col-md-1 sl3">
               <!--  Biểu số 3 -->
            </div>         
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3">STT</th>
                                    <th width="30%" class="text-center" rowspan="3">Nội dung</th>
                                    <th width="5%" class="text-center" rowspan="3">Tổng số công việc theo kế hoạch</th>
                                    <th width="25%" class="text-center" colspan="6">Công việc đã hoàn thành trong quý</th>
                                    <th width="10%" class="text-center" colspan="3">Công việc còn tồn cuối quý</th>
                                    <th width="6%" class="text-center" rowspan="3"  style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Tỉ lệ hoàn thành (%)</th>
                                    <th width="5%" class="text-center" rowspan="3">Số điểm đạt được hàng tháng</th>
                                    <th width="5%" class="text-center" rowspan="3">Điểm bình quân của quý</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2">Chất lượng</th>
                                    <th width="5%" class="text-center" rowspan="2" style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="5%" class="text-center" rowspan="2">Trong hạn</th>
                                    <th width="5%" class="text-center" rowspan="2">Quá hạn</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center">Trong hạn</th>
                                    <th width="5%" class="text-center">Quá hạn</th>
                                    <th width="5%" class="text-center">Đảm bảo</th>
                                    <th width="5%" class="text-center">Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>                                
                                <tr>
                                    <td class="text-center"><b>I</b></td>
                                    <td class="text-left"><b>Kết quả thực hiện nhiệm vụ quý {$quy} <br>(tối đa 65 điểm)</b></td>
                                    <td class="text-center">{$count_arr[0]}</td>
                                    <td class="text-center">{$count_arr[1]}</td>
                                    <td class="text-center">{$count_arr[2]}</td>
                                    <td class="text-center" style="color: red">{$count_arr[3]}</td>
                                    <td class="text-center">{$count_arr[4]}</td>
                                    <td class="text-center">{$count_arr[5]}</td>
                                    <td class="text-center">{$count_arr[6]}</td>
                                    <td class="text-center">{$count_arr[7]}</td>
                                    <td class="text-center">{$count_arr[8]}</td>
                                    <td class="text-center" style="color: red">{$count_arr[9]}</td>
                                    <td class="text-center">{$count_arr[10]}%</td>
                                    <td class="text-center">
                                    
                                    </td>
                                    <td class="text-center">
                                    <b>{$trungbinhthang1}</b>
                                    </td>

                                </tr>

                                {$i=1}
                                {foreach $list_data as $key => $value}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="text-center" style="color: blue">{$value[0]}</td>
                                    <td class="text-center" style="color: blue">{$value[1]}</td>
                                    <td class="text-center" style="color: blue">{$value[2]}</td>
                                    <td class="text-center" style="color: red">{$value[3]}</td>
                                    <td class="text-center" style="color: blue">{$value[4]}</td>
                                    <td class="text-center" style="color: blue">{$value[5]}</td>
                                    <td class="text-center" style="color: blue">{$value[6]}</td>
                                    <td class="text-center" style="color: blue">{$value[7]}</td>
                                    <td class="text-center" style="color: blue">{$value[8]}</td>
                                    <td class="text-center" style="color: red">{$value[9]}</td>
                                    <td class="text-center" style="color: blue">{$value[10]} %</td>
                                    <td class="text-center" style="color: blue">{$kyluat[$key]['0']['diem_1_tp']}</td>
                                    <td class="text-center" style="color: blue"></td>
                                </tr> 
                                {/foreach}
                                
                                <tr>
                                    <td class="text-center"><b>II</b></td>
                                    <td class="text-left"><b>Ý thức tổ chức kỷ luật (tối đa 20 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b></b></td>
                                    <td class="text-center"><b>{$trungbinhthang2}</b></td>
                                </tr> 
                                {$i=1}
                                {foreach $kyluat as $key => $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$kl['1']['diem_2_tp']}</b></td>
                                    <td class="text-center"><b></b></td>
                                </tr> 
                                {/foreach}
                                
                                <tr>
                                    <td class="text-center"><b>III</b></td>
                                    <td class="text-left"><b>Năng lực và kỹ năng thực hiện nhiệm vụ (tối đa 10 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b></b></td>
                                    <td class="text-center"><b>{$trungbinhthang3}</b></td>

                                </tr> 
                                {$i=1}
                                {foreach $kyluat as $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$kl['2']['diem_10_tp']}</b></td>
                                    <td class="text-center"><b></b></td>
                                </tr> 
                                {/foreach}
                                <tr>
                                    <th class="text-center">IV</th>
                                    <td class="text-left"><b>Điểm thưởng (điểm tối đa: 5)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$trungbinhthang4}</b></td>
                                </tr>
                                {$i=1}
                                {foreach $kyluat as $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$kl['3']['diem_19_tp']}</b></td>
                                    <td class="text-center"><b></b></td>
                                </tr> 
                                {/foreach} 
                                <!-- tổng cộng điểm của các tuần-->
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-left"><b>Tổng cộng (I+II+III+IV)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b></b></td>
                                    <td class="text-center"><b>{$tongdiemthang}</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {if $quyen < 7}
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                        <label>Nhận xét của trưởng đơn vị đối với đồng chí:<br><i style="color: red">{$canbophong[$cb_id]}</i></label>
                        
                    </div>
                    <div class="col-md-10">
                        <textarea class="form-control" name="lanhdao_nhanxet" style="rows=2" placeholder="Nhận xét của trưởng đơn vị">{if isset($giatri)}{$giatri['lanhdao_nhanxet']}{/if}</textarea>
                    </div>
                </div>
                {/if}
                {if ($quyen >=7 && $PK_iMaCB == $cb_id) || $quyen < 7}
                <div class="row" style="height: 10px"></div>
                <div class="row">
                    <div class="col-md-2">
                        {if $quyen >=7}<label>CÁ NHÂN TỰ PHÂN LOẠI:</label>{else}
                        <label>Lãnh đạo phân loại:</label>
                        {/if}
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiaquy" value="6" {if isset($giatri) && $giatri['danhgiaquy'] == 6}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiaquy_ld" value="6" {if isset($giatri) && $giatri['danhgiaquy_ld'] == 6}checked{/if}>
                        {/if}
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiaquy" value="5" {if isset($giatri) && $giatri['danhgiaquy'] == 5}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiaquy_ld" value="5" {if isset($giatri) && $giatri['danhgiaquy_ld'] == 5}checked{/if}>
                        {/if}
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiaquy" value="4" {if isset($giatri) && $giatri['danhgiaquy'] == 4}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiaquy_ld" value="4" {if isset($giatri) && $giatri['danhgiaquy_ld'] == 4}checked{/if}>
                        {/if}
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiaquy" value="3" {if isset($giatri) && $giatri['danhgiaquy'] == 3}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiaquy_ld" value="3" {if isset($giatri) && $giatri['danhgiaquy_ld'] == 3}checked{/if}>
                        {/if}
                        <b> Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực</b>
                    </div>
                    <div class="col-md-2">
                        {if $quyen >=7}
                            <input type="radio" name="danhgiaquy" value="2" {if isset($giatri) && $giatri['danhgiaquy'] == 2}checked{/if}>
                        {else}
                            <input type="radio" name="danhgiaquy_ld" value="2" {if isset($giatri) && $giatri['danhgiaquy_ld'] == 2}checked{/if}>
                        {/if}
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                    
                </div>  
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="quyenhan" value="{$quyen}">
                        <input type="hidden" name="canbo_id" value="{$cb_id}">
                        <input type="hidden" name="quy" value="{$quy}">
                        <input type="hidden" name="diem" value="{$tongdiemthang}">
                        {if isset($giatri) && (($giatri['active'] == 1)||($giatri['active'] >= 1 && $quyen < 7))}
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật đánh giá quý {$quy}</button>
                        {/if}
                    </div>
                </div> 
                {/if}                               
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

