<table width="100%">
  <tr>
    <td colspan="2" style="padding-top: 10px; text-align: left;">
      Biểu số 01
    </td>
  </tr>
    <tr>
        <td style=" text-align: center;width: 30%">
            UBND THÀNH PHỐ HÀ NỘI<br>
            <span style="text-decoration: underline;">Sở Ngoại Vụ </span>

            </div>
        </td>
        <td style=" text-align: center;width: 70%">
            CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br>
            <span style="text-decoration: underline;"> Độc lập - Tự do - Hạnh phúc</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top: 10px; text-align: center;">
            <b>BIỂU TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ, PHÂN LOẠI CÔNG CHỨC HÀNG THÁNG<br>
Tháng {$thangbd} năm {$nam} (Từ tuần {$tuanbd} đến Tuần {$tuankt})<br>
<i>(Áp dụng cho các đồng chí lãnh đạo Sở, Trưởng phòng, đơn vị trực thuộc Sở)</i></b>

        </td>
    </tr>
</table>



<table >
    <tr>
        <th width="5%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">STT</th>
        <th width="20%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Họ Tên</th>
        <th width="25%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Chức vụ</th>
        <th width="10%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Phần mềm tự động chấm điểm tháng</th>
        <th width="20%" class="text-center align-middle" colspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Cá nhân tự chấm điểm, nhận mực phân loại tháng</th>
        <th width="20%" class="text-center align-middle" colspan="2" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;">Giám đốc sở quyết định chấm điểm, phân loại tháng</th>
    </tr>
    <tr>
        <th width="5%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000">Số điểm</th>
        <th width="15%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000">Mức phân loại</th>
        <th width="5%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000">Số điểm</th>
        <th width="15%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;">Mức phân loại</th>
    </tr>
 
    <tr>
        <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>I</b></td>
        <td class="text-left" colspan="7" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;padding-left: 7px;"><b>Ban giám đốc sở</b></td>
    </tr>
    {$j = 1}
    {foreach $kehoach_ld as $kh_ld}
    {if $kh_ld.phong_id == 73}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$kh_ld.canbo_id]}
        </td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$chucvu[$kh_ld.canbo_id]}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$count_arr[$kh_ld.canbo_id][7]['diemtudong']}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$kh_ld.diem_tong}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
        Hoàn thành tốt nhiệm vụ
       </td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$kh_ld.diem_tong_tp}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
        {if $kh_ld.danhgia_lds == 8}
                <b>Hoàn thành xuất sắc tiêu biểu</b>{/if}
        {if $kh_ld.danhgia_lds == 6}
                <b>Hoàn thành xuất sắc nhiệm vụ</b>{/if}
            {if $kh_ld.danhgia_lds == 5}
                Hoàn thành tốt nhiệm vụ
            {/if}
            {if $kh_ld.danhgia_lds == 4}
                Hoàn thành nhiệm vụ
            {/if}
            {if $kh_ld.danhgia_lds == 3}
                Hoàn thành nhiệm vụ, còn hạn chế về năng lực
            {/if}
            {if $kh_ld.danhgia_lds == 2}
                Không hoàn thành nhiệm vụ
            {/if}
       </td>
   </tr>
   {/if}
   {/foreach}

   <tr>
        <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>II</b></td>
        <td class="text-left" colspan="7" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;"><b>Các trưởng phòng, đơn vị</b></td>
    </tr>
    {$jj = 1}
    {foreach $kehoach_ld as $kh_ld}
    {if $kh_ld.phong_id != 73}
    <tr>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$jj++}</td>
       <td style="border-top:1px solid #000; border-left: 1px solid #000">
            {$tencanbo[$kh_ld.canbo_id]}
      </td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$chucvu[$kh_ld.canbo_id]}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$count_arr[$kh_ld.canbo_id][22]['diemtudong']}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$kh_ld.diem_tong}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            Hoàn thành tốt nhiệm vụ
       </td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$kh_ld.diem_tong_tp}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
        {if $kh_ld.danhgia_lds == 8}
                <b>Hoàn thành xuất sắc tiêu biểu</b>{/if}
        {if $kh_ld.danhgia_lds == 6}
                <b>Hoàn thành xuất sắc nhiệm vụ</b>{/if}
            {if $kh_ld.danhgia_lds == 5}
                Hoàn thành tốt nhiệm vụ
            {/if}
            {if $kh_ld.danhgia_lds == 4}
                Hoàn thành nhiệm vụ
            {/if}
            {if $kh_ld.danhgia_lds == 3}
                Hoàn thành nhiệm vụ, còn hạn chế về năng lực
            {/if}
            {if $kh_ld.danhgia_lds == 2}
                >Không hoàn thành nhiệm vụ
            {/if}
       </td>
   </tr>
   {/if}
   {/foreach}
   <tr>
        <td class="text-left" style="border-top:1px solid #000;" colspan="8"></td>
    </tr>
    <tr>
      <td colspan="2" style="padding-top: 20px;padding-bottom: 50px;text-align: center;">
        Ngày / /2018<br> <b>Người tổng hợp</b>
      </td>
      <td colspan="3" style="padding-top: 20px;padding-bottom: 50px;text-align: center;">Ngày / /2018<br> <b>Chánh văn phòng</b></td>
      <td colspan="3" style="padding-top: 20px;padding-bottom: 50px;text-align: center;">
        Ngày / /2018<br> <b>Giám đốc</b>   
      </td>
    </tr>
</table>
<table width="100%">
  <tr>
    <td colspan="2" style="padding-top: 10px; text-align: left;">
      Biểu số 02
    </td>
  </tr>
    <tr>
        <td style=" text-align: center;width: 30%">
            UBND THÀNH PHỐ HÀ NỘI<br>
            <span style="text-decoration: underline;">Sở Ngoại Vụ </span>

            </div>
        </td>
        <td style=" text-align: center;width: 70%">
            CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br>
            <span style="text-decoration: underline;"> Độc lập - Tự do - Hạnh phúc</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top: 10px; text-align: center;">
            <b>BIỂU TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ, PHÂN LOẠI CÔNG CHỨC HÀNG THÁNG<br>
Tháng {$thangbd} năm {$nam} (Từ tuần {$tuanbd} đến Tuần {$tuankt})<br>
<i>(Áp dụng từ cấp phó trưởng phòng, đơn vị trực thuộc Sở trở xuống)</i></b>

        </td>
    </tr>
</table>

<table >
    <tr>
        <th width="3%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">TT</th>
        <th width="5%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">STT</th>
        <th width="15%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Họ Tên</th>
        <th width="10%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Chức vụ</th>
        <th width="10%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Phần mềm tính điểm bình quân tháng</th>
        <th width="10%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Cá nhân tự nhận mức phân loại</th>
        <th width="10%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Trưởng phòng, đơn vị phân loại tháng</th>
        <th width="20%" class="text-center align-middle" colspan="2" style="border-top:1px solid #000; border-left: 1px solid #000;">Giám đốc sở quyết định mức phân loại theo đề nghị của Trưởng phòng, đơn vị</th>
        <th width="10%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000">Giám đốc Sở quyết định mức phân loại khác</th>
        <th width="10%" class="text-center align-middle" rowspan="2" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;">Ghi chú</th>
    </tr>
    <tr>
        <th width="10%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000">Đồng ý</th>
        <th width="10%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000">Không đồng ý</th>
    </tr>
{$j=1}{$tt=1}
    {foreach $phongban as $keyphong => $pb}
      <tr>
        <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"></td>
        <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{intToRoman($j++)}</b></td>
        <td class="text-left" colspan="9" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;"><b>{$pb}</b></td>
    </tr>
   
    {$i=1}
    {foreach $kehoach as $kh}
    {if $kh.id_phong eq $keyphong}
    <tr>
      <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$tt++}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$i++}</td>
       <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
       {$tencanbo[$kh.id_cb]}
        </td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$chucvu[$kh.id_cb]}</td>
       <td class="text-center" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$kh.diem}</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {if $kh.danhgia_cv == 8}Hoàn thành xuất sắc tiêu biểu{/if}
            {if $kh.danhgia_cv == 6}Hoàn thành xuất sắc nhiệm vụ{/if}
            {if $kh.danhgia_cv == 5}Hoàn thành tốt nhiệm vụ{/if}
            {if $kh.danhgia_cv == 4}Hoàn thành nhiệm vụ{/if}
            {if $kh.danhgia_cv == 2}Không hoàn thành nhiệm vụ{/if}  </td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {if $kh.danhgia_tp == 8}<b>Hoàn thành xuất sắc tiêu biểu</b>{/if}
            {if $kh.danhgia_tp == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b>{/if}
            {if $kh.danhgia_tp == 5}Hoàn thành tốt nhiệm vụ{/if}
            {if $kh.danhgia_tp == 4}Hoàn thành nhiệm vụ{/if}
            {if $kh.danhgia_tp == 2}Không hoàn thành nhiệm vụ{/if}  </td>
       <td class="text-center" style="border-top:1px solid #000; border-left: 1px solid #000; text-align: center;">X</td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"></td>
       <td class="text-left" style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {if $kh.danhgia_gd == 8}<b>Hoàn thành xuất sắc tiêu biểu</b>{/if}
            {if $kh.danhgia_gd == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b>{/if}
            {if $kh.danhgia_gd == 5}Hoàn thành tốt nhiệm vụ{/if}
            {if $kh.danhgia_gd == 4}Hoàn thành nhiệm vụ{/if}
            {if $kh.danhgia_gd == 2}Không hoàn thành nhiệm vụ{/if}
       </td>
       <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000;">
        
       </td>
       </tr>
       {/if}
       {/foreach} 

    {/foreach}
    <tr>
        <td class="text-left" style="border-top:1px solid #000;" colspan="11"></td>
    </tr>
    <tr>
    <tr>
      <td colspan="3" style="padding-top: 20px;padding-bottom: 50px;text-align: center;">
        Ngày / /2018<br> <b>Người tổng hợp</b>
      </td>
      <td colspan="4" style="padding-top: 20px;padding-bottom: 50px;text-align: center;">Ngày / /2018<br> <b>Chánh văn phòng</b></td>
      <td colspan="3" style="padding-top: 20px;padding-bottom: 50px;text-align: center;">
        Ngày / /2018<br> <b>Giám đốc</b>   
      </td>
    </tr>
    </tr>
</table>          
          

