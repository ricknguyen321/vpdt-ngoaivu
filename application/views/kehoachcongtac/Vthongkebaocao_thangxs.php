<table width="100%">
  <tr>
    <td colspan="2" style="padding-top: 10px; text-align: left;">
      Biểu số 01
    </td>
  </tr>
    <tr>
        <td style=" text-align: center;width: 30%">
            UBND THÀNH PHỐ HÀ NỘI<br>
            <span style="text-decoration: underline;">Sở Ngoại Vụ </span>

            </div>
        </td>
        <td style=" text-align: center;width: 70%">
            CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br>
            <span style="text-decoration: underline;"> Độc lập - Tự do - Hạnh phúc</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top: 10px; text-align: center;">
            <b>BIỂU TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ, PHÂN LOẠI CÔNG CHỨC HÀNG THÁNG<br>
Từ tháng {$thangbd} đến tháng {$thangkt} năm {$nam}<br>
<i>(Áp dụng cho các đồng chí lãnh đạo Sở, Trưởng phòng, đơn vị trực thuộc Sở)</i></b>

        </td>
    </tr>
</table>



<table >
    <tr>
        <th width="5%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000">STT</th>
        <th width="20%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000">Họ Tên</th>
        {foreach $arr_thang as $thang}
        <th width="10%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000;{if $thang == $thangkt}border-right: 1px solid #000{/if}">Tháng {$thang}</th> 
        {/foreach} 
        <th width="10%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000;">Hoàn thành xs tiêu biểu</th> 
        <th width="10%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000;">Hoàn thành xs</th> 
        <th width="10%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000;">Hoàn thành tốt</th>
        <th width="10%" class="text-center align-middle" style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000">Họ Tên</th>        
    </tr>
 
    <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
    {$j = 1}
    <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Lãnh đạo sở</b></td>
    </tr>
    {foreach $count_arrld as $keyld => $valueld}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$keyld]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
          
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {if isset($valueld[$thang][0])}
            {if $valueld[$thang][0]['danhgia_lds'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b>(Lần: {$a++}){/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b>(Lần: {$b++}){/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000 ;padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$keyld]}
        </td>
   </tr>
   
   {/foreach}
<tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
    <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Lãnh đạo phòng ban, đơn vị</b></td>
    </tr>
    {foreach $count_arr as $key => $value}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key]}
        </td>
         {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {if isset($value[$thang][0])}
            {if $value[$thang][0]['danhgia_lds'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b>(Lần: {$a++}){/if}
            {if $value[$thang][0]['danhgia_lds'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b>(Lần: {$b++}){/if}
            {if $value[$thang][0]['danhgia_lds'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value[$thang][0]['danhgia_lds'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value[$thang][0]['danhgia_lds'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key]}
        </td>
   </tr>
   {/foreach}
<tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
  <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Văn phòng sở</b></td>
  </tr>
    
    {foreach $count_arr1 as $key1 => $value1}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key1]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {if isset($value1[$thang][0])}
            {if $value1[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b>(Lần: {$a++}){/if}
            {if $value1[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b>(Lần: {$b++}){/if}
            {if $value1[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value1[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value1[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key1]}
        </td>
   </tr>
   {/foreach}

  <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
<tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>CC Tài chính doanh nghiệp</b></td>
  </tr>
    
    {foreach $count_arr2 as $key2 => $value2}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key2]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value2[$thang][0])}
            {if $value2[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b>(Lần: {$a++}){/if}
            {if $value2[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value2[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value2[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value2[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key2]}
        </td>
   </tr>
   {/foreach}
<tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Phòng Quản lý công sản</b></td>
  </tr>
  
    {foreach $count_arr3 as $key3 => $value3}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key3]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value3[$thang][0])}
            {if $value3[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b>(Lần: {$a++}){/if}
            {if $value3[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b>(Lần: {$b++}){/if}
            {if $value3[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value3[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value3[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key3]}
        </td>
   </tr>
   {/foreach}
   <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Phòng Quản lý giá</b></td>
  </tr>
  
    {foreach $count_arr4 as $key4 => $value4}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key4]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value4[$thang][0])}
            {if $value4[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b> (Lần: {$a++}){/if}
            {if $value4[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value4[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value4[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value4[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key4]}
        </td>
   </tr>
   {/foreach}
   <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Phòng Quản lý ngân sách</b></td>
  </tr>
  
    {foreach $count_arr5 as $key5 => $value5}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key5]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value5[$thang][0])}
            {if $value5[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b> (Lần: {$a++}){/if}
            {if $value5[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value5[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value5[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value5[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key5]}
        </td>
   </tr>
   {/foreach}
   <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Phòng Tài chính hành chính sự nghiệp</b></td>
  </tr>
  
    {foreach $count_arr6 as $key6 => $value6}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key6]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value6[$thang][0])}
            {if $value6[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b> (Lần: {$a++}){/if}
            {if $value6[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value6[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value6[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value6[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key6]}
        </td>
   </tr>
   {/foreach}
   <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Thanh tra sở</b></td>
  </tr>
  
    {foreach $count_arr7 as $key7 => $value7}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key7]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value7[$thang][0])}
            {if $value7[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b> (Lần: {$a++}){/if}
            {if $value7[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value7[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value7[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value7[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key7]}
        </td>
   </tr>
   {/foreach}
   <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Phòng Tài chính đầu tư</b></td>
  </tr>
  
    {foreach $count_arr8 as $key8 => $value8}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key8]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value8[$thang][0])}
            {if $value8[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b> (Lần: {$a++}){/if}
            {if $value8[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value8[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value8[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value8[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key8]}
        </td>
   </tr>
   {/foreach}
   <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>Phòng Tin học và Thống kê</b></td>
  </tr>
  
    {foreach $count_arr9 as $key9 => $value9}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key9]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value9[$thang][0])}
            {if $value9[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b> (Lần: {$a++}){/if}
            {if $value9[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value9[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value9[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value9[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key9]}
        </td>
   </tr>
   {/foreach}
<tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
   <tr>
      <td colspan="{$socot}" style="border-top:1px solid #000;border-right: 1px solid #000"><b>TT mua sắm TCS và TT,TVTC</b></td>
  </tr>
  
    {foreach $count_arr10 as $key10 => $value10}
    <tr>
        <td class="text-left"  style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">{$j++}</td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key10]}
        </td>
        {$a=1}{$b=1}{$c=1}
        {foreach $arr_thang as $thang}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;{if $thang == $thangkt}border-right: 1px solid #000{/if}">
            {if isset($value10[$thang][0])}
            {if $value10[$thang][0]['danhgia_gd'] == 8}<b>Hoàn thành xuất sắc tiêu biểu</b> (Lần: {$a++}){/if}
            {if $value10[$thang][0]['danhgia_gd'] == 6}<b>Hoàn thành xuất sắc nhiệm vụ</b> (Lần: {$b++}){/if}
            {if $value10[$thang][0]['danhgia_gd'] == 5}Hoàn thành tốt nhiệm vụ (Lần: {$c++}){/if}
            {if $value10[$thang][0]['danhgia_gd'] == 4}Hoàn thành nhiệm vụ{/if}
            {if $value10[$thang][0]['danhgia_gd'] == 2}Không hoàn thành nhiệm vụ{/if}
            {/if}
        </td>
        {/foreach}
        {$a=$a-1}{$b=$b-1}{$c=$c-1}
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$a}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$b}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000; padding-left: 7px;"><b>{$c}</b>
        </td>
        <td style="border-top:1px solid #000; border-left: 1px solid #000;border-right: 1px solid #000; padding-left: 7px;">
            {$tencanbo[$key10]}
        </td>
   </tr>
   {/foreach}
   <tr>
      <td colspan="{$socot}"  style="border-top:1px solid #000;"></td>
    </tr>
</table>



          

