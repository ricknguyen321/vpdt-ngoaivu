<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">  
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    {if $iQuyenHan_DHNB == 3 or $iQuyenHan_DHNB >= 6} 
                    Thêm mới kế hoạch tuần
                    {else}
                        Kế hoạch công việc tuần {$week}
                    {/if}
                </span>
            </div>
            <div class="col-md-7 sl3">
                <span class="4 font" style="font-weight: 550">
                    Tuần công tác: 
                </span>
                <label class="8 sl2">
                    <select name="tuan" style="" class="form-control select2" onchange="this.form.submit()">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </label>
            </div>
            <div class="col-md-1">
                <span class="font">
                    Biểu số 1
                </span>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            {if $iQuyenHan_DHNB == 3 or $iQuyenHan_DHNB >= 6} 
                <div class="row">
                    <div class="col-sm-12"><span style="color: red; font-size: 18px ">Lưu ý: để nhập kế hoạch công tác tuần, chọn "<b>tuần công tác</b>" sau đó nhập dữ liệu.</span></div>
                </div>   
                <div class="row">
                    <div class="col-sm-12">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-sm-2">Nội dung công việc <span style="color: red">*</span></div>
                        <div class="col-sm-10">
                            <textarea name="kh_noidung" class="form-control" rows="3" style="border: 1px solid #3c8dbc;font-size: 14px" placeholder="Nhập nội dung công việc">{($giatri)?$giatri[0]['kh_noidung']:''}</textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-sm-5">Cán bộ thực hiện <span style="color: red">*</span></div>
                        <div class="col-sm-7">
                            {if $iQuyenHan_DHNB == 8}
                            <select name="canbo_id" required class="form-control" style="width:100%;border:1px solid #3c8dbc;" >
                                {foreach $canbo as $cb}
                                    {if $cb.PK_iMaCB == $PK_iMaCB}
                                        <option value="{$cb.PK_iMaCB}" {($giatri)?($giatri[0]["canbo_id"]==$cb.PK_iMaCB)?"selected":"":""}> {$cb.sHoTen}</option>
                                        }
                                    {/if}
                                {/foreach}
                            {else}
                            <select name="canbo_id" required class="form-control select2" style="width:100%;border:1px solid #3c8dbc;" >
                            <option value="">-- Chọn Cán Bộ --</option>
                                {foreach $canbo as $cb}
                                {if $cb.iQuyenHan_DHNB !=3 and $cb.iQuyenHan_DHNB !=6}
                                    <option value="{$cb.PK_iMaCB}" {($giatri)?($giatri[0]["canbo_id"]==$cb.PK_iMaCB)?"selected":"":""}> {$cb.sHoTen}</option>
                                {/if}
                                {/foreach}
                            {/if}
                            </select>
                        </div>
                        {if $iQuyenHan_DHNB < 8 || $iQuyenHan_DHNB == 11}
                        <div class="col-sm-5" style="padding-top: 3px;">
                            Cán bộ phối hợp:
                        </div> 
                        <div class="col-sm-7" style="padding-top: 3px;">
                            <input type="button" name="themphoihop"  value="Chọn phối hợp" data-toggle="modal" data-target="#themphoihop" class="btn btn-primary btn-sm">&nbsp;&nbsp;                
                        </div>
                        <input type="hidden" name="chuyenvien_ph" value="{($giatri)?$giatri[0]['phoihop']:''}"> 
                        {/if}
                        {if $iQuyenHan_DHNB == 8 && $FK_iMaPhongHD != 12 }
                        <div class="col-sm-5" style="padding-top: 3px;">
                            LĐ phòng phụ trách:<span style="color: red">*</span>
                        </div> 
                        <div class="col-sm-7" style="padding-top: 3px;">
                            <select name="lanhdao_id" required class="form-control" style="width:100%;border:1px solid #3c8dbc;" required="" >
                                <option value="">-- Chọn Cán Bộ --</option>
                                {foreach $canbo as $cb}
                                    {if $cb.iQuyenHan_DHNB == 7 || $cb.iQuyenHan_DHNB == 6 || $cb.iQuyenHan_DHNB == 3}
                                        <option value="{$cb.PK_iMaCB}" {($giatri)?($giatri[0]["lanhdao_id"]==$cb.PK_iMaCB)?"selected":"":""}>{$cb.sHoTen}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                        {/if}
                        {if $iQuyenHan_DHNB == 8 && $FK_iMaPhongHD == 12 }
                        <div class="col-sm-5" style="padding-top: 3px;">
                            Trưởng phòng phê duyệt:<span style="color: red">*</span>
                        </div> 
                        <div class="col-sm-7" style="padding-top: 3px;">
                            <select name="lanhdao_id" required class="form-control" style="width:100%;border:1px solid #3c8dbc;" required="" >
                                <option value="">-- Chọn Cán Bộ --</option>
                                {foreach $canbo as $cb}
                                    {if $cb.iQuyenHan_DHNB == 11}
                                        <option value="{$cb.PK_iMaCB}">{$cb.sHoTen}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                        {/if}
                    </div>  
                </div>
                <div class="row" style="height: 7px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-1">Hạn xử lý </div>
                        <div class="col-sm-2" style="padding-left: 42px;">
                           <input type="text" class="form-control datepic datemask" id="ngay_han" value="{if !empty($giatri) && $giatri[0]['ngay_han'] >'2017-01-01'}{date_select($giatri[0]['ngay_han'])}{/if}" placeholder="dd/mm/yyyy" name="ngay_han" style="border: 1px solid #3c8dbc;" >
                        </div>
                        <div class="col-sm-2">
                           <input type="radio" class="minimal" name="loai_kh" value="2" style="border: 1px solid #3c8dbc;" {if empty($giatri) ||  $giatri[0]['loai_kh'] == 2} checked {/if} >  &nbsp;&nbsp;<b>Nhiệm vụ đột xuất Lãnh đạo giao</b>
                        </div>
                        <div class="col-sm-3">
                           <input type="radio" class="minimal" name="loai_kh" value="3" style="border: 1px solid #3c8dbc;" {if !empty($giatri) &&  $giatri[0]['loai_kh'] == 3} checked {/if} >  &nbsp;&nbsp;<label>Nhiệm vụ thường xuyên </label><i>(Công việc có tính chất lặp lại theo tuần)</i>
                        </div>
                        <div class="col-sm-3">
                            <input type="radio" class="minimal" name="loai_kh" value="5" style="border: 1px solid #3c8dbc;" {if !empty($giatri) &&  $giatri[0]['loai_kh'] == 5} checked {/if} >  &nbsp;&nbsp;<label>Công việc khác</label><i> (Công đoàn,...)</i>
                        </div>
                        <div class="col-sm-1">
                            <input type="hidden" name="kehoach_ma" value="{$kehoach_ma}">
                            <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary btn-sm">Lưu lại</button>
                        </div>
                    </div> 
                </div>
                </form>
                {/if}
                <div class="row" style="height: 12px">

                    <div class="modal fade" id="themphoihop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header" style="padding: 8px;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1">Chọn cán bộ phối hợp</h4>
                          </div>
                          <div class="modal-body">
                              <div class="col-md-12">
                                    {foreach $canbo as $cb}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="checkbox" name="chuyenvien" value="{$cb.PK_iMaCB}"> {$cb.sHoTen} 
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ghilai()">Ghi lại</button>
                            <!-- <button type="button" class="btn btn-primary">Lưu lại</button> -->
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="modal fade" id="chuahoanthanh" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1"></h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" value="0" name="viecthuongxuyen">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="canbo_id">
                                    <input type="text" class="hide" name="ngay_han">
                                    <input type="text" class="hide" name="tencanbo">
                                    <input type="text" class="hide" name="trangthai_active">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Nội dung:</label>
                                    <textarea name="lydo" class="form-control" rows="4" style="font-size:15px"></textarea>
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Chọn file:</label>
                                    <input type="file" class="form-control" name="files[]">
                                  </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luuchuahoanthanh" value="Chưa hoàn thành" class="btn btn-primary btn-sm">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                      </div>
                    </div>

                    <!-- giao chuyên viên -->

                        <div class="modal fade" id="giaochuyenvien" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1"></h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="list_cvph" value="">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Nội dung:</label>
                                    <textarea name="pp_chidao" class="form-control" rows="4" style="font-size:15px"></textarea>
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Chọn chuyên viên:</label>
                                    <select name="pp_canbo_id" required class="form-control select2" style="width:100%;border:1px solid #3c8dbc;" >
                                        <option value="">-- Chọn Cán Bộ --</option>
                                            {foreach $canbo as $cb}
                                            {if $cb.iQuyenHan_DHNB !=3 and $cb.iQuyenHan_DHNB !=6 and $cb.iQuyenHan_DHNB !=7}
                                                <option value="{$cb.PK_iMaCB}"> {$cb.sHoTen}</option>
                                            {/if}
                                            {/foreach}
                                     
                                        </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Chọn chuyên viên phối hợp:</label>
                                <div class="col-md-12">
                                  {foreach $canbo as $cb}
                                  {if $cb.iQuyenHan_DHNB !=3 and $cb.iQuyenHan_DHNB !=6 and $cb.iQuyenHan_DHNB !=7}
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <input type="checkbox" name="pp_chuyenvien_ph" value="{$cb.PK_iMaCB}" onclick="ghilaicvph();"> {$cb.sHoTen} 
                                            </div>
                                        </div>
                                        {/if}
                                    {/foreach}
                                </div>
                                </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luuppchidao" value="Chưa hoàn thành" class="btn btn-primary btn-sm">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                      </div>
                    </div>
                </div>
                
                <div class="modal fade" id="TP_danhgia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title1" >Đánh giá kết quả công việc</h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="canbo_id">
                                    <input type="text" class="hide" name="tencanbo">
                                    <input type="text" class="hide" name="trangthai_active">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Đánh giá:</label>
                                    <textarea name="lydo" class="form-control" rows="4" style="font-size:15px"></textarea>
                                  </div>
                                  <div class="form-group">
                                        <input type="radio" name="chatluong" value="1" checked="checked"> Chất lượng đảm bảo &nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="chatluong" value="2"> Không đảm bảo chất lượng <br/>
                                        <input type="radio" name="chatluong" value="3"> Có tính sáng tạo, đổi mới, hiệu quả &nbsp;
                                  </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luutruongphongdanhgia" value="Chưa hoàn thành" class="btn btn-primary btn-sm">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                      </div>
                </div>

                <div class="modal fade" id="TP_danhgia1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title1" >Công việc chưa hoàn thành</h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="canbo_id">
                                    <input type="text" class="hide" name="tencanbo">
                                    <input type="text" class="hide" name="trangthai_active">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Lý do:</label>
                                    <textarea name="lydo" class="form-control" rows="4" style="font-size:15px"></textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="luutruongphongdanhgia" value="Chưa hoàn thành" class="btn btn-primary btn-sm">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                    </div>
                </div>

                <div class="modal fade" id="TP_duyet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title2" ></h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="tencanbo">
                                    <input type="text" class="hide" name="trangthai_active">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Lý do:</label>
                                    <textarea name="lydo" class="form-control" rows="4" style="font-size:15px"></textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="tpduyet" value="Chưa hoàn thành" class="btn btn-primary btn-sm">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                    </div>
                </div>


<!-- trưởng phòng gia hạn giải quyết -->
<div class="modal fade" id="TP_rahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title2" >Gia hạn cho kế hoạch công tác</h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="trangthai">
                                    <input type="text" class="hide" name="tencanbo">
                                    <input type="text" class="hide" name="trangthai_active">
                                  </div>
                                  <div class="form-group">
                                        <label for="message-text" class="control-label">Ngày hạn mới:</label> &nbsp;&nbsp;&nbsp;
                                        <input type="text" name="ngayhanmoi" value="" class="form-control datepic datemask"> 
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Lý do:</label>
                                    <textarea name="lydorahan" class="form-control" rows="4" style="font-size:15px" ></textarea>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <span style="color: red">
                                        &nbsp;&nbsp;&nbsp;- Gia hạn: người dùng nhập hạn mới, ghi lý do nếu có <br>
                                        &nbsp;&nbsp;&nbsp;- Không gia hạn: người dùng bỏ trống hạn, ghi lý do (để cán bộ cấp dưới nắm được lý do)
                                    </span> 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="tpduyetrahan" value="Chưa hoàn thành" class="btn btn-primary btn-sm">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                    </div>
                </div>


<!-- nhận xét công việc của phó phòng -->
                <div class="modal fade" id="PP_duyet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                      <div class="modal-dialog" role="document">
                        <form action="" method="post" enctype='multipart/form-data'>
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title2" >Nhận xét kết quả công việc của chuyên viên</h4>
                          </div>
                                <div class="modal-body">
                                
                                  <div class="form-group">
                                    <input type="text" class="hide" name="kehoach_id">
                                    <input type="text" class="hide" name="tencanbo">
                                    <input type="text" class="hide" name="trangthai_active">
                                  </div>
                                  <div class="form-group">
                                    <label for="message-text" class="control-label">Nội dung:</label>
                                    <textarea name="ketluan_pp" class="form-control" rows="4" style="font-size:15px" ></textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
                                    <button type="submit" name="ppduyet" value="Lưu nhận xét" class="btn btn-primary btn-sm">Lưu lại</button>
                                </div>
                          
                        </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" style="font-size: 14px;font-weight: bold;">Danh mục kế hoạch công việc-kết quả giải quyết tuần:
                        {foreach $arr as $key => $value}
                            {$giatri = $key+1}
                            {if $week == $giatri} {$week} ({$value}) {/if}                            
                        {/foreach}
                    </div>
                </div>
                <form method="post">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center" rowspan="2">STT</th>
                                    {if $iQuyenHan_DHNB < 7}
                                    <th width="8%" class="text-center" rowspan="2">Cán bộ thực hiện</th>
                                    {/if}
                                    <th width="8%" class="text-center" rowspan="2">Trách nhiệm giải quyết</th>
                                    <th width="8%" class="text-center" rowspan="2">Ngày nhận</th>
                                    <th width="6%" class="text-center" rowspan="2">Số ký hiệu</th>
                                    <th width="" class="text-center" rowspan="2">Nội dung</th>
                                    <th width="8%" class="text-center" rowspan="2">Hạn xử lý</th>
                                    <th width="15%" class="text-center" colspan="3">Kết quả thực hiện</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" >Hoàn Thành</th>
                                    <th width="5%" class="text-center">Đang thực hiện</th>
                                    <th width="5%" class="text-center">Tình trạng</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>I</th>
                                <th colspan="9">NHIỆM VỤ LÃNH ĐẠO GIAO, NHIỆM VỤ THƯỜNG XUYÊN, NHIỆM VỤ KHÁC
                                {if $iQuyenHan_DHNB <= 7} 
                                    <input type="hidden" name="tuanpost" value="{$week}">
                                {/if}
                                </th>
                            </tr>
                            <tr>
                                <th colspan="10">
                                    &nbsp;&nbsp;&nbsp;
                                    <span style="color: blue">Chọn cán bộ </span>
                                    <select id="tencanbo_id" name="tencanbo" onchange="this.form.submit()">
                                        <option value="0"> --chọn cán bộ-- </option>
                                        {foreach $canbo as $cb}
                                        <option value="{$cb.PK_iMaCB}"{if $idcbloc == $cb.PK_iMaCB}selected{/if}>{$cb.sHoTen} </option>
                                        {/foreach}
                                    </select>
{if $iQuyenHan_DHNB <= 6}
                                    &nbsp;&nbsp;&nbsp;
                                    <span style="color: red">Chọn Duyệt </span>
                                    <select id="trangthaitp" name="trangthaitp" onchange="this.form.submit()">
                                        <option value="0"> -- tất cả -- </option>
                                        
                                        <option value="1" {if $active_tt == 1}selected{/if} > -- Duyệt kết quả -- </option>
                                        
                                    </select> &nbsp;&nbsp;&nbsp;

                                    <input type="button" name="button_kehoach" class="btn btn-danger btn-sm" value="Duyệt tất cả kế hoạch"></th>
{/if}                                     
                            </tr>
                            {$i = 1}
                            {foreach $list_kh as $kh}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                {if $iQuyenHan_DHNB < 7}
                                <td width="8%" class="text-center">
                                    {if $kh.lanhdao_id != $kh.user_input && $kh.user_input != $kh.canbo_id}
                                    <b>Trưởng đơn vị giao việc</b><br>
                                    {/if}
                                    {foreach $canbo as $cb}
                                        {if $kh.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}
                                    {if $kh.canbo_id != $kh.user_input}
                                        {foreach $canbo as $cb}
                                            {if $kh.lanhdao_id == $cb.PK_iMaCB}
                                                <span style="color: blue"><i>(LĐ chỉ đạo {$cb.sHoTen})</i></span>
                                            {/if}
                                        {/foreach}
                                    {else}
                                        <span style="color: blue">(Cán bộ tự xây dựng kế hoạch)</span>
                                    {/if}

                                </td>
                                {/if}
                                <td>
                                    {if $kh.loai_kh ==5}
                                        Nhiệm vụ khác
                                    {/if}
                                    {if $kh.loai_kh ==3 or $kh.loai_kh ==4}
                                        Nhiệm vụ thường xuyên
                                    {/if}
                                    {if $kh.loai_kh ==2 && $iQuyenHan_DHNB == 8}
                                       {if $kh.canbo_id == $PK_iMaCB} Lãnh đạo giao {else} Phối hợp giải quyết {/if}
                                    {/if}
                                    {if $kh.loai_kh ==2 && $iQuyenHan_DHNB < 8}
                                       Lãnh đạo giao
                                    {/if}
                                    
                                    {if $iQuyenHan_DHNB >= 7}
                                    {if $kh.lanhdao_id != $kh.user_input && $kh.user_input != $kh.canbo_id}
                                    <b>Trưởng đơn vị giao việc</b><br>
                                    {/if}
                                    {if $kh.canbo_id != $kh.user_input}
                                        {foreach $canbo as $cb}
                                            {if $kh.lanhdao_id == $cb.PK_iMaCB}
                                                <span style="color: blue"><i>(LĐ chỉ đạo {$cb.sHoTen})</i></span>
                                            {/if}
                                        {/foreach}
                                    {else}
                                        {if $iQuyenHan_DHNB == 7 || $iQuyenHan_DHNB == 11 }
                                        <span style="color: blue"><br> (Cán bộ xây dựng kế hoạch:
                                            {foreach $canbo as $cb}
                                                {if $kh.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                            {/foreach})
                                        </span>
                                        {else}
                                        <span style="color: blue">(Cán bộ xây dựng kế hoạch)</span>
                                        {/if}
                                    {/if}
                                    {/if}
                                </td>
                                <td>{date_select($kh.ngay_nhan)}</td>
                                <td class="text-center">{$kh.vanban_skh}</td>
                                <td class="text-left" align="text-left">
                                    {if $PK_iMaCB == $kh.user_input && (($kh.active < 2 && $iQuyenHan_DHNB == 8)||($kh.active == 2 && $iQuyenHan_DHNB == 11)||($kh.active == 2 && $iQuyenHan_DHNB == 7) ||($kh.active == 3 && ($iQuyenHan_DHNB == 6 || $iQuyenHan_DHNB == 3)))}
                                    <a href="kehoachcongtac?id={$kh.kh_id}">{$kh.kh_noidung} - {date_time($kh.date_nhap)}
                                    {if $kh.pp_chidao != ''}<br>
                                    <b>Chỉ đạo của phó trưởng đơn vị:</b> {$kh.pp_chidao} - ({date_time($kh.pp_ngaygiao)})
                                    {/if}

                                    (Người nhận: {$dscanbo[$kh.canbo_id]})</a>
                                    <br><br><button type="submit" name="xoabanghi" onclick=" return confirm('Bạn có chắc chắn xóa công việc')" value="{$kh.kh_id}"> Xóa Công việc</button>
                                    {else}
                                    {$kh.kh_noidung} - {date_time($kh.date_nhap)}
                                    {if $kh.pp_chidao != ''}<br>
                                    <b>Chỉ đạo của phó trưởng đơn vị:</b> {$kh.pp_chidao} - ({date_time($kh.pp_ngaygiao)})
                                    {/if}
                                    (Người nhận: {$dscanbo[$kh.canbo_id]})
                                    {/if}

                                    {if $kh.ykien_pp !=''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                        <br><b>Ý kiến của {foreach $canbo as $cb}
                                                {if $kh.lanhdao_id == $cb.PK_iMaCB}{if $cb.iQuyenHan_DHNB ==7}phó phòng {else}trưởng phòng {/if} {$cb.sHoTen}{/if}
                                            {/foreach}:</b><br> <i>{$kh.ykien_pp} ({date_time($kh.ngay_ykien)})</i>
                                    {/if}
                                    {if $kh.ykien_tp !=''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                        <br><b>Ý kiến của lãnh đạo đơn vị {foreach $canbo as $cb}
                                                {if $truongdonvi == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                            {/foreach}:</b><br> <i>{$kh.ykien_tp} ({date_time($kh.ngay_danhgia)})</i>
                                    {/if}
                                    
                                    {if $kh.khokhan_vuongmac != ''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    <br>
                                    <label>Lý do chưa hoàn thành: </label>
                                    <span style="color: red">{$kh.khokhan_vuongmac} - {date_time($kh.ngay_hoanthanh)}</span>

                                        {if $kh.active == 3 && ($iQuyenHan_DHNB == 6 || $iQuyenHan_DHNB == 3)}

                                        <br>
                                            <input type="button" name="rahan" id="{$kh.kh_id}" value="Gia hạn" data-toggle="modal" class="btn btn-primary btn-xs" data-target="#TP_rahan"> 

                                        {/if}
                                    {/if}

                                    {if $kh.lydorahan != ''}
                                        <br><span style="color: blue">{$kh.lydorahan}</span>
                                    {/if}
                                    {if $kh.ngayhanmoi > '2018-01-01'}
                                        <br>Ngày gia hạn: <span style="color: blue">
                                        {date_select($kh.ngayhanmoi)}</span>
                                    {/if}
                                    {if $kh.lydorahan != '' || $kh.ngayhanmoi > '2018-01-01'}
                                        <br>Thời điểm gia hạn: <span style="color: blue">{date_time($kh.ngayrahanmoi)}</span>
                                    
                                    {/if}

                                    {if $kh.ket_qua_hoanthanh != ''}
                                    <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    <br>
                                    <label>Kết quả hoàn thành: </label>
                                    <span style="color: blue">{$kh.ket_qua_hoanthanh} - {date_time($kh.ngay_hoanthanh)}</span>
                                    {/if} 
                                    {if isset($kh.filename)}<br>
                                    <label>Tệp tin đính kèm: </label>
                                    {foreach $kh.filename as $file}
                                        {if $file !=''}<a href="kehoach_2019/{$file}"> [File] </a>{/if}
                                    {/foreach}
                                    {/if}
{$kiemtra=1}
{if isset($kh.file_phoihop)}
{if isset($kh.file_phoihop.0.canbo_id)}<br><i> Danh sách phối hợp:</i>{/if}
    {foreach $kh.file_phoihop as $phoihop}    
        <br> - <b>{$array_cb[$phoihop.canbo_id]}: </b>{$phoihop.lydo} &nbsp;&nbsp; 
        {if $phoihop.file_upload !=''}<a href="kehoach_2018/{$phoihop.file_upload}">[File] </a>{/if}

        {if $phoihop.canbo_id == $PK_iMaCB && $phoihop.active == 1 && $phoihop.chutri == 2}{$kiemtra=2}{/if}
    {/foreach}
{/if}
                                    {if $kh.ketluan_pp != ''}<hr style="margin-bottom: 6px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    <b>Nhận xét của {foreach $canbo as $cb}
                                                {if $kh.lanhdao_id == $cb.PK_iMaCB}{if $cb.iQuyenHan_DHNB ==7}phó phòng {else}trưởng phòng {/if} {$cb.sHoTen}{/if}
                                            {/foreach}:</b><br> 
                                    <span style="color: blue;">{$kh.ketluan_pp}</span>
                                    {/if}

                                    {if $kh.danh_gia != '' or ($kh.danh_gia == '' and $kh.active == 5) }<hr style="margin-bottom: 6px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                    {if  $kh.active == 3 &&  $kh.chatluong == 0}
                                    <label style="color: red">Kết luật việc chưa hoàn thành của trưởng đơn vị: </label>
                                    {else}
                                    <label>Kết luận của trưởng đơn vị: </label>
                                    {/if}
                                    <span style="color: blue">{$kh.danh_gia} - {date_time($kh.ngay_danhgia)}</span>
                                    {if $kh.sangtao==2} 
                                        <br><span style="color: red"><b>Có tính sáng tạo</b></span>{/if}
                                    {/if}
                                    {if $kh.chatluong==2} 
                                        <br><span style="color: red"><b>Chất lượng chưa đảm bảo</b></span>
                                    {/if}

                                    <div class="danhgia">
                                        <br />
                                        <!-- cán bộ chủ trì hoàn thành công việc -->
                                        {if $PK_iMaCB == $kh.canbo_id && ($kh.active ==3 or $kh.active ==4) && $kh.vanban_id =="" && $kiemtratuan == 2 && ($thu_hientai != 'Saturday')}
                                        {if $thu_hientai != 'Sunday'}                                      
                                            <input type="hidden" name="han_xl_{$kh.kh_id}" value="{$kh.ngay_han}">
                                            <input type="hidden" name="{$kh.kh_id}" value="{$kh.canbo_id}">
                                            {if $kh.loai_kh == 3}
                                                <input type="checkbox" name="loai_kh" class="ketthucviecthuongxuyet" id="check_{$kh.kh_id}" value="4"> <label>Kết thúc nhiệm vụ thường xuyên</label><i> (Tích chọn nút này nhiệm vụ thường xuyên sẽ kết thúc và không tự động chuyển sang tuần mới)</i><br>
                                            {/if}                                    

                                            <input type="button" name="hoanthanh" id="{$kh.kh_id}" value="Hoàn thành" data-toggle="modal" class="btn btn-primary btn-xs" data-target="#chuahoanthanh">&nbsp;&nbsp;
                                            <input type="button" name="chuahoanthanh" id="{$kh.kh_id}" data-toggle="modal" class="btn btn-warning btn-xs" data-target="#chuahoanthanh" value="Chưa hoàn thành">

                                            {if $kh.lanhdao_id == $kh.user_input && $kh.user_input != $PK_iMaCB && $iQuyenHan_DHNB == 7}
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="button" name="giaochuyenvien" id="{$kh.kh_id}" data-toggle="modal" class="btn btn-primary btn-xs" data-target="#giaochuyenvien" value="Giao chuyên viên">
                                            {/if}                                        
                                        {/if}
                                        {/if} 
                                        <!-- cán bộ phối hợp hoàn thành công việc -->
                                        {if $PK_iMaCB != $kh.lanhdao_id && $PK_iMaCB != $kh.canbo_id && ($kh.active ==3 or $kh.active ==4) && $kh.vanban_id =="" && $kiemtratuan == 2 && $iQuyenHan_DHNB > 7}                                        
                                            <input type="hidden" name="han_xl_{$kh.kh_id}" value="{$kh.ngay_han}">
                                            <input type="hidden" name="{$kh.kh_id}" value="{$kh.canbo_id}">
                                            {if $kh.loai_kh == 3}
                                                <input type="checkbox" name="loai_kh" class="ketthucviecthuongxuyet" id="check_{$kh.kh_id}" value="4"> <label>Kết thúc nhiệm vụ thường xuyên</label><i> (Tích chọn nút này nhiệm vụ thường xuyên sẽ kết thúc và không tự động chuyển sang tuần mới)</i><br>
                                            {/if}                                        
                                            <input type="button" name="hoanthanh" id="{$kh.kh_id}" value="Hoàn thành" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#chuahoanthanh">&nbsp;&nbsp;
                                            <input type="button" name="chuahoanthanh" class="btn btn-primary btn-xs" id="{$kh.kh_id}" data-toggle="modal" data-target="#chuahoanthanh" value="Chưa hoàn thành">                                        
                                        {/if}
                                        <!-- nhận xét của phó phòng -->
                                        {if  ($iQuyenHan_DHNB == 7 || $iQuyenHan_DHNB == 11) && $kh.active == 4 && $kh.vanban_id =="" && $kh.canbo_id != $PK_iMaCB}
                                        <br/>   
                                        <input type="button" name="duyetviec_pp" id="{$kh.kh_id}" quyen ="{$iQuyenHan_DHNB}" data-toggle="modal" data-target="#PP_duyet" value="Nhận xét kết quả công việc">
                                        {/if}

                                        {if $iQuyenHan_DHNB < 7 && $kh.active == 4 && $kh.vanban_id =="" && $week_hientai == $week}
                                        <br/>
    
                                        <input type="button" name="danhgia" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_danhgia" class="btn btn-primary btn-xs" value="Hoàn thành">&nbsp;&nbsp;
                                        <input type="button" name="danhgia1" id="{$kh.kh_id}" data-toggle="modal" data-target="#TP_danhgia1" class="btn btn-warning btn-xs" value="Chưa hoàn thành">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" name="kehoachcongtac_id" value="{$kh.kh_id}_{$kh.active}" onclick="concatkehoach()"> <b>chọn duyệt</b>
                                        {/if}

                                        {if ($iQuyenHan_DHNB == 7 || $iQuyenHan_DHNB == 11) && $kh.active == 0 && $kh.vanban_id =="" && $kh.ykien_pp == ""}
                                            <input type="button" name="khongduyet" id="{$kh.kh_id}" data-toggle="modal" class="btn btn-warning btn-xs" data-target="#TP_duyet" value="Không duyệt">&nbsp;&nbsp;
                                            <input type="button" name="duyetviec" id="{$kh.kh_id}" quyen ="{$iQuyenHan_DHNB}" data-toggle="modal" class="btn btn-primary btn-xs" data-target="#TP_duyet" value="Duyệt">
                                        {/if}
                                        {if ($iQuyenHan_DHNB == 6 ||  $iQuyenHan_DHNB == 3)  && ($kh.active == 1 or $kh.active == 2) && $kh.vanban_id =="" && $week_hientai == $week}
                                            <input type="button" name="khongduyet" id="{$kh.kh_id}"  data-toggle="modal" class="btn btn-warning btn-xs" data-target="#TP_duyet" value="Không duyệt">&nbsp;&nbsp;
                                            <input type="button" name="duyetviec" id="{$kh.kh_id}" quyen ="{$iQuyenHan_DHNB}"  data-toggle="modal" class="btn btn-primary btn-xs"  data-target="#TP_duyet" value="Duyệt">

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" name="kehoachcongtac_id" value="{$kh.kh_id}_{$kh.active}" onclick="concatkehoach()"> <b>chọn duyệt</b>
                                        {/if}

                                    </div>
                                    <input type="hidden" id="string_kh_id" name="string_kh_id" value="">
                                    <input type="hidden" id="string_active" name="string_active" value="">
                                </td>
                                <td class="text-center">{if $kh.ngay_han >'2017-01-01'}{date_select($kh.ngay_han)}{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh.active == 5}X{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh.active < 5}X{/if}</td>
                                <td class="text-center" style="color: green"> 
                                    {if ($kh.ngay_han >= $date_for_friday && $kh.active < 5) || ($kh.ngay_han >= date_insert($kh.ngay_hoanthanh)  && $kh.active == 5) || $kh.ngay_han < '2017-01-01' }Trong hạn{/if}

                                    {if $kh.ngay_han < $date_for_friday && $kh.active < 5 && $kh.ngay_han >'2017-01-01'}<span style="color: red">Quá hạn </span>{/if}

                                </td>
                            </tr>
                            {/foreach}
{if $iQuyenHan_DHNB <= 6}                            
                            <tr>
                                <th colspan="10"><input type="button" name="button_kehoach" class="btn btn-danger btn-sm" value="Duyệt tất cả kế hoạch"></th>
                            </tr>
{/if}                            
                            <tr>
                                <th>II</th>
                                <th colspan="9">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ</th>
                            </tr>
                            <tr>
                                <th>A</th>
                                <th colspan="9">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ CÓ YÊU CẦU SẢN PHẨM ĐẦU RA</th>
                            </tr>
                            {foreach $list_kh1 as $kh1}
                            <tr>
                                <td class="text-center">{$i++}</td>
                                {if $iQuyenHan_DHNB < 7}
                                <td width="8%" class="text-center">
                                    {foreach $canbo as $cb}
                                        {if $kh1.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}
                                </td>
                                {/if}
                                <td>
                                    {if $kh1.thuc_hien == 1}Chỉ Đạo{/if}
                                    {if $kh1.thuc_hien == 2}Phối hợp{/if}
                                    {if $kh1.thuc_hien == 3}Giải quyết chính{/if}
                                </td>
                                <td>{date_select($kh1.ngay_nhan)}</td>
                                <td class="text-center">
                                    {$kh1.vanban_skh}<br>
                                    <span style="color: red">(số đến: {laySoDen('PK_iMaVBDen',$kh1.vanban_id,'tbl_vanbanden')})</span>
                                </td>
                                <td class="text-left" align="text-left">
                                    {if $kh1.active >= 1  && $kh1.vanban_skh != ''}
                                        {if $kh1.thuc_hien ==3 && $kh1.trangthai_vbd == 1}
                                        <a href="giaymoichuyenvienchutrixuly/{$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==2 && $kh1.trangthai_vbd == 1}
                                        <a href="giaymoichuyenvienxuly/{$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==1 && $kh1.trangthai_vbd == 1}
                                        <a href="quytrinhgiaymoi/{$kh1.vanban_id}">
                                        {/if}

                                        {if $kh1.thuc_hien ==3 && $kh1.trangthai_vbd == 0}
                                        <a href="chuyenvienchutrixuly?vbd={$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==2 && $kh1.trangthai_vbd == 0}
                                        <a href="chuyenvienxuly?vbd={$kh1.vanban_id}">
                                        {/if}
                                        {if $kh1.thuc_hien ==1 && $kh1.trangthai_vbd == 0}
                                        <a href="quatrinhxuly/{$kh1.vanban_id}">
                                        {/if}
                                    {$kh1.kh_noidung}</a>
                                    {else}
                                    {$kh1.kh_noidung}
                                    {/if}
                                </td>
                                <td class="text-center">{if $kh1.ngay_han >'2017-01-01'}{date_select($kh1.ngay_han)}{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh1.active==5}X{/if}</td>
                                <td class="text-center" style="color: blue;font-weight: bold;">{if $kh1.active<5}X{/if}</td>
                                <td class="text-center" style="color: green"> 
                                    {if ($kh1.ngay_han >= $date_for_friday && $kh1.active < 5) || ($kh1.ngay_han >= date_insert($kh1.ngay_hoanthanh)  && $kh1.active == 5) || $kh1.ngay_han < '2017-01-01' }Trong hạn{/if}

                                    {if $kh1.ngay_han < $date_for_friday && $kh1.active < 5 && $kh1.ngay_han >'2017-01-01'}<span style="color: red">Quá hạn </span>{/if}
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>

                        <table id="" class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                 <th colspan="4">B.  CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ KHÔNG CÓ YÊU CẦU SẢN PHẨM ĐẦU RA</th>
                            </tr>
                            <tr>
                                <td class="text-center" style="font-weight:700;font-size:14px  ">VB Chủ Trì Hoàn Thành Trong Hạn</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">VB Chủ Trì Hoàn Thành Quá Hạn</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">VB Phối Hợp Hoàn Thành Trong Hạn</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">VB Phối Hợp Hoàn Thành Quá Hạn</td>
                            </tr>
                            <tr>
                                <td class="text-center" style="font-weight:700;font-size:14px  ">
                                    {if $tong_vbct_dh >0}<a href="{$url}dsphongchutri?PK_iMaVBDen={$ds_idctdh}"> {/if}
                                        {$tong_vbct_dh}
                                    {if $tong_vbct_dh >0}</a>{/if}</td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">
                                    {if $tong_vbct_qh >0}<a href="{$url}dsphongchutri?PK_iMaVBDen={$ds_idctqh}"> {/if}
                                        {$tong_vbct_qh}
                                    {if $tong_vbct_qh >0}</a>{/if}
                                </td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">
                                    {if $tong_vbph_dh >0}<a href="{$url}dsphoihop?PK_iMaVBDen={$ds_idphdh}" title="Click chuột để xem chi tiết"> {/if}
                                        {$tong_vbph_dh}
                                    {if $tong_vbph_dh >0}</a>{/if}
                                </td>
                                <td class="text-center" style="font-weight:700;font-size:14px ">
                                    {if $tong_vbph_qh >0}<a href="{$url}dsphoihop?PK_iMaVBDen={$ds_idphqh}" title="Click chuột để xem chi tiết"> {/if}
                                        {$tong_vbph_qh}
                                    {if $tong_vbph_qh >0}</a>{/if}
                                </td>
                            </tr>                                                    
                        </tbody>
                        </table>
                    </div>
                </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 text-left" style="margin-bottom: 5px;">
                            {if $iQuyenHan_DHNB >= 7}
                            <a href="dskehoachcongtac/{$week}"><input type="button" class="btn btn-danger" style="font-weight: bold;" name="danhgiakehoachtuan" value="Biểu số 5: Tổng hợp kết quả chấm điểm tuần {$week} của phòng"></a>
                            {/if}
                        </div>
                        <div class="col-md-6 text-right" style="margin-bottom: 5px;">
                            {if $iQuyenHan_DHNB >= 7 && $kiemtratuan == 2}
                            <a href="xemkehoachcongtac/{$week}"><input type="button" class="btn btn-danger" style="font-weight: bold;" name="danhgiakehoachtuan" value="Biểu số 2: đánh giá chấm điểm tuần {$week}"></a>
                            {/if}
                            {if $iQuyenHan_DHNB < 7 && $kiemtratuan == 2 && $iQuyenHan_DHNB != 4 && $iQuyenHan_DHNB != 5 }
                            <a href="dskehoachcongtac/{$week}"><input type="button" class="btn btn-danger" style="font-weight: bold;" name="danhgiakehoachtuan" value="Biểu số 2: đánh giá chấm điểm tuần {$week}"></a>
                            {/if}
                        </div>
                </div>
            </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var url = window.location.href;
        $(document).on('change','.ketthucviecthuongxuyet',function(){
            var id = $(this).attr('id');
            var checked = $('#' + id).is(":checked");
            if (checked == 0) {
                $('input[name=viecthuongxuyen]').val('0');
            } else {
                $('input[name=viecthuongxuyen]').val('4');
            }
        });
        $(document).on('click','input[name=hoanthanh]',function(){
            var id = $(this).attr('id');
            var canbo = $('input[name='+id+']').val();
            var ngay_han = $('input[name=han_xl_'+id+']').val();
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=ngay_han]').val(ngay_han);
            $('input[name=trangthai]').val(1);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);
            $('.modal-title').text("Hoàn thành công việc");
        });

        $(document).on('click','input[name=chuahoanthanh]',function(){
            var id = $(this).attr('id');
            var canbo = $('input[name='+id+']').val();
            var ngay_han = $('input[name=han_xl_'+id+']').val();
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=ngay_han]').val(ngay_han);
            $('input[name=trangthai]').val(2);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);

            $('.modal-title').text("Lý do chưa hoàn thành công việc");
            
        });

        $(document).on('click','input[name=giaochuyenvien]',function(){
            var id = $(this).attr('id');
            var canbo = $('input[name='+id+']').val();
            var ngay_han = $('input[name=han_xl_'+id+']').val();
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=ngay_han]').val(ngay_han);
            $('input[name=trangthai]').val(2);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);

            $('.modal-title').text("Lý do chưa hoàn thành công việc");
            
        });

        $(document).on('click','input[name=danhgia]',function(){
            var id = $(this).attr('id');
            var chatluong = $('input[name=chatluong]:checked').val();
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=trangthai]').val(1);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);
        });

        $(document).on('click','input[name=danhgia1]',function(){
            var id = $(this).attr('id');
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=canbo_id]').val(canbo);
            $('input[name=trangthai]').val(2);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);
        });

         $(document).on('click','input[name=duyetviec]',function(){
            var id = $(this).attr('id');
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(1);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);
            $('.modal-title2').text("Duyệt đề xuất của cán bộ");
        });
         $(document).on('click','input[name=khongduyet]',function(){
            var id = $(this).attr('id');
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(2);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);
                $('.modal-title2').text("Không duyệt đề xuất của cán bộ");            
        });

         $(document).on('click','input[name=duyetviec_pp]',function(){
            var id = $(this).attr('id');
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(4);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);

        });

         $(document).on('click','input[name=rahan]',function(){
            var id = $(this).attr('id');
            var tencanbo = $( "#tencanbo_id" ).val();
            var trangthai_active = $( "#trangthaitp option:selected" ).val();
            $('input[name=kehoach_id]').val(id);
            $('input[name=trangthai]').val(2);
            $('input[name=tencanbo]').val(tencanbo);
            $('input[name=trangthai_active]').val(trangthai_active);
        });

        $(document).on('click','input[name=button_kehoach]',function(){
            var a = {};
            a.string_kh_id = $( "#string_kh_id" ).val();
            a.string_active = $( "#string_active" ).val();
            var url      = window.location.href;
            $.ajax({
              url: url,
              data: a,
              type: 'post',
              success: function() {
                //alert(data);
                location.reload();
              }
            });
            
        });

       
    });

    function ghilai(){ 
        var str_id ='';
        $.each($("input[name='chuyenvien']:checked"), function(){
            str_id = str_id + "["+$(this).val()+"],";
        });
         str_id = str_id.substring(0, str_id.length - 1);
        $('input[name=chuyenvien_ph]').val(str_id);
    }  

    
    function ghilaicvph(){ 
        var str_id ='';
        $.each($("input[name='pp_chuyenvien_ph']:checked"), function(){
            str_id = str_id + "["+$(this).val()+"],";
        });
         str_id = str_id.substring(0, str_id.length - 1);
        $('input[name=list_cvph]').val(str_id);
    } 

    
    function concatkehoach(){ 
        var str_id ='0';
        var str_active ='0';
        $.each($("input[name='kehoachcongtac_id']:checked"), function(){
            var string = $(this).val();
            var array = string.split("_");
            str_id = str_id + ","+array[0];
            str_active = str_active +',' + array[1];
        });
        $('input[name=string_kh_id]').val(str_id);
        $('input[name=string_active]').val(str_active);
    }
    
</script>
