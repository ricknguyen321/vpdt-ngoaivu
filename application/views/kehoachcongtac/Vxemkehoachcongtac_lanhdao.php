<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-4">
                <span class="font">
                    Chấm điểm kết quả kế hoạch công tác tháng: 
                </span>
                <span class="4" style="font-weight: 550">
                     {$month}
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn Tháng công tác: 
                </span>
                <label class="8 sl2">
                    <select name="month" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                </label>
            </div>
            <div class="col-md-4 sl3" style="padding-bottom: 3px;">
                {if !empty($list_diem)}
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
                {/if}
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3">STT</th>
                                    <th width="30%" class="text-center" rowspan="3">Nội dung</th>
                                    <th width="5%" class="text-center" rowspan="3">Tổng số việc của văn phòng điện tử</th>
                                    <th width="25%" class="text-center" colspan="6">Công việc đã hoàn thành trong tháng</th>
                                    <th width="10%" class="text-center" colspan="3">Công việc còn tồn cuối tháng</th>
                                    <th width="5%" class="text-center" rowspan="3"  style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Tỉ lệ hoàn thành (%)</th>
                                    <th width="5%" class="text-center" rowspan="3">Máy chấm tự động</th>
                                    <th width="5%" class="text-center" rowspan="3">Cá nhân tự chấm điểm</th>
                                    <th width="5%" class="text-center" rowspan="3">lãnh đạo chấm điểm</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2">Chất lượng</th>
                                    <th width="5%" class="text-center" rowspan="2" style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center">Trong hạn</th>
                                    <th width="5%" class="text-center">Quá hạn</th>
                                    <th width="5%" class="text-center">Đảm bảo</th>
                                    <th width="5%" class="text-center">Chưa đảm bảo</th>
                                    <th width="5%" class="text-center" >Trong hạn</th>
                                    <th width="5%" class="text-center" >Quá hạn</th>
                                </tr>
                            </thead>
                            <tbody>                                
                                <tr>
                                    <td class="text-center"><b>A</b></td>
                                    <td class="text-left"><b>Kết quả thực hiện nhiệm vụ theo kế hoạch công tác tuần được duyệt <br>(tối đa 60 điểm)</b></td>
                                    <td class="text-center">{$count_arr[0]['tong']}</td>
                                    <td class="text-center">{$count_arr[1]['tong']}</td>
                                    <td class="text-center">{$count_arr[2]['tong']}</td>
                                    <td class="text-center" style="color: red">{$count_arr[3]['tong']}</td>
                                    <td class="text-center">{$count_arr[4]['tong']}</td>
                                    <td class="text-center">{$count_arr[5]['tong']}</td>
                                    <td class="text-center">{$count_arr[6]['tong']}</td>
                                    <td class="text-center">{$count_arr[7]['tong']}</td>
                                    <td class="text-center">{$count_arr[8]['tong']}</td>
                                    <td class="text-center" style="color: red">{$count_arr[9]['tong']}</td>
                                    <td class="text-center">{$count_arr[10]}%</td>
                                    <td class="text-center"><b>{$arr_chamtudong}</b></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}
                                    <select name="diem_1">
                                        <option value="0">Điểm</option>
                                        {for $i=60;$i>=0;$i--}
                                        {$j=$i+0.5}
                                        {if $j<60 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_1']==$j)?'selected':''}>{$j}</option>{/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_1']==$i)?selected:''}>{$i}.0</option>                                       
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_1']>0)?$list_diem[0]['diem_1']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}
                                    <select name="diem_1_tp">
                                        {for $i=0;$i<61;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_1_tp']=$list_diem[0]['diem_1']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_1_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<60 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_1_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_1_tp']}
                                    {else}
                                        -
                                    {/if}
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text-center"><b>B</b></td>
                                    <td class="text-left"><b>Ý thức tổ chức kỷ luật (tối đa 20 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_2']:''}</b></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_2_tp']:''}</b></td>
                                </tr> 
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Chấp hành tốt chủ trương, đường lối của Đảng, chính sách pháp luật của Nhà nước, nội quy, quy chế làm việc của cơ quan  <br><b>- Điểm tối đa: 5</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>5</b></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}
                                    <select  name="diem_3">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_3']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<5 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_3']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_3']>0)?$list_diem[0]['diem_3']:''}
                                    {/if}
                                    </select></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}
                                    <select  name="diem_3_tp">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_3_tp']=$list_diem[0]['diem_3']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_3_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j < 5 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_3_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_3_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-left">Không hách dịch, cửa quyền, sách nhiễu, gây phiền hà, tiêu cực trong thực hiện nhiệm vụ, công vụ <br><b>- Điểm tối đa: 4</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>4</b></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 3 || $quyen_lds == 6  || $quyen_lds == 5}
                                    <select name="diem_4">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_4']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j} {(!empty($list_diem) && $list_diem[0]['diem_4']==$j)?'selected':''}">{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_4']>0)?$list_diem[0]['diem_4']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}
                                    <select name="diem_4_tp">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_4_tp']=$list_diem[0]['diem_4']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_4_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_4_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_4_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-left">Có phẩm chất chính trị, đạo đức, lối sống lành mạnh; tác phong, lề lối làm việc chuẩn mực; có tinh thần trách nhiệm, tận tụy với công việc; giữ gìn đoàn kết, thực hiện nguyên tắc tập trung dân chủ trong cơ quan, đơn vị <br><b>- Điểm tối đa: 3</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>3</b></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}
                                    <select name="diem_5">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_5']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_5']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_5']>0)?$list_diem[0]['diem_5']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_5_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_5_tp']=$list_diem[0]['diem_5']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_5_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_5_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_5_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">4</td>
                                    <td class="text-left">Thực hiện văn hóa nơi công sở, giao tiếp thân thiện, lịch sự, trang phục phù hợp, đeo thẻ công chức trong giờ làm việc<br><b>- Điểm tối đa: 3</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>3</b></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}
                                    <select name="diem_6">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_6']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_6']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_6']>0)?$list_diem[0]['diem_6']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                        {if $quyen_lds == 4}<select name="diem_6_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_6_tp']=$list_diem[0]['diem_6']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_6_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_6_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_6_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">5</td>
                                    <td class="text-left">Xây dựng hình ảnh, giữ gìn uy tín cho bản thân, cơ quan, đơn vị và đồng nghiệp; chấp hành nghiêm các quy định của pháp luật về chế độ bảo mật, bí mật và kỷ luật phát ngôn của cơ quan, đơn vị <br><b>- Điểm tối đa: 3</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>3</b></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}
                                    <select name="diem_7">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_7']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_7']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_7']>0)?$list_diem[0]['diem_7']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_7_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_7_tp']=$list_diem[0]['diem_7']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_7_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_7_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_7_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">6</td>
                                    <td class="text-left">Sử dụng hiệu quả thời gian làm việc, không giải quyết việc riêng trong giờ làm việc<br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_8">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_8']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_8']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_8']>0)?$list_diem[0]['diem_8']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_8_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_8_tp']=$list_diem[0]['diem_8']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_8_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_8_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_8_tp']}
                                    {else}-{/if}</td>
									
									{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<input type="hidden" name="diem_9" value="0">{/if}
									{if $quyen_lds == 4}<input type="hidden" name="diem_9_tp" value="0">{/if}
                                </tr> 
                                <!-- <tr>
                                    <td class="text-center">7</td>
                                    <td class="text-left">Chấp hành nghiêm quy định về thời gian làm việc của nhà nước, của cơ quan; sắp xếp, sử dụng thời gianlàm việc khoa học và hiệu quả<br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_9">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_9']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_9']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_9']>0)?$list_diem[0]['diem_9']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_9_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                
                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_9_tp']=$list_diem[0]['diem_9']}
                                        {/if}
                                
                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_9_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_9_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_9_tp']}
                                    {else}-{/if}</td>
                                
                                </tr>  -->

                                <tr>
                                    <td class="text-center"><b>C</b></td>
                                    <td class="text-left"><b>Năng lực và kỹ năng thực hiện nhiệm vụ (tối đa 10 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_10']:''}</b></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_10_tp']:''}</b></td>

                                </tr> 
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Thường xuyên học tập, rèn luyện, nâng cao trình độ, kỹ năng chuyên môn nghiệp vụ, kiến thức ngoại ngữ, tin học; tham mưu đầy đủ, kịp thời, có chất lượng các văn bản phục vụ công tác chỉ đạo, điều hành của đơn vị, bộ phận theo chỉ đạo của lãnh đạo Sở và kế hoạch công tác đề ra <br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_11">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_11']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_11']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_11']>0)?$list_diem[0]['diem_11']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_11_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_11_tp']=$list_diem[0]['diem_11']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_11_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_11_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_11_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-left">Xây dựng kế hoạch công tác tuần, tháng của cá nhân, phòng, đơn vị, bộ phận theo lĩnh vực được phân công rõ nội dung, tiến độ <br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_12">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_12']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_12']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_12']>0)?$list_diem[0]['diem_12']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_12_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_12_tp']=$list_diem[0]['diem_12']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_12_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_12_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_12_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-left">Chỉ đạo, điều hành, kiểm soát việc thực hiện nhiệm vụ của đơn vị, bộ phận đảm bảo kịp thời, không bỏ sót nhiệm vụ. Phân công, chỉ đạo giải quyết công việc linh hoạt, có định hướng cụ thể, đúng quy trình<br><b>- Điểm tối đa: 2</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_13">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_13']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_13']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_13']>0)?$list_diem[0]['diem_13']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_13_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_13_tp']=$list_diem[0]['diem_13']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_13_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_13_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_13_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">4</td>
                                    <td class="text-left">Kiểm tra, đôn đốc việc thực hiện nhiệm vụ của công chức, viên chức, lao động hợp đồng trong đơn vị, bộ phận và giải quyết kịp thời những khó khăn, vướng mắc theo thẩm quyền <br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_14">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_14']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_14']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_14']>0)?$list_diem[0]['diem_14']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_14_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_14_tp']=$list_diem[0]['diem_14']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_14_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_14_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_14_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">5</td>
                                    <td class="text-left">Có năng lực quy tụ, tập hợp công chức, viên chức, xây dựng đơn vị, bộ phận đoàn kết, thống nhất. Phối hợp, tạo lập mối quan hệ tốt với cá nhân, tổ chức có liên quan trong thực hiện nhiệm vụ <br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_15">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_15']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_15']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_15']>0)?$list_diem[0]['diem_15']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_15_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_15_tp']=$list_diem[0]['diem_15']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_15_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_15_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_15_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">6</td>
                                    <td class="text-left">Sử dụng thành thạo các phần mềm, ứng dụng công nghệ thông tin đáp ứng yêu cầu công việc; các văn bản trên hệ thống Văn phòng điện tử thuộc trách nhiệm được xử lý kịp thời, đúng quy trình <br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6  || $quyen_lds == 5}<select name="diem_16">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_16']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_16']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_16']>0)?$list_diem[0]['diem_16']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_16_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_16_tp']=$list_diem[0]['diem_16']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_16_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_16_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_16_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                
                                <input type="hidden" name="diem_17" value="0">
                                <input type="hidden" name="diem_17_tp" value="0">
                                <tr>
                                    <td class="text-center">7</td>
                                    <td class="text-left">Thiết lập hồ sơ công việc đầy đủ theo các đầu mục công việc được phân công; lưu trữ hồ sơ, tài liệu đúng nguyên tắc; xây dựng cơ sở dữ liệu thuộc lĩnh vực được phân công phụ trách, làm cơ sở để xây dựng kho dữ liệu chung của cơ quan, đơn vị <br><b>- Điểm tối đa: 1</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b></td>
                                    <td class="text-center">{if $quyen_lds == 3 || $quyen_lds == 6 || $quyen_lds == 5}<select name="diem_18">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_18']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_18']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_18']>0)?$list_diem[0]['diem_18']:''}
                                    {/if}</td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_18_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_18_tp']=$list_diem[0]['diem_18']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_18_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_18_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && ($quyen_lds == 3 || $quyen_lds == 6)}
                                        {$list_diem[0]['diem_18_tp']}
                                    {else}-{/if}</td>

                                </tr>  

                                <tr>
                                    <th class="text-center">D</th>
                                    <td class="text-left"><b>Điểm thưởng (điểm tối đa: 10)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$diemthuongmaycham}</b></td>
                                    <td class="text-center">
                                        <input type="hidden" name="diem_19" value="0"></td>
                                    <td class="text-center">
                                    {if $quyen_lds == 4}<select name="diem_19_tp">
                                        {for $i=0;$i<11;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_19_tp']=$list_diem[0]['diem_19']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_19_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j < 10 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_19_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    </td>

                                </tr>
                                <tr>
                                    <th class="text-center" colspan="4" rowspan="2">Tổng điểm</th>
                                   
                                    <td class="text-center" colspan="4"><b>Tổng điểm máy chấm</b></td>
                                    <td class="text-center" colspan="4"><b>Tổng điểm cá nhân chấm</b></td>
                                    <td class="text-center" colspan="4"><b>Tổng điểm GĐ sở chấm</b></td>

                                </tr> 
                                <tr>                                    
                                    <td class="text-center" colspan="4"><b>{$arr_chamtudong+$diemthuongmaycham+30}</b></td>
                                    <td class="text-center" colspan="4"><b>{$list_diem[0]['diem_tong']}</b></td>
                                    <td class="text-center" colspan="4"><b>{$list_diem[0]['diem_tong_tp']}</b></td>

                                </tr>  
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                {if $PK_iMaCB == $cb_id }
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="danhgia" style="font-size: 14px" rows=15 placeholder="Cán bộ tự nhận xét về ưu điểm hạn chế">{if !empty($list_diem)}{$list_diem[0]['danhgia']}{else}{if $quyen >=6 }- Chấp hành nghiêm chủ trương, đường lối, nghị quyết của Đảng, chính sách pháp luật của Nhà nước; 
- Thực hiện tốt 02 bộ Quy tắc ứng xử của Thành phố và nội quy, quy chế làm việc của cơ quan.
- Có ý thức xây dựng phòng, đơn vị đoàn kết, thống nhất. 
- Kết quả công tác chỉ đạo,triển khai nhiệm vụ trong tháng:…………………………….
{/if}{if $quyen ==5 }- Chấp hành nghiêm chủ trương, đường lối, nghị quyết của Đảng, chính sách pháp luật của Nhà nước; 
- Thực hiện tốt 02 bộ Quy tắc ứng xử của Thành phố và nội quy, quy chế làm việc của cơ quan.
- Có ý thức xây dựng phòng, đơn vị đoàn kết, thống nhất. 
- Kết quả công tác chỉ đạo,triển khai nhiệm vụ trong tháng:…………………………….
{/if}
{/if}</textarea>
                    </div>
                </div>
<div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                        <label>CÁ NHÂN PHÂN LOẠI:</label>                       
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_canhan" value="8" {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 8}checked{/if}>
                    
                        <b> Hoàn thành xuất sắc tiêu biểu</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_canhan" value="6" {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 6}checked{/if}>
                    
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_canhan" value="5" {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 5}checked{/if}>                       
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_canhan" value="4" {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 4}checked{/if}>
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_canhan" value="2" {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 2}checked{/if}>
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                    
                </div>  
                <div class="row" style="height: 5px"></div>
                {if $list_diem[0]['active']<=1 || empty($list_diem) || ($quyen_lds == 5 and $list_diem[0]['active']<=2)}
                <div class="row">
                    <div class="col-md-12">
                        <!--<input type="hidden" name="month" value="{$month}">-->
                        <input type="hidden" name="phong" value="{$phong}">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật điểm tháng {$month}</button>
                    </div>
                </div>
                {/if}
                {/if}

                {if $quyen_lds == 4}
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                         <b>Cá nhân đ/c {$canbo} tự đánh giá:</b> {if !empty($list_diem)}<span style="font-size: 14px">{$list_diem[0]['danhgia']}</span><br/><br/>{/if}
                         
                         {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] >0}
                         <b>Cá nhân tự nhận mức phân loại: <br></b>
                            {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 8}<b> Hoàn thành xuất sắc tiêu biểu</b>{/if}
                            {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 6}<b> Hoàn thành xuất sắc nhiệm vụ </b>{/if}
                            {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 5}<b> Hoàn thành tốt nhiệm vụ </b>{/if}
                            {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 4}<b> Hoàn thành nhiệm vụ </b>{/if}
                            {if !empty($list_diem) && $list_diem[0]['danhgia_canhan'] == 2}<b> Không hoàn thành nhiệm vụ </b>{/if}
                            <br><br>
                         {/if}
                         <!-- {if !empty($list_diem)}<span style="color: red">{$list_diem[0]['danhgia_cvp']}</span><br/>{/if} --> 

                         {if !empty($list_diem)}<span style="color: blue">{$list_diem[0]['danhgia_pgd']}</span><br/>{/if}
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="danhgia_ld" style="font-size: 14px;" rows=15 placeholder="Lãnh đạo đánh giá cán bộ">{if !empty($list_diem)}{if $list_diem[0]['danhgia_ld'] !=''}{$list_diem[0]['danhgia_ld']}{else}{$list_diem[0]['danhgia_cvp']}{/if}{/if}</textarea>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>

                <div class="row">
                    <div class="col-md-2">
                        <label>GĐ SỞ PHÂN LOẠI:</label>
                        
                    
                        <input type="text" name="diem_tong_tp" placeholder="GĐ chấm điểm" value="{if !empty($list_diem)}{$list_diem[0]['diem_tong_tp']}{/if}">
                        
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="8" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 8}checked{/if}>
                    
                        <b> Hoàn thành xuất sắc tiêu biểu</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="6" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 6}checked{/if}>
                    
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="5" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 5}checked{/if}>                       
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="4" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 4}checked{/if}>
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="2" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 2}checked{/if}>
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                    
                </div>  

                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-12">
                        <!--<input type="hidden" name="month" value="{$month}">-->
                        <input type="hidden" name="phong" value="{$phong}">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Đánh giá tháng {$month}</button>
                    </div>
                </div>
                {/if}
                {if $quyen_lds == 3 && $cb_id != $PK_iMaCB}
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                         <b>Cá nhân đ/c {$canbo} tự đánh giá:</b> {if !empty($list_diem)}{$list_diem[0]['danhgia']}{/if} 
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="danhgia_cvp" style="font-size: 14px;" rows=10 placeholder="Chánh văn phòng nhận xét cán bộ">{if !empty($list_diem)}{$list_diem[0]['danhgia_cvp']}{/if}
                        </textarea>
                    </div>
                </div>

                <div class="row" style="height: 5px"></div>

                <div class="row">
                    <div class="col-md-2">
                        <label>GĐ SỞ PHÂN LOẠI:</label>
                        
                    
                        <input type="text" name="diem_tong_tp" placeholder="GĐ chấm điểm" value="{if !empty($list_diem)}{$list_diem[0]['diem_tong_tp']}{/if}">
                        
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="8" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 8}checked{/if}>
                    
                        <b> Hoàn thành xuất sắc tiêu biểu</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="6" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 6}checked{/if}>
                    
                        <b> Hoàn thành xuất sắc nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="5" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 5}checked{/if}>                       
                            <b> Hoàn thành tốt nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="4" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 4}checked{/if}>
                        <b> Hoàn thành nhiệm vụ</b>
                    </div>
                    <div class="col-md-2">
                            <input type="radio" required="" name="danhgia_lds" value="2" {if !empty($list_diem) && $list_diem[0]['danhgia_lds'] == 2}checked{/if}>
                        <b> Không hoàn thành nhiệm vụ</b>
                    </div>
                    
                </div> 
                
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <!--<input type="hidden" name="month" value="{$month}">-->
                        <input type="hidden" name="phong" value="{$phong}">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Đánh giá tháng {$month}</button>
                    </div>
                </div>
                {/if}
                {if $quyen_lds == 5  && $cb_id != $PK_iMaCB}
                <div class="row">
                    <div class="col-md-12">
                         <b>Cá nhân đ/c {$canbo} tự đánh giá:</b> {if !empty($list_diem)}{$list_diem[0]['danhgia']}{/if} 
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="danhgia_pgd" style="font-size: 14px;" rows=10 placeholder="Phó giám đốc nhận xét cán bộ">{if !empty($nhanxet_danhgia)}{$nhanxet_danhgia['sNhanXet']}{/if}</textarea>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <!--<input type="hidden" name="month" value="{$month}">-->
                        <input type="hidden" name="phong" value="{$phong}">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Đánh giá tháng {$month}</button>
                    </div>
                </div>
                {/if}

                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12" style="color: blue">Lưu ý tiêu chí<br> - Hoàn thành xuất sắc nhiệm vụ là: trung bình từ 90 điểm trở lên (phải có điểm thưởng) <br>- Hoàn thành tốt nhiệm vụ là: trung bình từ 70 điểm trở lên và dưới 90 điểm 
                    <br>- Hoàn thành nhiệm vụ là: trung bình từ 50 điểm trở lên và dưới 70 điểm 
                    <br>- Không hoàn thành nhiệm vụ là: trung bình dưới 50 điểm</div>
                </div>
                
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

