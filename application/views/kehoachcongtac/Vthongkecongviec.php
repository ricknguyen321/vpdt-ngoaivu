<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-7">
                <span class="font" style="color: blue;">
                    DS công việc của đ/c:<b> {$hoten}</b>
                </span>
                <span class="4" style="font-weight: 550"> từ 
                     <select name="tuanbd" style="width: 300px" class="form-control select2"
                     >
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $tuanbd == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </span>
            </div>
            <div class="col-md-4">
                <span class="4" style="font-weight: 550"> đến 
                     <select name="tuankt" style="width: 300px" class="form-control select2"
                     >
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $tuankt == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </span> 
            </div>
            <div class="col-md-1">
                <input type="submit" name="thongke" value="Gửi">
            </div>
                   
        </div>
    </section>
</form>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12" style="overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center">STT</th>
                                    <th width="67%" class="text-center">Nội dung</th>
                                    <th width="10%" class="text-center">Ngày hoàn thành</th>
                                    <th width="10%" class="text-center">Hạn giải quyét</th>
                                    <th width="10%" class="text-center">Số ngày hoàn thành trước hạn</th>
                                    
                                </tr>
                            </thead>    
                            <tbody>
                                {$i=1}
                                {if empty($dsviechoanthanh)}{else}
                                {foreach $dsviechoanthanh as $key => $value1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">
                                    {if $value1.vanban_skh neq ''}
                                    {$value1.vanban_skh} &nbsp;{$value1.kh_noidung}
                                    {else}
                                    <b>Công việc phòng giao:</b>&nbsp;{$value1.kh_noidung}
                                    {/if}

                                    {if $value1.sangtao eq 2} 
                                        <br><span style="color: red;font-weight: 600">Có sáng tạo</span>
                                    {/if}

                                    </td>
                                    <td class="text-center" style="color: blue">{date_select($value1.ngay_hoanthanh)}</td>
                                    <td class="text-center" style="color: blue">
                                    {if $value1.ngay_han gt '2000-01-01'}{date_select($value1.ngay_han)}{/if}</td>

                                    <td class="text-center" style="color: blue">

                                    {$songay = count_ngay($value1.ngay_han,$value1.ngay_hoanthanh)}


                                    {$songay_qh = count_ngay($value1.ngay_hoanthanh,$value1.ngay_han)}

                                    {if $value1.ngay_han lt '2000-01-01' or $songay eq 0}
                                        HT đúng hạn
                                    {/if}

                                    {if $songay gt 0 and $value1.ngay_han gt '2000-01-01'}
                                        <span style="color: blue; font-weight: 600"> Trước hạn {$songay} ngày</span>
                                    {/if}

                                    {if $songay lt 0 and  $value1.ngay_han gt '2000-01-01'}
                                        <span style="color: red; font-weight: 600"> Quá hạn {$songay_qh} ngày</span>
                                    {/if}

                                    
                                    
                                    </td>

                                   
                                </tr> 
                                {/foreach}{/if}
                                <tr>
                                    
                                    <th colspan="7"  style="border-right: 1px solid #c3c3c3">CÔNG VIỆC ĐANG THỰC HIỆN</th>
                                </tr>
                                {if empty($dsviecdangthuchien)}{else}
                                {foreach $dsviecdangthuchien as $key => $value}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">{if $value.vanban_skh neq ''}
                                    {$value1.vanban_skh} &nbsp;{$value.kh_noidung}
                                    {else}
                                    <b>Công việc phòng giao:</b>&nbsp;{$value.kh_noidung}
                                    {/if}</td>
                                    <td class="text-center" style="color: blue"></td>
                                    <td class="text-center" style="color: blue">{if $value.ngay_han gt '2000-01-01'}{date_select($value.ngay_han)}{/if}</td>

                                    <td class="text-center" style="color: blue"></td>

                                </tr> 
                                {/foreach}{/if}

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
    </section>
    <!-- /.content -->
</div>

