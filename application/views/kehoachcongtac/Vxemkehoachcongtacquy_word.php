<style type="text/css">
    .border_main{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        padding-left: 15px;
    }
    .border_main1{
        border-top: 1px solid #000;
        border-left: 1px solid #000;
        border-right: 1px solid #000;
        padding-left: 15px;
    }
</style>
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2" style="text-align: center;width: 20%;vertical-align: top; font-size: 14px">
                                    Sở Ngoại Vụ HÀ NỘI<BR>
                                    <span style="text-transform: uppercase;">
                                        <b>{$phongban[$phong_id]}</b>
                                    </span>
                                </td>
                                <td colspan="13" style="text-align: center;width: 80%">
                                    <B>TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ, CHẤM ĐIỂM MỨC ĐỘ HOÀN THÀNH NHIỆM VỤ QUÝ CỦA CÁ NHÂN </B><BR>
                                    <b><span style="padding: 30px 0 30px 0">Quý:{$quy}</span></b><BR>
                                     Họ và tên: {$canbophong[$cb_id]}; Chức vụ:
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
               <div class="row" >
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="border_main" rowspan="3">STT</th>
                                    <th width="30%" class="border_main" rowspan="3">Nội dung</th>
                                    <th width="5%" class="border_main" rowspan="3">Tổng số công việc theo kế hoạch</th>
                                    <th width="25%" class="border_main" colspan="6">Công việc đã hoàn thành trong quý</th>
                                    <th width="10%" class="border_main" colspan="3">Công việc còn tồn cuối quý</th>
                                    <th width="5%" class="border_main" rowspan="3" >Tỉ lệ hoàn thành (%)</th>
                                    <th width="5%" class="border_main" rowspan="3">Số điểm đạt được hàng tháng</th>
                                    <th width="5%" class="border_main1" rowspan="3">Điểm bình quân của quý</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="border_main" rowspan="2">Số lượng</th>
                                    <th width="10%" class="border_main" colspan="2">Thời gian</th>
                                    <th width="10%" class="border_main" colspan="2">Chất lượng</th>
                                    <th width="5%" class="border_main" rowspan="2" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="border_main" rowspan="2">Số lượng</th>
                                    <th width="5%" class="border_main" rowspan="2">Trong hạn</th>
                                    <th width="5%" class="border_main" rowspan="2">Quá hạn</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="border_main">Trong hạn</th>
                                    <th width="5%" class="border_main">Quá hạn</th>
                                    <th width="5%" class="border_main">Đảm bảo</th>
                                    <th width="5%" class="border_main">Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>                                
                                <tr>
                                    <td class="border_main"><b>I</b></td>
                                    <td class="border_main text-left"><b>Kết quả thực hiện nhiệm vụ quý {$quy} <br>(tối đa 65 điểm)</b></td>
                                    <td class="border_main">{$count_arr[0]}</td>
                                    <td class="border_main">{$count_arr[1]}</td>
                                    <td class="border_main">{$count_arr[2]}</td>
                                    <td class="border_main" style="color: red">{$count_arr[3]}</td>
                                    <td class="border_main">{$count_arr[4]}</td>
                                    <td class="border_main">{$count_arr[5]}</td>
                                    <td class="border_main">{$count_arr[6]}</td>
                                    <td class="border_main">{$count_arr[7]}</td>
                                    <td class="border_main">{$count_arr[8]}</td>
                                    <td class="border_main" style="color: red">{$count_arr[9]}</td>
                                    <td class="border_main">{$count_arr[10]}%</td>
                                    <td class="border_main">
                                    
                                    </td>
                                    <td class="border_main1">
                                    <b>{$trungbinhthang1}</b>
                                    </td>

                                </tr>

                                {$i=1}
                                {foreach $list_data as $key => $value}
                                {$j=$i-1}
                                <tr>
                                    <td class="border_main">{$i++}</td>
                                    <td class="border_main text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="border_main" style="color: blue">{$value[0]}</td>
                                    <td class="border_main" style="color: blue">{$value[1]}</td>
                                    <td class="border_main" style="color: blue">{$value[2]}</td>
                                    <td class="border_main" style="color: red">{$value[3]}</td>
                                    <td class="border_main" style="color: blue">{$value[4]}</td>
                                    <td class="border_main" style="color: blue">{$value[5]}</td>
                                    <td class="border_main" style="color: blue">{$value[6]}</td>
                                    <td class="border_main" style="color: blue">{$value[7]}</td>
                                    <td class="border_main" style="color: blue">{$value[8]}</td>
                                    <td class="border_main" style="color: red">{$value[9]}</td>
                                    <td class="border_main" style="color: blue">{$value[10]} %</td>
                                    <td class="border_main" style="color: blue">{$kyluat[$key]['0']['diem_1_tp']}</td>
                                    <td class="border_main1" style="color: blue"></td>
                                </tr> 
                                {/foreach}
                                
                                <tr>
                                    <td class="border_main"><b>II</b></td>
                                    <td class="border_main text-left"><b>Ý thức tổ chức kỷ luật (tối đa 20 điểm)</b></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"><b></b></td>
                                    <td class="border_main1"><b>{$trungbinhthang2}</b></td>
                                </tr> 
                                {$i=1}
                                {foreach $kyluat as $key => $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="border_main">{$i++}</td>
                                    <td class="border_main text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"><b>{$kl['1']['diem_2_tp']}</b></td>
                                    <td class="border_main1"><b></b></td>
                                </tr> 
                                {/foreach}
                                
                                <tr>
                                    <td class="border_main"><b>III</b></td>
                                    <td class="border_main text-left"><b>Năng lực và kỹ năng thực hiện nhiệm vụ (tối đa 10 điểm)</b></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"><b></b></td>
                                    <td class="border_main1"><b>{$trungbinhthang3}</b></td>

                                </tr> 
                                {$i=1}
                                {foreach $kyluat as $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="border_main">{$i++}</td>
                                    <td class="border_main text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"><b>{$kl['2']['diem_10_tp']}</b></td>
                                    <td class="border_main1"><b></b></td>
                                </tr> 
                                {/foreach}
                                <tr>
                                    <th class="border_main">IV</th>
                                    <td class="border_main text-left"><b>Điểm thưởng (điểm tối đa: 5)</b></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main1"><b>{$trungbinhthang4}</b></td>
                                </tr>
                                {$i=1}
                                {foreach $kyluat as $kl}
                                {$j=$i-1}
                                <tr>
                                    <td class="border_main">{$i++}</td>
                                    <td class="border_main text-left">Tháng {$arr_thang[$j]}</td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"><b>{$kl['3']['diem_19_tp']}</b></td>
                                    <td class="border_main1"><b></b></td>
                                </tr> 
                                {/foreach} 
                                <!-- tổng cộng điểm của các tuần-->
                                <tr>
                                    <td class="border_main"></td>
                                    <td class="border_main text-left"><b>Tổng cộng (I+II+III+IV)</b></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"></td>
                                    <td class="border_main"><b></b></td>
                                    <td class="border_main1"><b>{$tongdiemthang}</b></td>
                                </tr>
                                <tr><td colspan="15" style="border-top: 1px solid #000"></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                     
                <div class="row" style="height: 10px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped" width="100%">
                            <thead>
                                <tr>
                                    <td width="5%"></td>
                                    <td width="70%">
                                        <div class="col-sm-2" style="padding-bottom: 3px">
                                            <b>I. CÁ NHÂN TỰ PHÂN LOẠI:</b>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="checkbox" name="danhgiathang" value="6" {if isset($giatri) && $giatri['danhgiaquy'] == 6}checked{/if}>
                                              Hoàn thành xuất sắc nhiệm vụ 
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="checkbox" name="danhgiathang" value="5" {if isset($giatri) && $giatri['danhgiaquy'] == 5}checked{/if}>
                                              Hoàn thành tốt nhiệm vụ 
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="checkbox" name="danhgiathang" value="4" {if isset($giatri) && $giatri['danhgiaquy'] == 4}checked{/if}>
                                            Hoàn thành nhiệm vụ
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="checkbox" name="danhgiathang" value="3" {if isset($giatri) && $giatri['danhgiaquy'] == 3}checked{/if}>
                                             Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="checkbox" name="danhgiathang" value="2" {if isset($giatri) && $giatri['danhgiaquy'] == 2}checked{/if}>
                                             Không hoàn thành nhiệm vụ
                                        </div>
                                    </td>
                                    <td width="25%" style="text-align: center;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b>Cán bộ thực hiện</b><br>
                                    <i>(ký, ghi rõ họ tên)</i><br><br><br><br>
                                </td>
                                </tr>
                                <tr height="10px"><td colspan="3">&nbsp;</td></tr>
                                <tr>
                                    <td></td>
                                    <td colspan="3">
                                        <div class="col-sm-2" style="padding-bottom: 3px">
                                            <b>II. TRƯỞNG ĐƠN VỊ ĐÁNH GIÁ VÀ QUYẾT ĐỊNH MỨC PHÂN LOẠI:</b>
                                        </div>
                                        <div class="col-sm-2" style="padding-bottom: 1px">
                                            <b>&nbsp;1. Ý kiến nhận xét, đánh giá:</b>
                                        </div>
                                        <div class="col-sm-2">
                                            &nbsp;&nbsp;{if isset($giatri)}{$giatri['lanhdao_nhanxet']}{/if}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="3">
                                        <div class="col-sm-2" style="padding-bottom: 1px">
                                            <b>&nbsp;2. Quyết định mức phân loại quý: {$quy} năm {date('Y')}</b>
                                        </div>
                                        <div class="col-sm-2">
                                            &nbsp;Công chức (viên chức, lao động hợp đồng) xếp loại:<b> 
                                            {if isset($giatri) && $giatri['danhgiaquy_ld'] == 6}Hoàn thành xuất sắc nhiệm vụ {/if}
                                            {if isset($giatri) && $giatri['danhgiaquy_ld'] == 5}Hoàn thành tốt nhiệm vụ {/if}
                                            {if isset($giatri) && $giatri['danhgiaquy_ld'] == 4}Hoàn thành nhiệm vụ {/if}
                                            {if isset($giatri) && $giatri['danhgiaquy_ld'] == 3}Hoàn thành nhiệm vụ nhưng còn hạn chế về năng lực{/if}
                                            {if isset($giatri) && $giatri['danhgiaquy_ld'] == 2}Không hoàn thành nhiệm vụ {/if}
                                            </b>  
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                    <td width="25%" style="text-align: center;">
                                    <i>Ngày {date('d/m/Y')}</i><br>
                                    <b>Trưởng đơn vị</b><br>
                                    <i>(ký, ghi rõ họ tên)</i><br><br><br><br>
                                </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    
                </div> 
            </div>
                <div class="row" style="height: 5px"></div>                
            </div>
        </div>
      <!-- /.box -->

    </section>
 


