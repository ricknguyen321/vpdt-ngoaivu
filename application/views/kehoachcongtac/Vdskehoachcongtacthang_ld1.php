<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-3">
                <span class="font">
                    Danh sách kế hoạch công tác tháng
                </span>
            </div>
            <div class="col-md-4 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn Tháng công tác: 
                </span>
                <label class="8 sl2">
                    <select name="month" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select>
                </label>
            </div>
            <div class="col-md-4 sl3">
                <!-- <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word"> -->
            </div>
            <div class="col-md-1 sl3"></div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3" >STT</th>
                                    <th class="text-center" rowspan="3" >Phòng ban, đơn vị</th>
                                    <th width="10%" class="text-center" rowspan="3" >Tổng số việc theo kế hoạch</th>
                                    <th width="30%" class="text-center" colspan="6" >Công việc đã hoàn thành trong tháng</th>
                                    <th width="15%" class="text-center" colspan="3" >Công việc còn tồn cuối tháng</th>
                                    <th width="8%" class="text-center" style="border-left: 1px solid #FFF" rowspan="3"  >Tỉ lệ hoàn thành (%)</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2" >Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2"  >Chất lượng</th>
                                    <th width="5%" class="text-center" style="border-left: 1px solid #FFF" rowspan="2" >Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2" >Số lượng</th>
                                    <th width="5%" class="text-center"  rowspan="2">Trong hạn</th>
                                    <th width="5%" class="text-center"  rowspan="2">Chậm tiến độ</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" >Trong hạn</th>
                                    <th width="5%" class="text-center" >Quá hạn</th>
                                    <th width="5%" class="text-center" >Đảm bảo</th>
                                    <th width="5%" class="text-center" >Chưa đảm bảo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                {$ii=1}
                                {foreach $count_arr as $key => $giatri}
                                {if $key > 10}
                                <tr>
                                    <td>{$ii++}</td>
                                    <td style=" text-align: left;padding-left: 5px">
                                        <a href="dskehoachcongtacthang/{$giatri[15]}/{$month}" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết">{$phongban[$giatri[15]]}</a></td>
                                    <td>{$giatri[0]['tong']}</td>
                                    <td>{$giatri[1]['tong']}</td>
                                    <td>{$giatri[2]['tong']}</td>
                                    <td>{$giatri[3]['tong']}</td>
                                    <td>{$giatri[4]['tong']}</td>
                                    <td>{$giatri[5]['tong']}</td>
                                    <td>{$giatri[6]['tong']}</td>
                                    <td>{$giatri[7]['tong']}</td>
                                    <td>{$giatri[8]['tong']}</td>
                                    <td>{$giatri[9]['tong']}</td>
                                    <td>{$giatri[10]} %</td>
                                </tr>
                                {/if}
                                {/foreach}
                               
                            </tbody>
                        </table>
                    </div>
                </div>
              
                </div>   
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('td').css('vertical-align','top');
    });

    $(document).ready(function(){
        var url = window.location.href;
        $(document).on('click','button[name=chamdiem]',function(){
            var danhgia_id = $(this).val();
            var nhan_xet  = $('textarea[name="nhanxet_' + danhgia_id + '"]').val();
            var diem_1_tp  = $('select[name="diem_1_tp_' + danhgia_id + '"] option:selected').val();
            var diem_3_tp  = $('select[name="diem_3_tp_' + danhgia_id + '"] option:selected').val();
            var diem_4_tp  = $('select[name="diem_4_tp_' + danhgia_id + '"] option:selected').val();
            var diem_5_tp  = $('select[name="diem_5_tp_' + danhgia_id + '"] option:selected').val();
            var diem_6_tp  = $('select[name="diem_6_tp_' + danhgia_id + '"] option:selected').val();
            var diem_7_tp  = $('select[name="diem_7_tp_' + danhgia_id + '"] option:selected').val();
            var diem_8_tp  = $('select[name="diem_8_tp_' + danhgia_id + '"] option:selected').val();
            var diem_9_tp  = $('select[name="diem_9_tp_' + danhgia_id + '"] option:selected').val();
            var diem_11_tp  = $('select[name="diem_11_tp_' + danhgia_id + '"] option:selected').val();
            var diem_12_tp  = $('select[name="diem_12_tp_' + danhgia_id + '"] option:selected').val();
            var diem_13_tp  = $('select[name="diem_13_tp_' + danhgia_id + '"] option:selected').val();
            var diem_14_tp  = $('select[name="diem_14_tp_' + danhgia_id + '"] option:selected').val();
            var diem_15_tp  = $('select[name="diem_15_tp_' + danhgia_id + '"] option:selected').val();
            var diem_16_tp  = $('select[name="diem_16_tp_' + danhgia_id + '"] option:selected').val();
            var diem_18_tp  = $('select[name="diem_18_tp_' + danhgia_id + '"] option:selected').val();
            var diem_19_tp  = $('select[name="diem_19_tp_' + danhgia_id + '"] option:selected').val();
            var diem_2_tp = parseFloat(diem_3_tp)+parseFloat(diem_4_tp)+parseFloat(diem_5_tp)+parseFloat(diem_6_tp)+parseFloat(diem_7_tp)+parseFloat(diem_8_tp)+parseFloat(diem_9_tp);
            var diem_10_tp = parseFloat(diem_11_tp)+parseFloat(diem_12_tp)+parseFloat(diem_13_tp)+parseFloat(diem_14_tp)+parseFloat(diem_15_tp)+parseFloat(diem_16_tp)+parseFloat(diem_18_tp);
            var diem_tong_tp = parseFloat(diem_1_tp)+parseFloat(diem_2_tp)+parseFloat(diem_10_tp)+parseFloat(diem_19_tp);
            $.ajax({
                url:url,
                type:'POST',
                data:{
                    action:'chamdiem',
                    danhgia_id:danhgia_id,
                    diem_1_tp:diem_1_tp,
                    diem_2_tp:diem_2_tp,
                    diem_3_tp:diem_3_tp,
                    diem_4_tp:diem_4_tp,
                    diem_5_tp:diem_5_tp,
                    diem_6_tp:diem_6_tp,
                    diem_7_tp:diem_7_tp,
                    diem_8_tp:diem_8_tp,
                    diem_9_tp:diem_9_tp,
                    diem_10_tp:diem_10_tp,
                    diem_11_tp:diem_11_tp,
                    diem_12_tp:diem_12_tp,
                    diem_13_tp:diem_13_tp,
                    diem_14_tp:diem_14_tp,
                    diem_15_tp:diem_15_tp,
                    diem_16_tp:diem_16_tp,
                    diem_18_tp:diem_18_tp,
                    diem_19_tp:diem_19_tp,
                    diem_tong_tp:diem_tong_tp,
                    nhan_xet:nhan_xet
                },
                success:function(repon){ 
                    var result = JSON.parse(repon);
                    if(result>0)
                    {   
                        location.reload();
                        showMessage('Bạn đã chấm điểm thành công','info');
                    }


                }
            });
        });
    });
</script>

