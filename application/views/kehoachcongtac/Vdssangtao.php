<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-7">
                <span class="font" style="font-weight: 600">
                    Danh sách đầu việc có sáng tạo đổi mới tháng: 

                </span>
                     <!-- {$month}  -->
                     <select name="month" style="width: 50px" class="form-control select2" onchange="this.form.submit()">
                            {for $i=1;$i<=12;$i++}
                                <option value="{$i}" {if $month == $i} selected {/if}>{$i}</b></option>
                            {/for}
                    </select> <b>(có thể lựa chọn các tháng)</b>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12" style="overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td rowspan="2" width="3%"><b>STT</b></td>
                                    <td class="text-center" rowspan="2"><b>Họ tên</b></td>
                                    <td class="text-center" rowspan="2"><b>Phòng, ban</b></td>
                                    <td class="text-center" colspan="{$sotuan}"><b>Danh sách tuần trong tháng {$month}</b></td>
                                </tr>
                                <tr>
                                    {foreach $arr_tuan as $key => $value}
                                    <td class="text-center"><b>Tuần {$value}</b></td>
                                    {/foreach}
                                </tr>
                            </thead> 
                                {$i=1}
                                {foreach $list_data as $key => $value}
                                {if $value['tuan'] gt 0}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left"><b>{$tencanbo[$value['canbo_id']]}</b></td>
                                    <td class="text-left"><b>{$phongban[$value['phong_id']]}</b></td>
                                    {foreach $arr_tuan as $value1}
                                    <td>
                                    {if $value['tuan'] eq $value1}
                                        {if $value['vanban_skh'] neq ''}
                                        <b>Số ký hiệu VB: {$value['vanban_skh']}</b>
                                        {else}
                                        <b>Nhiệm vụ phòng giao: </b>
                                        {/if} 
                                        {$value['kh_noidung']} 
                                    {/if} 
                                    </td>
                                    {/foreach}
                                </tr> 
                                {/if}
                                {/foreach}
                              
                        </table>        
                    </div>
                </div>
                              
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

