<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-6">
                <span class="font">
                    Nhập kết quả kế hoạch công tác
                </span>
            </div>
            <div class="col-md-6 sl3">
                <span class="4" style="font-weight: 550">
                    Tuần công tác: 
                </span>
                <label class="8 sl2">
                 {$arr}    
                 <input type="hidden" name="id" value="{$giatri.id}" />
                 <input type="hidden" name="date_danhgia2" value="{($giatri)?$giatri['date_danhgia2']:''}" />
                 <input type="hidden" name="date_danhgia3" value="{($giatri)?$giatri['date_danhgia3']:''}" />
                 <input type="hidden" name="date_danhgia4" value="{($giatri)?$giatri['date_danhgia4']:''}" />
                 <input type="hidden" name="date_danhgia5" value="{($giatri)?$giatri['date_danhgia5']:''}" />
                 <input type="hidden" name="date_danhgia6" value="{($giatri)?$giatri['date_danhgia6']:''}" />
                 <input type="hidden" name="date_danhgia7" value="{($giatri)?$giatri['date_danhgia7']:''}" />

                </label>
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">Thứ</th>
                                    <th width="38%" class="text-center">Nội dung sáng</th>
                                    <th width="38%" class="text-center">Nội dung chiều</th>
                                    <th width="20%" class="text-center">Kết quả thực hiện</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="4%" class="text-left">T2</td>
                                    <td width="32%" class="text-left">                                    
                                        {($giatri)?$giatri['viec2s']:''}
                                        {if $giatri['phatsinh2s'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh2s']} </span>  
                                            <i>({date_time($giatri['date_phatsinh2'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        {($giatri)?$giatri['viec2c']:''}
                                        {if $giatri['phatsinh2c'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh2c']}</span>
                                            <i>({date_time($giatri['date_phatsinh2'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        <textarea name="danhgia2" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 2">{($giatri)?$giatri['danhgia2']:''}</textarea>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="4%" class="text-left">T3</td>
                                    <td width="32%" class="text-left">                                    
                                        {($giatri)?$giatri['viec3s']:''}
                                        {if $giatri['phatsinh3s'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh3s']} </span>  
                                            <i>({date_time($giatri['date_phatsinh3'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        {($giatri)?$giatri['viec3c']:''}
                                        {if $giatri['phatsinh3c'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh3c']}</span>
                                            <i>({date_time($giatri['date_phatsinh3'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                         <textarea name="danhgia3" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 3">{($giatri)?$giatri['danhgia3']:''}</textarea>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="4%" class="text-left">T4</td>
                                    <td width="32%" class="text-left">                                    
                                        {($giatri)?$giatri['viec4s']:''}
                                        {if $giatri['phatsinh4s'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh4s']} </span>  
                                            <i>({date_time($giatri['date_phatsinh4'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        {($giatri)?$giatri['viec4c']:''}
                                        {if $giatri['phatsinh4c'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh4c']}</span>
                                            <i>({date_time($giatri['date_phatsinh4'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                         <textarea name="danhgia4" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 4">{($giatri)?$giatri['danhgia4']:''}</textarea>
                                    </td>
                                </tr> 

                                <tr>
                                    <td width="4%" class="text-left">T5</td>
                                    <td width="32%" class="text-left">                                    
                                        {($giatri)?$giatri['viec5s']:''}
                                        {if $giatri['phatsinh5s'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh5s']} </span>  
                                            <i>({date_time($giatri['date_phatsinh5'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        {($giatri)?$giatri['viec5c']:''}
                                        {if $giatri['phatsinh5c'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh5c']}</span>
                                            <i>({date_time($giatri['date_phatsinh5'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        <textarea name="danhgia5" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 5">{($giatri)?$giatri['danhgia5']:''}</textarea>
                                    </td>
                                </tr> 

                                <tr>
                                    <td width="4%" class="text-left">T6</td>
                                    <td width="32%" class="text-left">                                    
                                        {($giatri)?$giatri['viec6s']:''}
                                        {if $giatri['phatsinh6s'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh6s']} </span>  
                                            <i>({date_time($giatri['date_phatsinh6'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        {($giatri)?$giatri['viec6c']:''}
                                        {if $giatri['phatsinh6c'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh6c']}</span>
                                            <i>({date_time($giatri['date_phatsinh6'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        <textarea name="danhgia6" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 6">{($giatri)?$giatri['danhgia6']:''}</textarea>
                                    </td>
                                </tr> 

                                <tr>
                                    <td width="4%" class="text-left">T7</td>
                                    <td width="32%" class="text-left">                                    
                                        {($giatri)?$giatri['viec7s']:''}
                                        {if $giatri['phatsinh7s'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh7s']} </span>  
                                            <i>({date_time($giatri['date_phatsinh7'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        {($giatri)?$giatri['viec7c']:''}
                                        {if $giatri['phatsinh7c'] !=''}
                                            <br/>
                                            <span style="color: red"><b>Việc phát sinh: </b>{$giatri['phatsinh7c']}</span>
                                            <i>({date_time($giatri['date_phatsinh7'])})</i>
                                        {/if}
                                    </td>
                                    <td width="32%" class="text-left">
                                        <textarea name="danhgia7" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Đánh giá kết quả thực hiện nhiệm vụ ngày thứ 7">{($giatri)?$giatri['danhgia7']:''}</textarea>
                                    </td>
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <!-- <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-2">
                       <label style="color: blue">Đánh giá kết quả thực hiện tuần của lãnh đạo </label>
                    </div>
                    <div class="col-md-8">
                        <textarea name="tp_danhgia" class="form-control" rows="2" style="border: 1px solid #3c8dbc;" placeholder="Lãnh đạo phòng đánh giá kết quả thực hiện tuần của chuyên viên"  required >{($giatri)?$giatri['tp_danhgia']:''}</textarea>
                        <input type="hidden" name="date_tp_danhgia" value="{($giatri)?date_time($giatri['date_tp_danhgia']):''}">
                    </div> 
                    <div class="col-md-2">
                       <label style="color: blue">Điểm đánh giá</label>
                       <select name="tp_diem" required>
                        <option value="">Chọn điểm</option>
                        {for $j=1;$j<=10;$j++} 
                            <option value="{$j}" {if $giatri['tp_diem'] == $j} selected="selected"{/if}>{$j}</option>
                        {/for}
                       </select>
                    </div>  
                </div> -->

                <div class="row" style="height: 7px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Lưu lại</button> 
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

