<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-3">
                <span class="font">
                    Danh sách kế hoạch công tác tuần
                </span>
            </div>
            <div class="col-md-5 sl3">
                <span class="4" style="font-weight: 550">
                    Chọn tuần công tác: 
                </span>
                <label class="8 sl2">
                    <select name="tuan" style="width: 100%" class="form-control select2" onchange="this.form.submit()">
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option {if $tuan == $i} selected {/if} value="{$i}" >Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </label>
            </div>
            <div class="col-md-3 sl3">
            {if !empty($giatri)}
                <!-- <input type="hidden" name="tuan" value="{$tuan}"> -->
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
            {/if}
            </div>
            <div class="col-md-1 sl3">
                Biểu số 5
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="2">TT</th>
                                    <th class="text-left" rowspan="2">Cán Bộ</th>
                                    <th width="4%" class="text-center" rowspan="2">Văn phòng điện tử <br><br>Tối đa 60 điểm</th>
                                    <th width="32%" colspan="7">Ý Thức tổ chức kỷ luật - tối đa: 20 điểm</th>
                                    <th width="32%" colspan="8">Năng lực, kỹ năng thực hiện nhiệm vụ - tối đa: 10 điểm</th>
                                    <th width="4%" class="text-center" rowspan="2" style="border-left: 1px solid #fff">Điểm thưởng<br><br>tối đa 10 điểm</th>                 
                                </tr>
                                <tr>
                                    <th width="4%" class="text-center">Tổng điểm</th>
                                    <th width="4%" class="text-center">Chấp hành đường lối,...</th>
                                    <th width="4%" class="text-center">Không hách dịch, cửa quyền,...</th>
                                    <th width="4%" class="text-center">Có phẩm chất chính trị, đạo đức,...</th>
                                    <th width="4%" class="text-center">Thực hiện văn hóa nơi công sở,...</th>
                                    <th width="4%" class="text-center">Xây dựng hình ảnh,...</th>
                                    <th width="4%" class="text-center">Sử dụng hiệu quả thời gian làm việc,...</th>
                                    
                                    <th width="4%" class="text-center">Tổng điểm</th>
                                    <th width="4%" class="text-center">Thường xuyên học tập, rèn luyện,...</th>
                                    <th width="4%" class="text-center">Xây dựng kế hoạch công tác tuần,...</th>
                                    <th width="4%" class="text-center">Chỉ đạo, điều hành, kiểm soát,...</th>
                                    <th width="4%" class="text-center">Kiểm tra, đôn đốc việc thực hiện...</th>
                                    <th width="4%" class="text-center">Có năng lực quy tụ, tập hợp công chức,...</th>
                                    <th width="4%" class="text-center">Sử dụng thành thạo các phần mềm,...</th>
                                    <th width="4%" class="text-center">Thiết lập hồ sơ công việc đầy đủ...</th>
                                </tr>
                            </thead>
                            <tbody>
                            {$iii = 1}
                            {foreach $giatri as $diem}
                            {if in_array($diem.canbo_id,$id_lanhdao)}

                            <tr>
                                <td style="vertical-align: middle !important" rowspan="4">{$iii++}</td>
                                <td >
                               
                                <b style="color: red">Máy chấm <br>{$diem.diemtong}</b>
                                </td>
                                <td class="text-center"><b>
                                    {$diem.diem1}
                                </b></td>
                                <td class="text-center"><b>
                                   {$diem.diem2}
                                </b></td>
                                <td class="text-center">
                                    <b>{$diem.diem3}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem4}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem5}</b>
                                </td>
                                <td class="text-center">
                                     <b>{$diem.diem6}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem7}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem8}</b>
                                </td>
                                
                                <td class="text-center">
                                    <b>{$diem.diem10}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem11}</b>
                                </td>
                                <td class="text-center">
                                   <b>{$diem.diem12}</b>
                                </td>                               
                                <td class="text-center">
                                  <b>{$diem.diem13}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem14}</b>
                                </td>
                                <td class="text-center">
                                   <b>{$diem.diem15}</b>
                                </td>
                                <td class="text-center">
                                   <b>{$diem.diem16}</b>
                                </td>
                                <td class="text-center">
                                   <b>1<!-- {$diem.diem18} --></b>
                                </td>
                                <td class="text-center"><b>
                                    <b>{$diem.diem19}</b>
                                </b></td>
                                
                            </tr>

                            <tr>
                                <td >
                                <b style="color: blue">Cá nhân chấm </b><i style="color: blue">{if $diem.ngay_cv_cham > '2018-01-01'} ({date_time($diem.ngay_cv_cham)})
                                {/if}</i><br><b style="color: blue">{$diem.diem_tong}</b>
                                </td>
                                <td class="text-center"><b style="color: blue">
                                    {$diem.diem_1}
                                </b></td>
                                <td class="text-center"><b style="color: blue">
                                   {$diem.diem_2}
                                </b></td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_3}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_4}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_5}</b>
                                </td>
                                <td class="text-center">
                                     <b style="color: blue">{$diem.diem_6}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_7}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_8}</b>
                                </td>
                                
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_10}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_11}</b>
                                </td>
                                <td class="text-center">
                                   <b style="color: blue">{$diem.diem_12}</b>
                                </td>                               
                                <td class="text-center">
                                  <b style="color: blue">{$diem.diem_13}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_14}</b>
                                </td>
                                <td class="text-center">
                                   <b style="color: blue">{$diem.diem_15}</b>
                                </td>
                                <td class="text-center">
                                   <b style="color: blue">{$diem.diem_16}</b>
                                </td>
                                <td class="text-center">
                                   <b style="color: blue">{$diem.diem_18}</b>
                                </td>
                                <td class="text-center"><b style="color: blue">
                                    <b>{$diem.diem_19}</b>
                                </b></td>
                                
                            </tr>


                            <tr>
                                <td rowspan="2"><a href="{$url}xemkehoachcongtac/{$tuan}/{$diem.canbo_id}/{$diem.danhgia_id}" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết"> {if isset($canbophong[$diem.canbo_id])}
                                {if $diem.active ==2}Trưởng đơn vị chấm 
                                {if $diem.ngay_cham > '2018-01-01'} ({date_time($diem.ngay_cham)})
                                {/if}
                                <br>{/if}
                                <b> {$canbophong[$diem.canbo_id]}</b> {/if}</a>
                                
                                <br>
                                <b style="color: red">{if $diem.active ==1}
                                        {($diem.diem_tong)?$diem.diem_tong:0}
                                    {else}{$diem.diem_tong_tp}{/if} Điểm</b>
                                </td>
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                    <select name="diem_1_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<61;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_1==$i)?selected:''}>{$i}.0</option>
                                        {if $j<60 }<option value="{$j}" {($diem.diem_1==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_1_tp}{/if}
                                </b></td>
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                        {($diem.diem_2)?$diem.diem_2:0}
                                    {else}{$diem.diem_2_tp}{/if}
                                </b></td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_3_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_3==$i)?selected:''}>{$i}.0</option>
                                        {if $j<5 }<option value="{$j}" {($diem.diem_3==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_3_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_4_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_4==$i)?selected:''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j}" {($diem.diem_4==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_4_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_5_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_5==$i)?selected:''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {($diem.diem_5==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_5_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_6_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_6==$i)?selected:''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {($diem.diem_6==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_6_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_7_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_7==$i)?selected:''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {($diem.diem_7==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_7_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_8_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_8==$i)?selected:''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {($diem.diem_8==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_8_tp}{/if}
                                </td>
                                
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                        {($diem.diem_10)?$diem.diem_10:0}
                                    {else}{$diem.diem_10_tp}{/if}
                                </b></td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_11_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_11==$i)?selected:''}>{$i}.0</option>
                                        {if $j< 2 }<option value="{$j}" {($diem.diem_11==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_11_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_12_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_12==$i)?selected:''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {($diem.diem_12==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_12_tp}{/if}
                                </td>                               
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_13_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_13==$i)?selected:''}>{$i}.0</option>
                                        {if $j< 2 }<option value="{$j}" {($diem.diem_13==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_13_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_14_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_14==$i)?selected:''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {($diem.diem_14==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_14_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_15_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_15==$i)?selected:''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {($diem.diem_15==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_15_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_16_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_16==$i)?selected:''}>{$i}.0</option>
                                        {if $j< 1 }<option value="{$j}" {($diem.diem_16==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_16_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_18_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_18==$i)?selected:''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {($diem.diem_18==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_18_tp}{/if}
                                </td>
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                    <select  name="diem_19_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<11;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_19==$i)?selected:''}>{$i}.0</option>
                                        {if $j<10 }<option value="{$j}" {($diem.diem_19==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_19_tp}{/if}
                                </b></td>
                            </tr>
                            <tr>
                                <td {if $diem.active ==1 && $quyen < 7} colspan="16" {else} colspan="18" {/if}>
                                    {if $diem.active ==1}
                                    <textarea class="form-control" name="nhanxet_{$diem.danhgia_id}" style=" font-size: 16px" rows=5 placeholder="Nhận xét của trưởng đơn vị"></textarea>
                                    {else}
                                        <span style="font-size: 16px">{$diem.nhan_xet}</span>
                                    {/if}
                                </td>
                                {if $diem.active ==1 && $quyen < 7 && $phong_id == $FK_iMaPhongHD }
                                <td colspan="2" class="text-center">
                                    <button type="button" name="chamdiem" value="{$diem.danhgia_id}">Chấm điểm</button>
                                </td>
                                {/if}
                            </tr>
                            {/if}  
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>   

                <div class="row">
                        <div class="col-md-12" style="overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="2">TT</th>
                                    <th class="text-left" rowspan="2">Cán Bộ</th>
                                    <th width="4%" class="text-center" rowspan="2">Văn phòng điện tử <br><br>Tối đa 60 điểm</th>
                                    <th width="32%" colspan="7">Ý Thức tổ chức kỷ luật - tối đa: 20 điểm</th>
                                    <th width="32%" colspan="6">Năng lực, kỹ năng thực hiện nhiệm vụ - tối đa: 10 điểm</th>
                                    <th width="4%" class="text-center" rowspan="2" style="border-left: 1px solid #fff">Điểm thưởng<br><br>tối đa 10 điểm</th>                 
                                </tr>
                                <tr>
                                    <th width="4%" class="text-center">Tổng điểm</th>
                                    <th width="4%" class="text-center">Chấp hành tốt chủ trương,...</th>
                                    <th width="4%" class="text-center">Không hách dịch, cửa quyền,...</th>
                                    <th width="4%" class="text-center">Có phẩm chất chính trị, đạo đức,...</th>
                                    <th width="4%" class="text-center">Thực hiện văn hóa nơi công sở,...</th>
                                    <th width="4%" class="text-center">Xây dựng hình ảnh, giữ gìn uy tín...</th>
                                    <th width="4%" class="text-center">Sử dụng hiệu quả thời gian làm việc,... </th>
                                    
                                    <th width="4%" class="text-center">Tổng điểm</th>
                                    <th width="4%" class="text-center">Thường xuyên học tập, rèn luyện,...</th>
                                    <th width="4%" class="text-center">Tham gia xây dựng kế hoạch công tác của đơn vị,...</th>
                                    <th width="4%" class="text-center">- Chủ động triển khai thực hiện nhiệm vụ, công việc,...</th>
                                    <th width="4%" class="text-center">Sử dụng thành thạo các phần mềm,...</th>
                                    <th width="4%" class="text-center">Thiết lập hồ sơ công việc đầy đủ theo các đầu mục...</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <!-- {$iij = 1} -->
                            {foreach $giatri as $diem}
                             {if !in_array($diem.canbo_id,$id_lanhdao)}
                             <tr>
                                <td style="vertical-align: middle !important" rowspan="4">{$iii++}</td>
                                <td >
                                    <b style="color: red">Máy chấm<br>{$diem.diemtong}</b>
                                </td>
                                <td class="text-center"><b>
                                    {$diem.diem1}
                                </b></td>
                                <td class="text-center"><b>
                                   {$diem.diem2}
                                </b></td>
                                <td class="text-center">
                                    <b>{$diem.diem3}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem4}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem5}</b>
                                </td>
                                <td class="text-center">
                                     <b>{$diem.diem6}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem7}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem8}</b>
                                </td>
                                
                                <td class="text-center">
                                    <b>{$diem.diem10}</b>
                                </td>
                                <td class="text-center">
                                    <b>{$diem.diem11}</b>
                                </td>
                                <td class="text-center">
                                   <b>{$diem.diem12}</b>
                                </td>                               
                                
                                <td class="text-center">
                                    <b>{$diem.diem14}</b>
                                </td>
                                <td class="text-center">
                                   <b>{$diem.diem15}</b>
                                </td>
                                <td class="text-center">
                                   <b>{$diem.diem16}</b>
                                </td>
                                
                                <td class="text-center"><b>
                                    <b>{$diem.diem19}</b>
                                </b></td>
                                
                            </tr>

                             <tr> 
                                <td>                              
                                <b style="color: blue">Cá nhân chấm</b>  {if $diem.ngay_cv_cham > '2018-01-01'} ({date_time($diem.ngay_cv_cham)})
                                {/if}<br><b style="color: blue">{$diem.diem_tong}</b>
                                </td>
                                <td class="text-center"><b style="color: blue">
                                    {$diem.diem_1}
                                </b></td>
                                <td class="text-center"><b style="color: blue">
                                   {$diem.diem_2}
                                </b></td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_3}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_4}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_5}</b>
                                </td>
                                <td class="text-center">
                                     <b style="color: blue">{$diem.diem_6}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_7}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_8}</b>
                                </td>
                                
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_10}</b>
                                </td>
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_11}</b>
                                </td>
                                <td class="text-center">
                                   <b style="color: blue">{$diem.diem_12}</b>
                                </td>                               
                                
                                <td class="text-center">
                                    <b style="color: blue">{$diem.diem_14}</b>
                                </td>
                                <td class="text-center">
                                   <b style="color: blue">{$diem.diem_15}</b>
                                </td>
                                <td class="text-center">
                                   <b style="color: blue">{$diem.diem_16}</b>
                                </td>
                                
                                <td class="text-center"><b style="color: blue">
                                    <b>{$diem.diem_19}</b>
                                </b></td>
                                
                            </tr>

                            <tr>
                                <!-- <td style="vertical-align: top !important" rowspan="2">{$ii++}</td> -->
                                <td rowspan="2"><a href="{$url}xemkehoachcongtac/{$tuan}/{$diem.canbo_id}/{$diem.danhgia_id}" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết"> {if isset($canbophong[$diem.canbo_id])}
                                {if $diem.active ==2}Trưởng đơn vị chấm 
                                 {if $diem.ngay_cham > '2018-01-01'} ({date_time($diem.ngay_cham)})
                                {/if}
                                <br>{/if} 
                                <b>{$canbophong[$diem.canbo_id]} </b>{/if}</a>
                                <br>
                                <b style="color: red">{if $diem.active ==1}
                                        {($diem.diem_tong)?$diem.diem_tong:0}
                                    {else}{$diem.diem_tong_tp}{/if} Điểm</b>
                                </td>
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                    <select name="diem_1_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<61;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_1==$i)?selected:''}>{$i}.0</option>
                                        {if $j<60 }<option value="{$j}" {($diem.diem_1==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_1_tp}{/if}
                                </b></td>
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                        {($diem.diem_2)?$diem.diem_2:0}
                                    {else}{$diem.diem_2_tp}{/if}
                                </b></td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_3_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_3==$i)?selected:''}>{$i}.0</option>
                                        {if $j<5 }<option value="{$j}" {($diem.diem_3==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_3_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_4_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_4==$i)?selected:''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j}" {($diem.diem_4==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_4_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_5_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_5==$i)?selected:''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {($diem.diem_5==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_5_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_6_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_6==$i)?selected:''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {($diem.diem_6==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_5_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_7_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_7==$i)?selected:''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {($diem.diem_7==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_7_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_8_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_8==$i)?selected:''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {($diem.diem_8==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_8_tp}{/if}
                                </td>
                                
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                        {($diem.diem_10)?$diem.diem_10:0}
                                    {else}{$diem.diem_10_tp}{/if}
                                </b></td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_11_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_11==$i)?selected:''}>{$i}.0</option>
                                        {if $j< 2 }<option value="{$j}" {($diem.diem_11==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_11_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_12_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_12==$i)?selected:''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {($diem.diem_12==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_12_tp}{/if}
                                </td>                               
                                <!-- <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_13_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_13==$i)?selected:''}>{$i}.0</option>
                                        {if $j< 2 }<option value="{$j}" {($diem.diem_13==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_13_tp}{/if}
                                </td> -->
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_14_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_14==$i)?selected:''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {($diem.diem_14==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_14_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_15_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_15==$i)?selected:''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {($diem.diem_15==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_15_tp}{/if}
                                </td>
                                <td class="text-center">
                                    {if $diem.active ==1}
                                    <select  name="diem_16_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_16==$i)?selected:''}>{$i}.0</option>
                                        {if $j< 2 }<option value="{$j}" {($diem.diem_16==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_16_tp}{/if}
                                </td>
                                
                                <td class="text-center"><b>
                                    {if $diem.active ==1}
                                    <select  name="diem_19_tp_{$diem.danhgia_id}">
                                        {for $i=0;$i<11;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {($diem.diem_19==$i)?selected:''}>{$i}.0</option>
                                        {if $j<10 }<option value="{$j}" {($diem.diem_19==$j)?selected:''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}{$diem.diem_19_tp}{/if}
                                </b></td>
                            </tr>
                            <tr>
                                <td {if $diem.active ==1 && $quyen < 7} colspan="14" {else} colspan="16" {/if} >
                                	{if $diem.nhan_xet_1 !=''}
                                    {if !empty($diem.ngay_nhanxet)}<b>Nhận xét của phó phòng {$canbophong[$diem.phophong_id]}</b> <i>({date_time($diem.ngay_nhanxet)})</i>: {/if}<span style="font-size: 14px;">{$diem.nhan_xet_1} <br></span>
                                    {/if}
                                    {if $diem.active ==1}
                                    <b>Nhận xét của trưởng phòng: </b><textarea class="form-control" name="nhanxet_{$diem.danhgia_id}" style="font-size: 16px" rows=5 placeholder="Nhận xét của trưởng đơn vị"></textarea>
                                    {else}
                                        <b>Nhận xét của trưởng phòng: </b> <span style="font-size: 14px">{$diem.nhan_xet}</span>
                                    {/if}
                                </td>
                                {if $diem.active ==1 && $quyen < 7 && $phong_id == $FK_iMaPhongHD}
                                <td colspan="2" class="text-center">
                                    <button type="button" name="chamdiem" value="{$diem.danhgia_id}">Chấm điểm</button>
                                </td>
                                {/if}
                            </tr>
                            {/if}
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>   
                
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //$('td').css('vertical-align','top');
    });

    $(document).ready(function(){
        var url = window.location.href;
        $(document).on('click','button[name=chamdiem]',function(){
            var danhgia_id = $(this).val();
            var nhan_xet  = $('textarea[name="nhanxet_' + danhgia_id + '"]').val();
            var diem_1_tp  = $('select[name="diem_1_tp_' + danhgia_id + '"] option:selected').val();
            var diem_3_tp  = $('select[name="diem_3_tp_' + danhgia_id + '"] option:selected').val();
            var diem_4_tp  = $('select[name="diem_4_tp_' + danhgia_id + '"] option:selected').val();
            var diem_5_tp  = $('select[name="diem_5_tp_' + danhgia_id + '"] option:selected').val();
            var diem_6_tp  = $('select[name="diem_6_tp_' + danhgia_id + '"] option:selected').val();
            var diem_7_tp  = $('select[name="diem_7_tp_' + danhgia_id + '"] option:selected').val();
            var diem_8_tp  = $('select[name="diem_8_tp_' + danhgia_id + '"] option:selected').val();
            var diem_9_tp  = $('select[name="diem_9_tp_' + danhgia_id + '"] option:selected').val();
            var diem_11_tp  = $('select[name="diem_11_tp_' + danhgia_id + '"] option:selected').val();
            var diem_12_tp  = $('select[name="diem_12_tp_' + danhgia_id + '"] option:selected').val();
            var diem_13_tp  = $('select[name="diem_13_tp_' + danhgia_id + '"] option:selected').val();
            if(typeof(diem_13_tp)  === 'undefined' )diem_13_tp=0;
            var diem_14_tp  = $('select[name="diem_14_tp_' + danhgia_id + '"] option:selected').val();
            var diem_15_tp  = $('select[name="diem_15_tp_' + danhgia_id + '"] option:selected').val();
            var diem_16_tp  = $('select[name="diem_16_tp_' + danhgia_id + '"] option:selected').val();
            var diem_18_tp  = $('select[name="diem_18_tp_' + danhgia_id + '"] option:selected').val();
            var diem_19_tp  = $('select[name="diem_19_tp_' + danhgia_id + '"] option:selected').val();
            var diem_2_tp = parseFloat(diem_3_tp)+parseFloat(diem_4_tp)+parseFloat(diem_5_tp)+parseFloat(diem_6_tp)+parseFloat(diem_7_tp)+parseFloat(diem_8_tp);//+parseFloat(diem_9_tp);
            if(parseFloat(diem_18_tp) >0){
                $diem_18_tp = parseFloat(diem_18_tp);
            }else{
                $diem_18_tp=0;
            }
            var diem_10_tp = parseFloat(diem_11_tp)+parseFloat(diem_12_tp)+parseFloat(diem_13_tp)+parseFloat(diem_14_tp)+parseFloat(diem_15_tp)+parseFloat(diem_16_tp)+$diem_18_tp;
            var diem_tong_tp = parseFloat(diem_1_tp)+parseFloat(diem_2_tp)+parseFloat(diem_10_tp)+parseFloat(diem_19_tp);
            //alert('1: '+diem_1_tp +' -2: '+diem_2_tp + ' -3: '+diem_3_tp + ' -4: '+diem_4_tp +  ' -5: '+diem_5_tp + ' -6: '+diem_6_tp + ' -7: '+diem_7_tp + ' -8: '+diem_8_tp + ' -9: '+diem_9_tp + ' -10: '+diem_10_tp + ' -11: '+diem_11_tp + ' -12: '+diem_12_tp + ' -13: '+diem_13_tp + ' -14: '+diem_14_tp + ' -15: '+diem_15_tp + ' -16: '+diem_16_tp + ' -18: '+diem_18_tp + ' -19: '+diem_19_tp );
            $.ajax({
                url:url,
                type:'POST',
                data:{
                    action:'chamdiem',
                    danhgia_id:danhgia_id,
                    diem_1_tp:diem_1_tp,
                    diem_2_tp:diem_2_tp,
                    diem_3_tp:diem_3_tp,
                    diem_4_tp:diem_4_tp,
                    diem_5_tp:diem_5_tp,
                    diem_6_tp:diem_6_tp,
                    diem_7_tp:diem_7_tp,
                    diem_8_tp:diem_8_tp,
                    diem_9_tp:diem_9_tp,
                    diem_10_tp:diem_10_tp,
                    diem_11_tp:diem_11_tp,
                    diem_12_tp:diem_12_tp,
                    diem_13_tp:diem_13_tp,
                    diem_14_tp:diem_14_tp,
                    diem_15_tp:diem_15_tp,
                    diem_16_tp:diem_16_tp,
                    diem_18_tp:diem_18_tp,
                    diem_19_tp:diem_19_tp,
                    diem_tong_tp:diem_tong_tp,
                    nhan_xet:nhan_xet
                },
                success:function(repon){ 
                    var result = JSON.parse(repon);
                    if(result>0)
                    {   
                        location.reload();
                        showMessage('Bạn đã chấm điểm thành công','info');
                    }


                }
            });
        });
    });
</script>

