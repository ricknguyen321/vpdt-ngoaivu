<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-9">
                    <h3 class="font">
                        Thống kê báo cáo đánh giá kết quả thực hiện nhiệm vụ công tác tuần
                    </h3>
                </div>
                <div class="col-sm-3">
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{$url}thongkebaocao" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Tuần bắt đầu  </label>

                                                    <div class="col-sm-8">
                                                        <select name="tuanbd" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $tuanbd == 0}selected ="selected"{/if}>-- Chọn tuần bắt đầu --</option>
                                                            {foreach $arr as $key_tuan => $value_tuan}
                                                                <option value="{$key_tuan+1}"  {if $tuanbd == $key_tuan+1}selected ="selected"{/if}> Tuần {$key_tuan +1}: &nbsp;&nbsp;&nbsp;{$value_tuan}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Tuần kết thúc</label>

                                                    <div class="col-sm-8">
                                                        <select name="tuankt" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $tuanbd == 0}selected ="selected"{/if}>-- Chọn tuần kết thúc --</option>
                                                            {foreach $arr as $key_tuan => $value_tuan}
                                                                <option value="{$key_tuan +1 }" {if $tuankt == $key_tuan+1}selected ="selected"{/if}> Tuần {$key_tuan +1}: &nbsp;&nbsp;&nbsp;{$value_tuan}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Phòng, đơn vị</label>

                                                    <div class="col-sm-8">
                                                        <select name="phongban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $phong == 0}selected ="selected"{/if}>-- Chọn phòng, đơn vị --</option>
                                                             {foreach $phongban as $key_phong => $value_phong}
                                                                <option value="{$key_phong}" {if $phong == $key_phong}selected ="selected"{/if}> {$value_phong}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Cán bộ</label>

                                                    <div class="col-sm-8">
                                                        <select name="canbo" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" >-- Chọn cán bộ --</option>
                                                            {foreach $danhsachcbphong as $value_canbo}
                                                                <option value="{$value_canbo.PK_iMaCB}" {if $canbo == $value_canbo.PK_iMaCB }selected ="selected"{/if}> {$value_canbo.sHoTen}</option>
                                                            {/foreach}
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Mức đánh giá :</label>
                                            
                                                    <div class="col-sm-8">
                                                        <select name="danhgia" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $loai == 0}selected ="selected"{/if}>-- Chọn mức đánh giá --</option>
                                                            <option value="1"  {if $loai == 1}selected ="selected"{/if}>-- Hoàn thành xuất sắc nhiệm vụ --</option>
                                                            <option value="2"  {if $loai == 2}selected ="selected"{/if}>-- Hoàn thành tốt nhiệm vụ --</option>
                                                            <option value="3"  {if $loai == 3}selected ="selected"{/if}>-- Hoàn thành nhiệm vụ --</option>
                                                            <option value="4" {if $loai == 4}selected ="selected"{/if}>-- Hoàn thành còn hạn chế về năng lực --</option>
                                                            <option value="5" {if $loai == 5}selected ="selected"{/if}>-- Không hoàn thành nhiệm vụ --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  -->
                                            <!-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-5 control-label">Chọn LĐ phòng, sở 
                                                    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="lanhdaodonvi" id="lanhdaodonvi" value="1" onchange="an_hien()">
                                                    </label>
                                            
                                                    <label for="" id="an_hien" class="col-sm-3 control-label">Chọn tháng                       
                                                    </label>
                                            
                                                    <div class="col-sm-4" id="an_hien1"> 
                                                        <select name="thang"  class="form-control ngayhan select2" style="width:100%">
                                                            {for $i=1;$i<=12;$i++}
                                                            <option value="{$i}" {if $i == 1}selected ="selected"{/if}>Tháng {$i}</option>
                                                            {/for}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->


                                            <div class="col-md-12" >
                                                <button class="btn btn-primary  pull-right" name="thongke" value="thongkebaocao" >Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-12 deletepadding">
                   <!--  {$phantrang} -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center">STT</th>
                                    <th width="12%" class="text-center">Cán bộ</th>
                                    <th width="12%" class="text-center">phòng</th>
                                    <th width="5%" class="text-center">Tuần</th>
                                    <th width="8%" class="text-center">Số điểm</th>
                                    <th width="" class="text-center">Đánh giá của lãnh đạo đơn vị</th>
									<!-- <th width="12%" class="text-center">Kết quả thực hiện</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            {$i = 1}
                            {foreach $kehoach as $kh}
                            <tr>
                               <td class="text-left">{$i++}</td>
                               <td>
                                <a href="{$url}xemkehoachcongtac/{$kh.tuan}/{$kh.canbo_id}/{$kh.danhgia_id}" target="_blank" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Click chuột để xem chi tiết">
                               {$tencanbo[$kh.canbo_id]}
                                </a></td>
                               <td class="text-left">{$phongban[$kh.phong_id]}</td>
                               <td class="text-center"><b>{$kh.tuan}</b></td>
                               <td class="text-left">{$kh.diem_tong_tp}</td>
                               <td class="text-left">
                                  {if $kh.nhan_xet_1 !=''}{$kh.nhan_xet_1} <br>{/if}
                                  {if $kh.nhan_xet !=''}<b>Nhận xét của trưởng đơn vị:</b> {$kh.nhan_xet}{/if}
                               </td>
                               <!-- <td class="text-left">
                                {if $kh.diem_tong_tp >= 90 && $kh.diem_19_tp > 0}
                                    <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>
                                {/if}
                                {if $kh.diem_tong_tp >= 80 && ($kh.diem_19_tp == 0 || $kh.diem_tong_tp < 90)}
                                    <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                {/if}
                                {if $kh.diem_tong_tp >= 70 && $kh.diem_tong_tp < 80}
                                    <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                {/if}                    
                                {if $kh.diem_tong_tp >= 60 && $kh.diem_tong_tp < 70 }
                                    <span style="color: #110000">Hoàn thành nhiệm vụ, còn hạn chế về năng lực</span>
                                {/if}  
                                {if $kh.diem_tong_tp < 60 }                 
                                    <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                {/if}
                               </td> -->
                           </tr>
                           {/foreach}
                            </tbody>
                        </table>
                        <!-- <div><b>Tổng văn bản: {$count}</b></div> -->
                    </div>
                </div>
                <div class="col-sm-12 deletepadding">
                   <!--  {$phantrang} -->
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

<script>
    $("#an_hien").hide();
    $("#an_hien1").hide();
    $(document).ready(function() {
        $(document).on('click','button[name=thongke]',function(){
            $(this).addClass('hide');
        });
    });
    var url = window.location.href;
    $(document).on('change','select[name=phongban]',function(){
    var maphong = $(this).val();
    $.ajax({
        url:url,
        type:'post',
        data:{
            action:'getmaphong',
            maphong:maphong
        },
        success:function(respon){
            var result = JSON.parse(respon);
            var tong = result.length;
            var html = '<option value="0">-- Chọn cán bộ --</option>';
            for(var i=0;i<tong;i++)
            {
                html+="<option value='"+result[i]['PK_iMaCB']+"'>"+result[i]['sHoTen']+"</option>";
            }
            $('select[name=canbo]').html(html);
            $('.select2').select2();
        }

    });
   });

function an_hien(){
    if(document.getElementById('lanhdaodonvi').checked) {
        $("#an_hien").show();
        $("#an_hien1").show();
    }else {
        $("#an_hien").hide();
        $("#an_hien1").hide();
    }
}
</script>
