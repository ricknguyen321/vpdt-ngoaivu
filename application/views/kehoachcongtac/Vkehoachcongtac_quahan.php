<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="row">
            <div class="col-md-12">
                <span style="color: red;font-weight:900;font-size: 18px ">Danh sách công việc quá hạn</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="4%" class="text-center" rowspan="2">STT</th>
                            {if $iQuyenHan_DHNB < 7}
                            <th width="8%" class="text-center" rowspan="2">Cán bộ thực hiện</th>
                            {/if}
                            <th width="8%" class="text-center" rowspan="2">Trách nhiệm giải quyết</th>
                            <th width="8%" class="text-center" rowspan="2">Ngày nhận</th>
                            <th width="6%" class="text-center" rowspan="2">Số ký hiệu</th>
                            <th width="" class="text-center" rowspan="2">Nội dung</th>
                            <th width="8%" class="text-center" rowspan="2">Hạn xử lý</th>
                            <th width="15%" class="text-center" colspan="3">Kết quả thực hiện</th>
                        </tr>
                        <tr>
                            <th width="5%" class="text-center" >Hoàn Thành</th>
                            <th width="5%" class="text-center">Đang thực hiện</th>
                            <th width="5%" class="text-center">Tình trạng</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>I</th>
                        <th colspan="9">NHIỆM VỤ LÃNH ĐẠO GIAO, NHIỆM VỤ THƯỜNG XUYÊN, NHIỆM VỤ KHÁC
                        
                        </th>
                    </tr>
                    {$i = 1}
                    {foreach $list_kh as $kh}
                    <tr>
                        <td class="text-center">{$i++}</td>
                        {if $iQuyenHan_DHNB < 7}
                        <td width="8%" class="text-center">
                            {foreach $canbo as $cb}
                                {if $kh.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                            {/foreach}
                            {if $kh.canbo_id != $kh.user_input}
                                {foreach $canbo as $cb}
                                    {if $kh.lanhdao_id == $cb.PK_iMaCB}
                                        <span style="color: blue"><i>(LĐ giao việc {$cb.sHoTen})</i></span>
                                    {/if}
                                {/foreach}
                            {else}
                                <span style="color: blue">(Cán bộ tự xây dựng kế hoạch)</span>
                            {/if}

                        </td>
                        {/if}
                        <td>
                            {if $kh.loai_kh ==5}
                                Nhiệm vụ khác
                            {/if}
                            {if $kh.loai_kh ==3 or $kh.loai_kh ==4}
                                Nhiệm vụ thường xuyên
                            {/if}
                            {if $kh.loai_kh ==2 && $iQuyenHan_DHNB == 8}
                               {if $kh.canbo_id == $PK_iMaCB} Lãnh đạo giao {else} Phối hợp giải quyết {/if}
                            {/if}
                            {if $kh.loai_kh ==2 && $iQuyenHan_DHNB < 8}
                               Lãnh đạo giao
                            {/if}
                            
                            {if $iQuyenHan_DHNB >= 7}
                            {if $kh.canbo_id != $kh.user_input}
                                {foreach $canbo as $cb}
                                    {if $kh.lanhdao_id == $cb.PK_iMaCB}
                                        <span style="color: blue"><i>(LĐ giao việc {$cb.sHoTen})</i></span>
                                    {/if}
                                {/foreach}
                            {else}
                                {if $iQuyenHan_DHNB == 7 || $iQuyenHan_DHNB == 11 }
                                <span style="color: blue"><br> (Cán bộ xây dựng kế hoạch:
                                    {foreach $canbo as $cb}
                                        {if $kh.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach})
                                </span>
                                {else}
                                <span style="color: blue">(Cán bộ xây dựng kế hoạch)</span>
                                {/if}
                            {/if}
                            {/if}
                        </td>
                        <td>{date_select($kh.ngay_nhan)}</td>
                        <td class="text-center">{$kh.vanban_skh}</td>
                        <td class="text-left" align="text-left">
                            
                            {$kh.kh_noidung}

                            {if $kh.ykien_pp !=''}
                            <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                <br><b>Ý kiến của {foreach $canbo as $cb}
                                        {if $kh.lanhdao_id == $cb.PK_iMaCB}{if $cb.iQuyenHan_DHNB ==7}phó phòng {else}trưởng phòng {/if} {$cb.sHoTen}{/if}
                                    {/foreach}:</b><br> <i>{$kh.ykien_pp} ({date_time($kh.ngay_ykien)})</i>
                            {/if}
                            {if $kh.ykien_tp !=''}
                            <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                                <br><b>Ý kiến của lãnh đạo đơn vị {foreach $canbo as $cb}
                                        {if $truongdonvi == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                                    {/foreach}:</b><br> <i>{$kh.ykien_tp} ({date_time($kh.ngay_danhgia)})</i>
                            {/if}
                            
                            {if $kh.khokhan_vuongmac != ''}
                            <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                            <br>
                            <label>Lý do chưa hoàn thành: </label>
                            <span style="color: red">{$kh.khokhan_vuongmac} - {date_time($kh.ngay_hoanthanh)}</span>
                            {/if}

                            {if $kh.ket_qua_hoanthanh != ''}
                            <hr style="margin-bottom: -8px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                            <br>
                            <label>Kết quả hoàn thành: </label>
                            <span style="color: blue">{$kh.ket_qua_hoanthanh} - {date_time($kh.ngay_hoanthanh)}</span>
                            {/if} 
                            {if isset($kh.filename)}<br>
                            <label>Tệp tin đính kèm: </label>
                            {foreach $kh.filename as $file}
                                {if $file !=''}<a href="kehoach_2018/{$file}"> [File] </a>{/if}
                            {/foreach}
                            {/if}
{$kiemtra=1}
{if isset($kh.file_phoihop)}
{if isset($kh.file_phoihop.0.canbo_id)}<br><i> Danh sách phối hợp:</i>{/if}
{foreach $kh.file_phoihop as $phoihop}    
<br> - <b>{$array_cb[$phoihop.canbo_id]}: </b>{$phoihop.lydo} &nbsp;&nbsp; 
{if $phoihop.file_upload !=''}<a href="kehoach_2018/{$phoihop.file_upload}">[File] </a>{/if}

{if $phoihop.canbo_id == $PK_iMaCB && $phoihop.active == 1 && $phoihop.chutri == 2}{$kiemtra=2}{/if}
{/foreach}
{/if}
                            {if $kh.ketluan_pp != ''}<hr style="margin-bottom: 6px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                            <b>Nhận xét của {foreach $canbo as $cb}
                                        {if $kh.lanhdao_id == $cb.PK_iMaCB}{if $cb.iQuyenHan_DHNB ==7}phó phòng {else}trưởng phòng {/if} {$cb.sHoTen}{/if}
                                    {/foreach}:</b><br> 
                            <span style="color: blue;">{$kh.ketluan_pp}</span>
                            {/if}

                            {if $kh.danh_gia != ''}<hr style="margin-bottom: 6px;margin-top: 4px;color:#119090; background-color:#119090; height:1px; border:none;" />
                            <label>Kết luận của trưởng đơn vị: </label>
                            <span style="color: blue">{$kh.danh_gia} - {date_time($kh.ngay_danhgia)}</span>
                            {if $kh.sangtao==2} 
                                <br><span style="color: red"><b>Có tính sáng tạo</b></span>{/if}
                            {/if}
                            {if $kh.chatluong==2} 
                                <br><span style="color: red"><b>Chất lượng chưa đảm bảo</b></span>
                            {/if}
                        </td>
                        <td class="text-center">{if $kh.ngay_han >'2017-01-01'}{date_select($kh.ngay_han)}{/if}</td>
                        <td class="text-center" style="color: blue;font-weight: bold;">{if $kh.active == 5}X{/if}</td>
                        <td class="text-center" style="color: blue;font-weight: bold;">{if $kh.active < 5}X{/if}</td>
                        <td class="text-center" style="color: green"> 
                            {if ($kh.ngay_han >= $date_for_friday && $kh.active < 5) || ($kh.ngay_han >= date_insert($kh.ngay_hoanthanh)  && $kh.active == 5) || $kh.ngay_han < '2017-01-01' }Trong hạn{/if}

                            {if $kh.ngay_han < $date_for_friday && $kh.active < 5 && $kh.ngay_han >'2017-01-01'}<span style="color: red">Quá hạn </span>{/if}

                        </td>
                    </tr>
                    {/foreach}
                    <tr>
                        <th>II</th>
                        <th colspan="9">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ</th>
                    </tr>
                    <tr>
                        <th>A</th>
                        <th colspan="9">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ CÓ YÊU CẦU VĂN BẢN ĐẦU RA</th>
                    </tr>
                    {foreach $list_kh1 as $kh1}
                    <tr>
                        <td class="text-center">{$i++}</td>
                        {if $iQuyenHan_DHNB < 7}
                        <td width="8%" class="text-center">
                            {foreach $canbo as $cb}
                                {if $kh1.canbo_id == $cb.PK_iMaCB} {$cb.sHoTen}{/if}
                            {/foreach}
                        </td>
                        {/if}
                        <td>
                            {if $kh1.thuc_hien == 1}Chỉ Đạo{/if}
                            {if $kh1.thuc_hien == 2}Phối hợp{/if}
                            {if $kh1.thuc_hien == 3}Giải quyết chính{/if}
                        </td>
                        <td>{date_select($kh1.ngay_nhan)}</td>
                        <td class="text-center">{$kh1.vanban_skh}
                            <br>
                                    <span style="color: red">(số đến: {laySoDen('PK_iMaVBDen',$kh1.vanban_id,'tbl_vanbanden')})</span>
                        </td>
                        <td class="text-left" align="text-left">
                            {if $kh1.active >= 1  && $kh1.vanban_skh != ''}
                                {if $kh1.thuc_hien ==3 && $kh1.trangthai_vbd == 1}
                                <a href="giaymoichuyenvienchutrixuly/{$kh1.vanban_id}">
                                {/if}
                                {if $kh1.thuc_hien ==2 && $kh1.trangthai_vbd == 1}
                                <a href="giaymoichuyenvienxuly/{$kh1.vanban_id}">
                                {/if}
                                {if $kh1.thuc_hien ==1 && $kh1.trangthai_vbd == 1}
                                <a href="quytrinhgiaymoi/{$kh1.vanban_id}">
                                {/if}

                                {if $kh1.thuc_hien ==3 && $kh1.trangthai_vbd == 0}
                                <a href="chuyenvienchutrixuly?vbd={$kh1.vanban_id}">
                                {/if}
                                {if $kh1.thuc_hien ==2 && $kh1.trangthai_vbd == 0}
                                <a href="chuyenvienxuly?vbd={$kh1.vanban_id}">
                                {/if}
                                {if $kh1.thuc_hien ==1 && $kh1.trangthai_vbd == 0}
                                <a href="quatrinhxuly/{$kh1.vanban_id}">
                                {/if}
                            {$kh1.kh_noidung}</a>
                            {else}
                            {$kh1.kh_noidung}
                            {/if}
                        </td>
                        <td class="text-center">{if $kh1.ngay_han >'2017-01-01'}{date_select($kh1.ngay_han)}{/if}</td>
                        <td class="text-center" style="color: blue;font-weight: bold;">{if $kh1.active==5}X{/if}</td>
                        <td class="text-center" style="color: blue;font-weight: bold;">{if $kh1.active<5}X{/if}</td>
                        <td class="text-center" style="color: green"> 
                            {if ($kh1.ngay_han >= $date_for_friday && $kh1.active < 5) || ($kh1.ngay_han >= date_insert($kh1.ngay_hoanthanh)  && $kh1.active == 5) || $kh1.ngay_han < '2017-01-01' }Trong hạn{/if}

                            {if $kh1.ngay_han < $date_for_friday && $kh1.active < 5 && $kh1.ngay_han >'2017-01-01'}<span style="color: red">Quá hạn </span>{/if}
                        </td>
                    </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
</div>

