<div class="content-wrapper">
    <!-- Content Header (Page header) -->
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">    
    <section class="content-header h2no">
        <div class="row" >
            <div class="col-md-8">
                <!-- <span class="font" style="color: red;">
                    Chấm điểm kết quả kế hoạch công tác tuần:
                    <b>Chọn tuần trước khi chấm điểm</b>
                </span>
                <span class="4" style="font-weight: 550">
                     {$tuan} ({$arr})
                     <select name="tuanchamdiem" style="width: 300px" class="form-control select2"
                     >
                            {$i=1}
                            {foreach $arr as $key => $value}
                                <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                            {/foreach}
                    </select>
                </span> -->
            </div>
            <div class="col-md-4 sl3" style="padding-bottom: 3px;">
                {if !empty($list_diem)}
                <input type="submit" name="ketxuatexcel" value="Kết xuất excel">
                <input type="submit" name="ketxuatword" value="Kết xuất word">
                {/if}
            </div>           
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" >
                    <div class="col-md-12" style="overflow-x: auto;">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center" rowspan="3">STT</th>
                                    <th width="30%" class="text-center" rowspan="3">Nội dung</th>
                                    <th width="5%" class="text-center" rowspan="3">Tổng số việc được giao</th>
                                    <th width="25%" class="text-center" colspan="6">Công việc đã hoàn thành trong tuần</th>
                                    <th width="10%" class="text-center" colspan="3">Công việc còn tồn cuối tuần</th>
                                    <th width="5%" class="text-center" rowspan="3"  style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Tỉ lệ hoàn thành (%)</th>
                                    <th width="5%" class="text-center" rowspan="3">Máy chấm điểm</th>
                                    <th width="5%" class="text-center" rowspan="3">Cá nhân tự chấm điểm</th>
                                    <th width="5%" class="text-center" rowspan="3">Trưởng đơn vị chấm điểm</th>
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    <th width="10%" class="text-center" colspan="2">Chất lượng</th>
                                    <th width="5%" class="text-center" rowspan="2" style="border: 1px solid #c3c3c3;border-top-width: 1px;border-bottom-width: 1px;
    border-left-width: 1px;">Có tính sáng tạo, đổi mới, hiệu quả</th>
                                    <th width="5%" class="text-center" rowspan="2">Số lượng</th>
                                    <th width="10%" class="text-center" colspan="2">Thời gian</th>
                                    
                                </tr>
                                <tr>
                                    <th width="5%" class="text-center">Trong hạn</th>
                                    <th width="5%" class="text-center">Quá hạn</th>
                                    <th width="5%" class="text-center">Đảm bảo</th>
                                    <th width="5%" class="text-center">Chưa đảm bảo</th>
                                    <th width="5%" class="text-center" >Trong hạn</th>
                                    <th width="5%" class="text-center" >Quá hạn</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center" colspan="2">(1)</td>
                                    <td class="text-center">(2)</td>
                                    <td class="text-center">(3)</td>
                                    <td class="text-center">(4)</td>
                                    <td class="text-center">(5)</td>
                                    <td class="text-center">(6)</td>
                                    <td class="text-center">(7)</td>
                                    <td class="text-center">(8)</td>
                                    <td class="text-center">(9)</td>
                                    <td class="text-center">(10)</td>
                                    <td class="text-center">(11)</td>
                                    <td class="text-center">(12)</td>
                                    <td class="text-center">(13)</td>
                                    <td class="text-center">(14)</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">(15)</td>
                                </tr>
                                <tr>
                                    <td class="text-center" colspan="2"><b>Tổng điểm <span style="color: blue">{($list_diem)? " Đ/c: {$canbophong[$list_diem[0]['canbo_id']]}":''}</span></b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>
                                        {(!empty($list_diem) && $list_diem[0]['diemtong']>0)?$list_diem[0]['diemtong']:''}
                                    </b></td>
                                    <td class="text-center"><b>
                                        {(!empty($list_diem) && $list_diem[0]['diem_tong']>0)?$list_diem[0]['diem_tong']:''}
                                    </b></td>
                                    <td class="text-center"  style="border-right: 1px solid #c3c3c3"><b>
                                     {(!empty($list_diem) && $list_diem[0]['diem_tong_tp']>0)?$list_diem[0]['diem_tong_tp']:''}
                                    </b></td>

                                </tr>
                                <tr>
                                    <td class="text-center"><b>A1</b></td>
                                    <td class="text-left"><b>Kết quả thực hiện nhiệm vụ trong tuần (I+II)<br>(tối đa 60 điểm)</b></td>
                                    <td class="text-center">{$count_arr[0]}</td>
                                    <td class="text-center">{$count_arr[1]}</td>
                                    <td class="text-center">{$count_arr[2]}</td>
                                    <td class="text-center" style="color: red">{$count_arr[3]}</td>
                                    <td class="text-center">{$count_arr[4]}</td>
                                    <td class="text-center">{$count_arr[5]}</td>
                                    <td class="text-center">{$count_arr[6]}</td>
                                    <td class="text-center">{$count_arr[7]}</td>
                                    <td class="text-center">{$count_arr[8]}</td>
                                    <td class="text-center" style="color: red">{$count_arr[9]}</td>
                                    <td class="text-center">{$count_arr[10]}%</td>
                                    <td class="text-center" style="font-weight: bold;"  rowspan="3">
                                    {$arr_chamtudong - $tong_diemtru}
                                    <input type="hidden" name="diem1" value="{$arr_chamtudong - $tong_diemtru}">
                                    </td>
                                    <td class="text-center" rowspan="3">
                                    {if $quyen >= 7}
                                    <select name="diem_1">
                                        <option value="0">Điểm</option>
                                        {for $i=60;$i>=0;$i--}
                                        {$j=$i+0.5}
                                        {if $j<60 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_1']==$j)?'selected':''}>{$j}</option>{/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_1']==$i)?selected:''}>{$i}.0</option>                                       
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_1']>0)?$list_diem[0]['diem_1']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center" rowspan="3"  style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}
                                    <select name="diem_1_tp">
                                        {for $i=0;$i<61;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_1_tp']=$list_diem[0]['diem_1']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_1_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<60 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_1_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_1_tp']}
                                    {else}
                                        -
                                    {/if}
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text-center" rowspan="2"><b>A2</b></td>
                                    <td class="text-left" rowspan="2"><b>Kết quả xử lý các VB trên VPĐT Không có yêu đầu ra<br><span style="color: blue">(Mỗi Văn Bản quá hạn bị trừ 0.5 điểm)</span></b></td>                                    
                                    <td class="text-center" colspan="4">VB chủ trì hoàn thành quá hạn</td>
                                    <td class="text-center" colspan="4">VB Phối hợp hoàn thành quá hạn</td>
                                    <td class="text-center" colspan="3" style="border: 1px solid #ccc;">Tổng điểm bị trừ</td>
                                </tr>
                                <tr style="background-color: #e1f8fb;">
                                    <td class="text-center" colspan="4">
                                        {if $tong_vbct_qh >0}<a href="{$url}dsphongchutri?PK_iMaVBDen={$ds_idctqh}"> {/if}
                                        {$tong_vbct_qh}
                                    {if $tong_vbct_qh >0}</a>{/if}</td>
                                    <td class="text-center" colspan="4">
                                    {if $tong_vbph_qh >0}<a href="{$url}dsphoihop?PK_iMaVBDen={$ds_idphqh}"> {/if}
                                        {$tong_vbph_qh}
                                    {if $tong_vbph_qh >0}</a>{/if}</td>
                                    <td class="text-center" colspan="3" style="border-right: 1px solid #c3c3c3 !important;">{$tong_diemtru}</td>
                                </tr>

                                <tr>
                                    <th class="text-center">I</th>
                                    <th colspan="16">NHIỆM VỤ LÃNH ĐẠO GIAO, NHIỆM VỤ THƯỜNG XUYÊN, NHIỆM VỤ KHÁC</th>
                                </tr> 
                                {$i=1}
                                {if empty($list_data1)}{else}
                                {foreach $list_data1 as $key => $value1}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left">{$value1.vanban_skh} &nbsp;{$value1.kh_noidung}</td>
                                    <td class="text-center" style="color: blue">X</td>
                                    <td class="text-center" style="color: blue">{if $value1.active ==5}X{else}{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value1.active == 5 && (date_insert($value1.ngay_hoanthanh) <= $value1.ngay_han || $value1.ngay_han <'2017-01-01' )}X{/if}</td>
                                    <td class="text-center" style="color: red">{if $value1.active == 5  && date_insert($value1.ngay_hoanthanh) > $value1.ngay_han && $value1.ngay_han >'2017-01-01'}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value1.active == 5 && $value1.chatluong == 1}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value1.active == 5 && $value1.chatluong == 2}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value1.active == 5 && $value1.sangtao == 2}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value1.active < 5}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value1.active < 5  &&  ($value1.ngay_han <'2017-01-01' || $hantuan <= $value1.ngay_han)}X{/if}</td>
                                    <td class="text-center" style="color: red">{if $value1.active < 5  && ($value1.ngay_han > '2017-01-01' && $hantuan > $value1.ngay_han && $value1.ngay_han > '2017-01-01')}X{/if}</td>
                                    <td class="text-center" style="color: blue"></td>
                                    <td class="text-center" style="color: blue"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center" style="color: blue" style="border-right: 1px solid #c3c3c3"></td>
                                </tr> 
                                {/foreach}{/if}
                                <tr>
                                    <th class="text-center">II</th>
                                    <th colspan="15"  style="border-right: 1px solid #c3c3c3">CÔNG VIỆC TỪ VĂN PHÒNG ĐIỆN TỬ</th>
                                </tr>
                                {if empty($list_data)}{else}
                                {foreach $list_data as $key => $value}
                                <tr>
                                    <td class="text-center">{$i++}</td>
                                    <td class="text-left"><a href="dsphongchutri?PK_iMaVBDen={$value.vanban_id}">{$value.vanban_skh} &nbsp;{$value.kh_noidung}</a></td>
                                    <td class="text-center" style="color: blue">X</td>
                                    <td class="text-center" style="color: blue">{if $value.active ==5}X{else}{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value.active == 5  && ($value.ngay_hoanthanh <= $value.ngay_han || $value.ngay_han <'2017-01-01' )}X{/if}</td>
                                    <td class="text-center" style="color: red">{if $value.active == 5  && date_insert($value.ngay_hoanthanh) > $value.ngay_han && $value.ngay_han >'2017-01-01' }X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value.active == 5  && $value.chatluong == 1}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value.active == 5  && $value.chatluong == 2}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value.active == 5  && $value.sangtao == 2}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value.active < 5}X{/if}</td>
                                    <td class="text-center" style="color: blue">{if $value.active < 5  && ($value.ngay_han <'2017-01-01' || $hantuan <= $value.ngay_han)}X{/if}</td>
                                    <td class="text-center" style="color: red">{if $value.active < 5  &&  ( $hantuan > $value.ngay_han && $value.ngay_han > '2017-01-01')}X{/if}</td> <td class="text-center" style="color: blue"></td>
                                    <td class="text-center" style="color: blue"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center" style="color: blue;border-right: 1px solid #c3c3c3"></td>

                                </tr> 
                                {/foreach}{/if}

                                <tr>
                                    <td class="text-center"><b>B</b></td>
                                    <td class="text-left"><b>Ý thức tổ chức kỷ luật (tối đa 20 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>20</b>
                                        <input type="hidden" name="diem2" value="20">
                                    </td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_2']:''}</b></td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3"><b>{($list_diem)?$list_diem[0]['diem_2_tp']:''}</b></td>
                                </tr> 
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Chấp hành tốt chủ trương, đường lối của Đảng, chính sách pháp luật của Nhà nước, nội quy, quy chế làm việc của cơ quan (tối đa 5 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>5</b>
                                        <input type="hidden" name="diem3" value="5">
                                    </td>
                                    <td class="text-center">
                                    {if $quyen >= 7}
                                    <select  name="diem_3">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_3']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<5 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_3']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_3']>0)?$list_diem[0]['diem_3']:''}
                                    {/if}
                                    </select></td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}
                                    <select  name="diem_3_tp">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_3_tp']=$list_diem[0]['diem_3']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_3_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j < 5 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_3_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_3_tp']}
                                    {else}-{/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-left">Không hách dịch, cửa quyền, sách nhiễu, gây phiền hà, tiêu cực trong thực hiện nhiệm vụ, công vụ (tối đa 4 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>4</b>
                                        <input type="hidden" name="diem4" value="4"></td>
                                    <td class="text-center">
                                    {if $quyen >= 7}
                                    <select  name="diem_4">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_4']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_4']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_4']>0)?$list_diem[0]['diem_4']:''}
                                    {/if}
                                    </select>
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}
                                    <select name="diem_4_tp">
                                        {for $i=0;$i<5;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_4_tp']=$list_diem[0]['diem_4']}
                                        {/if}

                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_4_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<4 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_4_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_4_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-left">Có phẩm chất chính trị, đạo đức, lối sống lành mạnh; tác phong, lề lối làm việc chuẩn mực; có tinh thần trách nhiệm, tận tụy với công việc; giữ gìn đoàn kết, thực hiện nguyên tắc tập trung dân chủ trong cơ quan, đơn vị (tối đa 3 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>3</b>
                                        <input type="hidden" name="diem5" value="3"></td>
                                    <td class="text-center">
                                    {if $quyen >= 7}
                                    <select name="diem_5">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_5']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_5']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_5']>0)?$list_diem[0]['diem_5']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_5_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_5_tp']=$list_diem[0]['diem_5']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_5_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_5_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_5_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">4</td>
                                    <td class="text-left">Thực hiện văn hóa nơi công sở, giao tiếp thân thiện, lịch sự, trang phục phù hợp, đeo thẻ công chức trong giờ làm việc (tối đa 3 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>3</b>
                                        <input type="hidden" name="diem6" value="3"></td>
                                    <td class="text-center">
                                    {if $quyen >= 7}
                                    <select name="diem_6">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_6']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_6']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_6']>0)?$list_diem[0]['diem_6']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                        {if $quyen < 7}<select name="diem_6_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_6_tp']=$list_diem[0]['diem_6']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_6_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3}<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_6_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_6_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">5</td>
                                    <td class="text-left">Xây dựng hình ảnh, giữ gìn uy tín cho bản thân, cơ quan, đơn vị và đồng nghiệp; chấp hành nghiêm các quy định của pháp luật về chế độ bảo mật, bí mật và kỷ luật phát ngôn của cơ quan, đơn vị (tối đa 3 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>3</b>
                                        <input type="hidden" name="diem7" value="3"></td>
                                    <td class="text-center">
                                    {if $quyen >= 7}
                                    <select name="diem_7">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_7']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_7']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_7']>0)?$list_diem[0]['diem_7']:''}
                                    {/if}
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_7_tp">
                                        {for $i=0;$i<4;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_7_tp']=$list_diem[0]['diem_7']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_7_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<3 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_7_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_7_tp']}
                                    {else}-{/if}
                                    </td>

                                </tr> 
                                <tr>
                                    <td class="text-center">6</td>
                                    <td class="text-left">Sử dụng hiệu quả thời gian làm việc, không giải quyết việc riêng trong giờ làm việc (tối đa 2 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b>
                                        <input type="hidden" name="diem8" value="2"></td>
                                    <td class="text-center">
                                    {if $quyen >= 7}<select name="diem_8">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_8']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_8']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_8']>0)?$list_diem[0]['diem_8']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_8_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_8_tp']=$list_diem[0]['diem_8']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_8_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_8_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_8_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                
                                <input type="hidden" name="diem9" value="0">
                                <input type="hidden" name="diem_9" value="0">
                                <input type="hidden" name="diem_9_tp" value="0">
                                <tr>
                                    <td class="text-center"><b>C</b></td>
                                    <td class="text-left"><b>Năng lực và kỹ năng thực hiện nhiệm vụ (tối đa 10 điểm)</b></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>10</b>
                                        <input type="hidden" name="diem10" value="10"></td>
                                    <td class="text-center"><b>{($list_diem)?$list_diem[0]['diem_10']:''}</b></td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3"><b>{($list_diem)?$list_diem[0]['diem_10_tp']:''}</b></td>

                                </tr> 
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Thường xuyên học tập, rèn luyện, nâng cao trình độ, kỹ năng chuyên môn nghiệp vụ, kiến thức ngoại ngữ, tin học; tham mưu đầy đủ, kịp thời, có chất lượng các văn bản phục vụ công tác chỉ đạo, điều hành của đơn vị, bộ phận theo chỉ đạo của lãnh đạo Sở và kế hoạch công tác đề ra (tối đa 2 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b>
                                        <input type="hidden" name="diem11" value="2"></td>
                                    <td class="text-center">{if $quyen >= 7}<select name="diem_11">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_11']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_11']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_11']>0)?$list_diem[0]['diem_11']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_11_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_11_tp']=$list_diem[0]['diem_11']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_11_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_11_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_11_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-left">Xây dựng kế hoạch công tác tuần, tháng của cá nhân, phòng, đơn vị, bộ phận theo lĩnh vực được phân công rõ nội dung, tiến độ (tối đa 2 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b>
                                        <input type="hidden" name="diem12" value="2"></td>
                                    <td class="text-center">{if $quyen >= 7}<select name="diem_12">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_12']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_12']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_12']>0)?$list_diem[0]['diem_12']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_12_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_12_tp']=$list_diem[0]['diem_12']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_12_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_12_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_12_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-left">Chỉ đạo, điều hành, kiểm soát việc thực hiện nhiệm vụ của đơn vị, bộ phận đảm bảo kịp thời, không bỏ sót nhiệm vụ. Phân công, chỉ đạo giải quyết công việc linh hoạt, có định hướng cụ thể, đúng quy trình (tối đa 2 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>2</b>
                                        <input type="hidden" name="diem13" value="2"></td>
                                    <td class="text-center">{if $quyen >= 7}<select name="diem_13">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_13']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_13']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_13']>0)?$list_diem[0]['diem_13']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_13_tp">
                                        {for $i=0;$i<3;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_13_tp']=$list_diem[0]['diem_13']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_13_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<2 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_13_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_13_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">4</td>
                                    <td class="text-left">Kiểm tra, đôn đốc việc thực hiện nhiệm vụ của công chức, viên chức, lao động hợp đồng trong đơn vị, bộ phận và giải quyết kịp thời những khó khăn, vướng mắc theo thẩm quyền (tối đa 1 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b>
                                        <input type="hidden" name="diem14" value="1"></td>
                                    <td class="text-center">{if $quyen >= 7}<select name="diem_14">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_14']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_14']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_14']>0)?$list_diem[0]['diem_14']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_14_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_14_tp']=$list_diem[0]['diem_14']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_14_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_14_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_14_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">5</td>
                                    <td class="text-left">Có năng lực quy tụ, tập hợp công chức, viên chức, xây dựng đơn vị, bộ phận đoàn kết, thống nhất. Phối hợp, tạo lập mối quan hệ tốt với cá nhân, tổ chức có liên quan trong thực hiện nhiệm vụ (tối đa 1 điểm);</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b>
                                        <input type="hidden" name="diem15" value="1"></td>
                                    <td class="text-center">{if $quyen >= 7}<select name="diem_15">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_15']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_15']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_15']>0)?$list_diem[0]['diem_15']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_15_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_15_tp']=$list_diem[0]['diem_15']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_15_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_15_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_15_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <tr>
                                    <td class="text-center">6</td>
                                    <td class="text-left">Sử dụng thành thạo các phần mềm, ứng dụng công nghệ thông tin đáp ứng yêu cầu công việc; các văn bản trên hệ thống Văn phòng điện tử thuộc trách nhiệm được xử lý kịp thời, đúng quy trình (tối đa 1 điểm); </td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b>
                                        <input type="hidden" name="diem16" value="1"></td>
                                    <td class="text-center">{if $quyen >= 7}<select name="diem_16">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_16']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_16']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_16']>0)?$list_diem[0]['diem_16']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_16_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_16_tp']=$list_diem[0]['diem_16']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_16_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_16_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_16_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                                <input type="hidden" name="diem17" value="0">
                                <input type="hidden" name="diem_17" value="0">
                                <input type="hidden" name="diem_17_tp" value="0">
                                <tr>
                                    <td class="text-center">7</td>
                                    <td class="text-left">Thiết lập hồ sơ công việc đầy đủ theo các đầu mục công việc được phân công; lưu trữ hồ sơ, tài liệu đúng nguyên tắc; xây dựng cơ sở dữ liệu thuộc lĩnh vực được phân công phụ trách, làm cơ sở để xây dựng kho dữ liệu chung của cơ quan, đơn vị (tối đa 1 điểm)</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>1</b>
                                        <input type="hidden" name="diem18" value="1"></td>
                                    <td class="text-center">{if $quyen >= 7}<select name="diem_18">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_18']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_18']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_18']>0)?$list_diem[0]['diem_18']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_18_tp">
                                        {for $i=0;$i<2;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_18_tp']=$list_diem[0]['diem_18']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_18_tp']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j<1 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_18_tp']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_18_tp']}
                                    {else}-{/if}</td>

                                </tr>  

                                <tr>
                                    <th class="text-center">D</th>
                                    <td class="text-left"><b>Điểm thưởng (điểm tối đa: 10)</b> Có một trong các tiêu chí sau đây:<br>
                                        - Có giải pháp cải tiến, đổi mới, sáng tạo, nâng cap hiệu quả công việc;<br>- Hoàn thành xuất sắc các nhiệm vụ đột xuất phát sinh;<br>- Tham mưu có hiệu quả đối với các nhiệm vụ mới và khó được lãnh đạo giao</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"><b>{$diemthuongmaycham}</b>
                                        <input type="hidden" name="diem19" value="{$diemthuongmaycham}"></td>
                                    <td class="text-center">{if $quyen >= 7}<!--<select name="diem_19">
                                        {for $i=0;$i<6;$i++}
                                        {$j=$i+0.5}
                                        <option value="{$i}" {(!empty($list_diem) && $list_diem[0]['diem_19']==$i)?'selected':''}>{$i}.0</option>
                                        {if $j< 5 }<option value="{$j}" {(!empty($list_diem) && $list_diem[0]['diem_19']==$j)?'selected':''}>{$j}</option>{/if}
                                        {/for}
                                    </select>-->--<input type="hidden" name="diem_19" value="0">
                                    {else}
                                        {(!empty($list_diem) && $list_diem[0]['diem_19']>0)?$list_diem[0]['diem_19']:''}
                                    {/if}</td>
                                    <td class="text-center" style="border-right: 1px solid #c3c3c3">
                                    {if $quyen < 7}<select name="diem_19_tp">
                                        {for $i=0;$i<11;$i++}
                                        {$j=$i+0.5}

                                        {if !empty($list_diem) && $list_diem[0]['active']==1}
                                        {$list_diem[0]['diem_19_tp']=$list_diem[0]['diem_19']}
                                        {/if}

                                        <option value="{$i}"  {(!empty($list_diem) && $list_diem[0]['diem_19_tp']==$i)?'selected':''}>{$i}.0</option>
                                        <!-- {if $j < 10 }<option value="{$j}"  {(!empty($list_diem) && $list_diem[0]['diem_19_tp']==$j)?'selected':''}>{$j}</option>
                                        {/if} -->
                                        {/for}
                                    </select>
                                    {/if}
                                    {if !empty($list_diem) && $list_diem[0].active ==2 && $quyen >= 7}
                                        {$list_diem[0]['diem_19_tp']}
                                    {else}-{/if}</td>

                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                {if !empty($list_diem) && $list_diem[0].active ==2}
                    <div class="row" style="height: auto;padding: 5px 5px 5px 15px">
                        <b> Nhận xét của trưởng đơn vị: </b> <i>({date_time($list_diem[0].ngay_cham)})</i> <br/>{$list_diem[0].nhan_xet}
                </div>
                {/if}

                {if !empty($mocham) && $mocham[0]['canbo'] == $cb_id && $quyen >= 7}
                <div class="row">
                    <div class="col-md-4">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật điểm tuần {$mocham[0]['tuan']}</button>
                    </div>
                    <div class="col-md-8">
                        <input type="hidden" name="tuanchamdiem" value="{$mocham[0]['tuan']}">
                    </div> 
                </div>
                {else}

                {if empty($list_diem) || (!empty($list_diem) && $list_diem[0].active ==1 && $quyen >= 7 && $list_diem[0].canbo_id == $cb_id)}    
                <div class="row">
                {if $thu_hientai == 'Friday' ||  $thu_hientai == 'Saturday'  ||  $thu_hientai == 'Sunday' } 
                    <!-- || $thu_hientai == 'Monday'
                      <div class="col-md-8">
                      <span class="font" style="color: red;">
                          <b>Chọn tuần trước khi chấm điểm</b>
                      </span>
                      <span class="4" style="font-weight: 550">
                          
                           <select name="tuanchamdiem" style="width: 300px" class="form-control select2"
                           >
                                  {$i=1}
                                  {foreach $arr as $key => $value}
                                      <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                                  {/foreach}
                          </select>
                      </span>
                                       </div>  --> 
                                         <div class="col-md-4">
                       <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật điểm tuần {if $thu_hientai == 'Monday'}{$tuan-1}{else}{$tuan}{/if}</button>
                                         </div>
                {/if}
                </div>
                {/if}
                {/if}
<!--!empty($mocham) && $mocham[0]['canbo'] == $cb_id &&-->
                {if !empty($mocham) && $mocham[0]['canbo'] == $cb_id && $quyen < 7}
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control textarea" name="nhan_xet" style="font-size: 14px;" rows=5 placeholder="Nhận xét của trưởng đơn vị">{$list_diem[0]['nhan_xet']}</textarea>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="tuanchamdiem" value="{$mocham[0]['tuan']}">
                        <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Nhận xét đánh giá tuần {$mocham[0]['tuan']}</button>
                    </div>
                </div>
                {else}

                {if !empty($list_diem) && $list_diem[0].active <=2 && $quyen < 7 && ($thu_hientai == 'Friday' ||  $thu_hientai == 'Saturday' ||  $thu_hientai == 'Sunday' ||  $thu_hientai == 'Monday' )}  
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="nhan_xet" style="font-size: 14px;" rows=5 placeholder="Nhận xét của trưởng đơn vị">{$list_diem[0]['nhan_xet']}</textarea>
                    </div>
                </div>
                <div class="row" style="height: 5px"></div>
                {if $FK_iMaPhongHD == $list_diem[0]['phong_id']}
                <div class="row">
                    <!--<div class="col-md-12">
                    <div class="col-md-8">
                      <span class="font" style="color: red;">
                          <b>Chọn tuần trước khi chấm điểm</b>
                      </span>
                      <span class="4" style="font-weight: 550">
                           <select name="tuanchamdiem" style="width: 300px" class="form-control select2"
                           >
                                  {$i=1}
                                  {foreach $arr as $key => $value}
                                      <option value="{$i}" {if $week == $i} selected {/if}>Tuần: {$i++} ({$value})</b></option>
                                  {/foreach}
                          </select>
                      </span>
                                      </div>
                                       
                                      <div class="col-md-4">
                       <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật điểm tuần {$tuan}</button>
                                       </div>
                                   </div>-->
                                   </div>   
                       <button type="submit" name="luudulieu" value="luudulieu" class="btn btn-primary">Cập nhật điểm tuần {$tuan}</button>
                   </div>
                                   </div>
                {/if}
                {/if}{/if}
                <div class="row" style="height: 5px"></div>
                <div class="row">
                    <span style="color: red;padding-left: 20px"><b>Lưu ý phương pháp tính điểm tự động:</b></span><br><br>
                    <span style="color: blue;padding-left: 20px">Tỉ lệ phần trăm:  (12) = [ (3) / [ (2) - (10) ] ] * 100</i></span>
                    <span style="color: blue;padding-left: 20px">Tổng điểm: (13) = (12) * 0.6 - (Tổng điểm bị trừ) - (5) + [ (8) * 2 ]</span>
                    <br /><br />
                    <span style="color: blue;padding-left: 20px">
                        
                            (5): Mỗi văn bản hoàn thành chậm hạn sẽ bị trừ 1 điểm với cán bộ thụ lý trực tiếp, trừ 0.5 điểm với lãnh đạo chỉ đạo;<br/>
                        
                    </span>
                    <span style="color: blue;padding-left: 20px">
                        
                            (8): Mỗi văn bản có tính sáng tạo được cộng tối thiểu 1.0 điểm;
                        
                    </span>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </form>
</div>

