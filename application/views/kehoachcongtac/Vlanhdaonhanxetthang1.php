<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding: 0px">
        <div class="row">
            <div class="col-sm-12 deletepadding1">
                <div class="col-sm-9">
                    <h3 class="font">
                        Giám đốc sở đánh giá kết quả thực hiện nhiệm vụ tháng đối với toàn bộ phó trưởng đơn vị, chuyên viên, HĐLĐ trong toàn sở
                    </h3>
                </div>
                <div class="col-sm-3">
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <div class="well" style="    background-color: rgba(60, 141, 188, 0.07);border: 1px solid #3c8dbc; border-radius: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{$url}lanhdaonhanxetthang" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-5 control-label">Chọn tháng:  </label>

                                                    <div class="col-sm-7">
                                                        <select name="thangbd" id="" class="form-control ngayhan select2" style="width:100%">
                                                            
                                                            {for $i=1;$i<=12;$i++}
                                                                <option value="{$i}"  {if $thangbd == $i}selected ="selected"{/if}> Tháng {$i}</option>
                                                            {/for}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Phòng, đơn vị</label>

                                                    <div class="col-sm-8">
                                                        <select name="phongban" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" {if $phong == 0}selected ="selected"{/if}>-- Chọn phòng, đơn vị --</option>
                                                            
                                                             {foreach $phongban as $key_phong => $value_phong}
                                                                <option value="{$key_phong}" {if $phong == $key_phong}selected ="selected"{/if}> {$value_phong}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Cán bộ</label>

                                                    <div class="col-sm-8">
                                                        <select name="canbo" id="" class="form-control ngayhan select2" style="width:100%">
                                                            <option value="0" >-- Chọn cán bộ --</option>
                                                            {foreach $danhsachcbphong as $value_canbo}
                                                                <option value="{$value_canbo.PK_iMaCB}" {if $canbo == $value_canbo.PK_iMaCB }selected ="selected"{/if}> {$value_canbo.sHoTen}</option>
                                                            {/foreach}
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" >
                                                <button class="btn btn-primary  pull-right" name="thongke" value="thongkebaocaothang" >Tìm kiếm</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-12 deletepadding">
                   <!--  {$phantrang} -->
                </div>
<!-- phần nhận xét đánh giá của ban giám đốc - chánh văn phong tham mưu -->
{if $quyen == 3 or $quyen == 4}
<div class="row">
    <div class="col-md-12">
        <table id="" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th width="4%" class="text-center align-middle">STT</th>
                <th width="10%" class="text-center align-middle">Lãnh đạo nhận xét</th>
                <th width="5%" class="text-center align-middle" >Tháng</th>
                <th width="" class="text-center align-middle">Nội dung nhận xét</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-left">1</td>
                <td class="text-left">{$tencanbo[$danhgia_gd.canbo_id]}</td>
                <td class="text-left">{$danhgia_gd.thang}</td>
                <td class="text-left">{$danhgia_gd.danhgia_ld}</td>
            </tr>
            {$i = 2}
            {foreach $danhgia_ld as $pgd}
            <tr>
                <td class="text-left">{$i++}</td>
                <td class="text-left">{$tencanbo[$pgd.FK_iMaCB_PP]}</td>
                <td class="text-left">{$pgd.thang}</td>
                <td class="text-left">{$pgd.sNhanXet}</td>
            </tr>
            {/foreach}

            {if $danhgia_gd.active lt 5}
            <tr>
                <td class="text-left" colspan="4"><b>Tổng hợp nhận xét công tác tháng của Ban Giám đốc với Giám đốc Sở:</b>
                <textarea name="nhanxet_bgd" rows="12" cols="145" placeholder="Tổng hợp nhận xét công tác tháng của Ban Giám đốc sở với Giám đốc sở...">{if $danhgia_gd.danhgia_bgd !=''}{$danhgia_gd.nhanxet_bgd}{/if}</textarea>
<br>
                <b>Mức hoàn thành nhiệm vụ của Giám đốc sở: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="danhgia_bgd" value="8" {if $danhgia_gd.danhgia_bgd == 8}checked=checked{/if}>
                    <span {if $danhgia_gd.danhgia_bgd == 8}style="color: red"{/if}> HT xuất sắc tiêu biểu</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="danhgia_bgd" value="6" {if $danhgia_gd.danhgia_bgd == 6}checked=checked{/if}>
                    <span {if $danhgia_gd.danhgia_bgd == 6}style="color: red"{/if}> HT xuất sắc nhiệm vụ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="danhgia_bgd" value="5" {if $danhgia_gd.danhgia_bgd == 5}checked=checked{/if}>
                    <span> HT tốt nhiệm vụ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="danhgia_bgd" value="4" {if $danhgia_gd.danhgia_bgd == 4}checked=checked{/if}>
                    <span> HT nhiệm vụ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="danhgia_bgd" value="2" {if $danhgia_gd.danhgia_bgd == 2}checked=checked{/if}>
                    <span> Không HT nhiệm vụ</span>
                <br><br>
                <input type="hidden" name="danhgia_id" value="{$danhgia_gd.dg_id}">
                <button class="btn btn-primary  pull-left" name="danhgiabgd" value="danhgiabangiamdoc" >{if $quyen == 3}Nhận xét{else}Duyệt{/if}</button>
                </td>
                
            </tr>
            {else}
                <tr>
                <td class="text-left" colspan="4"><b>Tổng hợp nhận xét công tác tháng của Ban Giám đốc với Giám đốc Sở:</b> 
                {if $danhgia_gd.danhgia_bgd !=''}{$danhgia_gd.nhanxet_bgd}{/if}

                <b>Mức hoàn thành nhiệm vụ của Giám đốc sở: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {if $danhgia_gd.danhgia_bgd == 8}
                        <span style="color: red"> HT xuất sắc tiêu biểu</span>
                    {/if}
                    {if $danhgia_gd.danhgia_bgd == 6}
                        <span style="color: red"> HT xuất sắc nhiệm vụ</span>
                    {/if}
                    {if $danhgia_gd.danhgia_bgd == 5}
                        <span style="color: blue"> HT tốt nhiệm vụ</span>
                    {/if} 
                    {if $danhgia_gd.danhgia_bgd == 4}
                        <span style="color: blue"> HT nhiệm vụ</span>
                    {/if}
                    {if $danhgia_gd.danhgia_bgd == 2}
                        <span style="color: blue">Không hoàn thành nhiệm vụ</span>
                    {/if}              
                </td>
                
            </tr>
            {/if}
            </tbody>
        </table> 
    </div>
</div> 
{/if}                              

                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="4%" class="text-center align-middle">STT</th>
                                    <th width="10%" class="text-center align-middle">Cán bộ</th>
                                    <th width="10%" class="text-center align-middle">phòng</th>
                                    <th width="5%" class="text-center align-middle" >Tháng</th>
                                    <th width="4%" class="text-center align-middle">Số điểm</th>
                                    <th width="" class="text-center align-middle">Đánh giá của lãnh đạo đơn vị</th>
                                    <th width="8%" class="text-center align-middle">Kết quả phân loại</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-left"><b>I</b></td>
                                <td class="text-left" colspan="6">
                                    <div style="float: left;width:650px "><b>Kết quả thực hiện nhiệm vụ tháng của phó phòng, chuyên viên</b></div>
                                    {if $kiemtra gt 0}
                                    <div style="float: right;width:250px">
                                        <input type="checkbox" id="checkall"> <b>Chọn tất cả </b>&nbsp;&nbsp;&nbsp;
                                        
                                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetkequathang" value="Duyệt" style="margin-bottom: 10px" data-original-title="" title=""><i class="fa fa-check"></i> Duyệt</button>
                                    </div>
                                    {/if}
                                </td>
                            </tr>
                            <input type="hidden" name="where" value="{$where}">
                            {$i = 1}
                            {foreach $kehoach as $kh}
                            <tr>
                               <td class="text-left">{$i++}</td>
                               <td>
                                <a href="{$url}xemkehoachcongtacthang/{$kh.id_cb}/{$kh.thang}" target="_blank">
                               {$tencanbo[$kh.id_cb]}
                                </a></td>
                               <td class="text-left">{$phongban[$kh.id_phong]}</td>
                               <td class="text-center"><b>{$kh.thang}</b></td>
                               <td class="text-left">{$kh.diem}</td>
                               <td class="text-left">
                                <div style="width: 600px">
                                    {if $kh.nhanxet_cv !=''}
                                    <button type="button" style="margin-bottom:15px; " class="btn btn-default btn-xs" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{$kh.nhanxet_cv}">Cá nhân tự nhận xét</button>&nbsp;&nbsp;&nbsp;{/if}

                                    {if $kh.nhanxet_pp !=''}
                                    <button type="button" style="margin-bottom:15px; " class="btn btn-default btn-xs" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{strip_tags($kh.nhanxet_pp)}">Phó trưởng đơn vị nhận xét</button>&nbsp;&nbsp;&nbsp;{/if}

                                    {if $kh.nhanxet !=''}
                                    <button type="button" style="margin-bottom:15px; "  class="btn btn-default btn-xs" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{$kh.nhanxet}">Nhận xét của trưởng đơn vị</button>{/if}

                                    <span style="float: right;"><input type="checkbox" name="duyet[{$kh.id}]" value="1" {if $kh.acitve > 3 and $quyen == 3}style="display:none;" class="khongduyet"{else} class="duyet" {/if}> <b>Chọn duyệt</b></span>
                                </div>
<div style="width: 210px;float: left;">
<span style="font-weight: bold;">Đánh giá của GĐ: {if $kh.acitve == 5}<span style="color: red">(đã duyệt)</span>{/if}<br></span>
{if $kh.acitve > 3 and $quyen == 3} 
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="8" {if $kh.danhgia_gd == 8}checked=checked{/if}{if $kh.danhgia_gd >0} onclick="return false;"{/if}>
                                    <span {if $kh.danhgia_gd == 8}style="color: red"{/if}> HT xuất sắc tiêu biểu</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="6" {if $kh.danhgia_gd == 6}checked=checked{/if}{if $kh.danhgia_gd >0} onclick="return false;"{/if}>
                                    <span {if $kh.danhgia_gd == 6}style="color: red"{/if}> HT xuất sắc nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="5" {if $kh.danhgia_gd == 5}checked=checked{/if}{if $kh.danhgia_gd >0} onclick=" return false;"{/if}>
                                    <span> HT tốt nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="4" {if $kh.danhgia_gd == 4}checked=checked{/if}{if $kh.danhgia_gd >0} onclick="return false;"{/if}>
                                    <span> HT nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="2" {if $kh.danhgia_gd == 2}checked=checked{/if}{if $kh.danhgia_gd >0} onclick="return false;"{/if}>
                                    <span> Không HT nhiệm vụ</span>&nbsp;&nbsp;&nbsp;
{else}
    {if $kh.danhgia_gd > 0}
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="8" {if $kh.danhgia_gd == 8}checked=checked{/if}{if $kh.danhgia_gd >0}{/if}>
                                    <span {if $kh.danhgia_gd == 8}style="color: red"{/if}> HT xuất sắc tiêu biểu</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="6" {if $kh.danhgia_gd == 6}checked=checked{/if}{if $kh.danhgia_gd >0}{/if}>
                                    <span {if $kh.danhgia_gd == 6}style="color: red"{/if}> HT xuất sắc nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="5" {if $kh.danhgia_gd == 5}checked=checked{/if}{if $kh.danhgia_gd >0}{/if}>
                                    <span> HT tốt nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="4" {if $kh.danhgia_gd == 4}checked=checked{/if}{if $kh.danhgia_gd >0}{/if}>
                                    <span> HT nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="2" {if $kh.danhgia_gd == 2}checked=checked{/if}{if $kh.danhgia_gd >0}{/if}>
                                    <span> Không HT nhiệm vụ</span>&nbsp;&nbsp;&nbsp;
    {else}
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="8" {if $kh.danhgia_tp == 8}checked=checked{/if}>
                                    <span {if $kh.danhgia_tp == 8}style="color: red"{/if}> HT xuất sắc tiêu biểu</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="6" {if $kh.danhgia_tp == 6}checked=checked{/if}>
                                    <span {if $kh.danhgia_tp == 6}style="color: red"{/if}> HT xuất sắc nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="5" {if $kh.danhgia_tp == 5}checked=checked{/if}>
                                    <span> HT tốt nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="4" {if $kh.danhgia_tp == 4}checked=checked{/if}>
                                    <span> HT nhiệm vụ</span><br>
                                    <input type="radio" name="danhgiaGD[{$kh.id}]" value="2" {if $kh.danhgia_tp == 2}checked=checked{/if}>
                                    <span> Không HT nhiệm vụ</span>&nbsp;&nbsp;&nbsp;
    {/if}
{/if}                                    
</div>
<div style="width: 300px;float: left;">
{if $kh.acitve > 3 and $quyen == 3 } 
{$kh.nhanxet_gd}
<input type="hidden" name="nhanxetGD[{$kh.id}]" value="{$kh.nhanxet_gd}">
{else}
<textarea name="nhanxetGD[{$kh.id}]" rows="6" cols="50" placeholder="Giám đốc nhận xét...">{if $kh.nhanxet_gd !=''}{$kh.nhanxet_gd}{else}(1)Thống nhất với phần tự nhận xét, đánh giá của cá nhân và nhận xét đánh giá của Lãnh đạo phòng, đơn vị. (2)Trong các tháng tiếp theo đề nghị đồng chí cần phải tự nhận xét, đánh giá cụ thể hơn về kết quả thực hiện nhiệm vụ được giao của bản thân. (3)Bên cạnh việc thực hiện các nhiệm vụ chuyên môn, đề nghị đồng chí cần tập trung chỉ đạo, đôn đốc, thực hiện việc cập nhật CSDL cá nhân trên hệ thống CSDL điện tử của sở.{/if}</textarea>
{/if} 
<input type="hidden" name="key_id[{$kh.id}]" value="{$kh.id}">
</div>                                   
                               </td>
{if $kh.acitve > 2 }  
<td class="text-left">
                                    {if $kh.danhgia_gd == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                    {if $kh.danhgia_gd == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh.danhgia_gd == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_gd == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_gd == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                               </td>

{else}
    {if $kh.danhgia_gd > 0}
                                <td class="text-left">
                                    {if $kh.danhgia_gd == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                    {if $kh.danhgia_gd == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh.danhgia_gd == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_gd == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_gd == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                               </td>
    {else}
                               <td class="text-left">
                                    {if $kh.danhgia_tp == 8}
                                        <span style="color: red">Hoàn thành xuất sắc tiêu biểu</span>{/if}
                                    {if $kh.danhgia_tp == 6}
                                        <span style="color: red">Hoàn thành xuất sắc nhiệm vụ</span>{/if}
                                    {if $kh.danhgia_tp == 5}
                                        <span style="color: blue">Hoàn thành tốt nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_tp == 4}
                                        <span style="color: #770000">Hoàn thành nhiệm vụ</span>
                                    {/if}
                                    {if $kh.danhgia_tp == 2}
                                        <span style="color: #003300">Không hoàn thành nhiệm vụ</span>
                                    {/if}
                               </td>
    {/if}
{/if}                               
                           </tr>
                           {/foreach}
<tr>
                                <td class="text-left"></td>
                                <td class="text-left" colspan="6">
                                    <div style="float: left;width:650px "></div>
                                    <div style="float: right;width:250px">
                                        
                                        <button type="submit" class="btn btn-success pull-right btn-sm" name="duyetkequathang" value="Duyệt" style="margin-bottom: 10px" data-original-title="" title=""><i class="fa fa-check"></i> Duyệt</button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- <div><b>Tổng văn bản: {$count}</b></div> -->
                    </div>
                </div>
                <div class="col-sm-12 deletepadding">
                   <!--  {$phantrang} -->
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>

<script>
    $("#an_hien").hide();
    $("#an_hien1").hide();
    $(document).ready(function() {
        $(document).on('click','button[name=thongke]',function(){
            $(this).addClass('hide');
        });
    });
    var url = window.location.href;
    $(document).on('change','select[name=phongban]',function(){
    var maphong = $(this).val();
    $.ajax({
        url:url,
        type:'post',
        data:{
            action:'getmaphong',
            maphong:maphong
        },
        success:function(respon){
            var result = JSON.parse(respon);
            var tong = result.length;
            var html = '<option value="0">-- Chọn cán bộ --</option>';
            for(var i=0;i<tong;i++)
            {
                html+="<option value='"+result[i]['PK_iMaCB']+"'>"+result[i]['sHoTen']+"</option>";
            }
            $('select[name=canbo]').html(html);
            $('.select2').select2();
        }

    });
   });

function an_hien(){
    if(document.getElementById('lanhdaodonvi').checked) {
        $("#an_hien").show();
        $("#an_hien1").show();
    }else {
        $("#an_hien").hide();
        $("#an_hien1").hide();
    }
}

$(document).on('click','#checkall',function () {
        var checked_status = this.checked;
        $(".duyet").each(function(){
            this.checked = checked_status;
        });
    });
</script>
