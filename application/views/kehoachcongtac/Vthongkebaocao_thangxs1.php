<table width="100%">
  <tr>
    <td colspan="11" style="padding-top: 10px; text-align: left;">
      Biểu số 01
    </td>
  </tr>
    <tr>
        <td style=" text-align: center;width: 30%" colspan="3">
            UBND THÀNH PHỐ HÀ NỘI<br>
            <span style="text-decoration: underline;">Sở Ngoại Vụ </span>

            </div>
        </td>
        <td style=" text-align: center;width: 70%" colspan="8">
            CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br>
            <span style="text-decoration: underline;"> Độc lập - Tự do - Hạnh phúc</span>
        </td>
    </tr>
    <tr>
        <td colspan="11" style="padding-top: 10px; text-align: center;">
            <b>BIỂU TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ, PHÂN LOẠI CÔNG CHỨC HÀNG THÁNG<br>
Từ tháng {$thangbd} đến tháng {$thangkt} năm {$nam}<br>
<i>(Áp dụng cho các đồng chí lãnh đạo Sở, Trưởng phòng, đơn vị trực thuộc Sở)</i></b>

        </td>
    </tr>
</table>



<table >
    <tr>
        <th width="5%" rowspan="2" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000">TT</th>
        <th width="5%" rowspan="2" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000">TT</th>
        <th width="15%" rowspan="2" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000">Họ Tên</th>
        <th width="40%" colspan="4" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000">Kết quả đánh giá phân loại hàng tháng (Từ tháng {$thangbd}/{$nam} đến tháng {$thangkt}/{$nam} )</th>
        <th width="30%" colspan="3" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000">Mức chi bổ xung thu nhập theo Quy chế chi tiêu nội bộ của sở năm 2018</th>
        <th width="5%" rowspan="2" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000">Ghi chú</th>
    </tr>
    <tr>    
        <!-- <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Hoàn thành xs tiêu biểu</th>  -->
        <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Hoàn thành xs</th> 
        <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Hoàn thành tốt</th>
        <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Hoàn thành nhiệm vụ</th>
        <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Không hoàn thành nhiệm vụ</th>

        <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Hoàn thành xuất sắc mức chi 500.000/đ/ng/tháng</th>
        <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Hoàn thành tốt nhiệm vụ mức chi 200.000/đ/ng/tháng</th>
        <th width="10%" class="text-center align-middle" style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;">Tổng số</th>       
    </tr>
 
    <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
    {$j = 1}{$jj = 1}
    <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000;padding-left: 10px"><b>Lãnh đạo sở</b></td>
    </tr>
    {foreach $count_arrld as $keyld => $valueld}
        <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {foreach $arr_thang as $thang}
            {if isset($valueld[$thang][0])}
            {if $valueld[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $valueld[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$keyld]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>
   </tr>
   
   {/foreach}
  <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000;padding-left: 10px"><b>Văn phòng sở</b></td>
  </tr>
     {$j = 1}
    {foreach $count_arr1 as $key1 => $value1}

    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value1[$thang][0])}
            {if $value1[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value1[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value1[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value1[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value1[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value1[$thang][0])}
            {if $value1[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value1[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value1[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value1[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value1[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key1]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}

  <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
<tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000;padding-left: 10px"><b>CC Tài chính doanh nghiệp</b></td>
  </tr>
    {$j=1}
    {foreach $count_arr2 as $key2 => $value2}
    
    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value2[$thang][0])}
            {if $value2[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value2[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value2[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value2[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value2[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value2[$thang][0])}
            {if $value2[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value2[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value2[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value2[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value2[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key2]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
<tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>Phòng Quản lý công sản</b></td>
  </tr>
  {$j=1}
    {foreach $count_arr3 as $key3 => $value3}
    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value3[$thang][0])}
            {if $value3[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value3[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value3[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value3[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value3[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value3[$thang][0])}
            {if $value3[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value3[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value3[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value3[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value3[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key3]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
   <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>Phòng Quản lý giá</b></td>
  </tr>
  {$j=1}
    {foreach $count_arr4 as $key4 => $value4}
    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value4[$thang][0])}
            {if $value4[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value4[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value4[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value4[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value4[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value4[$thang][0])}
            {if $value4[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value4[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value4[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value4[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value4[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key4]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
   <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>Phòng Quản lý ngân sách</b></td>
  </tr>
{$j=1} 
    {foreach $count_arr5 as $key5 => $value5}
     <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value5[$thang][0])}
            {if $value5[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value5[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value5[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value5[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value5[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value5[$thang][0])}
            {if $value5[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value5[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value5[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value5[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value5[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key5]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
   <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>Phòng Tài chính hành chính sự nghiệp</b></td>
  </tr>
    {$j =1} 
    {foreach $count_arr6 as $key6 => $value6}
    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
         {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value6[$thang][0])}
            {if $value6[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value6[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value6[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value6[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value6[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value6[$thang][0])}
            {if $value6[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value6[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value6[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value6[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value6[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key6]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
   <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>Thanh tra sở</b></td>
  </tr>
  {$j=1}
    {foreach $count_arr7 as $key7 => $value7}
    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value7[$thang][0])}
            {if $value7[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value7[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value7[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value7[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value7[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value7[$thang][0])}
            {if $value7[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value7[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value7[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value7[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value7[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key7]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
   <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>Phòng Tài chính đầu tư</b></td>
  </tr>
  {$j=1}
    {foreach $count_arr8 as $key8 => $value8}
    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value8[$thang][0])}
            {if $value8[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value8[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value8[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value8[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value8[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value8[$thang][0])}
            {if $value8[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value8[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value8[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value8[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value8[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key8]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
   <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>Phòng Tin học và Thống kê</b></td>
  </tr>
  {$j=1}
    {foreach $count_arr9 as $key9 => $value9}
   
        <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value9[$thang][0])}
            {if $value9[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value9[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value9[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value9[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value9[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value9[$thang][0])}
            {if $value9[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value9[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value9[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value9[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value9[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
     <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key9]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
<tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
   <tr>
      <td colspan="11" style="border-top:0.5pt solid #000;border-right: 0.5pt solid #000"><b>TT mua sắm TCS và TT,TVTC</b></td>
  </tr>
  {$j=1}
    {foreach $count_arr10 as $key10 => $value10}
    <!-- {$a=0}{$b=0}{$c=0}{$d=0}{$e=0}
        {if $j == 1}
        {foreach $arr_thang as $thang}
            {if isset($value10[$thang][0])}
            {if $value10[$thang][0]['danhgia_lds'] == 8}{$a++}{/if}
            {if $value10[$thang][0]['danhgia_lds'] == 6}{$b++}{/if}
            {if $value10[$thang][0]['danhgia_lds'] == 5}{$c++}{/if}
            {if $value10[$thang][0]['danhgia_lds'] == 4}{$d++}{/if}
            {if $value10[$thang][0]['danhgia_lds'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {else}
        {foreach $arr_thang as $thang}
            {if isset($value10[$thang][0])}
            {if $value10[$thang][0]['danhgia_gd'] == 8}{$a++}{/if}
            {if $value10[$thang][0]['danhgia_gd'] == 6}{$b++}{/if}
            {if $value10[$thang][0]['danhgia_gd'] == 5}{$c++}{/if}
            {if $value10[$thang][0]['danhgia_gd'] == 4}{$d++}{/if}
            {if $value10[$thang][0]['danhgia_gd'] == 2}{$e++}{/if}
            {/if}
        {/foreach}
        {/if}
        
        {$a= $a+$b}
        {$tsx=0}{$tt=0} -->
    <tr>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$jj++}</td>
        <td class="text-left"  style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">{$j++}</td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;">
            {$tencanbo[$key10]}
        </td>
        

       
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$a}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$c}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $d>0}{$d}{/if}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000 ;padding-right: 10px;"><b>{if $e>0}{$e}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $a>0}{$tsx=$a*500000} {formattien($tsx)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{if $c>0}{$tt=$c*200000} {formattien($tt)}{/if}</b>
        </td>
         <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000; padding-right: 10px;"><b>{$tongtien = $tsx+$tt}{formattien($tongtien)}</b>
        </td>
        <td style="border-top:0.5pt solid #000; border-left: 0.5pt solid #000;border-right: 0.5pt solid #000; padding-right: 10px;">
            
        </td>

    </tr>
   {/foreach}
   <tr>
      <td colspan="11"  style="border-top:0.5pt solid #000;"></td>
    </tr>
</table>



          

