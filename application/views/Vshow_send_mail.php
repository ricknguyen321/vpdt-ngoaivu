<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header h1no">
      <h1 class="font">
        Show send mail
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">STT</th>
                                    <th width="20%">Mail</th>
                                    <th width="25%">Mô tả</th>
                                    <th width="10%">Thời gian</th>
                                    <th>Nội Dung</th>
                                    <th width="5%">Loại</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($thongtin)}{$i=1}
                                    {foreach $thongtin as $tt}
                                        <tr>
                                            <td>{$i++}</td>
                                            <td>{$tt.sMail}</td>
                                            <td>{$tt.sMoTa}</td>
                                            <td>{date_time($tt.sThoiGian)}</td>
                                            <td>{catKyTu($tt.sNoiDung_Loi)}</td>
                                            <td class="text-center">{if $tt.iLoai==1}VB{else}GM{/if}</td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>