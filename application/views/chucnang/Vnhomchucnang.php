<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Nhóm chức năng
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Sở Ngoại Vụ</li>
        <li>Chức năng</li>
        <li class="active">Nhóm chức năng</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dulieu">Tên nhóm chức năng</label>
                                <input type="text" name="dulieu" class="form-control"placeholder="Nhập tên nhóm chức năng" value="{($thongtin)?($thongtin[0]['sTenNhomCN']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="dulieu">Icon</label>
                                <input type="text" name="icon" class="form-control"placeholder="Nhập tên icon" value="{($thongtin)?($thongtin[0]['iCon']):NULL}">
                            </div>
                            <div class="form-group">
                                <label for="dulieu">Tên màu</label>
                                <input type="text" name="mau" class="form-control"placeholder="Nhập tên màu" value="{($thongtin)?($thongtin[0]['sMaMau']):NULL}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật nhóm chức năng':'Thêm nhóm chức năng'}</button>
                                {if !empty($thongtin)}<a href="{$url}nhomchucnang" class="btn btn-primary pull-right" >Quay lại thêm mới</a>{/if}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:5%" class="text-center">STT</th>
                                            <th style="width:60%">Tên nhóm chức năng</th>
                                            <th style="width:15%" class="text-center">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                <tr>
                                                    <td>{$i++}</td>
                                                    <td>{$dl.sTenNhomCN}</td>
                                                    <td class="text-center"><a href="{$url}nhomchucnang/{$dl.PK_iMaNhomCN}" class="btn btn-xs btn-default" ata-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-edit"></i></a></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>