<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Chức năng
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Sở Ngoại Vụ</li>
        <li>Danh mục</li>
        <li class="active">Chức năng</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <form action="" method="post" class="form-horizontal" autocomplete="off">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Chức năng<label style="color:red;" for="">*</label></label>
                                <div class="col-sm-9">
                                    <input type="text" required name="dulieu" class="form-control"placeholder="Nhập tên Chức năng" value="{($thongtin)?($thongtin[0]['sTenCN']):NULL}">
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Link<label style="color:red;" for="">*</label></label>
                                <div class="col-sm-9">
                                    <input type="text" required name="link" class="form-control"placeholder="Nhập tên link chức năng" value="{($thongtin)?($thongtin[0]['sLink']):NULL}">
                                </div>
                            </div>                       
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nhóm chức năng<label style="color:red;" for="">*</label></label>
                                <div class="col-sm-9">
                                    <select name="nhomchucnang" style="width:100%" required id="" class="form-control select2">
                                    <option value="">-- Chọn Nhóm chức năng --</option>
                                    {if !empty($dstheloai)}
                                        {foreach $dstheloai as $tl}
                                            <option value="{$tl.PK_iMaNhomCN}" {($tl.PK_iMaNhomCN==$thongtin[0]['FK_iMaNhomCN'])?'selected':''}>{$tl.sTenNhomCN}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <button class="btn btn-primary pull-right" name="luudulieu" value="luudulieu" style="margin-left:10px;">{($thongtin)?'Cập nhật Chức năng':'Thêm Chức năng'}</button>
                                {if !empty($thongtin)}<a href="{$url}chucnang" class="btn btn-primary pull-right" >Quay lại thêm mới</a>{/if}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                            <div class="col-md-12"  style="overflow-x:auto;">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:5%" class="text-center">STT</th>
                                            <th style="width:60%">Tên Chức năng</th>
                                            <th style="width:15%" class="text-center">Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if !empty($dsdulieu)}{$i=1}
                                            {foreach $dsdulieu as $dl}
                                                <tr>
                                                    <td>{$i++}</td>
                                                    <td>{$dl.sTenCN}</td>
                                                    <td class="text-center"><a href="{$url}chucnang/{$dl.PK_iMaCN}" class="btn btn-xs btn-default" ata-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-edit"></i></a></td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>