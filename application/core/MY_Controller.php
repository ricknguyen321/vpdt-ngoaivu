<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class MY_Controller extends CI_Controller {

    protected $_session;

    public function __construct() {

        parent::__construct();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        $CI = &get_instance();

        $this->load->model('Mdangnhap');

        $token = $this->input->get('token');

        $data_ss = $this->Mdangnhap->session_ci(sha1($token))['data'];

        $s = unserialize_session_data(strstr($data_ss,'vanban'));

//        pr($s['nam']);

        if(isset($token)){

            $year = $s['nam'];

            if($year > 2017){

                $this->db = $CI->load->database('default_'.$year, TRUE);

            }

            $session_data=array('vanban'=> $s['vanban'],'nam'=> $s['nam']);

            $this->session->set_userdata($session_data);

            $this->_session =  $this->session->userdata()['vanban'];

//            pr($session_data);

        }else{

            $year = $this->session->userdata['nam'];

            if($year > 2017){

                $this->db = $CI->load->database('default_'.$year, TRUE);

            }

            $this->_session = $this->session->userdata['vanban'];

        }

        //date_default_timezone_set('Asia/Ho_Chi_Minh');

        // Không có session, đá về trang đăng nhập

        if(!isset($this->_session) || empty($this->_session)) {

            redirect(base_url().'dangnhap');

            exit();

        }

        if(_post('nam'))

        {

            $nam = _post('nam');

            if($nam > 2017){

                $CI = &get_instance();

                $this->db = $CI->load->database('default_'.$nam, TRUE);

                $_SESSION['nam'] = $nam;

            }

            else{

                $CI = &get_instance();

                $this->db = $CI->load->database('default', TRUE);

                $_SESSION['nam'] = $nam;

            }

        }

    }



}



/* End of file MY_Controller.php */

/* Location: ./application/core/MY_Controller.php */