<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']					= 'Cdangnhap';
$route['404_override']							= '';
$route['viethuongdansudung']					= 'huongdansudung/Chuongdansudung';
$route['xemhuongdansudung']						= 'huongdansudung/Cxemhuongdansudung';
$route['lichhopso']								= 'vanbandi/Clichhopso';
$route['nhaptheabtc']							= 'abtc/Cnhaptheabtc';
$route['nhaphosoabtc']							= 'abtc/Cnhaphosoabtc';
$route['hosoabtc']								= 'abtc/Chosoabtc';
$route['dstheabtc']								= 'abtc/Cdstheabtc';
$route['dshosoabtc']							= 'abtc/Cdshosoabtc';
$route['xinnghiphep']							= 'nghiphep/Cxinnghiphep';
$route['nghiphep']								= 'nghiphep/Cnghiphep';
$route['dsnghiphep']							= 'nghiphep/Cdsnghiphep';
$route['duyetnghiphep']							= 'nghiphep/Cduyetnghiphep';
$route['nhapcuoctiep']							= 'cuoctiep/Cnhapcuoctiep';
$route['dscuoctiep']							= 'cuoctiep/Cdscuoctiep';
$route['cuoctiep']								= 'cuoctiep/Ccuoctiep';
$route['nhaphnht']								= 'cuoctiep/Cnhaphnht';
$route['dshnht']								= 'cuoctiep/Cdshnht';
$route['hnht']									= 'cuoctiep/Chnht';
$route['dsfilechung']							= 'vanbanden/Cdsfilechung';

$route['giaymoidahopchuaxuly']					= 'vanbanden/Cgiaymoidahopchuaxuly';
// tài liệu phục vụ họp
$route['sapxeptailieuhop']						= 'tailieuphucvuhop/Csapxeptailieuhop';
$route['nhapmoitailieu']						= 'tailieuphucvuhop/Cnhapmoitailieu';
// $route['danhsachtailieu']					= 'tailieuphucvuhop/Cdanhsachtailieu';
$route['show_send_mail']						= 'Cshow_send_mail';
// $route['send_sms']							= 'Csend_sms';
// trả lời kiến nghị cử tri
$route['linhvuccha']							= 'traloikiennghicutri/Clinhvuccha';
$route['linhvuccon']							= 'traloikiennghicutri/Clinhvuccon';
$route['nhapmoikiennghi']						= 'traloikiennghicutri/Cnhapmoikiennghi';
// $route['danhsachkiennghi']					=  'traloikiennghicutri/Cdanhsachkiennghi';
$route['traloikiennghi/[0-9]+']					=  'traloikiennghicutri/Ctraloikiennghi';
// hồ sơ cá nhân
$route['hoso']									= 'hoso/Choso';
$route['themhoso']									= 'hoso/Cthemhoso';
$route['chitiethoso']							= 'hoso/Cchitiethoso';
$route['xemhoso']								= 'hoso/Cxemhoso';
$route['hosophongban']							= 'hoso/Chosophongban';
// văn bản đi

$route['lanhdaoky']								= 'vanbandi/Clanhdaoky';
$route['vanthuphathanh']						= 'vanbandi/Cvanthuphathanh';
$route['vanbandild_xem']						= 'vanbandi/Cvanbandild_xem';
$route['vanbanditrinhky']						= 'vanbandi/Cvanbanditrinhky';

$route['vanbandidaxuly_cct']					= 'vanbandi/Cvanbandidaxuly_cct';
$route['vanbandidaxuly_ccp']					= 'vanbandi/Cvanbandidaxuly_ccp';
$route['vanbandidaxuly_tp']						= 'vanbandi/Cvanbandidaxuly_tp';
$route['vanbandidaxuly_pp']						= 'vanbandi/Cvanbandidaxuly_pp';
$route['vanbandidaxuly_cv']					    = 'vanbandi/Cvanbandidaxuly_cv';
$route['vanbandiusertao']					    = 'vanbandi/Cvanbandiusertao';

$route['vanbandichoxuly_ld']					= 'vanbandi/Cvanbandichoxuly_ld';
$route['vbdidaphchuaduyet']					    = 'vanbandi/Cvbdidaphchuaduyet';
$route['vanbandichoxuly_cct']					= 'vanbandi/Cvanbandichoxuly_cct';
$route['vanbandichoxuly_ccp']					= 'vanbandi/Cvanbandichoxuly_ccp';
$route['vanbandichoxuly_tp']					= 'vanbandi/Cvanbandichoxuly_tp';
$route['vanbandichoxuly_pp']					= 'vanbandi/Cvanbandichoxuly_pp';
$route['vanbandichoxuly_cv']					= 'vanbandi/Cvanbandichoxuly_cv';

$route['vanbandichoxuly_cvp']					= 'vanbandi/Cvanbandichoxuly_cvp';
$route['vanbantrinhky']							= 'vanbandi/Cvanbantrinhky';
$route['vanbanlanhdaodaduyet']					= 'vanbandi/Cvanbanlanhdaodaduyet';
$route['vanbandidaxuly']						= 'vanbandi/Cvanbandidaxuly';
$route['vanbandiphoihopdaxuly']					= 'vanbandi/Cvanbandiphoihopdaxuly';
$route['xulyvanbandiphoihop/[0-9]+']			= 'vanbandi/Cxulyvanbandiphoihop';
$route['vanbandiphoihop']						= 'vanbandi/Cvanbandiphoihop';
$route['vanbandichoxuly']						= 'vanbandi/Cvanbandichoxuly';
$route['vanban']								= 'vanbandi/Cvanban';
$route['nhaplichhop']							= 'vanbandi/Cnhaplichhop';
$route['scanvanban']							= 'vanbandi/Cscanvanban';
$route['dsvanbanchoso']							='vanbandi/Cdsvanbanchoso';
$route['dsvanbanchoph']							='vanbandi/Cdsvanbanchoph';
$route['dsvanban']								= 'vanbandi/Cdsvanban';
$route['dsvanbanubnd']							= 'vanbandi/Cdsvanbanubnd';
$route['dsvanbanubnd_vt']						= 'vanbandi/Cdsvanbanubnd_vt';
$route['dsgiaymoidi']							= 'vanbandi/Cdsgiaymoidi';
$route['thongtinvanban']						= 'vanbandi/Cthongtinvanban';
$route['timkiemvanbandi']						= 'vanbandi/Ctimkiemvanbandi';
$route['vanbanphongban']						= 'vanbandi/Cvanbanphongban';
$route['giaymoiphongban']						= 'vanbandi/Cgiaymoiphongban';
$route['lichhoplanhdao']						= 'vanbandi/Clichhoplanhdao';
// đề xuất phối hợp phòng phối hợp
$route['dexuatphoihop_cv_ph']		= 'vanbanden/Cdexuatphoihop_cv_ph';
$route['dexuatphoihop_pp_ph']		= 'vanbanden/Cdexuatphoihop_pp_ph';
$route['dexuatphoihop_tpcc_ph']		= 'vanbanden/Cdexuatphoihop_tpcc_ph';
$route['dexuatphoihop_tp_ph']		= 'vanbanden/Cdexuatphoihop_tp_ph';
$route['dexuatphoihop_tpcc']		= 'vanbanden/Cdexuatphoihop_tpcc';
$route['dexuatphoihop_tp']			= 'vanbanden/Cdexuatphoihop_tp';
$route['dexuatphoihop_pp']			= 'vanbanden/Cdexuatphoihop_pp';
$route['dexuatphoihop_cv']			= 'vanbanden/Cdexuatphoihop_cv';
// 15/6/2017
$route['quatrinhxuly/[0-9]+']					= 'vanbanden/Cquatrinhxuly';
$route['vanbanchoxuly_gd']						= 'vanbanden/Cvanbanchoxuly_gd';
$route['vanbanchoxuly_pgd']						= 'vanbanden/Cvanbanchoxuly_pgd';
$route['vanbanchoxuly_tp']						= 'vanbanden/Cvanbanchoxuly_tp';
$route['vanbanchoxuly_pp']						= 'vanbanden/Cvanbanchoxuly_pp';
$route['vanbanchoxuly_cv']						= 'vanbanden/Cvanbanchoxuly_cv';
$route['vanbanchoxuly_cc']						= 'vanbanden/Cvanbanchoxuly_cc';
$route['vanbanchoxuly_ccp']						= 'vanbanden/Cvanbanchoxuly_ccp';
$route['vanbandaxuly_cc']						= 'vanbanden/Cvanbandaxuly_cc';
$route['vanbandaxuly_ccp']						= 'vanbanden/Cvanbandaxuly_ccp';
$route['vanbanchoxuly_cvph']					= 'vanbanden/Cvanbanchoxuly_cvph';
$route['dschidao']								= 'vanbanden/Cdschidao';
$route['dsykien']								= 'vanbanden/Cdsykien';
$route['dschidaogm']								= 'vanbanden/Cdschidaogm';
$route['dsykiengm']								= 'vanbanden/Cdsykiengm';
$route['dschidaovbdi']							= 'vanbandi/Cdschidaovbdi';
$route['dsykienvbdi']							= 'vanbandi/Cdsykienvbdi';
$route['vanbandachidao']						= 'vanbanden/Cvanbandachidao_chuahoanthanh';
$route['dsphoihop']								= 'vanbanden/Cdsphoihop';
$route['vanbandaphanloai']						= 'vanbanden/Cvanbandaphanloai';
$route['vanbanchuyenlai']						= 'vanbanden/Cvanbanchuyenlai';
$route['chuyenvienchutrixuly']					= 'vanbanden/Cchuyenvienchutrixuly';
$route['dsvanbanphongdaxuly']					= 'vanbanden/Cdsvanbanphongdaxuly';
$route['dsvanbantheophong']						= 'vanbanden/Cdsvanbantheophong';
$route['vanbanquantrong']						= 'vanbanden/Cvanbanquantrong';
$route['dsphongchutri']							= 'vanbanden/Cdsphongchutri';
$route['dsphongchutri1']						= 'vanbanden/Cdsphongchutri1';
$route['giaymoidaxuly_gd']						= 'vanbanden/Cgiaymoidaxuly_gd';
$route['vanbantheodoi']							= 'vanbanden/Cvanbantheodoi';
$route['vanbantheodoi_pp']						= 'vanbanden/Cvanbantheodoi_pp';
$route['quytrinhgiaymoi/[0-9]+']				= 'vanbanden/Cquytrinhgiaymoi';
$route['danhsachtongthe']						= 'vanbanden/Cdanhsachtongthe';
// văn bản đến
$route['timkiem_cvp']							= 'vanbanden/Ctimkiem_cvp';
$route['ketquatimkiem_cvp']						= 'vanbanden/Cketquatimkiem_cvp';
$route['readmail']								= 'vanbanden/Creadmail';
$route['readmaildang']							= 'vanbanden/Creadmaildang';
$route['vanbanden']								= 'vanbanden/Cvanbanden';
$route['nhapgiaymoi']							= 'vanbanden/Cnhapgiaymoi';
$route['vanbanden/[0-9]+']						= 'vanbanden/Cvanbanden';
$route['nhapgiaymoi/[0-9]+']					= 'vanbanden/Csuagiaymoi';
$route['dsvanbanden']							= 'vanbanden/Cdsvanbanden';
$route['thongkeVBDtheocanbo']					= 'vanbanden/CthongkeVBDtheocanbo';

$route['vanbanstc']								= 'vanbanden/Cvanbanstc';
$route['dsgiaymoi']								= 'vanbanden/Cdsgiaymoi';
$route['thongtindaura']							= 'vanbanden/Cthongtindaura';
$route['timkiemvanbanden']						= 'vanbanden/Ctimkiemvanbanden';
$route['chuyenvienxuly']						= 'vanbanden/Cchuyenvienxuly';
$route['vanbanchoxuly']							= 'vanbanden/Cvanbanchoxuly';
$route['vanbanchopheduyet']						= 'vanbanden/Cvanbanchopheduyet';
$route['phongphoihop']							= 'vanbanden/Cphongphoihop';
$route['lichcongtac']							= 'vanbanden/Clichcongtac';
$route['lichhopphongct']							= 'vanbanden/Clichhopphongct';
$route['vanbandentumail']						= 'vanbanden/Cvanbandentumail';
$route['vanbandentumaildang']					= 'vanbanden/Cvanbandentumaildang';
$route['lichcongtactuan']						= 'vanbanden/Clichcongtactuan';
$route['danhsachchitiet']						= 'vanbanden/Clistdetail_cb';
$route['soanbaocao/[0-9]+']						= 'vanbanden/Csoanbaocao';
$route['soanbaocao']							= 'vanbanden/Csoanbaocao';
$route['quanlythongbao']						= 'vanbanden/Cquanlythongbao';
$route['quanlythongbao/[0-9]+']					= 'vanbanden/Cquanlythongbao';
$route['vbden']									= 'vanbanden/Capi';
$route['vbdi']									= 'vanbanden/Capivbdi';
$route['congtacdang']							= 'vanbanden/Ccongtacdang';
$route['danhsachcongtacdang']					= 'vanbanden/Cdscongtacdang';
$route['congtacdang_cvp']						= 'vanbanden/Ccongtacdang_cvp';
$route['congtacdang_gd']						= 'vanbanden/Ccongtacdang_gd';
$route['congtacdang_pgd']						= 'vanbanden/Ccongtacdang_pgd';
$route['phophongphoihop']						= 'vanbanden/Cphophongphoihop';
$route['vanbanchoxuly_tct']						= 'vanbanden/Cvanbanchoxuly_tct';
$route['chuyenvienxuly_tct']					= 'vanbanden/Cchuyenvienxuly_tct';
$route['dstocongtac']							= 'vanbanden/Cdstocongtac';
$route['tocongtac_pp']							= 'vanbanden/Ctocongtac_pp';
$route['tocongtac_cv']							= 'vanbanden/Ctocongtac_cv';
$route['dsdonthu']								= 'vanbanden/Cdsdonthu';
$route['dsnguoidungphoihop']					= 'vanbanden/Cdsphoihopcv';
$route['congtacdang_tp']						= 'vanbanden/Ccongtacdang_tp';
$route['congtacdang_pp']						= 'vanbanden/Ccongtacdang_pp';
$route['congtacdang_cv']						= 'vanbanden/Ccongtacdang_cv';
$route['congtacdang_cc']						= 'vanbanden/Ccongtacdang_cc';
$route['phoihopchuyenlai_tp']					= 'vanbanden/Cphoihopchuyenlai_tp';
$route['dsdexuatgiahan']						= 'vanbanden/Cdsdexuatgiahan';
$route['vanbandaxuly_gd']						= 'vanbanden/Cvanbandaxuly_gd';
$route['dshandadexuat']							= 'vanbanden/Cdshandadexuat';
$route['chuyendoi']								= 'vanbanden/Cchuyendoi';
$route['vanbandaxulystcph_gd']					= 'vanbanden/Cvanbandaxulystcph_gd';
$route['vanbandaxulystcph_tp']					= 'vanbanden/Cvanbandaxulystcph_tp';
$route['vanbandaxulystcph_cc']					= 'vanbanden/Cvanbandaxulystcph_cc';
$route['vanbandaxulystcph_ccp']					= 'vanbanden/Cvanbandaxulystcph_ccp';
$route['api/welcomejs/users']					= 'v1/api/welcomejs/users';
$route['api/example_api/users']					= 'v1/api/Example_api/users';
$route['api/vbden_api/users']					= 'v1/api/Vanbanden_api/users';
$route['api/vbdi_api/users']					= 'v1/api/Vanbandi_api/users';
$route['duyetbaocao']							= 'vanbanden/Cduyetbaocao';
$route['dssoanthaobituchoi']					= 'vanbanden/Cdssoanthaobituchoi';
$route['giaymoidaxuly_pp']						= 'vanbanden/Cgiaymoidaxuly_pp';
$route['vanbanphoihopdachidao']					= 'vanbanden/Cvanbanphoihopdachidao';
$route['dsvb_bgd']								= 'vanbanden/Cdsvb_bgd';
// giấy mời
$route['giaymoi_cvp']							= 'vanbanden/Cgiaymoi_cvp';
$route['giaymoi_gd']							= 'vanbanden/Cgiaymoi_gd';
$route['giaymoi_pgd']							= 'vanbanden/Cgiaymoi_pgd';
$route['giaymoi_pp']							= 'vanbanden/Cgiaymoi_pp';
$route['giaymoidaphanloai']						= 'vanbanden/Cgiaymoidaphanloai';
$route['giaymoi_tp']							= 'vanbanden/Cgiaymoi_tp';
$route['giaymoi_cv']							= 'vanbanden/Cgiaymoi_cv';
$route['giaymoi_cvph']							= 'vanbanden/Cgiaymoi_cvph';
$route['giaymoiphoihop']						= 'vanbanden/Cgiaymoiphoihop';
$route['giaymoidaxuly']							= 'vanbanden/Cgiaymoidaxuly';
$route['giaymoiphoihopdaxuly']					= 'vanbanden/Cdsgiaymoiphoihopdaxuly';
$route['giaymoichuyenvienchutrixuly']			= 'vanbanden/Cgiaymoichuyenvienchutrixuly';
$route['giaymoidagiaiquyet']					= 'vanbanden/Cgiaymoidagiaiquyet';
$route['giaymoichuyenvienchutrixuly/[0-9]+']	= 'vanbanden/Cgiaymoichuyenvienchutrixuly';
$route['giaymoichuyenvienxuly/[0-9]+']			= 'vanbanden/Cgiaymoichuyenvienxuly';
$route['giaymoiphoihop/chuyenvienphoihop']		= 'vanbanden/Cgiaymoiphoihop/insertDocAwait';
$route['giaymoi_cc']							= 'vanbanden/Cgiaymoi_cc';
$route['giaymoidaxuly_cc']						= 'vanbanden/Cgiaymoidaxuly_cc';
$route['giaymoi_ccp']							= 'vanbanden/Cgiaymoi_ccp';
$route['giaymoidaxuly_ccp']						= 'vanbanden/Cgiaymoidaxuly_ccp';
$route['dsgiaymoiketluanmoi']					= 'vanbanden/Cdsgiaymoiketluanmoi';
$route['dsgiaymoiketluandaxem']					= 'vanbanden/Cdsgiaymoiketluandaxem';
$route['dssoanthaodagui']						= 'vanbanden/Cdssoanthaodagui';
$route['giaymoiphophongphoihop']				= 'vanbanden/Cgiaymoiphophongphoihop';
$route['giaymoichoduyet']						= 'vanbanden/Cgiaymoichoduyet';
$route['giaymoiphoihopchuyenlai_tp']			= 'vanbanden/Cgiaymoiphoihopchuyenlai_tp';
$route['giaymoihoanthanh']						= 'vanbanden/Cgiaymoihoanthanh';
$route['dsgm_bgd']								= 'vanbanden/Cdsgm_bgd';
// thuộc tính văn bản
$route['nguoiky']          = 'thuoctinhvanban/Cnguoiky';
$route['nguoikyliennganh'] = 'thuoctinhvanban/Cnguoikyliennganh';
$route['chucvunguoiky']    = 'thuoctinhvanban/Cchucvunguoiky';
$route['domat']            = 'thuoctinhvanban/Cdomat';
$route['dokhan']           = 'thuoctinhvanban/Cdokhan';
$route['noiguiden']        = 'thuoctinhvanban/Cnoiguiden';
$route['khuvuc']           = 'thuoctinhvanban/Ckhuvuc';
$route['linhvuc']          = 'thuoctinhvanban/Clinhvuc';
$route['loaivanban']       = 'thuoctinhvanban/Cloaivanban';
$route['emaildonvi']       = 'thuoctinhvanban/Cemaildonvi';
$route['songayxuly']       = 'thuoctinhvanban/Csongayxuly';
$route['linhvuc_qldv']     = 'thuoctinhvanban/Clinhvuc_qldv';
$route['quytrinhiso']	   = 'thuoctinhvanban/CquytrinhISO';
$route['donvi_ngoai']	   = 'thuoctinhvanban/Cdonvi_ngoai';
// quản trị cấp cao
$route['donvicaphai']	   = 'quantricapcao/Cdonvicaphai';
$route['phongban']         = 'quantricapcao/Cphongban';
$route['chucvu']           = 'quantricapcao/Cchucvu';
$route['canbo']            = 'quantricapcao/Ccanbo';
$route['doimatkhaucn']     = 'quantricapcao/Cdoimatkhaucanhan';
$route['nghile']		   = 'quantricapcao/Cnghile';

$route['dangnhap']             = 'Cdangnhap'; 
$route['dangxuat']             = 'Cdangnhap/DangXuat'; 
$route['welcome.html']         = 'Cwelcome';
$route['hosocanbo']     	   = 'Cwelcome/DangNhap_NhanSu';
$route['welcomead']            = 'Cwelcomead';
//Thông tin nội bộ
$route['dauviec_quanhuyen']= 'thongtinnoibo/Cdauviec_quanhuyen';
$route['thongtinmoi']      = 'thongtinnoibo/Cthongtinmoi';
$route['thongtinguidi']    = 'thongtinnoibo/Cthongtinguidi';
$route['thongtindaxem']    = 'thongtinnoibo/Cthongtindaxem';
$route['soanthaothongtin'] = 'thongtinnoibo/Csoanthaothongtin';
$route['xemchitiet']       = 'thongtinnoibo/Cxemchitiet';
$route['xemchitietguidi']  = 'thongtinnoibo/Cxemchitietguidi';
$route['chuyendi']		   = 'thongtinnoibo/Cchuyendi';
$route['danhsachvbdi']	   = 'thongtinnoibo/Cdanhsachvbdi';
$route['chitietvbdendi']   = 'thongtinnoibo/Cchitietvbdendi';
$route['danhsachvbden']	   = 'thongtinnoibo/Cdanhsachvbden';
$route['mocham'] 		   = 'thongtinnoibo/Cmocham';
$route['mochamthang'] 		   = 'thongtinnoibo/Cmochamthang';
$route['kiemtracongvu'] 		   = 'thongtinnoibo/Ckiemtracongvu';



// văn bản đến của chi cục
$route['vanbandenchicuc']			= 'vanbandenchicuc/Cvanbandenchicuc';
$route['dsvanbandenchicuc']			= 'vanbandenchicuc/Cdsvanbandenchicuc';
$route['dsvanbandenchicuctheodoi']	= 'vanbandenchicuc/Cdsvanbandenchicuctheodoi';
// upload files
$route['upload_files_vbdi_gm']		= 'vanbandi/Cupload_files_vbdi_gm';
$route['upload_files_vbdi']			= 'vanbandi/Cupload_files_vbdi';
$route['upload_files_vbden']		= 'vanbanden/Cupload_files_vbden';
$route['teptindi']					= 'vanbandi/Cteptindi';
$route['teptinden']					= 'vanbanden/Cteptinden';

// báo cáo thống kê
$route['tonghop_moi']			= 'thongke/Ctonghop_moi';
$route['tonghop']				= 'thongke/Ctonghop';
$route['baocaotonghop']			= 'thongke/Cbaocaotonghop';
$route['insoluutru_di']			= 'thongke/Cinsoluutru_di';
$route['insoluutru_den']		= 'thongke/Cinsoluutru_den';
$route['baocaodauvieccanhan']	= 'thongke/Cbaocaodauvieccanhan';
$route['baocaodauviecphong']	= 'thongke/Cbaocaodauviecphong';
$route['baocaodauviecbgd']		= 'thongke/Cbaocaodauviecbgd';
$route['thongkevanbantheolanhdao']	= 'thongke/Cthongkevanbantheolanhdao';
$route['thongkevanbantheophong']    = 'thongke/Cthongkevanbantheophong';
$route['danhsachvanban']			= 'thongke/Cdanhsachvanban';

// văn bản đi đơn vị cấp hai
$route['upload_files_di_caphai']	= 'vanbandi_caphai/Cupload_files_di_caphai';
$route['teptindi_caphai']			= 'vanbandi_caphai/Cteptindi_caphai';
$route['danhsachvanbandiphong']		= 'vanbandi_caphai/Cdanhsachvanbandiphong';
$route['thongtinvanbandi']			= 'vanbandi_caphai/Cthongtinvanbandi';
$route['danhsachvanbandi']			= 'vanbandi_caphai/Cdanhsachvanbandi';
$route['danhsachvanbanchoso']		= 'vanbandi_caphai/Cdanhsachvanbanchoso';
$route['nhapmoivanbandi']			= 'vanbandi_caphai/Cnhapmoivanbandi';
// văn bản đến đơn vị cấp hai
$route['dsvanbanphongchutri']		= 'vanbanden_caphai/Cdsvanbanphongchutri';
$route['dsvanbanphongphoihop']		= 'vanbanden_caphai/Cdsvanbanphongphoihop';

$route['duyetketqua_ch']			= 'vanbanden_caphai/Cduyetketqua_ch';

$route['vanbandaxuly_cv_ph']		= 'vanbanden_caphai/Cvanbandaxuly_cv_ph';
$route['vanbandaxuly_pp_ph']		= 'vanbanden_caphai/Cvanbandaxuly_pp_ph';

$route['vanbanchoxuly_cv_ph']		= 'vanbanden_caphai/Cvanbanchoxuly_cv_ph';
$route['vanbanchoxuly_pp_ph']		= 'vanbanden_caphai/Cvanbanchoxuly_pp_ph';

$route['vanbandaxuly_cv_ch_ph']		= 'vanbanden_caphai/Cvanbandaxuly_cv_ch_ph';
$route['vanbandaxuly_pp_ch_ph']		= 'vanbanden_caphai/Cvanbandaxuly_pp_ch_ph';
$route['vanbandaxuly_tp_ch_ph']		= 'vanbanden_caphai/Cvanbandaxuly_tp_ch_ph';

$route['vanbanchoxuly_cv_ch_ph']	= 'vanbanden_caphai/Cvanbanchoxuly_cv_ch_ph';
$route['vanbanchoxuly_pp_ch_ph']	= 'vanbanden_caphai/Cvanbanchoxuly_pp_ch_ph';
$route['vanbanchoxuly_tp_ch_ph']	= 'vanbanden_caphai/Cvanbanchoxuly_tp_ch_ph';

$route['vanbandaxuly_cv_ch']		= 'vanbanden_caphai/Cvanbandaxuly_cv_ch';
$route['vanbandaxuly_pp_ch']		= 'vanbanden_caphai/Cvanbandaxuly_pp_ch';
$route['vanbandaxuly_tp_ch']		= 'vanbanden_caphai/Cvanbandaxuly_tp_ch';
$route['vanbandaxuly_ldp']			= 'vanbanden_caphai/Cvanbandaxuly_ldp';
$route['vanbandaxuly_ld']			= 'vanbanden_caphai/Cvanbandaxuly_ld';

$route['vanbanchoxuly_cv_ch']		= 'vanbanden_caphai/Cvanbanchoxuly_cv_ch';
$route['vanbanchoxuly_pp_ch']		= 'vanbanden_caphai/Cvanbanchoxuly_pp_ch';
$route['vanbanchoxuly_tp_ch']		= 'vanbanden_caphai/Cvanbanchoxuly_tp_ch';
$route['vanbanchoxuly_ldp']			= 'vanbanden_caphai/Cvanbanchoxuly_ldp';
$route['vanbanchoxuly_ld']			= 'vanbanden_caphai/Cvanbanchoxuly_ld';

$route['teptincaphai']				= 'vanbanden_caphai/Cteptincaphai';
$route['upload_files_caphai']		= 'vanbanden_caphai/Cupload_files_caphai';
$route['danhsachtongthe_caphai']	= 'vanbanden_caphai/Cdanhsachtongthe_caphai';
$route['vanbanchoxulygq']			= 'vanbanden_caphai/Cvanbanchoxulygq';
$route['nhapmoivanban']				= 'vanbanden_caphai/Cnhapmoivanban';
$route['sualaivanban']				= 'vanbanden_caphai/Csualaivanban';
$route['nhapmoigiaymoi']			= 'vanbanden_caphai/Cnhapmoigiaymoi';
$route['sualaigiaymoi']				= 'vanbanden_caphai/Csualaigiaymoi';

// quản lý đầu việcs
// đề xuất ra hạn
$route['dadexuatrahan_bgd']			= 'qldv/Cdadexuatrahan_bgd';
$route['dadexuatrahan_cct']			= 'qldv/Cdadexuatrahan_cct';
$route['dadexuatrahan_ccp']			= 'qldv/Cdadexuatrahan_ccp';
$route['dadexuatrahan_tp']			= 'qldv/Cdadexuatrahan_tp';
$route['dadexuatrahan_pp']			= 'qldv/Cdadexuatrahan_pp';
$route['dadexuatrahan_cv']			= 'qldv/Cdadexuatrahan_cv';

$route['dexuatrahan_bgd']			= 'qldv/Cdexuatrahan_bgd';
$route['dexuatrahan_cct']			= 'qldv/Cdexuatrahan_cct';
$route['dexuatrahan_ccp']			= 'qldv/Cdexuatrahan_ccp';
$route['dexuatrahan_tp']			= 'qldv/Cdexuatrahan_tp';
$route['dexuatrahan_pp']			= 'qldv/Cdexuatrahan_pp';
$route['dexuatrahan_cv']			= 'qldv/Cdexuatrahan_cv';

//danh sách văn bản phòng chủ trì và phòng phối hợp
$route['danhsachquahanphong']			= 'qldv/Cdanhsachquahanphong';

$route['dsdauviecphongchutri']          = 'qldv/Cdsdauviecphongchutri';
$route['dsdauviecphongphoihop']         = 'qldv/Cdsdauviecphongphoihop';

// ==== đầu việc chủ trì phối hợp
$route['chitietdauviecphct/[0-9]+']		= 'qldv/Cchitietdauviecphct';
$route['dauviecchoxulyphct_tp_cc']		= 'qldv/Cdauviecchoxulyphct_tp_cc';
$route['dauviecchoxulyphct_pp_cc']		= 'qldv/Cdauviecchoxulyphct_pp_cc';
$route['dauviecchoxulyphct_cv']			= 'qldv/Cdauviecchoxulyphct_cv';
$route['dauviecchoxulyphct_pp']			= 'qldv/Cdauviecchoxulyphct_pp';
// ==== đầu việc phối hợp
$route['dauviecdaxulyph_pp']			= 'qldv/Cdauviecdaxulyph_pp';
$route['dauviecdaxulyph_tp']			= 'qldv/Cdauviecdaxulyph_tp';

$route['dauviecchoxulyph_pp_cc']		= 'qldv/Cdauviecchoxulyph_pp_cc';
$route['dauviecchoxulyph_tp_cc']		= 'qldv/Cdauviecchoxulyph_tp_cc';
$route['dauviecchoxulyph_ccp']			= 'qldv/Cdauviecchoxulyph_ccp';
$route['dauviecchoxulyph_cct']			= 'qldv/Cdauviecchoxulyph_cct';
$route['dauviecchoxulyph_cv']			= 'qldv/Cdauviecchoxulyph_cv';
$route['dauviecchoxulyphph_cv']			= 'qldv/Cdauviecchoxulyphph_cv';
$route['dauviecchoxulyph_pp']			= 'qldv/Cdauviecchoxulyph_pp';
$route['dauviecchoxulyph_tp']			= 'qldv/Cdauviecchoxulyph_tp';

// ==== đầu việc chủ trì
$route['dauviecdaxuly_pp_cc']			= 'qldv/Cdauviecdaxuly_pp_cc';
$route['dauviecdaxuly_tp_cc']			= 'qldv/Cdauviecdaxuly_tp_cc';
$route['dauviecdaxuly_ccp']				= 'qldv/Cdauviecdaxuly_ccp';
$route['dauviecdaxuly_cct']				= 'qldv/Cdauviecdaxuly_cct';
$route['dauviecdaxuly_cv']				= 'qldv/Cdauviecdaxuly_cv';
$route['dauviecdaxuly_pp']				= 'qldv/Cdauviecdaxuly_pp';
$route['dauviecdaxuly_tp']				= 'qldv/Cdauviecdaxuly_tp';
$route['dauviecdaxuly_pgd']				= 'qldv/Cdauviecdaxuly_pgd';
$route['dauviecdaxuly_gd']				= 'qldv/Cdauviecdaxuly_gd';

$route['dauviecchoxuly_pp_cc']			= 'qldv/Cdauviecchoxuly_pp_cc';
$route['dauviecchoxuly_tp_cc']			= 'qldv/Cdauviecchoxuly_tp_cc';
$route['dauviecchoxuly_ccp']			= 'qldv/Cdauviecchoxuly_ccp';
$route['dauviecchoxuly_cct']			= 'qldv/Cdauviecchoxuly_cct';
$route['dauviecchoxuly_cv']				= 'qldv/Cdauviecchoxuly_cv';
$route['dauviecchoxuly_pp']				= 'qldv/Cdauviecchoxuly_pp';
$route['dauviecchoxuly_tp']				= 'qldv/Cdauviecchoxuly_tp';
$route['dauviecchoxuly_pgd']			= 'qldv/Cdauviecchoxuly_pgd';
$route['dauviecchoxuly_gd']				= 'qldv/Cdauviecchoxuly_gd';

$route['dauviectralai']					= 'qldv/Cdauviectralai';
$route['dauviecchoduyet']				= 'qldv/Cdauviecchoduyet';
$route['danhsachtailieuphong']			= 'qldv/Cdanhsachtailieuphong';
$route['danhsachtailieu']				= 'qldv/Cdanhsachtailieu';
$route['danhsachkiennghi']				= 'qldv/Cdanhsachkiennghi';
$route['teptintailieu']				    = 'qldv/Cteptintailieu';

$route['ketquatralai']					= 'qldv/Cketquatralai';
$route['duyetketqua']					= 'qldv/Cduyetketqua';
$route['addqldv']						= 'qldv/Caddqldv';
$route['addqldv/[0-9]+']				= 'qldv/Caddqldv';
$route['addqldvDetails/[0-9]+']			= 'qldv/CaddqldvDetails';
$route['viewldvDetails/[0-9]+']			= 'qldv/CviewqldvDetails';
$route['dsdauviecvanban']				= 'qldv/Cdauviecvanban';
$route['dsdauviectheophong']			= 'qldv/Cdauviectheophong';
$route['dsdauvieclinhvuc']				= 'qldv/Cdauvieclinhvuc';
$route['giaiquyetdauviec']				= 'qldv/giaiquyetdauviec';
$route['editDetail/[0-9]+']				= 'qldv/CeditDetail';
$route['editDetailSub/[0-9]+']			= 'qldv/CeditDetailSub';
$route['addDetailSub/[0-9]+']			= 'qldv/CaddDetailSub';
$route['dauviecchogiaiquyet']			= 'qldv/Cdsdauviecchogiaiquyet';
$route['dauviecdagiaiquyet']			= 'qldv/Cdsdauviecdagiaiquyet';
$route['chitietdauviec/[0-9]+']			= 'qldv/Cchitietdauviec';
$route['danhgiadauviec']				= 'qldv/Cdanhgiadauviec';
$route['dauviecdahoanthanh']			= 'qldv/Cdauviecdahoanthanh';
$route['vanbannguoiphoihop']			= 'qldv/Cvanbannguoiphoihop';
$route['vanbanphongphoihop']			= 'qldv/Cvanbanphongphoihop';
$route['chitietdauviecphoihop/[0-9]+']	= 'qldv/Cchitietdauviecphoihop';
$route['dauviecchitiet']				= 'qldv/Cdauviecchitiet';
$route['dauviecchitiettong']			= 'qldv/Cdauviecchitiettong';


// lãnh đạo giao

$route['addlanhdaogiao']						= 'lanhdaogiao/Caddlanhdaogiao';
$route['addlanhdaogiao/[0-9]+']					= 'lanhdaogiao/Caddlanhdaogiao';
$route['addlanhdaogiaoDetails/[0-9]+']			= 'lanhdaogiao/CaddlanhdaogiaoDetails';

$route['chitietcvlanhdaogiao']					= 'lanhdaogiao/Cchitietcvlanhdaogiao';
$route['viewlanhdaogiaoDetails/[0-9]+']			= 'lanhdaogiao/CviewlanhdaogiaoDetails';
$route['editlanhdaogiaoDetail/[0-9]+/[0-9]+']	= 'lanhdaogiao/CeditlanhdaogiaoDetail';

$route['lanhdaoduyet']							= 'lanhdaogiao/Clanhdaoduyet';
$route['tpchiaviec']							= 'lanhdaogiao/Ctpchiaviec';
$route['dsphongdangthuchien']					= 'lanhdaogiao/Cdsphongdangthuchien';
$route['ccpchiaviec']							= 'lanhdaogiao/Cccpchiaviec';
$route['ccpchidaodangthuchien']					= 'lanhdaogiao/Cccpchidaodangthuchien';
$route['tpccchiaviec']							= 'lanhdaogiao/Ctpccchiaviec';
$route['tpccdangthuchien']						= 'lanhdaogiao/Ctpccdangthuchien';

$route['ppchiaviec']							= 'lanhdaogiao/Cppchiaviec';
$route['dsppdangchidao']						= 'lanhdaogiao/Cdsppdangchidao';
$route['cvthuchien/[0-9]+']						= 'lanhdaogiao/Ccvthuchien';
$route['dslanhdaoduyet']						= 'lanhdaogiao/Cdslanhdaoduyet';
$route['dschuyenvienxuly']						= 'lanhdaogiao/Cdschuyenvienxuly';
$route['dscvphxuly']							= 'lanhdaogiao/Cdscvphxuly';

$route['cvphthuchien/[0-9]+']					= 'lanhdaogiao/Ccvphthuchien';
$route['xemchitietdauviec/[0-9]+']				= 'lanhdaogiao/Cxemchitietdauviec';
$route['tpphchiaviec']							= 'lanhdaogiao/Ctpphchiaviec';
$route['tpphdangthuchien']						= 'lanhdaogiao/Ctpphdangthuchien';
$route['ldphchiaviec']							= 'lanhdaogiao/Cldphchiaviec';
$route['ldphdangthuchien']						= 'lanhdaogiao/Cldphdangthuchien';

$route['dscongviecdahoanthanh']					= 'lanhdaogiao/Cdscongviecdahoanthanh';

// kế hoạch công tác
$route['kehoachcongtac']							= 'kehoachcongtac/Ckehoachcongtac';
$route['kehoachcongtac/[0-9]+/[0-9]+']				= 'kehoachcongtac/Ckehoachcongtac';
$route['dskehoachcongtac/[0-9]+']					= 'kehoachcongtac/Cdskehoachcongtac';
$route['dskehoachcongtac/[0-9]+/[0-9]+']			= 'kehoachcongtac/Cdskehoachcongtac';

$route['suakehoachcongtac/[0-9]+']					= 'kehoachcongtac/Csuakehoachcongtac';
$route['kqkehoachcongtac/[0-9]+']					= 'kehoachcongtac/Ckqkehoachcongtac';
$route['viewkehoachcongtac/[0-9]+']					= 'kehoachcongtac/Cviewkehoachcongtac';
$route['xemkehoachcongtac/[0-9]+']					= 'kehoachcongtac/Cxemkehoachcongtac';
$route['xemkehoachcongtac/[0-9]+/[0-9]+/[0-9]+']	= 'kehoachcongtac/Cxemkehoachcongtac';
$route['xemkehoachcongtacthang']					= 'kehoachcongtac/Cxemkehoachcongtacthang';
$route['xemkehoachcongtacthang/[0-9]+']				= 'kehoachcongtac/Cxemkehoachcongtacthang';
$route['xemkehoachcongtacthang/[0-9]+/[0-9]+']		= 'kehoachcongtac/Cxemkehoachcongtacthang';
$route['dskehoachcongtacthang']						= 'kehoachcongtac/Cdskehoachcongtacthang';
$route['dskehoachcongtacthang/[0-9]+']				= 'kehoachcongtac/Cdskehoachcongtacthang';
$route['dskehoachcongtacthang/[0-9]+/[0-9]+']		= 'kehoachcongtac/Cdskehoachcongtacthang';
$route['dskehoachcongtacthangld']					= 'kehoachcongtac/Cdskehoachcongtacthang_ld';
$route['dskehoachcongtactuanld']					= 'kehoachcongtac/Cdskehoachcongtactuan_ld';
$route['dskehoachcongtacthangtp']					= 'kehoachcongtac/Cdskehoachcongtacthang_tp';
$route['xemkehoachcongtac_lanhdao/[0-9]+']			= 'kehoachcongtac/Cxemkehoachcongtac_lanhdao';
$route['xemkehoachcongtac_lanhdao/[0-9]+/[0-9]+']	= 'kehoachcongtac/Cxemkehoachcongtac_lanhdao';
$route['xemkehoachcongtac_lanhdao']					= 'kehoachcongtac/Cxemkehoachcongtac_lanhdao';
$route['xemkehoachcongtac_GD']					= 'kehoachcongtac/Cxemkehoachcongtac_GD';
$route['xemkehoachcongtac_GD/[0-9]+/[0-9]+']					= 'kehoachcongtac/Cxemkehoachcongtac_GD';

$route['xemkehoachcongtacquy/[0-9]+/[0-9]+']		= 'kehoachcongtac/Cxemkehoachcongtacquy';
$route['dskehoachcongtacquy']						= 'kehoachcongtac/Cdskehoachcongtacquy';
$route['dskehoachcongtacquy/[0-9]+']				= 'kehoachcongtac/Cdskehoachcongtacquy';
$route['dskehoachcongtacquy/[0-9]+/[0-9]+']			= 'kehoachcongtac/Cdskehoachcongtacquy';
$route['dskehoachcongtacquyld']						= 'kehoachcongtac/Cdskehoachcongtacquy_ld';
$route['dskehoachcongtacquytp']						= 'kehoachcongtac/Cdskehoachcongtacquy_tp';
$route['kehoachcongtac_quahan']						= 'kehoachcongtac/Ckehoachcongtac_quahan';

$route['thongkebaocao']      		= 'kehoachcongtac/Cthongkebaocao';
$route['thongkebaocaothang']      		= 'kehoachcongtac/Cthongkebaocao_thang';
$route['thongkebaocaothangIn']      		= 'kehoachcongtac/Cthongkebaocao_thangIn';
$route['thongkebaocaothangxs']      		= 'kehoachcongtac/Cthongkebaocao_thangxs';

$route['lanhdaonhanxetthang']      		= 'kehoachcongtac/Clanhdaonhanxetthang';


$route['trinhlanhdao']						= 'kehoachcongtac/Ctrinhlanhdao';
$route['trinhlanhdao/[0-9]+']						= 'kehoachcongtac/Ctrinhlanhdao';

$route['dsvanbanquahan']      = 'vanbanden/Cdsvanbanquahan';
$route['dsvbsapdenhan']      = 'vanbanden/Cdsvbsapdenhan';
$route['dsvanbanquahan/[0-9]+']      = 'kehoachcongtac/Cdsvanbanquahan';
$route['dsvanbanquahan/[0-9]+/[0-9]+']      = 'kehoachcongtac/Cdsvanbanquahan';

$route['dssangtao']						= 'kehoachcongtac/Cdssangtao';
$route['dssangtao/[0-9]+']				= 'kehoachcongtac/Cdssangtao';
$route['thongkecongviec/[0-9]+/[0-9]+']						= 'kehoachcongtac/Cthongkecongviec';

//$route['chendulieu']   = 'Cdangnhap_new';
$route['lichtuanlanhdao']				= 'vanbanden/Clichtuanlanhdao';

$route['giahanvanban']					= 'vanbanden/Cgiahanvanban';



$route['khctphong']     				= 'kehoachcongtac/Ckhctphong';
$route['viewkhctphong']     			= 'kehoachcongtac/Cviewkhctphong';
$route['suakhctphong/[0-9]+']     		= 'kehoachcongtac/Csuakhctphong';
$route['suakhctphong/[0-9]+/[0-9]+']    = 'kehoachcongtac/Csuakhctphong';
$route['kqkhctphong']     			    = 'kehoachcongtac/Ckqkhctphong';
$route['translate_uri_dashes'] = FALSE;
$route['translate_uri_dashes'] = FALSE;
