<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdoimatkhaucanhan extends MY_Controller {
	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*\W).{8,16}$/";
		$data['title']    = 'Đổi mật khẩu cá nhân';
		$ma = $this->_session['PK_iMaCB'];
		$data['matkhaucu']  = '';
		$data['matkhaumoi'] = '';
		$data['xacnhan']    = '';
		if($this->input->post('doimatkhau'))
		{
			$data['matkhaucu']  = _post('matkhaucu');
			$data['matkhaumoi'] = _post('matkhaumoi');
			$data['xacnhan']    = _post('xacnhan');
			$kiemtra = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$ma,'tbl_canbo');
			if(md5($data['matkhaucu'])==$kiemtra[0]['sMatKhau'])
			{
				if($data['matkhaumoi']==$data['xacnhan'])
				{
					$result = preg_match ($pattern, $data['matkhaumoi']);
					if ( $result ) {
						$data=array(
						'sMatKhau' => md5(_post('matkhaumoi')),
						'sBanRo'   => _post('matkhaumoi')
						);
						$ketqua = $this->Mdanhmuc->capnhatDuLieu('PK_iMaCB',$ma,'tbl_canbo',$data);

						if($ketqua>0)
						{
							$data['title']    = 'Đổi mật khẩu cá nhân';
							$data['matkhaucu']  = '';
							$data['matkhaumoi'] = '';
							$data['xacnhan']    = '';
							$data['content'] = messagebox('Đổi mật khẩu thành công','info');
							//sleep(3);
							redirect('dangxuat');
						}
						else{
							$data['content'] = messagebox('Đổi mật khẩu thất bại','danger');
						}
					} else {
						$data['content'] = messagebox('MẬT KHẨU CHƯA ĐÚNG QUY TẮC','danger');
					}
				}
				else{
					$data['content'] = messagebox('Xác nhận mật khẩu không khớp','danger');
				}
			}
			else{
				$data['content'] = messagebox('Mật khẩu cũ không đúng','danger');
			}
			
		}

		$data['taikhoan'] = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$ma,'tbl_canbo');
		$temp['data']     = $data;
		$temp['template'] ='quantricapcao/Vdoimatkhaucanhan';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdoimatkhaucanhan.php */
/* Location: ./application/controllers/quantricapcao/Cdoimatkhaucanhan.php */