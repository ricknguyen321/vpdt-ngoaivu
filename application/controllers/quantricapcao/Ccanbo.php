<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ccanbo extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('quantricapcao/Mcanbo');
		$this->load->library('pagination');
	}
	public function index()
	{
		$taikhoan = $this->_session['sTaiKhoan'];
		if($taikhoan!='admin')
		{
			redirect('dsvanbanden');
		}
		$data['dsphongban'] = $this->Mcanbo->layDL('sTenPB','asc','tbl_phongban');
		$data['phongchicuc'] = [['ma'=>3,'ten'=>'Phòng một'],['ma'=>4,'ten'=>'Phòng hai'],['ma'=>5,'ten'=>'Phòng ba']];
		$data['dschucvu']   = $this->Mcanbo->layDL('sTenCV','asc','tbl_chucvu');
		$action = _post('action');
		if(!empty($action))
		{
			switch ($action) {
				case 'kiemtrataikhoan':
					$this->kiemtraTaKhoan();
					break;
				
				default:
					# code...
					break;
			}
		}
		$ma                 = _get('id');
		if(!empty($ma))
		{
			$data['active']  = 1;
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['active']  = '';
			$data['content'] = $this->themDuLieu();
		}
		$data['quyenhan'] = array(['ma'=>1,'ten'=>'Quản trị'],['ma'=>2,'ten'=>'Văn thư'],['ma'=>3,'ten'=>'Chánh văn phòng'],['ma'=>4,'ten'=>'Giám đốc'],['ma'=>5,'ten'=>'Phó giám đốc'],['ma'=>6,'ten'=>'Trưởng phòng/ban'],['ma'=>7,'ten'=>'Phó phòng/ban'],['ma'=>8,'ten'=>'Chuyên viên'],['ma'=>9,'ten'=>'Văn thư chuyên trách'],['ma'=>13,'ten'=>'Văn thư chi cục']);
		$data['thongtin'] = $this->_thongtin;
		$data['phongph']  = explode(',',$this->_thongtin[0]['FK_iMaPhongPT']);
		$canbo             = $this->DSCB();
		$data['dsdulieu']  = $canbo['info'];
		$data['phongban']  = $canbo['phongban'];
		$data['chucvu']    = $canbo['chucvu'];
		$data['quyen']     = $canbo['quyenhan'];
		$data['hoten']     = $canbo['hoten'];
		$data['phantrang'] = $canbo['pagination'];
		$data['title']     = 'Quản lý thành viên';
		$temp['data']      = $data;
		$temp['template']  = 'quantricapcao/Vcanbo';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function kiemtraTaKhoan(){
		$id       = _post('id');
		$thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$id,'tbl_canbo');
		$taikhoan = _post('taikhoan');
		if(!empty($id))
		{
			if($taikhoan==$thongtin[0]['sTaiKhoan'])
			{
				$ketqua = 'ok';
			}
			else{
				$kiemtra = $this->Mdanhmuc->layDuLieu('sTaiKhoan',$taikhoan,'tbl_canbo');
				if(!empty($kiemtra))
				{
					$ketqua = 'trung';
				}else{
					$ketqua = 'ok';
				}
			}
		}
		else{
			$kiemtra = $this->Mdanhmuc->layDuLieu('sTaiKhoan',$taikhoan,'tbl_canbo');
			if(!empty($kiemtra))
			{
				$ketqua = 'trung';
			}else{
				$ketqua = 'ok';
			}
		}
		echo json_encode($ketqua);exit();
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$phutrach = _post('phongbanphutrach');
			if(!empty($phutrach))
			{
				$phongphutrach = implode(',', $phutrach);
			}
			else{
				$phongphutrach = 11;
			}
			$data=array(
				'sHoTen'         => _post('hoten'),
				'FK_iMaPhongHD'  => _post('phongbanhoatdong'),
				'FK_iMaPhongPT'  => $phongphutrach,
				'FK_iMaCV'       => _post('chucvu'),
				'sTaiKhoan'      => _post('taikhoan'),
				'sMatKhau'       => md5(_post('matkhau')),
				'sBanRo'         => _post('matkhau'),
				'sDienThoai'     => _post('dienthoai'),
				'sEmail'         => _post('email'),
				'iQuyenHan_DHNB' => _post('quyendhnb'),
				'iQuyenHan_TNVB' => _post('quyentnvb'),
				'iQuyenDB'       => _post('phongchicuc'),
				'iPhanMem'       => _post('phanmem'),
				'iTrangThai'     => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_canbo',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm thành viên thành công','info');
			}
			else{
				return messagebox('Thêm thành viên thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$ma,'tbl_canbo');
		if(_post('luudulieu'))
		{
			$phutrach = _post('phongbanphutrach');
			if(!empty($phutrach))
			{
				$phongphutrach = implode(',', $phutrach);
			}
			else{
				$phongphutrach = 11;
			}
			$data=array(
				'sHoTen'         => _post('hoten'),
				'FK_iMaPhongHD'  => _post('phongbanhoatdong'),
				'FK_iMaPhongPT'  => $phongphutrach,
				'FK_iMaCV'       => _post('chucvu'),
				'sTaiKhoan'      => _post('taikhoan'),
				'sDienThoai'     => _post('dienthoai'),
				'sEmail'         => _post('email'),
				'iQuyenHan_DHNB' => _post('quyendhnb'),
				'iQuyenHan_TNVB' => _post('quyentnvb'),
				'iQuyenDB'       => _post('phongchicuc'),
				'iPhanMem'       => _post('phanmem'),
				'iTrangThai'     => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaCB',$ma,'tbl_canbo',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$ma,'tbl_canbo');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật thành viên thành công','info');
			}
			else{
				return messagebox('Cập nhật thành viên thất bại','danger');
			}
		}
	}
	public function DSCB() 
	{
		$phongban = _get('phongban');
		$chucvu   = _get('chucvu');
		$quyenhan = _get('quyenhan');
		$hoten    = _get('hoten');
		$config['base_url']             = base_url().'canbo?phongban='.$phongban.'&chucvu='.$chucvu.'&quyenhan='.$quyenhan.'&hoten='.$hoten;
		$config['total_rows']           = $this->Mcanbo->demDSCB($phongban,$chucvu,$quyenhan,$hoten);
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'canbo');
      	}
		$data['phongban'] = $phongban;
		$data['chucvu']   = $chucvu;
		$data['quyenhan'] = $quyenhan;
		$data['hoten']    = $hoten;
		$data['info']       = $this->Mcanbo->layDSCB($phongban,$chucvu,$quyenhan,$hoten,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Ccanbo.php */
/* Location: ./application/controllers/quantricapcao/Ccanbo.php */