<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cphongban extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý phòng ban';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['active']  = 1;
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['active']  = '';
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_phongban','desc');
		$temp['data']     = $data;
		$temp['template'] = 'quantricapcao/Vphongban';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenPB'       => _post('ten'),
				'sVietTat'     => _post('tenviettat'),
				'sMoTa'        => _post('mota'),
				'iTrangThai'   => _post('trangthai'),
				'iLoaiPhanMem' => _post('phanmem'),
				'iPhanHe'      => _post('phanhe'),
				'iChiCuc'      => _post('chicuc'),
				'sNgayNhap'    => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_phongban',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm phòng ban thành công','info');
			}
			else{
				return messagebox('Thêm phòng ban thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$ma,'tbl_phongban');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenPB'       => _post('ten'),
				'sVietTat'     => _post('tenviettat'),
				'sMoTa'        => _post('mota'),
				'iTrangThai'   => _post('trangthai'),
				'iLoaiPhanMem' => _post('phanmem'),
				'iPhanHe'      => _post('phanhe'),
				'iChiCuc'      => _post('chicuc'),
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaPB',$ma,'tbl_phongban',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$ma,'tbl_phongban');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật phòng ban thành công','info');
			}
			else{
				return messagebox('Cập nhật phòng ban thất bại','danger');
			}
		}
	}

}

/* End of file Cphongban.php */
/* Location: ./application/controllers/quantricapcao/Cphongban.php */