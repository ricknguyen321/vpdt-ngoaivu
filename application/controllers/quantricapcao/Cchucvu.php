<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchucvu extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý chức vụ';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_chucvu','desc');
		$temp['data']     = $data;
		$temp['template'] = 'quantricapcao/Vchucvu';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenCV'     => _post('ten'),
				'sMoTa'      => _post('mota'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_chucvu',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm chức vụ thành công','info');
			}
			else{
				return messagebox('Thêm chức vụ thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCV',$ma,'tbl_chucvu');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenCV'     => _post('ten'),
				'sMoTa'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaCV',$ma,'tbl_chucvu',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCV',$ma,'tbl_chucvu');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật chức vụ thành công','info');
			}
			else{
				return messagebox('Cập nhật chức vụ thất bại','danger');
			}
		}
	}

}

/* End of file Cchucvu.php */
/* Location: ./application/controllers/quantricapcao/Cchucvu.php */