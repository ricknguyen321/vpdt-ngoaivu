<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdonvicaphai extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('quantricapcao/Mdonvicaphai');
	}
	public function index()
	{
		$data['phongban']  = $this->Mdonvicaphai->layDSPB_CH();
		$data['maphong']   = _get('phongban');
		if(_post('luudulieu'))
		{
			$macb			= _post('macb');
			$quyenhan		= _post('quyenhan');
			$phongban		= _post('phongban');
			$tendinhdanh	= _post('tendinhdanh');
			if(!empty($macb))
			{
				foreach ($macb as $key => $value) {
					$mangcb[]=array(
						'PK_iMaCB'			=> $value,
						'donvi_caphai'		=> 1,
						'quyenhan_caphai'	=> $quyenhan[$key],
						'phong_caphai'		=> $phongban[$key],
						'tendinhdanh'		=> $tendinhdanh[$key]
					);
				}
				$this->Mdanhmuc->capnhatnhieuDuLieu('tbl_canbo',$mangcb,'PK_iMaCB');
			}
		}
		$data['dscanbo']   = $this->Mdonvicaphai->layDSCB($data['maphong']);
		$data['quyenhan']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_quyenhan_caphai');
		$data['phong']	   = $this->Mdanhmuc->layDuLieu('FK_iMaDV',$data['maphong'],'tbl_phongban_caphai');
		$data['title']     = 'Quản lý đơn vị cấp hai';
		$temp['data']      = $data;
		$temp['template']  = 'quantricapcao/Vdonvicaphai';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cdonvicaphai.php */
/* Location: ./application/controllers/quantricapcao/Cdonvicaphai.php */