<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnghile extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Nghỉ lễ';
		$ma              = _get('id');
		if(!empty($ma))
		{
			if(_post('xoa'))
			{
				$this->xoaDuLieu();
			}
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			if(_post('xoa'))
			{
				$this->xoaDuLieu();
			}
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('iTrangThai','tbl_ngaynghi','1');
		$temp['data']     = $data;
		$temp['template'] = 'quantricapcao/Vnghile';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function xoaDuLieu()
	{
		$maxoa = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaNgayNghi',$maxoa,'tbl_ngaynghi');
		if($kiemtra>0)
		{
			redirect('nghile');
		}
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sNgayNghi'    => date_insert(_post('ngaynghi')),
				'sTenNgayNghi' => _post('mota'),
				'iTrangThai'   => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_ngaynghi',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm ngày nghỉ thành công','info');
			}
			else{
				return messagebox('Thêm ngày nghỉ thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNgayNghi',$ma,'tbl_ngaynghi');
		if(_post('luudulieu'))
		{
			$data=array(
				'sNgayNghi'    => date_insert(_post('ngaynghi')),
				'sTenNgayNghi' => _post('mota'),
				'iTrangThai'   => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaNgayNghi',$ma,'tbl_ngaynghi',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNgayNghi',$ma,'tbl_ngaynghi');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật ngày nghỉ thành công','info');
			}
			else{
				return messagebox('Cập nhật ngày nghỉ thất bại','danger');
			}
		}
	}

}

/* End of file Cnghile.php */
/* Location: ./application/controllers/quantricapcao/Cnghile.php */