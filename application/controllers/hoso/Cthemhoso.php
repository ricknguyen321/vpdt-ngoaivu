<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthemhoso extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('hoso/Mhoso');
	}
	public function index()
	{
		$data['title']   = 'Thêm mới hồ sơ';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		if (_post('themdl')) {
			$data['content'] = $this->themDuLieu();
		}
		if (_post('xoahoso')) {
			$data['content'] = $this->xoaDuLieu();
		}
		$action = _post('action');
		if(!empty($action))
		{
			switch ($action) {
				case 'timkiem':
					$this->timkiem();
					break;
				case 'themdulieu':
					$this->themVBHS();
					break;
				default:
					# code...
					break;
			}
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mhoso->layHScanhan($this->_session['PK_iMaCB']);
		$temp['data']     = $data;
		$temp['template'] = 'hoso/Vthemhoso';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themVBHS(){
		$loaivanban = _post('loaivanban');
		$mavanban   = _post('mavanban');
		$mahoso     = _post('mahoso');
		$macanbo    = $this->_session['PK_iMaCB'];
		$phongban   = $this->_session['FK_iMaPhongHD'];
		$kiemtra    = $this->Mhoso->kiemtraTrung($macanbo,$mahoso,$mavanban,$loaivanban);
		if($kiemtra==0)
		{
			if($loaivanban=='den'){
				$ketqua = $this->Mhoso->layThongTinVBDen($mavanban);
			}
			else{
				$ketqua = $this->Mhoso->layThongTinVBDi($mavanban);
			}
			$data=array(
				'FK_iMaVBDen' => ($loaivanban=='den')?$mavanban:0,
				'FK_iMaVBDi'  => ($loaivanban=='di')?$mavanban:0,
				'sThoiGian'   => date('Y-m-d H:i:s'),
				'FK_iMaCB'	  => $macanbo,
				'FK_iMaPB'	  => $phongban,
				'FK_iMaHS'	  => $mahoso,
				'iSoVB'		  => $ketqua['sovanban'],
				'sTenLVB'	  => $ketqua['sTenLVB'],
				'sKyHieu'     => $ketqua['sKyHieu'],
				'sNgayNhap'   => $ketqua['sNgayNhap'],
				'sMoTa'       => $ketqua['sMoTa'],
				'sDuongDan'   => $ketqua['sDuongDan']
			);
			$kiemtrathem= $this->Mdanhmuc->themdulieu('tbl_hosovanban',$data);
			if($kiemtrathem>0)
			{				
				echo json_encode('ok');exit();				 
			}
		}
		else{
			echo json_encode('trung');exit();
		}
	}
	public function timkiem()
	{
		$vanban   = _post('vanban');
		$sovanban = _post('sovanban');
		$mahoso   = _post('mahoso');
		$vanbandachon = $this->Mhoso->layHSDaChon($mahoso,$vanban);
		if(!empty($vanbandachon))
		{
			if($vanban=='di'){
				$mavanban = array_column($vanbandachon, 'FK_iMaVBDi');
			}
			else{
				$mavanban = array_column($vanbandachon, 'FK_iMaVBDen');
			}
		}else{
			$mavanban = array();
		}
		$ketqua   = $this->Mhoso->layVBTheoSo($vanban,$sovanban,$mavanban);
		echo json_encode($ketqua);exit();
	}
	public function themDuLieu()
	{

			$data=array(
				'sTenHS'     => _post('ten'),
				'sMota'      => _post('mota'),
				'FK_iMaCB'   => $this->_session['PK_iMaCB'],
				'iTrangThai' => _post('trangthai'),
				'sThoiGian'  => date('Y-m-d H:i:s'),
				'FK_iMaPB'   =>  $this->_session['FK_iMaPhongHD']
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_hoso',$data);
			if($kiemtra>0)
			{				
				//$this->notify();
				redirect(base_url().'hoso');
				return messagebox('Thêm hồ sơ THÀNH CÔNG','info');
			}
			else{
				return messagebox('Thêm hồ sơ thất bại','danger');
			}

	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaHS',$ma,'tbl_hoso');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenHS'     => _post('ten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra			= $this->Mdanhmuc->capnhatDuLieu('PK_iMaHS',$ma,'tbl_hoso',$data);
			$this->_thongtin	= $this->Mdanhmuc->layDuLieu('PK_iMaHS',$ma,'tbl_hoso');
			if($kiemtra>0)
			{
				redirect(base_url().'hoso');
				return messagebox('Cập nhật hồ sơ thành công','info');
			}
			else{
				return messagebox('Cập nhật hồ sơ thất bại','danger');
			}
		}
	}
	
	public function xoaDuLieu()
	{
		$ma = _post('xoahoso');
		
		$this->Mdanhmuc->xoaDuLieu('PK_iMaHS',$ma,'tbl_hoso');
		redirect(base_url().'hoso');
	}
	
	
	
	public function notify() {
			notify("Access granted", "success");
		}

}

/* End of file Choso.php */
/* Location: ./application/controllers/hoso/Choso.php */