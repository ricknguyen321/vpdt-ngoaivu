<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chosophongban extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('hoso/Mhoso');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['title']    = 'Hồ sơ phòng ban';
		$quyen    		  = $this->_session['iQuyenHan_DHNB'];
		if($quyen>6){
			redirect('welcome.html');
		}
		$data['thongtin'] = $this->_thongtin;
		$data['phongban'] = $this->Mhoso->layPB();
		$hoso                = $this->DSHoSo();
		$data['g_phongban']  = $hoso['phongban'];
		$data['g_tenhoso']   = $hoso['tenhoso'];
		$data['g_dulieu']    = $hoso['dulieu'];
		$data['g_phantrang'] = $hoso['phantrang'];
		$temp['data']     = $data;
		$temp['template'] = 'hoso/Vhosophongban';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function DSHoSo() 
	{
		$phongban = _get('phongban');
		$tenhoso  = _get('tenhoso');
		$config['base_url']             = base_url().'hosophongban?phongban='.$phongban.'&tenhoso='.$tenhoso;
		$config['total_rows']           = $this->Mhoso->demDSHS($phongban,$tenhoso);
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dsvanban');
      	}
		$data['phongban']  = $phongban;
		$data['tenhoso']   = $tenhoso;
		$data['dulieu']    = $this->Mhoso->layDSHS($phongban,$tenhoso,$config['per_page'], $data['page']);
		$data['phantrang'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Chosophongban.php */
/* Location: ./application/controllers/hoso/Chosophongban.php */