<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemhoso extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('hoso/Mhoso');
	}
	public function index()
	{
		$data['title']   = 'Quản lý hồ sơ';
		$quyen           = $this->_session['iQuyenHan_DHNB'];
		if($quyen!=4&&$quyen!=5)
		{
			redirect('dangnhap');
		}
		$data['phongban']	= _get('phongban');
		$data['dsphongban']	= $this->Mhoso->layPhongBanDuThao();
		if(!empty($data['dsphongban']))
		{
			$data['dsdulieu']   = $this->Mhoso->layDSHS_PB($data['phongban']);
		}
		else
		{
			$data['dsdulieu']   = '';
		}
		$temp['data']		= $data;
		$temp['template']	= 'hoso/Vxemhoso2';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cxemhoso.php */
/* Location: ./application/controllers/hoso/Cxemhoso.php */