<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchitiethoso extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('hoso/Mhoso');
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Chi tiết hồ sơ';
		$ma              = _get('id');
		$data['mahoso']   = $ma;
		$loai            = _get('loai');
		if (empty($loai)) {
			$loai = 'di';
		}
		$data['loai']   = $loai;
		$sovb            = _get('sovb');
		$data['sovb']   = $sovb;
		$trichyeu        = _get('trichyeu');
		$data['trichyeu']   = $trichyeu;
		
		if (!empty($sovb) || !empty($trichyeu)) {
			$data['dsvb'] = $this -> timkiem3($ma,$loai,$sovb,$trichyeu);
		}
		
		if(!is_numeric($ma))
		{
			redirect('hoso');
		}
		$data['tenhoso']  = $this->Mdanhmuc->layDuLieu('PK_iMaHS',$ma,'tbl_hoso');
		if(empty($data['tenhoso']))
		{
			redirect('hoso');
		}
		
		if(_post('luuhoso2'))
		{
			$this->themVBHS2();
		}
		if(_post('timkiem2'))
		{
			$this->timkiem2();
		}
		
		$action = _post('action');
		if(!empty($action))
		{
			switch ($action) {
				case 'timkiem':
					$this->timkiem();
					break;
				case 'themdulieu':
					$this->themVBHS();
					break;
				default:
					# code...
					break;
			}
		}
		
		if(_post('xoavanban'))
		{
			$mavanban = _post('xoavanban');
			$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaHSVB',$mavanban,'tbl_hosovanban');
			if($kiemtra>0)
			{
				$data['content'] = messagebox('Xóa văn bản trong hồ sơ thành công!','info');
			}
		}
		$taikhoan 		  = $this->_session['PK_iMaCB'];
		$quyen			  = $this->_session['iQuyenHan_DHNB'];
		$data['taikhoan']    = $taikhoan;
		if($quyen<=6)
		{
			$data['dsdulieu'] = $this->Mhoso->layChiTietHoSo(NULL,$ma);
		}
		else
		{
			$data['dsdulieu'] = $this->Mhoso->layChiTietHoSo($taikhoan,$ma);
		}
		$temp['data']     = $data;
		$temp['template'] = 'hoso/Vchitiethoso2';
		$this->load->view('layout_admin/layout', $temp);
	}
	
	public function themVBHS(){
		$loaivanban = _post('loaivanban');
		$mavanban   = _post('mavanban');
		$mahoso     = _post('mahoso');
		$macanbo    = $this->_session['PK_iMaCB'];
		$phongban   = $this->_session['FK_iMaPhongHD'];
		$kiemtra    = $this->Mhoso->kiemtraTrung($macanbo,$mahoso,$mavanban,$loaivanban);
		if($kiemtra==0)
		{
			if($loaivanban=='den'){
				$ketqua = $this->Mhoso->layThongTinVBDen($mavanban);
			}
			else{
				$ketqua = $this->Mhoso->layThongTinVBDi($mavanban);
			}
			$data=array(
				'FK_iMaVBDen' => ($loaivanban=='den')?$mavanban:0,
				'FK_iMaVBDi'  => ($loaivanban=='di')?$mavanban:0,
				'sThoiGian'   => date('Y-m-d H:i:s'),
				'FK_iMaCB'	  => $macanbo,
				'FK_iMaPB'	  => $phongban,
				'FK_iMaHS'	  => $mahoso,
				'iSoVB'		  => $ketqua['sovanban'],
				'sTenLVB'	  => $ketqua['sTenLVB'],
				'sKyHieu'     => $ketqua['sKyHieu'],
				'sNgayNhap'   => $ketqua['sNgayNhap'],
				'sMoTa'       => $ketqua['sMoTa'],
				'sDuongDan'   => $ketqua['sDuongDan']
			);
			$kiemtrathem= $this->Mdanhmuc->themdulieu('tbl_hosovanban',$data);
			if($kiemtrathem>0)
			{				
				echo json_encode('ok');exit();				 
			}
		}
		else{
			echo json_encode('trung');exit();
		}
	}
	public function timkiem()
	{
		$vanban   = _post('vanban');
		$sovanban = _post('sovanban');
		$trichyeu = _post('trichyeu');
		$mahoso   = _post('mahoso');
		$vanbandachon = $this->Mhoso->layHSDaChon($mahoso,$vanban);
		if(!empty($vanbandachon))
		{
			if($vanban=='di'){
				$mavanban = array_column($vanbandachon, 'FK_iMaVBDi');
			}
			else{
				$mavanban = array_column($vanbandachon, 'FK_iMaVBDen');
			}
		}else{
			$mavanban = array();
		}
		$ketqua   = $this->Mhoso->layVBTheoSo($vanban,$sovanban,$trichyeu,$mavanban);
		echo json_encode($ketqua);exit();
	}
	
	public function timkiem2()
	{
		if (!empty(_post('vanban'))){
			$loai = _post('vanban');
			//$data['loai']   = $loai;	
			echo $loai;
		}
		if (!empty(_post('sovanban'))){
			$sovb = _post('sovanban');			
			//$data['sovb']   = $sovb;	
			echo $sovb;			
		}
		if (!empty(_post('trichyeu'))){
			$trichyeu = _post('trichyeu');
			//$data['trichyeu']   = $trichyeu;
			echo $trichyeu;
		}
		redirect(base_url().'chitiethoso?id='._get('id').'&loai='.$loai.'&sovb='.$sovb.'&trichyeu='.$trichyeu);
	}
	
	public function timkiem3($ma,$loai,$sovb,$trichyeu)
	{
		if (!empty(_post('vanban'))){
			$loai = _post('vanban');
			$data['loai']   = $loai;	
		}
		if (!empty(_post('sovanban'))){
			$sovb = _post('sovanban');			
			$data['sovb']   = $sovb;	
		}
		if (!empty(_post('trichyeu'))){
			$trichyeu = _post('trichyeu');
			$data['trichyeu']   = $trichyeu;
		}
		$vanbandachon = $this->Mhoso->layHSDaChon($ma,$loai);
		if(!empty($vanbandachon))
		{
			if($loai=='di'){
				$mavanban = array_column($vanbandachon, 'FK_iMaVBDi');
			}
			else{
				$mavanban = array_column($vanbandachon, 'FK_iMaVBDen');
			}
		}else{
			$mavanban = array();
		}
		return $this->Mhoso->layVBTheoSo($loai,$sovb,$trichyeu,$mavanban);
		
	}
	
	public function themVBHS2(){
		
		if (_get('loai')){
			$loaivanban = _get('loai');
		} else {
			$loaivanban = 'di';
		}
		$mavanban   = _post('luuhoso2');		
		$mahoso     = _get('id');
		$trichyeu = _get('trichyeu');
		$macanbo    = $this->_session['PK_iMaCB'];
		$phongban   = $this->_session['FK_iMaPhongHD'];
		$kiemtra    = $this->Mhoso->kiemtraTrung($macanbo,$mahoso,$mavanban,$loaivanban);
		if($kiemtra==0)
		{
			if($loaivanban=='den'){
				$ketqua = $this->Mhoso->layThongTinVBDen($mavanban);
			}
			else{
				$ketqua = $this->Mhoso->layThongTinVBDi($mavanban);
			}
			$data=array(
				'FK_iMaVBDen' => ($loaivanban=='den')?$mavanban:0,
				'FK_iMaVBDi'  => ($loaivanban=='di')?$mavanban:0,
				'sThoiGian'   => date('Y-m-d H:i:s'),
				'FK_iMaCB'	  => $macanbo,
				'FK_iMaPB'	  => $phongban,
				'FK_iMaHS'	  => $mahoso,
				'iSoVB'		  => $ketqua['sovanban'],
				'sTenLVB'	  => $ketqua['sTenLVB'],
				'sKyHieu'     => $ketqua['sKyHieu'],
				'sNgayNhap'   => $ketqua['sNgayNhap'],
				'sMoTa'       => $ketqua['sMoTa'],
				'sDuongDan'   => $ketqua['sDuongDan']
			);
			$kiemtrathem= $this->Mdanhmuc->themdulieu('tbl_hosovanban',$data);
			if($kiemtrathem>0)
			{				
				redirect(base_url().'chitiethoso?id='._get('id').'&loai='._get('loai').'&sovb='._get('sovb').'&trichyeu='._get('trichyeu'));
			}		
		}
		else{
			return messagebox('Văn bản đã được thêm!','danger');
		}
	}

}

/* End of file Cchitiethoso.php */
/* Location: ./application/controllers/hoso/Cchitiethoso.php */