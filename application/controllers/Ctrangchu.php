<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctrangchu extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('danhmuc/Mloaitin');
		$this->load->model('tintuc/Mtintuc');
	}
	public function index()
	{
		$data['title']     = 'Sở Tài chính Hà Nội - Thành Phố Hà Nội';
		$data['tinnoibat'] = $this->Mtintuc->layTinTucNoiBat();
		if(!empty($data['tinnoibat']))
		{
			foreach ($data['tinnoibat'] as $key => $value) {
				if(file_exists('anhbaiviet/'.$value['PK_iMaTT'].'.jpg'))
				{
					$data['tinnoibat'][$key]['duongdan'] = 'dung';
				}
				else{
					$data['tinnoibat'][$key]['duongdan'] = 'sai';
				}
			}
		}
		$data['khauhieu']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_khauhieu');
		$data['dsloaitin'] = $this->Mloaitin->layLoaiTin('current');
		$data['dstin']     = $this->Mtintuc->layTinTucTT('current');
		if(!empty($data['dstin']))
		{
			foreach ($data['dstin'] as $key => $value) {
				if(file_exists('anhbaiviet/'.$value['PK_iMaTT'].'.jpg'))
				{
					$data['dstin'][$key]['duongdan'] = 'dung';
				}
				else{
					$data['dstin'][$key]['duongdan'] = 'sai';
				}
			}
		}
		$temp['data']      = $data;
		$temp['template']  = 'Vtrangchu';
		$this->load->view('layout/layout',$temp);
	}

}

/* End of file Ctrangchu.php */
/* Location: ./application/controllers/Ctrangchu.php */