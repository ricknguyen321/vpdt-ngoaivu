<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/26/2017
 * Time: 3:02 PM
 */
class Cdsvanbanphongdaxuly extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
    }
    public function index()
    {
//        pr($this->_session);
        $data['process'] = $this->Mvanbanden->getDocProcess();
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mvanbanchoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        $data['urlcv'] = $this->uri->segment(1);
//        pr($data['urlcv']);
        //danh sách đã xử lý
//        $data['getDocAwait'] = $this->Mvanbanchoxuly->getDocComplete($this->_session['FK_iMaPhongHD']);

        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                if(!empty($duongdan))
                {
                    $data['getDocAwait'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $data['getDocAwait'][$key]['sDuongDan'] = '';
                }
            }
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];


        // trình tự xử lý
        $process = $this->Mvanbanchoxuly->getDocProcess();
        $processcv = $this->Mvanbanchoxuly->getDocProcesscv();
        $processpb = $this->Mvanbanchoxuly->getDocProcessPB();
        $data['process'] = array_merge($process,$processpb,$processcv);
        $data['processcvph'] = $this->Mvanbanchoxuly->getDocProcessCVPPH();

        // duyệt văn bản chờ xử lý
//        $this->insertDocAwait();
        $data['title']    = 'Văn bản phòng đã giải quyết';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vdsvanbanphongdaxuly';
        $this->load->view('layout_admin/layout',$temp);
    }
//
//    /**
//     * @return duyệt văn bản
//     */
//    public function insertDocAwait()
//    {
//
//        if($this->input->post('duyetvanban')){
//            foreach ($this->input->post('duyet') as $key5 => $value5){
//                if(!empty($value5)){
//                    $Deputy = $this->input->post('phogiamdoc');
//                    $staff = $this->input->post('phongchutri');
//                    $DepartmentPH = $this->input->post('mangphoihop');
//                    $Doc_array= array();
//                    if (!empty($Deputy[$key5])) {
//                        $Doc_array['iTrangThai_TruyenNhan'] = '6';
//
//                        /** @var mang chuyen van ban cho pho phòng $PDir_array */
//                        $PDir_array= array();
//                        if(!empty($Deputy[$key5])){
//                            $PDir_array['PK_iMaCVCT'] = $Deputy[$key5];
//                            $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
//                            $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
//                            $PDir_array['sThoiGian'] = date('Y-m-d H:s',time());
//                            $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$key5];
//                            $PDir_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
//                            $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
//                            $PDir_array['CapGiaiQuyet'] = '6';
//                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
//                        }
//
//                    } else {
//                        $Doc_array['iTrangThai_TruyenNhan'] = '7';
//
//                        /** @var mang chuyen chuyen vien $Departmanet_array */
//                        // van ban phoi hop giua cac chuyen vien vowi nhau
//                        $Departmanet_array= array();
//                        $mangph = $this->input->post('mangphoihop')[$key5];
//                        $cvphoihop = explode( ',', $mangph );
////                        pr($cvphoihop);
//                        if(!empty($DepartmentPH[$key5])){
//                            foreach($cvphoihop as $key => $value){
//                                $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
//                                $Departmanet_array['PK_iMaCVPH'] = $value;
//                                $Departmanet_array['PK_iMaCVCT'] = $staff[$key5];
//                                $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
//                                $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
//                                $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$key5];
//                                $Departmanet_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
//                                $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
//                                $this->Mdanhmuc->themDuLieu('tbl_chuyenvienphoihop',$Departmanet_array);
//                            }
////                            pr($Departmanet_array);
//                        }
//                        // chuyen vien chu tri ko phoi hop
//                        $Departmanet_array1= array();
//                        if(!empty($staff)){
//                            $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
//                            $Departmanet_array1['PK_iMaCVCT'] = $staff[$key5];
//                            $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
//                            $Departmanet_array1['sThoiGian'] = date('Y-m-d H:s',time());
//                            $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$key5];
//                            $Departmanet_array1['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
//                            $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
//                            $Departmanet_array1['CapGiaiQuyet'] = '7';
//                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array1);
//                        }
//                    }
////                        pr($Doc_array);
//                    $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$this->input->post('doc_id')[$key5],'tbl_vanbanden',$Doc_array);
//
//                }
//            }
//            return redirect(base_url().'vanbanden/dsvanbanphongxuly');
//
//        }
//    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'dsvanbanphongdaxuly?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocComplete($this->_session['FK_iMaPhongHD'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 200;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_pgd');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocComplete($this->_session['FK_iMaPhongHD'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}