﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchuyendoi extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
    }
    public function index()
    {
        $timkiem = $this->input->post('timkiem');
        if($this->input->post('submit')){

        	// pr($this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$timkiem,'tbl_phongphoihop'));
            $dulieu = $this->Mdanhmuc->layDuLieu('iSoDen',$timkiem,'tbl_vanbanden');
            // pr($dulieu);
            // $mang = array(
            //     'PK_iMaVBDen' => $dulieu[0]['PK_iMaVBDen'],
            //     'PK_iMaCBChuyen' => 25,
            //     'sThoiGian' => '2017-11-20 00:00:00',
            //     'sMoTa' => 'Chuyển PP Nguyễn Thị Thanh Phương chủ trì.',
            //     'PK_iMaCVCT' => 533,
            //     'CapGiaiQuyet' => 7
            //     );
            // $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$mang);
            $data['timkiem'] = $dulieu;
        }
        if($this->input->post('submit1')){
            $primary_key = $this->input->post('primary_key');
            $id = $this->input->post('id');
            $primary_key1 = ($this->input->post('primary_key1')) ? $this->input->post('primary_key1') : NULL;
            $id1 = ($this->input->post('id1')) ? $this->input->post('id1') : NULL;
            $table = $this->input->post('table');
            $data1 = $this->input->post('data1');
            $data2 = $this->input->post('data2');
            $data_mang = array(
                $data1 => $data2
            );
//            pr($id1);
            $this->Mdanhmuc->capnhatDuLieu2($primary_key,$id,$primary_key1,$id1,$table,$data_mang);

        }
        $data['title']    = 'Danh sách tổng thể';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vchuyendoi';
        $this->load->view('layout_admin/layout',$temp);
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */
