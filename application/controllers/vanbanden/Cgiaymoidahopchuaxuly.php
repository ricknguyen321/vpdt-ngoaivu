<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cgiaymoidahopchuaxuly extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mgiaymoi','Mgiaymoi');
        $this->Mgiaymoi = new Mgiaymoi();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
        $this->load->model('Vanbanden/Mvanbanphoihop');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mgiaymoichoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        //danh sách chờ xử lý
        $data['urlcv'] = $this->uri->segment(1);
        $data['getDocAwait']    = $this->Mgiaymoi->layGiayMoiDaHopChuaHT($this->_session['PK_iMaCB']);
        $data['title']    = 'Giấy mời đã đi họp chưa hoàn thành';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoidahopchuaxuly';
        $this->load->view('layout_admin/layout',$temp);
    }

}

/* End of file Cgiaymoidahopchuaxuly.php */
/* Location: ./application/controllers/vanbanden/Cgiaymoidahopchuaxuly.php */