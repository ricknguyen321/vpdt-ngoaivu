<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 10/26/2017
 * Time: 8:35 AM
 */
class Cgiaymoiphoihopchuyenlai_tp extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
    }
    public function index()
    {
        $action = $this->input->post('action');
        switch ($action) {
            case 'getData':
                $this->getData();
                break;
            default:
                # code...
                break;
        }
//    pr($this->_session);
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mgiaymoichoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        // cán bộ đã chỉ đạo nhưng ko từ chối
        $data['getCBPH'] = $this->Mdanhmuc->layDuLieu('iTrangThai_TuChoi !=',1,'tbl_phongphoihop');
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwaitPPH']    = $page['info'];
//        pr($data['getDocAwaitPPH']);
        if(!empty($data['getDocAwaitPPH'])) {
            foreach ($data['getDocAwaitPPH'] as $key => $value) {
                $PPPHCT = $this->Mgiaymoichoxuly->getDocPPCT($value['PK_iMaVBDen'],$value['PK_iMaCB']);
                //pr($PPPHCT);
                if(!empty($PPPHCT[0])){
                    $data['getDocAwaitPPH'][$key]['PPPHCT'] = $PPPHCT[0]['smotanoidung'];
                }
                $process1 =  $this->Mgiaymoichoxuly->getDocProcessCVPPH($value['PK_iMaVBDen'],$this->_session['FK_iMaPhongHD']);
                if($this->_session['FK_iMaPhongHD'] == 11){
                    $idTP = $this->Mgiaymoichoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }elseif($this->_session['iQuyenHan_DHNB'] == 7 && $this->_session['FK_iMaPhongHD'] != 12){
                    $idTP = $this->Mgiaymoichoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }elseif($this->_session['FK_iMaPhongHD'] == 12){
                    $idTP = $this->Mgiaymoichoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }
                $arrayTT = array();
                if(!empty($process1)){
                    foreach($process1 as $key1 => $value1){
                        $arrayTT[$key1] = $value1['sHoTen'];
                    }
                }
                $data['getDocAwaitPPH'][$key]['TrinhTu'] = $arrayTT;
                if(!empty($idTP)){
                    $data['getDocAwaitPPH'][$key]['HoTenTP'] = $idTP;
                }
                //phó chủ trì từ chối
                $PhoCTTC = $this->Mdanhmuc->layDuLieu3where('PK_iMaPPPH',1,'PK_iMaVBDen',$value['PK_iMaVBDen'],'iTrangThai_TuChoi',1,'tbl_phongphoihop');
                if(!empty($PhoCTTC)){
                    $data['getDocAwaitPPH'][$key]['PK_iMaPPCT'] = $PhoCTTC[0]['PK_iMaCB'];
                }
                //phó chủ trì ko từ chối
                $PhoCT = $this->Mdanhmuc->layDuLieu2('PK_iMaPPPH',1,'PK_iMaVBDen',$value['PK_iMaVBDen'],'tbl_phongphoihop');
                if(!empty($PhoCT)){
                    $data['getDocAwaitPPH'][$key]['PK_iMaPCT'] = $PhoCT[0]['PK_iMaCB'];
                }

//                pr($mang);
            }
//            pr($data['getDocAwaitPPH']);
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['count'] = $page['count'];
//        pr($data['getDocAwaitPPH']);
        // duyệt văn bản chờ xử lý
        if($this->input->post('duyet')) {
            $this->insertDocAwait();
        }
        $data['title']    = 'Phòng phối hợp';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoiphoihopchuyenlai_tp';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $process = $this->Mgiaymoichoxuly->laylanhdaosaucung($ma);
        $staff = $this->input->post('phongchutri');
        $DepartmentPH = $this->input->post('mangphoihop');
        $Deputy = $this->input->post('phogiamdoc');
        $DeputyPH = $this->input->post('mangphoihoppp');
        $PPCT = $this->input->post('phoct');
        $Doc_array= array();
        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3) {
            if(!empty($PPCT[$ma])){
                $this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'PK_iMaPhong',$this->_session['FK_iMaPhongHD'],'tbl_phongphoihop');
            }else{
                $this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'PK_iMaCB',$this->input->post('canbotuchoi')[$ma],'tbl_phongphoihop');
                $this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'input_per',$this->input->post('canbotuchoi')[$ma],'tbl_phongphoihop');
            }
            if (!empty($Deputy[$ma]) && !empty($PPCT[$ma])) {
                $data_tp = array(
                    'PK_iMaCB' => $this->input->post('phogiamdoc')[$ma],
                    'PK_iMaPhong' => $this->_session['FK_iMaPhongHD'],
                    'PK_iMaVBDen' => $this->input->post('doc_id')[$ma],
                    'sThoiGian' => date('Y-m-d H:s', time()),
                    'sMoTa' => ($this->input->post('chidaophogiamdoc')[$ma]) ? $this->input->post('chidaophogiamdoc')[$ma] : NULL,
                    'sThoiGianHetHan' => (_post('hangiaiquyet') == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                    'input_per' => $this->_session['PK_iMaCB'],
                    'iTrangThai' => '2',
                    'PK_iMaPPPH' => '1'
                );
                $this->Mdanhmuc->themDuLieu('tbl_phongphoihop', $data_tp);
            }
            if(!empty($DeputyPH[$ma])) {
                $DeputyPH_array = array();
                $mangppph = $this->input->post('mangphoihoppp')[$ma];
                $ppphoihop = explode(',', $mangppph);
                foreach ($ppphoihop as $key1 => $value1) {
                    $DeputyPH_array['PK_iMaCB'] = $value1;
                    $DeputyPH_array['PK_iMaPPPH'] = '2';
                    $DeputyPH_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                    $DeputyPH_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                    $DeputyPH_array['sThoiGian'] = date('Y-m-d H:s', time());
                    $DeputyPH_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$ma];
                    $DeputyPH_array['sThoiGianHetHan'] = (_post('hangiaiquyet') == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                    $DeputyPH_array['input_per'] = $this->_session['PK_iMaCB'];
                    // phó phòng phối hợp là 2
                    $DeputyPH_array['iTrangThai'] = '2';
                    $this->Mdanhmuc->themDuLieu('tbl_phongphoihop', $DeputyPH_array);
                }
            }
        }
        /** @var mang chuyen chuyen vien $Departmanet_array */
        if (!empty($DepartmentPH[$ma])) {
            /** @var mang chuyen chuyen vien $Departmanet_array */
            $Departmanet_array= array();
            $mangph = $this->input->post('mangphoihop')[$ma];
            $cvphoihop = explode( ',', $mangph );
            foreach($cvphoihop as $key => $value){
                $Departmanet_array['PK_iMaCB'] = $value;
                $Departmanet_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
                $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet') == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                // chuyen viên xuống phó chủ trì phối hợp là 2 chuyển thẳng là 1
                $Departmanet_array['iTrangThai'] = 1;
//			   $Departmanet_array['iTrangThai'] = 1;
                $Departmanet_array['PK_iMaPPPH'] = '0';
                $this->Mdanhmuc->themDuLieu('tbl_phongphoihop',$Departmanet_array);
//                    pr($Departmanet_array);
            }
        }
        /// kế hoạch công tác
        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
            $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$ma,'tuan', date("W"),'canbo_id',$this->input->post('canbotuchoi')[$ma],'kehoach');
            if(!empty($Deputy[$ma])){
                $kehoach_data = array(
                    'vanban_id' => $this->input->post('doc_id')[$ma],
                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                    'vanban_skh' => $this->input->post('sohieu')[$ma],
                    'tuan' => date("W"),
                    'thuc_hien' => '2',
                    'loai_kh' => 1,
                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                    'canbo_id' => $Deputy[$ma],
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
                    'user_input' => $this->_session['PK_iMaCB'],
                    'lanhdao_so' => $process['PK_iMaCB'],
                    'phong_id' => $this->_session['FK_iMaPhongHD']
                );
//                        pr($kehoach_data);
                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data);

                //phó phối hợp
                if(!empty($DeputyPH[$ma])) {
                    $DeputyPH_array = array();
                    $mangppph = $this->input->post('mangphoihoppp')[$ma];
                    $ppphoihop = explode(',', $mangppph);
                    foreach ($ppphoihop as $key1 => $value1) {
                        $kehoach_data_ph = array(
                            'vanban_id' => $this->input->post('doc_id')[$ma],
                            'kh_noidung' => $this->input->post('noidungvb')[$ma],
                            'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                            'vanban_skh' => $this->input->post('sohieu')[$ma],
                            'tuan' => date("W"),
                            'thuc_hien' => '2',
                            'loai_kh' => 1,
                            'ngay_nhan' => date('Y-m-d H:i:s',time()),
                            'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                            'canbo_id' => $value1,
                            'lanhdao_id' => $this->_session['PK_iMaCB'],
                            'user_input' => $this->_session['PK_iMaCB'],
                            'lanhdao_so' => $process['PK_iMaCB'],
                            'phong_id' => $this->_session['FK_iMaPhongHD']
                        );
                        $this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_ph);
                    }
                }
            }
        }
        //chuyen chuyen vien phối hợp
        if(!empty($DepartmentPH[$ma])){
            $mangph = $this->input->post('mangphoihop')[$ma];
            $cvphoihop = explode( ',', $mangph );
            foreach($cvphoihop as $key => $value){
                $kehoach_data_cvph = array(
                    'vanban_id' => $this->input->post('doc_id')[$ma],
                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                    'vanban_skh' => $this->input->post('sohieu')[$ma],
                    'tuan' => date("W"),
                    'thuc_hien' => '2',
                    'loai_kh' => 1,
                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                    'canbo_id' => $value,
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
                    'user_input' => $this->_session['PK_iMaCB'],
                    'lanhdao_so' => $process['PK_iMaCB'],
                    'phong_id' => $this->_session['FK_iMaPhongHD']
                );

                $this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_cvph);
            }
        }
        return redirect(base_url().'giaymoiphoihopchuyenlai_tp');
    }
    public function PhanTrang()
    {
        if ($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
            $user=NULL;
            $iTrangThaiPH = NULL;
        }elseif ($this->_session['iQuyenHan_DHNB'] == 7){
            $user = $this->_session['PK_iMaCB'];
            $iTrangThaiPH = 2;
//            pr($user);
        }
        elseif ($this->_session['iQuyenHan_DHNB'] == 11){
            $user=$this->_session['PK_iMaCB'];
            $iTrangThaiPH = 1;
        }
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'giaymoiphoihopchuyenlai_tp?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mgiaymoichoxuly->PPPHTuChoi(NULL,$this->_session['FK_iMaPhongHD'],$user,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,NULL,NULL,$iTrangThaiPH));
//        pr($config['total_rows']);
        $config['per_page']             = 100;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'giaymoiphoihopchuyenlai_tp');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mgiaymoichoxuly->PPPHTuChoi(NULL,$this->_session['FK_iMaPhongHD'],$user,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page'],$iTrangThaiPH);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function getData(){
        //cán bộ phối hợp ko từ chối
        $idDoc =$this->input->post('idDoc');
        $getAccountDeputyDirector = $this->Mgiaymoichoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
        $data = $this->Mdanhmuc->layDuLieu2('iTrangThai_TuChoi !=',1,'PK_iMaVBDen',$idDoc,'tbl_phongphoihop');
        $array_pp = array();
        foreach($data as $k1 => $v1){
            $array_pp[] = $v1['PK_iMaCB'];
        }
//        pr($array_pp);
        $array_ppp = array();
        $i=0;
        foreach ($getAccountDeputyDirector as $k => $v){
            if(!in_array($v['PK_iMaCB'],$array_pp)){
                $array_ppp[$i]['PK_iMaCB'] = $v['PK_iMaCB'];
                $array_ppp[$i]['sHoTen'] = $v['sHoTen'];
                $i++;
            }
        }
        echo json_encode($array_ppp);
        exit();
    }

}