<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/3/2017
 * Time: 10:39 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandaphanloai extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {
        // danh sách giám đốc
        $data['getAccountDirec'] = $this->Mvanbanchoxuly->getAccount('0','4');
        // danh sách phó giám đốc
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0','5');
        // danh sách phòng
        $data['getDepartment'] = $this->Mvanbanchoxuly->getDepartment('0');
        // văn bản đã phân loại
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['Docawaitdir']    = $page['info'];
        if(!empty($data['Docawaitdir'])){
            foreach ($data['Docawaitdir'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $process = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen']);
                if(!empty($duongdan))
                {
                    $data['Docawaitdir'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }
                if(empty($duongdan)){
                    $data['Docawaitdir'][$key]['sDuongDan'] = '';
                }
                $arrayCBNhan = array();
                $arrayMoTa = array();
                $arrayMangPH = array();
                $arrayCT = array();
                foreach ($process as $key1 => $value1){
                    $ktlv = $this->Mvanbanchoxuly->TestCB($value1['PK_iMaCBNhan']);
                    $testlv = $ktlv['iQuyenHan_DHNB'];
                    if($value1['PK_iMaCBNhan'] != 0){
                        $arrayCBNhan[$testlv] = $value1['PK_iMaCBNhan'];
                        $arrayMoTa[$testlv] = $value1['sMoTa'];
                        $arrayCT[6] = $value1['PK_iMaPhongCT'];
                        $arrayMangPH[7] = $value1['PK_iMaPhongPH'];
                    }else{
                        $arrayMoTa[6] = $value1['sMoTa'];
                        $arrayMangPH[7] = $value1['PK_iMaPhongPH'];
                        $arrayCT[6] = $value1['PK_iMaPhongCT'];
                    }
                }
                $data['Docawaitdir'][$key]['PK_iMaCBNhan'] = $arrayCBNhan;
                $data['Docawaitdir'][$key]['PK_iMaPhongCT'] = $arrayCT;
                $data['Docawaitdir'][$key]['ChuThich'] = $arrayMoTa;
                $data['Docawaitdir'][$key]['PK_iMaPhongPH'] = $arrayMangPH;
//                pr($arrayCBNhan);
            }
//            pr($data['Docawaitdir']);
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
        // duyệt văn bản chờ xử lý
        $this->insertDocAwait();
        $data['title']    = 'Danh sách đã phân loại';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vvanbandaphanloai';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $taikhoan = $this->_session['PK_iMaCB'];
        if($this->input->post('duyetvanban')){
            $arrayDuyet = array();
//            pr($this->input->post('duyet'));
            $duyet = $this->input->post('duyet');
//            pr($duyet);
            foreach ($duyet as $key5 => $value5){
                if(!empty($value5)){
                    $giamdoc            =  _post('giamdoc')[$value5];
                    $phogiamdoc         =  _post('phogiamdoc')[$value5];
                    $phongchutri        =  _post('phongchutri')[$value5];
                    $noidunggiamdoc     =  _post('chidaogiamdoc')[$value5];
                    $noidungphogiamdoc  =  _post('chidaophogiamdoc')[$value5];
                    $noidungphongchutri =  _post('chidaophongchutri')[$value5];
                    if(!empty($giamdoc))
                    {
                        $manggiamdoc = array(
                            'FK_iMaVBDen'     => $value5,
                            'FK_iMaCB_Chuyen' => $taikhoan,
                            'FK_iMaCB_Nhan'   => $giamdoc,
                            'sNoiDung'        => $noidunggiamdoc,
                            'sThoiGian'       => date('Y-m-d H:i:s')
                        );
                        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
                    }
                    if(!empty($phogiamdoc))
                    {
                        $mangphogiamdoc = array(
                            'FK_iMaVBDen'     => $value5,
                            'FK_iMaCB_Chuyen' => $taikhoan,
                            'FK_iMaCB_Nhan'   => $phogiamdoc,
                            'sNoiDung'        => $noidungphogiamdoc,
                            'sThoiGian'       => date('Y-m-d H:i:s')
                        );
                        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangphogiamdoc);
                    }
                    if(!empty($phongchutri))
                    {
                        $phongbanHD  = $phongchutri;
                        if($phongbanHD==11) {
                            $quyenhan       = array(3);
                            $chucvuphong    = array(6);
                            $quyendb        = '';
                        }
                        else{
                            $quyenhan       = array(6);
                            $chucvuphong    = '';
                            $quyendb        = '';
                        }
                        $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
                        $canbonhan   = $truongphong[0]['PK_iMaCB'];
                        $mangphongchutri = array(
                            'FK_iMaVBDen'     => $value5,
                            'FK_iMaCB_Chuyen' => $taikhoan,
                            'FK_iMaCB_Nhan'   => $canbonhan,
                            'sNoiDung'        => $noidungphongchutri,
                            'sThoiGian'       => date('Y-m-d H:i:s')
                        );
                        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangphongchutri);
                    }
                    $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDen',$value5,'tbl_chuyennhanvanban');
                    $Director = $this->input->post('giamdoc');
                    $PDirector = $this->input->post('phogiamdoc');
                    $Deparment = $this->input->post('phongchutri');
                    $Doc_array= array();
                    if (!empty($Director[$key5])) {
                        $Doc_array['iTrangThai_TruyenNhan'] = '3';
                        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                    } elseif (!empty($PDirector[$key5])) {
                        $Doc_array['iTrangThai_TruyenNhan'] = '4';
                        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                    }  elseif (!empty($Deparment[$key5])) {
                        $Doc_array['iTrangThai_TruyenNhan'] = '5';
                        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                    }

                    /** @var mang chuyen van ban cho giam doc $PDir_array */
                    $Dir_array= array();
//                    pr($this->input->post('vanbanquantrong')[$key5]);
                    if(!empty($Director[$key5])){
                        $Dir_array['PK_iMaCBNhan'] = $Director[$key5];
                        $Dir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                        $Dir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                        $Dir_array['sThoiGian'] = date('Y-m-d H:s',time());
                        $Dir_array['sMoTa'] = $this->input->post('chidaogiamdoc')[$key5];
                        $Dir_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
                        $Dir_array['sVBQT'] = isset(_post('vanbanquantrong')[$key5])?_post('vanbanquantrong')[$key5]:0;
                        $Dir_array['input_per'] = $this->_session['PK_iMaCB'];
//                        pr($Dir_array);
                        $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Dir_array);
                    }

                    /** @var mang chuyen van ban cho pho giam doc $PDir_array */
                    $PDir_array= array();
                    if(!empty($PDirector[$key5])){
                        $PDir_array['PK_iMaCBNhan'] = $PDirector[$key5];
                        $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                        $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                        $PDir_array['sThoiGian'] = date('Y-m-d H:s',time());
                        $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$key5];
                        $PDir_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
                        $PDir_array['sVBQT'] = isset(_post('vanbanquantrong')[$key5])?_post('vanbanquantrong')[$key5]:0;
                        $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                        $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
                    }

                    /** @var mang chuyen phong chu tri va phoi hop $Departmanet_array */
                    $Departmanet_array= array();
                    if(!empty($Deparment[$key5])){
                        $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                        $Departmanet_array['PK_iMaPhongPH'] = $this->input->post('mangphoihop')[$key5];
                        $Departmanet_array['PK_iMaPhongCT'] = $Deparment[$key5];
                        $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                        $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
                        $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$key5];
                        $Departmanet_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
                        $Departmanet_array['sVBQT'] = isset(_post('vanbanquantrong')[$key5])?_post('vanbanquantrong')[$key5]:0;
                        $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
//                        pr($Departmanet_array);
                        $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array);
                    }

                }
            }
            return redirect(base_url().'vanbandaphanloai');

        }
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'vanbandaphanloai?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocDirAwait($loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 200;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_pgd');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocDirAwait($loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}