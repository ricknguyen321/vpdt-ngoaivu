<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csoanbaocao extends MY_Controller {

    protected $_thongtin;
    public function __construct() {
        parent:: __construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('thongtinnoibo/Msoanthaothongtin');
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
    }
    public function index()
    {
        $data['title']   = 'Soạn báo cáo kết quả cuộc họp';
        $ma              = _get('id');
        $idDoc = $this->uri->segment(2);
        if(!empty($ma))
        {
            $data['content'] = $this->capnhatDuLieu($ma);
        }
        else{
            $data['content'] = $this->themDuLieu($idDoc);
        }
        $mangpb =array();
        $data['dsphongban'] = $this->Msoanthaothongtin->layPhongBan(1);
        if(!empty($data['dsphongban']))
        {
            foreach ($data['dsphongban'] as $key => $value) {
                $mangpb[]=$value['PK_iMaPB'];
            }
        }
        $data['dsnguoinhanpb'] = $this->Msoanthaothongtin->layNguoiNhan(73);
//        pr($data['dsnguoinhanpb']);
        $data['thongtin'] = $this->_thongtin;
//        pr($data['thongtin']);
        $mavanban         = _get('id');
        $data['dsphong']  = $this->Msoanthaothongtin->layCBBC($mavanban);
        $data['dscanbo']  = $this->Msoanthaothongtin->layCBBC($mavanban);
//        pr($data['dscanbo']);
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vsoanbaocao';
        $this->load->view('layout_admin/layout', $temp);
    }
    public function themDuLieu($idDoc)
    {

        $laytenphong = $this->Mdanhmuc->layDuLieu('PK_iMaPB', $this->_session['FK_iMaPhongHD'], 'tbl_phongban');
        if ($this->input->post('luudulieu')) {
            $phongban = _post('phongban');
            $nguoinhan = _post('nguoinhan');
            // nếu tồn tại phòng ban thì kiểm tra có người nhận k, không có thì là cả phòng
            // nếu k tồn tại phòng ban thì mặc định là phòng ban giám đốc
            if (!empty($phongban)) {
//                pr($phongban);
                $data=array(
                    'sNoiDung'         => _post('noidung'),
                    'sNoiDungDeXuat'         => _post('noidungdexuat'),
                    'sNgayNhap'		=> date('Y-m-d H:i:s')
                );
                $id_insert = $this->Mdanhmuc->themDuLieu2('tbl_noidungbaocao',$data);
                foreach ($phongban as $key => $value) {
                    if (!empty($nguoinhan[$value])) {
                        $mangthem = array();
                        foreach ($nguoinhan[$value] as $k => $val) {
                            $mangthem[] = array(
                                'PK_iMaVBDen' => $idDoc,
                                'PK_iMaNoiDung' => $id_insert,
                                'PK_iMaCBNhap' => $this->_session['PK_iMaCB'],
                                'PK_iMaCB' => $val,
//                                'sTenPB'		=> $laytenphong[0]['sTenPB'],
                                'PK_iMaPB' => 73,
                                'PK_iMaPhongNhap' => $this->_session['FK_iMaPhongHD'],
                                'sNgayNhap' => date('Y-m-d H:i:s'),
                                'sFile' => 'doc_soanthao/' .clear($_FILES['files']['name']),
                                'iTrangThaiXem' => 0,
                                'iTrangThai' => 1
                            );
                        }
//                        pr($mangthem);
                        $this->Mdanhmuc->themNhieuDuLieu('tbl_soanbaocao', $mangthem);
                    } else {
                        if($value == 11)
                        {
                            $dscanbophong = $this->Msoanthaothongtin->layNguoiNhanTP($value,3);
                        }else{
                            $dscanbophong = $this->Msoanthaothongtin->layNguoiNhanTP($value,6);
                        }
//                        pr($dscanbophong);
                        if (!empty($dscanbophong)) {
                            $mangthem2 = array();
                            foreach ($dscanbophong as $key => $val) {
                                $mangthem2[] = array(
                                    'PK_iMaVBDen' => $idDoc,
                                    'PK_iMaNoiDung' => $id_insert,
                                    'PK_iMaCBNhap' => $this->_session['PK_iMaCB'],
                                    'PK_iMaCB' => $val['PK_iMaCB'],
//                                    'sTenPB'		=> $laytenphong[0]['sTenPB'],
                                    'PK_iMaPB' => $val['FK_iMaPhongHD'],
                                    'sNgayNhap' => date('Y-m-d H:i:s'),
                                    'sFile' => (!empty($_FILES['files']['name']))?'doc_soanthao/' .clear($_FILES['files']['name']) : NULL,
                                    'iTrangThaiXem' => 0,
                                    'PK_iMaPhongNhap' => $this->_session['FK_iMaPhongHD'],
                                    'iTrangThai' => 1
                                );
                            }
//                            pr($mangthem2);
                            $this->Mdanhmuc->themNhieuDuLieu('tbl_soanbaocao', $mangthem2);
                        }
                    }
                }
                $name = $_FILES['files']['name'];
                if (!empty($name)) {
                    $this->upload('doc_soanthao',clear($_FILES["files"]["name"]),'files');
                }
            redirect('dssoanthaodagui');
            }
        }
    }
    public function capnhatDuLieu($ma)
    {
        $this->_thongtin = $this->Mgiaymoichoxuly->getSoanThaoId(NULL,$ma);
        if(_post('luudulieu'))
        {
//            $this->Mdanhmuc->xoaDuLieu('PK_iMaSoanBaoCao',$ma,'tbl_soanbaocao');
            $data=array(
                'sNoiDung'         => _post('noidung'),
                'sNoiDungDeXuat'         => _post('noidungdexuat')
            );
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaNoiDung',$ma,'tbl_noidungbaocao',$data);
            $name = $_FILES['files']['name'];
            if (!empty($name)) {
                $this->upload('doc_soanthao',clear($_FILES["files"]["name"]),'files');
            }
            $phongban  = _post('phongban');
            $nguoinhan = _post('nguoinhan');
            // nếu tồn tại phòng ban thì kiểm tra có người nhận k, không có thì là cả phòng
            // nếu k tồn tại phòng ban thì mặc định là phòng ban giám đốc
            $this->Mdanhmuc->xoaDuLieu('PK_iMaNoiDung',$ma,'tbl_soanbaocao');
            foreach ($phongban as $key => $value) {
                if (!empty($nguoinhan[$value])) {
                    $mangthem = array();
                    foreach ($nguoinhan[$value] as $k => $val) {
                        $mangthem[] = array(
                            'PK_iMaVBDen' => _post('idvbden'),
                            'PK_iMaNoiDung' => $ma,
                            'PK_iMaCBNhap' => $this->_session['PK_iMaCB'],
                            'PK_iMaCB' => $val,
//                                'sTenPB'		=> $laytenphong[0]['sTenPB'],
                            'PK_iMaPB' => 73,
                            'sNgayNhap' => date('Y-m-d H:i:s'),
                            'sFile' => ($_FILES['files']['name']) ? 'doc_soanthao/' .clear($_FILES['files']['name']) : 'doc_soanthao/' .clear(_post('luufiles')),
                            'iTrangThaiXem' => 0,
                            'iTrangThai' => 1,
                            'PK_iMaPhongNhap' => $this->_session['FK_iMaPhongHD']
                        );
                    }
//                        pr($mangthem);
                    $this->Mdanhmuc->themNhieuDuLieu('tbl_soanbaocao', $mangthem);
                } else {
                    if($value == 11)
                    {
                        $dscanbophong = $this->Msoanthaothongtin->layNguoiNhanTP($value,3);
                    }else{
                        $dscanbophong = $this->Msoanthaothongtin->layNguoiNhanTP($value,6);
                    }
//                        pr($dscanbophong);
                    if (!empty($dscanbophong)) {
                        $mangthem2 = array();
                        foreach ($dscanbophong as $key => $val) {
                            $mangthem2[] = array(
                                'PK_iMaVBDen' => _post('idvbden'),
                                'PK_iMaNoiDung' => $ma,
                                'PK_iMaCBNhap' => $this->_session['PK_iMaCB'],
                                'PK_iMaCB' => $val['PK_iMaCB'],
//                                    'sTenPB'		=> $laytenphong[0]['sTenPB'],
                                'PK_iMaPB' => $val['FK_iMaPhongHD'],
                                'sNgayNhap' => date('Y-m-d H:i:s'),
                                'sFile' => ($_FILES['files']['name']) ? 'doc_soanthao/' .clear($_FILES['files']['name']) : 'doc_soanthao/' .clear(_post('luufiles')),
                                'iTrangThaiXem' => 0,
                                'iTrangThai' => 1,
                                'PK_iMaPhongNhap' => $this->_session['FK_iMaPhongHD']
                            );
                        }
//                            pr($mangthem2);
                        $this->Mdanhmuc->themNhieuDuLieu('tbl_soanbaocao', $mangthem2);

                    }
                }
            }
            redirect('dssoanthaodagui');
        }
    }
    public function upload($dir,$imgname,$filename)
    {
//        pr($dir);
        $config['upload_path']   = $dir;
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
        $config['overwrite']     = true;
        $config['file_name']     = $imgname;
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload($filename);
    }

}

/* End of file Csoanthaothongtin.php */
/* Location: ./application/controllers/thongtinnoibo/Csoanthaothongtin.php */