<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 11:09 AM
 */
class Ctocongtac_cv extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('Vanbanden/Mvanbanphoihop');
        $this->load->model('vanbandi/Mvanbandi');
        $this->load->library('pagination');
    }
    public function index()
    {
		if($this->input->post('themhan')){
            $data['content'] = $this->insertDateline();
        }
        if($this->input->post('tuchoitp')){
            $data['content'] = $this->Rejecttp();
        }
		
		$data['dsldduyet']        = $this->Mvanbanchoxuly->layldduyet($this->_session['FK_iMaPhongHD']);
		
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mvanbanchoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        //danh sách chờ xử lý
        $data['urlcv'] = $this->uri->segment(1);
//        pr($urlcv);
//        $data['getDocAwaitPPH'] = $this->Mvanbanchoxuly->getDocAwaitPPH(NULL,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);
//        pr($data['getDocAwaitPPH']);
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);

		/*
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $process = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processcv = $this->Mvanbanchoxuly->getDocProcesscv($value['PK_iMaVBDen']);
                $processpb = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $process1 = array_merge($process,$processpb,$processcv);
//                pr($process1);
                $arrayTT = array();
                if(!empty($process1)){
                    foreach($process1 as $key1 => $value1){
                        $arrayTT[$key1] = $value1['sHoTen'];
                    }
                }
                $data['getDocAwait'][$key]['TrinhTu'] = $arrayTT;
//                pr($arrayCBNhan);
            }
//            pr($data['getDocAwait']);
        }
		*/
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
        // trình tự xử lý
//        $process = $this->Mvanbanchoxuly->getDocProcess();
//        $processcv = $this->Mvanbanchoxuly->getDocProcesscv();
//        $processpb = $this->Mvanbanchoxuly->getDocProcessPB();
//        $data['process'] = array_merge($process,$processpb,$processcv);
//        $data['processcvph'] = $this->Mvanbanchoxuly->getDocProcessCVPPH();
//        pr($data['process']);
        $data['title']    = 'Văn bản thủ tục hành chính';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vtocongtac_cv';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'tocongtac_cv?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwaitCTC('7',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,NULL,NULL,NULL,NULL,1));
//        pr($config['total_rows']);
        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'tocongtac_cv');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwaitCTC('7',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page'],NULL,NULL,1);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
	
	
	
	public function insertDateline(){
        $idDoc = $this->input->post('mavbd');
        //$idLD = $this->input->post('ldgui');
		$datatp    = $this->Mvanbanphoihop->layTPVBden($idDoc); //ricknguyen321 
		$tp    = $datatp[0]['PK_iMaCBChuyen'];
		$ldduyet = $this->input->post('ldduyet');
		$noidung = $this->input->post('noidungtuchoi');
		$noidung = nl2br($noidung);
		$time = date('d/m H:i', time());
        $data_han = array(
            'iLanhDao' => (!empty($ldduyet))?$ldduyet:$tp,
            'iTrangThai_ThemHan'   => 1,
            'sHanChoDuyet'    => date_insert($this->input->post('handexuat')),
            'sNoiDungGiaHan'    => '<p><span>'.$noidung.'</span>'.' - <b>'.$this->_session['sHoTen'].'</b> - '.$time.'</p>'
        );
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
		
		$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 1,
				'FK_iMaCB_Gui'   => $this->_session['PK_iMaCB'],
				'sHanThongKe' => date_insert($this->input->post('handexuat')),
				'noidunggiahan'    => $noidung,
				'ngaygiahan'   => date('Y-m-d H:i:s',time())				
            );
		$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
		
        if ($DocGo > 0) {
			redirect('tocongtac_cv');
            return messagebox('Chuyền hạn văn bản về lãnh đạo thành công', 'info');
        } else {
            return messagebox('Chuyền hạn văn bản về lãnh đạo thất bại', 'danger');
        }
    }
     public function Rejecttp(){
        $idDoc        = $this->input->post('maphongbantp');
        $quyencc      = $this->input->post('capdo');
        $taikhoan     = $this->_session['PK_iMaCB'];
        $lanhdaophong = $this->Mvanbanphoihop->layPH_TP($idDoc,$taikhoan);
		$ldduyet2 = $this->input->post('ldduyet2');
		$noidungtc = nl2br(_post('noidungtuchoi'));
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $idDoc,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => (!empty($ldduyet2))?$ldduyet2:$lanhdaophong[0]['PK_iMaCBChuyen'],
            'sNoiDung'        => $noidungtc,
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
        $quyendb     = $this->_session['iQuyenDB'];
        $phongbanHD  = $this->_session['FK_iMaPhongHD'];
        if($phongbanHD==12)
        {
            $quyenhan       = array(11);
            $chucvuphong    = '';
            $quyendb        = $quyendb;
        }
        elseif ($phongbanHD==11) {
            $quyenhan       = array(3);
            $chucvuphong    = array(6);
            $quyendb        = '';
        }
        else{
            $quyenhan       = array(6);
            $chucvuphong    = '';
            $quyendb        = '';
        }
        $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
        $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden','PK_iMaCBDuyet',$truongphong[0]['PK_iMaCB']);
        // xóa trong bảng kế hoạch nếu chuyên viên, phó phòng từ chối
        $this->Mvanbanchoxuly->xoaDuLieu2Where('vanban_id',$idDoc,'canbo_id', $this->_session['PK_iMaCB'],'kehoach');
		$ndtc = nl2br($this->input->post('noidungtuchoi'));
        if($quyencc == 10){
            $this->Mdanhmuc->setDuLieu('doc_id',$idDoc,'tbl_chicuc','tcdn_active','3');
            $data_doc = array(
                'iTrangThai_TruyenNhan' => 6,
                'sLyDoTuChoiVanBanDen'    => $ndtc,
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
				redirect('tocongtac_cv');
                return messagebox('Chuyển văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyển văn bản về Lãnh đạo thất bại', 'danger');
            }
        }
        elseif($quyencc == 9){
            $this->Mdanhmuc->setDuLieu('doc_id',$idDoc,'tbl_chicuc','tcdn_active','2');
            $data_doc = array(
                'iTrangThai_TruyenNhan' => 6,
                'sLyDoTuChoiVanBanDen'    => $ndtc,
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
				redirect('tocongtac_cv');
                return messagebox('Chuyển văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyển văn bản về Lãnh đạo thất bại', 'danger');
            }
        }elseif($quyencc == 2){
            $data_doc = array(
                'iTrangThai_TruyenNhan' => 5,
                'sLyDoTuChoiVanBanDen'    => $ndtc,
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
				redirect('tocongtac_cv');
                return messagebox('Chuyển văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyển văn bản về Lãnh đạo thất bại', 'danger');
            }
        }
        else{
            $data_doc = array(
                'iTrangThai_TruyenNhan' => $this->input->post('capdo'),
                'sLyDoTuChoiVanBanDen'    => $ndtc,
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
				redirect('tocongtac_cv');
                return messagebox('Chuyển văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyển văn bản về Lãnh đạo thất bại', 'danger');
            }
        }
    }
	

}