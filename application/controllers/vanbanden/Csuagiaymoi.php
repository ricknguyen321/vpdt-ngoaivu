<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Csuagiaymoi extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mgiaymoi','Mgiaymoi');
        $this->Mgiaymoi = new Mgiaymoi();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
		$this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
    }
    public function index()
    {
        $data['nam'] = $this->input->get('nam');
        if(!empty($data['nam'])){
            if($this->input->get('nam') == 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default', TRUE);
            }
            if($this->input->get('nam') > 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default_'.$this->input->get('nam'), TRUE);
            }
        }
        $idAppointment = $this->uri->segment(2);
        // trình tự xử lý
        $process = $this->Mgiaymoichoxuly->getDocProcess($idAppointment);
        $processcv = $this->Mgiaymoichoxuly->getDocProcesscv($idAppointment);
        $processpb = $this->Mgiaymoichoxuly->getDocProcessPB($idAppointment);
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');
//        pr($process);
//        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($this->_session);
        $data['idDocGo'] = $idAppointment;
        $data['process'] = array_merge($process,$processpb,$processcv);
        // lấy kết quả
        $cvtc = $this->Mgiaymoichoxuly->getCVCT($idAppointment);
        //ket qua phong thu ly
        if(!empty($cvtc)){
            $data['resultPTL'] = $this->Mgiaymoichoxuly->getResultPTL($idAppointment,NULL,$cvtc[0]['PK_iMaCVCT']);
        }
        if(!empty($idAppointment)){
            $data['getdAppointment'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
            $data['VBDi'] = $this->Mdanhmuc->layDuLieu2('iSoDen',$data['getdAppointment'][0]['iSoDen'],'sNgayNhap >',date('Y').'-01'.'-01','tbl_vanbandi');

            foreach ($data['VBDi'] as $key2 => $value2){
                $file = $this->Mvanbanchoxuly->layFileLast($value2['PK_iMaVBDi']);
                if(!empty($file))
                {
                    $data['VBDi'][$key2]['sDuongDan'] = $file['sDuongDan'];
                }else{
                    $data['VBDi'][$key2]['sDuongDan'] = '';
                }
            }
//            pr($data['getdAppointment']);
        }
        (!empty($idAppointment) ? $this->updatedAppointment($idAppointment) : NULL);
        $data['title']    = 'Sửa giấy mời';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vsuagiaymoi';
        $this->load->view('layout_admin/layout',$temp);
    }
    /** Cập nhật giấy mời */
    public function updatedAppointment($idAppointment){
        if($this->input->post('luulai')){
			
			$giohop1        = _post('giohop');
				
				//ricknguyen321 xử lý định dạng giờ
				$giohop1 = str_replace('h', ':', $giohop1);
				$giohop1 = str_replace(' giờ ', ':', $giohop1);
				$giohop1 = str_replace('giờ', ':', $giohop1);
				$giohop1 = str_replace('phút', '', $giohop1);
				$giohop1 = str_replace(' : ', ':', $giohop1);
				$manggio = explode(':',$giohop1);
				if (count($manggio) == 2) {
					$gio = $manggio[0];
					$phut = $manggio[1];			
					if ($gio == '1') $gio = '01';
					if ($gio == '2') $gio = '02';
					if ($gio == '3') $gio = '03';
					if ($gio == '4') $gio = '04';
					if ($gio == '5') $gio = '05';
					if ($gio == '6') $gio = '06';
					if ($gio == '7') $gio = '07';
					if ($gio == '8') $gio = '08';
					if ($gio == '9') $gio = '09';
					if ($phut == '0') $phut = '00';
					if ($phut == '1') $phut = '01';
					if ($phut == '2') $phut = '02';
					if ($phut == '3') $phut = '03';
					if ($phut == '4') $phut = '04';
					if ($phut == '5') $phut = '05';
					if ($phut == '6') $phut = '06';
					if ($phut == '7') $phut = '07';
					if ($phut == '8') $phut = '08';
					if ($phut == '9') $phut = '09';			
					$giohop1 = $gio.':'.$phut;
				}
				
            $data_Appo = array(
            'sTenKV'                => _post('khuvuc'),
                'sKyHieu'           => _post('sokyhieu'),
                'sTenDV'            => _post('noiguiden'),
                'sTenLVB'           => _post('loaivanban'),
                'sNgayKy'           => date_insert(_post('ngayky')),
                'sMoTa'             => _post('trichyeu'),
                'sTenNguoiKy'       => _post('nguoiky'),
                'sGiayMoiGio'       => $giohop1,
                'sGiayMoiNgay'      => date_insert(_post('ngayhop')),
                'sNoiDung'          => _post('noidung'),
                'sGiayMoiDiaDiem'   => _post('diadiem'),
                'sGiayMoiChuTri'    => _post('nguoichutri'),
                'sChucVu'           => _post('chucvu'),
                //'sHanGiaiQuyet'     => ((_post('hangiaiquyet')) ? date_insert(_post('hangiaiquyet')) : '0000-00-00'),
                'iSoTrang'          => _post('sotrang'),
                'iSoDen'            => _post('soden'),
                'sNgayNhan'         => date_insert(_post('ngaynhan')),
                'PK_iMaCapNhat'   => $this->_session['PK_iMaCB'],
                'iGiayMoi'          => '1',
                'iVanBanQPPL'   => (!empty( _post('iVanBanQPPL'))) ?  _post('iVanBanQPPL') : '0',
                'iSTCChuTri'   => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
                'iVanBanTBKL'  => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
                'FK_iMaDM'            => _post('domat'),
                'FK_iMaDK	'            => _post('dokhan')
            );
//            pr($data_Appo);
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_Appo);
            return redirect(base_url().'dsgiaymoi');
        }
    }

}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */