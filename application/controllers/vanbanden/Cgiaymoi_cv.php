<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/15/2017
 * Time: 10:16 AM
 */
class Cgiaymoi_cv extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mgiaymoi','Mgiaymoi');
        $this->Mgiaymoi = new Mgiaymoi();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
        $this->load->model('Vanbanden/Mvanbanphoihop');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mgiaymoichoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        //danh sách chờ xử lý
        $data['urlcv'] = $this->uri->segment(1);
        if($this->input->post('tuchoitp')){
            $data['content'] = $this->Rejecttp();
        }
//        pr($urlcv);
//        $data['getDocAwaitPPH'] = $this->Mgiaymoichoxuly->getDocAwaitPPH(NULL,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);
//        pr($data['getDocAwaitPPH']);
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $process = $this->Mgiaymoichoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processcv = $this->Mgiaymoichoxuly->getDocProcesscv($value['PK_iMaVBDen']);
                $processpb = $this->Mgiaymoichoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $process1 = array_merge($process,$processpb,$processcv);
//                pr($process1);
                $arrayTT = array();
                if(!empty($process1)){
                    foreach($process1 as $key1 => $value1){
                        $arrayTT[$key1] = $value1['sHoTen'];
                    }
                }
                $data['getDocAwait'][$key]['TrinhTu'] = $arrayTT;
//                pr($arrayCBNhan);
            }
//            pr($data['getDocAwait']);
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
//        pr($data['process']);
        $data['title']    = 'Giấy mời chờ xử lý';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoi_cv';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'giaymoi_cv?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mgiaymoichoxuly->getDocAwait('7',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'giaymoi_cv');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mgiaymoichoxuly->getDocAwait('7',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function Rejecttp(){
        $idDoc        = $this->input->post('maphongbantp');
        $quyencc      = $this->input->post('capdo');
        $taikhoan     =  $this->_session['PK_iMaCB'];
        $lanhdaophong = $this->Mvanbanphoihop->layPH_TP($idDoc,$taikhoan);
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $idDoc,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => $lanhdaophong[0]['PK_iMaCBChuyen'],
            'sNoiDung'        => _post('noidungtuchoi'),
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
        $quyendb     = $this->_session['iQuyenDB'];
        $phongbanHD  = $this->_session['FK_iMaPhongHD'];
        if($phongbanHD==12)
        {
            $quyenhan       = array(11);
            $chucvuphong    = '';
            $quyendb        = $quyendb;
        }
        elseif ($phongbanHD==11) {
            $quyenhan       = array(3);
            $chucvuphong    = array(6);
            $quyendb        = '';
        }
        else{
            $quyenhan       = array(6);
            $chucvuphong    = '';
            $quyendb        = '';
        }
        $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
        $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden','PK_iMaCBDuyet',$truongphong[0]['PK_iMaCB']);
        // xóa trong bảng kế hoạch nếu chuyên viên, phó phòng từ chối
        $this->Mgiaymoichoxuly->xoaDuLieu2Where('vanban_id',$idDoc,'canbo_id', $this->_session['PK_iMaCB'],'kehoach');

        if($quyencc == 10){
            $this->Mdanhmuc->setDuLieu('doc_id',$idDoc,'tbl_chicuc','tcdn_active','3');
            $data_doc = array(
                'iTrangThai_TruyenNhan' => 6,
                'sLyDoTuChoiVanBanDen'    => $this->input->post('noidungtuchoi'),
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
                return messagebox('Chuyền văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyền văn bản về Lãnh đạo thất bại', 'danger');
            }
        }
        elseif($quyencc == 9){
            $this->Mdanhmuc->setDuLieu('doc_id',$idDoc,'tbl_chicuc','tcdn_active','2');
            $data_doc = array(
                'iTrangThai_TruyenNhan' => 6,
                'sLyDoTuChoiVanBanDen'    => $this->input->post('noidungtuchoi'),
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
                return messagebox('Chuyền văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyền văn bản về Lãnh đạo thất bại', 'danger');
            }
        }elseif($quyencc == 2){
            $data_doc = array(
                'iTrangThai_TruyenNhan' => 5,
                'sLyDoTuChoiVanBanDen'    => $this->input->post('noidungtuchoi'),
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
                return messagebox('Chuyền văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyền văn bản về Lãnh đạo thất bại', 'danger');
            }
        }
        else{
            $data_doc = array(
                'iTrangThai_TruyenNhan' => $this->input->post('capdo'),
                'sLyDoTuChoiVanBanDen'    => $this->input->post('noidungtuchoi'),
                'sCVTuChoi'    => $this->_session['sHoTen']
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
            if ($DocGo > 0) {
                return messagebox('Chuyền văn bản về Lãnh đạo thành công', 'info');
            } else {
                return messagebox('Chuyền văn bản về Lãnh đạo thất bại', 'danger');
            }
        }
    }

}