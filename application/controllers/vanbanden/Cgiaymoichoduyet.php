<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 10/16/2017
 * Time: 1:39 PM
 */
class Cgiaymoichoduyet extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly', 'Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mgiaymoichoxuly', 'Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mvanbanden', 'Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc', 'Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mtruyennhan');
        $this->load->model('Vanbanden/Mvanbanphoihop');
    }

    public function index()
    {
        $canbo                    = $this->Mtruyennhan->layDSCB();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        $taikhoan = $this->_session['PK_iMaCB'];
        if($this->input->post('tuchoi')){
            $mavanban       = $this->input->post('tuchoi');
            $nguoichuyen    = $this->Mvanbanphoihop->layCV_CT($mavanban);
            $nguoichuyenvb  = $nguoichuyen['PK_iMaCVCT'];
            $nguoiduyet     = $nguoichuyen['PK_iMaCBChuyen'];
            $luuvetduyet = array(
                'FK_iMaVB'        => $mavanban,
                'FK_iMaCB_Chuyen' => $taikhoan,
                'sThoiGian'       => date('Y-m-d H:i:s'),
                'sNoiDung'        => 'Từ chối kết quả',
                'FK_iMaCB_Nhan'   => $nguoichuyenvb
            );
            $this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);

            $data_vbden = array(
                'iTrangThai' => '0',
                'sNgayGiaiQuyet' => '0000-00-00',
                'PK_iMaCBDuyet'  => $nguoiduyet
            );
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$mavanban,'tbl_vanbanden',$data_vbden);
            return redirect(base_url().'giaymoichoduyet');
        }
//        pr($this->_session);
        //danh sách chờ xử lý
        $data['urlcv'] = $this->uri->segment(1);
//        pr($urlcv);
        $data['content'] = $this->updateDocAwait();
//        $data['getDocAwaitPPH'] = $this->Mvanbanchoxuly->getDocAwaitPPH(NULL,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);
//        pr($data['getDocAwaitPPH']);
        /** @var danh sách tìm kiếm phân trang $page */
        $page = $this->PhanTrang();
        $data['getDocAwait'] = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                // trình tự xử lý
                //$process = $this->Mgiaymoichoxuly->getDocProcess($value['PK_iMaVBDen']);
                //$processcv = $this->Mgiaymoichoxuly->getDocProcesscv($value['PK_iMaVBDen']);
                //$processpb = $this->Mgiaymoichoxuly->getDocProcessPB($value['PK_iMaVBDen'],$this->_session['FK_iMaPhongHD']);
                //$process1 = array_merge($process, $processpb, $processcv);
               // $file_ketqua = $this->Mdanhmuc->layDuLieu2('PK_iMaVBDen',$value['PK_iMaVBDen'],'PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_file_ketqua');
//                pr($file_ketqua);
               // $cvtc = $this->Mgiaymoichoxuly->getCVCT($value['PK_iMaVBDen']);
                //ket qua phong thu ly
                //if(!empty($cvtc)){
                  //  $resultPTL = $this->Mgiaymoichoxuly->getResultPTL($value['PK_iMaVBDen'],NULL,$cvtc[0]['PK_iMaPhongCT']);
                   // foreach($resultPTL as $value123){
                   //     $data['getDocAwait'][$key]['Ten_NHT'] =$value123['sHoTen'];
                   //     $data['getDocAwait'][$key]['MoTa_NHT'] =$value123['sMoTa'];
                  //  }
               // }
                //$arrayTT = array();
                //if(!empty($process1)){
                  //  foreach($process1 as $key1 => $value1){
               //         $arrayTT[$key1] = $value1['sHoTen'];
                //    }
                //}
               // $data['getDocAwait'][$key]['TrinhTu'] = $arrayTT;
                //$data['getDocAwait'][$key]['ketqua'] = $file_ketqua;
            }
        }
//        pr($data['getDocAwait']);
        $data['phantrang'] = $page['pagination'];
        $data['loaivanban'] = $page['loaivanban'];
        $data['sokyhieu'] = $page['sokyhieu'];
        $data['ngaynhap'] = $page['ngaynhap'];
        $data['donvi'] = $page['donvi'];
        $data['ngayky'] = $page['ngayky'];
        $data['ngayden'] = $page['ngayden'];
        $data['trichyeu'] = $page['trichyeu'];
        $data['nguoiky'] = $page['nguoiky'];
        $data['soden'] = $page['soden'];
        $data['denngay'] = $page['denngay'];
        $data['chucvu'] = $page['chucvu'];
        $data['nguoinhap'] = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
        $data['title'] = 'Giấy mời chờ duyệt';
        $temp['data'] = $data;
        $temp['template'] = 'vanbanden/Vgiaymoichoduyet';
        $this->load->view('layout_admin/layout', $temp);
    }

    public function updateDocAwait()
    {

        $taikhoan   = $this->_session['PK_iMaCB'];
        $chucvu     = $this->_session['iQuyenHan_DHNB'];// quyền trưởng phòng
        $phongbanHD = $this->_session['FK_iMaPhongHD'];
        if ($this->input->post('duyetvanban')) {
            foreach ($this->input->post('duyet') as $key5 => $value5) {
                if (!empty($value5)) {
                    if($phongbanHD==11)
                    {
                        $truongphong = $this->Mtruyennhan->layNguoiNhan(3,6,$phongbanHD,NULL);//TP
                        $nguoinhan   = $truongphong[0]['PK_iMaCB'];
                    }elseif($phongbanHD==12){
                        $nguoichuyen = $this->Mvanbanphoihop->laynguoiduyetduyet($taikhoan,$value5);
                        if(!empty($nguoichuyen))
                        {
                            $nguoinhan = $nguoichuyen['PK_iMaCBChuyen'];
                        }
                        else{
                            $truongphong = $this->Mtruyennhan->layNguoiNhan(6,NULL,$phongbanHD,NULL);//TP
                            $nguoinhan   = $truongphong[0]['PK_iMaCB'];
                        }
                    }
                    else{
                        $truongphong = $this->Mtruyennhan->layNguoiNhan(6,NULL,$phongbanHD,NULL);//TP
                        $nguoinhan   = $truongphong[0]['PK_iMaCB'];
                    }
                    if($chucvu==6 || $chucvu==3)
					{
							$luuvetduyet = array(
								'FK_iMaVB'        => $value5,
								'FK_iMaCB_Chuyen' => $taikhoan,
								'sThoiGian'       => date('Y-m-d H:i:s'),
								'sNoiDung'        => 'Duyệt',
								'FK_iMaCB_Nhan'   => 0
								);
							$this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);
							$Doc_array = array(
								'PK_iMaCBDuyet' => $nguoinhan,
								'iTrangThai' => 1
								);
							$DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen', $value5, 'tbl_vanbanden', $Doc_array);
                    }
                    else{
                        $luuvetduyet = array(
                            'FK_iMaVB'        => $value5,
                            'FK_iMaCB_Chuyen' => $taikhoan,
                            'sThoiGian'       => date('Y-m-d H:i:s'),
                            'sNoiDung'        => 'Duyệt',
                            'FK_iMaCB_Nhan'   => $nguoinhan
                        );
                        $this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);
                        $Doc_array = array(
                            'PK_iMaCBDuyet' => $nguoinhan
                        );
						$DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen', $value5, 'tbl_vanbanden', $Doc_array);
                    }
                    $data_kh2 = array(
                        'active' => 5,
                        'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                        'ket_qua_hoanthanh' => "Hoàn thành"
                    );
                    $data_kh1 = array(
                        'active' => 5
                    );
                    if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
                        //$this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$this->input->post('doc_id')[$key5],'tuan >=', date("W"),'thuc_hien !=',2,$data_kh1);
                    }else{
                       // $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$this->input->post('doc_id')[$key5],'tuan  >=', date("W"),'canbo_id ',$this->_session['PK_iMaCB'],$data_kh2);
                        //$this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$this->input->post('doc_id')[$key5],'tuan  >=', date("W"),'lanhdao_id ',$this->_session['PK_iMaCB'],$data_kh1);

                       // $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$this->input->post('doc_id')[$key5],'tuan >=', date("W"),'active < ',5,$data_kh2);
                    }
					$data_vbden = array(
                        'active' => 1
                    );
                    $this->Mdanhmuc->capnhatDuLieu2('PK_iMaVBDen',$this->input->post('doc_id')[$key5],'PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_file_ketqua',$data_vbden);
//                    pr($Doc_array);
                    
            
                }
            }
            return redirect(base_url() . 'giaymoichoduyet');

        }
    }

    public function PhanTrang()
    {
        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 10 || $this->_session['iQuyenHan_DHNB'] == 11 || $this->_session['iQuyenHan_DHNB'] == 3){
            $stt = '2';
            $cvctvb=NULL;
            $pheduyetCV=NULL;
            $Truongphong = $this->_session['PK_iMaCB'];
        } elseif($this->_session['iQuyenHan_DHNB'] == 7){
            $cvctvb = NULL;
            $stt='2';
            $pheduyetCV=NULL;
            $Truongphong = $this->_session['PK_iMaCB'];
//            pr($cvctvb);
        }elseif($this->_session['iQuyenHan_DHNB'] == 8 || $this->_session['iQuyenHan_DHNB'] == 12){
            $cvctvb = $this->_session['PK_iMaCB'];
            $stt=NULL;
            $pheduyetCV='1';
            $Truongphong = NULL;
//            pr($cvctvb);
        }
        $loaivanban = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap = $this->input->get('ngaynhap');
        $donvi = $this->input->get('donvi');
        $ngayky = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url'] = base_url() . 'giaymoichoduyet?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows'] = count($this->Mgiaymoichoxuly->getDocPheDuyet(NULL, NULL, NULL, $cvctvb, $stt,$pheduyetCV,$Truongphong, $loaivanban, $sokyhieu, $ngaynhap, $donvi, $ngayky, $ngayden, $trichyeu, $nguoiky, $soden, $denngay, $chucvu, $nguoinhap));
//        pr($config['total_rows']);
        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links'] = 9;
        $config['use_page_numbers'] = false;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo';
        $config['last_link'] = '&raquo';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&lt';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data['page'] = $this->input->get('page') ? $this->input->get('page') : 0;

        if ((is_numeric($data['page']) == FALSE) || $data['page'] % $config['per_page'] != 0 || ($data['page']) > $config['total_rows']) {
            redirect(base_url() . 'giaymoichoduyet');
        }
        $data['loaivanban'] = $loaivanban;
        $data['sokyhieu'] = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] = $denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['count'] = $config['total_rows'];
        $data['info'] = $this->Mgiaymoichoxuly->getDocPheDuyet(NULL, NULL,NULL, $cvctvb, $stt,$pheduyetCV,$Truongphong, $loaivanban, $sokyhieu, $ngaynhap, $donvi, $ngayky, $ngayden, $trichyeu, $nguoiky, $soden, $denngay, $chucvu, $nguoinhap, $config['per_page'], $data['page']);
        $data['pagination'] = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}