<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsvanbanden extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        if($this->input->get('nam') == 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default', TRUE);
        }
        if($this->input->get('nam') > 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default_'.$this->input->get('nam'), TRUE);
        }
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->library('pagination');
	}
	public function index()
	{
		$data['page']    = _get('page');
        // văn bản bị văn phòng trả lại
        if($this->input->post('tralai')){
           $PK_iMaVBDen = $this->input->post('PK_iMaVBDen');
           $sNgayGiaiQuyet = $this->input->post('sNgayGiaiQuyet');
           $lydotralai =  $this->input->post('lydotralai');
           //pr($sNgayGiaiQuyet);

          /* $sql = "update tbl_vanbanden set lydotralai ='".$lydotralai."', ngayhoanthanhcu= '".$sNgayGiaiQuyet."' where PK_iMaVBDen=".$PK_iMaVBDen;
           $this->Mvanbanden->return_vb($sql);*/
           redirect("dsvanbanden/");
        }
//        pr($mangVBDi);        
        if($this->input->post('xoavanban')){
			$mvb = $this->input->post('xoavanban');
			$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$mvb,'tbl_files_vbden');
			if(!empty($files))
			{
				foreach ($files as $key => $value) {
					if(file_exists($value['sDuongDan']))
					{
						unlink($value['sDuongDan']);
					}
				}
				$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDen',$mvb,'tbl_files_vbden');
				
			}
			
			$files2 = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$mvb,'tbl_file_ketqua');
			if(!empty($files2))
			{
				foreach ($files2 as $key => $value) {
					if(file_exists($value['sDuongDan']))
					{
						unlink($value['sDuongDan']);
					}
				}
				$this->Mdanhmuc->xoaDuLieu('PK_iMaVBDen',$mvb,'tbl_file_ketqua');
				
			}
		
			//trang thái = 3 - xóa văn bản
            //$this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$this->input->post('xoavanban'),'tbl_vanbanden','iTrangThai','3');			
			$this->Mdanhmuc->xoaDuLieu('PK_iMaVBDen',$mvb,'tbl_chuyennhanvanban');
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDen',$mvb,'tbl_luuvet_chuyennhan');
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDen',$mvb,'tbl_luuvet_chuyennhan');
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVB',$mvb,'tbl_luuvet_duyet');
			$this->Mdanhmuc->xoaDuLieu('PK_iMaVBDen',$mvb,'tbl_phongphoihop');
			$this->Mdanhmuc->xoaDuLieu('PK_iMaVBDen',$mvb,'tbl_vanbanden');
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDen',$mvb,'tbl_chidao');
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDen',$mvb,'tbl_ykien');
        }
        /** @var danh sách tìm kiếm phân trang $page */
        $page                  = $this->PhanTrang();
        $data['getDocGo']      = $page['info'];
        if(!empty($data['getDocGo'])){
            foreach ($data['getDocGo'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $processgd = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processpb1 = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                //$vbdi = $this->Mdanhmuc->layDuLieu2('iSoDen',$value['iSoDen'],'sNgayNhap >',date('Y').'-01'.'-01','tbl_vanbandi');
                $soden_tam = $value['iSoDen'];
                $vbdi = $this->Mdanhmuc->getvanbandi("(iSoDen = ".$soden_tam." OR iSoDen LIKE '%,".$soden_tam.",%' OR iSoDen LIKE '%,".$soden_tam." ,%'  OR iSoDen LIKE '%, ".$soden_tam.",%'  OR iSoDen LIKE '%, ".$soden_tam." ,%' OR  iSoDen LIKE '%, ".$soden_tam."' OR  iSoDen LIKE '%,".$soden_tam."') and sNgayNhap >'".date('Y')."-01-01'",'tbl_vanbandi');
                //echo "(iSoDen = ".$soden_tam." OR iSoDen LIKE '%,".$soden_tam.",%' OR iSoDen LIKE '%,".$soden_tam." ,%'  OR iSoDen LIKE '%, ".$soden_tam.",%'  OR iSoDen LIKE '%, ".$soden_tam." ,%' OR  iSoDen LIKE '%, ".$soden_tam."' OR  iSoDen LIKE '%,".$soden_tam."') and sNgayNhap >'".date('Y')."-01-01'";

                    $data['getDocGo'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                    if(!empty($processgd)){
                        $data['getDocGo'][$key]['sChuTri'] = $processgd[0]['sMoTa'];
                    }
                    if(empty($processgd)){
                        $data['getDocGo'][$key]['sChuTri'] = '';
                    }
                    if(!empty($processpb1)){
                        $data['getDocGo'][$key]['sPhongChuTri'] = $processpb1[0]['sHoTen'];
                    }
                    if(empty($processpb1)){
                        $data['getDocGo'][$key]['sPhongChuTri'] = '';
                    }
                    if(!empty($vbdi)){
                        $data['getDocGo'][$key]['VBDi'] = $vbdi[0]['PK_iMaVBDi'];
                    }
//                pr($vbdi);
            }
        }
//         pr($data['getDocGo']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']    = $page['loaivanban'];
        $data['sokyhieu']      = $page['sokyhieu'];
        $data['ngaynhap']      = $page['ngaynhap'];
//        pr($data['ngaynhap']);
        $data['donvi']         = $page['donvi'];
        $data['ngayky']        = $page['ngayky'];
        $data['ngayden']       = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['ngaymoi']       = $page['ngaymoi'];
        $data['nguoiky']       = $page['nguoiky'];
        $data['soden']         = $page['soden'];
        $data['denngay']       = $page['denngay'];
        $data['chucvu']        = $page['chucvu'];
        $data['nguoinhap']     = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
		$data['tonghop'] 	   = $page['tonghop'];
		$data['ideadline'] 	   = $page['ideadline'];
		$data['domat'] 	   		= $page['domat'];
		$data['ihoanthanh'] 	= $page['ihoanthanh'];
		$data['id'] 	= $page['id'];
        $data['count']         = $page['count'];
        $data['nam'] = $page['nam'];


		$data['title']    = 'Danh sách tổng thể văn bản đến';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vdsvanbanden';
		$this->load->view('layout_admin/layout',$temp);
	}
    public function PhanTrang()
    {
//        pr($this->_session);
//        pr($this->input->get('id'));
        if($this->_session['iQuyenHan_DHNB'] == 2 || $this->_session['iQuyenHan_DHNB'] == 9 || $this->_session['iQuyenHan_DHNB'] == 3 || $this->_session['iQuyenHan_DHNB'] == 4 || $this->_session['iQuyenHan_DHNB'] == 5){
            $quyenhan = NULL;
        }else{
            $quyenhan = 2;
        }
		$domat  	   = $this->input->get('domat');
		$ihoanthanh  	   = $this->input->get('ihoanthanh');
		$ideadline  	= $this->input->get('ideadline');
		$tonghop  	   = $this->input->get('tonghop');
        $sPhongChuTri  = $this->input->get('sPhongChuTri');		
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu      = $this->input->get('sokyhieu');
        $ngaynhap      = ($this->input->get('ngaynhap') > '01/01/2017') ? date_insert($this->input->get('ngaynhap')) : NULL;
        $donvi         = $this->input->get('donvi');
        $ngayky        = ($this->input->get('ngayky') > '01/01/2017') ? date_insert($this->input->get('ngayky')) : NULL;
        $ngayden       = ($this->input->get('ngayden') > '01/01/2017') ? date_insert($this->input->get('ngayden')) : NULL;
//        pr($ngayden);
        $trichyeu      = $this->input->get('trichyeu');
        $ngaymoi       = ($this->input->get('ngaymoi') > '01/01/2017') ? date_insert($this->input->get('ngaymoi')) : NULL;
        $nguoiky       = $this->input->get('nguoiky');
        $soden         = $this->input->get('soden');
        $denngay       = ($this->input->get('denngay') > '01/01/2017') ? date_insert($this->input->get('denngay')) : NULL;
        $chucvu        = $this->input->get('chucvu');
        $nguoinhap     = $this->input->get('nguoinhap');
        $cacloaivanban = $this->input->get('cacloaivanban');
        $nam = $this->input->get('nam');
        $id = $this->input->get('id');
        $idDocument    = $this->uri->segment(2);
        $config['base_url']             = base_url().'dsvanbanden?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&ngaymoi='.$ngaymoi.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&cacloaivanban='.$cacloaivanban.'&id='.$id.'&nam='.$nam.'&sPhongChuTri='.$sPhongChuTri.'&tonghop='.$tonghop.'&ideadline='.$ideadline.'&domat='.$domat.'&ihoanthanh='.$ihoanthanh;
        $config['total_rows']           = $this->Mvanbanden->countDocGo($id,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,$quyenhan,$sPhongChuTri,$tonghop,$ideadline,$domat,$ihoanthanh);
//        pr($config['total_rows']);
        $config['per_page']             = 30;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'dsvanbanden');
        }
		$data['domat'] 	   = $domat;
		$data['ihoanthanh'] 	   = $ihoanthanh;
		$data['ideadline'] 	   = $ideadline;
		$data['tonghop'] 	   = $tonghop;
        $data['sPhongChuTri']  = $sPhongChuTri;
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap']      = $ngaynhap;
        $data['donvi']         = $donvi;
        $data['ngayky']        = $ngayky;
        $data['ngayden']       = $ngayden;
        $data['trichyeu']      = $trichyeu;
        $data['ngaymoi']       = $ngaymoi;
        $data['nguoiky']       = $nguoiky;
        $data['soden']         = $soden;
        $data['denngay']       = $denngay;
        $data['chucvu']        = $chucvu;
        $data['nguoinhap']     = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['nam'] = $nam;
        $data['id'] = $id;
        $data['count']         = $config['total_rows'];
        $data['info']        = $this->Mvanbanden->getDocGo($id,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,$config['per_page'], $data['page'],$quyenhan,$sPhongChuTri,$tonghop,$ideadline,$domat,$ihoanthanh);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */