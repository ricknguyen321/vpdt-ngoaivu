<?php

/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/19/2017
 * Time: 8:23 AM
 */
class Cvanbanchopheduyet extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly', 'Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden', 'Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc', 'Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('vanbandi/Mtruyennhan');
        $this->load->model('Vanbanden/Mvanbanphoihop');
        $this->load->library('pagination');
    }

    public function index()
    {
        $canbo                    = $this->Mtruyennhan->layDSCB();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        $taikhoan = $this->_session['PK_iMaCB'];
        if($this->input->post('tuchoi')){
            $mavanban       = $this->input->post('iddoc');
            $nguoichuyen    = $this->Mvanbanphoihop->layCV_CT($mavanban);
            $nguoichuyenvb  = $nguoichuyen['PK_iMaCVCT'];
            $nguoiduyet     = $nguoichuyen['PK_iMaCBChuyen'];
            $luuvetduyet = array(
                'FK_iMaVB'        => $mavanban,
                'FK_iMaCB_Chuyen' => $taikhoan,
                'sThoiGian'       => date('Y-m-d H:i:s'),
                'sNoiDung'        => _post('noidungtuchoi'),
                'FK_iMaCB_Nhan'   => $nguoichuyenvb
            );
            $this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);
            $data_vbden = array(
                'iTrangThai'        => 0,
                'sNgayGiaiQuyet'    => '0000-00-00',
                'PK_iMaCBHoanThanh' => 0,
                'sLuuVaoKho'        => 0,
                'sLyDoTuChoiKetQua' => $this->input->post('noidungtuchoi'),
                'PK_iMaCBDuyet'     => $nguoiduyet
            );
            $this->Mvanbanchoxuly->xoaDuLieu2Where('PK_iMaCB',$this->input->post('canbo'),'PK_iMaVBDen',$this->input->post('tuchoi'),'tbl_file_ketqua');
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$mavanban,'tbl_vanbanden',$data_vbden);
            return redirect(base_url().'vanbanchopheduyet');
        }
//        pr($this->_session);
        //danh sách chờ xử lý
        $data['urlcv'] = $this->uri->segment(1);
//        pr($urlcv);
        $data['content'] = $this->updateDocAwait();
//        $data['getDocAwaitPPH'] = $this->Mvanbanchoxuly->getDocAwaitPPH(NULL,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);
//        pr($data['getDocAwaitPPH']);
        /** @var danh sách tìm kiếm phân trang $page */
        $page = $this->PhanTrang();
        $data['getDocAwait'] = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                // trình tự xử lý
                //$process = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                //$processcv = $this->Mvanbanchoxuly->getDocProcesscv($value['PK_iMaVBDen']);
                //$processpb = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen'],$this->_session['FK_iMaPhongHD']);
                //$process1 = array_merge($process, $processpb, $processcv);
//                pr($process1);
                //$cvtc = $this->Mvanbanchoxuly->getCVCT($value['PK_iMaVBDen']);
                $cvtc ='';
                $cvtc = $value['PK_iMaCBHoanThanh'];
                //ket qua phong thu ly
                if(!empty($cvtc)){
                    //$resultPTL = $this->Mvanbanchoxuly->getResultPTL($value['PK_iMaVBDen'],NULL,$cvtc[0]['PK_iMaPhongCT']);

                    $mota = $this->Mvanbanchoxuly->getDataFomWhere1('PK_iMaCB = '.$cvtc.' and PK_iMaVBDen = '.$value['PK_iMaVBDen'] ,'tbl_file_ketqua','PK_iMaKetQua');
                    $resultPTL = $this->Mvanbanchoxuly->getDataFomWhere('PK_iMaCB ='.$cvtc.' and iTrangThai = 0 ','tbl_canbo');
                    foreach($resultPTL as $value123){
                        $data['getDocAwait'][$key]['Ten_NHT'] =$value123['sHoTen'];
                        $data['getDocAwait'][$key]['MoTa_NHT'] =$mota[0]['sMoTa'];
                        $data['getDocAwait'][$key]['file_ketqua'] =$mota['sDuongDanFile'];
                    }
                }
                //$arrayTT = array();
                //if(!empty($process1)){
                  //  foreach($process1 as $key1 => $value1){
                   //     $arrayTT[$key1] = $value1['sHoTen'];
                 //   }
              //  }
                //$data['getDocAwait'][$key]['TrinhTu'] = $arrayTT;
            }
        }
//        pr($data['getDocAwait']);
        $data['phantrang'] = $page['pagination'];
        $data['loaivanban'] = $page['loaivanban'];
        $data['sokyhieu'] = $page['sokyhieu'];
        $data['ngaynhap'] = $page['ngaynhap'];
        $data['donvi'] = $page['donvi'];
        $data['ngayky'] = $page['ngayky'];
        $data['ngayden'] = $page['ngayden'];
        $data['trichyeu'] = $page['trichyeu'];
        $data['nguoiky'] = $page['nguoiky'];
        $data['soden'] = $page['soden'];
        $data['denngay'] = $page['denngay'];
        $data['chucvu'] = $page['chucvu'];
        $data['nguoinhap'] = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
        $data['title'] = 'Văn bản chờ phê duyệt';
        $temp['data'] = $data;
        $temp['template'] = 'vanbanden/Vvanbanchopheduyet';
        $this->load->view('layout_admin/layout', $temp);
    }
    public function updateDocAwait()
    {
        $taikhoan   = $this->_session['PK_iMaCB'];
        $chucvu     = $this->_session['iQuyenHan_DHNB'];// quyền trưởng phòng
        $phongbanHD = $this->_session['FK_iMaPhongHD'];    

        if ($this->input->post('duyetvanban')) {
            foreach ($this->input->post('duyet') as $key5 => $value5) {
                if (!empty($value5)) {
                    
                    if($phongbanHD==11)
                    {
                        $truongphong = $this->Mtruyennhan->layNguoiNhan(3,6,$phongbanHD,NULL);//TP
                        $nguoinhan   = $truongphong[0]['PK_iMaCB'];
                    }elseif($phongbanHD==12){
                        $nguoichuyen = $this->Mvanbanphoihop->laynguoiduyetduyet($taikhoan,$value5);
                        if(!empty($nguoichuyen))
                        {
                            $nguoinhan = $nguoichuyen['PK_iMaCBChuyen'];
                        }
                        else{
                            $truongphong = $this->Mtruyennhan->layNguoiNhan(6,NULL,$phongbanHD,NULL);//TP
                            $nguoinhan   = $truongphong[0]['PK_iMaCB'];
                        }
                    }
                    else{
                        $truongphong = $this->Mtruyennhan->layNguoiNhan(6,NULL,$phongbanHD,NULL);//TP
                        $nguoinhan   = $truongphong[0]['PK_iMaCB'];
                    }
                    // pr($nguoinhan);
                    if($chucvu==6 || $chucvu==3)
                    {
                        $luuvetduyet = array(
                            'FK_iMaVB'        => $value5,
                            'FK_iMaCB_Chuyen' => $taikhoan,
                            'sThoiGian'       => date('Y-m-d H:i:s'),
                            'sNoiDung'        => 'Duyệt',
                            'FK_iMaCB_Nhan'   => 0
                        );
                        $this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);
                        $Doc_array = array(
                            'iTrangThai' => '1',
                            'iCoSangTao' => (!empty($this->input->post('cosangtao')[$key5]) ? $this->input->post('cosangtao')[$key5] : 0),
                            /*'sLuuVaoKho' => (!empty($this->input->post('luukho')[$key5]) ? $this->input->post('luukho')[$key5] : 0),*/
                            'sNgayGiaiQuyet' => date('Y-m-d',time())
                        );

                        // xóa bản ghi trong bảng kế hoạch công tác nếu là văn bản phối hợp
                        $vbdaura_cv = $this->input->post('vbdaura_cv')[$key5];
                        if($vbdaura_cv != 1){
                            $this->Mvanbanchoxuly->xoaDuLieu2Where('vanban_id',$key5,'active',0,'kehoach');
                        }
                        //$Kehoach_array = array('active' => 5,'sangtao'=>(!empty($this->input->post('cosangtao')[$key5]) ? 2 : 1));
                        //$this->Mvanbanchoxuly->capnhatkehoach('vanban_id', $this->input->post('doc_id')[$key5],'thuc_hien',3,'tuan  >=', date("W"), $Kehoach_array); 
                        //$Kehoach_array_1 = array('active' => 5);
                        //$this->Mvanbanchoxuly->capnhatkehoach('vanban_id', $this->input->post('doc_id')[$key5],'thuc_hien',1,'tuan  >=', date("W"), $Kehoach_array_1);

                        //$this->Mvanbanchoxuly->capnhatkehoach('vanban_id', $this->input->post('doc_id')[$key5],'thuc_hien',2,'tuan  >=', date("W"), $Kehoach_array_1);
                        
                        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen', $value5, 'tbl_vanbanden', $Doc_array);
						
						//Thông tin đến giám đốc VB có thời hạn đã được giải quyết.
						$vanban = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden');
						if ($vanban[0]['iDeadline'] == 1) {
							$datatt=array(
								'sMoTa'         => '<i>[Thông tin này được gửi tự động đến GĐ khi văn bản có thời hạn được hoàn thành]</i><br><p>- Văn bản đến số: <b style="color:red; font-size:16px">'.$vanban[0]['iSoDen'].'</b></p><p>- Trích yếu: <a href="quatrinhxuly/'.$vanban[0]['PK_iMaVBDen'].'"><b>'.$vanban[0]['sMoTa'].'</b></a></p><p>- GĐ giao có thời hạn: <b>'.date_format(date_create($vanban[0]['sHanThongKe']),"d/m/y").'</b></p><b style="color:blue">* Văn bản này đã được lãnh đạo phòng duyệt hoàn thành.</b>',
								'FK_iMaCB_Nhap' => $this->_session['PK_iMaCB'],
								'FK_iMaPB'      => $this->_session['FK_iMaPhongHD'],
								'sNgayNhap'		=> date('Y-m-d H:i:s')
							);
							$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_vanban',$datatt);
							
							/*$mangthem2[] = array(
								'FK_iMaNguoiNhan' => $this->_session['PK_iMaCB'],
								'FK_iMaVB'        => $id_insert,
								'sThoiGianGui'    => date('Y-m-d H:i:s')
							);*/
							$mangthem2[] = array(
								'FK_iMaNguoiNhan' => 693,
								'FK_iMaVB'        => $id_insert,
								'sThoiGianGui'    => date('Y-m-d H:i:s')
							);
							$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem2);
						}
						
						
                    }
                    else{
                        $luuvetduyet = array(
                            'FK_iMaVB'        => $value5,
                            'FK_iMaCB_Chuyen' => $taikhoan,
                            'sThoiGian'       => date('Y-m-d H:i:s'),
                            'sNoiDung'        => 'Duyệt',
                            'FK_iMaCB_Nhan'   => $nguoinhan
                        );
                        $this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);
                        $Doc_array = array(
                            'PK_iMaCBDuyet' => $nguoinhan,
                            'iCoSangTao'    => (!empty($this->input->post('cosangtao')[$key5]) ? $this->input->post('cosangtao')[$key5] : 0)
                            /*'sLuuVaoKho' => (!empty($this->input->post('luukho')[$key5]) ? $this->input->post('luukho')[$key5] : 0),*/
                            // 'sNgayGiaiQuyet' => date('Y-m-d',time())
                        );

                        //$Kehoach_array = array('active' => 2,'ket_qua_hoanthanh'=>'đã duyệt','ngay_hoanthanh' => date('Y-m-d H:i:s'));
                        
                        //$this->Mvanbanchoxuly->capnhatkehoach('vanban_id', $this->input->post('doc_id')[$key5],'canbo_id',$this->_session['PK_iMaCB'], 'tuan  >=', date("W"), $Kehoach_array);
                        //$this->Mvanbanchoxuly->capnhatkehoach('vanban_id', $this->input->post('doc_id')[$key5],'active <',2, 'tuan  >=', date("W"), $Kehoach_array);

                        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen', $value5, 'tbl_vanbanden', $Doc_array);
                    }
//                    pr($Doc_array);                    
                }
            }
            return redirect(base_url() . 'vanbanchopheduyet');

        }
    }

    public function PhanTrang()
    {
        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 10 || $this->_session['iQuyenHan_DHNB'] == 11 || $this->_session['iQuyenHan_DHNB'] == 3){
            $loaivanban1 = $this->input->get('cacloaivanban');
            if($loaivanban1 ==  2){
                $stt=2;
            }else{
                $stt=1;
            }
            $cvctvb=NULL;
            $pheduyetCV=NULL;
            $Truongphong = $this->_session['PK_iMaCB'];
        } elseif($this->_session['iQuyenHan_DHNB'] == 7){
            $cvctvb = NULL;
            $loaivanban1 = $this->input->get('cacloaivanban');
            if($loaivanban1 ==  2){
                $stt=2;
            }else{
                $stt=1;
            }
            $pheduyetCV=NULL;
            $Truongphong = $this->_session['PK_iMaCB'];
//            pr($cvctvb);
        }elseif($this->_session['iQuyenHan_DHNB'] == 8 || $this->_session['iQuyenHan_DHNB'] == 12){
            $cvctvb = $this->_session['PK_iMaCB'];
            $loaivanban1 = $this->input->get('cacloaivanban');
            if($loaivanban1 ==  2){
                $stt=2;
            }
			if($loaivanban1 ==  1){
                $stt=1;
            }
			if($loaivanban1 ==  NULL){
                $stt=0;
            }
            $pheduyetCV='1';
            $Truongphong = NULL;
//            pr($cvctvb);
        }
        $loaivanban = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap = $this->input->get('ngaynhap');
        $donvi = $this->input->get('donvi');
        $ngayky = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url'] = base_url() . 'vanbanchopheduyet?cacloaivanban=2&loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows'] = count($this->Mvanbanchoxuly->getDocPheDuyet(NULL, NULL, NULL, $cvctvb, $stt,$pheduyetCV,$Truongphong, $loaivanban, $sokyhieu, $ngaynhap, $donvi, $ngayky, $ngayden, $trichyeu, $nguoiky, $soden, $denngay, $chucvu, $nguoinhap));
//        pr($config['total_rows']);
        $config['per_page'] = 30;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links'] = 9;
        $config['use_page_numbers'] = false;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo';
        $config['last_link'] = '&raquo';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&lt';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data['page'] = $this->input->get('page') ? $this->input->get('page') : 0;

        if ((is_numeric($data['page']) == FALSE) || $data['page'] % $config['per_page'] != 0 || ($data['page']) > $config['total_rows']) {
            redirect(base_url() . 'vanbanchopheduyet');
        }
        $data['loaivanban'] = $loaivanban;
        $data['sokyhieu'] = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] = $denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['count'] = $config['total_rows'];
        $data['info'] = $this->Mvanbanchoxuly->getDocPheDuyet(NULL, NULL,NULL, $cvctvb, $stt,$pheduyetCV,$Truongphong, $loaivanban, $sokyhieu, $ngaynhap, $donvi, $ngayky, $ngayden, $trichyeu, $nguoiky, $soden, $denngay, $chucvu, $nguoinhap, $config['per_page'], $data['page']);
        $data['pagination'] = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}