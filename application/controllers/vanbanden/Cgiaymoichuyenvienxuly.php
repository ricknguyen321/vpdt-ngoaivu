<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/17/2017
 * Time: 10:01 AM
 */
class Cgiaymoichuyenvienxuly extends MY_Controller {



    public function __construct()

    {

        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();

    }

    public function index()

    {
        $data['title']    = 'Danh sách tổng thể';
//        pr($this->_session);
        $idAppointment = $this->uri->segment(2);
        // trình tự xử lý
        $process = $this->Mgiaymoichoxuly->getDocProcess($idAppointment);
        $processcv = $this->Mgiaymoichoxuly->getDocProcesscv($idAppointment);
        $processpb = $this->Mgiaymoichoxuly->getDocProcessPB($idAppointment);
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');
        $data['idDocGo'] = $idAppointment;
        $data['process'] = array_merge($process,$processpb,$processcv);
		$data['chidao']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_chidao');
		$data['dexuat']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_ykien');
        if(!empty($idAppointment)){
            $data['getdAppointment'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
            $data['CBChiDao'] = $this->Mdanhmuc->layDuLieu2('PK_iMaVBDen',$idAppointment,'PK_iMaCB',$this->_session['PK_iMaCB'],'tbl_phongphoihop');
            // lay chuyen vien chu tri
            $cvtc = $this->Mgiaymoichoxuly->getCVCT($idAppointment);
            //ket qua phong thu ly
            if(!empty($cvtc)) {
                $data['resultPTL'] = $this->Mgiaymoichoxuly->getResultPTL($idAppointment, NULL, $cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPTL']);
                $data['resultPPH'] = $this->Mgiaymoichoxuly->getResultPPH($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
            }else{
                $data['resultPPH'] = $this->Mgiaymoichoxuly->getResultPPH($idAppointment,NULL,NULL);
            }
            $process2 =  $this->Mgiaymoichoxuly->getDocProcessCVPPH($idAppointment,$this->_session['FK_iMaPhongHD'], NULL);
            $data['TrinhTuPhoiHop'] = $process2;
//            pr($cvtc[0]['PK_iMaCVCT']);
        }
        // HOÀN THÀNH

        if($this->input->post('luulai')){
            $data_result = array(
                'sMoTa'               =>  $this->input->post('ketqua'),
                'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
                'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
                'PK_iMaVBDen'         =>  $idAppointment,
                'active'              =>  3,
                'sTenFiles'              =>  $this->input->post('fileketqua'),
                'sDuongDanFile'              =>  'doc_uploads/'.clear($_FILES["files"]["name"]),
                'date_file'           => date('Y-m-d',time())
            );
//            pr($data_result);

            $this->Mdanhmuc->themDuLieu('tbl_file_ketqua',$data_result);
//            $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden','iTrangThai','1');
            $data_status = array(
                'iTrangThai' =>  '3'
            );
            $this->Mgiaymoichoxuly->updatePPH($idAppointment,$this->_session['PK_iMaCB'],$data_status);
            $data_kh = array(
                'active' => 3,
				'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
				'ket_qua_hoanthanh'               =>  $this->input->post('ketqua')
            );
            $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$this->_session['PK_iMaCB'],$data_kh);
            $idCanbo = $this->input->post('canbo');
            $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$idCanbo,$data_kh);
            $this->upload('doc_uploads/',clear($_FILES["files"]["name"]),'files');
            return redirect(base_url().'giaymoichuyenvienxuly/'.$idAppointment);
        }
        $data['title']    = 'Chuyên viên xử lý';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoichuyenvienxuly';
        $this->load->view('layout_admin/layout',$temp);
    }
    /**
     * xử lý ảnh
     */
    public function upload($dir,$imgname,$filename)
    {
//        pr($dir);
        $config['upload_path']   = $dir;
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
        $config['overwrite']     = true;
        $config['file_name']     = $imgname;
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload($filename);
    }

}