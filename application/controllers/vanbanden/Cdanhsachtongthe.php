<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachtongthe extends CI_Controller {

    private $perPage = 5;

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
//        $this->load->library('pagination');
    }
    public function index()
    {
//        $count = $this->db->get('posts')->num_rows();

        if(!empty($this->input->get("page"))){
            $start = ceil($this->input->get("page") * $this->perPage);
            $data['posts'] = $this->Mvanbanden->getDocAjax($start, $this->perPage);
            $result = $this->parser->parse('vanbanden/Vdata', $data);
            echo json_encode($result);

        }else{
            $data['posts'] = $this->Mvanbanden->getDocAjax(5, $this->perPage);
//            pr($data['posts']);
            $data['title']    = 'Danh sách tổng thể';
            $temp['data']     = $data;
            $temp['template'] = 'vanbanden/Vdanhsachthongthe';
            $this->load->view('layout_admin/layout',$temp);
        }
    }

}