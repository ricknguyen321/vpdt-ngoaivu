<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**

 * cHUYEEN VIEN XU LY

 */

class Cchuyenvienchutrixuly extends MY_Controller {



    public function __construct()

    {

        parent::__construct();

        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');

        $this->Mdanhmuc = new Mdanhmuc();

        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanphoihop');
        $this->load->model('vanbandi/Mtruyennhan');

    }

    public function index()

    {
		$time = time();
//        pr($this->_session);
        $idAppointment            = $this->input->get('vbd');
        $thoigianhientai          = strtotime("-1 day",time());
        $data['thoigianhientai']  = date('Y-m-d',$thoigianhientai);
        $data['chuyenvienchutri'] = $this->Mvanbanphoihop->layCV_CT($idAppointment);
		$data['luuvetduyet']      = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$idAppointment,'tbl_luuvet_duyet');
        $phong_PH                 = $this->Mvanbanphoihop->layPB_PH($idAppointment);
		$data['dsldduyet']        = $this->Mvanbanchoxuly->layldduyet($this->_session['FK_iMaPhongHD']);
        $canbo                    = $this->Mtruyennhan->layDSCB2();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
			if (($value['iQuyenHan_DHNB'] == 6 || $value['iQuyenHan_DHNB'] == 3) && $value['FK_iMaPhongHD'] == $this->_session['FK_iMaPhongHD']  && $value['iTrangThai'] == 0) {
				$data['tp'] = $value;
				$data['phonghd'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_phongban');
			}
        }
        if(!empty($phong_PH))
        {
            $phongban                = explode(',',$phong_PH['PK_iMaPhongPH']);
            $data['ycpbphoihop'] = $this->Mvanbanphoihop->layPB($phongban);
			$data['phongbanphoihop'] = $this->Mvanbanphoihop->layQT_Phong_PH($idAppointment,$phongban);
			
        }
        else
        {
            $data['phongbanphoihop'] = '';
        }
        if(_post('dexuatyeucau'))
        {
            $data['content'] = $this->dexuatyeucau();
        }
		if ($this->input->post('luuykien')) {
			if (!empty($this->input->post('dexuat')) && $this->input->post('dexuat') != '') {
				$chidaold = array(
                'FK_iMaVBDen'        => $idAppointment,
                'sNoiDung'        => nl2br($this->input->post('dexuat')),
                'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
                'sThoiGian'   => date('Y-m-d H:i:s',time()),
				);
				$this->Mdanhmuc->themDuLieu('tbl_ykien',$chidaold);
				return redirect(base_url().'chuyenvienchutrixuly?vbd='.$idAppointment);
			} else {
				$data['content'] = messagebox('Chưa nhập nội dung','danger');
			}
		}
		if($this->input->post('luuhoso')){
           $data['content'] = $this->luuhoso();
        }
		$data['dshoso'] = $this->Mdanhmuc->layDuLieu('FK_iMaCB',$this->_session['PK_iMaCB'],'tbl_hoso');
		$dshsvb = $this->Mdanhmuc->layhosovbden($idAppointment);
		$dshsvanban = array();
		foreach ($dshsvb as $key => $value) {
			array_push($dshsvanban, $value['FK_iMaHS']);
		}
		$data['dshosovb'] = $dshsvanban;
		
        $data['chuyennhan_phoihop'] = $this->Mvanbanphoihop->layDS_CN($idAppointment);
        $data['luuvetchuyennhan']   = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_luuvet_chuyennhan');
		$data['chidao']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_chidao');
		$data['dexuat']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_ykien');
		$data['qtgiahan']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_luuvet_giahan');
        // trình tự xử lý
        $process = $this->Mvanbanchoxuly->getDocProcess($idAppointment);
        $processcv = $this->Mvanbanchoxuly->getDocProcesscv($idAppointment);
        $processpb = $this->Mvanbanchoxuly->getDocProcessPB($idAppointment);
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');
//        pr($process);
//        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($this->_session)
        $data['idDocGo'] = $idAppointment;
        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($data['process']);
        if(!empty($idAppointment)){
            $data['getdAppointment'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
            
			$data['VBDi'] = $this->Mdanhmuc->layDuLieu2('iSoDen',$data['getdAppointment'][0]['iSoDen'],'sNgayNhap >',date('Y').'-01'.'-01','tbl_vanbandi');
            foreach ($data['VBDi'] as $key2 => $value2){
                $file = $this->Mvanbanchoxuly->layFileLast($value2['PK_iMaVBDi']);
                if(!empty($file))
                {
                    $data['VBDi'][$key2]['sDuongDan'] = $file['sDuongDan'];
                }else{
                    $data['VBDi'][$key2]['sDuongDan'] = '';
                }
            }
            // lay chuyen vien chu tri
            $cvtc = $this->Mvanbanchoxuly->getCVCT($idAppointment);
            //ket qua phong thu ly
            if(!empty($cvtc)) {
                $data['resultPTL'] = $this->Mvanbanchoxuly->getResultPTL($idAppointment, NULL, $cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPTL']);
                $data['resultPPH'] = $this->Mvanbanchoxuly->getResultPPH($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPPH']);
            }
            else{
                $data['resultPPH'] = $this->Mvanbanchoxuly->getResultPPH($idAppointment,NULL,NULL);
            }
//            pr($cvtc[0]['PK_iMaCVCT']);
        }

        // HOÀN THÀNH
		$sKy_Hieu   =  $data['getdAppointment'][0]['sKy_Hieu'];
        $sNgay_Ky   =  $data['getdAppointment'][0]['sNgay_Ky'];
        $sMaCoQuan  =  $data['getdAppointment'][0]['sMaCoQuan'];
        $sTenCoQuan =  $data['getdAppointment'][0]['sTenCoQuan'];
        $taikhoan = $this->_session['PK_iMaCB'];

        if($this->input->post('luulai')){
            $phongban = $this->_session['FK_iMaPhongHD'];
			if($this->_session['iQuyenHan_DHNB'] < 8){
				$tp_pp    = $this->Mvanbanphoihop->layPH_TP($idAppointment,$taikhoan);				
				$tp_pp    = $tp_pp[0]['PK_iMaCBChuyen'];
			} else { // bỏ qua bước phó phòng duyệt
				$tp_pp    = $this->Mvanbanphoihop->layTPVBden($idAppointment); //ricknguyen321 
				$tp_pp    = $tp_pp[0]['PK_iMaCBChuyen'];
			}
			if (_post('ldduyet')) {
				$tp_pp = _post('ldduyet');
			}
			$data_vbden2 = array(
                    'PK_iMaCBDuyet' => $tp_pp
                );
			$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_vbden2);
            $luuvetduyet = array(
                'FK_iMaVB'        => $idAppointment,
                'FK_iMaCB_Chuyen' => $taikhoan,
                'sThoiGian'       => date('Y-m-d H:i:s'),
                'sNoiDung'        => nl2br(_post('ketqua')),
                'FK_iMaCB_Nhan'   => $tp_pp
            );
            $this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);
            if(!empty($sMaCoQuan)){
                //$this->createXML_PH($sKy_Hieu,$sNgay_Ky,$sMaCoQuan,$sTenCoQuan);
                //$this->sendMail($sKy_Hieu,$sNgay_Ky);
            }
			
			
            if($this->input->post('luukho')==2)$vbdaura_cv = 1;
            else $vbdaura_cv = 2;

            if($this->_session['iQuyenHan_DHNB'] < 7){
            	$data_vbden = array(
                    'iTrangThai' => '1',
                    'sNgayGiaiQuyet' => date('Y-m-d',time()),
                    'PK_iMaCBHoanThanh' => $this->_session['PK_iMaCB'],
                    'sLuuVaoKho' => $this->input->post('luukho'),
					'lydochammuon' => '',
                    //'ket_qua_hoanthanh'  =>  $this->input->post('ketqua'),
                    'vbdaura_cv' => $vbdaura_cv
                );
                $data_kh = array(
	                'active' => 5,
					'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
					'khokhan_vuongmac' => $this->input->post('chammuon'),
	                'ket_qua_hoanthanh'               =>  nl2br($this->input->post('ketqua'))
	            );
                if($vbdaura_cv == 1){
                    $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'thuc_hien !=',2,$data_kh);
                }else{
                   $this->Mvanbanchoxuly->xoaDuLieu2Where('vanban_id',$idAppointment,'', '','kehoach'); 
                }
	            
	            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_vbden);
				
				//Thông tin đến giám đốc VB có thời hạn đã được giải quyết.
				if ($data['getdAppointment'][0]['iDeadline'] == 1) {
					$datatt=array(
						'sMoTa'         => '<i>[Thông tin này được gửi tự động đến GĐ khi văn bản có thời hạn được hoàn thành]</i><br><p>- Văn bản đến số: <b style="color:red; font-size:16px">'.$data['getdAppointment'][0]['iSoDen'].'</b></p><p>- Trích yếu: <a href="quatrinhxuly/'.$data['getdAppointment'][0]['PK_iMaVBDen'].'"><b>'.$data['getdAppointment'][0]['sMoTa'].'</b></a></p><p>- GĐ giao có thời hạn: <b>'.$data['getdAppointment'][0]['sHanThongKe'].'</b></p>- Nội dung hoàn thành: <b style="color:blue">'.nl2br(_post('ketqua')).'</b>',
						'FK_iMaCB_Nhap' => $this->_session['PK_iMaCB'],
						'FK_iMaPB'      => $this->_session['FK_iMaPhongHD'],
						'sNgayNhap'		=> date('Y-m-d H:i:s')
					);
					$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_vanban',$datatt);
					
					/*$mangthem2[] = array(
						'FK_iMaNguoiNhan' => $this->_session['PK_iMaCB'],
						'FK_iMaVB'        => $id_insert,
						'sThoiGianGui'    => date('Y-m-d H:i:s')
					);*/
					$mangthem2[] = array(
						'FK_iMaNguoiNhan' => 735,
						'FK_iMaVB'        => $id_insert,
						'sThoiGianGui'    => date('Y-m-d H:i:s')
					);
					$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem2);
				}

            }else{
				$data_vbden = array(
                        'iTrangThai' => '2',
                        'sNgayGiaiQuyet' => date('Y-m-d',time()),
                        'PK_iMaCBHoanThanh' => $this->_session['PK_iMaCB'],
                        'sLuuVaoKho' => $this->input->post('luukho'),
                        'lydochammuon' => '',
                        //'ket_qua_hoanthanh'  =>  $this->input->post('ketqua'),
                        'vbdaura_cv' => $vbdaura_cv
                    );
	            $data_kh = array(
	                'active' => 2,
					'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
					'khokhan_vuongmac' => '',
	                'ket_qua_hoanthanh'               =>  nl2br($this->input->post('ketqua'))
	            );
                if($vbdaura_cv == 1){
                    $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$this->_session['PK_iMaCB'],$data_kh);
                }else{
                    $data_kh = array(
                        'active' => 0,
                        'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                        'khokhan_vuongmac' => $this->input->post('chammuon'),
                        'ket_qua_hoanthanh'               =>  nl2br($this->input->post('ketqua'))
                    );
                   $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'', '','','',$data_kh); 
                }
	            
	            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_vbden);
            }
			
			
            //ricknguyen321 upload kết quả
		   $time = time();
			$name = $_FILES['files']['name'];			
			if(!empty($name[0]))
			{
				$this->upload2('doc_uploads',$time);
				$files = array();
				foreach ($name as $key => $value) {					
					//ricknguyen321 thay thế các ký tự đặc biệt
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}
					$tmp = str_replace('$', '-', $tmp);
					$tmp = $tmp.'.'.$a[$c];

					$files[] = array(					
						'sMoTa'               =>  nl2br($this->input->post('ketqua')),
						'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
						'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
						'PK_iMaVBDen'         =>  $idAppointment,
						'active'              =>  3,
						'sTenFiles'              =>  $value,
						'sDuongDanFile'              =>  ($tmp=='.')?'':'doc_uploads/kq_'.$time.'_'.$tmp,
						'date_file'           => date('Y-m-d H:i:s',$time)				
						);
				} 
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_file_ketqua',$files);
				
			} else {
					$files = array();
					$files[] = array(					
					'sMoTa'               =>  nl2br($this->input->post('ketqua')),
					'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
					'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
					'PK_iMaVBDen'         =>  $idAppointment,
					'active'              =>  3,
					'date_file'           => date('Y-m-d H:i:s',$time)				
					);
					$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_file_ketqua',$files);
			}
            /*if($phongban==20)
            {
                $this->post_dulieu($idAppointment);
            }*/

            //Bat dau chuyen van ban sang co so du lieu-toan cau

            //$url   ='https://csdl.sotaichinh.hanoi.gov.vn/UM/Services/wsNhaDuLieuTuVB.asmx?wsdl';
            
            //$context = stream_context_create([
            //'ssl' => [
            //'verify_peer' => false,
            //'verify_peer_name' => false,
            //'allow_self_signed' => true,
            //]]);

            //$client = new SoapClient($url, [
            //'location' => $url,
            //'uri' => $url,
            //'stream_context' => $context
           // ]);

 // HOÀN THÀNH
            
        //$params=array(
            //'strTenDangNhapDi'=> $this->_session['sTaiKhoan'],//$data['getdAppointment'][0]['sTenDV'],
            //'strMaDonViDi'    => $this->_session['FK_iMaPhongHD'],//$data['getdAppointment'][0]['sTenLVB'],
            //'strIdPhongBanDi' => $sMaCoQuan,
            //'strTenVanBan'  =>$data['getdAppointment'][0]['sKy_Hieu'],
            //'strSoVanBan'  =>$sKy_Hieu,
            //'strNgayVanBanDi' => $sNgay_Ky,
            //'strTenDangNhapDen'=>$this->_session['sTaiKhoan'],
            //'strMaDonViDen'=>$this->_session['FK_iMaPhongHD'],
            //'IdPhongBanDen'=>$this->_session['FK_iMaPhongHD'],
           // 'strTrichYeu'=>$data['getdAppointment'][0]['sMoTa'],
           // 'strDuongDanFile' => $data['Filedinhkem'][0]['sDuongDan'],
           // 'blnVanBanPhapLy' =>0
           // );//pr($params);
       //print_r($params);



            //$response = $client->__soapCall('NhanDuLieuFile', array($params));
            //pr($response);
//ket thuc chuyen du lieu.  


            //return redirect(base_url().'chuyenvienchutrixuly?vbd='.$idAppointment);
			
			if ($this->_session['iQuyenHan_DHNB'] == 8) {
				 return redirect(base_url().'vanbanchoxuly_cv');
			}
			if ($this->_session['iQuyenHan_DHNB'] == 7) {
				 return redirect(base_url().'vanbanchoxuly_pp');
			}
			if ($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3) {
				 return redirect(base_url().'vanbanchoxuly_tp');
			}
           
        }

        if($this->input->post('luulaichuahoathanh')){
		
            if($this->input->post('luukho')==2)$vbdaura_cv = 1;
            else $vbdaura_cv = 2;

            if($this->_session['iQuyenHan_DHNB'] < 7){
                $data_vbden = array(
                    'iTrangThai' => '0',
                    //'sNgayGiaiQuyet' => date('Y-m-d',time()),
                    'PK_iMaCBHoanThanh' => $this->_session['PK_iMaCB'],
                    'sLuuVaoKho' => $this->input->post('luukho'),
                    'lydochammuon' => $this->input->post('chammuon'),
                    'vbdaura_cv' => $vbdaura_cv
                );
                $data_kh = array(
                    'active' => 2,
                    'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                    'khokhan_vuongmac' => $this->input->post('chammuon'),
                    'ket_qua_hoanthanh'               =>  nl2br($this->input->post('ketqua'))
                );
                if($vbdaura_cv == 1){
                    $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'thuc_hien !=',2,$data_kh);
                }else{
                   $this->Mvanbanchoxuly->xoaDuLieu2Where('vanban_id',$idAppointment,'', '','kehoach'); 
                }
                
                $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_vbden);

            }else{
                $data_vbden = array(
                        'iTrangThai' => '0',
                        //'sNgayGiaiQuyet' => date('Y-m-d',time()),
                        'PK_iMaCBHoanThanh' => $this->_session['PK_iMaCB'],
                        'sLuuVaoKho' => $this->input->post('luukho'),
                        'lydochammuon' => $this->input->post('chammuon'),
                        'vbdaura_cv' => $vbdaura_cv
                    );
                $data_kh = array(
                    'active' => 1,
                    'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                    'khokhan_vuongmac' => $this->input->post('chammuon'),
                    'ket_qua_hoanthanh'               =>  nl2br($this->input->post('ketqua'))
                );
                if($vbdaura_cv == 1){
                    $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$this->_session['PK_iMaCB'],$data_kh);
                }else{
                    $data_kh = array(
                        'active' => 0,
                        'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                        'khokhan_vuongmac' => $this->input->post('chammuon'),
                        'ket_qua_hoanthanh'               =>  nl2br($this->input->post('ketqua'))
                    );
                   $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'', '','','',$data_kh); 
                }
                
                $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_vbden);
            }
			
			
			
			//ricknguyen321
		   $time = time();
			$name = $_FILES['files']['name'];			
			if(!empty($name[0]))
			{
				$this->upload2('doc_uploads',$time);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}					
					$tmp = str_replace('$', '-', $tmp);					
					$tmp = $tmp.'.'.$a[$c];
					$files[] = array(
					
					'sMoTa'               =>  nl2br($this->input->post('ketqua')),
					'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
					'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
					'PK_iMaVBDen'         =>  $idAppointment,
					'active'              =>  3,
					'sTenFiles'              =>  ($this->input->post('fileketqua')!='')?$this->input->post('fileketqua'):$tmp,
					'sDuongDanFile'              =>  ($tmp=='.')?'':'doc_uploads/kq_'.$time.'_'.$tmp,
					'date_file'           => date('Y-m-d H:i:s',$time)				
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_file_ketqua',$files);
				
			}
			
            return redirect(base_url().'chuyenvienchutrixuly?vbd='.$idAppointment);
        }

		if($this->input->post('xoafile')){
            $this->Mdanhmuc->xoaDuLieu('PK_iMaKetQua',$this->input->post('xoafile'),'tbl_file_ketqua');
			return redirect(base_url().'chuyenvienchutrixuly?vbd='.$idAppointment);
        }
		if($this->input->post('xoadexuat')){
            $this->Mdanhmuc->xoaDuLieu('id',$this->input->post('xoadexuat'),'tbl_ykien');
			return redirect(base_url().'chuyenvienchutrixuly?vbd='.$idAppointment);
        }
        $data['title']    = 'Giao diện xử lý văn bản đến';
        $temp['data']     = $data;
		
		if($this->input->get('kx') == 'word'){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=Phiếu trình xử lý văn bản đến số ".$data['getdAppointment'][0]['iSoDen'].".doc");
			
            $temp['template'] = 'vanbanden/Vchuyenvienchutrixuly_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'vanbanden/Vchuyenvienchutrixuly';
			$this->load->view('layout_admin/layout',$temp);
        }
		
        
    }
	public function createXML_PH($sKy_Hieu,$sNgay_Ky,$sMaCoQuan,$sTenCoQuan)
    {
        $dir      = 'tmpxml/';
        $xml      = new DOMDocument("1.0",'UTF-8');

        $STRMADONVI = $xml->createElement('STRMADONVI');
        $STRMADONVI_Text = $xml->createTextNode('000.00.13.H26');
        $STRMADONVI->appendChild($STRMADONVI_Text);

        $STRTENDONVI = $xml->createElement('STRTENDONVI');
        $STRTENDONVI_Text = $xml->createTextNode('Sở Tài Chính');
        $STRTENDONVI->appendChild($STRTENDONVI_Text);

        $STRDIACHI = $xml->createElement('STRDIACHI');

        $STRDIENTHOAI = $xml->createElement('STRDIENTHOAI');

        $STREMAIL = $xml->createElement('STREMAIL');

        $STRWEBSITE = $xml->createElement('STRWEBSITE');

        $STRFAX = $xml->createElement('STRFAX');
        $STRSOKYHIEU = $xml->createElement('STRSOKYHIEU');
        $STRSOKYHIEU_Text = $xml->createTextNode($sKy_Hieu);
        $STRSOKYHIEU->appendChild($STRSOKYHIEU_Text);

        $STRNGAYKY = $xml->createElement('STRNGAYKY');
        $STRNGAYKY_Text = $xml->createTextNode($sNgay_Ky);
        $STRNGAYKY->appendChild($STRNGAYKY_Text);

        $STRMACOQUANBANHANH = $xml->createElement('STRMACOQUANBANHANH');
        $STRMACOQUANBANHANH_Text = $xml->createTextNode($sMaCoQuan);
        $STRMACOQUANBANHANH->appendChild($STRMACOQUANBANHANH_Text);

        $STRCOQUANBANHANH = $xml->createElement('STRCOQUANBANHANH');
        $STRCOQUANBANHANH_Text = $xml->createTextNode($sTenCoQuan);
        $STRCOQUANBANHANH->appendChild($STRCOQUANBANHANH_Text);

        $STRDONVIXULY = $xml->createElement('STRDONVIXULY');
        $STRDONVIXULY_Text = $xml->createTextNode('Chuyên viên');
        $STRDONVIXULY->appendChild($STRDONVIXULY_Text);

        $STRCANBOXULY = $xml->createElement('STRCANBOXULY');

        $STRDIENTHOAI_XL = $xml->createElement('STRDIENTHOAI');

        $STREMAIL_XL = $xml->createElement('STREMAIL');

        $STRMATRANGTHAI = $xml->createElement('STRMATRANGTHAI');
        $STRMATRANGTHAI_Text = $xml->createTextNode('4');
        $STRMATRANGTHAI->appendChild($STRMATRANGTHAI_Text);

        $STRNOIDUNGXULY = $xml->createElement('STRNOIDUNGXULY');
        $STRNOIDUNGXULY_Text = $xml->createTextNode('Đã hoàn thành');
        $STRNOIDUNGXULY->appendChild($STRNOIDUNGXULY_Text);

        
        $book   = $xml->createElement("EXPORTSTATUS");
        $NOIGUI     = $xml->createElement("NOIGUI");
        $NOIGUI->appendChild($STRMADONVI);
        $NOIGUI->appendChild($STRTENDONVI);
        $NOIGUI->appendChild($STRDIACHI);
        $NOIGUI->appendChild($STRDIENTHOAI);
        $NOIGUI->appendChild($STREMAIL);
        $NOIGUI->appendChild($STRWEBSITE);
        $NOIGUI->appendChild($STRFAX);

        $PHANHOIVANBAN = $xml->createElement('PHANHOIVANBAN');
        $PHANHOIVANBAN->appendChild($STRSOKYHIEU);
        $PHANHOIVANBAN->appendChild($STRNGAYKY);
        $PHANHOIVANBAN->appendChild($STRMACOQUANBANHANH);
        $PHANHOIVANBAN->appendChild($STRCOQUANBANHANH);

        $THONGTINXULY   = $xml->createElement("THONGTINXULY");
        $THONGTINXULY->appendChild($STRDONVIXULY);
        $THONGTINXULY->appendChild($STRCANBOXULY);
        $THONGTINXULY->appendChild($STRDIENTHOAI_XL);
        $THONGTINXULY->appendChild($STREMAIL_XL);

        $book->appendChild($NOIGUI);
        $book->appendChild($PHANHOIVANBAN);
        $book->appendChild($THONGTINXULY);
        $book->appendChild($STRMATRANGTHAI);
        $book->appendChild($STRNOIDUNGXULY);
        
        $xml->appendChild($book);
        $xml->formatOutput = true;
        $xml->save($dir.'XML_Output.sdk') or die("Error");
    }
    public function sendMail($sKy_Hieu,$sNgay_Ky)
    {
        $kyhieu = $sKy_Hieu;
        $ngayky = $sNgay_Ky;
        $name       = 'Văn thư Sở Tài chính';
        $from_email = 'vanthu_sotc@hanoi.gov.vn';
        $subject    = 'Phản hồi trạng thái văn bản ('.$kyhieu.' ngày '.$ngayky.'): Đã hoàn thành'; // nội dung gửi mail
        $message    = $subject;
        $to_email   = 'gnvbdtquatruc@hanoi.gov.vn';//$noinhan;// mail nhận
        //configure email settings
        $config['protocol']  = 'smtp';
        // $config['smtp_host'] = 'mail.thudo.gov.vn';
        // $config['smtp_port'] = '587';
        $config['smtp_host'] = 'ssl://mail.thudo.gov.vn';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'vanthu_sotc@hanoi.gov.vn';// email gửipthtk_sotc@hanoi.gov.vn
        $config['smtp_pass'] = 'sotchanoi042018';// pass cua mail trentinhoc@gmail.com
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';
        $config['wordwrap']  = TRUE;
        $config['newline']   = "\r\n"; //use double quotes
        $this->load->library('email', $config);
        $this->email->initialize($config);
        //send mail
        $this->email->clear(TRUE);
        $this->email->from($from_email, $name);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        $filename = 'tmpxml/XML_Output.sdk';
        $this->email->attach($filename);
        if ($this->email->send(FALSE)) {
        }
        else{
        }
    }
    public function dexuatyeucau()
    {
        $taikhoan = $this->_session['PK_iMaCB'];
        $phongban = $this->_session['FK_iMaPhongHD'];
        $mavanban = _get('vbd');
        $name     = $_FILES['file']['name'];
        if(!empty($name))
        {
            $dir = 'files_yeucauphoihop/';
            $time= time();
            $this->upload($dir,$name,$time);
        }
        $yeucaudexuat = nl2br(_post('yeucaudexuat'));
        $phongphoihop = _post('phongphoihop');
        $layPH_TP     = $this->Mvanbanphoihop->layPH_TP($mavanban,$taikhoan);
        if(!empty($layPH_TP))
        {
            foreach ($layPH_TP as $key => $value) {
                $data[]=array(
                    'FK_iMaVBDen'      => $mavanban,
                    'FK_iMaNguoi_Gui'  => $taikhoan,
                    'FK_iMaNguoi_Nhan' => $value['PK_iMaCBChuyen'],
                    'sThoiGian_Chuyen' => date('Y-m-d H:i:s'),
                    'sNoiDung'         => $yeucaudexuat,
                    'CT_PH'            => 1,
                    'iTrangThai'       => 1,
                    'FK_iMaPB_PH'      => $phongphoihop,
                    'sFile_YeuCau'     => ($name)?($dir.$time.'_'.clear($name)):''
                );
            }
            $kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_phoihop',$data);
            if($kiemtra>0)
            {
				return redirect(base_url().'chuyenvienchutrixuly?vbd='.$mavanban);
                return messagebox('Gửi đề xuất yêu cầu phối hợp thành công','info');
            }
        }

    }
    /*public function upload($dir,$name,$time)
    {
        if(is_dir($dir)==false){
            mkdir($dir);        // Create directory if it does not exist
        }
        $config['upload_path']   = $dir;
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
        $config['overwrite']     = true;
        $config['file_name']     = $time.'_'.clear($name);
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('file');
        $this->upload->data();
    }*/

    
    
    public function post_dulieu($idAppointment)
    {
        $phongban          = $this->_session['FK_iMaPhongHD'];
        $mang              = $this->Mvanbanchoxuly->laychuyennhan($idAppointment);
        $phongphoihop      = $mang[0]['PK_iMaPhongPH'];
        $mangchuyennhan    = array_column($mang, 'sHoTen','iQuyenHan_DHNB');
        $hoten             = $this->_session['sHoTen'];
        $quyen             = $this->_session['iQuyenHan_DHNB'];
        $laythongtinvanban = $this->Mvanbanchoxuly->laythongtinvanban($idAppointment);
        if(!empty($phongphoihop))
        {
            $mangphongphoihop = explode(',', $phongphoihop);
            foreach ($mangphongphoihop as $v) {
                $canbophongph[] = $this->Mvanbanchoxuly->layluongchuyennhanph($idAppointment,$v);
            }
        }
        $mang_post=array(
            '0' => ($laythongtinvanban)?$laythongtinvanban['PK_iMaVBDen']:'',
            '1' => (isset($mangchuyennhan[4]))?$mangchuyennhan[4]:'',
            '2' => (isset($mangchuyennhan[5]))?$mangchuyennhan[5]:'',
            '3' => ($quyen==6)?$hoten:(isset($mangchuyennhan[6]))?$mangchuyennhan[6]:'',
            '4' => ($quyen==7)?$hoten:(isset($mangchuyennhan[7]))?$mangchuyennhan[7]:'',
            '5' => ($quyen==8)?$hoten:'',
            '6' => ($laythongtinvanban)?$laythongtinvanban['sMoTa']:'',
            '7' => ($laythongtinvanban)?$laythongtinvanban['sDuongDan']:'',
            '8' => ($laythongtinvanban)?$laythongtinvanban['sTenLVB']:'',
            '9' => _post('ketqua'),
            '10'=> date('d/m/Y H:i:s'),
            '11'=> ($phongphoihop)?$canbophongph:''
        );
        $header = array(
            'username' => 'vpdt',
            'password' => '2018$%^#',
            'time'     => '1522122518',
            'checksum' => 'ceba0b1fc298d6281eccf5bd3034ecf5'
        );
        $mang_post_sang = array(
            'header'=>$header,
            'data'  =>$mang_post
        );
        $dulieu = json_encode($mang_post_sang);
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.apecsoft.asia/sotaichinh/api/vpdt/post_vanban",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $dulieu,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: 43b43904-edcf-7fe4-d656-25f924625d3e"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
        } else {
          // echo $response;
        }
    }
	
	
	public function upload($dir,$imgname,$filename)
    {
//        pr($dir);
        $config['upload_path']   = $dir;
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
        $config['overwrite']     = true;
        $config['file_name']     = $imgname;
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload($filename);
    }
	
	
	public function upload2($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {									
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'kq_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
	public function luuhoso(){
		 $idDoc       = $this->input->post('luuhoso');
		 $vanban = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden');
		 $manghs = $this->input->post('manghs'.$idDoc);
		 $dshsvb = $this->Mdanhmuc->layhosovbden($idDoc);
		$dshsvanban = array();
		foreach ($dshsvb as $key => $value) {
			array_push($dshsvanban, $value['FK_iMaHS']);
		}
		
			foreach ($manghs as $key => $value) {
				 if (in_array($value, $dshsvanban)) continue;
				 $data = array(
					'FK_iMaVBDen' => $idDoc,
					'sThoiGian' => date('Y-m-d H:i:s',time()),
					'FK_iMaCB' => $this->_session['PK_iMaCB'],
					'FK_iMaPB' => $this->_session['FK_iMaPhongHD'],
					'FK_iMaHS' => $value,
					'iSoVB' => $vanban[0]['iSoDen'],
					'sTenLVB' => $vanban[0]['sTenLVB'],
					'sKyHieu' => $vanban[0]['sKyHieu'],
					'sNgayNhap' => $vanban[0]['sNgayNhap'],
					'sMoTa' => $vanban[0]['sMoTa'],
					);
				 $this->Mdanhmuc->themDuLieu('tbl_hosovanban',$data);
			}
		
			foreach ($dshsvanban as $key => $value) {
				if(in_array($value,$manghs)) continue;
					//xoa ho so van ban
					$this->Mdanhmuc->xoaHoSoVB($idDoc,$value);
			}			
		
		redirect(base_url().'chuyenvienchutrixuly?vbd='.$idDoc);
	}

}



/* End of file Cnhapgiaymoi.php */

/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */