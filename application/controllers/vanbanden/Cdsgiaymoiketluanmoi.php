<?php

/**

 * Created by PhpStorm.

 * User: Minh Duy

 * Date: 9/18/2017

 * Time: 3:22 PM

 */

class Cdsgiaymoiketluanmoi extends MY_Controller {



    public function __construct()

    {
        ini_set('memory_limit', '2048M');
        parent::__construct();

        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');

        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();

        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');

        $this->Mvanbanden = new Mvanbanden();

        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');

        $this->Mdanhmuc = new Mdanhmuc();

        $this->load->library('pagination');

    }

    public function index()

    {
        $action = $this->input->post('action');
        switch ($action) {
            case 'getData':
                $this->getData();
                break;
            default:
                # code...
                break;
        }

        // danh sách phó phòng

        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);

//        pr($data['getAccountDeputyDirector']);

        // danh sách chuyên viên

        $data['getDepartment'] = $this->Mgiaymoichoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);

        //danh sách chờ xử lý
        // chuyen tiep bao cao cho can bo
        if($this->input->post('chuyentiepcanbo')){
            $data['content'] = $this->ChuyenTiep();
        }

        $data['urlcv'] = $this->uri->segment(1);
        if($this->input->post('duyetbc')){
            $this->DuyetVB();
        }
        if($this->input->post('tuchoibc')){
            $this->TuChoiVB();
        }

//        pr($urlcv);

//        $data['getDocAwaitPPH'] = $this->Mgiaymoichoxuly->getDocAwaitPPH(NULL,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);

//        pr($data['getDocAwaitPPH']);

        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();

        $data['getDocAwait']    = $page['info'];

//        pr($page['info']);

        if(!empty($data['getDocAwait'])){

            foreach ($data['getDocAwait'] as $key => $value) {

                $process = $this->Mgiaymoichoxuly->getDocProcess($value['PK_iMaVBDen']);

                $processcv = $this->Mgiaymoichoxuly->getDocProcesscv($value['PK_iMaVBDen']);

                $processpb = $this->Mgiaymoichoxuly->getDocProcessPB($value['PK_iMaVBDen']);

                $process1 = array_merge($process,$processpb,$processcv);
// danh sach cac phong duoc gui
                $listphong =  $this->Mdanhmuc->layDuLieudistinct('PK_iMaVBDen',$value['PK_iMaVBDen'],'tbl_soanbaocao');
                $idTenPhong = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_phongban');
                $mang_phong = array();
                foreach($listphong as $key2 => $value2){
                    foreach ($idTenPhong as $key4 => $value4){
                        if($value2['PK_iMaPB'] == $value4['PK_iMaPB']){
                            $mang_phong[$key2] = $value4['sTenPB'];
                        }
                    }
                }
//                pr($process1);

                $arrayTT = array();

                // if(!empty($process1)){

                //     foreach($process1 as $key1 => $value1){

                //         $arrayTT[$key1] = $value1['sHoTen'];

                //     }

                // }

                $data['getDocAwait'][$key]['TrinhTu'] = $arrayTT;
                $data['getDocAwait'][$key]['TenPhong'] = $mang_phong;

//                pr($arrayCBNhan);

            }

//            pr($data['getDocAwait']);

        }

        $data['phantrang']     = $page['pagination'];

        $data['loaivanban']      = $page['loaivanban'];

        $data['sokyhieu']    = $page['sokyhieu'];

        $data['ngaynhap']       = $page['ngaynhap'];

        $data['donvi']        = $page['donvi'];

        $data['ngayky']          = $page['ngayky'];

        $data['ngayden']      = $page['ngayden'];

        $data['trichyeu']      = $page['trichyeu'];

        $data['nguoiky']   = $page['nguoiky'];

        $data['soden']     = $page['soden'];

        $data['denngay']    = $page['denngay'];

        $data['chucvu']   =$page['chucvu'];

        $data['nguoinhap']  = $page['nguoinhap'];

        $data['cacloaivanban'] = $page['cacloaivanban'];

        $data['count'] = $page['count'];

        // trình tự xử lý
//        pr($data['process']);

        $data['title']    = 'Báo cáo kết quả cuộc họp mới';

        $temp['data']     = $data;

        $temp['template'] = 'vanbanden/Vdsgiaymoiketluanmoi';

        $this->load->view('layout_admin/layout',$temp);

    }

    public function PhanTrang()

    {

        $loaivanban    = $this->input->get('loaivanban');

        $sokyhieu = $this->input->get('sokyhieu');

        $ngaynhap      = $this->input->get('ngaynhap');

        $donvi    = $this->input->get('donvi');

        $ngayky    = $this->input->get('ngayky');

        $ngayden = $this->input->get('ngayden');

        $trichyeu = $this->input->get('trichyeu');

        $nguoiky = $this->input->get('nguoiky');

        $soden = $this->input->get('soden');

        $denngay = $this->input->get('denngay');

        $cacloaivanban = $this->input->get('cacloaivanban');

//        pr($this->input->get('denngay'));

        $chucvu = $this->input->get('chucvu');

        $nguoinhap = $this->input->get('nguoinhap');

        $config['base_url']             = base_url().'dsgiaymoiketluanmoi?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;

        $config['total_rows']           = count($this->Mgiaymoichoxuly->getMeeting($this->_session['PK_iMaCB'],0,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));

//        pr($config['total_rows']);

        $config['per_page']             = 10;

        $config['page_query_string']    = TRUE;

        $config['query_string_segment'] = 'page';

        $config['num_links']            = 9;

        $config['use_page_numbers']     = false;

        $config['full_tag_open']        = '<ul class="pagination">';

        $config['full_tag_close']       = '</ul>';

        $config['first_link']           = '&laquo';

        $config['last_link']            = '&raquo';

        $config['first_tag_open']       = '<li>';

        $config['first_tag_close']      = '</li>';

        $config['prev_link']            = '&lt';

        $config['prev_tag_open']        = '<li class="prev">';

        $config['prev_tag_close']       = '</li>';

        $config['next_link']            = '&gt;';

        $config['next_tag_open']        = '<li>';

        $config['next_tag_close']       = '</li>';

        $config['last_tag_open']        = '<li>';

        $config['last_tag_close']       = '</li>';

        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";

        $config['cur_tag_close']        = '</a></li>';

        $config['num_tag_open']         = '<li>';

        $config['num_tag_close']        = '</li>';



        $this->pagination->initialize($config);



        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;



        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {

            redirect(base_url().'dsgiaymoiketluanmoi');

        }

        $data['loaivanban']    = $loaivanban;

        $data['sokyhieu']      = $sokyhieu;

        $data['ngaynhap'] = $ngaynhap;

        $data['donvi'] = $donvi;

        $data['ngayky'] = $ngayky;

        $data['ngayden'] = $ngayden;

        $data['trichyeu'] = $trichyeu;

        $data['nguoiky'] = $nguoiky;

        $data['soden'] = $soden;

        $data['denngay'] =$denngay;

        $data['chucvu'] = $chucvu;

        $data['nguoinhap'] = $nguoinhap;

        $data['cacloaivanban'] =$cacloaivanban;

        $data['count']    = $config['total_rows'];

        $data['info']        = $this->Mgiaymoichoxuly->getMeeting($this->_session['PK_iMaCB'],0,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);

        $data['pagination']  = $this->pagination->create_links();

//        pr($data['info']);

        return $data;

    }
    public function getData(){
        $idTypeOf = $this->input->post('idTypeOf');
        $data = $this->Mdanhmuc->setDuLieu('PK_iMaSoanBaoCao',$idTypeOf,'tbl_soanbaocao','iTrangThaiXem','1');
        echo json_encode($data);
        exit();
    }
    public function DuyetVB(){
        $result = $this->input->post('data');
        $idDoc = $this->input->post('idVanBanDen');
        $idContent = $this->input->post('idNoiDung');
        $this->Mdanhmuc->capnhatDuLieu('PK_iMaNoiDung',$idContent,'tbl_noidungbaocao',$result);
        $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idDoc,'tbl_soanbaocao','iTrangThaiXem',0);
    }
    public function TuChoiVB(){
        $result = $this->input->post('data');
        $idDoc = $this->input->post('idVanBanDen');
        $idContent = $this->input->post('idNoiDung');
        $this->Mdanhmuc->capnhatDuLieu('PK_iMaNoiDung',$idContent,'tbl_noidungbaocao',$result);
        $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idDoc,'tbl_soanbaocao','iTrangThai',2);
    }
    public function ChuyenTiep(){
        $nguoinhan = $this->input->post('nguoinhan');
        $mang_share = array();
        foreach ($nguoinhan as $key3 => $value3){
            $mang_share[$key3]['PK_iMaVBDen'] = $this->input->post('maphongbantp');
            $mang_share[$key3]['PK_iMaCBNhap'] = $this->input->post('idCBNhap1');
            $mang_share[$key3]['PK_iMaNoiDung'] = $this->input->post('idNoiDung');
            $mang_share[$key3]['sNgayNhap'] = date('Y-m-d H:i:s');
            $mang_share[$key3]['PK_iMaCB'] = $value3;
            $mang_share[$key3]['PK_iMaPB'] = $this->_session['FK_iMaPhongHD'];
            $mang_share[$key3]['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
            $mang_share[$key3]['sNoiDungChuyenTiepTP'] = $this->input->post('sNoiDungChuyenTiepTP');
            $mang_share[$key3]['PK_iMaPhongNhap'] = $this->input->post('idPhong');
            $mang_share[$key3]['iTrangThaiXem'] = 0;
            $mang_share[$key3]['iTrangThai'] = 0;
            $mang_share[$key3]['sFile'] = $this->input->post('FileBaoCao');
        }
//            pr($mang_share);
        $DocGo = $this->Mdanhmuc->themNhieuDuLieu('tbl_soanbaocao',$mang_share);
//        pr($DocGo);
        if ($DocGo > 0) {
            return messagebox('Chuyển báo cáo thành công', 'info');
        } else {
            return messagebox('Chuyển báo cáo thất bại', 'danger');
        }
    }



}