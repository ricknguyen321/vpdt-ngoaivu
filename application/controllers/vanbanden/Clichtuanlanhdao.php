<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clichtuanlanhdao extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('Vanbanden/Mlichcongtac');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('Vanbanden/Mvanbanden');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		$dsphongban = $this->Mlichcongtac->layPhongBan();
		foreach ($dsphongban as $key => $value) {
			$mangphongban[$value['PK_iMaPB']] = $value['sTenPB'];
		}
		$data['mangphongban'] = $mangphongban;
		if(_post('luulai'))
		{
			$data['content'] = $this->themFile();
		}
		
		if(_post('luutl'))
		{
			$data['content'] = $this->themTLHop(_post('luutl'));
		}
		
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		$canbo                    = $this->Mtruyennhan->layDSCB();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
		$tuan				= _get('tuan');
		$year				= $_SESSION['nam'];//date('Y');
		if($year <'2000') $year		= date('Y');
		$week				= date('W');
		$tuan_week			= ($tuan)?$tuan:$week;
		$ngaytuan = [
			array('Thứ Hai',date('d/m/Y',strtotime( $year . "W". $tuan_week . 1))),
			array('Thứ Ba',date('d/m/Y',strtotime( $year . "W". $tuan_week . 2))),
			array('Thứ Tư',date('d/m/Y',strtotime( $year . "W". $tuan_week . 3))),
			array('Thứ Năm',date('d/m/Y',strtotime( $year . "W". $tuan_week . 4))),
			array('Thứ Sáu',date('d/m/Y',strtotime( $year . "W". $tuan_week . 5))),
			array('Thứ Bảy',date('d/m/Y',strtotime( $year . "W". $tuan_week . 6))),
			array('Chủ Nhật',date('d/m/Y',strtotime( $year . "W". $tuan_week . 7)))
		];//echo  $year . "W". $tuan_week . 1;
		$start_date            = strtotime( $year . "W". $tuan_week . 1);
		$end_date              = strtotime( $year . "W". $tuan_week . 7);
		$ngaybd                = date('Y-m-d',$start_date);
		$ngaykt                = date('Y-m-d',$end_date);
		$data['ngaytrongtuan'] = $ngaytuan;//pr($data['ngaytrongtuan']);
		$data['ngaytuan']	   = $ngaytuan;
		$data['tuan']          = (int)($tuan)?$tuan:$week;
		$tuantruoc             = ($data['tuan']!=1)?($data['tuan']-1):1;
		$data['tuantruoc']     = ($tuantruoc<10)?'0'.$tuantruoc:$tuantruoc;
		$tuansau               = ($data['tuan']!=53)?($data['tuan']+1):53;
		$data['tuansau']       = ($tuansau<10)?'0'.$tuansau:$tuansau;
		$data['tuanhientai']   = (strlen($week)==1)?'0'.$week:$week;
		$data['bgd']			= $this->Mlichcongtac->layBGD();//pr($data['bgd']);
		$data['ldso']			= _get('ldso');
		$taikhoan = $this->_session;
		$quyen    = $taikhoan['iQuyenHan_DHNB'];
		if($quyen==9 || $quyen==2 || $quyen==3 || $quyen==6)
		{
			$macb = _get('lanhdao');
			if(empty($macb))
			{
				$macb = 735;
			}
			$macanbo  = 735;
			$quyen    = 4;
			$phongban = 0;
		}
		else
		{
			$macb  = _get('lanhdao');
			$macanbo  = $taikhoan['PK_iMaCB'];
			$quyen    = $taikhoan['iQuyenHan_DHNB'];
			$phongban = $taikhoan['FK_iMaPhongHD'];
		}
		
		$phogiamdoc       = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($phogiamdoc as $key => $value) {
			$data['phogiamdoc'][]=$value['PK_iMaCB'];
		}
		$mavb = $this->Mlichcongtac->layMaVB($macb,$macanbo,$quyen,$phongban,$ngaybd,$ngaykt);
		$mangmavb=array(0);
		if(!empty($mavb))
		{
			foreach ($mavb as $key => $value) {
				
				$mangmavb[] =  $value['PK_iMaVBDen'];
			}
		}
		$data['lichhop']	   = $this->Mlichcongtac->layVBDen($ngaybd,$ngaykt,$mangmavb);//echo $ngaybd." - ".$ngaykt."<br>"; pr($data['lichhop']);

		//$data['lichhoptaiso']	   = $this->Mvanbandi->layLichHop2_2($ngaybd,$ngaykt,'chieu');
		// $chidao = $this->Mlichcongtac->layChiDao($mangmavb);
		// pr($mangmavb);
		if($macb > 0)$malanhdaoduhop = $macb;
		else  $malanhdaoduhop = 0;
		if(!empty($data['lichhop']))
		{
			foreach ($data['lichhop'] as $k => $l) {
				$phongPH  = $this->Mlichcongtac->layPhongPH($l['PK_iMaVBDen']);
				if(!empty($phongPH))
				{
					$data['lichhop'][$k]['phoihop'] = explode(',',$phongPH['PK_iMaPhongPH']);
				}
				$duongdan = $this->Mvanbanden->layFileLast($l['PK_iMaVBDen']);
                if(!empty($duongdan))
                {
                    $data['lichhop'][$k]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $data['lichhop'][$k]['sDuongDan'] = '';
                }
                $baocaohop = $this->Mlichcongtac->layBaoCaoHop($l['PK_iMaVBDen']);
                if(!empty($baocaohop))
                {
                	$data['lichhop'][$k]['baocaohop'] = $baocaohop;
                }
                else{
                	$data['lichhop'][$k]['baocaohop'] = '';
                }
				$noidung = '';
				$i=0;
				$chidao = $this->Mlichcongtac->layChiDao($l['PK_iMaVBDen']);
				foreach ($chidao as $cd) {
					if($l['PK_iMaVBDen'] == $cd['PK_iMaVBDen'])
					{
						if(!empty($chidao[0]['PK_iMaCBNhan']))
						{
							if($l['iDuHop']==1)
							{
								//$data['lichhop'][$k]['lanhdaoduhop'] = 'Giám đốc dự họp';
								$data['lichhop'][$k]['lanhdaoduhop'] = 'Ngô Minh Hoàng';
								if($l['iPGDDuHop']==1)
								{
									$data['lichhop'][$k]['lanhdaoduhop'] = $data['lichhop'][$k]['lanhdaoduhop'].'<br>- Phó giám đốc cùng dự họp';
								}
								if($l['iPhongDuHop']==1)
								{
									$data['lichhop'][$k]['lanhdaoduhop'] = $data['lichhop'][$k]['lanhdaoduhop'].'<br>- Phòng chủ trì cử cán bộ cùng dự họp';
								}
							}
							else{
								
								$i++;
								if(in_array($chidao[1]['PK_iMaCBNhan'], $data['phogiamdoc']))
								{
									// echo $l['iPhongDuHop'];
									if($l['iPhongDuHop']==1)
									{
										$data['lichhop'][$k]['lanhdaoduhop'] = '- Phòng dự họp';
									} else {
										$data['lichhop'][$k]['lanhdaoduhop'] = $chidao[1]['sHoTen'].'';
									}
								}
								else
								{
									if($l['iPhongDuHop']==1 && $l['iPGDDuHop']==1)
									{
										$data['lichhop'][$k]['lanhdaoduhop'] = $chidao[0]['sHoTen'].'<br>- Phòng chủ trì cử cán bộ cùng dự họp';
									} elseif($l['iPhongDuHop']==1 && $l['iPGDDuHop']!=1)
									{
										$data['lichhop'][$k]['lanhdaoduhop'] = '- Phòng dự họp';
									} else {
										$data['lichhop'][$k]['lanhdaoduhop'] = ($chidao[0]['PK_iMaCBNhan']=='735')?$chidao[0]['sHoTen']: $chidao[0]['sHoTen'];
									}
								}
							}
						}
						else{							
							if($l['iDuHop']==1)
							{
								//$data['lichhop'][$k]['lanhdaoduhop'] = 'Giám đốc dự họp';
								$data['lichhop'][$k]['lanhdaoduhop'] = 'Ngô Minh Hoàng';
								if($l['iPGDDuHop']==1)
								{
									$data['lichhop'][$k]['lanhdaoduhop'] = $data['lichhop'][$k]['lanhdaoduhop'].'<br>- PGĐ cùng dự họp';
								}
							}
							
						}
						// hàm explode dùng ở đây để tách phòng chủ trì ra so với các phong phối hợp
						$chidaocuoi = explode('.',$cd['sMoTa'])[0];
						$noidung = $noidung.'<p>'.$cd['sMoTa'].'</p>';
						//$noidung = $noidung.'<p>'.$chidaocuoi.'</p>';
						$data['lichhop'][$k]['chidao'] = $noidung;
					}
				}
			}
		}
		$data['thoigian']      = $week;
		$data['malanhdao']     = $macb;
		$arr_lichhopso = array();
		
		// lịch họp giấy mời đến
		foreach ($data['ngaytrongtuan'] as $key => $value) {
			$ngay=1;
			$sang=1;
			$chieu=1;
			$dem1 =0;
			$dem2 =0;
			foreach ($data['lichhop'] as $k => $val) {
				if($value[1]==date_select($val['sGiayMoiNgay']))
				{
					if($val['sGiayMoiGio']>'12:00')
					{							
						//$data['ngaytrongtuan'][$key]['chieu']['name'] = 'Chiều';
						$data['ngaytrongtuan'][$key]['chieu']['giaymoi'][] = $data['lichhop'][$k];
					}
					else{						
						//$data['ngaytrongtuan'][$key]['sang']['name'] = 'Sáng';
						$data['ngaytrongtuan'][$key]['sang']['giaymoi'][] = $data['lichhop'][$k];
					}
				}
				else{
				}
			}	
		}
		
		
		// lịch họp sở chủ trì
		foreach ($data['ngaytrongtuan'] as $key => $value) {
			$ngay=1;
			$sang=1;
			$chieu=1;
			$dem1 =0;
			$dem2 =0;
			$lichhopso = @$this->Mvanbandi->layLichHopSo($phongban,$quyen,$malanhdaoduhop,date_insert($value[1]),'01:00','24:00');
			
			if(count($lichhopso) > 0){
				
				
				foreach ($lichhopso as $k => $val) {
					if($value[1]==date_select($val['sNgayMoi']))
					{
						if($val['sGioMoi']>'12:00')
						{	
							
										$duongdan = $this->Mvanbandi->layFileLast($lichhopso[$k]['PK_iMaVBDi']);
										if(!empty($duongdan))
										{
											$lichhopso[$k]['sDuongDan'] = $duongdan['sDuongDan'];
										}else{
											$lichhopso[$k]['sDuongDan'] = '';
										}
										$data['ngaytrongtuan'][$key]['chieu']['giaymoi'][] = $lichhopso[$k];
									
						}
						else{
				
								
										$duongdan = $this->Mvanbandi->layFileLast($lichhopso[$k]['PK_iMaVBDi']);
										if(!empty($duongdan))
										{
											$lichhopso[$k]['sDuongDan'] = $duongdan['sDuongDan'];
										}else{
											$lichhopso[$k]['sDuongDan'] = '';
										}
										$data['ngaytrongtuan'][$key]['sang']['giaymoi'][] = $lichhopso[$k];

						}
					}
					
				}	
			}
			
			$data['ngaytrongtuan'][$key]['sang']['giaymoi'] = $this->sapxep($data['ngaytrongtuan'][$key]['sang']['giaymoi']);
			$data['ngaytrongtuan'][$key]['chieu']['giaymoi'] = $this->sapxep($data['ngaytrongtuan'][$key]['chieu']['giaymoi']);
			
		}
		
		//pr($data['ngaytrongtuan']);
		$data['nam_vbdi']       = date('Y');
		$data['quyenlanhdao']       = $quyen;
		$data['lanhdao']       = $this->Mvanbandi->layNguoiKy();
		$data['macanbo']	   = $this->_session['PK_iMaCB'];
		$data['title']         = 'Lịch họp Sở Ngoại vụ';
		$data['url']           = base_url();//pr($data['ngaytrongtuan']);
		$this->parser->parse('vanbanden/Vlichtuanlanhdao',$data);
	}
	public function themFile()
	{
		$mavanban = _post('luulai');
		$name     = $_FILES['files']['name'][$mavanban];
		if(!empty($name))
		{
			$this->upload('doc_uploads/',clear($name),$mavanban);
			$data_result = array(
				'PK_iMaPB'      =>  $this->_session['FK_iMaPhongHD'],
				'PK_iMaCB'      =>  $this->_session['PK_iMaCB'],
				'PK_iMaVBDen'   =>  $mavanban,
				'active'        =>  1,
				'sTenFiles'     =>  _post('tenfile['.$mavanban.']'),
				'sDuongDanFile' =>  'doc_uploads/'.clear($name),
				'date_file'     =>  date('Y-m-d H:i:s',time())
	        );
	        $this->Mdanhmuc->themDuLieu('tbl_file_ketqua',$data_result);
		}
        
	}
	public function upload($dir,$filename,$i)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            if ($_FILES['files']['name'][$i]) {
				$_FILES['files']['name']     = $file['files']['name'][$i];
				$_FILES['files']['type']     = $file['files']['type'][$i];
				$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
				$_FILES['files']['error']    = $file['files']['error'][$i];
				$_FILES['files']['size']     = $file['files']['size'][$i];
				$config['upload_path']   = $dir;
				$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
				$config['overwrite']     = true;
				$config['file_name']     = $filename;
				$this->load->library('upload');
             	$this->upload->initialize($config);
                $checkResult = $this->upload->do_upload('files');
                $fileData = $this->upload->data();
            }
        }
	}
	
	public function themTLHop($mavanban)
	{
			$time = time();
			$name = $_FILES[$mavanban.'files']['name'];			
			if(!empty($name[0]))
			{
				$this->upload2('doc_uploads_'.date('Y'),$time,$mavanban);
				$files = array();
				foreach ($name as $key => $value) {
					if ($value == '') continue;
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}					
					$tmp = str_replace('$', '-', $tmp);					
					$tmp = $tmp.'.'.$a[$c];	
					
					$files[] = array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'    => $value,
						'sDuongDan'   => 'doc_uploads_'.date('Y').'/tl_hop_'.$time.'_'.$tmp,
						'sThoiGian'   => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'    => $this->_session['PK_iMaCB']
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_tlhop',$files);
				if($kiemtra>0)
				{
					redirect('lichcongtac');
				}
				else{
					return messagebox('Thêm tệp tin thất bại','danger');
				}
			}
	}
	public function upload2($dir,$time,$ma)
	{
        $fileNumber = count($_FILES[$ma.'files']['name']);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES[$ma.'files']['name'][$i]) {									
                    #initialization new file data
					$_FILES[$ma.'files']['name']     = $file[$ma.'files']['name'][$i];
					$_FILES[$ma.'files']['type']     = $file[$ma.'files']['type'][$i];
					$_FILES[$ma.'files']['tmp_name'] = $file[$ma.'files']['tmp_name'][$i];
					$_FILES[$ma.'files']['error']    = $file[$ma.'files']['error'][$i];
					$_FILES[$ma.'files']['size']     = $file[$ma.'files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'tl_hop_'.$time.'_'.clear($_FILES[$ma.'files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload($ma.'files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_tlhop');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_tlhop');
		}
		
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_lichhopso');
		if($kiemtra>0)
		{
			return messagebox('Xóa lịch họp thành công!','info');
		}
				
	}
	
	public function layNgayTuan($week)
	{
		if(empty($week))
		{
			$week = date('W');
		}
		$year = date('Y');
		$bd   = 1;
		// $bd = mặc đinh bắt đầu là ngày thứ hai
		// 1 năm có 53 tuần
		// tuần hiện tại sẽ lấy các tham số mặc định
		// tuần trước sẽ kiểm tra tuần hiện tại có phải tuần đầu tiên của năm k
		// tuấn tiếp theo sẽ kiểm tra tuần hiện tại phải cuối năm k
		// ngày hiện tại $bd = ngày hiên tại( date("w") )
		if($week==53)
		{
			$year=$year+1;
			$week=1;
		}
		//$start_date ngày đầu tiên của tuần hiện tại
		if($week<10)
		{
			$so='0';
			for($i=$bd;$i<=7;$i++)
			{
				$start_date = strtotime( $year . "W".(int)$so. $week . $i);
				$ngaytuan[] = $this->sw_get_current_weekday($start_date,$week,$year,$i);

			}
			$ngatbatdau  = strtotime( $year . "W".(int)$so. $week . 1);
			$ngayketthuc = strtotime( $year . "W".(int)$so. $week . 7);
		}
		else{
			for($i=$bd;$i<=7;$i++)
			{
				$start_date = strtotime( $year . "W". $week . $i);
				$ngaytuan[] = $this->sw_get_current_weekday($start_date,$week,$year,$i);

			}
			$ngatbatdau  = strtotime( $year . "W". $week . 1);
			$ngayketthuc = strtotime( $year . "W". $week . 7);
		}
		return array($ngatbatdau,$ngayketthuc,$ngaytuan);
	}
	// hàm chuyển đổi các thứ tiếng anh sang tiếng việt 
	function sw_get_current_weekday($start_date,$week,$year,$i) {
	    date_default_timezone_set('Asia/Ho_Chi_Minh');
	    $weekday = date("l",$start_date);
	    $weekday = strtolower($weekday);
	    switch($weekday) {
	        case 'monday':
	            $weekday = 'Thứ hai';
	            break;
	        case 'tuesday':
	            $weekday = 'Thứ ba';
	            break;
	        case 'wednesday':
	            $weekday = 'Thứ tư';
	            break;
	        case 'thursday':
	            $weekday = 'Thứ năm';
	            break;
	        case 'friday':
	            $weekday = 'Thứ sáu';
	            break;
	        case 'saturday':
	            $weekday = 'Thứ bảy';
	            break;
	        default:
	            $weekday = 'Chủ nhật';
	            break;
	    }
	    if($week<10)
		{
			$so=0;
			return array($weekday,date('d/m/Y',strtotime( $year . "W".(int)$so. $week . $i)));
		}
		else{

		    return array($weekday,date('d/m/Y',strtotime( $year . "W". $week . $i)));
		}
	}
	
	function sapxep($mang) {
		$sophantu = count($mang); // hoặc dùng hàm $sophantu = count($mang);
  
		// Sắp xếp mảng
		for ($i = 0; $i < ($sophantu - 1); $i++)
		{
			for ($j = $i + 1; $j < $sophantu; $j++) // lặp các phần tử phía sau
			{
				if ((isset($mang[$i]['sGiayMoiGio']) && isset($mang[$j]['sGioMoi']) && $mang[$i]['sGiayMoiGio'] > $mang[$j]['sGioMoi']) || (isset($mang[$i]['sGioMoi']) && isset($mang[$j]['sGiayMoiGio']) && $mang[$i]['sGioMoi'] > $mang[$j]['sGiayMoiGio'])) // nếu phần tử $i bé hơn phần tử phía sau
				{
					// hoán vị
					$tmp = $mang[$j];
					$mang[$j] = $mang[$i];
					$mang[$i] = $tmp;
				}
			}
		}
		return $mang;
	   
   }

}

/* End of file Clichcongtac.php */
/* Location: ./application/controllers/vanbanden/Clichcongtac.php */