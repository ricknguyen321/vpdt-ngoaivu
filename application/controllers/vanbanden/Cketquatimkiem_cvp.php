<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cketquatimkiem_cvp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->library('pagination');
	}
	public function index()
	{
//        pr($mangVBDi);
        //trang thái là 3 là xóa văn bản
        if($this->input->post('xoavanban')){
            $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$this->input->post('xoavanban'),'tbl_vanbanden','iTrangThai','3');
        }
        /** @var danh sách tìm kiếm phân trang $page */
        $page                  = $this->PhanTrang();
        $data['getDocGo']      = $page['info'];
        if(!empty($data['getDocGo'])){
            foreach ($data['getDocGo'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $processgd = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processpb1 = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $vbdi = $this->Mvanbanchoxuly->layDuLieu('iSoDen',$value['iSoDen'],'tbl_vanbandi');
                    $data['getDocGo'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                    if(!empty($processgd)){
                        $data['getDocGo'][$key]['sChuTri'] = $processgd[0]['sMoTa'];
                    }
                    if(empty($processgd)){
                        $data['getDocGo'][$key]['sChuTri'] = '';
                    }
                    if(!empty($processpb1)){
                        $data['getDocGo'][$key]['sPhongChuTri'] = $processpb1[0]['sHoTen'];
                    }
                    if(empty($processpb1)){
                        $data['getDocGo'][$key]['sPhongChuTri'] = '';
                    }
                    if(!empty($vbdi)){
                        $data['getDocGo'][$key]['VBDi'] = $vbdi[0]['PK_iMaVBDi'];
                    }
//                pr($vbdi);
            }
        }
//         pr($data['getDocGo']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']    = $page['loaivanban'];
        $data['sokyhieu']      = $page['sokyhieu'];
        $data['ngaynhap']      = $page['ngaynhap'];
//        pr($data['ngaynhap']);
        $data['donvi']         = $page['donvi'];
        $data['ngayky']        = $page['ngayky'];
        $data['ngayden']       = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['ngaymoi']       = $page['ngaymoi'];
        $data['nguoiky']       = $page['nguoiky'];
        $data['soden']         = $page['soden'];
        $data['denngay']       = $page['denngay'];
        $data['chucvu']        = $page['chucvu'];
        $data['nguoinhap']     = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count']         = $page['count'];


		$data['title']    = 'Danh sách văn bản tìm kiếm';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vketquatimkiem_cvp';
		$this->load->view('layout_admin/layout',$temp);
	}
    public function PhanTrang()
    {
//        pr($this->_session);
//        pr($this->input->get('id'));
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu      = $this->input->get('sokyhieu');
        $ngaynhap      = ($this->input->get('ngaynhap') > '01/01/2017') ? date_insert($this->input->get('ngaynhap')) : NULL;
        $donvi         = $this->input->get('donvi');
        $ngayky        = ($this->input->get('ngayky') > '01/01/2017') ? date_insert($this->input->get('ngayky')) : NULL;
        $ngayden       = ($this->input->get('ngayden') > '01/01/2017') ? date_insert($this->input->get('ngayden')) : NULL;
//        pr($ngayden);
        $trichyeu      = $this->input->get('trichyeu');
        $ngaymoi       = ($this->input->get('ngaymoi') > '01/01/2017') ? date_insert($this->input->get('ngaymoi')) : NULL;
        $nguoiky       = $this->input->get('nguoiky');
        $soden         = $this->input->get('soden');
        $denngay       = ($this->input->get('denngay') > '01/01/2017') ? date_insert($this->input->get('denngay')) : NULL;
        $chucvu        = $this->input->get('chucvu');
        $nguoinhap     = $this->input->get('nguoinhap');
        $cacloaivanban = $this->input->get('cacloaivanban');
        $id = $this->input->get('id');
        $idDocument    = $this->uri->segment(2);
        $config['base_url']             = base_url().'dsvanbanden?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&ngaymoi='.$ngaymoi.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&cacloaivanban='.$cacloaivanban.'&id='.$id;
        $config['total_rows']           = $this->Mvanbanden->countDocGo($id,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban);
//        pr($config['total_rows']);
        $config['per_page']             = 25;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 18;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'dsvanbanden');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap']      = $ngaynhap;
        $data['donvi']         = $donvi;
        $data['ngayky']        = $ngayky;
        $data['ngayden']       = $ngayden;
        $data['trichyeu']      = $trichyeu;
        $data['ngaymoi']       = $ngaymoi;
        $data['nguoiky']       = $nguoiky;
        $data['soden']         = $soden;
        $data['denngay']       = $denngay;
        $data['chucvu']        = $chucvu;
        $data['nguoinhap']     = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['id'] = $id;
        $data['count']         = $config['total_rows'];
        $data['info']        = $this->Mvanbanden->getDocGo($id,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}

/* End of file Cketquatimkiem_cvp.php */
/* Location: ./application/controllers/vanbanden/Cketquatimkiem_cvp.php */