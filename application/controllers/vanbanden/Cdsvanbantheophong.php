<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/26/2017
 * Time: 3:02 PM
 */
class Cdsvanbantheophong extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
    }
    public function index()
    {
//        pr($this->_session);
        $data['process'] = $this->Mvanbanden->getDocProcess();
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mvanbanchoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        //danh sách đã xử lý
        $data['getDocAwait'] = $this->Mvanbanchoxuly->getDocComplete($this->_session['FK_iMaPhongHD']);

        // duyệt văn bản chờ xử lý
        $this->insertDocAwait();
        $data['title']    = 'Phòng phối hợp';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vdsvanbantheophong';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {

        if($this->input->post('duyetvanban')){
            foreach ($this->input->post('duyet') as $key5 => $value5){
                if(!empty($value5)){
                    $Deputy = $this->input->post('phogiamdoc');
                    $staff = $this->input->post('phongchutri');
                    $DepartmentPH = $this->input->post('mangphoihop');
                    $Doc_array= array();
                    if (!empty($Deputy[$key5])) {
                        $Doc_array['iTrangThai_TruyenNhan'] = '6';

                        /** @var mang chuyen van ban cho pho phòng $PDir_array */
                        $PDir_array= array();
                        if(!empty($Deputy[$key5])){
                            $PDir_array['PK_iMaCVCT'] = $Deputy[$key5];
                            $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                            $PDir_array['sThoiGian'] = date('Y-m-d H:s',time());
                            $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$key5];
                            $PDir_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
                            $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                            $PDir_array['CapGiaiQuyet'] = '6';
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
                        }

                    } else {
                        $Doc_array['iTrangThai_TruyenNhan'] = '7';

                        /** @var mang chuyen chuyen vien $Departmanet_array */
                        // van ban phoi hop giua cac chuyen vien vowi nhau
                        $Departmanet_array= array();
                        $mangph = $this->input->post('mangphoihop')[$key5];
                        $cvphoihop = explode( ',', $mangph );
//                        pr($cvphoihop);
                        if(!empty($DepartmentPH[$key5])){
                            foreach($cvphoihop as $key => $value){
                                $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                                $Departmanet_array['PK_iMaCVPH'] = $value;
                                $Departmanet_array['PK_iMaCVCT'] = $staff[$key5];
                                $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                                $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
                                $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$key5];
                                $Departmanet_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
                                $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                                $this->Mdanhmuc->themDuLieu('tbl_chuyenvienphoihop',$Departmanet_array);
                            }
//                            pr($Departmanet_array);
                        }
                        // chuyen vien chu tri ko phoi hop
                        $Departmanet_array1= array();
                        if(!empty($staff)){
                            $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVCT'] = $staff[$key5];
                            $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                            $Departmanet_array1['sThoiGian'] = date('Y-m-d H:s',time());
                            $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$key5];
                            $Departmanet_array1['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$key5]);
                            $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['CapGiaiQuyet'] = '7';
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array1);
                        }
                    }
//                        pr($Doc_array);
                    $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$this->input->post('doc_id')[$key5],'tbl_vanbanden',$Doc_array);

                }
            }
            return redirect(base_url().'vanbanden/Cvanbanchoxuly_tp');

        }
    }

}