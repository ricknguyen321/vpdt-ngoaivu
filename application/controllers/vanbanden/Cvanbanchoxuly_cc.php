<?php
/**
 * Created by PhpStorm.
 * User: MinhDuy
 * Date: 6/14/2017
 * Time: 3:56 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanchoxuly_cc extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {
		if($this->input->post('tuchoi')){
            $data['content'] = $this->Reject();
        }
        if($this->input->post('themhan')){
            $data['content'] = $this->insertDateline();
        }
//        pr($this->_session);
        // danh sách giám đốc
        $data['getAccountDirec'] = $this->Mvanbanchoxuly->getAccount('0','10');
        // danh sách phó giám đốc
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0','11');
        // danh sách phòng
//        $data['getDepartment'] = $this->Mvanbanchoxuly->getDepartment('0');
        // duyệt văn bản chờ xử lý
        if($this->input->post('duyet')) {
            $data['content'] =  $this->insertDocAwait();
        }
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $process = $this->Mvanbanchoxuly->laylanhdaosaucung($value['PK_iMaVBDen']);
                $chuyenlai = $this->Mvanbanden->layDuLieuCN('PK_iMaVBDen',$value['PK_iMaVBDen'],'tbl_vanban_chuyenlai');
                $phong = $this->Mvanbanden->layCBPhong('PK_iMaCB',$chuyenlai['PK_iMaCBTuChoi'],'tbl_canbo');
//                pr($phong);
                if($phong['FK_iMaPhongHD'] != $this->_session['FK_iMaPhongHD']){
                    $data['getDocAwait'][$key]['sLyDo'] = '';
                }else{
                    $data['getDocAwait'][$key]['sLyDo'] = $chuyenlai['sLyDo'];
                }
                if(!empty($duongdan))
                {
                    $data['getDocAwait'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $data['getDocAwait'][$key]['sDuongDan'] = '';
                }
				$data['getDocAwait'][$key]['ChiDao'] = $process['sHoTen'];
                $data['getDocAwait'][$key]['PK_iMaLDCD'] = $process['PK_iMaCB'];
				$data['getDocAwait'][$key]['QuyenHan'] = $process['iQuyenHan_DHNB'];
            }
        }
//        pr($hanxuly);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
//        pr($data['getDocAwait']);
        $data['title']    = 'Văn bản chờ xử lý';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vvanbanchoxuly_cc';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $taikhoan = $this->_session['PK_iMaCB'];
        $phophong                = _post('phogiamdoc')[$ma];
        $chuyenvien              = _post('phongchutri')[$ma];
        $noidungchuyenphophong   = _post('chidaophogiamdoc')[$ma];
        $noidungchuyenchuyenvien = _post('chidaophongchutri')[$ma];
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $ma,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => ($phophong)?$phophong:$chuyenvien,
            'sNoiDung'        => ($phophong)?$noidungchuyenphophong:$noidungchuyenchuyenvien,
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
            $process = $this->Mvanbanchoxuly->laylanhdaosaucung($ma);
            $PDirector = $this->input->post('phogiamdoc');
            $Deparment = $this->input->post('phongchutri');
            // đánh dấu văn bản có đầu ra
            $vbdaura_tp = $this->input->post('vbdaura_tp'.$ma);
            $vbdaura_tp = ($vbdaura_tp)?$vbdaura_tp:1;
            $Doc_array= array();

            $this->Mdanhmuc->xoaDuLieu('doc_id',$ma,'tbl_chicuc');
            if (!empty($PDirector[$ma])) {
                $Doc_array['iTrangThai_TruyenNhan'] = '6';
                $Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
                $Doc_array['vbdaura_tp'] = $vbdaura_tp;
            }  elseif (!empty($Deparment[$ma])) {
                $Doc_array['iTrangThai_TruyenNhan'] = '6';
                $Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
                $Doc_array['vbdaura_tp'] = $vbdaura_tp;
            }
//            pr($Doc_array);
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$ma,'tbl_vanbanden',$Doc_array);

            /** @var mang chuyen van ban cho pho chi cục $PDir_array */
            $PDir_array= array();
            if(!empty($Deparment[$ma]) && empty($PDirector[$ma])) {
                $PDir_array['doc_id'] = $this->input->post('doc_id')[$ma];
                $PDir_array['doc_giaymoi'] = '2';
                $PDir_array['department_id'] = $this->_session['FK_iMaPhongHD'];
                $PDir_array['ccp_id'] = $PDirector[$ma];
                $PDir_array['ccp_desc'] = $this->input->post('chidaophogiamdoc')[$ma];
                $PDir_array['tp_id'] = $Deparment[$ma];
                $PDir_array['tp_desc'] = $this->input->post('chidaophongchutri')[$ma];
                $PDir_array['ph_id'] = $this->input->post('mangphoihop')[$ma];
                $PDir_array['tcdn_active'] = '3';
                $PDir_array['tcdn_date'] = date('Y-m-d H:s', time());
                $PDir_array['hangiaiquyet'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
//                pr($PDir_array);
                $this->Mdanhmuc->themDuLieu('tbl_chicuc', $PDir_array);
                $mangchuyennhanvanban = array(
                    'PK_iMaCVCT'      => $Deparment[$ma],
                    'PK_iMaCBChuyen'  => $taikhoan,
                    'PK_iMaVBDen'     => $ma,
                    'sThoiGian'       => date('Y-m-d H:i:s'),
                    'sMoTa'           => $this->input->post('chidaophongchutri')[$ma],
                    'sThoiGianHetHan' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                    'input_per'       => $taikhoan,
                    'sVBQT'           => 0,
                    'CapGiaiQuyet'    => 0
                );
                $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$mangchuyennhanvanban);
//            return redirect(base_url().'vanbanchoxuly_cc');
            }else{
                $PDir_array['doc_id'] = $this->input->post('doc_id')[$ma];
                $PDir_array['doc_giaymoi'] = '2';
                $PDir_array['department_id'] = $this->_session['FK_iMaPhongHD'];
                $PDir_array['ccp_id'] = $PDirector[$ma];
                $PDir_array['ccp_desc'] = $this->input->post('chidaophogiamdoc')[$ma];
                $PDir_array['tp_id'] = $Deparment[$ma];
                $PDir_array['tp_desc'] = $this->input->post('chidaophongchutri')[$ma];
                $PDir_array['ph_id'] = $this->input->post('mangphoihop')[$ma];
                $PDir_array['tcdn_active'] = '2';
                $PDir_array['tcdn_date'] = date('Y-m-d H:s', time());
                $PDir_array['hangiaiquyet'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
//                pr($PDir_array);
                $this->Mdanhmuc->themDuLieu('tbl_chicuc', $PDir_array);
                $mangchuyennhanvanban = array(
                    'PK_iMaCVCT'      => $PDirector[$ma],
                    'PK_iMaCBChuyen'  => $taikhoan,
                    'PK_iMaVBDen'     => $ma,
                    'sThoiGian'       => date('Y-m-d H:i:s'),
                    'sMoTa'           => $this->input->post('chidaophogiamdoc')[$ma],
                    'sThoiGianHetHan' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                    'input_per'       => $taikhoan,
                    'sVBQT'           => 0,
                    'CapGiaiQuyet'    => 0
                );
                $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$mangchuyennhanvanban);
            }
        /// kế hoạch công tác
if($vbdaura_tp == 1){             
        if($this->_session['iQuyenHan_DHNB'] == 6){
            $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$ma,'tuan', date("W"),'phong_id',$this->_session['FK_iMaPhongHD'],'kehoach');
            if(!empty($PDirector[$ma])){
                $munber_week = $this->Mdanhmuc->getTuanMax($PDirector[$ma])['tuan'];
                $tuan_hientai = (int)date("W");
                if($munber_week == $tuan_hientai){
                    $cong = 1;
                }else{
                    $cong = 0;
                }
                $kehoach_data = array(
                    'vanban_id' => $this->input->post('doc_id')[$ma],
                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                    'vanban_skh' => $this->input->post('sohieu')[$ma],
                    'tuan' => (int)date("W")+$cong,
                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                    'canbo_id' => $PDirector[$ma],
                    'loai_kh' => 1,
					'chucvu'  => 2,
                    'user_input' => $this->_session['PK_iMaCB'],
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
                    'lanhdao_so' => $process['PK_iMaCB'],
                    'phong_id' => $this->_session['FK_iMaPhongHD']
                );
                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data);
            }
            if(!empty($Deparment[$ma])){
                $munber_week = $this->Mdanhmuc->getTuanMax($Deparment[$ma])['tuan'];
                $tuan_hientai = (int)date("W");
                if($munber_week == $tuan_hientai){
                    $cong = 1;
                }else{
                    $cong = 0;
                }
                $kehoach_data_cv = array(
                    'vanban_id' => $this->input->post('doc_id')[$ma],
                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                    'vanban_skh' => $this->input->post('sohieu')[$ma],
                    'tuan' => (int)date("W")+$cong,
                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                    'canbo_id' => $Deparment[$ma],
                    //'thuc_hien' => '3',
                    'loai_kh' => 1,
					'chucvu'  => 3,
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
                    'user_input' => $this->_session['PK_iMaCB'],
                    'lanhdao_so' => $process['PK_iMaCB'],
                    'phong_id' => $this->_session['FK_iMaPhongHD']
                );
                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);
            }
        }
}else{
    $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$ma,'tuan', date("W"),'phong_id',$this->_session['FK_iMaPhongHD'],'kehoach');
}
        }
		public function Reject(){
        $idDoc = $this->input->post('maphongban');
        $taikhoan    = $this->_session['PK_iMaCB'];
        $phongbanHD  =11;
        $quyenhan    = array(3);
        $chucvuphong = array(6);
        $quyendb     = '';
        $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $idDoc,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => $truongphong[0]['PK_iMaCB'],
            'sNoiDung'        => _post('noidungtuchoi'),
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
        $data_reject = array(
            'PK_iMaVBDen' => $idDoc,
            'PK_iMaCBTuChoi' => $this->_session['PK_iMaCB'],
            'sTenCB'   => $this->_session['sHoTen'],
            'sLyDo'    => $this->input->post('noidungtuchoi'),
            'sGhiChu'  => $this->input->post('ghichu'),
            'iTrangThai' => '1',
            'sThoiGianTao' => date('Y-m-d H:s',time())
        );
        $data_doc = array(
            'iTrangThai_TruyenNhan' => '2'
        );
        // xóa bản ghi trong bảng kế hoạch công tác nếu đã giao cho chuyên viên
        $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$idDoc,'tuan', date("W"),'phong_id',$this->_session['FK_iMaPhongHD'],'kehoach');
        
        $this->Mdanhmuc->themDuLieu('tbl_vanban_chuyenlai',$data_reject);
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
        if ($DocGo > 0) {
            return messagebox('Chuyền giấy mời về chánh văn phòng thành công', 'info');
        } else {
            return messagebox('Chuyền giấy mời về chánh văn phòng thất bại', 'danger');
        }
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'vanbanchoxuly_cc?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwait('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 200;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_cc');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwait('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function insertDateline(){
        $idDoc = $this->input->post('mavbd');
        $idLD = $this->input->post('ldgui');
        $data_han = array(
            'iLanhDao' => $idLD,
            'iTrangThai_ThemHan'   => 1,
            'sHanChoDuyet'    => date_insert($this->input->post('handexuat')),
            'sNoiDungGiaHan'    => '<span>'.$this->input->post('noidungtuchoi').'</span><br/>'.'(<b>'.$this->_session['sHoTen'].'</b>)'.'<br/>'
        );
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
        if ($DocGo > 0) {
            return messagebox('Chuyền hạn văn bản về lãnh đạo thành công', 'info');
        } else {
            return messagebox('Chuyền hạn văn bản về lãnh đạo thất bại', 'danger');
        }
    }

}