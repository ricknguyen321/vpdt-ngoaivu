<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/12/2017
 * Time: 1:28 PM
 */
class Cgiaymoi_tp extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mgiaymoi','Mgiaymoi');
        $this->Mgiaymoi = new Mgiaymoi();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {
//        $data = $this->Mgiaymoichoxuly->checkCBHop(115,'09:00','2017-07-29');
//        pr($data);
        $action = $this->input->post('action');
        switch ($action) {
            case 'checkCBHop':
                $this->checkCBHop();
                break;
            default:
                # code...
                break;
        }
        if($this->input->post('tuchoi')){
            $data['content'] = $this->Reject();
        }
        // danh sách phó phòng
        $taikhoan   = $this->_session['PK_iMaCB'];
        $phongbanHD = $this->_session['FK_iMaPhongHD'];
        $quyendb    = $this->_session['iQuyenDB'];
        $chucvu     = $this->_session['FK_iMaCV'];
        $quyen      = $this->_session['iQuyenHan_DHNB'];
        if($phongbanHD==12)
        {
            $chucvuphong    = '';
            $quyendb        = $quyendb;
        }
        else{
            $chucvuphong    = '';
            $quyendb        = '';
        }
        $data['getAccountDeputyDirector'] = $this->Mvanbandi->layLD_Phong($phongbanHD,7,$chucvuphong,$quyendb);
        $data['getDepartment'] = $this->Mvanbandi->layLD_Phong($phongbanHD,8,$chucvuphong,$quyendb);
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $process = $this->Mgiaymoichoxuly->laylanhdaosaucung($value['PK_iMaVBDen']);
                if(!empty($duongdan))
                {
                    $data['getDocAwait'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $data['getDocAwait'][$key]['sDuongDan'] = '';
                }
                $data['getDocAwait'][$key]['ChiDao'] = $process['sHoTen'];
                $data['getDocAwait'][$key]['QuyenHan'] = $process['iQuyenHan_DHNB'];
            }
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['count'] = $page['count'];

        // duyệt văn bản chờ xử lý
//        $this->insertDocAwait();
        if($this->input->post('duyet')) {
            $this->insertDocAwait();
        }
        $data['title']    = 'Trưởng phòng chỉ đạo';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoi_tp';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function Reject(){
        $idDoc       = $this->input->post('maphongban');
        $taikhoan    = $this->_session['PK_iMaCB'];
        $phongbanHD  =11;
        $quyenhan    = array(3);
        $chucvuphong = array(6);
        $quyendb     = '';
        $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $idDoc,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => $truongphong[0]['PK_iMaCB'],
            'sNoiDung'        => _post('noidungtuchoi'),
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);

        $data_reject = array(
            'PK_iMaVBDen' => $idDoc,
            'PK_iMaCBTuChoi' => $this->_session['PK_iMaCB'],
            'sTenCB'   => $this->_session['sHoTen'],
            'sLyDo'    => $this->input->post('noidungtuchoi'),
            'sGhiChu'  => $this->input->post('ghichu'),
			'PK_iMaPB' => $this->_session['FK_iMaPhongHD'],
            'iTrangThai' => '1',
            'sThoiGianTao' => date('Y-m-d H:i:s',time())
        );
        $data_doc = array(
            'iTrangThai_TruyenNhan' => '2'
        );

        // xóa bản ghi trong bảng kế hoạch công tác nếu đã giao cho chuyên viên
        $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$idDoc,'tuan', date("W"),'phong_id',$this->_session['FK_iMaPhongHD'],'kehoach');
        
        $this->Mdanhmuc->themDuLieu('tbl_vanban_chuyenlai',$data_reject);
		$this->Mdanhmuc->xoaDuLieu('vanban_id',$idDoc,'kehoach');
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
        if ($DocGo > 0) {
            return messagebox('Chuyền giấy mời về chánh văn phòng thành công', 'info');
        } else {
            return messagebox('Chuyền giấy mời về chánh văn phòng thất bại', 'danger');
        }
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $taikhoan = $this->_session['PK_iMaCB'];
        $phophong                = _post('phogiamdoc')[$ma];
        $chuyenvien              = _post('phongchutri')[$ma];
        $noidungchuyenphophong   = _post('chidaophogiamdoc')[$ma];
        $noidungchuyenchuyenvien = _post('chidaophongchutri')[$ma];
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $ma,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => ($phophong)?$phophong:$chuyenvien,
            'sNoiDung'        => ($phophong)?$noidungchuyenphophong:$noidungchuyenchuyenvien,
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
        $process = $this->Mvanbanchoxuly->laylanhdaosaucung($ma);
//        if($this->input->post('duyetvanban')){
//            foreach ($this->input->post('duyet') as $ma => $value5){
//                if(!empty($value5)){
                    // anh xa pho phong
                    $Deputy = $this->input->post('phogiamdoc');
                    $DeputyPH = $this->input->post('mangphoihoppp');
                    // anh xa phong chu tri
                    $staff = $this->input->post('phongchutri');
                    $DepartmentPH = $this->input->post('mangphoihop');
                    $Doc_array= array();
                    if (!empty($Deputy[$ma])) {
                        $Doc_array['iTrangThai_TruyenNhan'] = '6';
						$Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
						$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$this->input->post('doc_id')[$ma],'tbl_vanbanden',$Doc_array);
                    } elseif(!empty($staff[$ma])) {
                        $Doc_array['iTrangThai_TruyenNhan'] = '7';
						$Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
						$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$this->input->post('doc_id')[$ma],'tbl_vanbanden',$Doc_array);
                    }
                    // chi cuc
                    $Doc_arraycc= array();
                    if($this->_session['iQuyenHan_DHNB'] == 11){
                        $Doc_arraycc['tcdn_active'] = '4';
                        $this->Mdanhmuc->capnhatDuLieu('doc_id',$this->input->post('doc_id')[$ma],'tbl_chicuc',$Doc_arraycc);
                    }
					$this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'CapGiaiQuyet', 7,'tbl_chuyennhanvanban');
                    $this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'CapGiaiQuyet', 8,'tbl_chuyennhanvanban');
                    $this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'PK_iMaPhong', $this->_session['FK_iMaPhongHD'],'tbl_phongphoihop');
                    if (!empty($Deputy[$ma])) {

                        /** @var mang chuyen van ban cho pho phòng $PDir_array */
                        $PDir_array= array();
                        if(!empty($Deputy[$ma])){
                            $this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'PK_iMaCVCT', $Deputy[$ma],'tbl_chuyennhanvanban');
                            $PDir_array['PK_iMaCVCT'] = $Deputy[$ma];
                            $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                            $PDir_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                            $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$ma];
                            $PDir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                            $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                            $PDir_array['CapGiaiQuyet'] = '7';
							$PDir_array['PK_iMaCVPH'] = $this->input->post('mangphoihoppp')[$ma];
                            $PDir_array['iGiayMoi'] = 1;
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
                        }

                    }
                    if(!empty($staff[$ma])) {
                        /** @var mang chuyen chuyen vien $Departmanet_array */
                        // van ban phoi hop giua cac chuyen vien vowi nhau
                        $Departmanet_array= array();
                        $mangph = $this->input->post('mangphoihop')[$ma];
                        $cvphoihop = explode( ',', $mangph );
//                        pr($cvphoihop);
                        // chuyen vien chu tri ko phoi hop
                        $Departmanet_array1= array();
                        if(!empty($staff[$ma])){
                            $this->Mgiaymoichoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'PK_iMaCVCT', $staff[$ma],'tbl_chuyennhanvanban');
                            $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVCT'] = $staff[$ma];
                            $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                            $Departmanet_array1['sThoiGian'] = date('Y-m-d H:i:s',time());
                            $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                            $Departmanet_array1['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                            $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVPH'] = $this->input->post('mangphoihop')[$ma];
                            $Departmanet_array1['CapGiaiQuyet'] = '8';
                            $Departmanet_array1['iGiayMoi'] = 1;
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array1);
                        }
//                            pr($Departmanet_array1);
                        if(!empty($DepartmentPH[$ma])){
                            foreach($cvphoihop as $key => $value){
                                $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                                $Departmanet_array['PK_iMaCB'] = $value;
                                $Departmanet_array['PK_iMaCVCT'] = $staff[$ma];
                                $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                                $Departmanet_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                                $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                                $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                                $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                                $Departmanet_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                                $Departmanet_array['iGiayMoi'] = '1';
                                $this->Mdanhmuc->themDuLieu('tbl_phongphoihop',$Departmanet_array);
                            }
                        }
                    }
        if(!empty($DeputyPH[$ma])) {
            $DeputyPH_array = array();
            $mangppph = $this->input->post('mangphoihoppp')[$ma];
            $ppphoihop = explode(',', $mangppph);
            foreach ($ppphoihop as $key1 => $value1) {
                $DeputyPH_array['PK_iMaCB'] = $value1;
                $DeputyPH_array['PK_iMaPPPH'] = '2';
                $DeputyPH_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                $DeputyPH_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                $DeputyPH_array['sThoiGian'] = date('Y-m-d H:i:s', time());
                $DeputyPH_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$ma];
                $DeputyPH_array['sThoiGianHetHan'] = (_post('hangiaiquyet') == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                $DeputyPH_array['input_per'] = $this->_session['PK_iMaCB'];
                $DeputyPH_array['iTrangThai_PPPH'] = 1;
                // phó phòng phối hợp là 2
                $DeputyPH_array['iTrangThai'] = '2';
                $this->Mdanhmuc->themDuLieu('tbl_phongphoihop', $DeputyPH_array);
            }
        }
        /// kế hoạch công tác
        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3 || $this->_session['iQuyenHan_DHNB'] == 11){
            if($this->_session['iQuyenHan_DHNB'] != 11 ){
                $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$ma,'tuan', date("W"),'phong_id',$this->_session['FK_iMaPhongHD'],'kehoach');
            }
            $giahan = dateFromBusinessDays(1,_post('ngaymoi')[$ma]);
            if(!empty($Deputy[$ma])){
                $munber_week = $this->Mdanhmuc->getTuanMax($Deputy[$ma])['tuan'];
                $tuan_hientai = (int)date("W");
                if($munber_week == $tuan_hientai){
                    $cong = 1;
                }else{
                    $cong = 0;
                }
                $kehoach_data = array(
                    'vanban_id' => $this->input->post('doc_id')[$ma],
                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
					'vanban_skh' => $this->input->post('sohieu')[$ma],
                    'tuan' => (int)date("W")+$cong,
                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? $giahan : date_insert(_post('hangiaiquyet')[$ma]),
                    'canbo_id' => $Deputy[$ma],
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
					'loai_kh' => 1,
                    'chucvu'  => 7,
                    'trangthai_vbd' => 1,
					'user_input' => $this->_session['PK_iMaCB'],
                    'lanhdao_so' => $process['PK_iMaCB'],
                    'phong_id' => $this->_session['FK_iMaPhongHD']
                );
//                        pr($kehoach_data);
                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data);

                //phó phối hợp
                /*if(!empty($DeputyPH[$ma])) {
                    $DeputyPH_array = array();
                    $mangppph = $this->input->post('mangphoihoppp')[$ma];
                    $ppphoihop = explode(',', $mangppph);
                    foreach ($ppphoihop as $key1 => $value1) {
                        $kehoach_data_ph = array(
                            'vanban_id' => $this->input->post('doc_id')[$ma],
                            'kh_noidung' => $this->input->post('noidungvb')[$ma],
                            'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
							'vanban_skh' => $this->input->post('sohieu')[$ma],
                            'tuan' => date("W"),
                            'thuc_hien' => '2',
							'loai_kh' => 1,
                            'trangthai_vbd' => 1,
                            'chucvu'  => 7,
                            'ngay_nhan' => date('Y-m-d H:i:s',time()),
                            'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? $giahan : date_insert(_post('hangiaiquyet')[$ma]),
                            'canbo_id' => $value1,
                            'lanhdao_id' => $this->_session['PK_iMaCB'],
							'user_input' => $this->_session['PK_iMaCB'],
                            'lanhdao_so' => $process['PK_iMaCB'],
                            'phong_id' => $this->_session['FK_iMaPhongHD']
                        );

                        $this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_ph);
                    }
                }*/
            }else{
                $munber_week = $this->Mdanhmuc->getTuanMax($staff[$ma])['tuan'];
                $tuan_hientai = (int)date("W");
                if($munber_week == $tuan_hientai){
                    $cong = 1;
                }else{
                    $cong = 0;
                }
                $kehoach_data_cv = array(
                    'vanban_id' => $this->input->post('doc_id')[$ma],
                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
					'vanban_skh' => $this->input->post('sohieu')[$ma],
                    'tuan' => (int)date("W")+$cong,
                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? $giahan : date_insert(_post('hangiaiquyet')[$ma]),
                    'canbo_id' => $staff[$ma],
                    'thuc_hien' => '3',
					'loai_kh' => 1,
                    'trangthai_vbd' => 1,
                    'chucvu'  => 8,
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
					'user_input' => $this->_session['PK_iMaCB'],
                    'lanhdao_so' => $process['PK_iMaCB'],
                    'phong_id' => $this->_session['FK_iMaPhongHD']
                );
//                        pr($kehoach_data);
                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);

                //phó phối hợp
                /*if(!empty($DepartmentPH[$ma])){
                    $mangph = $this->input->post('mangphoihop')[$ma];
                    $cvphoihop = explode( ',', $mangph );
                    foreach($cvphoihop as $key => $value){
                        $kehoach_data_cvph = array(
                            'vanban_id' => $this->input->post('doc_id')[$ma],
                            'kh_noidung' => $this->input->post('noidungvb')[$ma],
                            'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
							'vanban_skh' => $this->input->post('sohieu')[$ma],
                            'tuan' => date("W"),
                            'thuc_hien' => '2',
							'loai_kh' => 1,
                            'trangthai_vbd' => 1,
                            'chucvu'  => 8,
                            'ngay_nhan' => date('Y-m-d H:i:s',time()),
                            'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? $giahan : date_insert(_post('hangiaiquyet')[$ma]),
                            'canbo_id' => $value,
                            'lanhdao_id' => $this->_session['PK_iMaCB'],
							'user_input' => $this->_session['PK_iMaCB'],
                            'lanhdao_so' => $process['PK_iMaCB'],
                            'phong_id' => $this->_session['FK_iMaPhongHD']
                        );

                        $this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_cvph);
                    }
                }*/
            }
        }
		return redirect(base_url().'giaymoi_tp');
    }
    public function PhanTrang()
    {
        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
            $stt = '5';
            $loaivanban    = $this->input->get('loaivanban');
            $sokyhieu = $this->input->get('sokyhieu');
            $ngaynhap      = $this->input->get('ngaynhap');
            $donvi    = $this->input->get('donvi');
            $ngayky    = $this->input->get('ngayky');
            $ngayden = $this->input->get('ngayden');
            $trichyeu = $this->input->get('trichyeu');
            $nguoiky = $this->input->get('nguoiky');
            $soden = $this->input->get('soden');
            $denngay = $this->input->get('denngay');
//        pr($this->input->get('denngay'));
            $chucvu = $this->input->get('chucvu');
            $nguoinhap = $this->input->get('nguoinhap');
            $config['base_url']             = base_url().'giaymoi_tp?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
            $config['total_rows']           = count($this->Mgiaymoichoxuly->getDocAwait1($stt,NULL,$this->_session['FK_iMaPhongHD'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
            $config['per_page']             = 10;
            $config['page_query_string']    = TRUE;
            $config['query_string_segment'] = 'page';
            $config['num_links']            = 9;
            $config['use_page_numbers']     = false;
            $config['full_tag_open']        = '<ul class="pagination">';
            $config['full_tag_close']       = '</ul>';
            $config['first_link']           = '&laquo';
            $config['last_link']            = '&raquo';
            $config['first_tag_open']       = '<li>';
            $config['first_tag_close']      = '</li>';
            $config['prev_link']            = '&lt';
            $config['prev_tag_open']        = '<li class="prev">';
            $config['prev_tag_close']       = '</li>';
            $config['next_link']            = '&gt;';
            $config['next_tag_open']        = '<li>';
            $config['next_tag_close']       = '</li>';
            $config['last_tag_open']        = '<li>';
            $config['last_tag_close']       = '</li>';
            $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
            $config['cur_tag_close']        = '</a></li>';
            $config['num_tag_open']         = '<li>';
            $config['num_tag_close']        = '</li>';

            $this->pagination->initialize($config);

            $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

            if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
                redirect(base_url().'giaymoi_tp');
            }
            $data['loaivanban']    = $loaivanban;
            $data['sokyhieu']      = $sokyhieu;
            $data['ngaynhap'] = $ngaynhap;
            $data['donvi'] = $donvi;
            $data['ngayky'] = $ngayky;
            $data['ngayden'] = $ngayden;
            $data['trichyeu'] = $trichyeu;
            $data['nguoiky'] = $nguoiky;
            $data['soden'] = $soden;
            $data['denngay'] =$denngay;
            $data['chucvu'] = $chucvu;
            $data['nguoinhap'] = $nguoinhap;
            $data['count']    = $config['total_rows'];
            $data['info']        = $this->Mgiaymoichoxuly->getDocAwait1($stt,NULL,$this->_session['FK_iMaPhongHD'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
            $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
            return $data;
        }else{
            $stt = '6';
            $loaivanban    = $this->input->get('loaivanban');
            $sokyhieu = $this->input->get('sokyhieu');
            $ngaynhap      = $this->input->get('ngaynhap');
            $donvi    = $this->input->get('donvi');
            $ngayky    = $this->input->get('ngayky');
            $ngayden = $this->input->get('ngayden');
            $trichyeu = $this->input->get('trichyeu');
            $nguoiky = $this->input->get('nguoiky');
            $soden = $this->input->get('soden');
            $denngay = $this->input->get('denngay');
//        pr($this->input->get('denngay'));
            $chucvu = $this->input->get('chucvu');
            $nguoinhap = $this->input->get('nguoinhap');
            $config['base_url']             = base_url().'giaymoi_tp?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
            $config['total_rows']           = count($this->Mgiaymoichoxuly->getDocConcacAwait('6','3',NULL,$this->_session['PK_iMaCB'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
            $config['per_page']             = 200;
            $config['page_query_string']    = TRUE;
            $config['query_string_segment'] = 'page';
            $config['num_links']            = 9;
            $config['use_page_numbers']     = false;
            $config['full_tag_open']        = '<ul class="pagination">';
            $config['full_tag_close']       = '</ul>';
            $config['first_link']           = '&laquo';
            $config['last_link']            = '&raquo';
            $config['first_tag_open']       = '<li>';
            $config['first_tag_close']      = '</li>';
            $config['prev_link']            = '&lt';
            $config['prev_tag_open']        = '<li class="prev">';
            $config['prev_tag_close']       = '</li>';
            $config['next_link']            = '&gt;';
            $config['next_tag_open']        = '<li>';
            $config['next_tag_close']       = '</li>';
            $config['last_tag_open']        = '<li>';
            $config['last_tag_close']       = '</li>';
            $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
            $config['cur_tag_close']        = '</a></li>';
            $config['num_tag_open']         = '<li>';
            $config['num_tag_close']        = '</li>';

            $this->pagination->initialize($config);

            $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

            if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
                redirect(base_url().'giaymoi_tp');
            }
            $data['loaivanban']    = $loaivanban;
            $data['sokyhieu']      = $sokyhieu;
            $data['ngaynhap'] = $ngaynhap;
            $data['donvi'] = $donvi;
            $data['ngayky'] = $ngayky;
            $data['ngayden'] = $ngayden;
            $data['trichyeu'] = $trichyeu;
            $data['nguoiky'] = $nguoiky;
            $data['soden'] = $soden;
            $data['denngay'] =$denngay;
            $data['chucvu'] = $chucvu;
            $data['nguoinhap'] = $nguoinhap;
            $data['count']    = $config['total_rows'];
            $data['info']        = $this->Mgiaymoichoxuly->getDocConcacAwait('6','3',NULL,$this->_session['PK_iMaCB'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
            $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
            return $data;
        }
    }
    public function checkCBHop(){
        $idCB = $this->input->post('idCB');
        $giohop = $this->input->post('giohop');
        $ngayhop = $this->input->post('ngayhop');
        $data = $this->Mgiaymoichoxuly->checkCBHop($idCB,$giohop,$ngayhop);
//        pr($data);
        echo json_encode($data);
        exit();
    }

}