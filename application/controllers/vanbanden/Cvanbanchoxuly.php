<?php
/**
 * Created by PhpStorm.
 * User: MinhDuy
 * Date: 6/14/2017
 * Time: 3:56 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanchoxuly extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {

        $action = _post('action');
        switch ($action) {
            case 'getDataDocGo':
                $this->getDataDocGo();
                break;
			case 'layNgayHetHan':
				$this->layNgayHetHan();
				break;
			default:
                # code...
                break;
        }
        if($this->input->post('luunoidung')){
            $data['content'] =  $this->insertDocGo();
        }
//        pr($this->_session);
        // danh sách giám đốc
        $data['getAccountDirec'] = $this->Mvanbanchoxuly->getAccount('0','4');
        // danh sách phó giám đốc
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0','5');
        // danh sách phòng
        $data['getDepartment'] = $this->Mvanbanchoxuly->getDepartment('0');
//        pr($data['getDepartment']);
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
		$data['dsquytrinh'] = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_quytrinhiso');
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
				$chuyenlai = $this->Mvanbanden->layDuLieuCN('PK_iMaVBDen',$value['PK_iMaVBDen'],'tbl_vanban_chuyenlai');
				$PhongCL = $this->Mvanbanchoxuly->getPhongBan($chuyenlai['PK_iMaCBTuChoi']);
                if(!empty($duongdan))
                {
                    $data['getDocAwait'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $data['getDocAwait'][$key]['sDuongDan'] = '';
                }
				if(!empty($value['PK_iMaCBTuChoi'])){
                    $data['getDocAwait'][$key]['PhongChuyenLai'] = $PhongCL;
                }
				if(!empty($chuyenlai['sLyDo'])) {
                    $data['getDocAwait'][$key]['sLyDo'] = $chuyenlai['sLyDo'];
                }
            }
        }
//        pr($page['info']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
		$data['pagei'] = $this->input->get('page');
//        pr($data['getDocAwait']);
        // duyệt văn bản chờ xử lý
        $this->insertDocAwait();
        $data['title']    = 'Văn bản chưa phân loại';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vvanbanchoxuly';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $taikhoan = $this->_session['PK_iMaCB'];
        if($this->input->post('duyetvanban')){ 
            $Director = $this->input->post('giamdoc');
            $PDirector = $this->input->post('phogiamdoc');
            $Deparment = $this->input->post('phongchutri');
            $Doc_array= array();// pr($this->input->post('doc_id'));
            $i = 0;
            foreach($this->input->post('doc_id') as $key4 => $value4 ){
				if(!empty($Director[$key4]) || !empty($PDirector[$key4]) || !empty($Deparment[$key4]))
                {
                    $sKy_Hieu   = _post('sKy_Hieu_'.$value4);
                    $sNgay_Ky   = _post('sNgay_Ky_'.$value4);
                    $sMaCoQuan  = _post('sMaCoQuan_'.$value4);
                    $sTenCoQuan = _post('sTenCoQuan_'.$value4);
                    if(!empty($sMaCoQuan)){
                        $this->createXML_PH($sKy_Hieu,$sNgay_Ky,$sMaCoQuan,$sTenCoQuan);
                        $this->sendMail($sKy_Hieu,$sNgay_Ky);
                    }
                }
                /** xóa chuyền nhận văn bản để trách trường hợp còn quy trình chuyền nhận */
                $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDen',$value4,'tbl_chuyennhanvanban');
                if (!empty($Director[$key4])) {
                    $Doc_array[$i]['iTrangThai_TruyenNhan'] = '3';
                } elseif (!empty($PDirector[$key4])) {
                    $Doc_array[$i]['iTrangThai_TruyenNhan'] = '4';
                }  elseif (!empty($Deparment[$key4])) {
                    $Doc_array[$i]['iTrangThai_TruyenNhan'] = '5';                    
                }
				$Doc_array[$i]['PK_iMaVBDen'] = $value4;
                $Doc_array[$i]['sHanThongKe'] = date_insert(_post('hangiaiquyet')[$value4]);
				$Doc_array[$i]['iDeadline'] = (_post('iDeadline_'.$value4)) ? 1 : NULL;
				$Doc_array[$i]['iSTCChuTri'] = (_post('iSTCChuTri_'.$value4)) ? 1 : 0;
				$Doc_array[$i]['iVanBanTBKL'] = (_post('iVanBanTBKL_'.$value4)) ? 1 : 0;
				$Doc_array[$i]['iToCongTac'] = (_post('iToCongTac_'.$value4)) ? 1 : 0;
				$Doc_array[$i]['iSTCPhoiHop'] = (_post('iSTCPhoiHop_'.$value4)) ? 1 : 0;
			
				$i++;
				
				//
            }
            //pr($Doc_array);
            /**  Cập nhật lại trạng thái khi chuyển đến từng quyền */
            $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_vanbanden',$Doc_array,'PK_iMaVBDen');
            /** @var mang chuyen van ban cho giam doc $Dir_array */
            $Dir_array= array();
            foreach($Director as $key1 => $value1 ){
                if(!empty($value1)){
                    $manggiamdoc = array(
                        'FK_iMaVBDen'     => $this->input->post('doc_id')[$key1],
                        'FK_iMaCB_Chuyen' => $taikhoan,
                        'FK_iMaCB_Nhan'   => $value1,
                        'sNoiDung'        => nl2br($this->input->post('chidaogiamdoc')[$key1]),
                        'sThoiGian'       => date('Y-m-d H:i:s')
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$manggiamdoc);

                    $Dir_array['PK_iMaCBNhan'] = $value1;
                    $Dir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                    $Dir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key1];
                    $Dir_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                    $Dir_array['sMoTa'] = nl2br($this->input->post('chidaogiamdoc')[$key1]);
                    $Dir_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$this->input->post('doc_id')[$key1]]);
                    $Dir_array['input_per'] = $this->_session['PK_iMaCB'];
                    $Dir_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key1]) ? $this->input->post('vanbanquantrong')[$key1] : 0;
                    $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Dir_array);
                }
            }

            /** @var mang chuyen van ban cho pho giam doc $PDir_array */
            $PDir_array= array();
            foreach($PDirector as $key2 => $value2 ){
                if(!empty($value2)){
                    $mangphogiamdoc = array(
                        'FK_iMaVBDen'     => $this->input->post('doc_id')[$key2],
                        'FK_iMaCB_Chuyen' => $taikhoan,
                        'FK_iMaCB_Nhan'   => $value2,
                        'sNoiDung'        => nl2br($this->input->post('chidaophogiamdoc')[$key2]),
                        'sThoiGian'       => date('Y-m-d H:i:s')
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangphogiamdoc);

                    $PDir_array['PK_iMaCBNhan'] = $value2;
                    $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                    $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key2];
                    $PDir_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                    $PDir_array['sMoTa'] = nl2br($this->input->post('chidaophogiamdoc')[$key2]);
                    $PDir_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$this->input->post('doc_id')[$key2]]);
                    $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                    $PDir_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key2]) ? $this->input->post('vanbanquantrong')[$key2] : 0;
                    $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
                }
            }

            /** @var mang chuyen phong chu tri va phoi hop $Departmanet_array */
            $Departmanet_array= array();
            foreach($Deparment as $key3 => $value3 ){
                if(!empty($value3)){
                    $phongbanHD  = $value3;
                    if($phongbanHD==11) {
                        $quyenhan       = array(3);
                        $chucvuphong    = array(6);
                        $quyendb        = '';
                    }
                    else{
                        $quyenhan       = array(6);
                        $chucvuphong    = '';
                        $quyendb        = '';
                    }
                    $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
                    $mangphongban = array(
                        'FK_iMaVBDen'     => $this->input->post('doc_id')[$key3],
                        'FK_iMaCB_Chuyen' => $taikhoan,
                        'FK_iMaCB_Nhan'   => $truongphong[0]['PK_iMaCB'],
                        'sNoiDung'        => nl2br($this->input->post('chidaophongchutri')[$key3]),
                        'sThoiGian'       => date('Y-m-d H:i:s')
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangphongban);

                    $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                    $Departmanet_array['PK_iMaPhongPH'] = $this->input->post('mangphoihop')[$key3];
                    $Departmanet_array['PK_iMaPhongCT'] = $value3;
                    $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key3];
                    $Departmanet_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                    $Departmanet_array['sMoTa'] = nl2br($this->input->post('chidaophongchutri')[$key3]);
                    $Departmanet_array['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$this->input->post('doc_id')[$key3]]);
                    $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                    $Departmanet_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key3]) ? $this->input->post('vanbanquantrong')[$key3] : 0;
                    $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array);
                }
            }


            return redirect(base_url().'vanbanchoxuly');

        }
    }
	public function createXML_PH($sKy_Hieu,$sNgay_Ky,$sMaCoQuan,$sTenCoQuan)
    {
        $dir      = 'tmpxml/';
        $xml      = new DOMDocument("1.0",'UTF-8');

        $STRMADONVI = $xml->createElement('STRMADONVI');
        $STRMADONVI_Text = $xml->createTextNode('000.00.13.H26');
        $STRMADONVI->appendChild($STRMADONVI_Text);

        $STRTENDONVI = $xml->createElement('STRTENDONVI');
        $STRTENDONVI_Text = $xml->createTextNode('Sở Tài Chính');
        $STRTENDONVI->appendChild($STRTENDONVI_Text);

        $STRDIACHI = $xml->createElement('STRDIACHI');

        $STRDIENTHOAI = $xml->createElement('STRDIENTHOAI');

        $STREMAIL = $xml->createElement('STREMAIL');

        $STRWEBSITE = $xml->createElement('STRWEBSITE');

        $STRFAX = $xml->createElement('STRFAX');
        $STRSOKYHIEU = $xml->createElement('STRSOKYHIEU');
        $STRSOKYHIEU_Text = $xml->createTextNode($sKy_Hieu);
        $STRSOKYHIEU->appendChild($STRSOKYHIEU_Text);

        $STRNGAYKY = $xml->createElement('STRNGAYKY');
        $STRNGAYKY_Text = $xml->createTextNode($sNgay_Ky);
        $STRNGAYKY->appendChild($STRNGAYKY_Text);

        $STRMACOQUANBANHANH = $xml->createElement('STRMACOQUANBANHANH');
        $STRMACOQUANBANHANH_Text = $xml->createTextNode($sMaCoQuan);
        $STRMACOQUANBANHANH->appendChild($STRMACOQUANBANHANH_Text);

        $STRCOQUANBANHANH = $xml->createElement('STRCOQUANBANHANH');
        $STRCOQUANBANHANH_Text = $xml->createTextNode($sTenCoQuan);
        $STRCOQUANBANHANH->appendChild($STRCOQUANBANHANH_Text);

        $STRDONVIXULY = $xml->createElement('STRDONVIXULY');
        $STRDONVIXULY_Text = $xml->createTextNode('Chánh văn phòng');
        $STRDONVIXULY->appendChild($STRDONVIXULY_Text);

        $STRCANBOXULY = $xml->createElement('STRCANBOXULY');

        $STRDIENTHOAI_XL = $xml->createElement('STRDIENTHOAI');

        $STREMAIL_XL = $xml->createElement('STREMAIL');

        $STRMATRANGTHAI = $xml->createElement('STRMATRANGTHAI');
        $STRMATRANGTHAI_Text = $xml->createTextNode('2');
        $STRMATRANGTHAI->appendChild($STRMATRANGTHAI_Text);

        $STRNOIDUNGXULY = $xml->createElement('STRNOIDUNGXULY');
        $STRNOIDUNGXULY_Text = $xml->createTextNode('Đang phân công xử lý');
        $STRNOIDUNGXULY->appendChild($STRNOIDUNGXULY_Text);

        
        $book   = $xml->createElement("EXPORTSTATUS");
        $NOIGUI     = $xml->createElement("NOIGUI");
        $NOIGUI->appendChild($STRMADONVI);
        $NOIGUI->appendChild($STRTENDONVI);
        $NOIGUI->appendChild($STRDIACHI);
        $NOIGUI->appendChild($STRDIENTHOAI);
        $NOIGUI->appendChild($STREMAIL);
        $NOIGUI->appendChild($STRWEBSITE);
        $NOIGUI->appendChild($STRFAX);

        $PHANHOIVANBAN = $xml->createElement('PHANHOIVANBAN');
        $PHANHOIVANBAN->appendChild($STRSOKYHIEU);
        $PHANHOIVANBAN->appendChild($STRNGAYKY);
        $PHANHOIVANBAN->appendChild($STRMACOQUANBANHANH);
        $PHANHOIVANBAN->appendChild($STRCOQUANBANHANH);

        $THONGTINXULY   = $xml->createElement("THONGTINXULY");
        $THONGTINXULY->appendChild($STRDONVIXULY);
        $THONGTINXULY->appendChild($STRCANBOXULY);
        $THONGTINXULY->appendChild($STRDIENTHOAI_XL);
        $THONGTINXULY->appendChild($STREMAIL_XL);

        $book->appendChild($NOIGUI);
        $book->appendChild($PHANHOIVANBAN);
        $book->appendChild($THONGTINXULY);
        $book->appendChild($STRMATRANGTHAI);
        $book->appendChild($STRNOIDUNGXULY);
        
        $xml->appendChild($book);
        $xml->formatOutput = true;
        $xml->save($dir.'XML_Output.sdk') or die("Error");
    }
    public function sendMail($sKy_Hieu,$sNgay_Ky)
    {
        $kyhieu = $sKy_Hieu;
        $ngayky = $sNgay_Ky;
        $name       = 'Văn thư Sở Tài chính';
        $from_email = 'vanthu_sotc@hanoi.gov.vn';
        $subject    = 'Phản hồi trạng thái văn bản ('.$kyhieu.' ngày '.$ngayky.'): Đang phân công xử lý'; // nội dung gửi mail
        $message    = $subject;
        $to_email   = 'gnvbdtquatruc@hanoi.gov.vn';//$noinhan;// mail nhận
        //configure email settings
        $config['protocol']  = 'smtp';
        // $config['smtp_host'] = 'mail.thudo.gov.vn';
        // $config['smtp_port'] = '587';
        $config['smtp_host'] = 'ssl://mail.thudo.gov.vn';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'vanthu_sotc@hanoi.gov.vn';// email gửipthtk_sotc@hanoi.gov.vn
        $config['smtp_pass'] = 'sotchanoi042018';// pass cua mail trentinhoc@gmail.com
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';
        $config['wordwrap']  = TRUE;
        $config['newline']   = "\r\n"; //use double quotes
        $this->load->library('email', $config);
        $this->email->initialize($config);
        //send mail
        $this->email->clear(TRUE);
        $this->email->from($from_email, $name);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        $filename = 'tmpxml/XML_Output.sdk';
        $this->email->attach($filename);
        if ($this->email->send(FALSE)) {
        }
        else{
        }
    }
    /** Thêm văn bản đến */
    public function insertDocGo()
    {
        $data_doc_go = array(
            'sTenKV' => _post('khuvuc'),
            'sKyHieu' => _post('sokyhieu'),
            'sTenLVB' => _post('loaivanban'),
            'sTenDV' => _post('donvi'),
            'sNgayKy' => (_post('ngayky')),
            'sTenLV' => _post('linhvuc'),
            'sMoTa' => _post('trichyeu').' [+]',
            'sTenNguoiKy' => _post('nguoiky'),
            'sNgayNhan' => (_post('ngaynhan')),
            'sNgayNhap' => date("Y-m-d H:i:s"),
            'iSoDen' => _post('soden'),
            'sChucVu' => _post('chucvu'),
            'sHanGiaiQuyet' => (_post('hangiaiquyet')),
            'iSoTrang' => _post('sotrang'),		
			'FK_iMaDM' => _post('domat'),
			'FK_iMaDK' => _post('dokhan'),			
            'iTrangThai_TruyenNhan' => '2',
            'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
            'sNoiDung'  => _post('noidunghop'),
            'sHanNoiDung' => (!empty(_post('hannoidung'))) ? date_insert(_post('hannoidung')) : (_post('hangiaiquyet1')),
            'sHanThongKe' => (!empty(_post('hannoidung'))) ? date_insert(_post('hannoidung')) : (_post('hangiaiquyet1'))
//                'iVanBanQPPL' => (!empty(_post('iVanBanQPPL'))) ? _post('iVanBanQPPL') : '0',
//                'iSTCChuTri' => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
//                'iVanBanTBKL' => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
//                'iVanBanMat' => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0'
        );
//            pr($data_doc_go);
        $DocGo = $this->Mdanhmuc->themDuLieu('tbl_vanbanden',$data_doc_go);
		
		// lấy id vừa thêm
		$vuatao = $this->Mdanhmuc->layVBDenVuaTao( _post('soden'), _post('sokyhieu'));
		$idvuatao = $vuatao[0]['PK_iMaVBDen'];		
		// thêm file_vbden
		$file_vb = array(
            'FK_iMaVBDen' => $idvuatao,
            'sTenFile' => _post('tenfile'),
            'sDuongDan' => _post('duongdan'),
            'sThoiGian' => date("Y-m-d H:i:s", time()),
            'FK_iMaCB' => $this->_session['PK_iMaCB']
        );
        $this->Mdanhmuc->themDuLieu('tbl_files_vbden',$file_vb);
		
		$data_doc_go = array(
			'iFile' => 1
        );
		$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idvuatao,'tbl_vanbanden',$data_doc_go);
		
        if ($DocGo > 0) {
			redirect('vanbanchoxuly');
            return messagebox('Thêm thành công', 'info');
        } else {
            return messagebox('Thêm văn bản đến thất bại', 'danger');
        }
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'vanbanchoxuly?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwait('2',NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 5;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwait('2',NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function getDataDocGo()
    {
        $idDoc = $this->input->post('idDoc');
        $getDataDoc =  $this->Mvanbanden->layDuLieu('PK_iMaVBDen',$idDoc);
//        pr($getAppo);
        echo json_encode($getDataDoc);exit();
    }
	
	public function layNgayHetHan()
    {               
		$ngaynhan   = (_post('ngaynhan'))?_post('ngaynhan'):date('d/m/Y');
        $songay     = _post('songay');
        $ngaynghi   = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_ngaynghi');
        $i          = 0;
        foreach ($ngaynghi as $key => $value) {
            if($value['sNgayNghi'] != date_insert($ngaynhan))
            {
                if(date_insert($ngaynhan) <= $value['sNgayNghi'] && $value['sNgayNghi'] <= dateFromBusinessDays((int)$songay,$ngaynhan)){
                    $i++;
                }
            }
        }       
        $hangiaiquyet = date_select(dateFromBusinessDays((int)$songay+$i,$ngaynhan));
		
        echo json_encode($hangiaiquyet); exit();

    }

}