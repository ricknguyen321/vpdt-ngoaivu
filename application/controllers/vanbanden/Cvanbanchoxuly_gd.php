<?php
/**
 * Created by PhpStorm.
 * User: MinhDuy
 * Date: 6/14/2017
 * Time: 3:56 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanchoxuly_gd extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {
		$action = _post('action');
        switch ($action) {
			case 'layNgayHetHan':
				$this->layNgayHetHan();
				break;
			default:
                # code...
                break;
        }
		
		$data['dsquytrinh'] = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_quytrinhiso');
//        pr($data_array);
        // danh sách giám đốc
        $data['getAccountDirec'] = $this->Mvanbanchoxuly->getAccount('0','4');
        // danh sách phó giám đốc
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0','5');
        // danh sách phòng
        $data['getDepartment'] = $this->Mvanbanchoxuly->getDepartment('0');
        //danh sách chờ xử lý
		if($this->input->post('luunoidung')){
            $data['content'] =  $this->insertDocGo();
        }
        if($this->_session['iQuyenHan_DHNB'] == '4'){
            /** @var danh sách tìm kiếm phân trang $page */
            $page               = $this->PhanTrang();
            $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
            foreach ($data['getDocAwait'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $process = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen']);
				$chuyenlai = $this->Mvanbanden->layDuLieuCN('PK_iMaVBDen',$value['PK_iMaVBDen'],'tbl_vanban_chuyenlai');
				$PhongCL = $this->Mvanbanchoxuly->getPhongBan($chuyenlai['PK_iMaCBTuChoi']);
                if(!empty($duongdan))
                {
                    $data['getDocAwait'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }
                if(empty($duongdan)){
                    $data['getDocAwait'][$key]['sDuongDan'] = '';
                }
                $arrayCBNhan = array();
                $arrayMoTa = array();
                $arrayMangPH = array();
                $arrayCT = array();
                foreach ($process as $key1 => $value1){
                    $ktlv = $this->Mvanbanchoxuly->TestCB($value1['PK_iMaCBNhan']);
                    $testlv = $ktlv['iQuyenHan_DHNB'];
                    if($value1['PK_iMaCBNhan'] != 0){
                        $arrayCBNhan[$testlv] = $value1['PK_iMaCBNhan'];
                        $arrayMoTa[$testlv] = $value1['sMoTa'];
                        $arrayMangPH[7] = $value1['PK_iMaPhongPH'];
                    }else{
                        $arrayMangPH[7] = $value1['PK_iMaPhongPH'];
                    }
                    if(!empty($value1['PK_iMaPhongCT'] != 0)){
                        $arrayCT[6] = $value1['PK_iMaPhongCT'];
                        $arrayMoTa[6] = $value1['sMoTa'];
                    }
                }
                if(!empty($value['PK_iMaCBTuChoi'])){
                    $data['getDocAwait'][$key]['PhongChuyenLai'] = $PhongCL;
                }
				if(!empty($chuyenlai['sLyDo'])) {
                    $data['getDocAwait'][$key]['sLyDo'] = $chuyenlai['sLyDo'];
                }
//                pr($PhongCL);
                $data['getDocAwait'][$key]['PK_iMaCBNhan'] = $arrayCBNhan;
                $data['getDocAwait'][$key]['PK_iMaPhongCT'] = $arrayCT;
                $data['getDocAwait'][$key]['ChuThich'] = $arrayMoTa;
                $data['getDocAwait'][$key]['PK_iMaPhongPH'] = $arrayMangPH;
//                pr($arrayCBNhan);
            }
//            pr($data['getDocAwait']);
            $data['phantrang']     = $page['pagination'];
            $data['loaivanban']      = $page['loaivanban'];
            $data['sokyhieu']    = $page['sokyhieu'];
            $data['ngaynhap']       = $page['ngaynhap'];
            $data['donvi']        = $page['donvi'];
            $data['ngayky']          = $page['ngayky'];
            $data['ngayden']      = $page['ngayden'];
            $data['trichyeu']      = $page['trichyeu'];
            $data['nguoiky']   = $page['nguoiky'];
            $data['soden']     = $page['soden'];
            $data['denngay']    = $page['denngay'];
            $data['chucvu']   =$page['chucvu'];
            $data['nguoinhap']  = $page['nguoinhap'];
            $data['count'] = $page['count'];
			$data['pagei'] = $this->input->get('page');
        }

        // duyệt văn bản chờ xử lý
        $this->insertDocAwait();
        $data['title']    = 'Giám đốc chỉ đạo';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vvanbanchoxuly_gd';
        $this->load->view('layout_admin/layout',$temp);
    }
	public function insertDocGo()
    {
        $data_doc_go = array(
            'sTenKV'                => _post('khuvuc'),
            'sKyHieu'               => _post('sokyhieu'),
            'sTenLVB'               => _post('loaivanban'),
            'sTenDV'                => _post('donvi'),
            'sNgayKy'               => (_post('ngayky')),
            'sTenLV'                => _post('linhvuc'),
            'sMoTa'                 => _post('trichyeu'),
            'sTenNguoiKy'           => _post('nguoiky'),
            'sNgayNhan'             => (_post('ngaynhan')),
            'sNgayNhap'             => date("Y-m-d"),
            'iSoDen'                => _post('soden'),
            'sChucVu'               => _post('chucvu'),
            'sHanGiaiQuyet'         => (_post('hangiaiquyet')),
            'iSoTrang'              => _post('sotrang'),
            'iTrangThai_TruyenNhan' => '3',
            'FK_iMaNguoiNhap'       => $this->_session['PK_iMaCB'],
            'sNoiDung'              => _post('noidunghop'),
            'sHanNoiDung'           => (!empty(_post('hannoidung'))) ? date_insert(_post('hannoidung')) : (_post('hangiaiquyet1')),
            'sHanThongKe'           => (!empty(_post('hannoidung'))) ? date_insert(_post('hannoidung')) : (_post('hangiaiquyet1'))
//                'iVanBanQPPL' => (!empty(_post('iVanBanQPPL'))) ? _post('iVanBanQPPL') : '0',
//                'iSTCChuTri' => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
//                'iVanBanTBKL' => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
//                'iVanBanMat' => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0'
        );
//            pr($data_doc_go);
        $DocGo = $this->Mdanhmuc->themDuLieu('tbl_vanbanden',$data_doc_go);
        if ($DocGo > 0) {
            return messagebox('Thêm thành công', 'info');
        } else {
            return messagebox('Thêm văn bản đến thất bại', 'danger');
        }
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
		$gd = $this->Mdanhmuc->layTTGD();
		$idgd = $gd[0]['PK_iMaCB'];
        $taikhoan = $this->_session['PK_iMaCB'];
        if($this->input->post('duyetvanban')){
            $arrayDuyet = array();
//            pr($this->input->post('duyet'));
            foreach ($this->input->post('duyet') as $key5 => $value5){
                if(!empty($value5)){
                    $phogiamdoc              = _post('phogiamdoc')[$value5];
                    $phongchutri             = _post('phongchutri')[$value5];
                    $noidungchuyenphogiamdoc = nl2br(_post('chidaophogiamdoc')[$value5]);
                    $noidungchuyenphong      = nl2br(_post('chidaophongchutri')[$value5]);
					
					if(!empty($phogiamdoc))
                    {
						$mangthemdulieu2 = array(
							'FK_iMaVBDen'     => $value5,
							'FK_iMaCB_Chuyen' => $taikhoan,
							'FK_iMaCB_Nhan'   => $phogiamdoc,
							'sNoiDung'        => $noidungchuyenphogiamdoc,
							'sThoiGian'       => date('Y-m-d H:i:s')
						);					
						$this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu2);
					}
					
                    if(!empty($phongchutri))
                    {
                        $phongbanHD  = $phongchutri;
                        if($phongbanHD==11) {
                            $quyenhan       = array(3);
                            $chucvuphong    = array(6);
                            $quyendb        = '';
                        }
                        else{
                            $quyenhan       = array(6);
                            $chucvuphong    = '';
                            $quyendb        = '';
                        }
                        $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
                        $canbonhan   = $truongphong[0]['PK_iMaCB'];
						
						$mangthemdulieu = array(
							'FK_iMaVBDen'     => $value5,
							'FK_iMaCB_Chuyen' => $taikhoan,
							'FK_iMaCB_Nhan'   => $canbonhan,
							'sNoiDung'        => $noidungchuyenphong,
							'sThoiGian'       => date('Y-m-d H:i:s')
						);					
						$this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
						
                    }
					
                    /** xóa chuyền nhận văn bản để trách trường hợp còn quy trình chuyền nhận */
                    $this->Mvanbanchoxuly->xoaDuLieu('PK_iMaVBDen',$value5,'tbl_chuyennhanvanban');
                    $PDirector = $this->input->post('phogiamdoc');
                    $Deparment = $this->input->post('phongchutri');
                    $PGDThamMuu = _post('pgdthammmuu')[$key5];
                    $Doc_array= array();
					
					$Doc_array['iDeadline'] = (_post('iDeadline_'.$value5)) ? 1 : NULL;
					$Doc_array['iSTCChuTri'] = (_post('iSTCChuTri_'.$value5)) ? 1 : 0;
					$Doc_array['iVanBanTBKL'] = (_post('iVanBanTBKL_'.$value5)) ? 1 : 0;
					$Doc_array['iToCongTac'] = (_post('iToCongTac_'.$value5)) ? 1 : 0;
					$Doc_array['iSTCPhoiHop'] = (_post('iSTCPhoiHop_'.$value5)) ? 1 : 0;

                    if(!empty($PGDThamMuu)){
                        if (!empty($Deparment[$key5])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '5';
                            if(!empty(_post('hangiaiquyet')[$key5])&&(_post('hangiaiquyet')[$key5] > '01/01/2017')){
								if (_post('iDeadline_'.$value5)){
									$Doc_array['sHanThongKe'] = date_insert(_post('hangiaiquyet')[$key5]);
								} else {
									$Doc_array['sHanThongKe'] = date_insert(_post('hangiaiquyet')[$key5]);
								}
                                $Doc_array['iTrangThai_GD'] = '1';
                                $Doc_array['iPgdThamMuu'] = _post('pgdthammmuu')[$key5];
                            }
                            /**  Cập nhật lại trạng thái khi chuyển đến từng quyền */
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        }
                    }else{
						if ($phogiamdoc == $idgg) {
							$Doc_array['iPgdThamMuu'] = '1';
						}
						
                        if (!empty($PDirector[$key5])) {
							if ($phogiamdoc == $idgd) {
								$Doc_array['iTrangThai_TruyenNhan'] = '5';
							} else {							
								$Doc_array['iTrangThai_TruyenNhan'] = '4';
							}
							
                            if(_post('hangiaiquyet')[$key5] != ""){
                                $Doc_array['sHanThongKe'] = date_insert(_post('hangiaiquyet')[$key5]);
                                $Doc_array['iTrangThai_GD'] = '1';
                            }
//                        pr($Doc_array);
                            /**  Cập nhật lại trạng thái khi chuyển đến từng quyền */
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        }
                        elseif (!empty($Deparment[$key5])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '5';
                            if(_post('hangiaiquyet')[$key5] != ""){
                                $Doc_array['sHanThongKe'] = date_insert(_post('hangiaiquyet')[$key5]);
                                $Doc_array['iTrangThai_GD'] = '1';
                            }
                            /**  Cập nhật lại trạng thái khi chuyển đến từng quyền */
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        }
                    }
                    /** @var mang chuyen van ban cho pho giam doc $PDir_array */
                    $PDir_array= array();
                    if(!empty($PDirector[$key5])){
                        $PDir_array['PK_iMaCBNhan'] = $PDirector[$key5];
                        $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                        $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                        $PDir_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                        $PDir_array['sMoTa'] = nl2br($this->input->post('chidaophogiamdoc')[$key5]);
                        $PDir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key5] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key5]);
                        $PDir_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key5]) ? $this->input->post('vanbanquantrong')[$key5] : 0;
                        $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                        $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
                    }

                    /** @var mang chuyen phong chu tri va phoi hop $Departmanet_array */
                    $Departmanet_array= array();
                    if(!empty($Deparment[$key5])){
                        $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                        $Departmanet_array['PK_iMaPhongPH'] = (!empty($this->input->post('mangphoihop')[$key5]) ? $this->input->post('mangphoihop')[$key5] : NULL);
                        $Departmanet_array['PK_iMaPhongCT'] = $Deparment[$key5];
                        $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                        $Departmanet_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                        $Departmanet_array['sMoTa'] = nl2br($this->input->post('chidaophongchutri')[$key5]);
                        $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key5] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key5]);
                        $Departmanet_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key5]) ? $this->input->post('vanbanquantrong')[$key5] : 0;
                        $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
//                        pr($Departmanet_array);
                        $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array);
                    }

                }
            }
            return redirect(base_url().'vanbanchoxuly_gd');

        }
    }

    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'vanbanchoxuly_gd?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwait('3',NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 5;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_gd');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwait('3',NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
	
	public function layNgayHetHan()
    {               
	
        $ngaynhan   = (_post('ngaynhan'))?_post('ngaynhan'):date('d/m/Y');
        $songay     = _post('songay');
        $ngaynghi   = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_ngaynghi');
        $i          = 0;
        foreach ($ngaynghi as $key => $value) {
            if($value['sNgayNghi'] != date_insert($ngaynhan))
            {
                if(date_insert($ngaynhan) <= $value['sNgayNghi'] && $value['sNgayNghi'] <= dateFromBusinessDays((int)$songay,$ngaynhan)){
                    $i++;
                }
            }
        }       
        $hangiaiquyet = date_select(dateFromBusinessDays((int)$songay+$i,$ngaynhan));
		
        echo json_encode($hangiaiquyet); exit();

    }

}