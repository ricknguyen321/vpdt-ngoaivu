<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/15/2017
 * Time: 10:25 AM
 */
class Cgiaymoichuyenvienchutrixuly extends MY_Controller {



    public function __construct()

    {

        parent::__construct();

        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('vanbandi/Mtruyennhan');
        $this->load->model('Vanbanden/Mvanbanphoihop');

    }

    public function index()

    {
// danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
        $data['truongphong'] = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0];
//        pr($data['truongphong']);

        $idAppointment = $this->uri->segment(2);
//        pr($idAppointment);

        // trình tự xử lý
        $canbo                    = $this->Mtruyennhan->layDSCB2();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        $data['luuvetchuyennhan']    = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_luuvet_chuyennhan');
        $data['luuvetduyet']    = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$idAppointment,'tbl_luuvet_duyet');
		$data['chidao']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_chidao');
		$data['dexuat']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_ykien');

        $process = $this->Mgiaymoichoxuly->getDocProcess($idAppointment);
        $processcv = $this->Mgiaymoichoxuly->getDocProcesscv($idAppointment);
        $processpb = $this->Mgiaymoichoxuly->getDocProcessPB($idAppointment);
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');
//        pr($process);
//        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($this->_session);
        $data['idDocGo'] = $idAppointment;
        $data['process'] = array_merge($process,$processpb,$processcv);
        if(!empty($idAppointment)){
            $data['getdAppointment'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
            // lay chuyen vien chu tri
            $cvtc = $this->Mgiaymoichoxuly->getCVCT($idAppointment);
//            pr($cvtc);
            //ket qua phong thu ly
            if(!empty($cvtc)) {
                $data['resultPTL'] = $this->Mgiaymoichoxuly->getResultPTL($idAppointment, NULL, $cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPTL']);
                $data['resultPPH'] = $this->Mgiaymoichoxuly->getResultPPH($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
            }else{
                $data['resultPPH'] = $this->Mgiaymoichoxuly->getResultPPH($idAppointment,NULL,NULL);
            }
//            pr($cvtc[0]['PK_iMaCVCT']);
        }
        // HOÀN THÀNH
        $taikhoan = $this->_session['PK_iMaCB'];
        if($this->input->post('luulai')){
            $phongban = $this->_session['FK_iMaPhongHD'];
            $tp_pp    = $this->Mvanbanphoihop->layPH_TP($idAppointment,$taikhoan);
            $tp_pp    = $tp_pp[0]['PK_iMaCBChuyen'];
            $luuvetduyet = array(
                'FK_iMaVB'        => $idAppointment,
                'FK_iMaCB_Chuyen' => $taikhoan,
                'sThoiGian'       => date('Y-m-d H:i:s'),
                'sNoiDung'        => _post('ketqua'),
                'FK_iMaCB_Nhan'   => $tp_pp
            );
            $this->Mdanhmuc->themDuLieu('tbl_luuvet_duyet',$luuvetduyet);
            $data_result = array(
                'sMoTa'               =>  $this->input->post('ketqua'),
                'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
                'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
                'PK_iMaVBDen'         =>  $idAppointment,
                'active'              =>  2,
               // 'sTenFiles'              =>  $this->input->post('fileketqua'),
                'sDuongDanFile'              =>  'doc_uploads/'.clear($_FILES["files"]["name"]),
                'date_file'           => date('Y-m-d H:i:s',time())
            );
//            pr($data_result);
           // $this->Mdanhmuc->themDuLieu('tbl_file_ketqua',$data_result);
            $data_giaymoi = array(
                'iTrangThai' => '2',
                'sNgayGiaiQuyet' => date('Y-m-d',time()),
                'PK_iMaCBHoanThanh' => $this->_session['PK_iMaCB']
            );
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_giaymoi);
            $data_kh = array(
                'active' => 2,
				'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                'ket_qua_hoanthanh'               =>  $this->input->post('ketqua')
            );
           // $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$this->_session['PK_iMaCB'],$data_kh);
           // $this->upload('doc_uploads_'.date('Y').'/',clear($_FILES["files"]["name"]),'files');
           // $this->uploadN();
			$data['content'] = $this->themDuLieu($idAppointment);
			if ($this->_session['iQuyenHan_DHNB'] == 8) {
				return redirect(base_url().'giaymoi_cv');
			}
			if ($this->_session['iQuyenHan_DHNB'] == 7) {
				return redirect(base_url().'giaymoi_pp');
			}
			if ($this->_session['iQuyenHan_DHNB'] == 6) {
				return redirect(base_url().'giaymoi_tp');
			}
        }
        if($this->input->post('xoafile')){
            $this->Mdanhmuc->xoaDuLieu('PK_iMaKetQua',$this->input->post('xoafile'),'tbl_file_ketqua');
        }
        $data['title']    = 'Xử lý giấy mời họp';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoichuyenvienchutrixuly';
        $this->load->view('layout_admin/layout',$temp);

    }
	
	public function themDuLieu($mavanban)
	{
			$time = time();
			$name = $_FILES['files']['name'];			
			if(!empty($name[0]))
			{
				$this->upload('doc_vbanden_'.date('Y'),$time);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}					
					$tmp = str_replace('$', '-', $tmp);					
					$tmp = $tmp.'.'.$a[$c];	
					
					$files[] = array(
						'FK_iMaVBDen' => $mavanban,
						'sTenFile'    => (_post('tenteptin')!='')?_post('tenteptin'):$value,
						'sDuongDan'   => 'doc_vbanden_'.date('Y').'/den_up_'.$time.'_'.$tmp,
						'sThoiGian'   => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'    => $this->_session['PK_iMaCB']
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbden',$files);
			}
	}

	public function upload($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {									
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'den_up_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}