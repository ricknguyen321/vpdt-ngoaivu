<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanquantrong extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->input->get('nam') == 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default', TRUE);
        }
        if($this->input->get('nam') > 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default_'.$this->input->get('nam'), TRUE);
        }
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->library('pagination');
    }
    public function index()
    {
        if($this->input->post('boquantrong')){
            $boquantrong = $this->input->post('vanbanquantrong');
            $i=1;
            foreach ($boquantrong as $key => $value){
                $this->Mdanhmuc->setDuLieu('PK_iMaCN',$key,'tbl_chuyennhanvanban','sVBQT','0');
            }
        }
        /** @var danh sách tìm kiếm phân trang $page */
        $page                  = $this->PhanTrang();
        $data['getDocGo']      = $page['info'];
        if(!empty($data['getDocGo'])){
            foreach ($data['getDocGo'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $processgd = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processpb1 = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $data['getDocGo'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                if(!empty($processgd)){
                    $data['getDocGo'][$key]['sChuTri'] = $processgd[0]['sMoTa'];
                }
                if(empty($processgd)){
                    $data['getDocGo'][$key]['sChuTri'] = '';
                }
                if(!empty($processpb1)){
                    $data['getDocGo'][$key]['sPhongChuTri'] = $processpb1[0]['sHoTen'];
                }
                if(empty($processpb1)){
                    $data['getDocGo'][$key]['sPhongChuTri'] = '';
                }
//                pr($processpb1);
            }
        }
//         pr($data['getDocGo']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']    = $page['loaivanban'];
        $data['sokyhieu']      = $page['sokyhieu'];
        $data['ngaynhap']      = $page['ngaynhap'];
        $data['donvi']         = $page['donvi'];
        $data['ngayky']        = $page['ngayky'];
        $data['ngayden']       = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['ngaymoi']       = $page['ngaymoi'];
        $data['nguoiky']       = $page['nguoiky'];
        $data['soden']         = $page['soden'];
        $data['denngay']       = $page['denngay'];
        $data['chucvu']        = $page['chucvu'];
        $data['nguoinhap']     = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count']         = $page['count'];
        $data['nam'] = $page['nam'];


        $data['title']    = 'Danh sách VB quan trọng';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vvanbanquantrong';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function PhanTrang()
    {
//        pr($this->_session);
//        pr($this->input->get('id'));
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu      = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi         = $this->input->get('donvi');
        $ngayky        = $this->input->get('ngayky');
        $ngayden       = $this->input->get('ngayden');
        $trichyeu      = $this->input->get('trichyeu');
        $ngaymoi       = $this->input->get('ngaymoi');
        $nguoiky       = $this->input->get('nguoiky');
        $soden         = $this->input->get('soden');
        $denngay       = $this->input->get('denngay');
        $chucvu        = $this->input->get('chucvu');
        $nguoinhap     = $this->input->get('nguoinhap');
        $cacloaivanban = $this->input->get('cacloaivanban');
        $nam = $this->input->get('nam');
        $id = $this->input->get('id');
        $idDocument    = $this->uri->segment(2);
        $config['base_url']             = base_url().'vanbanquantrong?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&ngaymoi='.$ngaymoi.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&cacloaivanban='.$cacloaivanban.'&id='.$id.'&nam='.$nam;
        $config['total_rows']           = count($this->Mvanbanden->getVBQT($this->_session['PK_iMaCB'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban));
//        pr($config['total_rows']);
        $config['per_page']             = 25;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanquantrong');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap']      = $ngaynhap;
        $data['donvi']         = $donvi;
        $data['ngayky']        = $ngayky;
        $data['ngayden']       = $ngayden;
        $data['trichyeu']      = $trichyeu;
        $data['ngaymoi']       = $ngaymoi;
        $data['nguoiky']       = $nguoiky;
        $data['soden']         = $soden;
        $data['denngay']       = $denngay;
        $data['chucvu']        = $chucvu;
        $data['nguoinhap']     = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['nam'] = $nam;
        $data['id'] = $id;
        $data['count']         = $config['total_rows'];
        $data['info']        = $this->Mvanbanden->getVBQT($this->_session['PK_iMaCB'],NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */