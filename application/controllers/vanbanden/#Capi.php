<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Capi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();

    }
    public function index()
    {
        $ID = $this->input->get('ID');
		if(!empty($ID)){
			$abcd = $this->Mdanhmuc->layDuLieuVBDen('PK_iMaVBDen',$ID,'tbl_vanbanden');
			$data['doid'] = json_encode($abcd);
		}
		$idTypeOf = $this->input->get('MA');
		if(!empty($idTypeOf)){
			$abc = $this->Mdanhmuc->layDuLieuVBDen('sKyHieu',$idTypeOf,'tbl_vanbanden');
			$data['do'] = json_encode($abc);
		}
        $data['url'] = base_url();
        $this->parser->parse('vanbanden/Vapi', $data, FALSE);
    }
    public function getData(){
        $idTypeOf = $this->input->get('id');
        $data = $this->Mdanhmuc->layDuLieuVBDen('sKyHieu',$idTypeOf,'tbl_vanbanden');
        echo json_encode($data);
        exit();
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */