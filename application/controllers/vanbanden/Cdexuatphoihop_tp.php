<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdexuatphoihop_tp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('Vanbanden/Mvanbanden');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->model('Vanbanden/Mvanbanphoihop');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		if(_post('guilen'))
		{
			$data['content'] = $this->guilen();
		}
		if(_post('tralai'))
		{
			$data['content'] = $this->tralai();
		}
		if(_post('dongy'))
		{
			$data['content'] = $this->dongy();
		}
		$vanban            = $this->DSVanBan($taikhoan);
		// pr($vanban);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Đề xuất phối hợp';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vdexuatphoihop_tp';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function dongy()
	{
		$taikhoan		= $this->_session['PK_iMaCB'];
		$mavanban		= _post('dongy');
		$phongphoihop	= _post('phongphoihop_'.$mavanban);
		$kiemtra		= $this->Mvanbanphoihop->capnhatKetQua($mavanban,$phongphoihop);
		if($kiemtra>0)
		{
			$machuyen = $this->Mvanbanphoihop->layTrangThai_PH_hai($mavanban,$taikhoan,$phongphoihop);
			$this->Mvanbanphoihop->capnhatTrangThai_PH_hai($machuyen['PK_iMaCN']);
			return messagebox('Thành công!','info');
		}
	}
	public function tralai()
	{
		$mavanban		= _post('tralai');
		$ykien			= _post('ykien_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];
		$phongphoihop	= _post('phongphoihop_'.$mavanban);
		$phongban		= $this->_session['FK_iMaPhongHD'];
		$chuyenvien		= $this->Mvanbanphoihop->layTK_CV($mavanban,$taikhoan,$phongphoihop);
		if(!empty($chuyenvien))
		{
			$data[]=array(
				'FK_iMaVBDen'		=> $mavanban,
				'FK_iMaNguoi_Gui'	=> $taikhoan,
				'FK_iMaNguoi_Nhan'	=> $chuyenvien['FK_iMaNguoi_Gui'],
				'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
				'sNoiDung'			=> $ykien,
				'CT_PH'				=> 1,
				'iTrangThai'		=> 1,
				'FK_iMaPB_PH'		=> $phongphoihop,
				'sFile_YeuCau'		=> ''
			);
			$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
			if($kiemtrataikhoan>0)
			{
				$id_insert = $this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_phoihop',$data);
	        	if($id_insert>0)
				{
					$this->Mvanbanphoihop->capnhatTrangThai_PH($mavanban,$taikhoan,2);
					return messagebox('Thành công!','info');
				}
			}
		}
	}
	public function guilen()
	{
		$mavanban		= _post('guilen');
		$phongphoihop	= _post('phongphoihop_'.$mavanban);
		$ykien			= _post('ykien_'.$mavanban);
		$file			= _post('file_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];

		$phongbanHD     = $phongphoihop;
		if ($phongbanHD==11) {
			$quyenhan		= array(3);
			$chucvuphong	= array(6);
			$quyendb 		= '';
		}
		else{
			$quyenhan		= array(6);
			$chucvuphong	= '';
			$quyendb 		= '';
		}
		$truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
		$data=array(
			'FK_iMaVBDen'		=> $mavanban,
			'FK_iMaNguoi_Gui'	=> $taikhoan,
			'FK_iMaNguoi_Nhan'	=> $truongphong[0]['PK_iMaCB'],
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sNoiDung'			=> $ykien,
			'CT_PH'				=> 2,
			'iTrangThai'		=> 1,
			'FK_iMaPB_PH'		=> $phongphoihop,
			'sFile_YeuCau'		=> $file
		);
		$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
		if($kiemtrataikhoan>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_chuyennhan_phoihop',$data);
			if($id_insert>0)
			{
				$this->Mvanbanphoihop->capnhatTrangThai($mavanban,$id_insert,$phongphoihop);
				return messagebox('Đề xuất đã gửi trưởng phòng phối hợp!','info');
			}
		}
	}
	public function DSVanBan($taikhoan) 
	{
		$config['base_url']             = base_url().'dexuatphoihop_pp';
		$config['total_rows']           = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,NULL); // 1:là chờ xử lý
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dexuatphoihop_pp');
      	}
		$data['info']       = $this->Mvanbanphoihop->layTTVB_DeXuat($taikhoan,1,NULL,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdexuatphoihop_tp.php */
/* Location: ./application/controllers/vanbanden/Cdexuatphoihop_tp.php */