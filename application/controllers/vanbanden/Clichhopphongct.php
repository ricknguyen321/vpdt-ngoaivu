<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 3:13 PM
 */
class Clichhopphongct extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->load->library('pagination');
    }
    public function index()
    {
//        pr($gd);
        // danh sach cho xu ly
        /** @var danh sách tìm kiếm phân trang $page */
		$cb = $this->_session['FK_iMaCB'];
		
		if($this->input->post('daxem')){
            $data['content'] = $this->markDaXem($this->input->post('daxem'));
        }
        $page               = $this->PhanTrang();
        $data['lichhop']    = $page['info'];
//            pr($this->_session);
        
		$data['phantrang']     = $page['pagination'];
       
        $data['count'] = $page['count'];

//        pr($data['processcvph']);
        $data['title']    = 'Danh sách chỉ đạo của lãnh đạo Sở';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vlichhopphongct';
        $this->load->view('layout_admin/layout',$temp);
    }
	
	public function markDaXem($idChiDao){
		$data2 = array(
			'id_chidao'   => $idChiDao,
			'id_cb'   => $this->_session['PK_iMaCB'],
			'time'   => date('Y-m-d H:i:s', time())
        );
		$this->Mdanhmuc->themDuLieu('tbl_chidao_cb',$data2);         
		return redirect(base_url().'lichhopphongct');           
    }
	
    public function PhanTrang()
    {
       
        $config['base_url']             = base_url().'lichhopphongct';
        //$config['total_rows']           = count($this->Mvanbanchoxuly->layDSChiDao($this->_session['PK_iMaCB'],$config['per_page'], $data['page']));
//        pr($config['total_rows']);
        $config['per_page']             = 100;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'lichhopphongct');
        }
		$tenpb = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_phongban');
		
        $data['info']        = $this->Mvanbanden->layLichHopPhongCT($tenpb[0]['sTenPB'], NULL);
		$config['total_rows']           = count($data['info']);
        $data['count']    = $config['total_rows'];
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}