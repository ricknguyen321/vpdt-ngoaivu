<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 11:09 AM
 */
class Cdshandadexuat extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mtruyennhan');
    }
    public function index()
    {
        $canbo = $this->Mtruyennhan->layDSCB();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        if($this->input->post('tuchoi')){
            $data['content'] = $this->Reject();
        }
        if($this->input->post('tuchoitp')){
            $data['content'] = $this->Rejecttp();
        }
        if($this->input->post('themhan')){
            $data['content'] = $this->insertDateline();
        }
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $processs = $this->Mvanbanchoxuly->laylanhdaosaucung($value['PK_iMaVBDen']);
                $process = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processcv = $this->Mvanbanchoxuly->getDocProcesscv($value['PK_iMaVBDen']);
                $processpb = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $process1 = array_merge($process,$processpb,$processcv);
//                pr($process1);
                $arrayTT = array();
                if(!empty($process1)){
                    foreach($process1 as $key1 => $value1){
                        $arrayTT[$key1] = $value1['sHoTen'];
                    }
                }
                $data['getDocAwait'][$key]['TrinhTu'] = $arrayTT;
                $data['getDocAwait'][$key]['ChiDao'] = $processs['sHoTen'];
                $data['getDocAwait'][$key]['PK_iMaLDCD'] = $processs['PK_iMaCB'];
                $data['getDocAwait'][$key]['QuyenHan'] = $processs['iQuyenHan_DHNB'];

                if($this->_session['FK_iMaPhongHD'] == 11){
                    $idTP = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }elseif($this->_session['iQuyenHan_DHNB'] == 7 && $this->_session['FK_iMaPhongHD'] != 12){
                    $idTP = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }elseif($this->_session['FK_iMaPhongHD'] == 12){
                    $idTP = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }
                if(!empty($idTP)){
                    $data['getDocAwait'][$key]['ChiDaoPP'] = $idTP;
                    $data['getDocAwait'][$key]['PK_iMaLDCDTP'] = $idTP1;
                }
//                pr($arrayCBNhan);
            }
//            pr($data['getDocAwait']);
        }
//        pr($page['info']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
//        pr($data['process']);
        $data['title']    = 'Danh sách VB đã đề xuất hạn giải quyết';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vdshandadexuat';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function Reject(){
        $idDoc = $this->input->post('maphongban');
        $data_reject = array(
            'PK_iMaVBDen' => $idDoc,
            'PK_iMaCBTuChoi' => $this->_session['PK_iMaCB'],
            'sTenCB'   => $this->_session['sHoTen'],
            'sLyDo'    => $this->input->post('noidungtuchoi'),
            'sGhiChu'  => $this->input->post('ghichu'),
            'iTrangThai' => '1',
            'sThoiGianTao' => date('Y-m-d H:s',time())
        );
        $data_doc = array(
            'iTrangThai_TruyenNhan' => '2',
            'iTrangThai' => '0'
        );
        $this->Mdanhmuc->themDuLieu('tbl_vanban_chuyenlai',$data_reject);
        $this->Mdanhmuc->xoaDuLieu('vanban_id',$idDoc,'kehoach');
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
        if ($DocGo > 0) {
			redirect('dshandadexuat');
            return messagebox('Chuyền văn bản về chánh văn phòng thành công', 'info');
        } else {
            return messagebox('Chuyền văn bản về chánh văn phòng thất bại', 'danger');
        }
    }
    public function Rejecttp(){
        $idDoc = $this->input->post('maphongbantp');
        $data_reject = array(
            'PK_iMaVBDen' => $idDoc,
            'PK_iMaCBTuChoi' => $this->_session['PK_iMaCB'],
            'sTenCB'   => $this->_session['sHoTen'],
            'sLyDo'    => $this->input->post('noidungtuchoi'),
            'sGhiChu'  => $this->input->post('ghichu'),
            'iTrangThai' => '1',
            'sThoiGianTao' => date('Y-m-d H:s',time())
        );
        $data_doc = array(
            'iTrangThai_TruyenNhan' => '5',
            'iTrangThai' => '0'
        );
//        pr($data_doc);
        $this->Mdanhmuc->themDuLieu('tbl_vanban_chuyenlai',$data_reject);
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
        if ($DocGo > 0) {
			redirect('dshandadexuat');
            return messagebox('Chuyền văn bản về trưởng phòng thành công', 'info');
        } else {
            return messagebox('Chuyền văn bản về trưởng phòng thất bại', 'danger');
        }
    }
    public function PhanTrang()
    {
        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3)
        {
            $PhongCT = $this->_session['FK_iMaPhongHD'];
            $CVCT = NULL;
        }
        if($this->_session['iQuyenHan_DHNB'] == 7 || $this->_session['iQuyenHan_DHNB'] == 8)
        {
            $PhongCT = NULL;
            $CVCT = $this->_session['PK_iMaCB'];
        }
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'dshandadexuat?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&cacloaivanban='.$cacloaivanban;
        $config['total_rows']           = count($this->Mvanbanchoxuly->layVBGiaHanTuChoi(NULL,NULL,$PhongCT,$CVCT,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,NULL,NULL,NULL,$cacloaivanban));
//        pr($config['total_rows']);
        $config['per_page']             = 200;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'dshandadexuat');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->layVBGiaHanTuChoi(NULL,NULL,$PhongCT,$CVCT,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page'],NULL,$cacloaivanban);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function insertDateline(){
        $idDoc = $this->input->post('mavbd');
        $idLD = $this->input->post('ldgui');
		$noidung = $this->input->post('noidungtuchoi');
		$noidung = nl2br($noidung);
		$time = date('d/m H:i', time());
        $data_han = array(
            'iLanhDao' => $idLD,
            'iTrangThai_ThemHan'   => 1,
            'sHanChoDuyet'    => date_insert($this->input->post('handexuat')),
            //'sNoiDungGiaHan'    => '<span>'.$this->input->post('noidungtuchoi').'</span><br/>'.'(<b>'.$this->_session['sHoTen'].'</b>)'.'<br/>'
			'sNoiDungGiaHan'    => '<p><span>'.$noidung.'</span>'.' - <b>'.$this->_session['sHoTen'].'</b> - '.$time.'</p>'
        );
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
		
		$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 1,
				'FK_iMaCB_Gui'   => $this->_session['PK_iMaCB'],
				'sHanThongKe' => date_insert($this->input->post('handexuat')),
				'noidunggiahan'    => $noidung,
				'ngaygiahan'   => date('Y-m-d H:i:s',time())				
            );
		$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
        if ($DocGo > 0) {
			redirect('dshandadexuat');
            return messagebox('Chuyền hạn văn bản về lãnh đạo thành công', 'info');
        } else {
            return messagebox('Chuyền hạn văn bản về lãnh đạo thất bại', 'danger');
        }
    }

}