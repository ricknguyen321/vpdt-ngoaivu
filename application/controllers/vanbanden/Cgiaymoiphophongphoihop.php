<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 10:50 AM
 */
class Cgiaymoiphophongphoihop extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
    }
    public function index()
    {
//    pr($this->_session);
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mvanbanchoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwaitPPH']    = $page['info'];
//        pr($data['getDocAwaitPPH']);
        if(!empty($data['getDocAwaitPPH'])) {
            foreach ($data['getDocAwaitPPH'] as $key => $value) {
                $PPPHCT = $this->Mvanbanchoxuly->getDocPPCT($value['PK_iMaVBDen'],$this->_session['PK_iMaCB']);
                //pr($PPPHCT);
                if(!empty($PPPHCT[0])){
                    $data['getDocAwaitPPH'][$key]['PPPHCT'] = $PPPHCT[0]['smotanoidung'];
                }
                $process1 =  $this->Mvanbanchoxuly->getDocProcessCVPPH($value['PK_iMaVBDen'],$this->_session['FK_iMaPhongHD']);
                if($this->_session['FK_iMaPhongHD'] == 11){
                    $idTP = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }elseif($this->_session['iQuyenHan_DHNB'] == 7 && $this->_session['FK_iMaPhongHD'] != 12){
                    $idTP = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }elseif($this->_session['FK_iMaPhongHD'] == 12){
                    $idTP = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }
                $arrayTT = array();
                if(!empty($process1)){
                    foreach($process1 as $key1 => $value1){
                        $arrayTT[$key1] = $value1['sHoTen'];
                    }
                }
                $data['getDocAwaitPPH'][$key]['TrinhTu'] = $arrayTT;
                if(!empty($idTP)){
                    $data['getDocAwaitPPH'][$key]['HoTenTP'] = $idTP;
                }

//                pr($mang);
            }
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['count'] = $page['count'];
//        pr($data['getDocAwaitPPH']);
        // duyệt văn bản chờ xử lý
        if($this->input->post('duyet')) {
            $this->insertDocAwait();
        }
        $data['title']    = 'Giấy mời Phó phòng phối hợp';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoiphophongphoihop';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $process = $this->Mvanbanchoxuly->laylanhdaosaucung($ma);
        $staff = $this->input->post('phongchutri');
        $DepartmentPH = $this->input->post('mangphoihop');
        $Deputy = $this->input->post('phogiamdoc');
        $DeputyPH = $this->input->post('mangphoihoppp');
        if($this->_session['iQuyenHan_DHNB'] == 7) {
            $this->Mdanhmuc->setDuLieu('PK_iMaPhongPH',$this->input->post('id_phoihop')[$ma],'tbl_phongphoihop','iTrangThai','4');
        }
        $this->Mvanbanchoxuly->xoaDuLieu3Where('PK_iMaVBDen',$ma,'input_per', $this->_session['PK_iMaCB'],'iTrangThai_PPPH',1,'tbl_phongphoihop');
        /** @var mang chuyen chuyen vien $Departmanet_array */
        if (!empty($DepartmentPH[$ma])) {
            /** @var mang chuyen chuyen vien $Departmanet_array */
            $Departmanet_array= array();
            $mangph = $this->input->post('mangphoihop')[$ma];
            $cvphoihop = explode( ',', $mangph );
            foreach($cvphoihop as $key => $value){
                $Departmanet_array['PK_iMaCB'] = $value;
                $Departmanet_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                $Departmanet_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet') == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                // chuyen viên xuống phó chủ trì phối hợp là 2 chuyển thẳng là 1
                $Departmanet_array['iTrangThai'] = 1;
                $Departmanet_array['iTrangThai_PPPH'] = 1;
                $Departmanet_array['PK_iMaPPPH'] = '0';
                $this->Mdanhmuc->themDuLieu('tbl_phongphoihop',$Departmanet_array);
//                    pr($Departmanet_array);
            }
            $giahan = dateFromBusinessDays(1,_post('ngaymoi')[$ma]);
            //chuyen chuyen vien phối hợp
            foreach($cvphoihop as $key => $value){
                $kehoach_data_cvph = array(
                    'vanban_id' => $this->input->post('doc_id')[$ma],
                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
					'vanban_skh' => $this->input->post('sohieu')[$ma],
                    'tuan' => date("W"),
                    'thuc_hien' => '2',
                    'loai_kh' => 1,
					'chucvu'  => 8,
                    'trangthai_vbd' => 1,
                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? $giahan  : date_insert(_post('hangiaiquyet')[$ma]),
                    'canbo_id' => $value,
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
					'user_input' => $this->_session['PK_iMaCB'],
                    'lanhdao_so' => $process['PK_iMaCB'],
                    'phong_id' => $this->_session['FK_iMaPhongHD']
                );

                $this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_cvph);
            }
        }
        return redirect(base_url().'giaymoiphophongphoihop');
    }
    public function PhanTrang()
    {
        if ($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
            $user=NULL;
            $iTrangThaiPH = NULL;
        }elseif ($this->_session['iQuyenHan_DHNB'] == 7){
            $user = $this->_session['PK_iMaCB'];
            $iTrangThaiPH = 2;
//            pr($user);
        }
        elseif ($this->_session['iQuyenHan_DHNB'] == 11){
            $user=$this->_session['PK_iMaCB'];
            $iTrangThaiPH = 1;
        }
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'phongphoihop?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwaitPPPH($this->_session['iQuyenHan_DHNB'],$this->_session['FK_iMaPhongHD'],$user,1,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,NULL,NULL,$iTrangThaiPH));
//        pr($config['total_rows']);
        $config['per_page']             = 100;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'phongphoihop');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwaitPPPH($this->_session['iQuyenHan_DHNB'],$this->_session['FK_iMaPhongHD'],$user,1,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page'],$iTrangThaiPH);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}