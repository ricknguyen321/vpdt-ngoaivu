<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctimkiem_cvp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['title']    = 'Tìm kiếm chánh văn phòng';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vtimkiem_cvp';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Ctimkiem_cvp.php */
/* Location: ./application/controllers/vanbanden/Ctimkiem_cvp.php */