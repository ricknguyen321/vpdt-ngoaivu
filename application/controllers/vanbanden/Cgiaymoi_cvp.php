<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/12/2017
 * Time: 9:35 AM
 */
class Cgiaymoi_cvp extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mvanbandi');
    }
    public function index()
    {
        $action = _post('action');
        switch ($action) {
            case 'getDataDocGo':
                $this->getDataDocGo();
                break;
        }
        if($this->input->post('luunoidung')){
            $data['content'] =  $this->insertDocGo();
        }
//        pr($this->_session);
        // danh sách giám đốc
        $data['getAccountDirec'] = $this->Mgiaymoichoxuly->getAccount('0','4');
        // danh sách phó giám đốc
        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0','5');
        // danh sách phòng
        $data['getDepartment'] = $this->Mgiaymoichoxuly->getDepartment('0');
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                if(!empty($duongdan))
                {
                    $data['getDocAwait'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $data['getDocAwait'][$key]['sDuongDan'] = '';
                }
            }
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
//        pr($data['getDocAwait']);
        // duyệt văn bản chờ xử lý
        $this->insertDocAwait();
        $data['title']    = 'Phân loại giấy mời';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoi_cvp';
        $this->load->view('layout_admin/layout',$temp);
    }
    /** Thêm văn bản đến */
    public function insertDocGo()
    {
        $data_doc_go = array(
            'sTenKV' => _post('khuvuc'),
            'sKyHieu' => _post('sokyhieu'),
            'sTenLVB' => _post('loaivanban'),
            'sTenDV' => _post('donvi'),
            'sNgayKy' => (_post('ngayky')),
            'sTenLV' => _post('linhvuc'),
            'sMoTa' => _post('trichyeu'),
            'sTenNguoiKy' => _post('nguoiky'),
            'sNgayNhan' => (_post('ngaynhan')),
            'sNgayNhap' => date("Y-m-d"),
            'iSoDen' => _post('soden'),
            'sChucVu' => _post('chucvu'),
            'sHanGiaiQuyet' => (_post('hangiaiquyet')),
            'iSoTrang' => _post('sotrang'),
            'iTrangThai_TruyenNhan' => '2',
            'iGiayMoi' => '1',
            'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
            'sNoiDung'  => _post('noidunghop'),
            'sGiayMoiGio' => (!empty(_post('giohop'))) ? (_post('giohop')) : (_post('giohopcu')),
            'sGiayMoiNgay' => (!empty(_post('ngayhop'))) ? date_insert(_post('ngayhop')) : (_post('ngayhopcu')),
            'sGiayMoiDiaDiem' => (!empty(_post('diadiemhop'))) ? (_post('diadiemhop')) : (_post('diachihopcu')),
            'sHanNoiDung' => (!empty(_post('hannoidung'))) ? date_insert(_post('hannoidung')) : (_post('hangiaiquyet1')),
            'sHanThongKe' => (!empty(_post('hannoidung'))) ? date_insert(_post('hannoidung')) : (_post('hangiaiquyet1'))
        );
//            pr($data_doc_go);
        $DocGo = $this->Mdanhmuc->themDuLieu('tbl_vanbanden',$data_doc_go);
        if ($DocGo > 0) {
            return messagebox('Thêm thành công', 'info');
        } else {
            return messagebox('Thêm văn bản đến thất bại', 'danger');
        }
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $taikhoan = $this->_session['PK_iMaCB'];
        if($this->input->post('duyetvanban')){
            $Director = $this->input->post('giamdoc');
            $PDirector = $this->input->post('phogiamdoc');
            $Deparment = $this->input->post('phongchutri');
            $Doc_array= array();
            $i = 0;
            foreach($this->input->post('doc_id') as $key4 => $value4 ){
                $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDen',$value4,'tbl_chuyennhanvanban');
                if (!empty($Director[$key4])) {
                    $Doc_array[$i]['PK_iMaVBDen'] = $value4;
                    $Doc_array[$i]['iTrangThai_TruyenNhan'] = '3'; 
                    $Doc_array[$i]['iDuHop'] = _post('giamdocduhop')[$key4];
					$Doc_array[$i]['iPGDDuHop'] = _post('pgdduhop')[$key4];
                    $Doc_array[$i]['iPhongDuHop'] = (_post('phongduhop')) ? _post('phongduhop')[$key4] : 0;
                    $i++;
                } elseif (!empty($PDirector[$key4])) {
                    $Doc_array[$i]['PK_iMaVBDen'] = $value4;
                    $Doc_array[$i]['iTrangThai_TruyenNhan'] = '4'; 
                    $Doc_array[$i]['iDuHop'] = _post('giamdocduhop')[$key4];
					$Doc_array[$i]['iPGDDuHop'] = _post('pgdduhop')[$key4];
                    $Doc_array[$i]['iPhongDuHop'] = (_post('phongduhop')) ? _post('phongduhop')[$key4] : 0;
                    $i++;
                } elseif (!empty($Deparment[$key4])) {
                    $Doc_array[$i]['PK_iMaVBDen'] = $value4;
                    $Doc_array[$i]['iTrangThai_TruyenNhan'] = '5';
                    $Doc_array[$i]['iDuHop'] = _post('giamdocduhop')[$key4];
					$Doc_array[$i]['iPGDDuHop'] = _post('pgdduhop')[$key4];
                    $Doc_array[$i]['iPhongDuHop'] = (_post('phongduhop')) ? _post('phongduhop')[$key4] : 0;
                    $i++;
                }
				
            }
//            pr($Doc_array);
            $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_vanbanden',$Doc_array,'PK_iMaVBDen');
            /** @var mang chuyen van ban cho giam doc $Dir_array */
            $Dir_array= array();
            foreach($Director as $key1 => $value1 ){
                if(!empty($value1)){
                    $manggiamdoc = array(
                        'FK_iMaVBDen'     => $this->input->post('doc_id')[$key1],
                        'FK_iMaCB_Chuyen' => $taikhoan,
                        'FK_iMaCB_Nhan'   => $value1,
                        'sNoiDung'        => $this->input->post('chidaogiamdoc')[$key1],
                        'sThoiGian'       => date('Y-m-d H:i:s')
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$manggiamdoc);

                    $Dir_array['PK_iMaCBNhan'] = $value1;
                    $Dir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                    $Dir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key1];
                    $Dir_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                    $Dir_array['sMoTa'] = $this->input->post('chidaogiamdoc')[$key1];
                    $Dir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key1] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key1]);
                    $Dir_array['input_per'] = $this->_session['PK_iMaCB'];
                    $Dir_array['iGiayMoi'] = 1;
                    $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Dir_array);
                }
            }

            /** @var mang chuyen van ban cho pho giam doc $PDir_array */
            $PDir_array= array();
            foreach($PDirector as $key2 => $value2 ){
                if(!empty($value2)){
                    $mangphogiamdoc = array(
                        'FK_iMaVBDen'     => $this->input->post('doc_id')[$key2],
                        'FK_iMaCB_Chuyen' => $taikhoan,
                        'FK_iMaCB_Nhan'   => $value2,
                        'sNoiDung'        => $this->input->post('chidaophogiamdoc')[$key2],
                        'sThoiGian'       => date('Y-m-d H:i:s')
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangphogiamdoc);

                    $PDir_array['PK_iMaCBNhan'] = $value2;
                    $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                    $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key2];
                    $PDir_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                    $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$key2];
                    $PDir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key2] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key2]);
                    $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                    $PDir_array['iGiayMoi'] = 1;
                    $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
                }
            }

            /** @var mang chuyen phong chu tri va phoi hop $Departmanet_array */
            $Departmanet_array= array();
            foreach($Deparment as $key3 => $value3 ){
                if(!empty($value3)){
                    $phongbanHD  = $value3;
                    if($phongbanHD==11) {
                        $quyenhan       = array(3);
                        $chucvuphong    = array(6);
                        $quyendb        = '';
                    }
                    else{
                        $quyenhan       = array(6);
                        $chucvuphong    = '';
                        $quyendb        = '';
                    }
                    $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
                    $mangphongban = array(
                        'FK_iMaVBDen'     => $this->input->post('doc_id')[$key3],
                        'FK_iMaCB_Chuyen' => $taikhoan,
                        'FK_iMaCB_Nhan'   => $truongphong[0]['PK_iMaCB'],
                        'sNoiDung'        => $this->input->post('chidaophongchutri')[$key3],
                        'sThoiGian'       => date('Y-m-d H:i:s')
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangphongban);
                    
                    $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                    $Departmanet_array['PK_iMaPhongPH'] = $this->input->post('mangphoihop')[$key3];
                    $Departmanet_array['PK_iMaPhongCT'] = $value3;
                    $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key3];
                    $Departmanet_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                    $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$key3];
                    $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key3] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key3]);
                    $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                    $Departmanet_array['iGiayMoi'] = 1;
                    $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array);
                }
            }


            return redirect(base_url().'giaymoi_cvp');

        }
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'giaymoi_cvp?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mgiaymoichoxuly->getDocAwait('2',NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 200;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_pgd');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mgiaymoichoxuly->getDocAwait('2',NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function getDataDocGo()
    {
        $idDoc = $this->input->post('idDoc');
        $getDataDoc =  $this->Mvanbanden->layDuLieu('PK_iMaVBDen',$idDoc);
//        pr($getAppo);
        echo json_encode($getDataDoc);exit();
    }

}