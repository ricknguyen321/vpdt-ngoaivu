<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Capivbdi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();

    }
    public function index()
    {
		$ID = $this->input->get('ID');
		if(!empty($ID)){
			$abcd = $this->Mdanhmuc->layDuLieuVBDi('PK_iMaVBDi',$ID,'tbl_vanbandi');
			$data['doid'] = json_encode($abcd);
		}
		$idTypeOf = $this->input->get('MA');
		if(!empty($idTypeOf)){
			$abc = $this->Mdanhmuc->layDuLieuVBDi('sKyHieu',$idTypeOf,'tbl_vanbandi');
			$data['do'] = json_encode($abc);
		}
        $data['url'] = base_url();
        $this->parser->parse('vanbanden/Vapivbdi', $data, FALSE);
    }
    public function getData(){
        $idTypeOf = $this->input->get('id');
        $data = $this->Mdanhmuc->layDuLieuVBDi('sKyHieu',$idTypeOf,'tbl_vanbandi');
        echo json_encode($data);
        exit();
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */