<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandentumaildang extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('Vanbanden/Mvanbanden');
    }
    public function index()
    {
		$data['khuvuc']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_khuvuc');
		$data['loaivanban'] = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaivanban');
		$data['linhvuc']    = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_linhvuc');
		$data['domat']      = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_domat');
		$data['dokhan']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_dokhan');
		$data['chucvu']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_chucvunguoiky');
		$data['quytrinh']   = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_quytrinhiso');
		$action = _post('action');
        switch ($action) {
            case 'layNgayHetHan':
                $this->layNgayHetHan();
                break;
            case 'layChucVu':
                $this->layChucVu();
                break;
            default:
                # code...
                break;
        }
        $ma            = _get('id');
        $this->Mdanhmuc->setDuLieu('mail_id',$ma,'tbl_mail_dang','mail_status',1);
        $thongtin      = $this->Mdanhmuc->layDuLieu('mail_id',$ma,'tbl_mail_dang');
        $data['ktpdf'] = _get('pdf');
		$data['trichyeu']   = $thongtin[0]['mail_subject'];
		$data['noigui']   = $thongtin[0]['mail_from'];
        $data['pdf']   = 'emaildangFile_'.substr($thongtin[0]['mail_date'],0,4).'/'._get('pdf');
		$data['mail_date']   = $thongtin[0]['mail_date'];
		$data['mail_files']   = $thongtin[0]['mail_files'];
        $xml           = _get('xml');
        $getAppo       = $this->Mdanhmuc->laySodenCuoi($_SESSION['nam']);
        $data['soden'] = $getAppo['iSoDen']+1;
        $opts          = array('http' => array('header' => 'Accept-Charset: UTF-8, *;q=0'));
        $context       = stream_context_create($opts);
        $xml           = file_get_contents('emaildangFile_'.substr($thongtin[0]['mail_date'],0,4).'/'.$xml); 
        $string        = preg_replace('/[\x00-\x1F\x7F]/u', '', $xml);  
        if(empty($string))  
        {
             $string   = iconv('UTF-16LE', 'UTF-8', $xml);
        }
        $data['xml']   = simplexml_load_string($string);
		if(isset($data['xml']->NOIGUI)){
            $get_xml     = $data['xml'];
            $data['xml'] = $data['xml']->NOIDUNGVANBAN;
        }
        if(_post('luulai'))
        {
            $loaivanban = trim(_post('loaivanban'));
            $sokyhieu   = trim(_post('sokyhieu'));
            $noigui     = trim(_post('noiguiden'));
            $ngayky     = date_insert(_post('ngayky'));
            $nguoiky    = trim(_post('nguoiky'));
            $vanbanmat  = (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0';
            $kiemtra = $this->Mdanhmuc->kiemtraTrung_VBD($sokyhieu,$ngayky,$nguoiky,$loaivanban);
			//$kiemtra = $this->Mdanhmuc->kiemtra_Trung($sokyhieu,$noigui,$nguoiky);
            if($kiemtra==0)
            {
                $this->themDuLieu();
            }
            else{
                $data['content'] = messagebox('Văn bản này đã tồn tại trong hệ thống!','danger');
            }
        }
		$data['title']      = 'Nhập văn bản từ hệ thống mail Chi ủy';
		$temp['data']       = $data;
		$temp['template']   = 'vanbanden/Vvanbandentumaildang';
		$this->load->view('layout_admin/layout',$temp);
    }
	public function createXML_PH($get_xml)
    {
        $dir      = 'tmpxml/';
        $xml      = new DOMDocument("1.0",'UTF-8');

        $STRMADONVI = $xml->createElement('STRMADONVI');
        $STRMADONVI_Text = $xml->createTextNode('000.00.13.H26');
        $STRMADONVI->appendChild($STRMADONVI_Text);

        $STRTENDONVI = $xml->createElement('STRTENDONVI');
        $STRTENDONVI_Text = $xml->createTextNode('Sở Tài Chính');
        $STRTENDONVI->appendChild($STRTENDONVI_Text);

        $STRDIACHI = $xml->createElement('STRDIACHI');

        $STRDIENTHOAI = $xml->createElement('STRDIENTHOAI');

        $STREMAIL = $xml->createElement('STREMAIL');

        $STRWEBSITE = $xml->createElement('STRWEBSITE');

        $STRFAX = $xml->createElement('STRFAX');
        $STRSOKYHIEU = $xml->createElement('STRSOKYHIEU');
        $STRSOKYHIEU_Text = $xml->createTextNode($get_xml->NOIDUNGVANBAN->STRKYHIEU);
        $STRSOKYHIEU->appendChild($STRSOKYHIEU_Text);

        $STRNGAYKY = $xml->createElement('STRNGAYKY');
        $STRNGAYKY_Text = $xml->createTextNode($get_xml->NOIDUNGVANBAN->STRNGAYKY);
        $STRNGAYKY->appendChild($STRNGAYKY_Text);

        $STRMACOQUANBANHANH = $xml->createElement('STRMACOQUANBANHANH');
        $STRMACOQUANBANHANH_Text = $xml->createTextNode($get_xml->NOIGUI->STRMADONVI);
        $STRMACOQUANBANHANH->appendChild($STRMACOQUANBANHANH_Text);

        $STRCOQUANBANHANH = $xml->createElement('STRCOQUANBANHANH');
        $STRCOQUANBANHANH_Text = $xml->createTextNode($get_xml->NOIGUI->STRTENDONVI);
        $STRCOQUANBANHANH->appendChild($STRCOQUANBANHANH_Text);

        $STRDONVIXULY = $xml->createElement('STRDONVIXULY');
        $STRDONVIXULY_Text = $xml->createTextNode('Văn thư Sở Tài Chính');
        $STRDONVIXULY->appendChild($STRDONVIXULY_Text);

        $STRCANBOXULY = $xml->createElement('STRCANBOXULY');

        $STRDIENTHOAI_XL = $xml->createElement('STRDIENTHOAI');

        $STREMAIL_XL = $xml->createElement('STREMAIL');

        $STRMATRANGTHAI = $xml->createElement('STRMATRANGTHAI');
        $STRMATRANGTHAI_Text = $xml->createTextNode('1');
        $STRMATRANGTHAI->appendChild($STRMATRANGTHAI_Text);

        $STRNOIDUNGXULY = $xml->createElement('STRNOIDUNGXULY');
        $STRNOIDUNGXULY_Text = $xml->createTextNode('Đã tiếp nhận vào hệ thống');
        $STRNOIDUNGXULY->appendChild($STRNOIDUNGXULY_Text);

        
        $book   = $xml->createElement("EXPORTSTATUS");
        $NOIGUI     = $xml->createElement("NOIGUI");
        $NOIGUI->appendChild($STRMADONVI);
        $NOIGUI->appendChild($STRTENDONVI);
        $NOIGUI->appendChild($STRDIACHI);
        $NOIGUI->appendChild($STRDIENTHOAI);
        $NOIGUI->appendChild($STREMAIL);
        $NOIGUI->appendChild($STRWEBSITE);
        $NOIGUI->appendChild($STRFAX);

        $PHANHOIVANBAN = $xml->createElement('PHANHOIVANBAN');
        $PHANHOIVANBAN->appendChild($STRSOKYHIEU);
        $PHANHOIVANBAN->appendChild($STRNGAYKY);
        $PHANHOIVANBAN->appendChild($STRMACOQUANBANHANH);
        $PHANHOIVANBAN->appendChild($STRCOQUANBANHANH);

        $THONGTINXULY   = $xml->createElement("THONGTINXULY");
        $THONGTINXULY->appendChild($STRDONVIXULY);
        $THONGTINXULY->appendChild($STRCANBOXULY);
        $THONGTINXULY->appendChild($STRDIENTHOAI_XL);
        $THONGTINXULY->appendChild($STREMAIL_XL);

        $book->appendChild($NOIGUI);
        $book->appendChild($PHANHOIVANBAN);
        $book->appendChild($THONGTINXULY);
        $book->appendChild($STRMATRANGTHAI);
        $book->appendChild($STRNOIDUNGXULY);
        
        $xml->appendChild($book);
        $xml->formatOutput = true;
        $xml->save($dir.'XML_Output.sdk') or die("Error");
    }
    public function sendMail($get_xml)
    {
        $kyhieu = $get_xml->NOIDUNGVANBAN->STRKYHIEU;
        $ngayky = $get_xml->NOIDUNGVANBAN->STRNGAYKY;
        $name       = 'Văn thư Sở Tài chính';
        $from_email = 'vanthu_sotc@hanoi.gov.vn';
        $subject    = 'Phản hồi trạng thái văn bản ('.$kyhieu.' ngày '.$ngayky.'): Văn bản đã đến văn thư'; // nội dung gửi mail
        $message    = $subject;
        $to_email   = 'gnvbdtquatruc@hanoi.gov.vn';//$noinhan;// mail nhận
        //configure email settings
        $config['protocol']  = 'smtp';
        // $config['smtp_host'] = 'mail.thudo.gov.vn';
        // $config['smtp_port'] = '587';
        $config['smtp_host'] = 'ssl://mail.thudo.gov.vn';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'vanthu_sotc@hanoi.gov.vn';// email gửipthtk_sotc@hanoi.gov.vn
        $config['smtp_pass'] = 'sotchanoi042018';// pass cua mail trentinhoc@gmail.com
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';
        $config['wordwrap']  = TRUE;
        $config['newline']   = "\r\n"; //use double quotes
        $this->load->library('email', $config);
        $this->email->initialize($config);
        //send mail
        $this->email->clear(TRUE);
        $this->email->from($from_email, $name);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        $filename = 'tmpxml/XML_Output.sdk';
        $this->email->attach($filename);
        if ($this->email->send(FALSE)) {
        }
        else{
        }
    }
    // lấy chức vụ người ký
    public function layChucVu()
    {
        $hoten = _post('hoten');
        $chucvu = $this->Mdanhmuc->layDuLieu('sHoTen',$hoten,'tbl_nguoiky');
        if(!empty($chucvu))
        {
            $chucvunguoiky= $chucvu[0]['sChucVu'];
        }
        else{
            $chucvunguoiky ='';
        }
        echo json_encode($chucvunguoiky);exit();
    }
    // lấy ngày hết hạn
    public function layNgayHetHan()
    {
        $loaivanban = _post('loaivanban');
        $ngaynhan   = (_post('ngaynhan'))?_post('ngaynhan'):date('d/m/Y');
        $songay     = _post('songay');        
        $ngaynghi   = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_ngaynghi');
        $i          =0;
        foreach ($ngaynghi as $key => $value) {
            if($value['sNgayNghi']!=date_insert($ngaynhan))
            {
                if(date_insert($ngaynhan) <= $value['sNgayNghi'] && $value['sNgayNghi'] <= dateFromBusinessDays((int)$songay,$ngaynhan)){
                    $i++;
                }
            }
            
        }
        //if($loaivanban=='Quyết định' || $loaivanban=='Giấy mời')
       // {
         //   $hangiaiquyet = '';
        //}
       // else{
        $hangiaiquyet = date_select(dateFromBusinessDays((int)$songay+$i,$ngaynhan));
       // }
        echo json_encode($hangiaiquyet); exit();
        
    }
    public function themDuLieu(){
		if(isset($get_xml->NOIGUI)){
            // file theo chuẩn mới
           // $this->createXML_PH($get_xml);
           // $this->sendMail($get_xml);
        }
        else{
            // file theo chuẩn cũ
        }
        // if($this->_session['PK_iMaCB'] == 577){
        //         sleep(0.3);
        //     }
        //     if($this->_session['PK_iMaCB'] == 136){
        //         sleep(0.2);
        //     }
        //     if($this->_session['PK_iMaCB'] == 81){
        //         sleep(0.1);
        //     }
        $giomoi = _post('giohop1');		
		
        $getAppo = $this->Mvanbanden->layDuLieuEnd(NULL,NULL,$_SESSION['nam']);
        $soden = $getAppo['iSoDen']+1;
        $loaivanban = _post('loaivanban');
        if($loaivanban=='Giấy mời')// giấy mời
        {
			$hannoidung = _post('hannoidung');
            $hanxuly    = _post('hangiaiquyet');
            $noidunggiaymoi = _post('noidunggiaymoi');
            $diadiemhop     = _post('diadiemhop');
            $giohop         = _post('giohop');
            $ngayhop        = _post('ngayhop');

            $diadiemhop1    = _post('diadiemhop1');
            $giohop1        = _post('giohop1');
				//ricknguyen321 xử lý định dạng giờ
				$giohop1 = str_replace('h', ':', $giohop1);
				$giohop1 = str_replace(' giờ ', ':', $giohop1);
				$giohop1 = str_replace('giờ', ':', $giohop1);
				$giohop1 = str_replace('phút', '', $giohop1);
				$giohop1 = str_replace(' : ', ':', $giohop1);
				$manggio = explode(':',$giohop1);
				if (count($manggio) == 2) {
					$gio = $manggio[0];
					$phut = $manggio[1];			
					if ($gio == '1') $gio = '01';
					if ($gio == '2') $gio = '02';
					if ($gio == '3') $gio = '03';
					if ($gio == '4') $gio = '04';
					if ($gio == '5') $gio = '05';
					if ($gio == '6') $gio = '06';
					if ($gio == '7') $gio = '07';
					if ($gio == '8') $gio = '08';
					if ($gio == '9') $gio = '09';
					if ($phut == '0') $phut = '00';
					if ($phut == '1') $phut = '01';
					if ($phut == '2') $phut = '02';
					if ($phut == '3') $phut = '03';
					if ($phut == '4') $phut = '04';
					if ($phut == '5') $phut = '05';
					if ($phut == '6') $phut = '06';
					if ($phut == '7') $phut = '07';
					if ($phut == '8') $phut = '08';
					if ($phut == '9') $phut = '09';			
					$giohop1 = $gio.':'.$phut;
				}
            $ngayhop1       = _post('ngayhop1');
            $tong =  count($noidunggiaymoi);
            foreach ($noidunggiaymoi as $key => $value) {
                if($tong==1 || ($tong>1 && !empty($value)))
                {
					
					//ricknguyen321 xử lý định dạng giờ
					$giohop[$key] = str_replace('h', ':', $giohop[$key]);
					$giohop[$key] = str_replace(' giờ ', ':', $giohop[$key]);
					$giohop[$key] = str_replace('giờ', ':', $giohop[$key]);
					$giohop[$key] = str_replace('phút', '', $giohop[$key]);
					$giohop[$key] = str_replace(' : ', ':', $giohop[$key]);
					$manggio = explode(':',$giohop[$key]);
					if (count($manggio) == 2) {
						$gio = $manggio[0];
						$phut = $manggio[1];			
						if ($gio == '1') $gio = '01';
						if ($gio == '2') $gio = '02';
						if ($gio == '3') $gio = '03';
						if ($gio == '4') $gio = '04';
						if ($gio == '5') $gio = '05';
						if ($gio == '6') $gio = '06';
						if ($gio == '7') $gio = '07';
						if ($gio == '8') $gio = '08';
						if ($gio == '9') $gio = '09';
						if ($phut == '0') $phut = '00';
						if ($phut == '1') $phut = '01';
						if ($phut == '2') $phut = '02';
						if ($phut == '3') $phut = '03';
						if ($phut == '4') $phut = '04';
						if ($phut == '5') $phut = '05';
						if ($phut == '6') $phut = '06';
						if ($phut == '7') $phut = '07';
						if ($phut == '8') $phut = '08';
						if ($phut == '9') $phut = '09';			
						$giohop[$key] = $gio.':'.$phut;
					}
					
                    $data = array(
                        'iVanBanQPPL'           => (_post('qppl'))?_post('qppl'):0,
                        'iSTCChuTri'            => (_post('stc'))?_post('stc'):0,
                        'iVanBanTBKL'           => (_post('tbkl'))?_post('tbkl'):0,
                        'iVanBanMat'            => (_post('vbm'))?_post('vbm'):0,
                        'iToCongTac'            => (!empty(_post('iToCongTac'))) ? _post('iToCongTac') : 0,
                        'sTenKV'                => _post('khuvuc'),
                        'sTenDV'                => _post('noiguiden'),
                        'sKyHieu'               => _post('sokyhieu'),
                        'sNgayKy'               => date_insert(_post('ngayky')),
                        'sTenLVB'               => 'Giấy mời',
                        'iTrangThai_TruyenNhan' => '2',
                        'sTenLV'                => _post('linhvuc'),
                        'sMoTa'                 => _post('trichyeu'),
                        'sGiayMoiGio'           => ($giohop[$key])?$giohop[$key]:$giohop1,
                        'sGiayMoiNgay'          => ($ngayhop[$key])?date_insert($ngayhop[$key]):date_insert($ngayhop1),
                        'sGiayMoiDiaDiem'       => ($diadiemhop[$key])?$diadiemhop[$key]:$diadiemhop1,
                        'sGiayMoiChuTri'        => _post('nguoichutri'),
                        'sTenNguoiKy'           => _post('nguoiky'),
                        'sChucVu'               => _post('chucvu'),
						'sHanGiaiQuyet'         => ($ngayhop[$key])?date_insert($ngayhop[$key]):date_insert($ngayhop1),
						'sHanThongKe'           => ($ngayhop[$key])?date_insert($ngayhop[$key]):date_insert($ngayhop1),
                        //'sHanGiaiQuyet'         => ($hannoidung[$key]&&$hannoidung[$key]!='dd/mm/yyyy')?date_insert($hannoidung[$key]):((!empty(_post('iSTCPhoiHop'))) ? '0000-00-00' :date_insert($hanxuly)),
                        //'sHanThongKe'           => ($hannoidung[$key]&&$hannoidung[$key]!='dd/mm/yyyy')?date_insert($hannoidung[$key]):((!empty(_post('iSTCPhoiHop'))) ? '0000-00-00' :date_insert($hanxuly)),
						'iDeadline'  			=> _post('iDeadline'),
						'hanvb'  				=> _post('hanvb'),
                        'sNoiDung'              => $value,
                        'iSoTrang'              => _post('sotrang'),
                        'iSoDen'                => $soden,
                        'sNgayNhan'             => date_insert(_post('ngaynhan')),
                        'FK_iMaNguoiNhap'       => $this->_session['PK_iMaCB'],
                        'FK_iMaDM'              => _post('domat'),
                        'FK_iMaDK'              => _post('dokhan'),
                        'sNgayNhap'             => date('Y-m-d H:i:s',time()),
                        'iGiayMoi'              => 1,
                        'iMail'                 => 2
                    );
                    $kiemtra  = $this->Mdanhmuc->themDuLieu2('tbl_vanbanden',$data);
                }
                $duongdan = _post('duongdanfile');
				$tenfile = substr($duongdan,0, 60);
                $data_file=array(
                    'FK_iMaVBDen'    => $kiemtra,
                    'sTenFile'       => $tenfile,
                    'sDuongDan'      => $duongdan,
                    'sThoiGian'      => date('Y-m-d H:i:s'),
                    'docFile_active' => 1,
                    'FK_iMaCB'       => $this->_session['PK_iMaCB']
                    );
                if(!empty($duongdan))
                {
                    $this->Mdanhmuc->themDuLieu('tbl_files_vbden',$data_file);
                    $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$kiemtra,'tbl_vanbanden','iFile',1);
                }
            }
            redirect('readmaildang?trangthai=0');
        }
        else // văn bản bình thường
        {
            $noidunghop = _post('noidunghop');
            $hannoidung = _post('hannoidung');
            $hanxuly    = _post('hangiaiquyet');
			
            if(empty($hanxuly) && $hanxuly =='dd/mm/yyyy')
            {
                $hanxuly   = '00/00/0000';
            }
			
            $tong = count($noidunghop);
            foreach ($noidunghop as $key => $value) {
                if($tong==1 || ($tong>1 && !empty($value)))
                {
                    $data = array(
                        'iVanBanQPPL'           => (_post('qppl'))?_post('qppl'):0,
                        'iSTCChuTri'            => (_post('stc'))?_post('stc'):0,
                        'iVanBanTBKL'           => (_post('tbkl'))?_post('tbkl'):0,
                        'iVanBanMat'            => (_post('vbm'))?_post('vbm'):0,
                        'iToCongTac'            => (!empty(_post('iToCongTac'))) ? _post('iToCongTac') : 0,
						'iSTCPhoiHop' 			=> 1,
                        'sTenKV'                => _post('khuvuc'),
                        'sTenDV'                => _post('noiguiden'),
                        'sKyHieu'               => _post('sokyhieu'),
                        'sNgayKy'               => date_insert(_post('ngayky')),
                        'sTenLVB'               => _post('loaivanban'),
                        'iTrangThai_TruyenNhan' => '2',
                        'sTenLV'                => _post('linhvuc'),
                        'sMoTa'                 => _post('trichyeu'),
                        'sTenNguoiKy'           => _post('nguoiky'),
                        'sChucVu'               => _post('chucvu'),
                        'sHanGiaiQuyet'         => ($hannoidung[$key]&&$hannoidung[$key]!='dd/mm/yyyy')?date_insert($hannoidung[$key]):((!empty(_post('iSTCPhoiHop'))) ? '0000-00-00' :date_insert($hanxuly)),
                        'sHanThongKe'           => ($hannoidung[$key]&&$hannoidung[$key]!='dd/mm/yyyy')?date_insert($hannoidung[$key]):((!empty(_post('iSTCPhoiHop'))) ? '0000-00-00' :date_insert($hanxuly)),
                        'iDeadline'  			=> _post('iDeadline'),
						'hanvb'  				=> _post('hanvb'),
                        'sNoiDung'              => $value,
                        'iSoTrang'              => _post('sotrang'),
                        'iSoDen'                => $soden,
                        'sNgayNhan'             => date_insert(_post('ngaynhan')),
                        'FK_iMaNguoiNhap'       => $this->_session['PK_iMaCB'],
                        'FK_iMaDM'              => _post('domat'),
                        'FK_iMaDK'              => _post('dokhan'),
                        'sNgayNhap'             => date('Y-m-d H:i:s',time()),
                        'iGiayMoi'              => 2,
                        'iMail'                 => 2
						
                    );
                    $kiemtra  = $this->Mdanhmuc->themDuLieu2('tbl_vanbanden',$data);
                }
				
				$mail_date = _post('mail_date');
				$mail_files = _post('mail_files');
				$mail_files = explode(";",$mail_files);
				$k = 0;
				foreach($mail_files as $mail_file){
					if (!empty($mail_file) && $mail_file != '' && $mail_file != ' ' && 'sdk' != $this->filename_extension($mail_file) && 'SDK' != $this->filename_extension($mail_file)) {
						$mail_file = trim($mail_file);
						$duongdan = base_url().'emaildangFile_'.substr($mail_date,0,4).'/'.$mail_file;
						$data_file=array(
							'FK_iMaVBDen'    => $kiemtra,
							'sTenFile'       => substr($mail_file,0, 60),
							'sDuongDan'      => $duongdan,
							'sThoiGian'      => date('Y-m-d H:i:s'),
							'docFile_active' => 1,
							'FK_iMaCB'       => $this->_session['PK_iMaCB']
							);
						$this->Mdanhmuc->themDuLieu('tbl_files_vbden',$data_file);
						$k = 1;
					}
				}
				if($k == 1){
					$this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$kiemtra,'tbl_vanbanden','iFile',1);
                }
            }
            redirect('readmaildang?trangthai=0');
        }
	}
	
	function filename_extension($filename) {
	    $pos = strrpos($filename, '.');
	    if($pos===false) {
	        return false;
	    } else {
	        return substr($filename, $pos+1);
	    }
	}
}

/* End of file Cvanbandentumail.php */
/* Location: ./application/controllers/vanbanden/Cvanbandentumail.php */