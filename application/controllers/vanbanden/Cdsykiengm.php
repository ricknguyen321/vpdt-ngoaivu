<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 3:13 PM
 */
class Cdsykiengm extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->load->library('pagination');
    }
    public function index()
    {
//        pr($gd);
        // danh sach cho xu ly
        /** @var danh sách tìm kiếm phân trang $page */
		if($this->input->post('daxem')){
            $data['content'] = $this->markDaXem($this->input->post('daxem'));
        }
        $page               = $this->PhanTrang();
        $data['dsykien']    = $page['info'];
//            pr($this->_session);
        
		$data['phantrang']     = $page['pagination'];
       
        $data['count'] = $page['count'];

//        pr($data['processcvph']);
        $data['title']    = 'Danh sách ý kiến, đề xuất';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vdsykien';
        $this->load->view('layout_admin/layout',$temp);
    }
	
	public function markDaXem($idYKien){
		echo $idYKien;
        $data2 = array(
			'id_ykien'   => $idYKien,
			'time'   => date('Y-m-d H:i:s', time()),
			'id_cb'   => $this->_session['PK_iMaCB']
        );
		$this->Mdanhmuc->themDuLieu('tbl_ykien_cb',$data2);        
		return redirect(base_url().'dsykien');     
    }
	
    public function PhanTrang()
    {
       
        $config['base_url']             = base_url().'dsykien';
        $config['total_rows']           = count($this->Mgiaymoichoxuly->layDSYKien($this->_session['PK_iMaCB'],$this->_session['sHoTen'],$config['per_page'], $data['page']));
//        pr($config['total_rows']);
        $config['per_page']             = 100;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_cvph');
        }

        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mgiaymoichoxuly->layDSYKien($this->_session['PK_iMaCB'],$this->_session['sHoTen'],$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}