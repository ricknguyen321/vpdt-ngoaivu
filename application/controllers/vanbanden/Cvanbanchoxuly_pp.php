<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 10:50 AM
 */
class Cvanbanchoxuly_pp extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
        $this->load->model('vanbandi/Mvanbandi');
        $this->load->model('Vanbanden/Mvanbanphoihop');
    }
    public function index()
    {
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $taikhoan   = $this->_session['PK_iMaCB'];
        $phongbanHD = $this->_session['FK_iMaPhongHD'];
        $quyendb    = $this->_session['iQuyenDB'];
        $chucvu     = $this->_session['FK_iMaCV'];
        $quyen      = $this->_session['iQuyenHan_DHNB'];
        if($phongbanHD==12)
        {
            $chucvuphong    = '';
            $quyendb        = $quyendb;
        }
        else{
            $chucvuphong    = '';
            $quyendb        = '';
        }
        $data['getDepartment'] = $this->Mvanbandi->layLD_Phong($phongbanHD,8,$chucvuphong,$quyendb);
        //danh sách chờ xử lý
        if($this->input->post('tuchoitp')){
            $data['content'] = $this->Rejecttp();
        }
        if($this->input->post('themhan')){
            $data['content'] = $this->insertDateline();
        }
		
		$data['dsldduyet']        = $this->Mvanbanchoxuly->layldduyet($this->_session['FK_iMaPhongHD']);

        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];//pr($data['getDocAwait'] );
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $process = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen'],NULL,NULL,'8','1');
                $processpp = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen'],NULL,NULL,'7');
//                pr($process);
                if(!empty($process)){
                    $data['getDocAwait'][$key]['PK_iMaCVCT'] = $process[0]['PK_iMaCVCT'];
                    $data['getDocAwait'][$key]['MoTaCVCT'] = $process[0]['sMoTa'];
                    $data['getDocAwait'][$key]['PK_iMaCVPH'] = $process[0]['PK_iMaCVPH'];
                    $data['getDocAwait'][$key]['PK_iMaCNCT'] = $process[0]['PK_iMaCN'];
                }
                if(empty($process)){
                    $data['getDocAwait'][$key]['PK_iMaCVCT'] = '';
                    $data['getDocAwait'][$key]['MoTaCVCT'] = '';
                }
                if(!empty($processpp)){
                    $data['getDocAwait'][$key]['PK_iMaPP'] = $processpp[0]['PK_iMaCVCT'];
                    $data['getDocAwait'][$key]['MoTaPP'] = $processpp[0]['sMoTa'];
                    $data['getDocAwait'][$key]['PK_iMaCNPP'] = $processpp[0]['PK_iMaCN'];
                    $data['getDocAwait'][$key]['sHanChoPP'] = $processpp[0]['sThoiGianHetHan'];
                }
                if(empty($processpp)){
                    $data['getDocAwait'][$key]['PK_iMaPP'] = '';
                    $data['getDocAwait'][$key]['MoTaPP'] = '';
                }
				if($this->_session['FK_iMaPhongHD'] == 11){
                    $idTP = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }elseif($this->_session['iQuyenHan_DHNB'] == 7 && $this->_session['FK_iMaPhongHD'] != 12){
                    $idTP = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }elseif($this->_session['FK_iMaPhongHD'] == 12){
                    $idTP = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }
                if(!empty($idTP)){
                    $data['getDocAwait'][$key]['ChiDao'] = $idTP;
                    $data['getDocAwait'][$key]['PK_iMaLDCD'] = $idTP1;
                }
//                pr($arrayCBNhan);
            }
//            pr($data['getDocAwait']);
        }
//        pr($data['getDocAwait'] );
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];

        // duyệt văn bản chờ xử lý
        if($this->input->post('duyet')) {
            $this->insertDocAwait();
        }
        $data['title']    = 'Văn bản đến của Phó phòng';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vvanbanchoxuly_pp';
        $this->load->view('layout_admin/layout',$temp);
    }
	public function Rejecttp(){
        $idDoc = $this->input->post('maphongbantp');
        $taikhoan    = $this->_session['PK_iMaCB'];
        $quyendb     = $this->_session['iQuyenDB'];
        $phongbanHD  = $this->_session['FK_iMaPhongHD'];
        if($phongbanHD==12)
        {
            $quyenhan       = array(11);
            $chucvuphong    = '';
            $quyendb        = $quyendb;
        }
        elseif ($phongbanHD==11) {
            $quyenhan       = array(3);
            $chucvuphong    = array(6);
            $quyendb        = '';
        }
        else{
            $quyenhan       = array(6);
            $chucvuphong    = '';
            $quyendb        = '';
        }
        $truongphong = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $idDoc,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => $truongphong[0]['PK_iMaCB'],
            'sNoiDung'        => _post('noidungtuchoi'),
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
        
        $data_reject = array(
            'PK_iMaVBDen' => $idDoc,
            'PK_iMaCBTuChoi' => $this->_session['PK_iMaCB'],
            'sTenCB'   => $this->_session['sHoTen'],
            'sLyDo'    => $this->input->post('noidungtuchoi'),
            'sGhiChu'  => $this->input->post('ghichu'),
			'PK_iMaPB' => $this->_session['FK_iMaPhongHD'],
            'iTrangThai' => '1',
            'sThoiGianTao' => date('Y-m-d H:i:s',time())
        );
        $data_doc = array(
            'iTrangThai_TruyenNhan' => ($phongbanHD==12)?6:5,
			'sLyDoTuChoiKetQua' => '',
            // 'sLyDoTuChoiVanBanDen' => ' ',
            'iTrangThai' => '0'
        );
        if($phongbanHD==12)
        {
            $laymachuyennhan = $this->Mvanbanphoihop->layMaChuyenNhan($taikhoan,$idDoc);
            $laymachuyennhan = $laymachuyennhan['PK_iMaCN'];
            $manguoichuyen   = $laymachuyennhan['PK_iMaCBChuyen'];

            $laytruongphong  = $this->Mvanbanphoihop->layMaChuyenNhan($manguoichuyen,$idDoc);
            $laytruongphong  = $laytruongphong['PK_iMaCBChuyen'];

            $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden','PK_iMaCBDuyet',$laytruongphong);
            
            $this->Mvanbanphoihop->xoaChuyenNhan($laymachuyennhan,$idDoc);
            $this->Mdanhmuc->setDuLieu('doc_id',$idDoc,'tbl_chicuc','tcdn_active','3');
        }
        $this->Mdanhmuc->themDuLieu('tbl_vanban_chuyenlai',$data_reject);
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
        if ($DocGo > 0) {
			redirect('vanbanchoxuly_pp');
            return messagebox('Chuyền văn bản về trưởng phòng thành công', 'info');
        } else {
            return messagebox('Chuyền văn bản về trưởng phòng thất bại', 'danger');
        }
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $taikhoan    = $this->_session['PK_iMaCB'];
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $ma,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => _post('phongchutri')[$ma],
            'sNoiDung'        => _post('chidaophongchutri')[$ma],
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
        $process = $this->Mvanbanchoxuly->laylanhdaosaucung($ma);
//        if($this->input->post('duyetvanban')){
//            foreach ($this->input->post('duyet') as $ma => $value5){
//                if(!empty($value5)){
                    // chuyen vien chu tri
                    $staff = $this->input->post('phongchutri');
                    //mang phoi hop
                    $DepartmentPH = $this->input->post('mangphoihop');
                    // đánh dấu văn bản có đầu ra
                    $vbdaura_pp = $this->input->post('vbdaura_pp'.$ma);
                    $vbdaura_pp = ($vbdaura_pp)?$vbdaura_pp:1;
                    $Doc_array= array();
                    if (!empty($staff[$ma])) {
                        $Doc_array['iTrangThai_TruyenNhan'] = '7';
                        $Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
                    }
                    if($vbdaura_pp >0){
                       $Doc_array['vbdaura_pp'] = $vbdaura_pp; 
                    }
                    $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$this->input->post('doc_id')[$ma],'tbl_vanbanden',$Doc_array);
                    if(!empty($staff[$ma])) {
                        $this->Mvanbanchoxuly->xoaDuLieu3Where('PK_iMaVBDen',$ma,'PK_iMaPhong', $this->_session['FK_iMaPhongHD'],'iTrangThai_PPPH',0,'tbl_phongphoihop');
                        /** @var mang chuyen chuyen vien $Departmanet_array */
                        // van ban phoi hop giua cac chuyen vien vowi nhau
                        $Departmanet_array= array();
                        $mangph = $this->input->post('mangphoihop')[$ma];
                        $cvphoihop = explode( ',', $mangph );
//                        pr($cvphoihop);
                        if(!empty($DepartmentPH[$ma])){
                            foreach($cvphoihop as $key => $value){
                                $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                                $Departmanet_array['PK_iMaCB'] = $value;
                                $Departmanet_array['PK_iMaCVCT'] = $staff[$ma];
                                $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                                $Departmanet_array['sThoiGian'] = date('Y-m-d H:i:s',time());
                                $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                                $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                                $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                                $Departmanet_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                                $this->Mdanhmuc->themDuLieu('tbl_phongphoihop',$Departmanet_array);
                            }
//                            pr($Departmanet_array);
                        }
                        // chuyen vien chu tri ko phoi hop
                        $Departmanet_array1= array();
                        $cvct = $this->input->post('cvct');
                        if(!empty($cvct[$ma])){
                            $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVCT'] = $staff[$ma];
                            $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                            $Departmanet_array1['sThoiGian'] = date('Y-m-d H:i:s',time());
                            $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                            $Departmanet_array1['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                            $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['CapGiaiQuyet'] = '8';
                            $Departmanet_array1['PK_iMaCVPH'] = $mangph;
                            $this->Mvanbanchoxuly->updateCBCT($ma,$cvct[$ma],$Departmanet_array1);
                        }elseif(empty($cvct[$ma])){
                            $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVCT'] = $staff[$ma];
                            $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                            $Departmanet_array1['sThoiGian'] = date('Y-m-d H:i:s',time());
                            $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                            $Departmanet_array1['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$ma]);
                            $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVPH'] = $mangph;
                            $Departmanet_array1['CapGiaiQuyet'] = '8';
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array1);
                        }
						if($vbdaura_pp == 1){
							$munber_week = $this->Mdanhmuc->getTuanMax($staff[$ma])['tuan'];
							$tuan_hientai = (int)date("W");
							if($munber_week == $tuan_hientai){
								$cong = 1;
							}else{
								$cong = 0;
							}
                        //kế hoạch công tác
                        $kehoach_data_cv = array(
                            'vanban_id' => $this->input->post('doc_id')[$ma],
                            'kh_noidung' => $this->input->post('noidungvb')[$ma],
							'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
							'vanban_skh' => $this->input->post('sohieu')[$ma],
                            'tuan' => (int)date("W")+$cong,
                            'ngay_nhan' => date('Y-m-d H:i:s',time()),
                            'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                            'canbo_id' => $staff[$ma],
                            'thuc_hien' => '3',
							'loai_kh' => 1,
							'chucvu'  => 8,
                            'lanhdao_id' => $this->_session['PK_iMaCB'],
							'user_input' => $this->_session['PK_iMaCB'],
                            'lanhdao_so' => $process['PK_iMaCB'],
                            'phong_id' => $this->_session['FK_iMaPhongHD']
                        );
//                        pr($kehoach_data);
                        $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);

                        //phó phối hợp
                        /*if(!empty($DepartmentPH[$ma])){
                            $mangph = $this->input->post('mangphoihop')[$ma];
                            $cvphoihop = explode( ',', $mangph );
                            foreach($cvphoihop as $key => $value){
                                $kehoach_data_cvph = array(
                                    'vanban_id' => $this->input->post('doc_id')[$ma],
                                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
									'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
									'vanban_skh' => $this->input->post('sohieu')[$ma],
                                    'tuan' => date("W"),
                                    'thuc_hien' => '2',
                                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                                    'canbo_id' => $value,
									'loai_kh' => 1,
									'chucvu'  => 8,
                                    'lanhdao_id' => $this->_session['PK_iMaCB'],
									'user_input' => $this->_session['PK_iMaCB'],
                                    'lanhdao_so' => $process['PK_iMaCB'],
                                    'phong_id' => $this->_session['FK_iMaPhongHD']
                                );

                                $this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_cvph);
                            }
                        }*/
}                        
            return redirect(base_url().'vanbanchoxuly_pp');

        }
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'vanbanchoxuly_pp?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwait('6',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,NULL,NULL,NULL,NULL,1));
//        pr($config['total_rows']);
        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_pp');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwait('6',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page'],NULL,NULL,1);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function insertDateline(){
        $idDoc = $this->input->post('mavbd');
        $idLD = $this->input->post('ldgui');
		$ldduyet = $this->input->post('ldduyet');
		$noidung = $this->input->post('noidungtuchoi');
		$noidung = nl2br($noidung);
        $data_han = array(
            'iLanhDao' => (!empty($ldduyet))?$ldduyet:$idLD,
            'iTrangThai_ThemHan'   => 1,
            'sHanChoDuyet'    => date_insert($this->input->post('handexuat')),
            'sNoiDungGiaHan'    => '<span>'.$noidung.'</span><br/>'.'(<b>'.$this->_session['sHoTen'].'</b>)'.'<br/>'
        );
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
		
		$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 1,
				'FK_iMaCB_Gui'   => $this->_session['PK_iMaCB'],
				'sHanThongKe' => date_insert($this->input->post('handexuat')),
				'noidunggiahan'    => $noidung,
				'ngaygiahan'   => date('Y-m-d H:i:s',time())				
            );
		$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
		
        if ($DocGo > 0) {
			redirect(base_url().'vanbanchoxuly_pp');
            return messagebox('Chuyền hạn văn bản về lãnh đạo thành công', 'info');
        } else {
            return messagebox('Chuyền hạn văn bản về lãnh đạo thất bại', 'danger');
        }
    }

}