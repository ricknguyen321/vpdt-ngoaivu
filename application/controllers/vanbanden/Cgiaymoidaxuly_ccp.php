<?php
/**
 * Created by PhpStorm.
 * User: MinhDuy
 * Date: 6/14/2017
 * Time: 3:56 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cgiaymoidaxuly_ccp extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
		$this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->library('pagination');
    }
    public function index()
    {
//        pr($this->_session);
        // danh sách giám đốc
        $data['getAccountDirec'] = $this->Mgiaymoichoxuly->getAccount('0','10');
        // danh sách phó giám đốc
        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0','11');
        // danh sách phòng
//        $data['getDepartment'] = $this->Mgiaymoichoxuly->getDepartment('0');
        // duyệt văn bản chờ xử lý
        if($this->input->post('duyet')) {
            $data['content'] =  $this->insertDocAwait();
        }
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                if(!empty($duongdan))
                {
                    $data['getDocAwait'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $data['getDocAwait'][$key]['sDuongDan'] = '';
                }
                $getDocConcac = $this->Mvanbanden->getDocConcac1($value['PK_iMaVBDen']);
//                pr($getDocConcac);
                if(!empty($getDocConcac)){
                    $data['getDocAwait'][$key]['ccp_id'] = $getDocConcac[0]['ccp_id'];
                    $data['getDocAwait'][$key]['ccp_desc'] = $getDocConcac[0]['ccp_desc'];
                    $data['getDocAwait'][$key]['tp_id'] = $getDocConcac[0]['tp_id'];
                    $data['getDocAwait'][$key]['tp_desc'] = $getDocConcac[0]['tp_desc'];
                    $data['getDocAwait'][$key]['ph_id'] = $getDocConcac[0]['ph_id'];
                    $data['getDocAwait'][$key]['tcdn_id'] = $getDocConcac[0]['tcdn_id'];
                    $data['getDocAwait'][$key]['sHanChoPP'] = $getDocConcac[0]['hangiaiquyet'];
                }
            }
        }
//        pr($data['getDocAwait']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
//        pr($data['getDocAwait']);
        $data['title']    = 'Văn bản đã xử lý';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoi_ccp';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $process = $this->Mvanbanchoxuly->laylanhdaosaucung($ma);
        $PDirector = $this->input->post('phogiamdoc');
        $Deparment = $this->input->post('phongchutri');
        $Doc_array= array();
        if (!empty($PDirector[$ma])) {
            $Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
        }  elseif (!empty($Deparment[$ma])) {
            $Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
        }
//            pr($Doc_array);
        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$ma,'tbl_vanbanden',$Doc_array);

        /** @var mang chuyen van ban cho pho chi cục $PDir_array */
        $PDir_array= array();
        if(!empty($PDirector[$ma]) || !empty($Deparment[$ma])) {
            $PDir_array['doc_id'] = $this->input->post('doc_id')[$ma];
            $PDir_array['doc_giaymoi'] = '1';
            $PDir_array['department_id'] = $this->_session['FK_iMaPhongHD'];
            $PDir_array['ccp_id'] = $PDirector[$ma];
            $PDir_array['ccp_desc'] = $this->input->post('chidaophogiamdoc')[$ma];
            $PDir_array['tp_id'] = $Deparment[$ma];
            $PDir_array['tp_desc'] = $this->input->post('chidaophongchutri')[$ma];
            $PDir_array['ph_id'] = $this->input->post('mangphoihop')[$ma];
            $PDir_array['tcdn_active'] = '3';
            $PDir_array['tcdn_date'] = date('Y-m-d H:s', time());
            $PDir_array['hangiaiquyet'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
            $PDir_array['input_per'] = $this->_session['PK_iMaCB'];

            //$this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$ma,'tuan', date("W"),'lanhdao_id',$this->_session['PK_iMaCB'],'kehoach');
            /*$kehoach_data_cv = array(
                'vanban_id' => $this->input->post('doc_id')[$ma],
                'kh_noidung' => $this->input->post('noidungvb')[$ma],
                'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                'vanban_skh' => $this->input->post('sohieu')[$ma],
                'tuan' => date("W"),
                'ngay_nhan' => date('Y-m-d H:i:s',time()),
                'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? _post('ngaymoi')[$ma] : date_insert(_post('hangiaiquyet')[$ma]),
                'canbo_id' => $Deparment[$ma],
                'thuc_hien' => '1',
                'loai_kh' => 1,
                'lanhdao_id' => $this->_session['PK_iMaCB'],
                'user_input' => $this->_session['PK_iMaCB'],
                'lanhdao_so' => $process['PK_iMaCB'],
                'phong_id' => $this->_session['FK_iMaPhongHD']
            );
            $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);*/
//                pr($PDir_array);
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('tcdn_id',$this->input->post('tcdn_id')[$ma],'tbl_chicuc',$PDir_array);
            if ($DocGo > 0) {
                return messagebox('Duyệt văn bản thành công', 'info');
            } else {
                return messagebox('Duyệt văn bản thất bại', 'danger');
            }
//            return redirect(base_url().'vanbanchoxuly_cc');
        }

    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'vanbanchoxuly_cc?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mgiaymoichoxuly->getPConCac($this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 200;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_cc');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mgiaymoichoxuly->getPConCac($this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}