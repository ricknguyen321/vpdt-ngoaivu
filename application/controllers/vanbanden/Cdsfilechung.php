<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 3:13 PM
 */
class Cdsfilechung extends MY_Controller
{
    protected $_thongtin;
	protected $_files;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly', 'Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden', 'Mvanbanden');
        $this->load->model('vanbandi/Mvanbandi');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc', 'Mdanhmuc');
        $this->load->library('pagination');
    }
    public function index()
    {
        
        $cb = $this->_session['FK_iMaCB'];
        
        $ma         = _get('id');
		$tonghop	 = _get('tonghop');
        $data['id'] = $ma;
        if (!empty($ma)) {
            $this->capnhatDuLieu($ma);
        } else {
            $data['content'] = $this->themDuLieu();
        }
        if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
        $page              = $this->PhanTrang();
		$data['tonghop'] 	   = $tonghop;
        $data['dsfiles']   = $page['info'];
		$data['thongtin']	  = $this->_thongtin;
        //            pr($this->_session);
        $data['dsphong']   = $this->Mvanbandi->layPhongBanDuThao();
		$canbo      = $this->Mvanbandi->layCB();
		foreach ($canbo as $key => $cb) {
			$data['mangcb'][$cb['PK_iMaCB']] = $cb['sHoTen'];
		}
        $data['phantrang'] = $page['pagination'];
        
        $data['count'] = $page['count'];
        
        //        pr($data['processcvph']);
        $data['title']    = 'Danh sách tài liệu, biểu mẫu chung';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vdsfilechung';
        $this->load->view('layout_admin/layout', $temp);
    }
    
    
    public function PhanTrang()
    {
        $tonghop	 = _get('tonghop');
		$data['tonghop']    = $tonghop;
        $config['base_url']             = base_url() . 'dsfilechung';
        $config['total_rows']           = count($this->Mdanhmuc->layfilebieumau($tenfile,$category,$phong,$tag,$note,$config['per_page'], $data['page'],$tonghop));
        //        pr($config['total_rows']);
        $config['per_page']             = 50;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';
        
        $this->pagination->initialize($config);
        
        $data['page'] = $this->input->get('page') ? $this->input->get('page') : 0;
        
        if ((is_numeric($data['page']) == FALSE) || $data['page'] % $config['per_page'] != 0 || ($data['page']) > $config['total_rows']) {
            redirect(base_url() . 'dsfilechung');
        }
        
        $data['count']      = $config['total_rows'];
        $data['info']       = $this->Mdanhmuc->layfilebieumau($tenfile,$category,$phong,$tag,$note,$config['per_page'], $data['page'],$tonghop);
        $data['pagination'] = $this->pagination->create_links();
        //        pr($data['info']);
        return $data;
    }
	
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_files_chung');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
		}
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('id',$ma,'tbl_files_chung');
		if($kiemtra>0)
		{
			return messagebox('Xóa file biểu mẫu thành công!','info');
		}
	}
    
    public function capnhatDuLieu($ma)
    {
        $this->_thongtin = $this->Mdanhmuc->layDuLieu('id', $ma, 'tbl_files_chung');
        if (_post('luudulieu')) {
            $data    = array(
                'category' => _post('category'),
                'phong' => _post('phong'),
                'tag' => _post('tag'),
                'note' => _post('note')
            );
            $kiemtra = $this->Mdanhmuc->capnhatDuLieu('id', $ma, 'tbl_files_chung', $data);
            redirect('dsfilechung');
        }
    }
    
    public function themDuLieu()
    {
        $time = time();
        if (_post('luudulieu')) {
            $canbo = $this->_session['PK_iMaCB'];
            $data  = array(
                'category' => _post('category'),
                'phong' => _post('phong'),
                'tag' => _post('tag'),
                'note' => _post('note')
            );
            $name  = $_FILES['files']['name'];
            if (empty($name[0])) {
                $this->_thongtin = array($data);
                return messagebox('Bạn chưa chọn file', 'danger');
            }
            $name = $_FILES['files']['name'];
            if (!empty($name[0])) {
                $this->upload('doc_bieumau_' . date('Y'), $time);
                $files = array();
                foreach ($name as $key => $value) {
                    $val = clear($value);
                    $a   = explode('.', $val);
                    $c   = count($a) - 1;
                    $tmp = '';
                    for ($i = 0; $i < $c; $i++) {
                        if ($i == 0) {
                            $tmp = $tmp . $a[$i];
                        } else {
                            $tmp = $tmp . '-' . $a[$i];
                        }
                    }
                    $tmp = str_replace('$', '-', $tmp);
                    $tmp = $tmp . '.' . $a[$c];
                    
                    if ($tmp != '.') {
                        $files[] = array(
                            'sTenFile' => $value,
                            'sDuongDan' => 'doc_bieumau_' . date('Y') . '/bm_' . $time . '_' . $tmp,
                            'category' => _post('category'),
                            'phong' => _post('phong'),
                            'tag' => _post('tag'),
                            'note' => _post('note'),
                            'sThoiGian' => date('Y-m-d H:i:s', time()),
                            'FK_iMaCB' => $this->_session['PK_iMaCB']
                        );
                    }
                }
                $kiemtrafile = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_chung', $files);
                if ($kiemtrafile > 0) {
                    redirect('dsfilechung');
                } else {
                    return messagebox('Thêm file biểu mẫu thất bại', 'danger');
                }
            }
        }
    }
    
    public function upload($dir, $time)
    {
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
                    $_FILES['files']['name']     = $file['files']['name'][$i];
                    $_FILES['files']['type']     = $file['files']['type'][$i];
                    $_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
                    $_FILES['files']['error']    = $file['files']['error'][$i];
                    $_FILES['files']['size']     = $file['files']['size'][$i];
                    if (is_dir($dir) == false) {
                        mkdir($dir); // Create directory if it does not exist
                    }
                    $config['upload_path']   = $dir . '/';
                    $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
                    $config['overwrite']     = true;
                    $config['file_name']     = 'bm_' . $time . '_' . clear($_FILES['files']['name']);
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData    = $this->upload->data();
                }
            }
        }
    }
    
}