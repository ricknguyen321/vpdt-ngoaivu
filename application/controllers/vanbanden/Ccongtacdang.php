<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ccongtacdang extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
    }
    public function index()
    {
//        $query = $this->Mvanbanden->getDL();
//	    pr($this->_session);
        $action = _post('action');
        switch ($action) {
            case 'layNgayHetHan':
                $this->layNgayHetHan();
                break;
            case 'layChucVu':
                $this->layChucVu();
                break;
            case 'getData':
                $this->checkTrichYeu();
                break;
            default:
                # code...
                break;
        }
        $idDocGo = $this->uri->segment(2);
        if(!empty($idDocGo)){
            $process = $this->Mvanbanchoxuly->getDocProcess($idDocGo);
            $processcv = $this->Mvanbanchoxuly->getDocProcesscv($idDocGo);
            $processpb = $this->Mvanbanchoxuly->getDocProcessPB($idDocGo);
            $data['process'] = array_merge($process,$processpb,$processcv);
        }
        $data['idDocGo'] = $idDocGo;
        if(!empty($idDocGo)){
            $data['getDocGo'] = $this->Mvanbanden->layDuLieu('PK_iMaVBDen',$idDocGo);
            $data['VBDi'] = $this->Mvanbanchoxuly->layDuLieu('iSoDen',$data['getDocGo'][0]['iSoDen'],'tbl_vanbandi');
            foreach ($data['VBDi'] as $key2 => $value2){
                $file = $this->Mvanbanchoxuly->layFileLast($value2['PK_iMaVBDi']);
                if(!empty($file))
                {
                    $data['VBDi'][$key2]['sDuongDan'] = $file['sDuongDan'];
                }else{
                    $data['VBDi'][$key2]['sDuongDan'] = '';
                }
            }
        }
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idDocGo,'tbl_files_vbden');
        // lấy kết quả
        $cvtc = $this->Mvanbanchoxuly->getCVCT($idDocGo);
        //ket qua phong thu ly
        if(!empty($cvtc)){
            $data['resultPTL'] = $this->Mvanbanchoxuly->getResultPTL($idDocGo,NULL,$cvtc[0]['PK_iMaPhongCT']);
        }
        (!empty($idDocGo) ? $this->updateDocGo($idDocGo) : $data['content'] = $this->insertDocGo());
        $getAppo = $this->Mvanbanden->layDuLieuEnd();
//        pr($getAppo);
        $data['soden'] = $getAppo['iSoDen']+1;
        $data['dsquytrinh'] = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_quytrinhiso');

        $data['title']    = 'Nhập văn bản đến';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vcongtacdang';
        $this->load->view('layout_admin/layout',$temp);
    }
    // lấy chức vụ người ký
    public function layChucVu()
    {
        $hoten = _post('hoten');
        $chucvu = $this->Mdanhmuc->layDuLieu('sHoTen',$hoten,'tbl_nguoiky');
        if(!empty($chucvu))
        {
            $chucvunguoiky= $chucvu[0]['sChucVu'];
        }
        else{
            $chucvunguoiky ='';
        }
        echo json_encode($chucvunguoiky);exit();
    }
    // lấy ngày hết hạn
    public function layNgayHetHan()
    {
        $loaivanban = _post('loaivanban');
        $ngaynhan   = (_post('ngaynhan'))?_post('ngaynhan'):date('d/m/Y');
        $songay     = _post('songay');
        $ngaynghi   = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_ngaynghi');
        $i          =0;
        foreach ($ngaynghi as $key => $value) {
            if($value['sNgayNghi']!=date_insert($ngaynhan))
            {
                if(date_insert($ngaynhan) <= $value['sNgayNghi'] && $value['sNgayNghi'] <= dateFromBusinessDays($songay,$ngaynhan)){
                    $i++;
                }
            }

        }
        if($loaivanban=='Quyết định' || $loaivanban=='quyết định')
        {
            $hangiaiquyet = '';
        }
        else{
            $hangiaiquyet = date_select(dateFromBusinessDays($songay+$i,$ngaynhan));
        }
        echo json_encode($hangiaiquyet); exit();

    }
    /** Thêm văn bản đến */
    public function insertDocGo()
    {

        if ($this->input->post('luulai')) {
            if($this->_session['PK_iMaCB'] == 577){
                sleep(2.4);
            }
            if($this->_session['PK_iMaCB'] == 136){
                sleep(1.6);
            }
            if($this->_session['PK_iMaCB'] == 81){
                sleep(0.8);
            }
            $noidung = $this->input->post('noidunghop');
            $getAppo = $this->Mvanbanden->layDuLieuEnd();
            $soden = $getAppo['iSoDen']+1;
            $data_doc_go = array(
                'sTenKV' => _post('khuvuc'),
                'sKyHieu' => _post('sokyhieu'),
                'sTenLVB' => _post('loaivanban'),
                'sTenDV' => _post('donvi'),
                'sNgayKy' => date_insert(_post('ngayky')),
                'sTenLV' => _post('linhvuc'),
                'sMoTa' => _post('trichyeu'),
                'sTenNguoiKy' => _post('nguoiky'),
                'sNgayNhan' => date_insert(_post('ngaynhan')),
                'sNgayNhap' => date("Y-m-d"),
                'iSoDen' => $soden,
                'sChucVu' => _post('chucvu'),
                'sHanGiaiQuyet' => date_insert(_post('hangiaiquyet')),
                'sHanThongKe' => date_insert(_post('hangiaiquyet')),
                'iSoTrang' => _post('sotrang'),
                'iTrangThai_TruyenNhan' => '2',
                'iGiayMoi' => '3',
                'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
                'iVanBanQPPL' => (!empty(_post('iVanBanQPPL'))) ? _post('iVanBanQPPL') : '0',
                'iSTCChuTri' => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
                'iVanBanTBKL' => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
                'iVanBanMat' => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0'
            );
            $array_update = array();
            for ($i=0, $count = count($noidung); $i<$count; $i++)
            {
                $array_update[] = array(
                    'sTenKV' => _post('khuvuc'),
                    'sKyHieu' => _post('sokyhieu'),
                    'sTenLVB' => _post('loaivanban'),
                    'sTenDV' => _post('donvi'),
                    'sNgayKy' => date_insert(_post('ngayky')),
                    'sTenLV' => _post('linhvuc'),
                    'sMoTa' => _post('trichyeu'),
                    'sNoiDung'  => _post('noidunghop')[$i],
                    'sTenNguoiKy' => _post('nguoiky'),
                    'sNgayNhan' => date_insert(_post('ngaynhan')),
                    'sNgayNhap' => date("Y-m-d"),
                    'iSoDen' => $soden,
                    'sChucVu' => _post('chucvu'),
                    'sHanGiaiQuyet' => date_insert(_post('hangiaiquyet')),
                    'iSoTrang' => _post('sotrang'),
                    'iTrangThai_TruyenNhan' => '2',
                    'iGiayMoi' => '3',
                    'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
                    'iVanBanQPPL' => (!empty(_post('iVanBanQPPL'))) ? _post('iVanBanQPPL') : '0',
                    'iSTCChuTri' => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
                    'iVanBanTBKL' => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
                    'iVanBanMat' => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0',
                    'sHanNoiDung' => (!empty(_post('hannoidung')[$i])) ? date_insert(_post('hannoidung')[$i]) : date_insert(_post('hangiaiquyet')),
                    'sHanThongKe' => (!empty(_post('hannoidung')[$i])) ? date_insert(_post('hannoidung')[$i]) : date_insert(_post('hangiaiquyet'))

                );
            }

//            pr($data_doc_go);
            if(empty($array_update[0]['sNoiDung'])){
                // pr($data_doc_go);
                $DocGo = $this->Mdanhmuc->themDuLieu('tbl_vanbanden',$data_doc_go);
                if ($DocGo > 0) {
                    return messagebox('Thêm thành công số đến là: ' .$soden, 'info');
                } else {
                    return messagebox('Thêm văn bản đến thất bại', 'danger');
                }
            }else{
//                pr($array_update);
                $DocGo = $this->Mdanhmuc->themNhieuDuLieu('tbl_vanbanden',$array_update);
                if ($DocGo > 0) {
                    return messagebox('Thêm thành công số đến là: ' .$soden, 'info');
                } else {
                    return messagebox('Thêm văn bản đến thất bại', 'danger');
                }

            }
        }
    }
    /** Cập nhật văn bản đến */
    public function updateDocGo($idDocGo){
        if($this->input->post('luulai')){
            $data_doc_go = array(
                'sTenKV'      => _post('khuvuc'),
                'sKyHieu'       => _post('sokyhieu'),
                'sTenLVB'       => _post('loaivanban'),
                'sTenDV'        => _post('donvi'),
                'sNgayKy'       => date_insert(_post('ngayky')),
                'sTenLV'        => _post('linhvuc'),
                'sMoTa'         => _post('trichyeu'),
                'sTenNguoiKy'   => _post('nguoiky'),
                'sNgayNhan'     => date_insert(_post('ngaynhan')),
                'iSoDen'        => _post('soden'),
                'sChucVu'       => _post('chucvu'),
                'sHanGiaiQuyet' => date_insert(_post('hangiaiquyet')),
                'iSoTrang'      => _post('sotrang'),
                'sNgayNhap'     => date("Y-m-d"),
                'PK_iMaCapNhat' => $this->_session['PK_iMaCB'],
                'iVanBanQPPL'   => (!empty( _post('iVanBanQPPL'))) ?  _post('iVanBanQPPL') : '0',
                'iSTCChuTri'   => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
                'iVanBanTBKL'  => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
                'iVanBanMat'   => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0',
                'sNoiDung'  => _post('noidunghop')[0],
                'sHanNoiDung' => date_insert(_post('hangiaiquyet'))

            );
//            pr($data_doc_go);
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDocGo,'tbl_vanbanden',$data_doc_go);
            return redirect(base_url().'dsvanbanden');
        }
    }
    public function checkSoDen()
    {
//        $soden = $this->input->post('soden');
        $getAppo = $this->Mvanbanden->getSoDen();
//        pr($getAppo);
        echo json_encode($getAppo);exit();
    }
    public function checkTrichYeu()
    {
        $trichyeu = $this->input->post('trichyeu');
        $getAppo1 = $this->Mvanbanden->getSoDen('sMoTa',$trichyeu);
//        pr($getAppo1);
        echo json_encode($getAppo1);exit();
    }

}

/* End of file Cvanbanden.php */
/* Location: ./application/controllers/vanban/Cvanbanden.php */