<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Cquatrinhxuly extends MY_Controller {
	protected $_thongtin;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
//        $this->load->model('vanbanden/Mvanbanden','Mvanbanden');
//        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanphoihop');
        $this->load->model('vanbandi/Mtruyennhan');
    }
    public function index()
    {
        $data['nam'] = $this->input->get('nam');
        if(!empty($data['nam'])){
            if($this->input->get('nam') == 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default', TRUE);
            }
            if($this->input->get('nam') > 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default_'.$this->input->get('nam'), TRUE);
            }
        }
        $idAppointment = $this->uri->segment(2);

        $canbo                    = $this->Mtruyennhan->layDSCB2();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
			if (($value['iQuyenHan_DHNB'] == 6 || $value['iQuyenHan_DHNB'] == 3) && $value['FK_iMaPhongHD'] == $this->_session['FK_iMaPhongHD']) {
				$data['tp'] = $value;
				$data['phonghd'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_phongban');
			}
        }
        $phong_PH                    = $this->Mvanbanphoihop->layPB_PH($idAppointment);
        if(!empty($phong_PH))
        {
            $phongban                = explode(',',$phong_PH['PK_iMaPhongPH']);
            $data['phongbanphoihop'] = $this->Mvanbanphoihop->layQT_Phong_PH($idAppointment,$phongban);
			
        }
        else
        {
            $data['phongbanphoihop'] = '';
        }
		
		if($this->input->post('luuhoso')){
           $data['content'] = $this->luuhoso();
        }
		$data['dshoso'] = $this->Mdanhmuc->layDuLieu('FK_iMaCB',$this->_session['PK_iMaCB'],'tbl_hoso');
		$dshsvb = $this->Mdanhmuc->layhosovbden($idAppointment);
		$dshsvanban = array();
		foreach ($dshsvb as $key => $value) {
			array_push($dshsvanban, $value['FK_iMaHS']);
		}
		$data['dshosovb'] = $dshsvanban;
		$data['chuyennhan_phoihop'] = $this->Mvanbanphoihop->layDS_CN($idAppointment);
        $data['luuvetchuyennhan']    = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_luuvet_chuyennhan');
        $data['luuvetduyet']         = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$idAppointment,'tbl_luuvet_duyet');
		$data['chidao']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_chidao');
		$data['dexuat']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_ykien');
		$data['qtgiahan']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_luuvet_giahan');
        // trình tự xử lý
        $process = $this->Mvanbanchoxuly->getDocProcess($idAppointment);
        $processcv = $this->Mvanbanchoxuly->getDocProcesscv($idAppointment);
        $processpb = $this->Mvanbanchoxuly->getDocProcessPB($idAppointment);
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');
//        pr($process);
//        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($this->_session);
        $data['idDocGo'] = $idAppointment;
        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($processcv);
        // lấy kết quả
        $cvtc = $this->Mvanbanchoxuly->getCVCT($idAppointment);
        //ket qua phong thu ly
        if(!empty($cvtc)){
            $data['resultPTL'] = $this->Mvanbanchoxuly->getResultPTL($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
			$data['resultPPH'] = $this->Mvanbanchoxuly->getResultPPH($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
        }
//        pr($data['resultPTL']);
        $result =  layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_file_ketqua');

        $data['result'] = $result;

//        pr($data['result']);

		if ($this->input->post('luulai')) {
			if (!empty($this->input->post('chidao')) && $this->input->post('chidao') != '') {
				$chidaold = array(
					'FK_iMaVBDen'        => $idAppointment,
					'sNoiDung'        => nl2br($this->input->post('chidao')),
					'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
					'sThoiGian'   => date('Y-m-d H:i:s',time()),
				);
				
				$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
				return redirect(base_url().'quatrinhxuly/'.$idAppointment);
			} else {
				$data['content'] = messagebox('Chưa nhập nội dung','danger');
			}
		}
		
		if ($this->input->post('luuykien')) {
			if (!empty($this->input->post('dexuat')) && $this->input->post('dexuat') != '') {
				$chidaold = array(
					'FK_iMaVBDen'        => $idAppointment,
					'sNoiDung'        => nl2br($this->input->post('dexuat')),
					'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
					'sThoiGian'   => date('Y-m-d H:i:s',time()),
				);
				
				$this->Mdanhmuc->themDuLieu('tbl_ykien',$chidaold);
				return redirect(base_url().'quatrinhxuly/'.$idAppointment);
			} else {
				$data['content'] = messagebox('Chưa nhập nội dung','danger');
			}
		}
		
		if($this->input->post('xoadexuat')){
            $this->Mdanhmuc->xoaDuLieu('id',$this->input->post('xoadexuat'),'tbl_ykien');
			return redirect(base_url().'quatrinhxuly/'.$idAppointment);
        }
		if($this->input->post('xoachidao')){
            $this->Mdanhmuc->xoaDuLieu('id',$this->input->post('xoachidao'),'tbl_chidao');
			return redirect(base_url().'quatrinhxuly/'.$idAppointment);
        }

        if(!empty($idAppointment)){
            $this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
			$data['getdAppointment'] = $this->_thongtin;
            //$data['VBDi'] = $this->Mdanhmuc->layDuLieu2('iSoDen',$data['getdAppointment'][0]['iSoDen'],'sNgayNhap >',date('Y').'-01'.'-01','tbl_vanbandi');

            $soden_tam=$data['getdAppointment'][0]['iSoDen'];
            $data['VBDi'] = $this->Mdanhmuc->getvanbandi("(iSoDen = ".$soden_tam." OR iSoDen LIKE '%,".$soden_tam.",%' OR iSoDen LIKE '%,".$soden_tam." ,%'  OR iSoDen LIKE '%, ".$soden_tam.",%'  OR iSoDen LIKE '%, ".$soden_tam." ,%' OR  iSoDen LIKE '%, ".$soden_tam."' OR  iSoDen LIKE '%,".$soden_tam."') and sNgayNhap >='".$data['getdAppointment'][0]['sNgayNhan']."'",'tbl_vanbandi');

            foreach ($data['VBDi'] as $key2 => $value2){
                $file = $this->Mvanbanchoxuly->layFileLast($value2['PK_iMaVBDi']);
                if(!empty($file))
                {
                    $data['VBDi'][$key2]['sDuongDan'] = $file['sDuongDan'];
                }else{
                    $data['VBDi'][$key2]['sDuongDan'] = '';
                }
            }
//            pr($data['VBDi']);
        }
        $data['title']    = 'Quá trình xử lý văn bản đến';
        $temp['data']     = $data;
		
		if(isset($_POST['ketxuatword'])){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=Phiếu trình xử lý văn bản đến số ".$data['getdAppointment'][0]['iSoDen'].".doc");
			
            $temp['template'] = 'vanbanden/Vquatrinhxuly_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }elseif($this->input->get('kx') == 'word'){
			header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=Phiếu xử lý văn bản số ".$data['getdAppointment'][0]['iSoDen'].".doc");
			
            $temp['template'] = 'vanbanden/Vquatrinhxuly_pxl_word';
			$this->load->view('layout_admin/layout_word',$temp);
        } else {
			$temp['template'] = 'vanbanden/Vquatrinhxuly';
			$this->load->view('layout_admin/layout',$temp);
		}
        
    }
	
	public function luuhoso(){
		 $idDoc       = $this->input->post('luuhoso');
		 $vanban = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden');
		 $manghs = $this->input->post('manghs'.$idDoc);
		 $dshsvb = $this->Mdanhmuc->layhosovbden($idDoc);
		$dshsvanban = array();
		foreach ($dshsvb as $key => $value) {
			array_push($dshsvanban, $value['FK_iMaHS']);
		}
		
		
			foreach ($manghs as $key => $value) {
				 if (in_array($value, $dshsvanban)) continue;
				 $data = array(
					'FK_iMaVBDen' => $idDoc,
					'sThoiGian' => date('Y-m-d H:i:s',time()),
					'FK_iMaCB' => $this->_session['PK_iMaCB'],
					'FK_iMaPB' => $this->_session['FK_iMaPhongHD'],
					'FK_iMaHS' => $value,
					'iSoVB' => $vanban[0]['iSoDen'],
					'sTenLVB' => $vanban[0]['sTenLVB'],
					'sKyHieu' => $vanban[0]['sKyHieu'],
					'sNgayNhap' => $vanban[0]['sNgayNhap'],
					'sMoTa' => $vanban[0]['sMoTa'],
					);
				 $this->Mdanhmuc->themDuLieu('tbl_hosovanban',$data);
			}
			foreach ($dshsvanban as $key => $value) {
				if(in_array($value,$manghs)) continue;
					//xoa ho so van ban
					$this->Mdanhmuc->xoaHoSoVBDen($idDoc,$value);
			}	
		redirect(base_url().'quatrinhxuly/'.$idDoc);
	}

}