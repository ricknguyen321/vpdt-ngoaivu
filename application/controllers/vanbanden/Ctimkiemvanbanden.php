<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctimkiemvanbanden extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['title']    = 'Tìm kiếm văn bản đến';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vtimkiemvanbanden';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Ctimkiemvanbanden.php */
/* Location: ./application/controllers/vanbanden/Ctimkiemvanbanden.php */