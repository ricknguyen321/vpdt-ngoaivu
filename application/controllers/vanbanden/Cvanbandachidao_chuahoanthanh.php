<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/26/2017
 * Time: 3:02 PM
 */
class Cvanbandachidao_chuahoanthanh extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
    }
    public function index()
    {
//pr($this->_session);
//        pr($data['process']);
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mvanbanchoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        //danh sách chờ xử lý
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $process = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen'],NULL,NULL,'8','1');
                $processpp = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen'],NULL,NULL,'7');
//                pr($process);
                if(!empty($process)){
                    $data['getDocAwait'][$key]['PK_iMaCVCT'] = $process[0]['PK_iMaCVCT'];
                    $data['getDocAwait'][$key]['MoTaCVCT'] = $process[0]['sMoTa'];
                    $data['getDocAwait'][$key]['PK_iMaCVPH'] = $process[0]['PK_iMaCVPH'];
                    $data['getDocAwait'][$key]['PK_iMaCNCT'] = $process[0]['PK_iMaCN'];
                    $data['getDocAwait'][$key]['sHanChoCV'] = $process[0]['sThoiGianHetHan'];
                }
                if(empty($process)){
                    $data['getDocAwait'][$key]['PK_iMaCVCT'] = '';
                    $data['getDocAwait'][$key]['MoTaCVCT'] = '';
                }
                if(!empty($processpp)){
                    $data['getDocAwait'][$key]['PK_iMaPP'] = $processpp[0]['PK_iMaCVCT'];
                    $data['getDocAwait'][$key]['MoTaPP'] = $processpp[0]['sMoTa'];
                    $data['getDocAwait'][$key]['PK_iMaCNPP'] = $processpp[0]['PK_iMaCN'];
                    $data['getDocAwait'][$key]['sHanChoPP'] = $processpp[0]['sThoiGianHetHan'];
					$data['getDocAwait'][$key]['PK_iMaPPPH'] = $processpp[0]['PK_iMaCVPH'];
                }
                if(empty($processpp)){
                    $data['getDocAwait'][$key]['PK_iMaPP'] = '';
                    $data['getDocAwait'][$key]['MoTaPP'] = '';
                }
//                pr($arrayCBNhan);
            }
//            pr($data['getDocAwait']);
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['count'] = $page['count'];
//        pr($data['getDocAwait']);

        // duyệt văn bản chờ xử lý
//        $this->insertDocAwait();
        if($this->input->post('duyet')) {
            $this->insertDocAwait();
        }
        $data['title']    = 'Văn bản đã chỉ đạo';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vchuyenviendachidao_chuahoanthanh';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $taikhoan = $this->_session['PK_iMaCB'];
        $phophong                = _post('phogiamdoc')[$ma];
        $chuyenvien              = _post('phongchutri')[$ma];
        $noidungchuyenphophong   = _post('chidaophogiamdoc')[$ma];
        $noidungchuyenchuyenvien = _post('chidaophongchutri')[$ma];
        $mangthemdulieu = array(
            'FK_iMaVBDen'     => $ma,
            'FK_iMaCB_Chuyen' => $taikhoan,
            'FK_iMaCB_Nhan'   => ($phophong)?$phophong:$chuyenvien,
            'sNoiDung'        => ($phophong)?$noidungchuyenphophong:$noidungchuyenchuyenvien,
            'sThoiGian'       => date('Y-m-d H:i:s')
        );
        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
        
        $process = $this->Mvanbanchoxuly->laylanhdaosaucung($ma);
//        if($this->input->post('duyetvanban')){
//            foreach ($this->input->post('duyet') as $key5 => $value5){
//                if(!empty($value5)){
                    // anh xa pho phong
                    $Deputy = $this->input->post('phogiamdoc');
                    $staff = $this->input->post('phongchutri');
                    $DepartmentPH = $this->input->post('mangphoihop');
                    $DeputyPH = $this->input->post('mangphoihoppp');
                    $PGDThamMuu = _post('ppthammmuu')[$ma];
                    $Doc_array= array();
                    if(!empty($PGDThamMuu)){
                        if(!empty($staff[$ma])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '7';
                            $Doc_array['iPpThamMuu'] = _post('ppthammmuu')[$ma];
                        }
                    }else{
                        if (!empty($Deputy[$ma])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '6';
                        } else {
                            $Doc_array['iTrangThai_TruyenNhan'] = '7';
                        }
                    }
//                    pr($Doc_array);
                    $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$this->input->post('doc_id')[$ma],'tbl_vanbanden',$Doc_array);
					
                    if(!empty($staff[$ma]) || $Deputy[$ma]) {
                        if($this->_session['iQuyenHan_DHNB'] == 7){
                            $this->Mvanbanchoxuly->xoaDuLieu3Where('PK_iMaVBDen',$ma,'PK_iMaPhong', $this->_session['FK_iMaPhongHD'],'iTrangThai_PPPH',0,'tbl_phongphoihop');
                        }
                        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
                            $this->Mvanbanchoxuly->xoaDuLieu2Where('PK_iMaVBDen',$ma,'PK_iMaPhong', $this->_session['FK_iMaPhongHD'],'tbl_phongphoihop');
                        }
                        /** @var mang chuyen chuyen vien $Departmanet_array */
                        // van ban phoi hop giua cac chuyen vien vowi nhau
                        $Departmanet_array= array();
                        $mangph = $this->input->post('mangphoihop')[$ma];
                        $cvphoihop = explode( ',', $mangph );
//                        pr($cvphoihop);
                        /** @var mang chuyen van ban cho pho phòng $PDir_array */
                        $PDir_array= array();
                        $vbpp = $this->input->post('vbpp')[$ma];
                        $vbpp1 = $this->input->post('vbpp1')[$ma];
                        if(!empty($Deputy[$ma])){
                            if($Deputy[$ma] != $vbpp1){
//                                $this->Mdanhmuc->xoaDuLieu('PK_iMaCN',$vbpp,'tbl_chuyennhanvanban');
                                if(!empty($Deputy[$ma])){
                                    $this->Mdanhmuc->xoaDuLieu('PK_iMaCN',$vbpp,'tbl_chuyennhanvanban');
                                    $PDir_array['PK_iMaCVCT'] = $Deputy[$ma];
                                    $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                                    $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                                    $PDir_array['sThoiGian'] = date('Y-m-d H:s',time());
                                    $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$ma];
                                    $PDir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                                    $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                                    $PDir_array['CapGiaiQuyet'] = '7';
									$PDir_array['PK_iMaCVPH'] = $this->input->post('mangphoihoppp')[$ma];
                                    $PDir_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$ma]) ? $this->input->post('vanbanquantrong')[$ma] : 0;
                                    $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$PDir_array);
//                                    pr($PDir_array);
                                }
                                }else {
                                $PDir_array['PK_iMaCVCT'] = $Deputy[$ma];
                                $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                                $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                                $PDir_array['sThoiGian'] = date('Y-m-d H:s',time());
                                $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$ma];
                                $PDir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                                $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
                                $PDir_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$ma]) ? $this->input->post('vanbanquantrong')[$ma] : 0;
                                $PDir_array['CapGiaiQuyet'] = '7';
								$PDir_array['PK_iMaCVPH'] = $this->input->post('mangphoihoppp')[$ma];
//                                pr($PDir_array);
                                $this->Mdanhmuc->capnhatDuLieu('PK_iMaCN',$vbpp,'tbl_chuyennhanvanban',$PDir_array);
                            }
                            if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
                                if(!empty($DeputyPH[$ma])) {
                                    $DeputyPH_array = array();
                                    $mangppph = $this->input->post('mangphoihoppp')[$ma];
                                    $ppphoihop = explode(',', $mangppph);
                                    foreach ($ppphoihop as $key1 => $value1) {
                                        $DeputyPH_array['PK_iMaCB'] = $value1;
                                        $DeputyPH_array['PK_iMaPPPH'] = '2';
                                        $DeputyPH_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                                        $DeputyPH_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                                        $DeputyPH_array['sThoiGian'] = date('Y-m-d H:s', time());
                                        $DeputyPH_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$ma];
                                        $DeputyPH_array['sThoiGianHetHan'] = (_post('hangiaiquyet') == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                                        $DeputyPH_array['input_per'] = $this->_session['PK_iMaCB'];
                                        $DeputyPH_array['iTrangThai_PPPH'] = 1;
                                        // phó phòng phối hợp là 2
                                        $DeputyPH_array['iTrangThai'] = '2';
                                        $this->Mdanhmuc->themDuLieu('tbl_phongphoihop', $DeputyPH_array);
                                    }
                                }
                            }
                        }
                        if(!empty($vbpp)&&$Deputy[$ma]==""){
                            $this->Mdanhmuc->xoaDuLieu('PK_iMaCN',$vbpp,'tbl_chuyennhanvanban');
                        }
                        if(!empty($DepartmentPH[$ma])){
//                            $this->Mdanhmuc->xoaDuLieu('PK',$vbpp,'tbl_chuyennhanvanban');
                            foreach($cvphoihop as $key => $value){
                                $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                                $Departmanet_array['PK_iMaCB'] = $value;
                                $Departmanet_array['PK_iMaCVCT'] = $staff[$ma];
                                $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                                $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
                                $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                                $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                                $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                                $Departmanet_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                                $this->Mdanhmuc->themDuLieu('tbl_phongphoihop',$Departmanet_array);
                            }
//                            pr($Departmanet_array);
                        }
                        // chuyen vien chu tri ko phoi hop
                        $Departmanet_array1= array();
                        $cvct = $this->input->post('cvct');
                        $cvct1 = $this->input->post('cvct1');
                        if(!empty($cvct[$ma])){
                            $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVCT'] = $staff[$ma];
                            $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                            $Departmanet_array1['sThoiGian'] = date('Y-m-d H:s',time());
                            $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                            $Departmanet_array1['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                            $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['CapGiaiQuyet'] = '8';
                            $Departmanet_array1['PK_iMaCVPH'] = $mangph;
                            $Departmanet_array1['sVBQT'] = ($this->input->post('vanbanquantrong')[$ma]) ? $this->input->post('vanbanquantrong')[$ma] : 0;
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaCN',$cvct[$ma],'tbl_chuyennhanvanban',$Departmanet_array1);
                        }elseif(empty($cvct[$ma])){
                            $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVCT'] = $staff[$ma];
                            $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                            $Departmanet_array1['sThoiGian'] = date('Y-m-d H:s',time());
                            $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                            $Departmanet_array1['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                            $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                            $Departmanet_array1['PK_iMaCVPH'] = $mangph;
                            $Departmanet_array1['CapGiaiQuyet'] = '8';
                            $Departmanet_array1['sVBQT'] = ($this->input->post('vanbanquantrong')[$ma]) ? $this->input->post('vanbanquantrong')[$ma] : 0;
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array1);
                        }
$vbdaura_tp =  _post('vbdaura_tp')[$ma];
if($vbdaura_tp==1){                        
                        /// kế hoạch công tác
                        if($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 3){
                            $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$ma,'tuan', date("W"),'phong_id',$this->_session['FK_iMaPhongHD'],'kehoach');
                            if(!empty($Deputy[$ma])){
                                $munber_week = $this->Mdanhmuc->getTuanMax($Deputy[$ma])['tuan'];
                                $tuan_hientai = (int)date("W");
                                if($munber_week == $tuan_hientai){
                                    $cong = 1;
                                }else{
                                    $cong = 0;
                                }
                                $kehoach_data = array(
                                    'vanban_id' => $this->input->post('doc_id')[$ma],
                                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
									'vanban_skh' => $this->input->post('sohieu')[$ma],
                                    'tuan' => (int)date("W")+$cong,
                                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                                    'canbo_id' => $Deputy[$ma],
									'loai_kh' => 1,
									'chucvu'  => 7,
                                    'lanhdao_id' => $this->_session['PK_iMaCB'],
									'user_input' => $this->_session['PK_iMaCB'],
                                    'lanhdao_so' => $process['PK_iMaCB'],
                                    'phong_id' => $this->_session['FK_iMaPhongHD']
                                );
//                        pr($kehoach_data);
                                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data);
                             
                            }else{
                                $munber_week = $this->Mdanhmuc->getTuanMax($staff[$ma])['tuan'];
                                $tuan_hientai = (int)date("W");
                                if($munber_week == $tuan_hientai){
                                    $cong = 1;
                                }else{
                                    $cong = 0;
                                }
                                $kehoach_data_cv = array(
                                    'vanban_id' => $this->input->post('doc_id')[$ma],
                                    'kh_noidung' => $this->input->post('noidungvb')[$ma],
                                    'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
									'vanban_skh' => $this->input->post('sohieu')[$ma],
                                    'tuan' => (int)date("W")+$cong,
                                    'ngay_nhan' => date('Y-m-d H:i:s',time()),
                                    'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                                    'canbo_id' => $staff[$ma],
                                    'thuc_hien' => '3',
									'loai_kh' => 1,
									'chucvu'  => 8,
                                    'lanhdao_id' => $this->_session['PK_iMaCB'],
									'user_input' => $this->_session['PK_iMaCB'],
                                    'lanhdao_so' => $process['PK_iMaCB'],
                                    'phong_id' => $this->_session['FK_iMaPhongHD']
                                );
//                        pr($kehoach_data);
                                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);                          
                            }
                        }
                        if($this->_session['iQuyenHan_DHNB'] == 7){
                            $this->Mvanbanchoxuly->xoaDuLieu3Where('vanban_id',$ma,'tuan', date("W"),'lanhdao_id',$this->_session['PK_iMaCB'],'kehoach');

                            $munber_week = $this->Mdanhmuc->getTuanMax($staff[$ma])['tuan'];
                            $tuan_hientai = (int)date("W");
                            if($munber_week == $tuan_hientai){
                                $cong = 1;
                            }else{
                                $cong = 0;
                            }

                            $kehoach_data_cv = array(
                                'vanban_id' => $this->input->post('doc_id')[$ma],
                                'kh_noidung' => $this->input->post('noidungvb')[$ma],
                                'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
								'vanban_skh' => $this->input->post('sohieu')[$ma],
                                'tuan' => (int)date("W")+$cong,
                                'ngay_nhan' => date('Y-m-d H:i:s',time()),
                                'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                                'canbo_id' => $staff[$ma],
                                'thuc_hien' => '3',
								'loai_kh' => 1,
								'chucvu'  => 8,
                                'lanhdao_id' => $this->_session['PK_iMaCB'],
								'user_input' => $this->_session['PK_iMaCB'],
                                'lanhdao_so' => $process['PK_iMaCB'],
                                'phong_id' => $this->_session['FK_iMaPhongHD']
                            );
//                        pr($kehoach_data);
                            $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);
                        }
}                        
            return redirect(base_url().'vanbandachidao');

        }
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'vanbandachidao?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocTPCDAwait($this->_session['PK_iMaCB'],$this->_session['FK_iMaPhongHD'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));
//        pr($config['total_rows']);
        $config['per_page']             = 30;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbandachidao');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocTPCDAwait($this->_session['PK_iMaCB'],$this->_session['FK_iMaPhongHD'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}