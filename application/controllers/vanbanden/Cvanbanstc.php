<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanstc extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['title']    = 'Văn bản STC chủ trì';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vvanbanstc';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cvanbanstc.php */
/* Location: ./application/controllers/vanbanden/Cvanbanstc.php */