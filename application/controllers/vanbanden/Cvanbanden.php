<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanden extends MY_Controller {
    protected $mang;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
		$this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
	}
	public function index()
	{
        $data['domat']      = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_domat');
		$data['dokhan']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_dokhan');
        $quyen = $this->_session['iQuyenHan_DHNB'];
		$macb = $this->_session['PK_iMaCB'];
        if($quyen!=9 &&  $quyen!=3 && $macb != 705 && $macb != 730 && $macb != 731)
        {
            redirect('dangnhap');
        }
        $data['nam'] = $this->input->get('nam');
        if(!empty($data['nam'])){
            if($this->input->get('nam') == 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default', TRUE);
            }
            if($this->input->get('nam') > 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default_'.$this->input->get('nam'), TRUE);
            }
        }
//        $query = $this->Mvanbanden->getDL();
//	    pr($this->_session);
        $action = _post('action');
        switch ($action) {
            case 'layNgayHetHan':
                $this->layNgayHetHan();
                break;
            case 'layChucVu':
                $this->layChucVu();
                break;
            case 'getData':
                $this->checkTrichYeu();
                break;
            default:
                # code...
                break;
        }
	    $idDocGo = $this->uri->segment(2);
        if(!empty($idDocGo)){
            $process = $this->Mvanbanchoxuly->getDocProcess($idDocGo);
            $processcv = $this->Mvanbanchoxuly->getDocProcesscv($idDocGo);
            $processpb = $this->Mvanbanchoxuly->getDocProcessPB($idDocGo);
            $data['process'] = array_merge($process,$processpb,$processcv);
        }
		
	    $data['idDocGo'] = $idDocGo;
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idDocGo,'tbl_files_vbden');
        // lấy kết quả
        $cvtc = $this->Mvanbanchoxuly->getCVCT($idDocGo);
        //ket qua phong thu ly
        if(!empty($cvtc)){
            //$data['resultPTL'] = $this->Mvanbanchoxuly->getResultPTL($idDocGo,NULL,$cvtc[0]['PK_iMaPhongCT']);
        }
		
        (!empty($idDocGo) ? $this->updateDocGo($idDocGo) : $data['content'] = $this->insertDocGo());
		
        $data['getDocGo'] = $this->mang;
		
		if(!empty($idDocGo)){
			
            $data['getDocGo'] = $this->Mvanbanden->layDuLieu('PK_iMaVBDen',$idDocGo);
			
            /*$data['VBDi'] = $this->Mdanhmuc->layDuLieu2('iSoDen',$data['getDocGo'][0]['iSoDen'],'sNgayNhap >',date('Y').'-01'.'-01','tbl_vanbandi');

            foreach ($data['VBDi'] as $key2 => $value2){
                $file = $this->Mvanbanchoxuly->layFileLast($value2['PK_iMaVBDi']);
                if(!empty($file))
                {
                    $data['VBDi'][$key2]['sDuongDan'] = $file['sDuongDan'];
                }else{
                    $data['VBDi'][$key2]['sDuongDan'] = '';
                }
            }*/
        }
		
//        pr($data['getDocGo']);
//        pr($data['content']['class']);
        $getAppo = $this->Mvanbanden->layDuLieuEnd(NULL,NULL,$_SESSION['nam']);
//        pr($getAppo);
        $data['soden'] = $getAppo['iSoDen']+1;
        $data['dsquytrinh'] = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_quytrinhiso');

		$data['title']    = 'Nhập mới văn bản đến';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vvanbanden';
		$this->load->view('layout_admin/layout',$temp);
	}
    // lấy chức vụ người ký
    public function layChucVu()
    {
        $hoten = _post('hoten');
        $chucvu = $this->Mdanhmuc->layDuLieu('sHoTen',$hoten,'tbl_nguoiky');
        if(!empty($chucvu))
        {
            $chucvunguoiky= $chucvu[0]['sChucVu'];
        }
        else{
            $chucvunguoiky ='';
        }
        echo json_encode($chucvunguoiky);exit();
    }
    // lấy ngày hết hạn
    public function layNgayHetHan()
    {
        $loaivanban = _post('loaivanban');
        $ngaynhan   = (_post('ngaynhan'))?_post('ngaynhan'):date('d/m/Y');
        $songay     = _post('songay');
        $ngaynghi   = $this->Mdanhmuc->layDuLieu('iTrangThai',1,'tbl_ngaynghi');
        $i          =0;
        foreach ($ngaynghi as $key => $value) {
            if($value['sNgayNghi']!=date_insert($ngaynhan))
            {
                if(date_insert($ngaynhan) <= $value['sNgayNghi'] && $value['sNgayNghi'] <= dateFromBusinessDays((int)$songay,$ngaynhan)){
                    $i++;
                }
            }

        }
        //if($loaivanban=='Quyết định' || $loaivanban=='Giấy mời')
        //{
          //  $hangiaiquyet = '';
       // }
       // else{
            $hangiaiquyet = date_select(dateFromBusinessDays((int)$songay+$i,$ngaynhan));
       // }
        echo json_encode($hangiaiquyet); exit();

    }
	/** Thêm văn bản đến */
	public function insertDocGo()
    {

        if ($this->input->post('luulai')) {
			
			if (date_insert(_post('hangiaiquyet')) < date_insert(date("Y-m-d", time()))) 
				return messagebox('Hạn giải quyết chưa đúng.', 'danger');
			
            $loaivanban = trim(_post('loaivanban'));
            $sokyhieu   = trim(_post('sokyhieu'));
            $noigui     = trim(_post('donvi'));
            $ngayky     = date_insert(_post('ngayky'));
            $nguoiky    = trim(_post('nguoiky'));
            $vanbanmat  = (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0';
            $kiemtra = $this->Mdanhmuc->kiemtraTrung_VBD($sokyhieu,$ngayky,$nguoiky,$loaivanban);
			//$kiemtra = $this->Mdanhmuc->kiemtra_Trung($sokyhieu,$noigui,$nguoiky);
            if($kiemtra==0)
            {
    //             if($this->_session['PK_iMaCB'] == 577){
				// 	sleep(0.3);
				// }
				// if($this->_session['PK_iMaCB'] == 136){
				// 	sleep(0.2);
				// }
				// if($this->_session['PK_iMaCB'] == 81){
				// 	sleep(0.1);
				// }
				$noidung = $this->input->post('noidunghop');
				//$getAppo = $this->Mvanbanden->layDuLieuEnd(NULL,NULL,$_SESSION['nam']);
				//$soden = $getAppo['iSoDen']+1;
                $sodenlonnhat = $this->Mvanbanden->laySoDenLonNhat(date('Y'))['soden'];
                $soden = $sodenlonnhat +1;
				$data_doc_go = array(
					'sTenKV' => _post('khuvuc'),
					'sKyHieu' => _post('sokyhieu'),
					'sTenLVB' => _post('loaivanban'),
					'sTenDV' => _post('donvi'),
					'sNgayKy' => date_insert(_post('ngayky')),
					'sTenLV' => _post('linhvuc'),
					'sMoTa' => _post('trichyeu'),
					'sTenNguoiKy' => _post('nguoiky'),
					'sNgayNhan' => date_insert(_post('ngaynhan')),
					'sNgayNhap' => date("Y-m-d H:i:s", time()),
					'iSoDen' => $soden,
					'sChucVu' => _post('chucvu'),
					'sHanGiaiQuyet' => date_insert(_post('hangiaiquyet')),
					'sHanThongKe' => date_insert(_post('hangiaiquyet')),
                    'hanvb'  => _post('hanvb'),
					'iSoTrang' => _post('sotrang'),
					'iTrangThai_TruyenNhan' => '2',
					'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
					'FK_iMaDM'              => _post('domat'),
                    'FK_iMaDK'              => _post('dokhan'),
					'iVanBanQPPL' => (!empty(_post('iVanBanQPPL'))) ? _post('iVanBanQPPL') : '0',
					'iSTCChuTri' => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
					'iVanBanTBKL' => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
					'iVanBanMat' => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0',
                    'iToCongTac' => (!empty(_post('iToCongTac'))) ? _post('iToCongTac') : '0',
					'iDeadline' => (!empty(_post('iDeadline'))) ? _post('iDeadline') : NULL,
                    'iSTCPhoiHop' => (!empty(_post('iSTCPhoiHop'))) ? _post('iSTCPhoiHop') : '0'
				);
				$array_update = array();
				for ($i=0, $count = count($noidung); $i<$count; $i++)
				{
					$array_update[] = array(
						'sTenKV' => _post('khuvuc'),
						'sKyHieu' => _post('sokyhieu'),
						'sTenLVB' => _post('loaivanban'),
						'sTenDV' => _post('donvi'),
						'sNgayKy' => date_insert(_post('ngayky')),
						'sTenLV' => _post('linhvuc'),
						'sMoTa' => _post('trichyeu'),
						'sNoiDung'  => _post('noidunghop')[$i],
						'sTenNguoiKy' => _post('nguoiky'),
						'sNgayNhan' => date_insert(_post('ngaynhan')),
						'sNgayNhap' => date("Y-m-d H:i:s", time()),
						'iSoDen' => $soden,
						'sChucVu' => _post('chucvu'),
						'sHanGiaiQuyet' => (!empty(_post('iToCongTac'))) ? '0000-00-00' : date_insert(_post('hangiaiquyet')),
						'iSoTrang' => _post('sotrang'),
                        'hanvb'  => '1',
						'iTrangThai_TruyenNhan' => '2',
						'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
						'FK_iMaDM'              => _post('domat'),
                        'FK_iMaDK'              => _post('dokhan'),
						'iVanBanQPPL' => (!empty(_post('iVanBanQPPL'))) ? _post('iVanBanQPPL') : '0',
						'iSTCChuTri' => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
						'iVanBanTBKL' => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
						'iVanBanMat' => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0',
                        'iToCongTac' => (!empty(_post('iToCongTac'))) ? _post('iToCongTac') : '0',
						'iDeadline' => (!empty(_post('iDeadline'))) ? _post('iDeadline') : NULL,
                        'iSTCPhoiHop' => (!empty(_post('iSTCPhoiHop'))) ? _post('iSTCPhoiHop') : '0',
						'sHanNoiDung' => (!empty(_post('hannoidung')[$i])) ? date_insert(_post('hannoidung')[$i]) : (!empty(_post('iToCongTac'))) ? '0000-00-00' : ((!empty(_post('iSTCPhoiHop'))) ? '0000-00-00' :date_insert(_post('hangiaiquyet'))),
						'sHanThongKe' => (!empty(_post('hannoidung')[$i])) ? date_insert(_post('hannoidung')[$i]) : (!empty(_post('iToCongTac'))) ? '0000-00-00' : ((!empty(_post('iSTCPhoiHop'))) ? '0000-00-00' :date_insert(_post('hangiaiquyet')))

					);
				}
				
//	            pr($data_doc_go);
				if(empty($array_update[0]['sNoiDung'])){
				   // pr($data_doc_go);
					$DocGo = $this->Mdanhmuc->themDuLieu('tbl_vanbanden',$data_doc_go);
					if ($DocGo > 0) {
						// upload file
						$vbvuatao = $this->Mdanhmuc->layVBDenVuaTao($soden, _post('sokyhieu'));
						$this->themDuLieu2($vbvuatao[0]['PK_iMaVBDen']);
						return messagebox('Thêm thành công số đến là: ' .$soden, 'info');
					} else {
						return messagebox('Thêm văn bản đến thất bại', 'danger');
					}
				}else{
	//                pr($array_update);
					$DocGo = $this->Mdanhmuc->themNhieuDuLieu('tbl_vanbanden',$array_update);
					if ($DocGo > 0) {
						return messagebox('Thêm thành công số đến là: ' .$soden, 'info');
					} else {
						return messagebox('Thêm văn bản đến thất bại', 'danger');
					}

				}
            }
            else{
                $getAppo = $this->Mvanbanden->layDuLieuEnd(NULL,NULL,$_SESSION['nam']);
                $soden = $getAppo['iSoDen']+1;
                $data_doc_go[] = array(
                    'sTenKV' => _post('khuvuc'),
                    'sKyHieu' => _post('sokyhieu'),
                    'sTenLVB' => _post('loaivanban'),
                    'sTenDV' => _post('donvi'),
                    'sNgayKy' => date_insert(_post('ngayky')),
                    'sTenLV' => _post('linhvuc'),
                    'sMoTa' => _post('trichyeu'),
                    'iSoDen' => $soden,
                    'sTenNguoiKy' => _post('nguoiky'),
                    'sNgayNhan' => date_insert(_post('ngaynhan')),
                    // 'sNgayNhap' => date("Y-m-d"),
                    'sChucVu' => _post('chucvu'),
                    'sHanGiaiQuyet' => date_insert(_post('hangiaiquyet')),
                    'sHanThongKe' => date_insert(_post('hangiaiquyet')),
                    'hanvb'  => _post('hanvb'),
                    'iSoTrang' => _post('sotrang'),
                    'iTrangThai_TruyenNhan' => '2',
                    'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
					'FK_iMaDM'              => _post('domat'),
                    'FK_iMaDK'              => _post('dokhan'),
                    'iVanBanQPPL' => (!empty(_post('iVanBanQPPL'))) ? _post('iVanBanQPPL') : '0',
                    'iSTCChuTri' => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
                    'iVanBanTBKL' => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
                    'iVanBanMat' => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0',
                    'iToCongTac' => (!empty(_post('iToCongTac'))) ? _post('iToCongTac') : '0',
					'iDeadline' => (!empty(_post('iDeadline'))) ? _post('iDeadline') : NULL,
                    'sHoTen' => $this->_session['sHoTen'],
                    'sNoiDung' => ''
                );
                $this->mang = $data_doc_go;
                return messagebox('Văn bản này đã tồn tại trong hệ thống!','danger');
            }
        }
    }
    /** Cập nhật văn bản đến */
    public function updateDocGo($idDocGo){
        if($this->input->post('luulai')){
            $data_doc_go = array(
                'sTenKV'      => _post('khuvuc'),
                'sKyHieu'       => _post('sokyhieu'),
                'sTenLVB'       => _post('loaivanban'),
                'sTenDV'        => _post('donvi'),
                'sNgayKy'       => date_insert(_post('ngayky')),
                'sTenLV'        => _post('linhvuc'),
                'sMoTa'         => _post('trichyeu'),
                'sTenNguoiKy'   => _post('nguoiky'),
                'sNgayNhan'     => date_insert(_post('ngaynhan')),
                'iSoDen'        => _post('soden'),
                'sChucVu'       => _post('chucvu'),
                'sHanGiaiQuyet' => date_insert(_post('hangiaiquyet')),
                'sHanThongKe' => date_insert(_post('hangiaiquyet')),
                'hanvb'  => _post('hanvb'),
                'iSoTrang'      => _post('sotrang'),
                //'sNgayNhap'     => date("Y-m-d H:i:s", time()),
                'PK_iMaCapNhat' => $this->_session['PK_iMaCB'],
				'FK_iMaDM'              => _post('domat'),
                'FK_iMaDK'              => _post('dokhan'),
                'iVanBanQPPL'   => (!empty( _post('iVanBanQPPL'))) ?  _post('iVanBanQPPL') : '0',
                'iSTCChuTri'   => (!empty(_post('iSTCChuTri'))) ? _post('iSTCChuTri') : '0',
                'iVanBanTBKL'  => (!empty(_post('iVanBanTBKL'))) ? _post('iVanBanTBKL') : '0',
                'iSTCPhoiHop' => (!empty(_post('iSTCPhoiHop'))) ? _post('iSTCPhoiHop') : '0',
                'iVanBanMat'   => (!empty(_post('iVanBanMat'))) ? _post('iVanBanMat') : '0',
                'sNoiDung'  => _post('noidunghop')[0],
                'iToCongTac' => (!empty(_post('iToCongTac'))) ? _post('iToCongTac') : '0',
				'iDeadline' => (!empty(_post('iDeadline'))) ? _post('iDeadline') : NULL,
                'sHanNoiDung' => date_insert(_post('hangiaiquyet'))

            );
//            pr($data_doc_go);
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDocGo,'tbl_vanbanden',$data_doc_go);
			$this->themDuLieu2($idDocGo);
            return redirect(base_url().'dsvanbanden');
        }
    }
    public function checkSoDen()
    {
//        $soden = $this->input->post('soden');
        $getAppo = $this->Mvanbanden->getSoDen(NULL,NULL,$_SESSION['nam']);
//        pr($getAppo);
        echo json_encode($getAppo);exit();
    }
    public function checkTrichYeu()
    {
        $trichyeu = $this->input->post('trichyeu');
        $getAppo1 = $this->Mvanbanden->getSoDen('sMoTa',$trichyeu);
//        pr($getAppo1);
        echo json_encode($getAppo1);exit();
    }
	
	public function themDuLieu2($mavanban)
	{
		
			$time = time();
			$name = $_FILES['files']['name'];			
			if(!empty($name[0]))
			{
				$this->upload('doc_vbanden_'.date('Y'),$time);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}
					$tmp = str_replace('$', '-', $tmp);
					$tmp = $tmp.'.'.$a[$c];
					
					echo $tmp;
					$files[] = array(
						'FK_iMaVBDen' => $mavanban,
						'sTenFile'    => $value,
						'sDuongDan'   => 'doc_vbanden_'.date('Y').'/den_up_'.$time.'_'.$tmp,
						'sThoiGian'   => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'    => $this->_session['PK_iMaCB']
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbden',$files);
				echo $kiemtra;
				if($kiemtra>0)
				{
					$this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$mavanban,'tbl_vanbanden','iFile',1);
					
				}
				
			}
		
	}
	public function upload($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {									
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'den_up_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cvanbanden.php */
/* Location: ./application/controllers/vanban/Cvanbanden.php */