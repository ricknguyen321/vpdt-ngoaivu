<?php

/**

 * Created by PhpStorm.

 * User: Minh Duy

 * Date: 9/18/2017

 * Time: 3:22 PM

 */

class Cduyetbaocao extends MY_Controller {



    public function __construct()

    {

        parent::__construct();

        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');

        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();

        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');

        $this->Mvanbanden = new Mvanbanden();

        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');

        $this->Mdanhmuc = new Mdanhmuc();

        $this->load->library('pagination');

    }

    public function index()

    {
        // danh sách phó phòng

        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);

//        pr($data['getAccountDeputyDirector']);

        // danh sách chuyên viên

        $data['getDepartment'] = $this->Mgiaymoichoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);

        //danh sách chờ xử lý

        $data['urlcv'] = $this->uri->segment(1);


//        pr($urlcv);
        //duyệt báo cáo
        if($this->input->post('duyet')){
            $idDoc =$this->input->post('duyet');
            $idND = $this->input->post('idND');
            $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idDoc,'tbl_soanbaocao','iTrangThai','0');
            $this->Mdanhmuc->setDuLieu('PK_iMaNoiDung',$idND,'tbl_noidungbaocao','sYKien',' ');
        }
        if($this->input->post('tuchoibc')){
            $this->TuChoiVB();
        }

//        $data['getDocAwaitPPH'] = $this->Mgiaymoichoxuly->getDocAwaitPPH(NULL,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);

//        pr($data['getDocAwaitPPH']);

        /** @var danh sách tìm kiếm phân trang $page */

        $page               = $this->PhanTrang();

        $data['getDocAwait']    = $page['info'];

//        pr($page['info']);

        if(!empty($data['getDocAwait'])){

            foreach ($data['getDocAwait'] as $key => $value) {

                $process = $this->Mgiaymoichoxuly->getDocProcess($value['PK_iMaVBDen']);

                $processcv = $this->Mgiaymoichoxuly->getDocProcesscv($value['PK_iMaVBDen']);

                $processpb = $this->Mgiaymoichoxuly->getDocProcessPB($value['PK_iMaVBDen']);

                $process1 = array_merge($process,$processpb,$processcv);
                // danh sach cac phong duoc gui
                $listphong =  $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$value['PK_iMaVBDen'],'tbl_soanbaocao');
                $idTenPhong = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_phongban');
                $mang_phong = array();
                foreach($listphong as $key2 => $value2){
                    foreach ($idTenPhong as $key1 => $value1){
                        if($value2['PK_iMaPB'] == $value1['PK_iMaPB']){
                            $mang_phong[$key2] = $value1['sTenPB'];
                        }
                    }
                }


                $arrayTT = array();

                if(!empty($process1)){

                    foreach($process1 as $key1 => $value1){

                        $arrayTT[$key1] = $value1['sHoTen'];

                    }

                }

                $data['getDocAwait'][$key]['TrinhTu'] = $arrayTT;
                $data['getDocAwait'][$key]['TenPhong'] = $mang_phong;

//                pr($arrayCBNhan);

            }

//            pr($data['getDocAwait']);

        }

        $data['phantrang']     = $page['pagination'];

        $data['loaivanban']      = $page['loaivanban'];

        $data['sokyhieu']    = $page['sokyhieu'];

        $data['ngaynhap']       = $page['ngaynhap'];

        $data['donvi']        = $page['donvi'];

        $data['ngayky']          = $page['ngayky'];

        $data['ngayden']      = $page['ngayden'];

        $data['trichyeu']      = $page['trichyeu'];

        $data['nguoiky']   = $page['nguoiky'];

        $data['soden']     = $page['soden'];

        $data['denngay']    = $page['denngay'];

        $data['chucvu']   =$page['chucvu'];

        $data['nguoinhap']  = $page['nguoinhap'];

        $data['cacloaivanban'] = $page['cacloaivanban'];

        $data['count'] = $page['count'];

        // trình tự xử lý

//        pr($data['process']);

        $data['title']    = 'Van ban chuyen vien xu ly';

        $temp['data']     = $data;

        $temp['template'] = 'vanbanden/Vduyetbaocao';

        $this->load->view('layout_admin/layout',$temp);

    }

    public function PhanTrang()

    {

        $loaivanban    = $this->input->get('loaivanban');

        $sokyhieu = $this->input->get('sokyhieu');

        $ngaynhap      = $this->input->get('ngaynhap');

        $donvi    = $this->input->get('donvi');

        $ngayky    = $this->input->get('ngayky');

        $ngayden = $this->input->get('ngayden');

        $trichyeu = $this->input->get('trichyeu');

        $nguoiky = $this->input->get('nguoiky');

        $soden = $this->input->get('soden');

        $denngay = $this->input->get('denngay');

        $cacloaivanban = $this->input->get('cacloaivanban');

//        pr($this->input->get('denngay'));

        $chucvu = $this->input->get('chucvu');

        $nguoinhap = $this->input->get('nguoinhap');

        $config['base_url']             = base_url().'duyetbaocao?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;

        $config['total_rows']           = count($this->Mgiaymoichoxuly->getDuyetBaoCao(NULL,$this->_session['FK_iMaPhongHD'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));

//        pr($config['total_rows']);

        $config['per_page']             = 10;

        $config['page_query_string']    = TRUE;

        $config['query_string_segment'] = 'page';

        $config['num_links']            = 9;

        $config['use_page_numbers']     = false;

        $config['full_tag_open']        = '<ul class="pagination">';

        $config['full_tag_close']       = '</ul>';

        $config['first_link']           = '&laquo';

        $config['last_link']            = '&raquo';

        $config['first_tag_open']       = '<li>';

        $config['first_tag_close']      = '</li>';

        $config['prev_link']            = '&lt';

        $config['prev_tag_open']        = '<li class="prev">';

        $config['prev_tag_close']       = '</li>';

        $config['next_link']            = '&gt;';

        $config['next_tag_open']        = '<li>';

        $config['next_tag_close']       = '</li>';

        $config['last_tag_open']        = '<li>';

        $config['last_tag_close']       = '</li>';

        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";

        $config['cur_tag_close']        = '</a></li>';

        $config['num_tag_open']         = '<li>';

        $config['num_tag_close']        = '</li>';



        $this->pagination->initialize($config);



        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;



        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {

            redirect(base_url().'vanbanchoxuly_pgd');

        }

        $data['loaivanban']    = $loaivanban;

        $data['sokyhieu']      = $sokyhieu;

        $data['ngaynhap'] = $ngaynhap;

        $data['donvi'] = $donvi;

        $data['ngayky'] = $ngayky;

        $data['ngayden'] = $ngayden;

        $data['trichyeu'] = $trichyeu;

        $data['nguoiky'] = $nguoiky;

        $data['soden'] = $soden;

        $data['denngay'] =$denngay;

        $data['chucvu'] = $chucvu;

        $data['nguoinhap'] = $nguoinhap;

        $data['cacloaivanban'] =$cacloaivanban;

        $data['count']    = $config['total_rows'];

        $data['info']        = $this->Mgiaymoichoxuly->getDuyetBaoCao(NULL,$this->_session['FK_iMaPhongHD'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);

        $data['pagination']  = $this->pagination->create_links();

//        pr($data['info']);

        return $data;

    }

    public function TuChoiVB(){
        $result = $this->input->post('data');
        $idDoc = $this->input->post('maphongbantp');
        $idContent = $this->input->post('idNoiDung');
        $this->Mdanhmuc->capnhatDuLieu('PK_iMaNoiDung',$idContent,'tbl_noidungbaocao',$result);
        // trạng thái là 2 - từ chối
        $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idDoc,'tbl_soanbaocao','iTrangThai',2);
    }



}