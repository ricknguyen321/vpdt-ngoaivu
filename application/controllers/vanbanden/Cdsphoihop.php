<?php

/**

 * Created by PhpStorm.

 * User: Minh Duy

 * Date: 6/27/2017

 * Time: 3:13 PM

 */

class Cdsphoihop extends MY_Controller {



    public function __construct()

    {

        parent::__construct();

        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');

        $this->Mvanbanchoxuly = new Mvanbanchoxuly();

        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');

        $this->Mvanbanden = new Mvanbanden();

        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');

        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');

    }

    public function index()

    {
        $data['urlcv'] = $this->uri->segment(1);
        if($this->_session['iQuyenHan_DHNB'] == 7){
            $data['content'] = $this->Reject();
        }

        //danh sách chờ xử lý

//        pr($data['getDocAwaitPPH']);
        $page               = $this->PhanTrang();
        $data['getDocAwaitPPH']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['getDocAwaitPPH'])){
            foreach ($data['getDocAwaitPPH'] as $key => $value) {
                $process = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processcv = $this->Mvanbanchoxuly->getDocProcesscv($value['PK_iMaVBDen']);
                $processpb = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $process1 = array_merge($process,$processpb,$processcv);
                if(!empty($process)){
                    $data['getDocAwaitPPH'][$key]['TrinhTu'] = $process[0]['sHoTen'];
                }
//                pr($arrayCBNhan);
                $processph =  $this->Mvanbanchoxuly->getDocProcessCVPPH($value['PK_iMaVBDen'],NULL, $this->_session['PK_iMaCB']);
//                pr($process1);
                $arrayTT1 = array();
                if(!empty($processph)){
                    foreach($processph as $key2 => $value2){
                        $arrayTT1[$key2] = $value2['sHoTen'];
                    }
                }
                $data['getDocAwaitPPH'][$key]['TrinhTuPH'] = $arrayTT1;
            }
//            pr($data['getDocAwaitPPH']);
        }
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['count'] = $page['count'];

//        pr($data['process']);

        $data['title']    = 'Danh sách VB đến phòng phối hợp';

        $temp['data']     = $data;

        $temp['template'] = 'vanbanden/Vdsphongphoihop';

        $this->load->view('layout_admin/layout',$temp);

    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'dsphoihop?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwaitDSCTPPH(NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap));

        $PK_iMaVBDen ='';
        $PK_iMaVBDen = $this->input->get('PK_iMaVBDen');
        if($PK_iMaVBDen !=''){
            $config['base_url']             = base_url().'dsphoihop?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&PK_iMaVBDen='.$PK_iMaVBDen;
            $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwaitDSCTPPH2($PK_iMaVBDen));
        }
//        pr($config['total_rows']);
        $config['per_page']             = 30;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'dsphoihop');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwaitDSCTPPH(NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
        if($PK_iMaVBDen !=''){
            $data['info']        = $this->Mvanbanchoxuly->getDocAwaitDSCTPPH2($PK_iMaVBDen);
        }
//        pr($data['info']);
        return $data;
    }
    public function Reject(){
        if($this->input->post('tuchoi')){
            $ma = $this->input->post('maphongban');
            $data_tc = array(
                'iTrangThai_TuChoi' => 1,
                'sNoiDung_TuChoi' => $this->input->post('noidungtuchoi')
            );
            $DocGo =  $this->Mdanhmuc->capnhatDuLieu2('PK_iMaVBDen',$ma,'PK_iMaCB',$this->_session['PK_iMaCB'],'tbl_phongphoihop',$data_tc);
            if ($DocGo > 0) {
                return messagebox('Chuyền văn bản về Trưởng phòng thành công', 'info');
            } else {
                return messagebox('Chuyền văn bản về Trưởng phòng thất bại', 'danger');
            }
        }
    }



}