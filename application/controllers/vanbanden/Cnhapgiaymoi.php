<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Cnhapgiaymoi extends MY_Controller {
    protected $mang;
    protected $mangnhieu;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
	}
	public function index()
	{
        $quyen = $this->_session['iQuyenHan_DHNB'];
		$macb = $this->_session['PK_iMaCB'];
        if($quyen!=9 && $quyen!=3 && $macb != 705 && $macb != 730 && $macb != 731)
        {
            redirect('dangnhap');
        }
        $action = _post('action');
        switch ($action) {
            case 'layChucVu':
                $this->layChucVu();
                break;
            default:
                # code...
                break;
        }
        $data['content'] = $this->insertdAppointment();
        $getAppo = $this->Mvanbanden->layDuLieuEnd(NULL,NULL,$_SESSION['nam']);
        $data['getDocGo'] = $this->mang;
        $data['getDocGos'] = $this->mangnhieu;
//        pr($this->mangnhieu);
        $data['soden'] = $getAppo['iSoDen']+1;
		$data['title']    = 'Nhập mới giấy mời';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vnhapgiaymoi';
		$this->load->view('layout_admin/layout',$temp);
	}
    // lấy chức vụ người ký
    public function layChucVu()
    {
        $hoten = _post('hoten');
        $chucvu = $this->Mdanhmuc->layDuLieu('sHoTen',$hoten,'tbl_nguoiky');
        if(!empty($chucvu))
        {
            $chucvunguoiky= $chucvu[0]['sChucVu'];
        }
        else{
            $chucvunguoiky ='';
        }
        if(empty($hoten))
        {
            $chucvunguoiky ='';
        }
        echo json_encode($chucvunguoiky);exit();
    }
	/** Thêm giấy mời */
    public function insertdAppointment(){
        if ($this->input->post('luulai')) {
            $sKyHieu    = trim(_post('sokyhieu'));
            $ngayky     = trim(date_insert(_post('ngayky')));
			$noigui     = trim(_post('noiguiden'));
            $nguoiky     = trim(_post('nguoiky'));
            $kiemtra = $this->Mdanhmuc->kiemtraTrung1($sKyHieu,$ngayky,$nguoiky);
			//$kiemtra = $this->Mdanhmuc->kiemtra_Trung($sKyHieu,$noigui,$nguoiky);
            if($kiemtra==0)
            {
                if($this->_session['PK_iMaCB'] == 577){
                    sleep(0.3);
                }
                if($this->_session['PK_iMaCB'] == 136){
                    sleep(0.2);
                }
                if($this->_session['PK_iMaCB'] == 81){
                    sleep(0.1);
                }
                $noidung = $this->input->post('noidunghop');
                $getAppo = $this->Mvanbanden->layDuLieuEnd(NULL,NULL,$_SESSION['nam']);
				$giohop1        = _post('giohopgm');
				
				//ricknguyen321 xử lý định dạng giờ
				$giohop1 = str_replace('h', ':', $giohop1);
				$giohop1 = str_replace(' giờ ', ':', $giohop1);
				$giohop1 = str_replace('giờ', ':', $giohop1);
				$giohop1 = str_replace('phút', '', $giohop1);
				$giohop1 = str_replace(' : ', ':', $giohop1);
				$manggio = explode(':',$giohop1);
				if (count($manggio) == 2) {
					$gio = $manggio[0];
					$phut = $manggio[1];			
					if ($gio == '1') $gio = '01';
					if ($gio == '2') $gio = '02';
					if ($gio == '3') $gio = '03';
					if ($gio == '4') $gio = '04';
					if ($gio == '5') $gio = '05';
					if ($gio == '6') $gio = '06';
					if ($gio == '7') $gio = '07';
					if ($gio == '8') $gio = '08';
					if ($gio == '9') $gio = '09';
					if ($phut == '0') $phut = '00';
					if ($phut == '1') $phut = '01';
					if ($phut == '2') $phut = '02';
					if ($phut == '3') $phut = '03';
					if ($phut == '4') $phut = '04';
					if ($phut == '5') $phut = '05';
					if ($phut == '6') $phut = '06';
					if ($phut == '7') $phut = '07';
					if ($phut == '8') $phut = '08';
					if ($phut == '9') $phut = '09';			
					$giohop1 = $gio.':'.$phut;
				}
				
                $soden = $getAppo['iSoDen']+1;
                $data_Appo = array(
                    'sKyHieu'           => _post('sokyhieu'),
                    'sTenDV'            => _post('noiguiden'),
                    'sTenLVB'           => 'Giấy mời',
                    'sNgayKy'           => date_insert(_post('ngayky')),
                    'sMoTa'             => _post('noidung'),
                    'sTenNguoiKy'       => _post('nguoiky'),
                    'sGiayMoiGio'       => $giohop1,
                    'sGiayMoiNgay'      => (_post('ngayhopgm')&&_post('ngayhopgm')!='dd/mm/yyyy')?date_insert(_post('ngayhopgm')):'0000-00-00',
					'sHanThongKe'      => (_post('ngayhopgm')&&_post('ngayhopgm')!='dd/mm/yyyy')?date_insert(_post('ngayhopgm')):'1970-01-01',
                    'sGiayMoiDiaDiem'   => _post('diadiem'),
                    'sGiayMoiChuTri'    => _post('nguoichutri'),
                    'sChucVu'           => _post('chucvu'),
                    'iSoTrang'          => _post('sotrang'),
                    'iSoDen'            => $soden,
                    'sNgayNhap'         => date('Y-m-d H:i:s'),
                    'sNgayNhan'         => date_insert(_post('ngaynhan')),
                    'FK_iMaNguoiNhap'   => $this->_session['PK_iMaCB'],
                    'iGiayMoi'          => '1',
                    'iTrangThai_TruyenNhan'        => '2',
                    'vbdaura_tp'        => 1,
                    'vbdaura_tp'        => 1,
                    'vbdaura_tp'        => 1
                );
                $array_update = array();
                for ($i=0, $count = count($noidung); $i<$count; $i++)
                {
                    $array_update[] = array(
                        'sKyHieu'         => _post('sokyhieu'),
                        'sTenDV'          => _post('noiguiden'),
                        'sTenLVB'         => 'Giấy mời',
                        'sNgayKy'         => date_insert(_post('ngayky')),
                        'sMoTa'           => _post('noidung'),
                        'sNoiDung'        => _post('noidunghop')[$i],
                        'sTenNguoiKy'     => _post('nguoiky'),
                        'sGiayMoiGio'     => (_post('giohop')[$i]) ? (_post('giohop')[$i]) : (_post('giohopgm')),
                        'sGiayMoiNgay'    => (_post('ngayhop')[$i]) ? date_insert(_post('ngayhop')[$i]) : date_insert(_post('ngayhopgm')),
                        'sGiayMoiDiaDiem' => (_post('diadiemhop')[$i]) ?_post('diadiemhop')[$i] : _post('diadiem') ,
                        'sGiayMoiChuTri'  => _post('nguoichutri'),
                        'sChucVu'         => _post('chucvu'),
                        'iSoTrang'        => _post('sotrang'),
                        'iSoDen'          => $soden,
                        'sNgayNhap'       => date('Y-m-d H:i:s'),
                        'sNgayNhan'       => date_insert(_post('ngaynhan')),
                        'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
                        'iGiayMoi'        => '1',
                        'iTrangThai_TruyenNhan'        => '2',
                        'vbdaura_tp'        => 1,
                        'vbdaura_tp'        => 1,
                        'vbdaura_tp'        => 1
                    );
                }
//                pr($array_update);
                if(empty($array_update[0]['sNoiDung'])){
                    $Appo = $this->Mdanhmuc->themDuLieu('tbl_vanbanden',$data_Appo);
                    if ($Appo > 0) {
						$vbvuatao = $this->Mdanhmuc->layVBDenVuaTao($soden, _post('sokyhieu'));
						$this->themDuLieu2($vbvuatao[0]['PK_iMaVBDen']);
                        return messagebox('Thêm thành công số đến là: ' .$soden, 'info');
                    } else {
                        return messagebox('Thêm giấy mời đi thất bại', 'danger');
                    }

                }else{
                    $Appo1 = $this->Mdanhmuc->themNhieuDuLieu('tbl_vanbanden',$array_update);
                    if ($Appo1 > 0) {
                        return messagebox('Thêm thành công số đến là: ' .$soden, 'info');
                    } else {
                        return messagebox('Thêm giấy mời đi thất bại', 'danger');
                    }

                }
            }else{
                $getAppo = $this->Mvanbanden->layDuLieuEnd(NULL,NULL,$_SESSION['nam']);
                $soden = $getAppo['iSoDen']+1;
                $data_doc_go[] = array(
                    'sKyHieu'           => _post('sokyhieu'),
                    'sTenDV'            => _post('noiguiden'),
                    'sTenLVB'           => 'Giấy mời',
                    'sNgayKy'           => date_insert(_post('ngayky')),
                    'sMoTa'             => _post('noidung'),
                    'sTenNguoiKy'       => _post('nguoiky'),
                    'sGiayMoiGio'       => _post('giohopgm'),
                    'sGiayMoiNgay'      => date_insert(_post('ngayhopgm')),
                    'sGiayMoiDiaDiem'   => _post('diadiem'),
                    'sGiayMoiChuTri'    => _post('nguoichutri'),
                    'sChucVu'           => _post('chucvu'),
                    'iSoTrang'          => _post('sotrang'),
                    'iSoDen'            => $soden,
                    'sNgayNhap'         => date('Y-m-d H:i:s'),
                    'sNgayNhan'         => date_insert(_post('ngaynhan')),
                    'FK_iMaNguoiNhap'   => $this->_session['PK_iMaCB'],
                    'iGiayMoi'          => '1',
                    'iTrangThai_TruyenNhan'        => '2',
                    'sHoTen' => $this->_session['sHoTen'],
                    'sNoiDung' => ''
                );
                $array_update = array();
                $noidung = $this->input->post('noidunghop');
                for ($i=0, $count = count($noidung); $i<$count; $i++)
                {
                    $array_update[] = array(
                        'sKyHieu'         => _post('sokyhieu'),
                        'sTenDV'          => _post('noiguiden'),
                        'sTenLVB'         => 'Giấy mời',
                        'sNgayKy'         => date_insert(_post('ngayky')),
                        'sMoTa'           => _post('noidung'),
                        'sNoiDung'        => _post('noidunghop')[$i],
                        'sTenNguoiKy'     => _post('nguoiky'),
                        'sGiayMoiGio'     => (_post('giohop')[$i]) ? (_post('giohop')[$i]) : (_post('giohopgm')),
                        'sGiayMoiNgay'    => (_post('ngayhop')[$i]) ? date_insert(_post('ngayhop')[$i]) : date_insert(_post('ngayhopgm')),
                        'sGiayMoiDiaDiem' => (_post('diadiemhop')[$i]) ?_post('diadiemhop')[$i] : _post('diadiem') ,
                        'sGiayMoiChuTri'  => _post('nguoichutri'),
                        'sChucVu'         => _post('chucvu'),
                        'iSoTrang'        => _post('sotrang'),
                        'iSoDen'          => $soden,
                        'sNgayNhap'       => date('Y-m-d H:i:s'),
                        'sNgayNhan'       => date_insert(_post('ngaynhan')),
                        'FK_iMaNguoiNhap' => $this->_session['PK_iMaCB'],
                        'iGiayMoi'        => '1',
                        'iTrangThai_TruyenNhan'        => '2',
                        'vbdaura_tp'        => 1,
                        'vbdaura_tp'        => 1,
                        'vbdaura_tp'        => 1
                    );
                }

//                pr($array_update);
                $this->mang = $data_doc_go;
                $this->mangnhieu = $array_update;
                return messagebox('Văn bản này đã tồn tại trong hệ thống!','danger');
            }
        }
    }
	
	public function themDuLieu2($mavanban)
	{
		echo $mavanban;
		
			$time = time();
			$name = $_FILES['files']['name'];			
			if(!empty($name[0]))
			{
				$this->upload('doc_vbanden_'.date('Y'),$time);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}
					$tmp = str_replace('$', '-', $tmp);
					$tmp = $tmp.'.'.$a[$c];
					
					echo $tmp;
					$files[] = array(
						'FK_iMaVBDen' => $mavanban,
						'sTenFile'    => $value,
						'sDuongDan'   => 'doc_vbanden_'.date('Y').'/den_up_'.$time.'_'.$tmp,
						'sThoiGian'   => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'    => $this->_session['PK_iMaCB']
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbden',$files);
				echo $kiemtra;
				if($kiemtra>0)
				{
					$this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$mavanban,'tbl_vanbanden','iFile',1);
					
				}
				
			}
		
	}
	public function upload($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {									
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'den_up_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */