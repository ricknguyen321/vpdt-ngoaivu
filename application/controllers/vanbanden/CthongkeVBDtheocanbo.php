<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CthongkeVBDtheocanbo extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        if($this->input->get('nam') == 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default', TRUE);
        }
        if($this->input->get('nam') > 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default_'.$this->input->get('nam'), TRUE);
        }
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->library('pagination');
	}
	public function index()
	{

        /** @var danh sách tìm kiếm phân trang $page */
        $page                  = $this->PhanTrang();
        $data['getDocGo']      = $page['info'];
        if(!empty($data['getDocGo'])){
            foreach ($data['getDocGo'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $processgd = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processpb1 = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                
                $soden_tam = $value['iSoDen'];
                $vbdi = $this->Mdanhmuc->getvanbandi("(iSoDen = ".$soden_tam." OR iSoDen LIKE '%,".$soden_tam.",%' OR iSoDen LIKE '%,".$soden_tam." ,%'  OR iSoDen LIKE '%, ".$soden_tam.",%'  OR iSoDen LIKE '%, ".$soden_tam." ,%' OR  iSoDen LIKE '%, ".$soden_tam."' OR  iSoDen LIKE '%,".$soden_tam."') and sNgayNhap >'".date('Y')."-01-01'",'tbl_vanbandi');

                    $data['getDocGo'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                    if(!empty($processgd)){
                        $data['getDocGo'][$key]['sChuTri'] = $processgd[0]['sMoTa'];
                    }
                    if(empty($processgd)){
                        $data['getDocGo'][$key]['sChuTri'] = '';
                    }
                    if(!empty($processpb1)){
                        $data['getDocGo'][$key]['sPhongChuTri'] = $processpb1[0]['sHoTen'];
                    }
                    if(empty($processpb1)){
                        $data['getDocGo'][$key]['sPhongChuTri'] = '';
                    }
                    if(!empty($vbdi)){
                        $data['getDocGo'][$key]['VBDi'] = $vbdi[0]['PK_iMaVBDi'];
                        $data['getDocGo'][$key]['iSoVBDi'] = $vbdi[0]['iSoVBDi'];
                        $data['getDocGo'][$key]['sKyHieuVBDi'] = $vbdi[0]['sKyHieu'];
                    }
//                pr($vbdi);
            }
        }

        $ngayhoanthanh='';
        if($this->input->get('ngaynhap') > '01/01/2017'){
            $ngayhoanthanh .=' and ngay_hoanthanh >= "' .date_insert($this->input->get('ngaynhap')).'"' ;
        } 
        if($this->input->get('ngayden') > '01/01/2017'){
            $ngayhoanthanh .=' and ngay_hoanthanh <= "' .date_insert($this->input->get('ngayden')).'"' ;
        } 

        $list_kh = $this->Mdanhmuc->get_kehoach3('canbo_id ='.$this->_session['PK_iMaCB'].' and loai_kh > 1   and vanban_id is null and active =5 '.$ngayhoanthanh,'kehoach','canbo_id,active');
//        pr($list_kh);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']    = $page['loaivanban'];
        $data['sokyhieu']      = $page['sokyhieu'];
        $data['ngaynhap']      = $page['ngaynhap'];
//        pr($data['ngaynhap']);
        $data['donvi']         = $page['donvi'];
        $data['ngayky']        = $page['ngayky'];
        $data['ngayden']       = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['ngaymoi']       = $page['ngaymoi'];
        $data['nguoiky']       = $page['nguoiky'];
        $data['soden']         = $page['soden'];
        $data['denngay']       = $page['denngay'];
        $data['chucvu']        = $page['chucvu'];
        $data['nguoinhap']     = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['list_kh']       = $list_kh;
        $data['count_kh']       = count($list_kh);
        $data['count']         = $page['count'];
        $data['nam'] = $page['nam'];


		$data['title']    = 'Danh sách tổng thể';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/VthongkeVBDtheocanbo';
		$this->load->view('layout_admin/layout',$temp);
	}
    public function PhanTrang()
    {
//        pr($this->_session);
//        pr($this->input->get('id'));
        if($this->_session['iQuyenHan_DHNB'] == 2 || $this->_session['iQuyenHan_DHNB'] == 9 || $this->_session['iQuyenHan_DHNB'] == 3 || $this->_session['iQuyenHan_DHNB'] == 4 || $this->_session['iQuyenHan_DHNB'] == 5){
            $quyenhan = NULL;
        }else{
            $quyenhan = 2;
        }
        $sPhongChuTri  = $this->input->get('sPhongChuTri');
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu      = $this->input->get('sokyhieu');
        $ngaynhap      = ($this->input->get('ngaynhap') > '01/01/2017') ? date_insert($this->input->get('ngaynhap')) : NULL;
        $donvi         = $this->input->get('donvi');
        $ngayky        = ($this->input->get('ngayky') > '01/01/2017') ? date_insert($this->input->get('ngayky')) : NULL;
        $ngayden       = ($this->input->get('ngayden') > '01/01/2017') ? date_insert($this->input->get('ngayden')) : NULL;
//        pr($ngayden);
        $trichyeu      = $this->input->get('trichyeu');
        $ngaymoi       = ($this->input->get('ngaymoi') > '01/01/2017') ? date_insert($this->input->get('ngaymoi')) : NULL;
        $nguoiky       = $this->input->get('nguoiky');
        $soden         = $this->input->get('soden');
        $denngay       = ($this->input->get('denngay') > '01/01/2017') ? date_insert($this->input->get('denngay')) : NULL;
        $chucvu        = $this->input->get('chucvu');
        $nguoinhap     = $this->input->get('nguoinhap');
        $cacloaivanban = $this->input->get('cacloaivanban');
        $canbothuly    = $this->_session['PK_iMaCB'];
        $nam = $this->input->get('nam');
        $id = $this->input->get('id');
        $idDocument    = $this->uri->segment(2);
        $config['base_url']             = base_url().'thongkeVBDtheocanbo?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&ngaymoi='.$ngaymoi.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&cacloaivanban='.$cacloaivanban.'&id='.$id.'&nam='.$nam.'&sPhongChuTri='.$sPhongChuTri.'&$canbothuly='.$canbothuly;
        $config['total_rows']           = $this->Mvanbanden->countDocGo1($id,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,$quyenhan,$sPhongChuTri,$canbothuly);
//        pr($config['total_rows']);
        $config['per_page']             = 10000;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 18;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'thongkeVBDtheocanbo');
        }
        $data['sPhongChuTri']  = $sPhongChuTri;
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap']      = $ngaynhap;
        $data['donvi']         = $donvi;
        $data['ngayky']        = $ngayky;
        $data['ngayden']       = $ngayden;
        $data['trichyeu']      = $trichyeu;
        $data['ngaymoi']       = $ngaymoi;
        $data['nguoiky']       = $nguoiky;
        $data['soden']         = $soden;
        $data['denngay']       = $denngay;
        $data['chucvu']        = $chucvu;
        $data['nguoinhap']     = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['nam'] = $nam;
        $data['id'] = $id;
        $data['count']         = $config['total_rows'];
        $data['info']        = $this->Mvanbanden->getDocGo1($id,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,$config['per_page'], $data['page'],$quyenhan,$sPhongChuTri,$canbothuly);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */