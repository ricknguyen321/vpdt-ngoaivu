<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clistdetail_cb extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->library('pagination');
		$this->load->model('thongke/Mthongke','Mthongke');
        $this->Mthongke = new Mthongke();
    }
    public function index()
    {
		$data['page']    = _get('page');
        //trang thái là 3 là xóa văn bản
        if($this->input->post('xoavanban')){
            $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$this->input->post('xoavanban'),'tbl_vanbanden','iTrangThai','3');
        }
        /** @var danh sách tìm kiếm phân trang $page */
        $page                  = $this->PhanTrang();
        $data['getDocGo']      = $page['info'];
        if(!empty($data['getDocGo'])){
            foreach ($data['getDocGo'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $processgd = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processpb1 = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $data['getDocGo'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                if(!empty($processgd)){
                    $data['getDocGo'][$key]['sChuTri'] = $processgd[0]['sMoTa'];
                }
                if(empty($processgd)){
                    $data['getDocGo'][$key]['sChuTri'] = '';
                }
                if(!empty($processpb1)){
                    $data['getDocGo'][$key]['sPhongChuTri'] = $processpb1[0]['sHoTen'];
                }
                if(empty($processpb1)){
                    $data['getDocGo'][$key]['sPhongChuTri'] = '';
                }
//                pr($processpb1);
            }
        }
//         pr($data['getDocGo']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']    = $page['loaivanban'];
        $data['sokyhieu']      = $page['sokyhieu'];
        $data['ngaynhaptu']      = $page['ngaynhaptu'];
        $data['donvi']         = $page['donvi'];
        $data['ngayky']        = $page['ngayky'];
        $data['ngaynhapden']       = $page['ngaynhapden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['ngaymoi']       = $page['ngaymoi'];
        $data['nguoiky']       = $page['nguoiky'];
        $data['soden']         = $page['soden'];
        $data['chucvu']        = $page['chucvu'];
        $data['nguoinhap']     = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count']         = $page['count'];
		
		$id = $this->input->get('id');
		$pb = $this->input->get('pb');
		$data['id']       = $id;
		$data['pb']       = $pb;
		$chuagiaiquyet = $this->input->get('chuagiaiquyet');
        $dagiaiquyet = $this->input->get('dagiaiquyet');
		$data['dagiaiquyet']       = $dagiaiquyet;
		$data['chuagiaiquyet']     = $chuagiaiquyet;

        $data['title']    = 'Danh sách tổng thể';
        $temp['data']     = $data;        
		
		if(isset($_POST['ketxuatword'])){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=Danh sách văn bản".".doc");
			
            $temp['template'] = 'vanbanden/Vlistdetail_cb_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'vanbanden/Vlistdetail_cb';
			$this->load->view('layout_admin/layout',$temp);
        }
		
    }
    public function PhanTrang()
    {
//        pr($this->_session);
//        pr($this->input->get('id'));
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu      = $this->input->get('sokyhieu');
        $ngaynhaptu = ($this->input->get('ngaynhaptu') > '01/01/2017') ? date_insert($this->input->get('ngaynhaptu')) : NULL;
        $donvi         = $this->input->get('donvi');
        $ngayky        = $this->input->get('ngayky');
        $ngaynhapden = ($this->input->get('ngaynhapden') > '01/01/2017') ? date_insert($this->input->get('ngaynhapden')) : NULL;
        $trichyeu      = $this->input->get('trichyeu');
        $ngaymoi       = $this->input->get('ngaymoi');
        $nguoiky       = $this->input->get('nguoiky');
        $soden         = $this->input->get('soden');
        $chucvu        = $this->input->get('chucvu');
        $nguoinhap     = $this->input->get('nguoinhap');
        $cacloaivanban = $this->input->get('cacloaivanban');
        $id = $this->input->get('id');
		$data['id']       = $id;
		$pb = $this->input->get('pb');
		$data['pb']       = $pb;
		if (!empty($id)){
			$phong = $this->Mthongke->layMaPhong($id);
			$phongban = $phong[0]['FK_iMaPhongHD'];
		} else {
			$phongban = $this->_session['FK_iMaPhongHD'];
		}
		$canbo = $this->Mthongke->layDSCanBoPhong($phongban);
            $truongphong = 0;
			$quyen = 0;
            if(!empty($canbo))
            {
                foreach ($canbo as $key => $value) {
                    if($value['iQuyenHan_DHNB']==6 || $value['iQuyenHan_DHNB']==3)
                    {
                        $truongphong = $value['PK_iMaCB'];
                    }
					if($value['PK_iMaCB'] == $id)
                    {
                        $quyen = $value['iQuyenHan_DHNB'];
                    }
                }
            } 
			if ($quyen == 0) {
				$ttcb =  $this->Mdanhmuc->layTTCB($id);
				$quyen = $ttcb[0]['iQuyenHan_DHNB'];			
			}
		
        $idLanhDao = $this->input->get('idld');
//        pr($idLanhDao);
		$tp = $this->input->get('tp');
        $chuagiaiquyet = $this->input->get('chuagiaiquyet');
        $dagiaiquyet = $this->input->get('dagiaiquyet');
		$data['dagiaiquyet']       = $dagiaiquyet;
		$data['chuagiaiquyet']     = $chuagiaiquyet;
//        pr($dagiaiquyet);
        $sangtao = $this->input->get('sangtao');
        $idDocument    = $this->uri->segment(2);
        $config['base_url']             = base_url().'danhsachchitiet?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhaptu='.$ngaynhaptu.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngaynhapden='.$ngaynhapden.'&trichyeu='.$trichyeu.'&ngaymoi='.$ngaymoi.'&nguoiky='.$nguoiky.'&soden='.$soden.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&cacloaivanban='.$cacloaivanban.'&pb='.$pb.'&id='.$id.'&tp='.$tp.'&chuagiaiquyet='.$chuagiaiquyet.'&dagiaiquyet='.$dagiaiquyet;
        //$config['total_rows']           = count($this->Mvanbanden->getReportMembers($truongphong,$id,$chuagiaiquyet,$dagiaiquyet,$sangtao,2,$loaivanban,$sokyhieu,$ngaynhaptu,$donvi,$ngayky,$ngaynhapden,$trichyeu,$ngaymoi,$nguoiky,$soden,$chucvu,$nguoinhap,$cacloaivanban,$tp,$this->_session['FK_iMaPhongHD'],$idLanhDao));
//        pr($config['total_rows']);
        $config['per_page']             = 30;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 10;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

       

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;
        
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhaptu']      = $ngaynhaptu;
        $data['donvi']         = $donvi;
        $data['ngayky']        = $ngayky;
        $data['ngaynhapden']       = $ngaynhapden;
        $data['trichyeu']      = $trichyeu;
        $data['ngaymoi']       = $ngaymoi;
        $data['nguoiky']       = $nguoiky;
        $data['soden']         = $soden;
        $data['denngay']       = $denngay;
        $data['chucvu']        = $chucvu;
        $data['nguoinhap']     = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['id'] = $id;
		$data['pb'] = $pb;
       
        if (empty($pb)) {
			if ($chuagiaiquyet == 1 && $dagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportV2($id,array(0,2),NULL,1,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportV2($id,array(0,2),NULL,1,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} elseif ($chuagiaiquyet == 0 && $dagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportV2($id,array(0,2),NULL,2,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportV2($id,array(0,2),NULL,2,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} 
			if ($dagiaiquyet == 1 && $chuagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportV2($id,1,		  NULL,NULL,1,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportV2($id,1,		  NULL,NULL,1,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} elseif ($dagiaiquyet == 0 && $chuagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportV2($id,1,		  NULL,NULL,2,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportV2($id,1,		  NULL,NULL,2,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} 
		} else {
			if ($chuagiaiquyet == 1 && $dagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportPhong($pb,array(0,2),NULL,1,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportPhong($pb,array(0,2),NULL,1,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} elseif ($chuagiaiquyet == 0 && $dagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportPhong($pb,array(0,2),NULL,2,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportPhong($pb,array(0,2),NULL,2,	NULL,NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} 
			if ($dagiaiquyet == 1 && $chuagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportPhong($pb,1,		  NULL,NULL,1,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportPhong($pb,1,		  NULL,NULL,1,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} elseif ($dagiaiquyet == 0 && $chuagiaiquyet == '') {
				$data['info'] = $this->Mvanbanden->getReportPhong($pb,1,		  NULL,NULL,2,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
				$config['total_rows'] = $this->Mvanbanden->countReportPhong($pb,1,		  NULL,NULL,2,	 NULL,NULL,NULL,$truongphong,$ngaynhaptu,$ngaynhapden,$quyen,$config['per_page'], $data['page']);
			} 
		}
		$this->pagination->initialize($config);
		
		$data['count']         = $config['total_rows'];
		if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'danhsachchitiet');
        }
		
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}

/* End of file Clistdetail_cb.php */
/* Location: ./application/controllers/vanban/Clistdetail_cb.php */