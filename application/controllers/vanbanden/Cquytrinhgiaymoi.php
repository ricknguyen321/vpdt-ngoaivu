<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/15/2017
 * Time: 10:25 AM
 */
class Cquytrinhgiaymoi extends MY_Controller {



    public function __construct()

    {

        parent::__construct();

        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');

        $this->Mdanhmuc = new Mdanhmuc();

        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
		$this->load->model('vanbandi/Mtruyennhan');

        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();

    }

    public function index()

    {
        $data['nam'] = $this->input->get('nam');
        if(!empty($data['nam'])){
            if($this->input->get('nam') == 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default', TRUE);
            }
            if($this->input->get('nam') > 2017){
                $CI = &get_instance();
                $this->db = $CI->load->database('default_'.$this->input->get('nam'), TRUE);
            }
        }
//        pr($this->_session);

        $idAppointment = $this->uri->segment(2);
//        pr($idAppointment);

        // trình tự xử lý

		$canbo                    = $this->Mtruyennhan->layDSCB2();
        foreach ($canbo as $key => $value) {
            $data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        $process = $this->Mgiaymoichoxuly->getDocProcess($idAppointment);
        $processcv = $this->Mgiaymoichoxuly->getDocProcesscv($idAppointment);
        $processpb = $this->Mgiaymoichoxuly->getDocProcessPB($idAppointment);
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');
		$data['luuvetchuyennhan']    = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_luuvet_chuyennhan');
		 $data['luuvetduyet']    = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$idAppointment,'tbl_luuvet_duyet');
//        pr($process);
//        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($this->_session);
        $data['idDocGo'] = $idAppointment;
        $data['process'] = array_merge($process,$processpb,$processcv);
		
		$data['chidao']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_chidao');
		$data['dexuat']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_ykien');
		
		if ($this->input->post('luulai')) {
			$chidaold = array(
                'FK_iMaVBDen'        => $idAppointment,
                'sNoiDung'        => nl2br($this->input->post('chidao')),
                'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
                'sThoiGian'   => date('Y-m-d H:i:s',time()),
            );
			
			$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
			return redirect(base_url().'quytrinhgiaymoi/'.$idAppointment);
		}
		
		if ($this->input->post('luuykien')) {
			$chidaold = array(
                'FK_iMaVBDen'        => $idAppointment,
                'sNoiDung'        => nl2br($this->input->post('dexuat')),
                'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
                'sThoiGian'   => date('Y-m-d H:i:s',time()),
            );
			
			$this->Mdanhmuc->themDuLieu('tbl_ykien',$chidaold);
			return redirect(base_url().'quytrinhgiaymoi/'.$idAppointment);
		}
		
		if($this->input->post('xoadexuat')){
            $this->Mdanhmuc->xoaDuLieu('id',$this->input->post('xoadexuat'),'tbl_ykien');
			return redirect(base_url().'quytrinhgiaymoi/'.$idAppointment);
        }
		if($this->input->post('xoachidao')){
            $this->Mdanhmuc->xoaDuLieu('id',$this->input->post('xoachidao'),'tbl_chidao');
			return redirect(base_url().'quytrinhgiaymoi/'.$idAppointment);
        }
		
        if(!empty($idAppointment)){
            $data['getdAppointment'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
            $data['VBDi'] = $this->Mdanhmuc->layDuLieu2('iSoDen',$data['getdAppointment'][0]['iSoDen'],'sNgayNhap >',date('Y').'-01'.'-01','tbl_vanbandi');
            foreach ($data['VBDi'] as $key2 => $value2){
                $file = $this->Mvanbanchoxuly->layFileLast($value2['PK_iMaVBDi']);
                if(!empty($file))
                {
                    $data['VBDi'][$key2]['sDuongDan'] = $file['sDuongDan'];
                }else{
                    $data['VBDi'][$key2]['sDuongDan'] = '';
                }
            }
            // lay chuyen vien chu tri
            $cvtc = $this->Mgiaymoichoxuly->getCVCT($idAppointment);
            //ket qua phong thu ly
            if(!empty($cvtc)) {
                $data['resultPTL'] = $this->Mgiaymoichoxuly->getResultPTL($idAppointment, NULL, $cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPTL']);
                $data['resultPPH'] = $this->Mgiaymoichoxuly->getResultPPH($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
            }else{
                $data['resultPPH'] = $this->Mgiaymoichoxuly->getResultPPH($idAppointment,NULL,NULL);
            }
//            pr($cvtc[0]['PK_iMaCVCT']);
        }
        $data['title']    = 'Chi tiết giấy mời';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vquytrinhgiaymoi';
        $this->load->view('layout_admin/layout',$temp);

    }



}