<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clichcongtactuan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('Vanbanden/Mlichcongtac');
	}
	public function index()
	{
		$data['title']  = 'Lịch công tác tuần';
		$year           = date('Y');
		$week           = date('W');
		$start_date     = strtotime( $year . "W". $week . 1);
		$end_date       = strtotime( $year . "W". $week . 6);
		$data['thuhai'] = date('d/m/Y',$start_date);
		$data['thubay'] = date('d/m/Y',$end_date);
		$data['danhsach'] = $this->Mlichcongtac->layLichCongTacTuan(date_insert($data['thuhai']),date_insert($data['thubay']));
		if(!empty($data['danhsach']))
		{
			foreach ($data['danhsach'] as $key => $value) {
				$ten = $this->Mlichcongtac->layTenVietTat($value['PK_iMaVBDen']);
				if(!empty($ten))
				{
					$data['danhsach'][$key]['phonggiaiquyet'] = $ten['sVietTat'];
				}
				else{
					$data['danhsach'][$key]['phonggiaiquyet'] = '';
				}
			}
		}
		$data['url']	= base_url();
		$this->parser->parse('vanbanden/Vlichcongtactuan',$data);
	}

}

/* End of file Clichcongtactuan.php */
/* Location: ./application/controllers/vanbanden/Clichcongtactuan.php */