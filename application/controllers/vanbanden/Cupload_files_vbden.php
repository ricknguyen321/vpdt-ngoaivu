<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupload_files_vbden extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['content'] = ''; 
		if(_post('upload'))
		{
			 //$name = $_FILES['files']['name'];
			//pr($name);
			$data['content'] = $this->upload();
		}
		$data['title']    = 'Upload Files văn bản đến';
		$data['url']      = base_url();
		$this->parser->parse('vanbanden/Vupload_files_vbden',$data);
	}
	public function upload()
	{
		$dir  = 'doc_uploads_'.date('Y');
        $name = $_FILES['files']['name'];
        $tong = count($name);
		if(!empty($name[0]))
		{
            $file = $_FILES;
            for ($i = 0; $i < $tong; $i++) {
				#initialization new file data
				$_FILES['files']['name']     = $file['files']['name'][$i];
				$_FILES['files']['type']     = $file['files']['type'][$i];
				$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
				$_FILES['files']['error']    = $file['files']['error'][$i];
				$_FILES['files']['size']     = $file['files']['size'][$i];

				if(is_dir($dir)==false){
					mkdir($dir);		// Create directory if it does not exist
				}
				$time = time();
				$file_name = $_FILES['files']['name'];
				$tentep    = substr($file_name,0,(strlen($file_name)-4)); 
				$thongtin  = $this->Mdanhmuc->layDuLieu('iSoDen',$tentep,'tbl_vanbanden');
				if(!empty($thongtin))
				{
					foreach ($thongtin as $key => $val) {
						$data=array(
							'FK_iMaVBDen' => $val['PK_iMaVBDen'],
							'sTenFile'    => $i.$time.'_vbden_'.clear($file_name),
							'sDuongDan'   => $dir.'/'.$i.$time.'_vbden_'.clear($file_name),
							'sThoiGian'   => date('Y-m-d H:i:s'),
							'FK_iMaCB'    => $this->_session['PK_iMaCB']
							);
						$this->Mdanhmuc->themDuLieu('tbl_files_vbden',$data);
						$this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$val['PK_iMaVBDen'],'tbl_vanbanden','iFile',1);
					}
				}
				$config['upload_path']   = $dir;
				$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
				$config['overwrite']     = true;
				$config['file_name']     = $i.$time.'_vbden_'.clear($file_name);
				$this->load->library('upload');
				$this->upload->initialize($config);
				$checkResult = $this->upload->do_upload('files');
				$fileData = $this->upload->data();
            }
            return messagebox('Upload Files thành công','info');
        }
        else
        {
        	return messagebox('Bạn chưa ch�  File','danger');
        }
	}

}

/* End of file Cupload_files_vbdi.php */
/* Location: ./application/controllers/vanbanden/Cupload_files_vbdi.php */