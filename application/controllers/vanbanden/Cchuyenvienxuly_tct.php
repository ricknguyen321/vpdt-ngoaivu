<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**

 * cHUYEEN VIEN XU LY

 */

class Cchuyenvienxuly_tct extends MY_Controller {



    public function __construct()

    {

        parent::__construct();

        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');

        $this->Mdanhmuc = new Mdanhmuc();

        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();

    }

    public function index()

    {
//        pr($this->_session);
        $idAppointment = $this->input->get('vbd');
        // trình tự xử lý
        $process = $this->Mvanbanchoxuly->getDocProcess($idAppointment);
        $processcv = $this->Mvanbanchoxuly->getDocProcesscv($idAppointment);
        $processpb = $this->Mvanbanchoxuly->getDocProcessPB($idAppointment);
        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');
//        pr($process);
//        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($this->_session)
        $data['idDocGo'] = $idAppointment;
        $data['process'] = array_merge($process,$processpb,$processcv);
//        pr($data['process']);
        if(!empty($idAppointment)){
            $data['getdAppointment'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
            $data['VBDi'] = $this->Mdanhmuc->layDuLieu2('iSoDen',$data['getdAppointment'][0]['iSoDen'],'sNgayNhap >','2017-04-01','tbl_vanbandi');
            foreach ($data['VBDi'] as $key2 => $value2){
                $file = $this->Mvanbanchoxuly->layFileLast($value2['PK_iMaVBDi']);
                if(!empty($file))
                {
                    $data['VBDi'][$key2]['sDuongDan'] = $file['sDuongDan'];
                }else{
                    $data['VBDi'][$key2]['sDuongDan'] = '';
                }
            }
            // lay chuyen vien chu tri
            $cvtc = $this->Mvanbanchoxuly->getCVCT($idAppointment);
            //ket qua phong thu ly
            if(!empty($cvtc)) {
                $data['resultPTL'] = $this->Mvanbanchoxuly->getResultPTL($idAppointment, NULL, $cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPTL']);
                $data['resultPPH'] = $this->Mvanbanchoxuly->getResultPPH($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPPH']);
            }
            else{
                $data['resultPPH'] = $this->Mvanbanchoxuly->getResultPPH($idAppointment,NULL,NULL);
            }
//            pr($cvtc[0]['PK_iMaCVCT']);
        }

        // HOÀN THÀNH

        if($this->input->post('luulai')){
            if($this->input->post('testtime') > '2017-09-21'){
                $data_vbden = array(
                    'iTrangThai' => '2',
                    'sNgayGiaiQuyet' => date('Y-m-d',time()),
                    'PK_iMaCBHoanThanh' => $this->_session['PK_iMaCB'],
                    'sLuuVaoKho' => $this->input->post('luukho'),
                    'lydochammuon' => $this->input->post('chammuon')
                );
            }else{
                $data_vbden = array(
                    'iTrangThai' => '1',
                    'sNgayGiaiQuyet' => date('Y-m-d',time()),
                    'PK_iMaCBHoanThanh' => $this->_session['PK_iMaCB'],
                    'lydochammuon' => $this->input->post('chammuon')
                );
            }
            $data_kh = array(
                'active' => 2,
                'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                'khokhan_vuongmac' => $this->input->post('chammuon'),
                'ket_qua_hoanthanh'               =>  $this->input->post('ketqua')
            );
            $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$this->_session['PK_iMaCB'],$data_kh);
            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden',$data_vbden);

            //files
            $uploaf =  $this->input->post('fileketqua');
            foreach($uploaf as $k => $v){
                if(!empty($v)){
                    $data_result_kq = array(
                        'sMoTa'               =>  $this->input->post('ketqua'),
                        'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
                        'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
                        'PK_iMaVBDen'         =>  $idAppointment,
                        'active'              =>  3,
                        'sTenFiles'              =>  $v,
                        'sDuongDanFile'              =>  'doc_uploads/'.clear($_FILES["files"]["name"][$k]),
                        'date_file'           => date('Y-m-d',time())
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_file_ketqua',$data_result_kq);
                    $this->upload('doc_uploads/',clear($_FILES["files"]["name"][$k]),'files');
                }
            }
            return redirect(base_url().'chuyenvienxuly_tct?vbd='.$idAppointment);
        }

        if($this->input->post('uploadfile')){
            $uploaf =  $this->input->post('fileketqua');
            foreach($uploaf as $k => $v){
                if(!empty($v)){
                    $data_result = array(
                        'sMoTa'               =>  $this->input->post('ketqua'),
                        'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
                        'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
                        'PK_iMaVBDen'         =>  $idAppointment,
                        'active'              =>  3,
                        'sTenFiles'              =>  $v,
                        'sDuongDanFile'              =>  'doc_uploads/'.clear($_FILES["files"]["name"][$k]),
                        'date_file'           => date('Y-m-d',time())
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_file_ketqua',$data_result);
                }
            }
            $this->uploadN();
            return redirect(base_url().'chuyenvienxuly_tct?vbd='.$idAppointment);
        }
        $data['title']    = 'Chuyen vien chu tri xu ly';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vchuyenvienxuly_tct';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function uploadN()
    {
        $dir  = 'doc_uploads';
        $name = $_FILES['files']['name'];
//        pr($name);
        $tong = count($name);
        if(!empty($name[0]))
        {
            $file = $_FILES;
            for ($i = 0; $i < $tong; $i++) {
                #initialization new file data
                $_FILES['files']['name']     = $file['files']['name'][$i];
                $_FILES['files']['type']     = $file['files']['type'][$i];
                $_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
                $_FILES['files']['error']    = $file['files']['error'][$i];
                $_FILES['files']['size']     = $file['files']['size'][$i];

                if(is_dir($dir)==false){
                    mkdir($dir);		// Create directory if it does not exist
                }
                $file_name = $_FILES['files']['name'];
                $config['upload_path']   = $dir;
                $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
                $config['overwrite']     = true;
                $config['file_name']     = clear($file_name);
                $this->load->library('upload');
                $this->upload->initialize($config);
                $checkResult = $this->upload->do_upload('files');
                $fileData = $this->upload->data();
            }
            return messagebox('Upload Files thành công','info');
        }
        else
        {
            return messagebox('Bạn chưa chọn File','danger');
        }
    }


}



/* End of file Cnhapgiaymoi.php */

/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */