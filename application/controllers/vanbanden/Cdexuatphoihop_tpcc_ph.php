<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdexuatphoihop_tpcc_ph extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('Vanbanden/Mvanbanden');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->model('Vanbanden/Mvanbanphoihop');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$data['canbophong'] = $this->Mvanbanphoihop->layDS_PB($phongbanHD,$taikhoan,$quyendb,NULL);
		if(_post('guilen'))
		{
			$data['content'] = $this->guilen();
		}
		if(_post('tralai'))
		{
			$data['content'] = $this->tralai();
		}
		if(_post('tralaiketqua'))
		{
			$data['content'] = $this->tralaiketqua();
		}
		if(_post('dongy'))
		{
			$data['content'] = $this->dongy();
		}
		$vanban            = $this->DSVanBan($taikhoan);
		// pr($vanban);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Đề xuất phối hợp';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vdexuatphoihop_tpcc_ph';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function tralai()
	{
		$mavanban		= _post('tralai');
		$ykien			= _post('ykien_'.$mavanban);
		$file			= _post('file_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];
		$phongphoihop	= _post('phongphoihop_'.$mavanban);
		$chuyenvien		= $this->Mvanbanphoihop->layTK_CV($mavanban,$taikhoan,$phongphoihop);
		$data=array(
			'FK_iMaVBDen'		=> $mavanban,
			'FK_iMaNguoi_Gui'	=> $taikhoan,
			'FK_iMaNguoi_Nhan'	=> $chuyenvien['FK_iMaNguoi_Gui'],
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sNoiDung'			=> $ykien,
			'CT_PH'				=> 2,
			'iTrangThai'		=> 1,
			'FK_iMaPB_PH'		=> $phongphoihop,
			'sFile_YeuCau'		=> $file
		);
		$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
		if($kiemtrataikhoan>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_chuyennhan_phoihop',$data);
			if($id_insert>0)
			{
				$this->Mvanbanphoihop->capnhatTrangThai($mavanban,$id_insert,$phongphoihop);
				$this->Mvanbanphoihop->capnhatTrangThai_PH_TL($mavanban,$taikhoan,3);
				return messagebox('Thành công!','info');
			}
		}
	}
	public function tralaiketqua()
	{
		$mavanban		= _post('tralaiketqua');
		$ykien			= _post('ykien_'.$mavanban);
		$file			= _post('file_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];
		$nguoitralai    = $this->Mvanbanphoihop->layNguoiGui_KQ($mavanban,$taikhoan);
		$phongphoihop   = _post('phongphoihop_'.$mavanban);
		$data=array(
			'FK_iMaVBDen'		=> $mavanban,
			'FK_iMaNguoi_Gui'	=> $taikhoan,
			'FK_iMaNguoi_Nhan'	=> $nguoitralai['FK_iMaNguoi_Gui'],
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sNoiDung'			=> $ykien,
			'CT_PH'				=> 2,
			'iTrangThai'		=> 1,
			'FK_iMaPB_PH'		=> $phongphoihop,
			'sFile_YeuCau'		=> $file
		);
		$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
		if($kiemtrataikhoan>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_chuyennhan_phoihop',$data);
			if($id_insert>0)
			{
				$this->Mvanbanphoihop->capnhatTrangThai($mavanban,$id_insert,$phongphoihop);
				return messagebox('Thành phòng!','info');
			}
		}
	}
	public function dongy()
	{
		$mavanban		= _post('dongy');
		$taikhoan		= $this->_session['PK_iMaCB'];
		$ykien			= _post('ykien_'.$mavanban);
		$filetraloi     = _post('fileTraloi_'.$mavanban);
		$file     		= _post('file_'.$mavanban) ;
		$phongban		= $this->_session['FK_iMaPhongHD'];
		$phongphoihop	= _post('phongphoihop_'.$mavanban);
		$chuyenvien		= $this->Mvanbanphoihop->layTK_CV($mavanban,$taikhoan,$phongphoihop);
		$data=array(
			'FK_iMaVBDen'		=> $mavanban,
			'FK_iMaNguoi_Gui'	=> $taikhoan,
			'FK_iMaNguoi_Nhan'	=> $chuyenvien['FK_iMaNguoi_Gui'],
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sNoiDung'			=> $ykien,
			'CT_PH'				=> 2,
			'iTrangThai'		=> 1,
			'FK_iMaPB_PH'		=> $phongphoihop,
			'sFile_YeuCau'		=> $file,
			'sFile_TraLoi'		=> $filetraloi,
			'CH_TL'				=> 2
		);
		$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
		if($kiemtrataikhoan>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_chuyennhan_phoihop',$data);
			if($id_insert>0)
			{
				$this->Mvanbanphoihop->capnhatTrangThai($mavanban,$id_insert,$phongphoihop);
				return messagebox('Thành cônng!','info');
			}
		}
	}
	public function guilen()
	{
		$mavanban		= _post('guilen');
		$ykien			= _post('ykien_'.$mavanban);
		$file			= _post('file_'.$mavanban);
		$canbophong		= _post('canbophong_'.$mavanban);
		$phongphoihop   = _post('phongphoihop_'.$mavanban);
		$quyen_nguoinhan= _post('quyen_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];
		$phongban 		= $this->_session['FK_iMaPhongHD'];
		$data=array(
			'FK_iMaVBDen'		=> $mavanban,
			'FK_iMaNguoi_Gui'	=> $taikhoan,
			'FK_iMaNguoi_Nhan'	=> $canbophong,
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sNoiDung'			=> $ykien,
			'CT_PH'				=> 2,
			'iTrangThai'		=> 1,
			'FK_iMaPB_PH'		=> $phongphoihop,
			'sFile_YeuCau'		=> $file
		);
		$machuyennhan = $this->Mvanbanphoihop->layMaCN_Cuoi($mavanban,$taikhoan);
		$thongtin		= $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$mavanban,'tbl_vanbanden');
		$layLD_So		= $this->Mvanbanphoihop->layLD_So($mavanban,$phongban);
		$kehoach_data_cvph = array(
			'vanban_id'			=> $mavanban,
			'kh_noidung'		=> $thongtin[0]['sMoTa'],
			'date_nhap'			=> $thongtin[0]['sNgayNhap'],
			'vanban_skh'		=> $thongtin[0]['sKyHieu'],
			'tuan'				=> date("W"),
			'thuc_hien'			=> '2',
			'loai_kh'			=> 1,
			'chucvu'			=> $quyen_nguoinhan,
			'ngay_nhan'			=> date('Y-m-d H:i:s'),
			'ngay_han'			=> $thongtin[0]['sHanThongKe'],
			'canbo_id'			=> $canbophong,
			'lanhdao_id'		=> $taikhoan,
			'user_input'		=> $taikhoan,
			'lanhdao_so'		=> $layLD_So['PK_iMaCBChuyen'],
			'phong_id'			=> $phongban,
			'active'			=> 4,
			'ket_qua_hoanthanh'	=> '',
			'ngay_hoanthanh'	=> ''
        );
		$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
		if($kiemtrataikhoan>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_chuyennhan_phoihop',$data);
			$this->Mvanbanphoihop->xoaVBDaGiao($mavanban,$phongban,$taikhoan);
			$kiemtra_KH=$this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_cvph);
			if($id_insert>0)
			{
				$this->Mvanbanphoihop->capnhatTrangThai($mavanban,$id_insert,$phongphoihop);
				return messagebox('Thành công!','info');
			}
		}
	}
	public function DSVanBan($taikhoan) 
	{
		$config['base_url']             = base_url().'dexuatphoihop_pp';
		$config['total_rows']           = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,NULL); // 1:là chờ xử lý
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dexuatphoihop_pp');
      	}
		$data['info']       = $this->Mvanbanphoihop->layTTVB_DeXuat($taikhoan,1,NULL,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdexuatphoihop_tpcc_ph.php */
/* Location: ./application/controllers/vanbanden/Cdexuatphoihop_tpcc_ph.php */