<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Creadmaildang extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('Vanbanden/Mreadmaildang');
		$this->load->library('pagination');
	}
	public function index()
	{
		$quyen = $this->_session['iQuyenHan_DHNB'];
		$macb = $this->_session['PK_iMaCB'];
        if($quyen!=9 &&  $quyen!=3 && $macb != 705 && $macb != 730 && $macb != 731 && $macb != 700)
        {
            redirect('dangnhap');
        }
		$trangthai 	= _get('trangthai');
		$lancuoi = $this->Mdanhmuc->layLanDocMailCuoi(2);
		$lastfetch = $lancuoi[0]['time'];
		if(!isset($trangthai))
		{
			// xử lý điều kiện 15p đọc mail 1 lần
			
			$date2 = date('Y-m-d H:i:s',time());
			$newtime = strtotime ( '-15 minute' , strtotime ( $date2 ) ) ;
			$newtime = date ( 'Y-m-d H:i:s' , $newtime );			
			//echo $newtime;
			if ( $newtime < $lastfetch) {
				echo '<script type="text/javascript">alert("Hệ thống mới được cập nhật email lúc ' . $lastfetch . ' - Để tránh làm chậm hệ thống, vui lòng cập nhật email sau 15 phút kể từ thời gian này!")</script>';
			}
			else {
				$this->docMail();
				$lastfetch = $date2;
			}
		}
		if(_post('xoa'))
		{
			$ma = _post('xoa');
			$kiemtra = $this->Mdanhmuc->setDuLieu('mail_id',$ma,'tbl_mail_dang','mail_active',2);
			if($kiemtra>0)
			{
				$data['content'] = messagebox('Xóa mail thành công!','info');
			}
		}
		if(_post('xoanhieu'))
		{
			$mangxoa = _post('mangxoa');
			if(!empty($mangxoa))
			{
				$kiemtra = $this->Mdanhmuc->capnhatMangDang($mangxoa);
				if($kiemtra>0)
				{
					$data['content'] = messagebox('Xóa văn bản từ mail thành công','info');
				}
			}
			else{
				$data['content'] = messagebox('Chưa chọn văn bản để xóa','danger');
			}
		}
		$canbo                    = $this->Mdanhmuc->layDSDonVi();
        foreach ($canbo as $key => $value) {
            $data['dsdonvi'][$value['sEmail']] = $value['sTenDV'];
        }
		
		$data['lastfetch']    = $lastfetch;
		$data['loadpage']  = 1;
		$email             = $this->DSMAIL();
		$data['dsmail']    = $email['info'];
		$data['trangthai'] = $email['trangthai'];
		$data['chude']     = $email['chude'];
		$data['phantrang'] = $email['pagination'];
		$data['title']     = 'Danh sách email đảng đến';
		$temp['data']      = $data;
		$temp['template']  = 'vanbanden/Vreadmaildang';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSMAIL() 
	{	

		$trangthai 	= _get('trangthai');
		if(!isset($trangthai))
		{
			$trangthai = 0;
		}
		$chude     	= _get('chude');
		$config['base_url']             = base_url().'readmaildang?trangthai='.$trangthai.'&chude='.$chude;
		$config['total_rows']           = $this->Mreadmaildang->demMail($chude,$trangthai);
		$config['per_page']             = 20;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dsvanban');
      	}
		$data['trangthai']  = $trangthai;
		$data['chude']      = $chude;
		$data['info']       = $this->Mreadmaildang->layMail($chude,$trangthai,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}
	function filename_extension($filename) {
	    $pos = strrpos($filename, '.');
	    if($pos===false) {
	        return false;
	    } else {
	        return substr($filename, $pos+1);
	    }
	}

	function upperListEncode() { //convert mb_list_encodings() to uppercase
	    $encodes=mb_list_encodings();
	    foreach ($encodes as $encode) $tencode[]=strtoupper($encode);
	    return $tencode;
	    }

	function decode($string)
	{
	    $tabChaine=imap_mime_header_decode($string);
	    $texte='';
	    for ($i=0; $i<count($tabChaine); $i++) {
	        
	        switch (strtoupper($tabChaine[$i]->charset)) { //convert charset to uppercase
	            case 'UTF-8': $texte.= $tabChaine[$i]->text; //utf8 is ok
	                break;
	            case 'DEFAULT': $texte.= $tabChaine[$i]->text; //no convert
	                break;
	            default: if (in_array(strtoupper($tabChaine[$i]->charset),$this->upperListEncode())) //found in mb_list_encodings()
	                        {$texte.= mb_convert_encoding($tabChaine[$i]->text,'UTF-8',$tabChaine[$i]->charset);}
	                     else { //try to convert with iconv()
	                          $ret = iconv($tabChaine[$i]->charset, "UTF-8", $tabChaine[$i]->text);    
	                          if (!$ret) $texte.=$tabChaine[$i]->text;  //an error occurs (unknown charset) 
	                          else $texte.=$ret;
	                        }                    
	                break;
	            }
	        }      
	    return $texte;    
	}

	public function docMail()
	{
		set_time_limit(3000); 

		/* connect to gmail with your credentials */
		//$hostname = '{mail.hanoi.gov.vn:993/imap/ssl/novalidate-cert/notls}';
		//$username = 'doquanghoc_sotc@hanoi.gov.vn'; # e.g somebody@gmail.com
		//$password = '011868941';

		$hostname = '{mail.thudo.gov.vn:995/pop3/ssl/novalidate-cert/notls}';
		$username = 'vanthu_cusongv@hanoi.gov.vn';
		$password = 'SongvTUHN@201912';
		// $inbox = imap_open($server,$username,$password) or die('Cannot connect to Email: ');

		$maxdate = $this->Mdanhmuc->layNgayMaxDang();
		/* try to connect */
		$date = date("j F Y",strtotime($maxdate['mail_date']));
		//$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Email: ' . imap_last_error());
		$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Email: ');
		// if(!empty($maxdate))
		// {
		// 	$emails = imap_search($inbox,'UNSEEN');
		// }
		// else{
			// $emails = imap_search($inbox,'ON "'.$date.'"');
		// }
		// $emails = @array_slice(imap_search($inbox,'ALL'),0,200);
		// $emails = imap_search($inbox,'ALL');
		//$emails = imap_search($inbox,'SINCE "'.$date.'"');
		$emails = imap_search($inbox,'ALL');
		/* if any emails found, iterate through each email */
		$date_db = strtotime($maxdate['mail_date'])-3600; 
		$maxdate['mail_date'] = date('Y-m-d H:i:s',$date_db);
		$today2 = date("l");
		$today2 = strtolower($today2);
		$limit=50;
		if ($today2 == 'monday') $limit=100;
		if($emails) {
		    /* put the newest emails on top */
		    rsort($emails);//pr($emails);
		    /* for every email... */
		    foreach($emails as $key => $email_number) 
		    {
				if($key > limit) break;// chi quet so luong mail < 99
		        /* get information specific to this email */
		        $header			= imap_fetchheader($inbox,$email_number);
				$to_header		= explode("\n", imap_fetchheader($inbox,$email_number));
				$cut_header		= explode(' ',trim($to_header[5]));
				if(!empty($cut_header[1]))
				{
					$date_header	= $cut_header[1].' '.$cut_header[2].' '.$cut_header[3].' '.$cut_header[4];
				}
				else{
					$date_header	= $cut_header[2].' '.$cut_header[3].' '.$cut_header[4].' '.$cut_header[5];
				}
		        
		        $overview = imap_fetch_overview($inbox,$email_number,0);
				$arr['mail_subject'] = $this->decode($overview[0]->subject);
				$arr['mail_from'] = $this->decode($overview[0]->from);
				$arr['mail_date'] = date('Y-m-d H:i:s',strtotime($date_header));
				// echo $arr['mail_subject'].'<br/>';
		        /* get mail message */
		        // pr($date_header);
		        if($maxdate['mail_date']<$arr['mail_date']){
		        	$kiemtra = $this->Mreadmaildang->battrungMail($arr['mail_subject'],$arr['mail_from'],$arr['mail_date']);
		        	if($kiemtra==0)
		        	{
				        $message = imap_fetchbody($inbox,$email_number,2);
				        /* get mail structure */
				        $structure = imap_fetchstructure($inbox, $email_number);
				        $attachments = array();
				        /* if any attachments found... */
				        if(isset($structure->parts) && count($structure->parts)) 
				        {
				            for($i = 0; $i < count($structure->parts); $i++) 
				            {
				                $attachments[$i] = array(
				                    'is_attachment' => false,
				                    'filename' => '',
				                    'name' => '',
				                    'attachment' => ''
				                );
				            
				                if($structure->parts[$i]->ifdparameters) 
				                {
				                    foreach($structure->parts[$i]->dparameters as $object) 
				                    {
				                        if(strtolower($object->attribute) == 'filename') 
				                        {
				                            $attachments[$i]['is_attachment'] = true;
				                            $attachments[$i]['filename'] = $object->value;
				                        }
				                    }
				                }
				            
				                if($structure->parts[$i]->ifparameters) 
				                {
				                    foreach($structure->parts[$i]->parameters as $object) 
				                    {
				                        if(strtolower($object->attribute) == 'name') 
				                        {
				                            $attachments[$i]['is_attachment'] = true;
				                            $attachments[$i]['name'] = $object->value;
				                        }
				                    }
				                }
				            
				                if($attachments[$i]['is_attachment']) 
				                {
				                    $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i+1);
				                    
				                    /* 4 = QUOTED-PRINTABLE encoding */
				                    if($structure->parts[$i]->encoding == 3) 
				                    { 
				                        $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
				                    }
				                    /* 3 = BASE64 encoding */
				                    elseif($structure->parts[$i]->encoding == 4) 
				                    { 
				                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
				                    }
				                }
				            }
				        }
				        $time=time();
				        /* iterate through each attachment and save it */
						$mail_files = '';
				        foreach($attachments as $attachment)
				        {
				            if($attachment['is_attachment'] == 1)
				            {
				                $filename = iconv_mime_decode($attachment['name']);     
				                $filename = str_replace(' ','_',$filename);            
				                if(empty($filename)) $filename = $time . ".dat";		
								if(is_dir('emaildangFile_'.date('Y'))==false){
									mkdir('emaildangFile_'.date('Y'));		// Create directory if it does not exist
								}
								// download and save pdf
								//if('pdf' === $this->filename_extension( $filename) || 'PDF' === $this->filename_extension( $filename)){
									//$filename = str_replace('.sdk','.xml',$filename);
									$fp = @fopen("emaildangFile_".date('Y')."/".$key.'_'.strtotime($date_header).'_'.$filename, "w+");
									
									fwrite($fp, $attachment['attachment']);
	                				fclose($fp);
									if('pdf' === $this->filename_extension( $filename) || 'PDF' === $this->filename_extension( $filename)){
										$arr['mail_attachment1'] = $key.'_'.strtotime($date_header).'_'.$filename;
									}
									$mail_files = $mail_files.$key.'_'.strtotime($date_header).'_'.$filename.'; ';
								//}
								// download and save xml
								if('sdk' === $this->filename_extension( $filename) || 'SDK' === $this->filename_extension( $filename)){					
									$filename = str_replace('.sdk','.xml',$filename);
									$fp = @fopen("emaildangFile_".date('Y')."/".$key.'_'.strtotime($date_header).'_'.$filename, "w+");
									
									
									fwrite($fp,mb_convert_encoding($attachment['attachment'], 'UTF-8', 'UCS-2LE,UTF-16LE,ASCII,JIS,UTF-8,EUC-JP,SJIS' ));//UTF-16LE									
									//fwrite($fp,$attachment['attachment']);
									fclose($fp);
									
									$arr['mail_attachment'] = $key.'_'.strtotime($date_header).'_'.$filename;	
								}
				            }

				        }
				        if(!empty($arr['mail_attachment1']) || !empty($arr['mail_attachment']) || !empty($mail_files))
						{
							$data=array(
							'mail_subject'    => $arr['mail_subject'],
							'mail_from'       => $arr['mail_from'],
							'mail_date'       => $arr['mail_date'],
							'mail_attachment' => $arr['mail_attachment'],
							'mail_files'	  => $mail_files,
							'mail_pdf'        => $arr['mail_attachment1']
							);
							$this->Mdanhmuc->themDuLieu('tbl_mail_dang',$data);
						}
						$arr['mail_attachment'] ='';
						$arr['mail_attachment1']='';
					}
			    }
		    }    
		} 
		/* close the connection */
		imap_close($inbox);
		// cập nhật lần cuối fetch mail
		$dltg = array(
			'FK_iMaCB'       => $this->_session['PK_iMaCB'],       
			'mail_type'       => 2, 		   
			'time'   => date('Y-m-d H:i:s',time()),
		);
		$this->Mdanhmuc->themDuLieu('tbl_mail_fetch',$dltg);
	}
}

/* End of file Creadmail.php */
/* Location: ./application/controllers/vanbanden/Creadmail.php */