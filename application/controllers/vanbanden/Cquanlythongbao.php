<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 9/20/2017
 * Time: 5:15 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cquanlythongbao extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
    }
    public function index()
    {
        $idMessage = $this->uri->segment(2);
        (!empty($idMessage)) ? $data['content'] = $this->updateMessage($idMessage) : $data['content'] = $this->insertMessage();
        $data['getMessage'] = $this->Mdanhmuc->layDuLieu('iTrangThai_Xoa !=',1,'tbl_thongbao');
        if(!empty($idMessage)){
            $data['getMessageId'] = $this->Mdanhmuc->layDuLieu('PK_iMaThongBao',$idMessage,'tbl_thongbao');
        }
        if($this->input->post('xoavanban')){
            $xoa = $this->Mdanhmuc->setDuLieu('PK_iMaThongBao',$this->input->post('xoavanban'),'tbl_thongbao','iTrangThai_Xoa',1);
            return redirect(base_url().'quanlythongbao');
        }
        $data['title']    = 'Quản lý thông báo';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vquanlythongbao';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function insertMessage(){
        if($this->input->post('luu')){
            $data = array(
				'sTieuDe' => _post('tieude'),
                'sNoiDung' => nl2br(_post('noidung')),
                'sThoiGian' => date('Y-m-d H:s',time()),
                'sNgayBatDau'  => date_insert(_post('ngaybatdau')),
                'sNgayKetThuc' => date_insert(_post('ngayketthuc')),
                'iTrangThai' => _post('an'),
				'iTips' => _post('iTips')
            );
            $DocGo = $this->Mdanhmuc->themDuLieu('tbl_thongbao',$data);
            if ($DocGo > 0) {
                return messagebox('Thêm thành công', 'info');
            } else {
                return messagebox('Thêm thất bại', 'danger');
            }
        }
    }
    public function updateMessage($idMessage){
//        pr($data['getMessageId']);
        if($this->input->post('luu')){
            $data = array(
				'sTieuDe' => _post('tieude'),
                'sNoiDung' => nl2br(_post('noidung')),
                'sThoiGian' => date('Y-m-d H:s',time()),
                'sNgayBatDau'  => date_insert(_post('ngaybatdau')),
                'sNgayKetThuc' => date_insert(_post('ngayketthuc')),
                'iTrangThai' => _post('an')
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaThongBao',$idMessage,'tbl_thongbao',$data);
            if ($DocGo > 0) {
                 messagebox('Cập nhật thành công', 'info');
            } else {
                 messagebox('Cập nhật thất bại', 'danger');
            }
            return redirect(base_url().'quanlythongbao');
        }
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */