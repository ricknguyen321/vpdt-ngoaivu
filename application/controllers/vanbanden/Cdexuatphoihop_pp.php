<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdexuatphoihop_pp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('Vanbanden/Mvanbanden');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->model('Vanbanden/Mvanbanphoihop');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if($phongbanHD==12)
		{
			$quyenhan		= array(11);
			$chucvuphong	= '';
			$quyendb        = $quyendb;
		}
		elseif ($phongbanHD==11) {
			$quyenhan		= array(3);
			$chucvuphong	= array(6);
			$quyendb 		= '';
		}
		else{
			$quyenhan		= array(6);
			$chucvuphong	= '';
			$quyendb 		= '';
		}
		$data['truongphong'] = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
		if(_post('guilen'))
		{
			$data['content'] = $this->guilen();
		}
		if(_post('tralai'))
		{
			$data['content'] = $this->tralai();
		}
		$vanban            = $this->DSVanBan($taikhoan);
		// pr($vanban);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Đề xuất phối hợp';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vdexuatphoihop_pp';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function tralai()
	{
		$mavanban		= _post('tralai');
		$ykien			= _post('ykien_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];
		$phongphoihop	= _post('phongphoihop_'.$mavanban);
		$chuyenvien		= $this->Mvanbanphoihop->layTK_CV($mavanban,$taikhoan,$phongphoihop);
		$data=array(
			'FK_iMaVBDen'		=> $mavanban,
			'FK_iMaNguoi_Gui'	=> $taikhoan,
			'FK_iMaNguoi_Nhan'	=> $chuyenvien['FK_iMaNguoi_Gui'],
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sNoiDung'			=> $ykien,
			'CT_PH'				=> 1,
			'iTrangThai'		=> 1,
			'FK_iMaPB_PH'		=> $phongphoihop,
			'sFile_YeuCau'		=> ''
		);
		$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
		if($kiemtrataikhoan>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_chuyennhan_phoihop',$data);
			if($id_insert>0)
			{
				$this->Mvanbanphoihop->capnhatTrangThai($mavanban,$id_insert,$phongphoihop);
				return messagebox('Thành công!','info');
			}
		}
	}
	public function guilen()
	{
		$mavanban		= _post('guilen');
		$ykien			= _post('ykien_'.$mavanban);
		$file			= _post('file_'.$mavanban);
		$truongphong	= _post('truongphong_'.$mavanban);
		$phongphoihop   = _post('phongphoihop_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];
		$name     		= $_FILES['filetl_'.$mavanban]['name'];
		if(!empty($name))
        {
            $dir = 'files_yeucauphoihop/';
            $time= time();
            $this->upload($dir,$name,$time,$mavanban);
        }
		$data=array(
			'FK_iMaVBDen'		=> $mavanban,
			'FK_iMaNguoi_Gui'	=> $taikhoan,
			'FK_iMaNguoi_Nhan'	=> $truongphong,
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sNoiDung'			=> $ykien,
			'CT_PH'				=> 1,
			'iTrangThai'		=> 1,
			'FK_iMaPB_PH'		=> $phongphoihop,
			'sFile_YeuCau'		=> ($name)?($dir.$time.'_'.clear($name)):$file
		);
		
		$kiemtrataikhoan = $this->Mvanbanphoihop->kiemtraTaiKhoan($mavanban,$taikhoan);
		if($kiemtrataikhoan>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_chuyennhan_phoihop',$data);
			if($id_insert>0)
			{
				$this->Mvanbanphoihop->capnhatTrangThai($mavanban,$id_insert,$phongphoihop);
				return messagebox('Đề xuất đã gửi trưởng phòng!','info');
			}
		}
	}
	public function upload($dir,$name,$time,$mavanban)
    {
        if(is_dir($dir)==false){
            mkdir($dir);        // Create directory if it does not exist
        }
        $config['upload_path']   = $dir;
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
        $config['overwrite']     = true;
        $config['file_name']     = $time.'_'.clear($name);
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('filetl_'.$mavanban);
        $this->upload->data();
    }
	public function DSVanBan($taikhoan) 
	{
		$config['base_url']             = base_url().'dexuatphoihop_pp';
		$config['total_rows']           = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,NULL); // 1:là chờ xử lý
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dexuatphoihop_pp');
      	}
		$data['info']       = $this->Mvanbanphoihop->layTTVB_DeXuat($taikhoan,1,NULL,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdexuatphoihop_pp.php */
/* Location: ./application/controllers/vanbanden/Cdexuatphoihop_pp.php */