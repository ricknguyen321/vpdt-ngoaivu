<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 6/27/2017
 * Time: 10:50 AM
 */
class Ccongtacdang_pp extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->library('pagination');
    }
    public function index()
    {
        // danh sách phó phòng
        $data['getAccountDeputyDirector'] = $this->Mvanbanchoxuly->getAccount('0',7,$this->_session['FK_iMaPhongHD']);
//        pr($data['getAccountDeputyDirector']);
        // danh sách chuyên viên
        $data['getDepartment'] = $this->Mvanbanchoxuly->getAccount('0',8,$this->_session['FK_iMaPhongHD']);
        //danh sách chờ xử lý
        if($this->input->post('tuchoitp')){
            $data['content'] = $this->Rejecttp();
        }

        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['getDocAwait']    = $page['info'];
        if(!empty($data['getDocAwait'])){
            foreach ($data['getDocAwait'] as $key => $value) {
                $process = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen'],NULL,NULL,'8','1');
                $processpp = $this->Mvanbanden->getDocProcess($value['PK_iMaVBDen'],NULL,NULL,'7');
//                pr($process);
                if(!empty($process)){
                    $data['getDocAwait'][$key]['PK_iMaCVCT'] = $process[0]['PK_iMaCVCT'];
                    $data['getDocAwait'][$key]['MoTaCVCT'] = $process[0]['sMoTa'];
                    $data['getDocAwait'][$key]['PK_iMaCVPH'] = $process[0]['PK_iMaCVPH'];
                    $data['getDocAwait'][$key]['PK_iMaCNCT'] = $process[0]['PK_iMaCN'];
                }
                if(empty($process)){
                    $data['getDocAwait'][$key]['PK_iMaCVCT'] = '';
                    $data['getDocAwait'][$key]['MoTaCVCT'] = '';
                }
                if(!empty($processpp)){
                    $data['getDocAwait'][$key]['PK_iMaPP'] = $processpp[0]['PK_iMaCVCT'];
                    $data['getDocAwait'][$key]['MoTaPP'] = $processpp[0]['sMoTa'];
                    $data['getDocAwait'][$key]['PK_iMaCNPP'] = $processpp[0]['PK_iMaCN'];
                    $data['getDocAwait'][$key]['sHanChoPP'] = $processpp[0]['sThoiGianHetHan'];
                }
                if(empty($processpp)){
                    $data['getDocAwait'][$key]['PK_iMaPP'] = '';
                    $data['getDocAwait'][$key]['MoTaPP'] = '';
                }
                if($this->_session['FK_iMaPhongHD'] == 11){
                    $idTP = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }elseif($this->_session['iQuyenHan_DHNB'] == 7 && $this->_session['FK_iMaPhongHD'] != 12){
                    $idTP = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }elseif($this->_session['FK_iMaPhongHD'] == 12){
                    $idTP = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
//                    pr($idTP);
                }
                if(!empty($idTP)){
                    $data['getDocAwait'][$key]['ChiDao'] = $idTP;
                }
//                pr($arrayCBNhan);
            }
//            pr($data['getDocAwait']);
        }
//        pr($data['getDocAwait'] );
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];

        // duyệt văn bản chờ xử lý
//        $this->insertDocAwait();
        if($this->input->post('duyet')) {
            $this->insertDocAwait();
        }
        $data['title']    = 'Van ban den cua pho phong';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vcongtacdang_pp';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function Rejecttp(){
        $idDoc = $this->input->post('maphongbantp');
        $data_reject = array(
            'PK_iMaVBDen' => $idDoc,
            'PK_iMaCBTuChoi' => $this->_session['PK_iMaCB'],
            'sTenCB'   => $this->_session['sHoTen'],
            'sLyDo'    => $this->input->post('noidungtuchoi'),
			'PK_iMaPB' => $this->_session['FK_iMaPhongHD'],
            'sGhiChu'  => $this->input->post('ghichu'),
            'iTrangThai' => '1',
            'sThoiGianTao' => date('Y-m-d H:s',time())
        );
        $data_doc = array(
            'iTrangThai_TruyenNhan' => '5',
            'iTrangThai' => '0'
        );
//        pr($data_doc);
        $this->Mdanhmuc->themDuLieu('tbl_vanban_chuyenlai',$data_reject);
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_doc);
        if ($DocGo > 0) {
            return messagebox('Chuyền văn bản về trưởng phòng thành công', 'info');
        } else {
            return messagebox('Chuyền văn bản về trưởng phòng thất bại', 'danger');
        }
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        $ma = $this->input->post('duyet');
        $process = $this->Mvanbanchoxuly->laylanhdaosaucung($ma);
//        if($this->input->post('duyetvanban')){
//            foreach ($this->input->post('duyet') as $ma => $value5){
//                if(!empty($value5)){
        // chuyen vien chu tri
        $staff = $this->input->post('phongchutri');
        //mang phoi hop
        $DepartmentPH = $this->input->post('mangphoihop');
        $Doc_array= array();
        if (!empty($staff[$ma])) {
            $Doc_array['iTrangThai_TruyenNhan'] = '7';
            $Doc_array['PK_iMaCBDuyet'] = $this->_session['PK_iMaCB'];
        }
        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$this->input->post('doc_id')[$ma],'tbl_vanbanden',$Doc_array);
        if(!empty($staff[$ma])) {
            $this->Mvanbanchoxuly->xoaDuLieu3Where('PK_iMaVBDen',$ma,'PK_iMaPhong', $this->_session['FK_iMaPhongHD'],'iTrangThai_PPPH',0,'tbl_phongphoihop');
            /** @var mang chuyen chuyen vien $Departmanet_array */
            // van ban phoi hop giua cac chuyen vien vowi nhau
            $Departmanet_array= array();
            $mangph = $this->input->post('mangphoihop')[$ma];
            $cvphoihop = explode( ',', $mangph );
//                        pr($cvphoihop);
            if(!empty($DepartmentPH[$ma])){
                foreach($cvphoihop as $key => $value){
                    $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                    $Departmanet_array['PK_iMaCB'] = $value;
                    $Departmanet_array['PK_iMaCVCT'] = $staff[$ma];
                    $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                    $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
                    $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                    $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                    $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                    $Departmanet_array['PK_iMaPhong'] = $this->_session['FK_iMaPhongHD'];
                    $this->Mdanhmuc->themDuLieu('tbl_phongphoihop',$Departmanet_array);
                }
//                            pr($Departmanet_array);
            }
            // chuyen vien chu tri ko phoi hop
            $Departmanet_array1= array();
            $cvct = $this->input->post('cvct');
            if(!empty($cvct[$ma])){
                $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                $Departmanet_array1['PK_iMaCVCT'] = $staff[$ma];
                $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                $Departmanet_array1['sThoiGian'] = date('Y-m-d H:s',time());
                $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                $Departmanet_array1['sThoiGianHetHan'] = (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]);
                $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                $Departmanet_array1['CapGiaiQuyet'] = '8';
                $Departmanet_array1['PK_iMaCVPH'] = $mangph;
                $this->Mvanbanchoxuly->updateCBCT($ma,$cvct[$ma],$Departmanet_array1);
            }elseif(empty($cvct[$ma])){
                $Departmanet_array1['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                $Departmanet_array1['PK_iMaCVCT'] = $staff[$ma];
                $Departmanet_array1['PK_iMaVBDen'] = $this->input->post('doc_id')[$ma];
                $Departmanet_array1['sThoiGian'] = date('Y-m-d H:s',time());
                $Departmanet_array1['sMoTa'] = $this->input->post('chidaophongchutri')[$ma];
                $Departmanet_array1['sThoiGianHetHan'] = date_insert(_post('hangiaiquyet')[$ma]);
                $Departmanet_array1['input_per'] = $this->_session['PK_iMaCB'];
                $Departmanet_array1['PK_iMaCVPH'] = $mangph;
                $Departmanet_array1['CapGiaiQuyet'] = '8';
                $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array1);
            }

            //kế hoạch công tác
            $kehoach_data_cv = array(
                'vanban_id' => $this->input->post('doc_id')[$ma],
                'kh_noidung' => $this->input->post('noidungvb')[$ma],
                'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                'vanban_skh' => $this->input->post('sohieu')[$ma],
                'tuan' => date("W"),
                'ngay_nhan' => date('Y-m-d H:i:s',time()),
                'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                'canbo_id' => $staff[$ma],
                'thuc_hien' => '3',
                'loai_kh' => 1,
                'lanhdao_id' => $this->_session['PK_iMaCB'],
                'user_input' => $this->_session['PK_iMaCB'],
                'lanhdao_so' => $process['PK_iMaCB'],
                'phong_id' => $this->_session['FK_iMaPhongHD']
            );
//                        pr($kehoach_data);
            $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);

            //phó phối hợp
            if(!empty($DepartmentPH[$ma])){
                $mangph = $this->input->post('mangphoihop')[$ma];
                $cvphoihop = explode( ',', $mangph );
                foreach($cvphoihop as $key => $value){
                    $kehoach_data_cvph = array(
                        'vanban_id' => $this->input->post('doc_id')[$ma],
                        'kh_noidung' => $this->input->post('noidungvb')[$ma],
                        'date_nhap' => $this->input->post('ngaynhapvb')[$ma],
                        'vanban_skh' => $this->input->post('sohieu')[$ma],
                        'tuan' => date("W"),
                        'thuc_hien' => '2',
                        'ngay_nhan' => date('Y-m-d H:i:s',time()),
                        'ngay_han' => (_post('hangiaiquyet')[$ma] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$ma]),
                        'canbo_id' => $value,
                        'loai_kh' => 1,
                        'lanhdao_id' => $this->_session['PK_iMaCB'],
                        'user_input' => $this->_session['PK_iMaCB'],
                        'lanhdao_so' => $process['PK_iMaCB'],
                        'phong_id' => $this->_session['FK_iMaPhongHD']
                    );

                    $this->Mdanhmuc->themDuLieu('kehoach', $kehoach_data_cvph);
                }
            }
            return redirect(base_url().'congtacdang_pp');

        }
    }
    public function PhanTrang()
    {
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'congtacdang_pp?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = count($this->Mvanbanchoxuly->getDocAwaitCTD('6',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,NULL,NULL,NULL,NULL));
//        pr($config['total_rows']);
        $config['per_page']             = 200;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'congtacdang_pp');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mvanbanchoxuly->getDocAwaitCTD('6',NULL,NULL,$this->_session['PK_iMaCB'],$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page'],NULL,NULL);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}