<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**

 * Created by PhpStorm.

 * User: MinhDuy

 * Date: 6/14/2017

 * Time: 10:02 AM

 * cHUYEEN VIEN XU LY

 */

class Cchuyenvienxuly extends MY_Controller {



    public function __construct()

    {

        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();

    }

    public function index()

    {
		
        $idAppointment = $this->input->get('vbd');

        // trình tự xử lý

        $process = $this->Mvanbanchoxuly->getDocProcess($idAppointment);

        $processcv = $this->Mvanbanchoxuly->getDocProcesscv($idAppointment);

        $processpb = $this->Mvanbanchoxuly->getDocProcessPB($idAppointment);

        $data['Filedinhkem']  = layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_files_vbden');

        $data['idDocGo'] = $idAppointment;

        $data['process'] = array_merge($process,$processpb,$processcv);
		
		$data['chidao'] = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_chidao');
		
		$data['dexuat'] = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$idAppointment,'tbl_ykien');


        if(!empty($idAppointment)){
            $data['getdAppointment'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden');
            $data['CBChiDao'] = $this->Mdanhmuc->layDuLieu2('PK_iMaVBDen',$idAppointment,'PK_iMaCB',$this->_session['PK_iMaCB'],'tbl_phongphoihop');
            // lay chuyen vien chu tri
            $cvtc = $this->Mvanbanchoxuly->getCVCT($idAppointment);
//            pr($cvtc[0]['PK_iMaCVCT']);
            //ket qua phong thu ly
            if(!empty($cvtc)) {
                $data['resultPTL'] = $this->Mvanbanchoxuly->getResultPTL($idAppointment, NULL, $cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPTL']);
                $data['resultPPH'] = $this->Mvanbanchoxuly->getResultPPH($idAppointment,NULL,$cvtc[0]['PK_iMaPhongCT']);
//                pr($data['resultPPH']);
            }else{
                $data['resultPPH'] = $this->Mvanbanchoxuly->getResultPPH($idAppointment,NULL,NULL);
            }
            $process2 =  $this->Mvanbanchoxuly->getDocProcessCVPPH($idAppointment,$this->_session['FK_iMaPhongHD'], NULL);
            $data['TrinhTuPhoiHop'] = $process2;

        }

        // HOÀN THÀNH

        if($this->input->post('luulai')){

            $data_result = array(
                'sMoTa'               =>  nl2br($this->input->post('ketqua')),
                'PK_iMaPB'            =>  $this->_session['FK_iMaPhongHD'],
                'PK_iMaCB'            =>  $this->_session['PK_iMaCB'],
                'PK_iMaVBDen'         =>  $idAppointment,
                'active'              =>  3,
                'date_file'           => date('Y-m-d H:i:s',time())
            );
//            pr($data_result);

            $this->Mdanhmuc->themDuLieu('tbl_file_ketqua',$data_result);
//            $this->Mdanhmuc->setDuLieu('PK_iMaVBDen',$idAppointment,'tbl_vanbanden','iTrangThai','1');
            $data_status = array(
                'iTrangThai' =>  '3',
                'sThoiGianHoanThanh' => date('Y-m-d')
            );
            $this->Mvanbanchoxuly->updatePPH($idAppointment,$this->_session['PK_iMaCB'],$data_status);
            $data_kh = array(
                'active' => 3,
				'ngay_hoanthanh' => date('Y-m-d H:i:s',time()),
                'ket_qua_hoanthanh'               =>  nl2br($this->input->post('ketqua'))
            );
            $layidlanhdao = $this->Mdanhmuc->get_kehoach3('canbo_id="'.$this->_session['PK_iMaCB'].'" and vanban_id="'.$idAppointment.'"','kehoach');
            $lanhdaocapnhat=$layidlanhdao[0]['lanhdao_id'];
            $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$this->_session['PK_iMaCB'],$data_kh);
            $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idAppointment,'tuan', date("W"),'canbo_id',$lanhdaocapnhat,$data_kh);
            //return redirect(base_url().'chuyenvienxuly?vbd='.$idAppointment);
            return redirect(base_url().'vanbanchoxuly_cvph');

        }
		
        $data['title']    = 'Phối hợp xử lý văn bản';

        $temp['data']     = $data;

        $temp['template'] = 'vanbanden/Vchuyenvienxuly_vb';

        $this->load->view('layout_admin/layout',$temp);

    }

}



/* End of file Cnhapgiaymoi.php */

/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */