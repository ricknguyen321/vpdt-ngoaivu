<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 10/20/2017
 * Time: 9:18 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsdexuatgiahan extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->library('pagination');
        $this->load->model('Vanbanden/Mvanbanphoihop');
    }
    public function index()
    {       
        if($this->input->post('duyethan')){
            $data['content'] = $this->checkDateline();
        }
		if($this->input->post('tuchoihangd')){
            $data['content'] = $this->tuchoihangd();
        }
        if($this->input->post('duyethancb')){
            $data['content'] = $this->insertDateline();
        }
        if($this->input->post('duyethanpp')){
            $data['content'] = $this->insertDatelineTP();
        }
        if($this->input->post('tuchoi')){
            $data['content'] = $this->tuchoihan();
        }
        /** @var danh sách tìm kiếm phân trang $page */
        $page                  = $this->PhanTrang();
        $data['getDocGo']      = $page['info'];
        if(!empty($data['getDocGo'])){
            foreach ($data['getDocGo'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $processgd = $this->Mvanbanchoxuly->getDocProcess($value['PK_iMaVBDen']);
                $processpb1 = $this->Mvanbanchoxuly->getDocProcessPB($value['PK_iMaVBDen']);
                $process = $this->Mvanbanchoxuly->laylanhdaosaucung($value['PK_iMaVBDen']);
                $data['getDocGo'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                if(!empty($processgd)){
                    $data['getDocGo'][$key]['sChuTri'] = $processgd[0]['sMoTa'];
                }
                if(empty($processgd)){
                    $data['getDocGo'][$key]['sChuTri'] = '';
                }
                if(!empty($processpb1)){
                    $data['getDocGo'][$key]['sPhongChuTri'] = $processpb1[0]['sHoTen'];
                }
                if(empty($processpb1)){
                    $data['getDocGo'][$key]['sPhongChuTri'] = '';
                }
                $data['getDocGo'][$key]['ChiDao'] = $process['sHoTen'];
                $data['getDocGo'][$key]['PK_iMaLDCD'] = $process['PK_iMaCB'];
                //phó phòng hiể thị tp
                if($this->_session['FK_iMaPhongHD'] == 11){
                    $idTP = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCBVP($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }elseif($this->_session['iQuyenHan_DHNB'] == 7 && $this->_session['FK_iMaPhongHD'] != 12){
                    $idTP = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCB($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }elseif($this->_session['FK_iMaPhongHD'] == 12){
                    $idTP = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['sHoTen'];
                    $idTP1 = $this->Mvanbanchoxuly->getidCCCB($this->_session['FK_iMaPhongHD'])[0]['PK_iMaCB'];
//                    pr($idTP);
                }
                if(!empty($idTP)){
                    $data['getDocGo'][$key]['ChiDaoPP'] = $idTP;
                    $data['getDocGo'][$key]['PK_iMaLDCDPP'] = $idTP1;
                }
            }
        }
//         pr($data['getDocGo']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']    = $page['loaivanban'];
        $data['sokyhieu']      = $page['sokyhieu'];
        $data['ngaynhap']      = $page['ngaynhap'];
        $data['donvi']         = $page['donvi'];
        $data['ngayky']        = $page['ngayky'];
        $data['ngayden']       = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['ngaymoi']       = $page['ngaymoi'];
        $data['nguoiky']       = $page['nguoiky'];
        $data['soden']         = $page['soden'];
        $data['denngay']       = $page['denngay'];
        $data['chucvu']        = $page['chucvu'];
        $data['nguoinhap']     = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count']         = $page['count'];


        $data['title']    = 'Danh sách đề xuất gia hạn';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vdsdexuatgiahan';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function PhanTrang()
    {
//        pr($this->_session);
//        pr($this->input->get('id'));
        $chuyenvien = NULL;
        $lanhdao = $this->_session['PK_iMaCB'];
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu      = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi         = $this->input->get('donvi');
        $ngayky        = $this->input->get('ngayky');
        $ngayden       = $this->input->get('ngayden');
        $trichyeu      = $this->input->get('trichyeu');
        $ngaymoi       = $this->input->get('ngaymoi');
        $nguoiky       = $this->input->get('nguoiky');
        $soden         = $this->input->get('soden');
        $denngay       = $this->input->get('denngay');
        $chucvu        = $this->input->get('chucvu');
        $nguoinhap     = $this->input->get('nguoinhap');
        $cacloaivanban = $this->input->get('cacloaivanban');
        $id = $this->input->get('id');
        $idDocument    = $this->uri->segment(2);
        $config['base_url']             = base_url().'dsdexuatgiahan?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&ngaymoi='.$ngaymoi.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap.'&cacloaivanban='.$cacloaivanban.'&id='.$id;
        $config['total_rows']           = count($this->Mvanbanden->dsGiaHan($lanhdao,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,NULL,NULL,$chuyenvien));
//        pr($config['total_rows']);
        $config['per_page']             = 25;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'dsdexuatgiahan');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap']      = $ngaynhap;
        $data['donvi']         = $donvi;
        $data['ngayky']        = $ngayky;
        $data['ngayden']       = $ngayden;
        $data['trichyeu']      = $trichyeu;
        $data['ngaymoi']       = $ngaymoi;
        $data['nguoiky']       = $nguoiky;
        $data['soden']         = $soden;
        $data['denngay']       = $denngay;
        $data['chucvu']        = $chucvu;
        $data['nguoinhap']     = $nguoinhap;
        $data['cacloaivanban'] = $cacloaivanban;
        $data['id'] = $id;
        $data['count']         = $config['total_rows'];
        $data['info']        = $this->Mvanbanden->dsGiaHan($lanhdao,NULL,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$ngaymoi,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$cacloaivanban,$config['per_page'], $data['page'],$chuyenvien);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }
    public function checkDateline(){
        $idDoc = $this->input->post('duyethan');
        //check giam đốc gia hạn
        $arr = $this->Mvanbanden->layGiaHanGiamDoc($this->input->post('duyethan'));
        if(count($arr)>0 && $this->_session['iQuyenHan_DHNB'] == 5){
            $data_giamdoc = array('iLanhDao' => 673);
            $DocGo1 = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_giamdoc);
            if ($DocGo1 > 0) {
				 return redirect(base_url().'dsdexuatgiahan');
                return messagebox('Chuyển duyệt hạn lên giám đốc thành công', 'info');
            } else {
                return messagebox('Chuyển duyệt hạn lên giám đốc thất bại', 'danger');
            }
        } else {
            $data_hanmoi = array(
				'ngay_han' => date_insert($this->input->post('handexuat')[$idDoc])
            );
            $this->Mvanbanchoxuly->capnhatkehoach('vanban_id',$idDoc,'tuan >=', date("W"),NULL,NULL,$data_hanmoi);
            $data_han = array(
                'iTrangThai_ThemHan'   => 2,
                'sHanThongKe'    => date_insert($this->input->post('handexuat')[$idDoc])
            );
            $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
			
			$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 2,
				'FK_iMaCB_Duyet'   => $this->_session['PK_iMaCB'],
				'sHanThongKe'    => date_insert($this->input->post('handexuat')[$idDoc]),
				'ngayduyet'   => date('Y-m-d H:i:s',time())
            );
			$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
			
            if ($DocGo > 0) {
				$chidaold = array(
					'FK_iMaVBDen'        => $idDoc,
					'sNoiDung'        => nl2br($this->input->post('chidaoduyet')[$idDoc]),
					'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
					'sThoiGian'   => date('Y-m-d H:i:s',time()),
				);
				if ($this->input->post('chidaoduyet')[$idDoc] != '' && $this->input->post('chidaoduyet')[$idDoc] != 'Đồng ý gia hạn giải quyết.' && $this->input->post('chidaoduyet')[$idDoc] != 'Đồng ý' && $this->input->post('chidaoduyet')[$idDoc] != 'Đồng ý.' && $this->input->post('chidaoduyet')[$idDoc] != 'Duyệt' && $this->input->post('chidaoduyet')[$idDoc] != 'Duyệt.' && $this->input->post('chidaoduyet')[$idDoc] != 'Đồng ý gia hạn.' && $this->input->post('chidaoduyet')[$idDoc] != 'Đồng ý gia hạn' && $this->input->post('chidaoduyet')[$idDoc] != 'đồng ý' && $this->input->post('chidaoduyet')[$idDoc] != 'đồng ý.') {
					$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
				}
				 return redirect(base_url().'dsdexuatgiahan');
                return messagebox('Duyệt thành công', 'info');
            } else {
                return messagebox('Duyệt thất bại', 'danger');
            }
        }	
    }
	public function tuchoihangd(){			
		$idDoc = $this->input->post('tuchoihangd');
        $hoten         = $this->_session['sHoTen'];
		$time = date('d/m H:i', time());
        //$thoigian      = date('d/m/Y H:i:s');
        $lydo          = nl2br($this->input->post('chidaoduyet')[$idDoc]);
        $noidungtuchoi = "$lydo - <b>$hoten</b> - <i>($time)</i>";
        $data_han = array(
            'iTrangThai_ThemHan'   => 3,
            'sLyDoTuChoiHan'    => $noidungtuchoi
        );
		
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
		
		$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 3,
				'FK_iMaCB_Duyet'   => $this->_session['PK_iMaCB'],
				'noidungtuchoi'    => $noidungtuchoi,
				'ngayduyet'   => date('Y-m-d H:i:s',time())
            );
		$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
		if ($this->_session['iQuyenHan_DHNB'] == 4 || $this->_session['iQuyenHan_DHNB'] == 5) {
			$chidaold = array(
						'FK_iMaVBDen'        => $idDoc,
						'sNoiDung'        => $noidungtuchoi,
						'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
						'sThoiGian'   => date('Y-m-d H:i:s',time()),
					);
			if ($lydo != '') {
				$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
			}
		}
        return redirect(base_url().'dsdexuatgiahan');
        
    }
    public function insertDateline(){
        $idDoc = $this->input->post('duyethancb');
        $idLD = $this->input->post('idlanhdao');
		$time = date('d/m H:i', time());
		$noidungdexuat = nl2br($this->input->post('noidungdexuat')[$idDoc]);
        $data_han = array(
            'iLanhDao' => $idLD[$idDoc],
            'iTrangThai_ThemHan'   => 1,
            'sHanChoDuyet'    => date_insert($this->input->post('handexuat')[$idDoc]),
            'sNoiDungGiaHan'    => $this->input->post('gheplydo')[$idDoc].'<p><span>'.$noidungdexuat.'</span>'.' - <b>'.$this->_session['sHoTen'].'</b> - '.$time.'</p>'
        );
//        pr($data_han);
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
		
		$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 1,
				'FK_iMaCB_Gui'   => $this->_session['PK_iMaCB'],
				'sHanThongKe' => date_insert($this->input->post('handexuat')[$idDoc]),
				'noidunggiahan'    => $noidungdexuat,
				'ngaygiahan'   => date('Y-m-d H:i:s',time())
            );
		$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
		
        return redirect(base_url().'dsdexuatgiahan');
    }
    // đề xuất hạn lên trưởng pòng
    public function insertDatelineTP(){
        $phongbanHD = $this->_session['FK_iMaPhongHD'];
        $taikhoan   = $this->_session['PK_iMaCB'];
        $idDoc      = $this->input->post('duyethanpp');
        $idLD       = $this->input->post('idlanhdao');
		$time = date('d/m H:i', time());
        if($phongbanHD==12)
        {
            $nguoichuyen = $this->Mvanbanphoihop->laynguoiduyet($taikhoan,$idDoc);
            if(!empty($nguoichuyen))
            {
                $nguoinhan = $nguoichuyen['FK_iMaCB_Chuyen'];
            }
            else{
                $nguoinhan = $idLD[$idDoc];
            }
        }
        else{
            $nguoinhan = $idLD[$idDoc];
        }
		
		$noidungdexuattp = nl2br($this->input->post('noidungdexuattp')[$idDoc]);
        $data_han = array(
            'iLanhDao' => $nguoinhan,
            'iTrangThai_ThemHan'   => 1,
            'sHanChoDuyet'    => date_insert($this->input->post('handexuat')[$idDoc]),
			'sNoiDungGiaHan'    => $this->input->post('gheplydo')[$idDoc].'<p><span>'.$noidungdexuat.'</span>'.' - <b>'.$this->_session['sHoTen'].'</b> - '.$time.'</p>'
        );
//        pr($data_han);
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
		
		$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 1,
				'FK_iMaCB_Gui'   => $this->_session['PK_iMaCB'],
				'sHanThongKe' => date_insert($this->input->post('handexuat')[$idDoc]),
				'noidunggiahan'    => $noidungdexuattp,
				'ngaygiahan'   => date('Y-m-d H:i:s',time())				
            );
		$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
		
        return redirect(base_url().'dsdexuatgiahan');
    }
    //
    public function tuchoihan_cu(){
        $idDoc = $this->input->post('mavbd');
        $data_han = array(
            'iTrangThai_ThemHan'   => 3,
            'sLyDoTuChoiHan'    => $this->input->post('lydotuchoi')
        );
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
        return redirect(base_url().'dsdexuatgiahan');
    }
    public function tuchoihan(){
        $idDoc         = $this->input->post('mavbd');
        $hoten         = $this->_session['sHoTen'];
		$time = date('d/m H:i', time());
        //$thoigian      = date('d/m/Y H:i:s');
        $lydo          = nl2br(_post('lydotuchoi'));
        $noidungtuchoi = "$lydo - <b>$hoten</b> - <i>($time)</i>";
        $data_han = array(
            'iTrangThai_ThemHan'   => 3,
            'sLyDoTuChoiHan'    => $noidungtuchoi
        );
		
        $DocGo = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$idDoc,'tbl_vanbanden',$data_han);
		
		$data_han2 = array(
				'FK_iMaVBDen' 		=> $idDoc,
                'iTrangThai_ThemHan'   => 3,
				'FK_iMaCB_Duyet'   => $this->_session['PK_iMaCB'],
				'noidungtuchoi'    => $noidungtuchoi,
				'ngayduyet'   => date('Y-m-d H:i:s',time())
            );
		$this->Mdanhmuc->themDuLieu('tbl_luuvet_giahan',$data_han2);
		if ($this->_session['iQuyenHan_DHNB'] == 4 || $this->_session['iQuyenHan_DHNB'] == 5) {
			$chidaold = array(
						'FK_iMaVBDen'        => $idDoc,
						'sNoiDung'        => $noidungtuchoi,
						'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
						'sThoiGian'   => date('Y-m-d H:i:s',time()),
					);
			if (_post('lydotuchoi') != '') {
				$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
			}
		}
        return redirect(base_url().'dsdexuatgiahan');
    }

}

/* End of file Cdsvanbanden.php */
/* Location: ./application/controllers/vanban/Cdsvanbanden.php */