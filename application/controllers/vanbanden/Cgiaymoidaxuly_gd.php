<?php
/**
 * Created by PhpStorm.
 * User: Minh Duy
 * Date: 7/12/2017
 * Time: 10:53 AM
 */
class Cgiaymoidaxuly_gd extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('Vanbanden/Mgiaymoi','Mgiaymoi');
        $this->Mgiaymoi = new Mgiaymoi();
        $this->load->library('pagination');
    }
    public function index()
    {
        // danh sách giám đốc
        $data['getAccountDirec'] = $this->Mgiaymoichoxuly->getAccount('0','4');
        // danh sách phó giám đốc
        $data['getAccountDeputyDirector'] = $this->Mgiaymoichoxuly->getAccount('0','5');
        // danh sách phòng
        $data['getDepartment'] = $this->Mgiaymoichoxuly->getDepartment('0');
        // văn bản đã phân loại
        /** @var danh sách tìm kiếm phân trang $page */
        $page               = $this->PhanTrang();
        $data['Docawaitdir']    = $page['info'];
//        pr($page['info']);
        if(!empty($data['Docawaitdir'])){
            foreach ($data['Docawaitdir'] as $key => $value) {
                $duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                $process = $this->Mgiaymoi->getDocProcess($value['PK_iMaVBDen']);
//                pr($process);
                if(!empty($duongdan))
                {
                    $data['Docawaitdir'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }
                if(empty($duongdan)){
                    $data['Docawaitdir'][$key]['sDuongDan'] = '';
                }
                $arrayCBNhan = array();
                $arrayMoTa = array();
                $arrayMangPH = array();
                $arrayCT = array();
                $hanxuly = array();
                $arrayCN = array();
                $arrayCN1 = array();
                foreach ($process as $key1 => $value1) {
                    $ktlv = $this->Mgiaymoichoxuly->TestCB($value1['PK_iMaCBNhan']);
                    $testlv = $ktlv['iQuyenHan_DHNB'];
                    if ($value1['PK_iMaCBNhan'] != 0) {
                        $arrayCBNhan[$testlv] = $value1['PK_iMaCBNhan'];
                        $arrayMoTa[$testlv] = $value1['sMoTa'];
//                        $arrayCT[6] = $value1['PK_iMaPhongCT'];
                        $arrayMangPH[7] = $value1['PK_iMaPhongPH'];
                        $arrayCN1[$testlv] = $value1['PK_iMaCN'];
                        $hanxuly[$testlv] = $value1['sThoiGianHetHan'];
                        $data['Docawaitdir'][$key]['PK_iMaCN1'] = $arrayCN1;

                    } else {
//                        $arrayMoTa[6] = $value1['sMoTa'];
                        $arrayMangPH[7] = $value1['PK_iMaPhongPH'];
//                        $arrayCT[6] = $value1['PK_iMaPhongCT'];
                    }
                    if (!empty($value1['PK_iMaPhongCT'] != 0)) {
                        $arrayCT[6] = $value1['PK_iMaPhongCT'];
                        $arrayMoTa[6] = $value1['sMoTa'];
                        $arrayCN[6] = $value1['PK_iMaCN'];
                        $hanxuly[6] = $value1['sThoiGianHetHan'];
                        $data['Docawaitdir'][$key]['PK_iMaCN'] = $arrayCN;
                    }
                }
                $data['Docawaitdir'][$key]['PK_iMaCBNhan'] = $arrayCBNhan;
                $data['Docawaitdir'][$key]['PK_iMaPhongCT'] = $arrayCT;
                $data['Docawaitdir'][$key]['ChuThich'] = $arrayMoTa;
                $data['Docawaitdir'][$key]['PK_iMaPhongPH'] = $arrayMangPH;
                // $data['Docawaitdir'][$key]['PK_iMaCN'] = $arrayCN;
                $data['Docawaitdir'][$key]['sThoiGianHetHan'] = $hanxuly;
//                pr($arrayCBNhan);
            }

        }
//        pr($data['Docawaitdir']);
        $data['phantrang']     = $page['pagination'];
        $data['loaivanban']      = $page['loaivanban'];
        $data['sokyhieu']    = $page['sokyhieu'];
        $data['ngaynhap']       = $page['ngaynhap'];
        $data['donvi']        = $page['donvi'];
        $data['ngayky']          = $page['ngayky'];
        $data['ngayden']      = $page['ngayden'];
        $data['trichyeu']      = $page['trichyeu'];
        $data['nguoiky']   = $page['nguoiky'];
        $data['soden']     = $page['soden'];
        $data['denngay']    = $page['denngay'];
        $data['chucvu']   =$page['chucvu'];
        $data['nguoinhap']  = $page['nguoinhap'];
        $data['cacloaivanban'] = $page['cacloaivanban'];
        $data['count'] = $page['count'];
        // duyệt văn bản chờ xử lý
        $this->insertDocAwait();
        $data['title']    = 'Giấy mời đã phân loại';
        $temp['data']     = $data;
        $temp['template'] = 'vanbanden/Vgiaymoidaxuly_gd';
        $this->load->view('layout_admin/layout',$temp);
    }

    /**
     * @return duyệt văn bản
     */
    public function insertDocAwait()
    {
        if($this->input->post('duyetvanban')){
            $arrayDuyet = array();
//            pr($this->input->post('duyet'));
            foreach ($this->input->post('duyet') as $key5 => $value5){
                if(!empty($value5)){
                    $PDirector = $this->input->post('phogiamdoc');
                    $Deparment = $this->input->post('phongchutri');
                    $Doc_array= array();
                    if($this->_session['iQuyenHan_DHNB'] == 5){
                        if (($PDirector[$key5] != $this->_session['PK_iMaCB']) && !empty($PDirector[$key5])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '4';
                            $Doc_array['iDuHop'] = _post('giamdocduhop')[$key5];
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        }  elseif (!empty($Deparment[$key5])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '5';
                            $Doc_array['iDuHop'] = _post('giamdocduhop')[$key5];
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        } elseif (empty($Deparment[$key5])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '4';
                            $Doc_array['iDuHop'] = _post('giamdocduhop')[$key5];
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        }
                    }
                    if($this->_session['iQuyenHan_DHNB'] == 4){
                        if (!empty($PDirector[$key5])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '4';
                            $Doc_array['iDuHop'] = (isset($this->input->post('giamdocduhop')[$value5]) ? $this->input->post('giamdocduhop')[$value5] : 0);
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        }  elseif (!empty($Deparment[$key5])) {
                            $Doc_array['iTrangThai_TruyenNhan'] = '5';
                            $Doc_array['iDuHop'] = (isset($this->input->post('giamdocduhop')[$value5]) ? $this->input->post('giamdocduhop')[$value5] : 0);
                            $Doc_array['iPhongDuHop'] = (isset($this->input->post('phongduhop')[$value5]) ? $this->input->post('phongduhop')[$value5] : 0);
                            $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$value5,'tbl_vanbanden',$Doc_array);
                        }
                    }
                    /** @var mang chuyen van ban cho pho giam doc $PDir_array */
                    $PDir_array= array();
                    $idpgd = $this->input->post('idpgd')[$key5];
                    if(!empty($PDirector[$key5])){
//                        pr();
                        if(!empty($idpgd)) {
                            if ($PDirector[$key5] != $this->_session['PK_iMaCB']) {
                                $PDir_array['PK_iMaCBNhan'] = $PDirector[$key5];
                                $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                                $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                                $PDir_array['sThoiGian'] = date('Y-m-d H:s', time());
                                $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$key5];
                                $PDir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key5] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key5]);
                                $PDir_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key5]) ? $this->input->post('vanbanquantrong')[$key5] : 0;
                                $PDir_array['iGiayMoi'] = 1;
                                $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
//                            pr($PDir_array);
                                $this->Mdanhmuc->capnhatDuLieu('PK_iMaCN', $idpgd, 'tbl_chuyennhanvanban', $PDir_array);
                            }
                        }else{
                            $PDir_array['PK_iMaCBNhan'] = $PDirector[$key5];
                            $PDir_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                            $PDir_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                            $PDir_array['sThoiGian'] = date('Y-m-d H:s', time());
                            $PDir_array['sMoTa'] = $this->input->post('chidaophogiamdoc')[$key5];
                            $PDir_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key5] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key5]);
                            $PDir_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key5]) ? $this->input->post('vanbanquantrong')[$key5] : 0;
                            $PDir_array['iGiayMoi'] = 1;
                            $PDir_array['input_per'] = $this->_session['PK_iMaCB'];
//                            pr($PDir_array);
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban', $PDir_array);

                        }
                    }
                    if(!empty($idpgd)&&$PDirector[$key5]==""){
                        $this->Mdanhmuc->xoaDuLieu('PK_iMaCN',$idpgd,'tbl_chuyennhanvanban');
                    }
                    /** @var mang chuyen phong chu tri va phoi hop $Departmanet_array */
                    $Departmanet_array= array();
                    $idpcc = $this->input->post('idpcc')[$key5];
                    if(!empty($idpcc)){
                        $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                        $Departmanet_array['PK_iMaPhongPH'] = $this->input->post('mangphoihop')[$key5];
                        $Departmanet_array['PK_iMaPhongCT'] = $Deparment[$key5];
                        $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                        $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
                        $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$key5];
                        $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key5] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key5]);
                        $Departmanet_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key5]) ? $this->input->post('vanbanquantrong')[$key5] : 0;
                        $Departmanet_array['iGiayMoi'] = 1;
                        $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
//                        pr($Departmanet_array);
                        $this->Mdanhmuc->capnhatDuLieu('PK_iMaCN',$idpcc,'tbl_chuyennhanvanban',$Departmanet_array);
                    }else{
                        $Departmanet_array['PK_iMaCBChuyen'] = $this->_session['PK_iMaCB'];
                        $Departmanet_array['PK_iMaPhongPH'] = $this->input->post('mangphoihop')[$key5];
                        $Departmanet_array['PK_iMaPhongCT'] = $Deparment[$key5];
                        $Departmanet_array['PK_iMaVBDen'] = $this->input->post('doc_id')[$key5];
                        $Departmanet_array['sThoiGian'] = date('Y-m-d H:s',time());
                        $Departmanet_array['sMoTa'] = $this->input->post('chidaophongchutri')[$key5];
                        $Departmanet_array['sThoiGianHetHan'] = (_post('hangiaiquyet')[$key5] == "") ? '0000-00-00' : date_insert(_post('hangiaiquyet')[$key5]);
                        $Departmanet_array['sVBQT'] = ($this->input->post('vanbanquantrong')[$key5]) ? $this->input->post('vanbanquantrong')[$key5] : 0;
                        $Departmanet_array['iGiayMoi'] = 1;
                        $Departmanet_array['input_per'] = $this->_session['PK_iMaCB'];
                        $this->Mdanhmuc->themDuLieu('tbl_chuyennhanvanban',$Departmanet_array);
                    }

                }
            }
            return redirect(base_url().'giaymoidaxuly_gd');

        }
    }
    public function PhanTrang()
    {
        if($this->_session['iQuyenHan_DHNB'] == 3){
            $giamdoc = 1;
        }
        else{
            $giamdoc = 2;
        }
        $loaivanban    = $this->input->get('loaivanban');
        $sokyhieu = $this->input->get('sokyhieu');
        $ngaynhap      = $this->input->get('ngaynhap');
        $donvi    = $this->input->get('donvi');
        $ngayky    = $this->input->get('ngayky');
        $ngayden = $this->input->get('ngayden');
        $trichyeu = $this->input->get('trichyeu');
        $nguoiky = $this->input->get('nguoiky');
        $soden = $this->input->get('soden');
        $denngay = $this->input->get('denngay');
        $cacloaivanban = $this->input->get('cacloaivanban');
//        pr($this->input->get('denngay'));
        $chucvu = $this->input->get('chucvu');
        $nguoinhap = $this->input->get('nguoinhap');
        $config['base_url']             = base_url().'giaymoidaxuly_gd?loaivanban='.$loaivanban.'&sokyhieu='.$sokyhieu.'&ngaynhap='.$ngaynhap.'&donvi='.$donvi.'&ngayky='.$ngayky.'&ngayden='.$ngayden.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&soden='.$soden.'&denngay='.$denngay.'&chucvu='.$chucvu.'&nguoinhap='.$nguoinhap;
        $config['total_rows']           = $this->Mgiaymoichoxuly->countDocGDAwait($this->_session['PK_iMaCB'],$giamdoc,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap);
//        pr($config['total_rows']);
        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 9;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = $this->input->get('page') ? $this->input->get('page') : 0;

        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'vanbanchoxuly_pgd');
        }
        $data['loaivanban']    = $loaivanban;
        $data['sokyhieu']      = $sokyhieu;
        $data['ngaynhap'] = $ngaynhap;
        $data['donvi'] = $donvi;
        $data['ngayky'] = $ngayky;
        $data['ngayden'] = $ngayden;
        $data['trichyeu'] = $trichyeu;
        $data['nguoiky'] = $nguoiky;
        $data['soden'] = $soden;
        $data['denngay'] =$denngay;
        $data['chucvu'] = $chucvu;
        $data['nguoinhap'] = $nguoinhap;
        $data['cacloaivanban'] =$cacloaivanban;
        $data['count']    = $config['total_rows'];
        $data['info']        = $this->Mgiaymoichoxuly->getDocGDAwait($this->_session['PK_iMaCB'],$giamdoc,$loaivanban,$sokyhieu,$ngaynhap,$donvi,$ngayky,$ngayden,$trichyeu,$nguoiky,$soden,$denngay,$chucvu,$nguoinhap,$config['per_page'], $data['page']);
        $data['pagination']  = $this->pagination->create_links();
//        pr($data['info']);
        return $data;
    }

}