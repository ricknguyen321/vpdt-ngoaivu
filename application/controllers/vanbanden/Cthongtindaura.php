<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongtindaura extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		$id              = _get('id');
		$process         = $this->Mvanbanchoxuly->getDocProcess($id);
		$processcv       = $this->Mvanbanchoxuly->getDocProcesscv($id);
		$processpb       = $this->Mvanbanchoxuly->getDocProcessPB($id);
		$data['process'] = array_merge($process,$processpb,$processcv);
		$data['files']   = layDuLieu('FK_iMaVBDen',$id,'tbl_files_vbden');
		if(!empty($id)){
            $data['thongtin'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$id,'tbl_vanbanden');
            $soden = $data['thongtin'][0]['iSoDen'];
            $mavbdi = $this->Mdanhmuc->layDuLieu('iSoDen',$soden,'tbl_vanbandi');
            if(!empty($mavbdi))
            {

            	$data['filesdi']   = $this->Mvanbandi->layFileLast($mavbdi[0]['PK_iMaVBDi']);
            }
            else{
            	$data['filesdi']   = '';
            }
        }
        // pr($data['filesdi']);
		$data['title']    = 'Thông tin văn bản';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden/Vthongtindaura';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cthongtindaura.php */
/* Location: ./application/controllers/vanbanden/Cthongtindaura.php */