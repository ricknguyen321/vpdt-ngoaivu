<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cwelcome extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
		$this->load->model('vanbandi/Mvanbandi');
        $this->load->model('Vanbanden/Mvanbanchoxuly');
		$this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
		$this->load->model('Vanbanden/Mgiaymoi');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mdmadmin','Mdmadmin');
        $this->Mdmadmin = new Mdmadmin();
        $this->load->model('vanbandi/Mtruyennhan');
        //$this->load->model('qldv/MqldvDeatails');
        //$this->load->model('qldv/Mqldv_phoihop');
        //$this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
        //$this->load->model('vanbanden_caphai/Mvanban_caphai');
        //$this->load->model('vanbandi_caphai/Mvanbandi_caphai');
        $this->load->model('Vanbanden/Mvanbanphoihop');
		$this->load->model('nghiphep/Mnghiphep');
    }
	public function DangNhap_NhanSu(){
        redirect('http://14.238.6.9/nhansu/haha_nhansu/'.$this->_session['FK_iMaCB']);
    }
	public function index()
        {
		//ricknguyen321 yêu cầu toàn bộ cbcc thay đổi mật khâu theo đúng quy tắc.
		$pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*\W).{8,16}$/";
		$result = preg_match ($pattern, $this->_session['sBanRo']);
		$data['trangchu'] = 1;
		$dateb = getdate();
		//$data['giaythu'] = $dateb['seconds'];
		//$data['ngaythu'] = $dateb['mday'];
		//$data['thangthu'] = $dateb['mon'];
		$tmp = $dateb['seconds'] % 10;
		//echo $tmp;
		//if ($tmp <= 1) {
			//$data['santa'] = 1;
			if ($tmp == 0) {
				$data['music'] = 1;
			}
		//}
		
		$is_mobile = '0';
		if(preg_match('/(android|iphone|ipad|up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
			$is_mobile=1;
		if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')>0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']))))
			$is_mobile=1;
		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
		$mobile_agents = array('w3c ','acs-','alav','alca','amoi','andr','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','oper','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda','xda-');
	 
		if(in_array($mobile_ua,$mobile_agents))
			$is_mobile=1;
	 
		if (isset($_SERVER['ALL_HTTP'])) {
			if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini')>0)
				$is_mobile=1;
		}
		if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows')>0)
			$is_mobile=0;
		$data['is_mobile'] = $is_mobile;
		
		if ( !$result ) {
			echo '<script language="javascript">';
			echo 'alert("Weak password. Please change your password!")';
			echo '</script>';
			redirect(base_url().'doimatkhaucn');
		}
			
        if($this->_session['iLoaiPhanMem'] != 2){

        }else{
            redirect('thongtinmoi');
        }
//	    pr($this->_session);
        /** Vanw ban den */
        $data['thongbao'] = $this->Mdanhmuc->layDuLieu2('iTrangThai !=',1,'iTrangThai_Xoa !=',1,'tbl_thongbao');
        if($this->_session['iQuyenHan_DHNB'] == 3){
            // văn bản chưa phân loại
            $data['plvb'] = $this->Mdmadmin->getDocAwait('2');
			$data['plvbctd'] = $this->Mdmadmin->getDocAwaitCTD('2');
            // văn bản đã phân loại
            //$docawaitdir = $this->Mdmadmin->getDocDirAwait('2');
            //$data['vbdpl'] = $docawaitdir;
            // văn bản chuyển lại
            $data['vbcl'] = $this->Mdmadmin->getDocReject();
            // phòng chủ trì
            $data['pct'] = $this->Mdmadmin->getDocAwait('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
            // phong phối hợp
            $pph = $this->Mdmadmin->getDocAwaitPPH_welcome(3,$this->_session['FK_iMaPhongHD'],NULL,'2','5','0');
            $data['pph'] = $pph;
            $phoihopxl = $this->Mdmadmin->getDocAwaitPPHDXL_welcome(3,$this->_session['FK_iMaPhongHD'],NULL,'1','5',1);
            // //$data['phdxl'] = $phoihopxl;
            // văn bản phòng đã xử lý
//            $data['pdxy'] = count($this->Mvanbanchoxuly->getDocComplete($this->_session['FK_iMaPhongHD']));
            //$data['vanbanchidao'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB']);
            // phân loại giấy mời
            $data['plgm'] = $this->Mdmadmin->getDocAwait('2',NULL,NULL,NULL,'1');
            // giấy mười đã phân loại
            //$data['gmdpl'] = $this->Mdmadmin->getDocDirAwait('1');
            // giấy mời chờ xử lý
            $data['gmcxl'] = $this->Mdmadmin->getDocAwait1('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,'1');
            // giấy mời phối hợp chờ xử lý
            $gmphcxl = $this->Mdmadmin->getDocAwaitPPH_welcome(3,$this->_session['FK_iMaPhongHD'],NULL,'1','5',0);
			//$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,1);
			//$data['dsdonthuht'] = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,NULL,1);
            $data['gmphcxl'] = $gmphcxl;
            // giấy mời đã xử lý
            ////$data['gmdxl'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB'],NULL,'1');
//            pr(//$data['gmdxl']);
            //$data['vbqt'] = $this->Mdmadmin->getVBQT($this->_session['PK_iMaCB']);
			//$data['vbqtdgq'] = $this->Mdmadmin->getVBQTGQ($this->_session['PK_iMaCB']);
            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
            $data['vbcduyet'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB']);
			//$data['dschutri'] = $this->Mdmadmin->getDocCT_welcome($this->_session['FK_iMaPhongHD']);
			$data['tuchoigm'] = $this->Mdmadmin->PPPHTuChoi_welcome($this->_session['FK_iMaPhongHD'],1);
			$data['vbduyetgiaymoi'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB'],1);
			//$data['dsphoihop'] = $this->Mdmadmin->getDocAwaitDSCTPPH_welcome($this->_session['FK_iMaPhongHD']);
			$data['tocongtac'] = $this->Mdmadmin->getDocAwaitTCT('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
			$data['tuchoipp'] = $this->Mdmadmin->PPPHTuChoi_welcome($this->_session['FK_iMaPhongHD']);
            $data['bcchoduyet'] = $this->Mdmadmin->getDuyetBaoCao(NULL,$this->_session['FK_iMaPhongHD']);
            $data['dsgiahan'] = $this->Mdmadmin->dsGiaHan($this->_session['PK_iMaCB']);
            $data['congtacdang'] = $this->Mdmadmin->getCTD('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL);
            //$data['phdxl'] = $this->Mdmadmin->getDocDaPPH($this->_session['iQuyenHan_DHNB'],$this->_session['FK_iMaPhongHD']);
//            pr($data['phdxl']);
            //$data['gmphdxl'] = $this->Mdmadmin->getDocDaPPH(6,$this->_session['FK_iMaPhongHD'],NULL,NULL,1);
            //$data['vanbandaxuly_stcph'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB'],NULL,NULL,1);

            //$data['tonglanhdaogiao'] = $this->Mdmadmin->countphongthuly($this->_session['FK_iMaPhongHD']);

            //$data['tongphlanhdaogiao'] = $this->Mdmadmin->countphongphoihop($this->_session['FK_iMaPhongHD']);
			
			$data['tongvbquahan'] = count($this->Mdanhmuc->getDocCT_quahan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
			$data['tongvbsapdenhan'] = count($this->Mdanhmuc->demDocCT_sapdenhan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));

        }
        if($this->_session['iQuyenHan_DHNB'] == 4){
            //$data['dggiaochoduyet'] = $this->Mdmadmin->countlanhdaogiao($this->_session['PK_iMaCB']);

            $data['vbcxlcvp'] = $this->Mdmadmin->getDocAwait('3');
			$data['plvbctd'] = $this->Mdmadmin->getDocAwaitCTD('3');
            //$data['dstocongtac'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB']);
            //$data['dstocongtac_ht'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB'],2);
            //$data['vbqt'] = $this->Mdmadmin->getVBQT($this->_session['PK_iMaCB']);
			//$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,1);
			//$data['dsdonthuht'] = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,NULL,1);
			//$data['vbqtdgq'] = $this->Mdmadmin->getVBQTGQ($this->_session['PK_iMaCB']);
//            pr(//$data['vbqt']);
            $data['gmcxl'] = $this->Mdmadmin->getDocAwait('3',NULL,NULL,NULL,'1');
            ////$data['gmdxl'] = $this->Mdmadmin->getDocGDAwait($this->_session['PK_iMaCB']);
            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
			$data['dsgiahan'] = $this->Mdmadmin->dsGiaHan($this->_session['PK_iMaCB']);
			//$data['vanbandaxuly'] = $this->Mdmadmin->getDocGoGDAwait($this->_session['PK_iMaCB']);
			//$data['vanbandaxuly_stcph'] = $this->Mdmadmin->getDocGoGDAwait($this->_session['PK_iMaCB'],1);
			$data['tongvbquahan'] = count($this->Mdanhmuc->getDocCT_quahan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
			$data['tongvbsapdenhan'] = count($this->Mdanhmuc->demDocCT_sapdenhan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
        }
        if($this->_session['iQuyenHan_DHNB'] == 5){
            //$data['dggiaochoduyet'] = $this->Mdmadmin->countlanhdaogiao($this->_session['PK_iMaCB']);
            $data['vbcxlcvp'] = $this->Mdmadmin->getDocAwait('4',$this->_session['PK_iMaCB']);
			$data['plvbctd'] = $this->Mdmadmin->getDocAwaitCTD('4',$this->_session['PK_iMaCB']);
            $data['theodoivanban'] = $this->Mvanbanchoxuly->demDocFollow('5',$this->_session['PK_iMaCB'],NULL,NULL,'1',NULL);
            //$data['vbqt'] = $this->Mdmadmin->getVBQT($this->_session['PK_iMaCB']);
            //$data['dstocongtac'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB']);
            //$data['dstocongtac_ht'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB'],2);
			// danh sách đơn thư chưa ht - hoàn thành
			//$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,1);
			//$data['dsdonthuht'] = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,NULL,1);
			//văn bản quan trọng
			//$data['vbqtdgq'] = $this->Mdmadmin->getVBQTGQ($this->_session['PK_iMaCB']);
			//giấy mời chờ xử lý
            $data['gmcxl'] = $this->Mdmadmin->getDocAwait('4',$this->_session['PK_iMaCB'],NULL,NULL,'1');
            //$data['gmdxl'] = $this->Mdmadmin->getDocGDAwait($this->_session['PK_iMaCB']);
            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
			$data['dsgiahan'] = $this->Mdmadmin->dsGiaHan($this->_session['PK_iMaCB']);
			//$data['vanbandaxuly'] = $this->Mdmadmin->getDocGoGDAwait($this->_session['PK_iMaCB']);
			//$data['vanbandaxuly_stcph'] = $this->Mdmadmin->getDocGoGDAwait($this->_session['PK_iMaCB'],1);
            //$data['getDocAwaitPH'] = $this->Mdmadmin->getDocAwaitBGD($this->_session['FK_iMaPhongHD'],NULL,$this->_session['PK_iMaCB']);
            //$data['getAppoAwaitPH'] = $this->Mdmadmin->getDocAwaitBGD($this->_session['FK_iMaPhongHD'],1,$this->_session['PK_iMaCB']);
            //$data['getDocSeenBGD'] = $this->Mdmadmin->getDocSeenBGD($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);
            //$data['getDocSeenBGD_GM'] = $this->Mdmadmin->getDocSeenBGD($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],1);
			$data['tongvbquahan'] = count($this->Mdanhmuc->getDocCT_quahan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
			$data['tongvbsapdenhan'] = count($this->Mdanhmuc->demDocCT_sapdenhan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
        }
        // quyền trưởng phòng
        if($this->_session['iQuyenHan_DHNB'] == 6){
            //$data['tonglanhdaogiao'] = $this->Mdmadmin->countphongthuly($this->_session['FK_iMaPhongHD']);
            //$data['tongphlanhdaogiao'] = $this->Mdmadmin->countphongphoihop($this->_session['FK_iMaPhongHD']);

            $data['vbcxlcvp'] = $this->Mdmadmin->getDocAwait('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
            $data['congtacdang'] = $this->Mdmadmin->getCTD('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL);
            $data['tocongtac'] = $this->Mdmadmin->getDocAwaitTCT('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
			//$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,$this->_session['FK_iMaPhongHD'],NULL,1);
            ////$data['dsdonthuht'] = $this->Mdmadmin->getDonThu(NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,1);
            // van ban phoi hop
            $pph = $this->Mdmadmin->getDocAwaitPPH_welcome(6,$this->_session['FK_iMaPhongHD'],NULL,'2','5',0);
            $data['pph'] = $pph;
            //$data['vanbanchidao'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB']);
            $data['gmcxl'] = $this->Mdmadmin->getDocAwait1('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,'1');
            // giay moi da xu ly
            //$data['gmdxl'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB'],$this->_session['FK_iMaPhongHD'],'1');
			$data['gmdht'] = $this->Mdmadmin->getDocTPCDHT($this->_session['PK_iMaCB'],$this->_session['FK_iMaPhongHD'],'1');
            //giay moi phoi hop cho xu ly
            $gmph = $this->Mdmadmin->getDocAwaitPPH_welcome(6,$this->_session['FK_iMaPhongHD'],NULL,'1','5',0);
            $data['gmph'] = $gmph;
			
            $phoihopxl = $this->Mdmadmin->getDocAwaitPPHDXL_welcome(6,$this->_session['FK_iMaPhongHD'],NULL,'1','5',1);
            //$data['phdxl'] = $phoihopxl;
            // van ban cho duyet hoan thanh
            $data['vbcduyet'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB']);
			$data['vbduyetgiaymoi'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB'],1);
			$data['dstocongtac'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB']);
            $data['dstocongtac_ht'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB'],2);
//            pr($data['vanbanchidao']);

            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
            //$data['vbqt'] = $this->Mdmadmin->getVBQT($this->_session['PK_iMaCB']);
			//$data['vbqtdgq'] = $this->Mdmadmin->getVBQTGQ($this->_session['PK_iMaCB']);
			$data['tuchoipp'] = $this->Mdmadmin->PPPHTuChoi_welcome($this->_session['FK_iMaPhongHD']);
			$data['tuchoigm'] = $this->Mdmadmin->PPPHTuChoi_welcome($this->_session['FK_iMaPhongHD'],1);
			//$data['dschutri'] = $this->Mdmadmin->getDocCT_welcome($this->_session['FK_iMaPhongHD']);
			$data['dsgiahan'] = $this->Mdmadmin->dsGiaHan($this->_session['PK_iMaCB']);
			
			//$data['dsdadexuat'] = $this->Mdmadmin->layVBGiaHanTuChoi_welcome(NULL,$this->_session['FK_iMaPhongHD']);
			//$data['dsphoihop'] = $this->Mdmadmin->getDocAwaitDSCTPPH_welcome($this->_session['FK_iMaPhongHD']);
			//$data['vanbandaxuly_stcph'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB'],NULL,NULL,1);
            $data['bcchoduyet'] = $this->Mdmadmin->getDuyetBaoCao(NULL,$this->_session['FK_iMaPhongHD']);
            //$data['phdxl'] = $this->Mdmadmin->getDocDaPPH($this->_session['iQuyenHan_DHNB'],$this->_session['FK_iMaPhongHD']);
//            pr($data['phdxl']);
            //$data['gmphdxl'] = $this->Mdmadmin->getDocDaPPH(6,$this->_session['FK_iMaPhongHD'],NULL,NULL,1);
			$data['tongvbquahan'] = count($this->Mdanhmuc->getDocCT_quahan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
			$data['tongvbsapdenhan'] = count($this->Mdanhmuc->demDocCT_sapdenhan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
             // pr($data['phdxl']);
            if($this->_session['iQuyenHan_DHNB'] == 6 && $this->_session['FK_iMaCV'] == 15){
                //$data['vanbanchidao_cc'] = $this->Mdmadmin->getConCac($this->_session['FK_iMaPhongHD']);
                //$data['vanbandaxuly_stcphcc'] = $this->Mdmadmin->getConCac($this->_session['FK_iMaPhongHD'],NULL,1);
                $data['tocongtac'] = $this->Mdmadmin->getDocAwaitTCT('5',NULL,$this->_session['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
                $data['gmdxl_cc'] = $this->Mdmadmin->getConCac($this->_session['FK_iMaPhongHD'],'1');
                //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
                //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
                //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
            }
        }
        if($this->_session['iQuyenHan_DHNB'] == 7){
            $data['vbcxlcvp'] = $this->Mdmadmin->getDocAwait('6',NULL,NULL,$this->_session['PK_iMaCB'],NULL,NULL,NULL,1);
            $data['tocongtac'] = $this->Mdmadmin->getDocAwaitTCT('6',NULL,NULL,$this->_session['PK_iMaCB'],NULL,NULL,NULL,1);
            $data['congtacdang'] = $this->Mdmadmin->getCTD('6',NULL,NULL,$this->_session['PK_iMaCB'],NULL,NULL,NULL);
			//$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,NULL,$this->_session['PK_iMaCB'],1);
            ////$data['dsdonthuht'] = $this->Mdmadmin->getDonThu(NULL,NULL,$this->_session['PK_iMaCB'],NULL,1);
			$data['phophongphoihop'] = $this->Mdmadmin->getDocAwaitPPPH($this->_session['iQuyenHan_DHNB'],$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],NULL);
			$data['gm_phophongph'] = $this->Mdmadmin->getDocAwaitPPPH($this->_session['iQuyenHan_DHNB'],$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],1);
            $data['theodoivanban'] = $this->Mdmadmin->getDocAwait('7',NULL,NULL,$this->_session['PK_iMaCB'],NULL,NULL,'1');
            $pph = $this->Mdmadmin->getDocAwaitPPH_welcome(7,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],'2',NULL,NULL,'2');
            $data['pph'] = $pph;
            $gmph = $this->Mdmadmin->getDocAwaitPPH_welcome(7,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],'1',NULL,NULL,'2');
            $data['gmph'] = $gmph;
            //$data['gmphdxl'] = $this->Mdmadmin->getDocDaPPH(7,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],'4',1);
            //$data['vanbanchidao'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB']);
            $data['gmcxl'] = $this->Mdmadmin->getDocAwait('6',NULL,NULL,$this->_session['PK_iMaCB'],'1');
            //$data['gmdxl'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB'],$this->_session['FK_iMaPhongHD'],'1');
            $data['vbcduyet'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB']);
			$data['vbduyetgiaymoi'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB'],1);
            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
			//$data['dschutri'] = $this->Mdmadmin->getDocCT_welcome($this->_session['FK_iMaPhongHD']);
			//$data['dstocongtac'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB']);
            //$data['dstocongtac_ht'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB'],2);
			$data['dsgiahan'] = $this->Mdmadmin->dsGiaHan($this->_session['PK_iMaCB']);
			//$data['dsdadexuat'] = $this->Mdmadmin->layVBGiaHanTuChoi_welcome(NULL,NULL,$this->_session['PK_iMaCB']);
			//$data['dsphoihop'] = $this->Mdmadmin->getDocAwaitDSCTPPH_welcome($this->_session['FK_iMaPhongHD']);
			//$data['canboxuly'] = $this->Mdmadmin->countDocGo($this->_session['PK_iMaCB']);
			//$data['vanbandaxuly_stcph'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB'],NULL,NULL,1);
            $data['daguibituchoi'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB'],2);
            //$data['phdxl'] = $this->Mdmadmin->getDocDaPPH(7,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],'4');
            //$data['dsphoihopcv'] = $this->Mdmadmin->getListCVPH_welcome($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB']);

            //$data['tongphophonglanhdaogiao'] = $this->Mdmadmin->countphophongthuly($this->_session['PK_iMaCB']);

            //$data['tongphoihopthuly'] = $this->Mdmadmin->countphoihopthuly($this->_session['PK_iMaCB']);
			$data['tongvbquahan'] = count($this->Mdanhmuc->getDocCT_quahan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
			$data['tongvbsapdenhan'] = count($this->Mdanhmuc->demDocCT_sapdenhan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
            
        }
        if($this->_session['iQuyenHan_DHNB'] == 8){
            $getDocAwaitPPH = $this->Mdmadmin->getDocAwaitCVPPH(NULL,NULL,$this->_session['PK_iMaCB'],'2',NULL,NULL,1);
            $data['vbcxlcvp'] = $this->Mdmadmin->getDocAwait('7',NULL,NULL,$this->_session['PK_iMaCB'],NULL,NULL,NULL,1);
            $data['congtacdang'] = $this->Mdmadmin->getCTD('7',NULL,NULL,$this->_session['PK_iMaCB'],NULL,NULL,NULL);
            $data['tocongtac'] = $this->Mdmadmin->getDocAwaitTCT('7',NULL,NULL,$this->_session['PK_iMaCB'],NULL,NULL,NULL,1);
			//$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,NULL,$this->_session['PK_iMaCB'],1);
            ////$data['dsdonthuht'] = $this->Mdmadmin->getDonThu(NULL,NULL,$this->_session['PK_iMaCB'],NULL,1);
            $data['vbph'] = $getDocAwaitPPH;
            $data['gmcxl'] = $this->Mdmadmin->getDocAwait('7',NULL,NULL,$this->_session['PK_iMaCB'],'1');
            //$data['gmdxl'] = $this->Mdmadmin->getDocComplete(NULL,$this->_session['PK_iMaCB']);
            $data['gmph'] = $this->Mdmadmin->getDocAwaitCVPPH(NULL,NULL,$this->_session['PK_iMaCB'],'1',NULL,NULL,1);
//            pr($data['gmph']);
            //$data['vbcduyet'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, $this->_session['PK_iMaCB'], 2);
			//$data['vbhoanthanh'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, $this->_session['PK_iMaCB'], 1);
			$data['vbduyetgiaymoi'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, $this->_session['PK_iMaCB'], 2,NULL,NULL,1);
//            pr(//$data['gmdxl']);
            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
			//$data['dschutri'] = $this->Mdmadmin->getDocCT_welcome($this->_session['FK_iMaPhongHD']);
			$data['dstocongtac'] = $this->Mdmadmin->getToCongTacHT(NULL,NULL,$this->_session['PK_iMaCB'], 7);
            $data['dstocongtac_ht'] = $this->Mdmadmin->getToCongTacHT(NULL,2,$this->_session['PK_iMaCB']);
			 //$data['dsdadexuat'] = $this->Mdmadmin->layVBGiaHanTuChoi_welcome(NULL,NULL,$this->_session['PK_iMaCB']);
			 //$data['dsphoihop'] = $this->Mdmadmin->getDocAwaitDSCTPPH_sua($this->_session['FK_iMaPhongHD']);
			 //$data['dsphoihopcv'] = $this->Mdmadmin->getListCVPH_sua(NULL,$this->_session['PK_iMaCB']);
			 //$data['canboxuly'] = $this->Mdmadmin->countDocGo($this->_session['PK_iMaCB']);
             $data['daguibituchoi'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB'],2);

            //$data['tongchuyenvienlanhdaogiao'] = $this->Mdmadmin->countchuyenvienthuly($this->_session['PK_iMaCB']);

            //$data['tongphoihopthuly'] = $this->Mdmadmin->countphoihopthuly($this->_session['PK_iMaCB']);
			if ($data['vbcxlcvp'] > 0 || $data['dstocongtac'] > 0) {
				$data['tongvbquahan'] = count($this->Mdanhmuc->getDocCT_quahan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
				$data['tongvbsapdenhan'] = count($this->Mdanhmuc->demDocCT_sapdenhan($this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB']));
			}
        }
        if($this->_session['iQuyenHan_DHNB'] == 10){
            $data['vbcxlcvp'] = $this->Mdmadmin->getDocConcacAwait('6','2',NULL,NULL,$this->_session['PK_iMaCB']);
            $data['vanbanchidao_pcc'] = $this->Mdmadmin->getPConCac($this->_session['PK_iMaCB']);
			$data['vanbandaxuly_stcphccp'] = $this->Mdmadmin->getPConCac($this->_session['PK_iMaCB'],NULL,1);
			//$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,NULL,$this->_session['PK_iMaCB']);
            $data['gmdxl_pcc'] = $this->Mdmadmin->getPConCac($this->_session['PK_iMaCB'],'1');
            $data['gmcxl'] = $this->Mdmadmin->getDocConcacAwait('6','2',NULL,NULL,$this->_session['PK_iMaCB'],'1');
            $data['vbcduyet'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB']);
			$data['vbduyetgiaymoi'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB'],1);
            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
			//$data['dschutri'] = $this->Mdmadmin->getDocCT_welcome($this->_session['FK_iMaPhongHD']);
            //$data['dschutri1'] = $this->Mdmadmin->getDocCT2($this->_session['PK_iMaCB']);
			//$data['dsphoihop'] = $this->Mdmadmin->getDocAwaitDSCTPPH_welcome($this->_session['FK_iMaPhongHD']);
            $data['dsgiahan'] = $this->Mdmadmin->dsGiaHan($this->_session['PK_iMaCB']);
            $data['daguibituchoi'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB'],2);

            $data['tongccplanhdaogiao'] = $this->Mdmadmin->countccpthuly($this->_session['PK_iMaCB']);

            //$data['tongphoihopthuly'] = $this->Mdmadmin->countphoihopthuly($this->_session['PK_iMaCB']);

        }
        if($this->_session['iQuyenHan_DHNB'] == 11){
            $data['vbcxlcvp'] = $this->Mdmadmin->getDocConcacAwait('6','3',NULL,$this->_session['PK_iMaCB']);
            $data['congtacdang'] = $this->Mdmadmin->getDocCcDangAwait('6','3',NULL,$this->_session['PK_iMaCB']);
            //$data['dsdonthu'] = $this->Mdmadmin->getDonThu(NULL,NULL,$this->_session['PK_iMaCB'],1);
            ////$data['dsdonthuht'] = $this->Mdmadmin->getDonThu(NULL,NULL,$this->_session['PK_iMaCB'],NULL,1);
            // van ban phoi hop
            $data['pph'] = $this->Mdmadmin->getDocAwaitPPH_welcome(11,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],'2',NULL,NULL,'1');
            $data['vanbanchidao'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB']);
            $data['gmcxl'] = $this->Mdmadmin->getDocConcacAwait('6','3',NULL,$this->_session['PK_iMaCB'],NULL,'1');
            // giay moi da xu ly
            //$data['gmdxl'] = $this->Mdmadmin->getDocTPCDAwait($this->_session['PK_iMaCB'],$this->_session['FK_iMaPhongHD'],'1');
            //giay moi phoi hop cho xu ly
            $data['gmph'] = $this->Mdmadmin->getDocAwaitPPH_welcome(11,$this->_session['FK_iMaPhongHD'],$this->_session['PK_iMaCB'],'1','1');
//            pr($this->Mgiaymoichoxuly->getDocAwaitPPH(NULL,$this->_session['FK_iMaPhongHD'],NULL,'5'));
            //$data['phdxl'] = $this->Mdmadmin->getDocAwaitPPHDXL_welcome($this->_session['iQuyenHan_DHNB'],$this->_session['FK_iMaPhongHD'],NULL,'1','6');
            // van ban cho duyet hoan thanh
            $data['vbcduyet'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB']);
            $data['vbduyetgiaymoi'] = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$this->_session['PK_iMaCB'],1);
//            pr($data['vanbanchidao']);
            //$data['chuaxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],0);
            //$data['daxem'] = $this->Mdmadmin->getMeeting($this->_session['PK_iMaCB'],1);
            //$data['dagui'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB']);
            //$data['dschutri'] = $this->Mdmadmin->getDocCT_welcome($this->_session['FK_iMaPhongHD']);
            //$data['dschutri1'] = $this->Mdmadmin->getDocCT1($this->_session['PK_iMaCB']);
            $data['dstocongtac'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB']);
            $data['dstocongtac_ht'] = $this->Mdmadmin->getToCongTacHT($this->_session['PK_iMaCB'],2);
            //$data['dsphoihop'] = $this->Mdmadmin->getDocAwaitDSCTPPH_welcome($this->_session['FK_iMaPhongHD']);
            $data['dsgiahan'] = $this->Mdmadmin->dsGiaHan($this->_session['PK_iMaCB']);
            $data['daguibituchoi'] = $this->Mdmadmin->getSoanThaoDaGui($this->_session['PK_iMaCB'],2);

            $data['tongtpcclanhdaogiao'] = $this->Mdmadmin->counttpccthuly($this->_session['PK_iMaCB']);
            //$data['tongphoihopthuly'] = $this->Mdmadmin->countphoihopthuly($this->_session['PK_iMaCB']);
        }
        /*****************************/
        $taikhoan       = $this->_session['PK_iMaCB'];
        $chucvu         = $this->_session['FK_iMaCV'];
        $quyen          = $this->_session['iQuyenHan_DHNB'];
        $phongban       = $this->_session['FK_iMaPhongHD'];
        $phongbanchicuc = $this->_session['iQuyenDB'];
		
		
        $trangthaikq    = 6;
        $phongchicuc    = NULL;
        $trangthaiduyet = 10;// không ai cả
        $canbo          = '';// không ai cả
        if($phongban==12)
        {
            if($quyen==6 || $quyen==10)
            {
                $phongchicuc    = NULL;
            }
            else{
                $phongchicuc = $phongbanchicuc;
            }
            
        }
        switch ($quyen) {
            case 3:
                if($chucvu==6) // chức vụ chánh văn phòng
                {
                    $trangthaikq = 1;
                    $trangthai_tk       = 5;
                    $trangthai =3; #TP
                    $ten_taikhoan       = 'truongphong';
                    $ykien              = 'ykien_tp';
                    $ten_trangthai_xem  = '';
                    $trangthai_xem      = '';
                    $chude              = '';
					$trangthaiduyet     = 2;
                }else{// bộ phận tổng hợp
                    $canbo              = $taikhoan;
                    $phongban           = '';
                    $trangthaikq = 2;
                    $trangthai_tk       = 55;
                    $trangthai =33;
                    $ten_taikhoan       = 'bophantonghop';
                    $ykien              = 'ykien_bpth';
                    $ten_trangthai_xem  = 'trangthai_xem_bpth';
                    $trangthai_xem      = 1;
                    $chude              = '';
                    $trangthaiduyet     = 3;
                }
                break;
            case 4: #GD
                $trangthai_tk       = 1;
                $trangthai =7;
                $ten_taikhoan       = 'giamdoc';
                $ykien              = 'ykien_gd';
                $ten_trangthai_xem  = 'trangthai_xem_gd';
                $trangthai_xem      = 1;
                $chude              = '';
                break;
            case 5: #PGD
                $trangthai_tk       = 2;
                $trangthai =6;
                $ten_taikhoan       = 'phogiamdoc';
                $ykien              = 'ykien_pgd';
                $ten_trangthai_xem  = 'trangthai_xem_pgd';
                $trangthai_xem      = 1;
                $chude              = '';
                break;
            case 6: #TP
                $trangthaikq = 1;
                $trangthaiduyet = 2;
                if($chucvu==15) // chức vụ chi cục trưởng
                {
                    $trangthai_tk       = 3;
                    $trangthai    = 5; #CCT
                    $ten_taikhoan = 'truongchicuc';
                    $ykien        = 'ykien_tcc';
                }
                if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
                {
                    $trangthai_tk       = 5;
                    $trangthai    = 3;
                    $ten_taikhoan = 'truongphong';
                    $ykien        = 'ykien_tp';
                }
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 7: #PTP
                $canbo              = $taikhoan;
                $trangthaiduyet     = 1;
                $trangthai_tk       = 6;
                $trangthai =2;
                $ten_taikhoan       = 'phophong';
                $ykien              = 'ykien_pp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 8: #CV
                $trangthai_tk       = 7;
                $trangthai =1;
                $ten_taikhoan       = 'chuyenvien';
                $ykien              = 'ykien_cv';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 10:#CCP
                $trangthaikq = 1;
                $trangthai_tk       = 4;
                $trangthai =4; 
                $ten_taikhoan       = 'phochicuc';
                $ykien              = 'ykien_pcc';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 11:#TP bên chi cục
                $canbo              = $taikhoan;
                $trangthaiduyet     = 1;
                $trangthai_tk       = 555;
                $trangthai =333;
                $ten_taikhoan       = 'truongphong';
                $ykien              = 'ykien_tp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            default:
                $trangthai =9;
                break;
        }
		
        if($trangthai==9){
            $trangthai_tk        = 0;
            $data['vbdichoxuly'] = 0;
            $data['vbdidaxuly']  = 0;
            //$data['dagui']       = '';
            //$data['chuaxem']     = '';
            //$data['daxem']       ='';
        }
        else{
            $data['vbdichoxuly']    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
            $data['vbdidaxuly']     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
        }
        
        if($quyen==3&&$chucvu==6)
        {
            $trangthai2 =999; #TP
            $ten_taikhoan       = 'chanhvanphong';
            $ykien              = 'ykien_cvp';
            $ten_trangthai_xem  = 'trangthai_xem_cvp';
            $trangthai_xem      = 1;
            $chude              = '';
            $data['vbdichoxulycvp']    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
        }		
        $phongchicuc = $this->_session['iQuyenDB'];
		/*
        $data['dauvieclanhdaochoxuly']      = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauvieclanhdaodaxuly']       = $this->Mchuyennhan->demDauViecDaXuLy($taikhoan,2,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecchutrichoxuly']       = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecchutriphchoxuly']     = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecchutriphdaxuly']      = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopchoxuly']      = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopchoxuly_phph'] = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecchoxulyctphct_pp_cc'] = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecchoxulyctphph_pp_cc'] = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecdaxulyctphct_pp_cc'] = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,2,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecdaxulyctphph_pp_cc'] = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,2,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

        $data['dauviecphoihopchoxuly_tp']   = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopchoxuly_cvph'] = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopdaxuly']       = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,2,2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopdaxuly_ph']    = $this->Mchuyennhan->demDauViecDaXuLyPH($phongban,$taikhoan,2,2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopdaxuly_phph']  = $this->Mchuyennhan->demDauViecDaXuLyPH($phongban,$taikhoan,2,2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopdaxuly_tpph']  = $this->Mchuyennhan->demDauViecDaXuLyPH($phongchicuc,$taikhoan,2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecchutridaxuly']        = $this->Mchuyennhan->demDauViecDaXuLy($taikhoan,2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphoihopdaxuly_tp']    = $this->Mchuyennhan->demDauViecDaXuLy($taikhoan,2,2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['dauviecphongchutri']         = $this->Mchuyennhan->demDauViecCuaPhong($phongban,1,$phongchicuc);
        $data['dauviecphongphoihop']        = $this->Mchuyennhan->demDauViecCuaPhong($phongban,2,$phongchicuc);
        $data['dauviecchoduyet']            = $this->Mchuyennhan->demDauViecChoDuyet($taikhoan);
        $data['dauviectralai']              = $this->Mchuyennhan->demDauViecTraLai($taikhoan);
        $data['duyetketqua']                = $this->Mketqua->demTTKT($trangthaiduyet,$phongban,$canbo);
        $data['dauviecquahan']              = $this->Mketqua->demViecQuaHan($phongban);
        $data['trangthai_tk']   = $trangthai_tk;
		*/
        // kế hoạch công tác
        $week = (int)date("W");
       
        $month = (int)date('m');
        /*
		if($quyen == 7 or $quyen == 11){
            

           $data['tong_sl_cv'] =  $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND (((active = 0 and lanhdao_id = ".$this->_session['PK_iMaCB'].") or (active = 3 and canbo_id = ".$this->_session['PK_iMaCB'].")  or (active = 4 and (ketluan_pp IS NULL or ketluan_pp ='') and lanhdao_id = ".$this->_session['PK_iMaCB'].")) or ( active <= 3 and phoihop like '%[".$this->_session['PK_iMaCB']."]%')) and tuan =". $week);
        }
        if($quyen == 8){
            $data['tong_sl_cv'] = $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND (( active = 3 and canbo_id = ".$this->_session['PK_iMaCB'].") or ( active <= 3 and phoihop like '%[".$this->_session['PK_iMaCB']."]%')) and tuan =". $week);
        }
        if($quyen == 6 || $quyen == 3){
        	
            $data['tong_sl_cv'] = $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND ( active = 1 or active = 2 or  active = 4) and phong_id = ".$this->_session['FK_iMaPhongHD']." and tuan =". $week);
        }
		*/
		
        // tổng văn vản quá hạn
        if($this->_session['iQuyenHan_DHNB'] == 4 || $this->_session['iQuyenHan_DHNB'] == 5){
        	$data['tong_cho_ldduyet'] = $this->Mdanhmuc->get_kehoach7('lanhdao_id ='.$this->_session['PK_iMaCB'].'  ','kehoachtrinhld','kh_id');
            $this->_session['FK_iMaPhongHD'] = NULL;
        }else{
            $this->_session['FK_iMaPhongHD'] = $this->_session['FK_iMaPhongHD'];
        }
		//$data['vbcothoihan'] = $this->Mvanbanden->countDocGo(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
        $data['vanbandi_phong']  = $this->Mvanbandi->demDSVB_Phong($phongban,1);
        $data['giaymoidi_phong'] = $this->Mvanbandi->demDSVB_Phong($phongban,10);
        
        if(date('l') == 'Monday' or date('l') == 'Tuesday')$week = $week -1 ;
        if($week ==0) $week = 53;
        $data['tuan'] = $week;
        $data['thang'] = $month;
        $data['taikhoan'] = $taikhoan;
        
        $data['trangthai']      = $trangthai;
        $data['title']       = 'Chào mừng đồng chí '.$this->_session['sHoTen'];
		$macanbo             = $this->_session['PK_iMaCB'];
		$data['tinmoi']      = $this->Msoanthaothongtin->demVanBanMoiNhan2($this->_session['PK_iMaCB'],0);
		//$data['tindaxem']    = $this->Msoanthaothongtin->demDuLieu($this->_session['PK_iMaCB'],1);
		//$data['tindagui']    = $this->Msoanthaothongtin->demTinDaGui($macanbo);
		$data['vanbandi']    = $this->Mvanbandi->layVBChoSo(NULL,1,1);
		$data['giaymoidi']   = $this->Mvanbandi->layVBChoSo(NULL,1,10);
		$data['vanbanchoso'] = $this->Mvanbandi->layVBChoSo(NULL,0,NULL);
        $data['vanbandaduyet'] = $this->Mtruyennhan->demvanbandaduyet($phongban);
		$data['dsnghiphep'] = $this->Mnghiphep->demDSNghiPhep(NULL,NULL);
		$data['nghiphepcb'] = $this->Mnghiphep->demDSNghiPhep(NULL,$this->_session['PK_iMaCB']);
		$data['nghiphepmoi'] = $this->Mnghiphep->demNghiPhepMoi();
		if ($this->_session['iQuyenHan_DHNB'] < 8) {
			$data['phepchoduyet'] = count($this->Mnghiphep->laydschoduyet($this->_session['PK_iMaCB']));
		}

		/*
        // văn bản cấp hai
        $donvi                      = $this->_session['FK_iMaPhongHD'];
        $phong_caphai               = $this->_session['phong_caphai'];
        $data['lanhdaochoxuly']     = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,1,1,1,NULL,NULL);        
        $data['lanhdaodaxuly']      = $this->Mvanban_caphai->demDSVB_Phong_ChuaXuLy($donvi,$taikhoan);
        
        $data['phongct_choxuly']    = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,2,1,1,NULL,NULL);
        $data['phongph_choxuly']    = $this->Mvanban_caphai->demDSVB_Phong_PH($donvi,$taikhoan,3,1,1,NULL,NULL);
        $data['phongct_daxuly']     = $this->Mvanban_caphai->demDSVB_Phong_ChuaGiaiQuyet($donvi,$taikhoan,NULL);
        $data['phongph_daxuly']     = $this->Mvanban_caphai->demDSVB_Phong_PH_ChuaGiaiQuyet($donvi,$taikhoan,NULL);

        $data['vanbanchoduyet']     = $this->Mvanban_caphai->demDSVB_ChoDuyet($taikhoan);

        $data['vanbanph_choxuly'] = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,NULL,1,1,NULL,2);
        $data['vanbanph_daxuly']  = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,NULL,2,1,NULL,2);

        $data['phongct_choxuly_ppcv']    = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,2,1,1,NULL,1);
        $data['phongct_daxuly_ppcv']     = $this->Mvanban_caphai->demDSVB_Phong_ChuaGiaiQuyet($donvi,$taikhoan,1);
        
        $data['phongph_choxuly_ppcv']    = $this->Mvanban_caphai->demDSVB_Phong_PH($donvi,$taikhoan,3,1,1,NULL,1);
        $data['phongph_daxuly_ppcv']     = $this->Mvanban_caphai->demDSVB_Phong_PH_ChuaGiaiQuyet($donvi,$taikhoan,1);

        $data['vanbanphongchutri']      = $this->Mvanban_caphai->demDSVB_Phong($donvi,$phong_caphai,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['vanbanphongphoihop']     = $this->Mvanban_caphai->demDSVB_Phong($donvi,$phong_caphai,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['vanbantongthe_caphai']   = $this->Mvanban_caphai->demVBDen_caphai($donvi,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        // văn bản đi cấp hai
        $nam                          = date('Y');
        $data['vanbandi_caphai']      = $this->Mvanbandi_caphai->demDSVB_ChoSo(2,$donvi,$nam,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['vanbandichoso_caphai'] = $this->Mvanbandi_caphai->demDSVB_ChoSo(1,$donvi,$nam,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $data['vanbandiphong_caphai'] = $this->Mvanbandi_caphai->demDSVB_ChoSo(2,$donvi,$nam,NULL,NULL,$phong_caphai,NULL,NULL,NULL,NULL);
		
		*/
		
        // yêu cầu đề xuất phối hợp 20/04/2018
        $data['dexuatphoihop']     = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,1);
        $data['dexuatphoihopxuly'] = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,2);
        // đầu việc chờ xử gia hạn ngày 12/04/2018
        /*
		if($trangthai_tk==7)
        {
            $data['giahanchoduyet'] = $this->Mchuyennhan->demDX_Han($taikhoan);
            $data['giahandaduyet']  = $this->Mchuyennhan->demDaDX_Han($taikhoan);
        }
        else
        {
            $data['giahanchoxuly'] = $this->Mchuyennhan->demDX_Han_TP($taikhoan);
            $data['giahandaxuly']  = $this->Mchuyennhan->demDaDX_Han_TP($taikhoan);
        }
		*/
        // Văn bản đi chỉnh sửa ngày 03/04/2018
        $data['vanbandichoxuly'] = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,1);
        //$data['vanbandidaxuly']  = $this->Mvanbandi->demVBDi_DaXuLy($taikhoan,2);
		//$data['vanbandiusertao'] = $this->Mvanbandi->demVBDi_UserTao($taikhoan,2);
        //$data['vanbandi_phong']  = $this->Mvanbandi->demDSVB_Phong($phongban,1);
        //$data['giaymoidi_phong'] = $this->Mvanbandi->demDSVB_Phong($phongban,10);
        $data['vanbanditrinhky'] = $this->Mvanbandi->demVBDi_DaTrinhKy($taikhoan);
		$data['chidaodaxem']	 = $this->Mvanbanchoxuly->demChiDaoDaXem($this->_session['PK_iMaCB'], NULL, NULL);
		$data['dschidao']		 = $this->Mvanbanchoxuly->demDSChiDao($this->_session['PK_iMaCB'],$this->_session['sHoTen'], NULL, NULL);
		$data['ykiendaxem']		 = $this->Mvanbanchoxuly->demYKienDaXem($this->_session['PK_iMaCB'], NULL, NULL);
		$data['dsykien']		 = $this->Mvanbanchoxuly->demDSYKien($this->_session['PK_iMaCB'],$this->_session['sHoTen'], NULL, NULL);
		
		$data['chidaodaxemgm']	 = $this->Mgiaymoichoxuly->demChiDaoDaXem($this->_session['PK_iMaCB'], NULL, NULL);
		$data['dschidaogm']		 = $this->Mgiaymoichoxuly->demDSChiDao($this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB'],$this->_session['sHoTen'], NULL, NULL);
		//echo $data['dschidaogm']	;
		//echo $data['chidaodaxemgm'];
		
		$data['ykiendaxemgm']		 = $this->Mgiaymoichoxuly->demYKienDaXem($this->_session['PK_iMaCB'], NULL, NULL);
		$data['dsykiengm']		 = $this->Mgiaymoichoxuly->demDSYKien($this->_session['PK_iMaCB'],$this->_session['sHoTen'], NULL, NULL);
		
		$data['chidaodaxemvbdi']	 = $this->Mvanbandi->demChiDaoDaXem($this->_session['PK_iMaCB'], NULL, NULL);		
		$data['dschidaovbdi']		 = $this->Mvanbandi->demDSChiDao($this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB'],$this->_session['sHoTen'], NULL, NULL);
		$data['ykiendaxemvbdi']		 = $this->Mvanbandi->demYKienDaXem($this->_session['PK_iMaCB'], NULL, NULL);
		$data['dsykienvbdi']		 = $this->Mvanbandi->demDSYKien($this->_session['PK_iMaCB'],$this->_session['sHoTen'], NULL, NULL);
		
		$tenpb = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_phongban');
        $data['lichhopphong']        = count($this->Mvanbanden->layLichHopPhongCT($tenpb[0]['sTenPB'], NULL));
		
		$data['giaymoidahopchuaxuly'] = $this->Mgiaymoi->demGiayMoiDaHopChuaHT($taikhoan);
        // văn bản đi để xem
        $data['vanbandi_xem']    = $this->Mvanbandi->demLDSo_Xem($taikhoan);
		$data['daphchuaduyet']    = $this->Mvanbandi->demVBDi_daphathanh_chuaduyet($this->_session['PK_iMaCB'],$this->_session['iQuyenHan_DHNB'],NULL);
		$temp['data']        = $data;
		$temp['template']    = 'Vwelcome';
		$this->load->view('layout_admin/layout', $temp);
	}
}
