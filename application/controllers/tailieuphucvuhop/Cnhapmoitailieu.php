<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhapmoitailieu extends MY_Controller {

	protected $_thongtin;
	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('tailieuphucvuhop/Mtailieuphucvuhop');
    }
    public function index()
    {
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		$data['ma']			= _get('ma');
		if(_post('luudulieu'))
		{
			if(!empty($data['ma']))
			{
				$data['content'] = $this->capnhatDuLieu($data['ma'],$taikhoan);
			}	
			else{
				$data['content'] = $this->themDuLieu($taikhoan);
			}
		}
		$data['linhvuc']	= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_linhvuc');
		$data['loaitailieu']= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaitailieu');
		$data['lanhdao']	= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['phongban']	= $this->Mtailieuphucvuhop->layPhongBanDuThao();
		if(!empty($data['ma']))
		{
			$data['thongtin']   = $this->Mdanhmuc->layDuLieu('PK_iMaTaiLieu',$data['ma'],'tbl_tailieuphucvuhop');
		}
		else{
			$data['thongtin']	= $this->Mtailieuphucvuhop->layTTCuoi();
		}
		$data['title']		= 'Nhập mới tài liệu phục vụ kỳ họp';
		$temp['data']		= $data;
		$temp['template']	= 'tailieuphucvuhop/Vnhapmoitailieu';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function capnhatDuLieu($ma,$taikhoan)
    {
    	$phoihop = _post('phongphoihop');
    	$data=array(
			'sKhoa'				=> _post('khoa'),
			'sNhiemKy'			=> _post('nhiemky'),
			'sKyHop'			=> _post('kyhop'),
			'iTrangThaiKyHop'	=> _post('dinhkybatthuong'),
			'sTenPhuLuc'		=> _post('tenphuluc'),
			'sNoiDung'			=> _post('noidung'),
			'iTrangThaiTaiLieu'	=> (_post('thuongxuyen'))?_post('thuongxuyen'):0,
			'FK_iMaLV'			=> (_post('linhvuc'))?_post('linhvuc'):0,
			'FK_iMaPhong_CT'	=> _post('phongchutri'),
			'FK_iMaPhong_PH'	=> ($phoihop)?implode(',',$phoihop):'',
			'FK_iMaCB_LanhDao'	=> (_post('lanhdao'))?_post('lanhdao'):0,
			'FK_iMaCB_Nhap'		=> $taikhoan,
			'sNgayNhap'			=> date('Y-m-d H:i:s'),
			'FK_iMaLTL'			=> _post('loaitailieu')
    	);
    	$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaTaiLieu',$ma,'tbl_tailieuphucvuhop',$data);
    	if($kiemtra>0)
    	{
    		$this->themFile($kiemtra);
    		redirect('danhsachtailieu');
    	}
    }
    public function themDuLieu($taikhoan)
    {
    	$phoihop = _post('phongphoihop');
    	$data=array(
			'sKhoa'				=> _post('khoa'),
			'sNhiemKy'			=> _post('nhiemky'),
			'sKyHop'			=> _post('kyhop'),
			'iTrangThaiKyHop'	=> _post('dinhkybatthuong'),
			'sTenPhuLuc'		=> _post('tenphuluc'),
			'sNoiDung'			=> _post('noidung'),
			'iTrangThaiTaiLieu'	=> (_post('thuongxuyen'))?_post('thuongxuyen'):0,
			'FK_iMaLV'			=> (_post('linhvuc'))?_post('linhvuc'):0,
			'FK_iMaPhong_CT'	=> _post('phongchutri'),
			'FK_iMaPhong_PH'	=> ($phoihop)?implode(',',$phoihop):'',
			'FK_iMaCB_LanhDao'	=> (_post('lanhdao'))?_post('lanhdao'):0,
			'FK_iMaCB_Nhap'		=> $taikhoan,
			'sNgayNhap'			=> date('Y-m-d H:i:s'),
			'FK_iMaLTL'			=> _post('loaitailieu')
    	);
    	$kiemtra = $this->Mdanhmuc->themDuLieu2('tbl_tailieuphucvuhop',$data);
    	if($kiemtra>0)
    	{
    		$this->themFile($kiemtra);
    		return messagebox('Thêm tài liệu phục vụ họp thành công!','info');
    	}
    }
    public function themFile($mavanban)
	{
		$taikhoan	= $this->_session['PK_iMaCB'];
		$name		= $_FILES['files']['name'];
		if(!empty($name[0]))
		{
			$this->upload('tailieu');
			$files = array();
			foreach ($name as $key => $value) {
				$files[] = array(
					'FK_iMaTaiLieu'	=> $mavanban,
					'FK_iMaCB'		=> $taikhoan,
					'sNgayNhap'		=> date('Y-m-d H:i:s'),
					'sTenFile'		=> $value,
					'sDuongDan'		=> 'tailieu/'.time().clear($value),
					'CT_PH'			=> 1,
					'iTrangThai'	=> 1
					);
			}
			$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_file_tailieu',$files);
			if($kiemtra>0)
			{
				$this->Mdanhmuc->setDuLieu('PK_iMaTaiLieu',$mavanban,'tbl_tailieuphucvuhop','iFile',2);				
			}
		}
	}
	public function upload($dir)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = time().clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cnhapmoitailieu.php */
/* Location: ./application/controllers/tailieuphucvuhop/Cnhapmoitailieu.php */