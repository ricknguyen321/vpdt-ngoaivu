<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachtailieu extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('tailieuphucvuhop/Mtailieuphucvuhop');
        $this->load->library('pagination');
    }
    public function index()
    {
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		if(_post('luudulieu'))
		{
			$data['content'] = $this->themDuLieu($taikhoan);
		}
		$dulieu						= $this->layDSTaiLieu();
		$data['dulieu']				= $dulieu['dulieu'];
		$data['khoa']				= $dulieu['khoa'];
		$data['nhiemky']			= $dulieu['nhiemky'];
		$data['kyhop']				= $dulieu['kyhop'];
		$data['dinhkybatthuong']	= $dulieu['dinhkybatthuong'];
		$data['tenphuluc']			= $dulieu['tenphuluc'];
		$data['trangthaitailieu']	= $dulieu['trangthaitailieu'];
		$data['noidungtailieu']		= $dulieu['noidungtailieu'];
		$data['linhvuc']			= $dulieu['linhvuc'];
		$data['lanhdao']			= $dulieu['lanhdao'];
		$data['phongchutri']		= $dulieu['phongchutri'];
		$data['loaitailieu']		= $dulieu['loaitailieu'];
		$data['phantrang']			= $dulieu['phantrang'];
		if(!empty($data['dulieu']))
		{
			foreach ($data['dulieu'] as $key => $value) {
				$data['dulieu'][$key]['dsfile'] = $this->Mtailieuphucvuhop->layFile($value['PK_iMaTaiLieu']);
			}
		}
		// pr($data['dulieu']);
		$data['loaitailieu']	= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaitailieu');
		$data['linhvuc']		= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_linhvuc');
		$data['lanhdao']		= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($data['lanhdao'] as $key => $value) {
			$data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$data['phongban']		= $this->Mtailieuphucvuhop->layPhongBanDuThao();
		foreach ($data['phongban'] as $key => $value) {
			$data['mangphong'][$value['PK_iMaPB']] = $value['sVietTat'];
		}
		$data['title']			= 'Nhập mới tài liệu phục vụ kỳ họp';
		$temp['data']			= $data;
		$temp['template']		= 'tailieuphucvuhop/Vdanhsachtailieu';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function layDSTaiLieu() 
	{
		$data['khoa']				= _get('khoa');
		$data['nhiemky']			= _get('nhiemky');
		$data['kyhop']				= _get('kyhop');
		$data['dinhkybatthuong']	= _get('dinhkybatthuong');
		$data['tenphuluc']			= _get('tenphuluc');
		$data['trangthaitailieu']	= _get('trangthaitailieu');
		$data['noidungtailieu']		= _get('noidungtailieu');
		$data['linhvuc']			= _get('linhvuc');
		$data['lanhdao']			= _get('lanhdao');
		$data['phongchutri']		= _get('phongchutri');
		$data['loaitailieu']		= _get('loaitailieu');

		$config['base_url']         = base_url().'danhsachtailieu?khoa='.$data['khoa'].'&nhiemky='.$data['nhiemky'].'&kyhop='.$data['kyhop'].'&dinhkybatthuong='.$data['dinhkybatthuong'].'&tenphuluc='.$data['tenphuluc'].'&trangthaitailieu='.$data['trangthaitailieu'].'&noidungtailieu='.$data['noidungtailieu'].'&linhvuc='.$data['linhvuc'].'&lanhdao='.$data['lanhdao'].'&phongchutri='.$data['phongchutri'].'&loaitailieu='.$data['loaitailieu'];
		$config['total_rows']           = $this->Mtailieuphucvuhop->demDSTaiLieu($data['khoa'],$data['nhiemky'],$data['kyhop'],$data['dinhkybatthuong'],$data['tenphuluc'],$data['trangthaitailieu'],$data['noidungtailieu'],$data['linhvuc'],$data['lanhdao'],$data['phongchutri'],$data['loaitailieu']);
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'danhsachtailieu');
      	}
		$data['dulieu']		= $this->Mtailieuphucvuhop->layDSTaiLieu($data['khoa'],$data['nhiemky'],$data['kyhop'],$data['dinhkybatthuong'],$data['tenphuluc'],$data['trangthaitailieu'],$data['noidungtailieu'],$data['linhvuc'],$data['lanhdao'],$data['phongchutri'],$data['loaitailieu'],$config['per_page'],$data['page']);
		$data['phantrang']	= $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdanhsachtailieu.php */
/* Location: ./application/controllers/tailieuphucvuhop/Cdanhsachtailieu.php */