<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csapxeptailieuhop extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('tailieuphucvuhop/Mtailieuphucvuhop');
    }
    public function index()
    {
		$quyen					= $this->_session['iQuyenHan_DHNB'];
		$taikhoan				= $this->_session['PK_iMaCB'];
		$data['khoa']			= $this->Mtailieuphucvuhop->layDL('sKhoa');
		$data['nhiemky']		= $this->Mtailieuphucvuhop->layDL('sNhiemKy');
		$data['kyhop']			= $this->Mtailieuphucvuhop->layDL('sKyHop');
		if(_post('capnhat'))
		{
			$ma		= _post('ma');
			$thutu	= _post('thutu');
			if(!empty($ma))
			{
				foreach ($ma as $key => $value) {
					$data_capnhat[] =array(
						'PK_iMaTaiLieu'	=> $value,
						'stt'			=> $thutu[$key]
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatNhieuDuLieu('tbl_tailieuphucvuhop',$data_capnhat,'PK_iMaTaiLieu');
				if($kiemtra>0)
				{
					$data['content'] = messagebox('Sắp xếp thứ tự thành công!','info');
				}
			}
		}

		$data['get_khoa']		= _get('khoa');
		$data['get_nhiemky']	= _get('nhiemky');
		$data['get_kyhop']		= _get('kyhop');
		if(empty($data['get_khoa'])&&empty($data['get_nhiemky'])&&empty($data['get_kyhop']))
		{
			$data['dulieu'] = '';
		}
		else
		{
			$data['dulieu']			= $this->Mtailieuphucvuhop->layTTTaiLieu($data['get_khoa'],$data['get_nhiemky'],$data['get_kyhop']);
		}
		$data['title']		= 'Sắp xếp tài liệu phục vụ họp';
		$temp['data']		= $data;
		$temp['template']	= 'tailieuphucvuhop/Vsapxeptailieuhop';
		$this->load->view('layout_admin/layout',$temp);
    }

}

/* End of file Csapxeptailieuhop.php */
/* Location: ./application/controllers/tailieuphucvuhop/Csapxeptailieuhop.php */