<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhapcuoctiep extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$ma = _get('id');
		$data['ma'] = $ma;
		if ($ma > 0) {
			
			$data['tailieu'] = $this->Mdanhmuc->layDuLieu('FK_iMaCuocTiep',$ma,'tbl_files_cuoctiep');

			if(_post('xoa'))
			{
				$data['content'] = $this->xoaDuLieu($ma);
			}
		}
		if(!empty($ma))
		{			
			$data['content'] = $this->capnhatDuLieu($ma);						
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin']	  = $this->_thongtin;
		
		$data['phongban'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB','','tbl_phongban');	

		$data['dslds']	  = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',78,'tbl_canbo');		
		
		$data['dscbls']	  = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',77,'tbl_canbo');
		
		$data['dsldtpall'] 	= $this->Mdanhmuc->layDuLieu('id',NULL,'tbl_ldtp');
		$data['dsldtp'] 	= $this->Mdanhmuc->layDuLieu('iexpired',2,'tbl_ldtp');
		$data['dscoquan'] 	= $this->Mdanhmuc->laycoquanchutri();
		
		$data['mangldtp']= explode(', ',$this->_thongtin[0]['ldtiep']);
		
		$data['mangtp']= explode(', ',$this->_thongtin[0]['tangpham']);
		
		foreach ($data['mangtp'] as $key => $value) {
			if ($value != 'Khuê Văn Các' && $value != 'Đầu Rồng' && $value != 'Bình trà Hanoia') 
			{
				$data['tpkhac'] = $value;
			}
		}
		
		$giomoi = $this->_thongtin[0]['sGioMoi'];
	  
		$data['title']        = 'Nhập mới thông tin cuộc tiếp';
		$temp['data']         = $data;
		$temp['template']     = 'cuoctiep/Vnhapcuoctiep';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_cuoctiep');
		if(_post('luudulieu'))
		{
			$ldt = _post('ldtiep');		
			if(!empty($ldt))
			{
				$ldt= implode(', ', $ldt);
			}
			$arr=array();
			if (_post('tangpham1')) array_push($arr,_post('tangpham1'));
			if (_post('tangpham2')) array_push($arr,_post('tangpham2'));
			if (_post('tangpham3')) array_push($arr,_post('tangpham3'));
			if (_post('tangpham')) array_push($arr,_post('tangpham'));
			
			$tp = implode(', ', $arr);
			$tpub = _post('tangphamub');
			//return messagebox($tp,'danger');
			$data=array(
				'tendoan'		   		=> _post('tendoan'),
				'truongdoan'        	=> _post('truongdoan'),
				'quoc_gia'        	=> _post('quoc_gia'),
				'coquanct'        	=> _post('coquanct'),
				'ldtiep'     			=> $ldt,
				'ldso'         			=> _post('ldso'),
				'vbdi'         			=> _post('vbdi'),
				'diadiem'         			=> _post('diadiem'),
				'noidung'      			=> nl2br(_post('noidung')),
				'ngaytiep'    			=> date_insert(_post('ngaytiep')),
				'cvth'    				=> _post('cvth'),
				'tangpham_cq'    		=> ($tp)?1:(($tpub)?2:NULL),
				'tangpham'    			=> ($tp)?$tp:(($tpub)?$tpub:NULL),
				'phiendich'    			=> _post('phiendich')?_post('phiendich'):(_post('phiendichpb')?_post('phiendichpb'):NULL),	
				'note'    				=> _post('note')
			);
				
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_cuoctiep',$data);
			$name    = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('doc_cuoctiep_'.date('Y'), $time);
				$files = array();
				foreach ($name as $key => $value) {
					$val = clear($value);
					$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaCuocTiep' => $ma,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_cuoctiep_'.date('Y').'/tl_ct_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
					);
				}
				$kiemtrafile=$this->Mdanhmuc->themNhieuDuLieu('tbl_files_cuoctiep',$files);
				
			}
			if(empty(_post('tendoan')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập tên đoàn!','danger');
			}
						
			redirect('cuoctiep?id='.$ma);
		}
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_cuoctiep');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		redirect('nhapcuoctiep?id='.$mavanban);
		
	}
	public function themDuLieu()
	{
		$time = time();
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if(_post('luudulieu'))
		{
			$ldt = _post('ldtiep');		
			if(!empty($ldt))
			{
				$ldt= implode(', ', $ldt);
			}
			
			$arr=array();
			if (_post('tangpham1')) array_push($arr,_post('tangpham1'));
			if (_post('tangpham2')) array_push($arr,_post('tangpham2'));
			if (_post('tangpham3')) array_push($arr,_post('tangpham3'));
			if (_post('tangpham')) array_push($arr,_post('tangpham'));
			
			$tp = implode(', ', $arr);
			$tpub = _post('tangphamub');
			
			
			$data=array(
				'tendoan'		   		=> _post('tendoan'),
				'truongdoan'        	=> _post('truongdoan'),
				'quoc_gia'        	=> _post('quoc_gia'),
				'coquanct'        	=> _post('coquanct'),
				'ldtiep'     			=> $ldt,
				'ldso'         			=> _post('ldso'),
				'vbdi'         			=> _post('vbdi'),
				'diadiem'         			=> _post('diadiem'),
				'noidung'      			=> nl2br(_post('noidung')),
				'ngaytiep'    			=> date_insert(_post('ngaytiep')),
				'cvth'    				=> _post('cvth'),
				'tangpham_cq'    		=> ($tp)?1:(($tpub)?2:NULL),
				'tangpham'    			=> ($tp)?$tp:(($tpub)?$tpub:NULL),
				'phiendich'    			=> _post('phiendich')?_post('phiendich'):(_post('phiendichpb')?_post('phiendichpb'):NULL),
				'FK_iMaCBNhap'    		=> $this->_session['PK_iMaCB'],		
				'note'    				=> _post('note'),
				'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
			);
			
			
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_cuoctiep',$data);
			
			$name = $_FILES['files']['name'];
			
			
			if($id_insert>0)
			{
				
				$name = $_FILES['files']['name'];
				if(!empty($name[0]))
				{
					$this->upload('doc_cuoctiep_'.date('Y'), $time);
					$files = array();
					foreach ($name as $key => $value) {
						$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaCuocTiep' => $id_insert,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_cuoctiep_'.date('Y').'/tl_ct_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB'],
						);
					}
					$kiemtrafile = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_cuoctiep',$files);
				
				}
				echo '<script language="javascript">';
				echo 'alert(Thêm thông tin cuộc tiếp THÀNH CÔNG)';  //not showing an alert box.
				echo '</script>';
				redirect('dscuoctiep');	
			}
			else{
				return messagebox('Thêm thông tin cuộc tiếp thất bại','danger');
			}
		}
	}
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'tl_ct_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */