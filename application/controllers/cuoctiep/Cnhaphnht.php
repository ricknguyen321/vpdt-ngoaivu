<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhaphnht extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$ma = _get('id');
		$data['ma'] = $ma;
		if ($ma > 0) {
			
			$data['tailieu'] = $this->Mdanhmuc->layDuLieu('FK_iMahnht',$ma,'tbl_files_hnht');

			if(_post('xoa'))
			{
				$data['content'] = $this->xoaDuLieu($ma);
			}
		}
		if(!empty($ma))
		{			
			$data['content'] = $this->capnhatDuLieu($ma);						
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin']	  = $this->_thongtin;
		
		$data['phongban'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB','','tbl_phongban');	

		$data['dslds']	  = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',78,'tbl_canbo');		
		
		$data['dscbls']	  = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',77,'tbl_canbo');
		
		$giomoi = $this->_thongtin[0]['sGioMoi'];
	  
		$data['title']        = 'Nhập mới thông tin hội nghị hội thảo';
		$temp['data']         = $data;
		$temp['template']     = 'cuoctiep/Vnhaphnht';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_hnht');
		if(_post('luudulieu'))
		{
			
			//return messagebox($tp,'danger');
			$data=array(
				'ten_hnht'		   		=> _post('ten_hnht'),
				'coquan_th'       	 	=> _post('coquan_th'),
				'noidung'        		=> _post('noidung'),
				'thoi_gian'        		=> date_insert(_post('thoi_gian')),
				'diadiem'         		=> _post('diadiem'),
				'cq_capphep'         	=> _post('cq_capphep'),
				'sldb1'      			=> _post('sldb1'),
				'sldb2'      			=> _post('sldb2'),
				'sldb3'      			=> _post('sldb3'),
				'sldb4'      			=> _post('sldb4'),
				'baocao'    			=> _post('baocao'),
				'kinhphi'    			=> _post('kinhphi'),
				'note'    				=> _post('note')
			);
			if(empty(_post('ten_hnht')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập tên hội nghị hội thảo!','danger');
			}
				
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_hnht',$data);
			$name    = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('doc_hnht_'.date('Y'), $time);
				$files = array();
				foreach ($name as $key => $value) {
					$val = clear($value);
					$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMahnht' => $ma,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_hnht_'.date('Y').'/tl_hnht_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
					);
				}
				$kiemtrafile=$this->Mdanhmuc->themNhieuDuLieu('tbl_files_hnht',$files);
				
			}			
						
			redirect('hnht?id='.$ma);
		}
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_hnht');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		redirect('nhaphnht?id='.$mavanban);
		
	}
	public function themDuLieu()
	{
		$time = time();
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if(_post('luudulieu'))
		{			
			$data=array(
				'ten_hnht'		   		=> _post('ten_hnht'),
				'coquan_th'       	 	=> _post('coquan_th'),
				'noidung'        		=> _post('noidung'),
				'thoi_gian'        		=> date_insert(_post('thoi_gian')),
				'diadiem'         		=> _post('diadiem'),
				'cq_capphep'         	=> _post('cq_capphep'),
				'sldb1'      			=> _post('sldb1'),
				'sldb2'      			=> _post('sldb2'),
				'sldb3'      			=> _post('sldb3'),
				'sldb4'      			=> _post('sldb4'),
				'baocao'    			=> _post('baocao'),
				'kinhphi'    			=> _post('kinhphi'),
				'FK_iMaCBNhap'    		=> $this->_session['PK_iMaCB'],		
				'note'    				=> _post('note'),
				'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
			);			
			
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_hnht',$data);			
			$name = $_FILES['files']['name'];
			
			if($id_insert>0)
			{				
				$name = $_FILES['files']['name'];
				if(!empty($name[0]))
				{
					$this->upload('doc_hnht_'.date('Y'), $time);
					$files = array();
					foreach ($name as $key => $value) {
						$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMahnht' => $id_insert,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_hnht_'.date('Y').'/tl_hnht_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB'],
						);
					}
					$kiemtrafile = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_hnht',$files);
				
				}
				redirect('dshnht');	
			}
			else{
				return messagebox('Thêm thông tin hội nghị hội thảo thất bại','danger');
			}
		}
	}
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'tl_hnht_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */