<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ccuoctiep extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		$ma = _get('id');
		if ($ma > 0) {			
			$data['tailieu'] = $this->Mdanhmuc->layDuLieu('FK_iMaCuocTiep',$ma,'tbl_files_cuoctiep');
			if(_post('xoa'))
			{
				$data['content'] = $this->xoaDuLieu($ma);
			}
		}
		$this->_thongtin 	= $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_cuoctiep');
		$data['thongtin']	= $this->_thongtin;
		$data['ldtp']	= $this->Mdanhmuc->layDuLieu('id',$this->_thongtin[0]['ldtiep'],'tbl_ldtp');
		$data['title']        = 'Thông tin cuộc tiếp của lãnh đạo'; 
		$temp['data']         = $data;
		$temp['template']     = 'cuoctiep/Vcuoctiep';
		$this->load->view('layout_admin/layout',$temp); 
	}
	
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_cuoctiep');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		redirect('cuoctiep?id='.$mavanban);
		
	}
	
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */