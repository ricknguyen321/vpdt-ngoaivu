<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdshnht extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('cuoctiep/Mcuoctiep');
		$this->load->library('pagination');
	}
	public function index()
	{
		//ricknguyen321 yêu cầu toàn bộ cbcc thay đổi mật khâu theo đúng quy tắc.
		$pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*\W).{8,16}$/";
		$result = preg_match ($pattern, $this->_session['sBanRo']);
			
		if ( !$result ) {
			echo '<script language="javascript">';
			echo 'alert("Weak password. Please change your password!")';
			echo '</script>';
			redirect(base_url().'doimatkhaucn');
		}
		
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		$vanban             = $this->DSVanBan();
		$data['dscqcp'] = $this->Mcuoctiep->layDSCQCP();
		$data['dsdvtc'] = $this->Mcuoctiep->layDSDVTC();
		$data['dshnht']	= $vanban['info'];
		$data['cq']  = $vanban['cq'];
		$data['dvtc']  = $vanban['dvtc'];
		$data['tungay']  = $vanban['tungay'];
		$data['denngay']  = $vanban['denngay'];
		$data['count'] = $vanban['count'];		
		$data['tonghop'] 	   = $vanban['tonghop'];		
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Danh sách hội nghị, hội thảo';
		$data['page']    = _get('page');
		$temp['data']     = $data;
		if($this->input->get('kx') == 'word'){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=Danh sách hội nghị hội thảo.doc");
			
            $temp['template'] = 'cuoctiep/Vdshnht_word';
            $this->load->view('layout_admin/layout_word',$temp);
        } else {
			$temp['template'] = 'cuoctiep/Vdshnht';
			$this->load->view('layout_admin/layout',$temp);
		}
	}
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMahnht',$ma,'tbl_files_hnht');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMahnht',$ma,'tbl_files_hnht');
		}
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('id',$ma,'tbl_hnht');
		if($kiemtra>0)
		{
			return messagebox('Xóa thông tin cuộc tiếp thành công!','info');
		}
	}
	
	public function DSVanBan() 
	{
		
		$tonghop	 = _get('tonghop');
		$dvtc	 = _get('dvtcth');
		$cq	 = _get('cqcp');
		$tungay   = _get('tungay');
		$denngay   = _get('denngay');
		if(!empty($tungay))
		{
			$tungay = date_insert($tungay);
		}
		else{
			$tungay = '';
		}
		if(!empty($denngay))
		{
			$denngay = date_insert($denngay);
		}
		else{
			$denngay = '';
		}
		
		$config['base_url']             = base_url().'dshnht?tonghop='.$tonghop.'&cq='.$cq.'&dvtcth='.$dvtc.'&tungay='.$tungay.'&denngay='.$denngay;
		
		$config['total_rows']           = $this->Mcuoctiep->demDShnht($tonghop,$dvtc,$tungay,$denngay,$cq);
	
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dshnht');
      	}
		
		$data['tonghop']    = $tonghop;
		$data['cq']    = $cq;
		$data['dvtc']    = $dvtc;
		$data['tungay']    = _get('tungay');
		$data['denngay']    = _get('denngay');
		$data['count']      = $config['total_rows'];
		$data['info']       = $this->Mcuoctiep->layDShnht($config['per_page'], $data['page'],$tonghop,$dvtc,$tungay,$denngay,$cq);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdsvanban.php */
/* Location: ./application/controllers/vanban/Cdsvanban.php */