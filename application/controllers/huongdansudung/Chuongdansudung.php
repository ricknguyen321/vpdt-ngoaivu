<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chuongdansudung extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Viết hướng dẫn sử dụng';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
			$data['quyenhan'] = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_quyen');
			$data['thongtin'] = $this->_thongtin;
			$data['dsdulieu'] = $this->Mdanhmuc->layChucNangHeThong();
			$temp['data']     = $data;
			$temp['template'] = 'huongdansudung/Vhuongdansudung';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenChucNang'  => _post('chucnang'),
				'sHuongDan'     => $this->input->post('huongdan',false),
				'FK_iMaQuyen'   => _post('quyenhan'),
				'iTrangThai'    => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_chucnang',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm hướng dẫn thành công','info');
			}
			else{
				return messagebox('Thêm hướng dẫn thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaChucNang',$ma,'tbl_chucnang');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenChucNang'  => _post('chucnang'),
				'sHuongDan'     => $this->input->post('huongdan',false),
				'FK_iMaQuyen'   => _post('quyenhan'),
				'iTrangThai'    => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaChucNang',$ma,'tbl_chucnang',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaChucNang',$ma,'tbl_chucnang');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật hướng dẫn thành công','info');
			}
			else{
				return messagebox('Cập nhật hướng dẫn thất bại','danger');
			}
		}
	}

}

/* End of file Chuongdansudung.php */
/* Location: ./application/controllers/Chuongdansudung.php */