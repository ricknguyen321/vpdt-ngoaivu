<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemhuongdansudung extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Xem hướng dẫn sử dụng';
		$data['quyenhan'] = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_quyen');
		$data['dsdulieu'] = $this->Mdanhmuc->layChucNangHeThong();
		$temp['data']     = $data;
		$temp['template'] = 'huongdansudung/Vxemhuongdansudung';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cxemhuongdansudung.php */
/* Location: ./application/controllers/huongdansudung/Cxemhuongdansudung.php */