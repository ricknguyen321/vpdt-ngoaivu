<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cduyetnghiphep extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('nghiphep/Mnghiphep');
		$this->load->library('pagination');
	}
	public function index()
	{
		//ricknguyen321 yêu cầu toàn bộ cbcc thay đổi mật khâu theo đúng quy tắc.
		$pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*\W).{8,16}$/";
		$result = preg_match ($pattern, $this->_session['sBanRo']);
			
		if ( !$result ) {
			echo '<script language="javascript">';
			echo 'alert("Weak password. Please change your password!")';
			echo '</script>';
			redirect(base_url().'doimatkhaucn');
		}
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		if(_post('tuchoi'))
		{
			$data['content'] = $this->tuchoi();
		}
		if(_post('duyet'))
		{
			$data['content'] = $this->duyet();
		}
		$data['dsldvp']       = $this->Mnghiphep->layldvp();
		$vanban             = $this->DSVanBan();
		$data['dsnghiphep']	= $vanban['info'];
		$data['count'] = $vanban['count'];		
		$data['tonghop'] 	   = $vanban['tonghop'];		
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Danh sách cán bộ xin nghỉ phép';
		$data['page']    = _get('page');
		$temp['data']     = $data;
		$temp['template'] = 'nghiphep/Vduyetnghiphep';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu()
	{
		$ma = _post('xoa');

		$kiemtra = $this->Mdanhmuc->xoaDuLieu('id',$ma,'tbl_nghi_phep');
		if($kiemtra>0)
		{
			return messagebox('Xóa phê duyệt thành công!','info');
		}
	}
	public function duyet()
	{
		$ma = _post('duyet');
		$donnp	  = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_nghi_phep');
		$cbtao	  = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$donnp[0]['FK_iMaCB'],'tbl_canbo');
		$gd 		= $this->Mnghiphep->laygd();
		if ($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 7) {
			if ($donnp[0]['so_ngay_nghi'] == 0) {
				$data=array(
					'yk_tp'    				=> '<b>'.nl2br((!empty(_post('ykien_tp'.$ma)))?_post('ykien_tp'.$ma):'Duyệt ./.').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',				
					'tp_duyet'        		=> 1,
					'bgd_duyet'        		=> 1,
					'cb_giu'    			=> $donnp[0]['FK_iMaCB']
				);
				$this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_nghi_phep',$data);		
				redirect('duyetnghiphep');
			}
			$data=array(
				'yk_tp'    				=> '<b>'.nl2br((!empty(_post('ykien_tp'.$ma)))?_post('ykien_tp'.$ma):'Kính trình BGĐ phê duyệt./.').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',				
				'tp_duyet'        		=> 1,
				'cb_giu'    			=> ($this->_session['FK_iMaPhongHD'] == 11)?$gd[0]['PK_iMaCB']:_post('ldduyet')
			);
		} elseif ($this->_session['iQuyenHan_DHNB'] == 3) {
			if ($donnp[0]['so_ngay_nghi'] == 0) {
				$data=array(
					'yk_ldvp'    				=> '<b>'.nl2br((!empty(_post('ykien_tp'.$ma)))?_post('ykien_tp'.$ma):'Duyệt ./.').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',				
					'ldvp_duyet'        		=> 1,
					'bgd_duyet'        		=> 1,
					'cb_giu'    			=> $donnp[0]['FK_iMaCB']
				);
				$this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_nghi_phep',$data);		
				redirect('duyetnghiphep');
			}
			$data=array(
				'yk_ldvp'    				=> '<b>'.nl2br((!empty(_post('ykien_tp'.$ma)))?_post('ykien_tp'.$ma):'Kính trình BGĐ phê duyệt./.').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',
				'ldvp_duyet'        		=> 1,
				'cb_giu'					=> $gd[0]['PK_iMaCB']
			);
		} elseif ($this->_session['iQuyenHan_DHNB'] == 4 || $this->_session['iQuyenHan_DHNB'] == 5) {
			$data=array(
				'yk_bgd'    			=> '<b>'.nl2br((!empty(_post('ykien_bgd'.$ma)))?_post('ykien_bgd'.$ma):'Duyệt').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',
				'bgd_duyet'        		=> 1,
				'cb_giu'    			=> $donnp[0]['FK_iMaCB']
			);
			if ($donnp[0]['loai_phep'] == 'Nghỉ phép') {
				// cập nhật số ngày phép còn lại
				$data2=array(
					'ngay_phep'    			=> $cbtao[0]['ngay_phep'] - $donnp[0]['so_ngay_nghi']
				);
				$this->Mdanhmuc->capnhatDuLieu('PK_iMaCB',$donnp[0]['FK_iMaCB'],'tbl_canbo',$data2);
				}
			$this->guittduyet();
		}
		$this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_nghi_phep',$data);		
		redirect('duyetnghiphep');
	}
	
	public function tuchoi()
	{
		$ma = _post('tuchoi');
		$donnp	  = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_nghi_phep');
		if ($this->_session['iQuyenHan_DHNB'] == 6 || $this->_session['iQuyenHan_DHNB'] == 7) {
			$data=array(
				'yk_tp'    				=> '<b style="color:red">'.nl2br((!empty(_post('ykien_tp'.$ma)))?_post('ykien_tp'.$ma):'Từ chối').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',
				'tp_duyet'        		=> 2,
				'cb_giu'    			=> $donnp[0]['FK_iMaCB']
			);
		} elseif ($this->_session['iQuyenHan_DHNB'] == 3) {
			$data=array(
				'yk_ldvp'    				=> '<b style="color:red">'.nl2br((!empty(_post('ykien_tp'.$ma)))?_post('ykien_tp'.$ma):'Từ chối').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',
				'ldvp_duyet'        		=> 2,
				'cb_giu'    			=> $donnp[0]['FK_iMaCB']
			);
		} elseif ($this->_session['iQuyenHan_DHNB'] == 4 || $this->_session['iQuyenHan_DHNB'] == 5) {
			$data=array(
				'yk_bgd'    			=> '<b style="color:red">'.nl2br((!empty(_post('ykien_bgd'.$ma)))?_post('ykien_bgd'.$ma):'Từ chối').'</b> <br> <i class="pull-right"> '.$this->_session['sHoTen'].' - '.date('d/m/Y H:i:s', time()).'</i>',
				'bgd_duyet'        		=> 2,
				'cb_giu'    			=> $donnp[0]['FK_iMaCB']
			);
			$this->guitttuchoi();
		}
		
		$kiemtra = $this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_nghi_phep',$data);
		
		redirect('duyetnghiphep');
	}
	public function guittduyet()
	{
		
	}
	public function guitttuchoi()
	{
		
	}
	
	public function DSVanBan() 
	{
		
		$tonghop	 = _get('tonghop');
		$id 		= _get('id');
		
		$config['base_url']             = base_url().'duyetnghiphep?tonghop='.$tonghop.'&id='.$id;
		
		$config['total_rows']           = count($this->Mnghiphep->laydschoduyet($this->_session['PK_iMaCB']));
	
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'duyetnghiphep');
      	}
		
		$data['tonghop']    = $tonghop;
		$data['count']      = $config['total_rows'];
		$data['info']       = $this->Mnghiphep->laydschoduyet($this->_session['PK_iMaCB']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdsvanban.php */
/* Location: ./application/controllers/vanban/Cdsvanban.php */