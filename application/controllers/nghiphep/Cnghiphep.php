<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnghiphep extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		$ma = _get('id');
		
		$this->_thongtin 	= $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_nghi_phep');
		$data['thongtin']	= $this->_thongtin;
		
		$data['title']        = 'Thông tin nghỉ phep';
		$temp['data']         = $data;
		$temp['template']     = 'nghiphep/Vnghiphep';
		$this->load->view('layout_admin/layout',$temp);
	}
	
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */