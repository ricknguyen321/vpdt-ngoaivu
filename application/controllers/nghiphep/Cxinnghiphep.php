<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxinnghiphep extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('nghiphep/Mnghiphep');
	}
	public function index()
	{
		$ma = _get('id');
		
		if(!empty($ma))
		{			
			$data['content'] = $this->capnhatDuLieu($ma);						
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin']	  = $this->_thongtin;
		$giotu = explode(':', $this->_thongtin[0]['gio_tu']);
		if (count($giotu) == 2) {
			$data['giotu'] = $giotu[0];
			$data['phuttu'] = $giotu[1];
		} else {			
		}
		$gioden = explode(':', $this->_thongtin[0]['gio_den']);
		if (count($gioden) == 2) {
			$data['gioden'] = $gioden[0];
			$data['phutden'] = $gioden[1];
		} else {
			
		}
		$canbo	  = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$this->_session['PK_iMaCB'],'tbl_canbo');
		$data['ngayphepconlai'] = $canbo[0]['ngay_phep'];
		$data['dsldduyet']       = $this->Mnghiphep->layldduyetnghi($this->_session['FK_iMaPhongHD']);
		$data['dsldvp']       = $this->Mnghiphep->layldvp();
		$data['title']        = 'Nhập phê duyệt nghỉ phép';
		$temp['data']         = $data;
		$temp['template']     = 'nghiphep/Vxinnghiphep';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_nghi_phep');	
		$cbnhan = _post('ldduyet');
		if ($this->_session['iQuyenHan_DHNB'] == 3) {
			$cbnhan = 735;
		}		
		if(_post('luudulieu'))
		{
			$data=array(
				'ly_do'		   			=> nl2br(_post('ly_do')),
				'ho_ten'        		=> $this->_session['sHoTen'],
				'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
				'phong_ban'         	=> '',
				'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
				'loai_phep'    			=> _post('loai_phep'),
				'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
				'cb_giu'    			=> $cbnhan,
				'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
				'gio_den'    			=> _post('gioden').':'._post('phutden'),
				'ngay_tu'    			=> date_insert(_post('ngay_tu')),
				'ngay_den'    			=> date_insert(_post('ngay_den')),
				'cong_viec'    				=> nl2br(_post('cong_viec')),
				//'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
			);
			
			if(empty(_post('ngay_tu')))
			{
				$data=array(
					'ly_do'		   			=> nl2br(_post('ly_do')),
					'ho_ten'        		=> $this->_session['sHoTen'],
					'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
					'phong_ban'         	=> '',
					'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
					'loai_phep'    			=> _post('loai_phep'),
					'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
					'cb_giu'    			=> $cbnhan,
					'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
					'gio_den'    			=> _post('gioden').':'._post('phutden'),
					'ngay_den'    			=> date_insert(_post('ngay_den')),
					'cong_viec'    				=> nl2br(_post('cong_viec')),
					//'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập ngày!','danger');
			}
			if(empty(_post('ngay_den')))
			{
				$data=array(
					'ly_do'		   			=> nl2br(_post('ly_do')),
					'ho_ten'        		=> $this->_session['sHoTen'],
					'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
					'phong_ban'         	=> '',
					'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
					'loai_phep'    			=> _post('loai_phep'),
					'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
					'cb_giu'    			=> $cbnhan,
					'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
					'gio_den'    			=> _post('gioden').':'._post('phutden'),
					'ngay_tu'    			=> date_insert(_post('ngay_tu')),
					'cong_viec'    				=> nl2br(_post('cong_viec')),
					//'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập ngày!','danger');
			}
			
			if (date_insert(_post('ngay_den')) < date_insert(_post('ngay_tu'))) {
				$data=array(
					'ly_do'		   			=> nl2br(_post('ly_do')),
					'ho_ten'        		=> $this->_session['sHoTen'],
					'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
					'phong_ban'         	=> '',
					'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
					'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
					'gio_den'    			=> _post('gioden').':'._post('phutden'),
					'loai_phep'    			=> _post('loai_phep'),
					'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
					'cb_giu'    			=> $cbnhan,
					'ngay_den'    			=> date_insert(_post('ngay_den')),
					'ngay_tu'    			=> date_insert(_post('ngay_tu')),
					'cong_viec'    				=> nl2br(_post('cong_viec')),
					//'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Chọn ngày chưa đúng!','danger');
			}
					
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_nghi_phep',$data);
			redirect('dsnghiphep');
		}
	}
	
	public function themDuLieu()
	{
		$time = time();		
		$ldvp = $this->Mnghiphep->layldvp();
		$gd = $this->Mnghiphep->laygd();
		
		$cbnhan = _post('ldduyet');
		
		if ($this->_session['iQuyenHan_DHNB'] == 3) {
			$cbnhan = $gd[0]['PK_iMaCB'];
		}
		if ($this->_session['iQuyenHan_DHNB'] == 6) {
			$cbnhan = $ldvp[0]['PK_iMaCB'];
		}
		if(_post('luudulieu'))
		{
			$data=array(
				'ly_do'		   			=> nl2br(_post('ly_do')),
				'ho_ten'        		=> $this->_session['sHoTen'],
				'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
				'phong_ban'         	=> '',
				'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
				'loai_phep'    			=> _post('loai_phep'),
				'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
				'cb_giu'    			=> $cbnhan,
				'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
				'gio_den'    			=> _post('gioden').':'._post('phutden'),
				'ngay_tu'    			=> date_insert(_post('ngay_tu')),
				'ngay_den'    			=> date_insert(_post('ngay_den')),
				'cong_viec'    				=> nl2br(_post('cong_viec')),
				'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
			);
			
			
			if(empty(_post('ngay_tu')))
			{
				$data=array(
					'ly_do'		   			=> nl2br(_post('ly_do')),
					'ho_ten'        		=> $this->_session['sHoTen'],
					'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
					'phong_ban'         	=> '',
					'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
					'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
					'gio_den'    			=> _post('gioden').':'._post('phutden'),
					'loai_phep'    			=> _post('loai_phep'),
					'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
					'cb_giu'    			=> $cbnhan,
					'ngay_den'    			=> date_insert(_post('ngay_den')),
					'cong_viec'    				=> nl2br(_post('cong_viec')),
					'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập ngày!','danger');
			}
			if(empty(_post('ngay_den')))
			{
				$data=array(
					'ly_do'		   			=> nl2br(_post('ly_do')),
					'ho_ten'        		=> $this->_session['sHoTen'],
					'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
					'phong_ban'         	=> '',
					'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
					'loai_phep'    			=> _post('loai_phep'),
					'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
					'cb_giu'    			=> $cbnhan,
					'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
					'gio_den'    			=> _post('gioden').':'._post('phutden'),
					'ngay_tu'    			=> date_insert(_post('ngay_tu')),
					'cong_viec'    				=> nl2br(_post('cong_viec')),
					'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập ngày!','danger');
			}
			if (date_insert(_post('ngay_den')) < date_insert(_post('ngay_tu'))) {
				$data=array(
					'ly_do'		   			=> nl2br(_post('ly_do')),
					'ho_ten'        		=> $this->_session['sHoTen'],
					'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
					'phong_ban'         	=> '',
					'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
					'loai_phep'    			=> _post('loai_phep'),
					'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
					'cb_giu'    			=> $cbnhan,
					'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
					'gio_den'    			=> _post('gioden').':'._post('phutden'),
					'ngay_den'    			=> date_insert(_post('ngay_den')),
					'ngay_tu'    			=> date_insert(_post('ngay_tu')),
					'cong_viec'    				=> nl2br(_post('cong_viec')),
					'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Chọn ngày chưa đúng!','danger');
			}
			
			if (_post('giotu') > 23 || _post('gioden') > 23 || _post('phuttu') > 59 || _post('phutden') > 59) {
				$data=array(
					'ly_do'		   			=> nl2br(_post('ly_do')),
					'ho_ten'        		=> $this->_session['sHoTen'],
					'FK_iMaCB'     			=> $this->_session['PK_iMaCB'],
					'phong_ban'         	=> '',
					'FK_iMaPhongHD'      	=> $this->_session['FK_iMaPhongHD'],
					'loai_phep'    			=> _post('loai_phep'),
					'so_ngay_nghi'    		=> _post('so_ngay_nghi'),
					'cb_giu'    			=> $cbnhan,
					'gio_tu'    			=> _post('giotu').':'._post('phuttu'),
					'gio_den'    			=> _post('gioden').':'._post('phutden'),
					'ngay_den'    			=> date_insert(_post('ngay_den')),
					'ngay_tu'    			=> date_insert(_post('ngay_tu')),
					'cong_viec'    				=> nl2br(_post('cong_viec')),
					'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Nhập giờ chưa đúng!','danger');
			}
			
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_nghi_phep',$data);
			
		
			if($id_insert>0)
			{
				echo '<script language="javascript">';
				echo 'alert(Thêm mới đơn xin nghỉ thành công.)';  //not showing an alert box.
				echo '</script>';
				redirect('dsnghiphep');	
			}
			else{
				return messagebox('Thêm mới đơn xin nghỉ thất bại!','danger');
			}
		}
	}
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */