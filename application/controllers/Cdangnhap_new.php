<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cdangnhap_new extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Mdangnhap');
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
        $week = (int)date("W", strtotime(date('Y-m-d')));
        $week_1 = $week-1;

        $giatri = $this->Mdanhmuc->getData_kehoach_new('tuan',$week_1,'active',3,'loai_kh',3,'canbo_id',$vanban['PK_iMaCB'],'kehoach');
        foreach ($giatri as $key => $value) {
            $value['kh_id_sub'] = $value['kh_id'];
            unset($value['kh_id']);
            $value['tuan'] = $week;
            $this->Mdanhmuc->themDuLieu('kehoach',$value);
        }

	}	
}

/* End of file Cdangnhap.php */
/* Location: ./application/controllers/dangnhap/Cdangnhap.php */