<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhapmoivanbandi extends MY_Controller {

	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->model('vanbandi_caphai/Mvanbandi_caphai');
	}
	public function index()
	{
		$action = _post('action');
		if(!empty($action))
		{
			switch ($action) {
				case 'laychucvu':
					$this->layChucVu();
					break;
				case 'laykyhieu':
					$this->layKyHieu();
					break;
				default:
					# code...
					break;
			}
		}
		$canbo      = $this->Mvanbandi->layCB();
		foreach ($canbo as $key => $cb) {
			$data['mangcb'][$cb['PK_iMaCB']] = $cb['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$donvi 		= $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$ma = _get('id');
		if(!empty($ma))
		{
			$this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['loai']		  = _get('loai');
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dsduthao']     = $this->Mdanhmuc->layDuLieu('FK_iMaDV',$donvi,'tbl_phongban_caphai');
		// pr($data['dsduthao']);
		$data['thongtin']	  = $this->_thongtin;
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi_caphai->layNguoiKy_CH($donvi,NULL);

		// nơi nhận mail quận huyện và sở ban ngành
		$data['title']        = 'Nhập mới văn bản đi';
		$temp['data']         = $data;
		$temp['template']     = 'vanbandi_caphai/Vnhapmoivanbandi';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$loai       = _get('loai');
		$donvi		= $this->_session['FK_iMaPhongHD'];
		$phongban	= $this->_session['phong_caphai'];
		$quyen		= $this->_session['quyenhan_caphai'];
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi_caphai');
		if(_post('luudulieu'))
		{
			$canbo   = $this->_session['PK_iMaCB'];
			$ngaymoi = _post('ngaymoi');
			if(!empty($ngaymoi))
			{
				$ngay    = date_insert($ngaymoi);
				$gioimoi = _post('giomoi');
			}
			else{
				$ngay    = '0000-00-00';
				$gioimoi ='';
			}
			$data=array(
				'sNgayMoi'         => $ngay,
				'sGioMoi'          => $gioimoi,
				'sDiaDiemMoi'      => _post('diadiem'),
				'FK_iMaLVB'        => _post('loaivanban'),
				'FK_iMaPB'         => _post('noiduthao'),
				'sMoTa'            => _post('mota'),
				'iSoTrang'         => _post('sotrang'),
				'sKyHieu'          => _post('sokyhieu'),
				'sTenLV'           => _post('linhvuc'),
				'FK_iMaCB_Ky'      => _post('nguoiky'),
				'sChucVu'		   => _post('chucvu'),
				'iSoDen'           => _post('sotraloi'),
				'iSoVBDi'		   => _post('sovanban'),
				'FK_iMaCB'    	   => $canbo,
				'sNgayNhap'        => date('Y-m-d H:i:s'),
				'iNam'			   => $_SESSION['nam'],
				'FK_iMaDV'		   => $donvi,
				'FK_iMaPB'		   => $phongban,
				'sNgayVBDi'        =>  date_insert(_post('ngaythang')),
				'sNoiNhan'		   => _post('noinhan')
			);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi_caphai',$data);
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('caphai_'.$_SESSION['nam'],$time);
				$file = array(
					'FK_iMaVBDi' => $ma,
					'sTenFile'   => _post('sokyhieu'),
					'sDuongDan'  => 'caphai_'.$_SESSION['nam'].'/vbdi_'.$time.'_'.clear($name[0]),
					'sThoiGian'  => date('Y-m-d H:i:s'),
					'FK_iMaCB'   => $this->_session['PK_iMaCB'],
					'FK_iMaDV'   => $donvi,
					'FK_iMaPB'   => $phongban,
					'iNam'		 => $_SESSION['nam']
					);
				$this->Mdanhmuc->themDuLieu('tbl_files_vbdi_caphai',$file);
			}
			if($kiemtra>0)
			{
				redirect($loai);
			}
		}
	}
	public function themDuLieu()
	{
		$donvi		= $this->_session['FK_iMaPhongHD'];
		$phongban	= $this->_session['phong_caphai'];
		$quyen		= $this->_session['quyenhan_caphai'];
		if(_post('luudulieu'))
		{
			$canbo   = $this->_session['PK_iMaCB'];
			$ngaymoi = _post('ngaymoi');
			if(!empty($ngaymoi))
			{
				$ngay    = date_insert($ngaymoi);
				$gioimoi = _post('giomoi');
			}
			else{
				$ngay    = '0000-00-00';
				$gioimoi ='';
			}
			$data=array(
				'sNgayMoi'         => $ngay,
				'sGioMoi'          => $gioimoi,
				'sDiaDiemMoi'      => _post('diadiem'),
				'FK_iMaLVB'        => _post('loaivanban'),
				'FK_iMaPB'         => _post('noiduthao'),
				'sMoTa'            => _post('mota'),
				'iSoTrang'         => _post('sotrang'),
				'sKyHieu'          => _post('sokyhieu'),
				'sTenLV'           => _post('linhvuc'),
				'FK_iMaCB_Ky'      => _post('nguoiky'),
				'sChucVu'		   => _post('chucvu'),
				'iSoDen'           => _post('sotraloi'),
				'FK_iMaCB'    	   => $canbo,
				'sNgayNhap'        => date('Y-m-d H:i:s'),
				'iNam'			   => $_SESSION['nam'],
				'FK_iMaDV'		   => $donvi,
				'FK_iMaPB'		   => $phongban,
				'sNgayVBDi'        =>  date_insert(_post('ngaythang')),
				'sNoiNhan'		   => _post('noinhan')
			);
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_vanbandi_caphai',$data);
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('caphai_'.$_SESSION['nam'],$time);
				$file = array(
					'FK_iMaVBDi' => $id_insert,
					'sTenFile'   => _post('sokyhieu'),
					'sDuongDan'  => 'caphai_'.$_SESSION['nam'].'/vbdi_'.$time.'_'.clear($name[0]),
					'sThoiGian'  => date('Y-m-d H:i:s'),
					'FK_iMaCB'   => $this->_session['PK_iMaCB'],
					'FK_iMaDV'   => $donvi,
					'FK_iMaPB'   => $phongban,
					'iNam'		 => $_SESSION['nam']
					);
				$this->Mdanhmuc->themDuLieu('tbl_files_vbdi_caphai',$file);
			}
			return messagebox('Thêm văn bản đi thành công!','info');
		}
	}
	public function upload($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'vbdi_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	public function layKyHieu()
	{
		$donvi 		= $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$donvi,'tbl_phongban');
		$maloai  = _post('maloai');
		$layLoai = $this->Mdanhmuc->layDuLieu('PK_iMaLVB',$maloai,'tbl_loaivanban');
		$sokyhieu = $layLoai[0]['sTenVietTat'].'-'.$phongban[0]['sVietTat'];
		echo json_encode($sokyhieu); exit();
	}
	public function layChucVu()
	{
		$donvi = $this->_session['FK_iMaPhongHD'];
		$ma= _post('ma');
		$ketqua = $this->Mvanbandi_caphai->layNguoiKy_CH($donvi,$ma);
		echo json_encode($ketqua[0]['tendinhdanh']);
		exit();
	}

}

/* End of file Cnhapmoivanbandi.php */
/* Location: ./application/controllers/vanbandi_caphai/Cnhapmoivanbandi.php */