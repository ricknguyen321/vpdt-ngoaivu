<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachvanbanchoso extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->library('pagination');
		$this->load->model('vanbandi_caphai/Mvanbandi_caphai');
	}
	public function index()
	{
		$donvi 		= $this->_session['FK_iMaPhongHD'];
		$nam 		= date('Y');
		if(_post('duyet'))
		{
			$ma        = _post('duyet');
			$solonnhat = $this->Mvanbandi_caphai->laySoDiMoi($donvi,$nam);
			$sodi      = $solonnhat['iSoVBDi'] +1;
			$ngaythang = _post("ngaythang[$ma]");
			$data=array(
				'iSoVBDi'            => $sodi,
				'sNgayVBDi'          => date_insert($ngaythang[0])
				);
			$kiemtra= $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi_caphai',$data);
			if($kiemtra>0)
			{
				$data['thongbaoso'] = $sodi;
			}
		}
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dsduthao']     = $this->Mdanhmuc->layDuLieu('FK_iMaDV',$donvi,'tbl_phongban_caphai');
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi_caphai->layNguoiKy_CH($donvi,NULL);
		$vanban             = $this->DSVanBan();
		$data['dsvanban']	= $vanban['info'];
		if(!empty($data['dsvanban'])){
			foreach ($data['dsvanban'] as $key => $value) {
				$duongdan = $this->Mvanbandi_caphai->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$data['dsvanban'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$data['dsvanban'][$key]['sDuongDan'] = '';
				}
			}
		}
		$data['loaivanban'] = $vanban['loaivanban'];
		$data['noiduthao']  = $vanban['noiduthao'];
		$data['kyhieu']     = $vanban['kyhieu'];
		$data['ngaythang']  = $vanban['ngaythang'];
		$data['trichyeu']   = $vanban['trichyeu'];
		$data['nguoikyvb']  = $vanban['nguoiky'];
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Danh sách văn bản chờ số';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi_caphai/Vdanhsachvanbanchoso';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi_caphai');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi_caphai');
		}
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi_caphai');
		if($kiemtra>0)
		{
			return messagebox('Xóa văn bản thành công!','info');
		}
	}
	public function DSVanBan() 
	{
		$donvi      = $this->_session['FK_iMaPhongHD'];
		$nam		= date('Y');
		$loaivanban	= _get('loaivanban');
		$noiduthao	= _get('noiduthao');
		$kyhieu		= _get('kyhieu');
		$ngaythang	= _get('ngaythang');
		if(!empty($ngaythang))
		{
			$ngaythang = date_insert($ngaythang);
		}
		else{
			$ngaythang = '';
		}
		$trichyeu   = _get('trichyeu');
		$nguoiky    = _get('nguoiky');
		$config['base_url']             = base_url().'danhsachvanbanchoso?loaivanban='.$loaivanban.'&noiduthao='.$noiduthao.'&kyhieu='.$kyhieu.'&ngaythang='.$ngaythang.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky;
		$config['total_rows']           = $this->Mvanbandi_caphai->demDSVB_ChoSo(1,$donvi,$nam,NULL,$loaivanban,$noiduthao,$kyhieu,$ngaythang,$trichyeu,$nguoiky);
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dsvanbanchoso');
      	}
		$data['loaivanban'] = $loaivanban;
		$data['noiduthao']  = $noiduthao;
		$data['kyhieu']     = $kyhieu;
		$data['ngaythang']  = $ngaythang;
		$data['trichyeu']   = $trichyeu;
		$data['nguoiky']    = $nguoiky;
		$data['info']       = $this->Mvanbandi_caphai->layDSVB_ChoSo(1,$donvi,$nam,NULL,$loaivanban,$noiduthao,$kyhieu,$ngaythang,$trichyeu,$nguoiky,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdanhsachvanbanchoso.php */
/* Location: ./application/controllers/vanbandi_caphai/Cdanhsachvanbanchoso.php */