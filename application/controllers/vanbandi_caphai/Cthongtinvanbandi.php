<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongtinvanbandi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->model('vanbandi_caphai/Mvanbandi_caphai');
	}
	public function index()
	{
		$data['dssoden']    = '';
		$data['title']      = 'Thông tin văn bản';
		$mavaban            = _get('id');
		$data['thongtin']   = $this->Mvanbandi_caphai->layThongTinChiTiet($mavaban);
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$data['dsfile']	   = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavaban,'tbl_files_vbdi_caphai');
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi_caphai/Vthongtinvanbandi';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cthongtinvanbandi.php */
/* Location: ./application/controllers/vanbandi_caphai/Cthongtinvanbandi.php */