<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cshow_send_mail extends MY_Controller {

	protected $_thongtin;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']    = 'Show send mail';
		$time = date('Y-m-d');
		$data['thongtin'] = $this->Mdanhmuc->layMailDaGui_theongay($time);
		$temp['data']     = $data;
		$temp['template'] = 'Vshow_send_mail';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cshow_send_mail.php */
/* Location: ./application/controllers/Cshow_send_mail.php */