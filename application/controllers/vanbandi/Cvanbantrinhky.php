<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbantrinhky extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->library('pagination');
	}
	public function index()
	{
		$quyen = $this->_session['iQuyenHan_DHNB'];
		if($quyen!=3&&$quyen!=4&&$quyen!=5)
		{
			redirect('welcome.html');
		}
		$data['trangthai']  = 1;
		$action = _post('action');
		if(!empty($action))
		{ 
			$mavanban = _post('mavanban');
			$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi','trangthai_xem',2);
		}
		$vanban             = $this->DSVanBan();
		$data['dsvanban']	= $vanban['info'];
		if(!empty($data['dsvanban'])){
			foreach ($data['dsvanban'] as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$data['dsvanban'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$data['dsvanban'][$key]['sDuongDan'] = '';
				}
			}
		}
		$data['trangthai'] = $vanban['trangthai'];
		$data['chude']     = $vanban['chude'];
		$data['phantrang'] = $vanban['pagination'];
		$data['title']     = 'Danh sách văn bản trình ký';
		$temp['data']      = $data;
		$temp['template']  = 'vanbandi/Vvanbantrinhky';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBan() 
	{
		$trangthai 	= _get('trangthai');
		if(!isset($trangthai))
		{
			$trangthai = 1;
		}
		$chude     	= _get('chude');
		$canbo      = $this->_session['PK_iMaCB'];
		$config['base_url']             = base_url().'readmail?trangthai='.$trangthai.'&chude='.$chude;
		$config['total_rows']           = $this->Mvanbandi->demVBTrinhKy($trangthai,$canbo,$chude);
		$config['per_page']             = 20;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dsvanban');
      	}
		$data['trangthai']  = $trangthai;
		$data['chude']      = $chude;
		$data['info']       = $this->Mvanbandi->layVBTrinhKy($trangthai,$canbo,$chude,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cvanbantrinhky.php */
/* Location: ./application/controllers/vanbandi/Cvanbantrinhky.php */