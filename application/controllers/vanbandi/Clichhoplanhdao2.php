<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clichhoplanhdao2 extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('Vanbanden/Mlichcongtac');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('Vanbanden/Mvanbanden');
	}
	public function index()
	{
		$dsphongban = $this->Mlichcongtac->layPhongBan();
		foreach ($dsphongban as $key => $value) {
			$mangphongban[$value['PK_iMaPB']] = $value['sTenPB'];
		}
		$data['mangphongban'] = $mangphongban;
		$thoigian              =  _get('thoigian');
		$ngaytuan              = $this->layNgayTuan($thoigian);
		$taikhoan = $this->_session;
		$quyen    = $taikhoan['iQuyenHan_DHNB'];
		$data['lichhop']	   = $this->Mvanbandi->layLichHop(date('Y-m-d',$ngaytuan[0]),date('Y-m-d',$ngaytuan[1]));
		$data['thoigian']      = $thoigian;
		$data['malanhdao']     = $macb;
		$data['ngaytrongtuan'] = $ngaytuan[2];
		foreach ($data['ngaytrongtuan'] as $key => $value) {
			$ngay=1;
			$sang=1;
			$chieu=1;
			foreach ($data['lichhop'] as $k => $val) {
				if($value[1]==date_select($val['sNgayMoi']))
				{
					if($val['sGioMoi']>'12:00')
					{
						$data['ngaytrongtuan'][$key]['chieu']['name'] = 'Chiều';
						$data['ngaytrongtuan'][$key]['chieu']['giaymoi'][] = $data['lichhop'][$k];
					}
					else{
						$data['ngaytrongtuan'][$key]['sang']['name'] = 'Sáng';
						$data['ngaytrongtuan'][$key]['sang']['giaymoi'][] = $data['lichhop'][$k];
					}
				}
			}
		}
		// pr($data['ngaytrongtuan']);
		$data['lanhdao']       = $this->Mvanbandi->layNguoiKy();
		$data['macanbo']	   = $this->_session['PK_iMaCB'];
		$data['title']         = 'Lịch họp của lãnh đạo';
		$data['url']           = base_url();
		$this->parser->parse('vanbandi/Vlichhoplanhdao2',$data);
	}
	public function layNgayTuan($thoigian)
	{
		$year = date('Y');
		$week = date('W');
		$bd   = 1;
		// $bd = mặc đinh bắt đầu là ngày thứ hai
		// 1 năm có 53 tuần
		// tuần hiện tại sẽ lấy các tham số mặc định
		// tuần trước sẽ kiểm tra tuần hiện tại có phải tuần đầu tiên của năm k
		// tuấn tiếp theo sẽ kiểm tra tuần hiện tại phải cuối năm k
		// ngày hiện tại $bd = ngày hiên tại( date("w") )
		switch ($thoigian) {
			case 'hientai':
				break;
			case 'tuantruoc':
				if($week==1)
				{
					$year=$year-1;
					$week=53;
				}
				else{
					$year=$year;
					$week=$week-1;
				}
				break;
			case 'tuantiep':
				if($week==53)
				{
					$year=$year+1;
					$week=1;
				}
				else{
					$year=$year;
					$week=$week+1;
				}
				break;
			
			default:
				$bd = date("w");
				break;
		}
		
		//$start_date ngày đầu tiên của tuần hiện tại
		for($i=$bd;$i<=7;$i++)
		{
			$start_date = strtotime( $year . "W". $week . $i);
			$ngaytuan[] = $this->sw_get_current_weekday($start_date,$week,$year,$i);
		}
		$ngatbatdau  = strtotime( $year . "W". $week . 1);
		$ngayketthuc = strtotime( $year . "W". $week . 7);
		return array($ngatbatdau,$ngayketthuc,$ngaytuan);
	}
	// hàm chuyển đổi các thứ tiếng anh sang tiếng việt 
	function sw_get_current_weekday($start_date,$week,$year,$i) {
	    date_default_timezone_set('Asia/Ho_Chi_Minh');
	    $weekday = date("l",$start_date);
	    $weekday = strtolower($weekday);
	    switch($weekday) {
	        case 'monday':
	            $weekday = 'Thứ hai';
	            break;
	        case 'tuesday':
	            $weekday = 'Thứ ba';
	            break;
	        case 'wednesday':
	            $weekday = 'Thứ tư';
	            break;
	        case 'thursday':
	            $weekday = 'Thứ năm';
	            break;
	        case 'friday':
	            $weekday = 'Thứ sáu';
	            break;
	        case 'saturday':
	            $weekday = 'Thứ bảy';
	            break;
	        default:
	            $weekday = 'Chủ nhật';
	            break;
	    }
	    return array($weekday,date('d/m/Y',strtotime( $year . "W". $week . $i)));
	}

}

/* End of file Clichcongtac.php */
/* Location: ./application/controllers/vanbanden/Clichcongtac.php */