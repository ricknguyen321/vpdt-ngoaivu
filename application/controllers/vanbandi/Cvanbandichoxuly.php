<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandichoxuly extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$action     = _post('action');
		if(!empty($action))
		{ 
			$maVB = _post('maVB');
			if($quyen==4)
			{
				$this->Mtruyennhan->capnhattrangthai($maVB,'giamdoc',$taikhoan,'trangthai_xem_gd');
			}
			if($quyen==5)
			{
				$this->Mtruyennhan->capnhattrangthai($maVB,'phogiamdoc',$taikhoan,'trangthai_xem_pgd');
			}
			if($quyen==3&&$chucvu==5)
			{
				$this->Mtruyennhan->capnhattrangthai($maVB,'bophantonghop',$taikhoan,'trangthai_xem_bpth');
			}
		}
		$tinhtrang = _get('tinhtrang');
		if(!empty($tinhtrang))
		{
			$tinhtrang = $tinhtrang;
		}
		else{
			$tinhtrang = 1;
		}
		$data['tinhtrang'] = $tinhtrang;
		$chude             = _get('chude');
		$data['chude']     = $chude;
		switch ($quyen) {
			case 3:
				if($chucvu==6) // chức vụ chánh văn phòng
				{
					$trangthai =3; #TP
					$data['tennguoinhan1']  = 'Chọn giám đốc';
					$data['tennguoinhan2']  = 'Chọn phó giám đốc';
					$data['tennguoinhan3']  = 'Chọn bộ phận tổng hợp';
					$data['tennguoinhan4']  = 'Chọn phó phòng';
					$data['tennguoinhan5']  = 'Chọn chuyên viên';
					$data['tennguoinhan6']  = 'Chọn chánh văn phòng';
					$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(4,NULL,NULL,NULL);//GD
					$data['nguoinhan2'] = $this->Mtruyennhan->layNguoiNhan(5,NULL,NULL,NULL);//PDG
					$data['nguoinhan3'] = $this->Mtruyennhan->layNguoiNhan(3,5,NULL,NULL);//PDG 5: là chức vụ chuyên viên
					$data['nguoinhan6'] = $this->Mtruyennhan->layNguoiNhan(3,6,NULL,NULL);// chánh văn phòng;
					$ten_taikhoan       = 'truongphong';
					$ykien 				= 'ykien_tp';
					$ten_trangthai_xem  = '';
					$trangthai_xem 		= '';
					$chude 				= '';
				}else{// bộ phận tổng hợp
					$trangthai =33;
					$ten_taikhoan       = 'bophantonghop';
					$ykien 				= 'ykien_bpth';
					$ten_trangthai_xem  = 'trangthai_xem_bpth';
					$trangthai_xem 		= $tinhtrang;
					$chude 				= $chude;
				}
				break;
			case 4: #GD
				$trangthai =7;
				$ten_taikhoan       = 'giamdoc';
				$ykien 				= 'ykien_gd';
				$ten_trangthai_xem  = 'trangthai_xem_gd';
				$trangthai_xem 		= $tinhtrang;
				$chude 				= $chude;
				break;
			case 5: #PGD
				$trangthai =6;
				$ten_taikhoan       = 'phogiamdoc';
				$ykien 				= 'ykien_pgd';
				$ten_trangthai_xem  = 'trangthai_xem_pgd';
				$trangthai_xem 		= $tinhtrang;
				$chude 				= $chude;
				break;
			case 6: #TP
				$data['tennguoinhan1']  = 'Chọn giám đốc';
				$data['tennguoinhan2']  = 'Chọn phó giám đốc';
				$data['tennguoinhan3']  = 'Chọn bộ phận tổng hợp';
				$data['tennguoinhan4']  = 'Chọn phó phòng';
				$data['tennguoinhan5']  = 'Chọn chuyên viên';
				$data['tennguoinhan6']  = 'Chọn chánh văn phòng';
				$data['tennguoinhan7']  = 'Chọn chi cục phó';
				$data['tennguoinhan8']  = 'Chọn trưởng phòng';
				$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(4,NULL,NULL,NULL);//GD
				$data['nguoinhan2'] = $this->Mtruyennhan->layNguoiNhan(5,NULL,NULL,NULL);//PDG
				$data['nguoinhan3'] = $this->Mtruyennhan->layNguoiNhan(3,5,NULL,NULL);//PDG 5: là chức vụ chuyên viên
				$data['nguoinhan6'] = $this->Mtruyennhan->layNguoiNhan(3,6,NULL,NULL);// chánh văn phòng
				if($chucvu==15) // chức vụ chi cục trưởng
				{
					$trangthai    = 5; #CCT
					$ten_taikhoan = 'truongchicuc';
					$ykien        = 'ykien_tcc';
				}
				if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
				{
					$trangthai    = 3;
					$ten_taikhoan = 'truongphong';
					$ykien        = 'ykien_tp';
				}
				$ten_trangthai_xem  = '';
				$trangthai_xem 		= '';
				$chude 				= '';
				break;
			case 7: #PTP
				$trangthai =2;
				$data['tennguoinhan1']  = 'Chọn trưởng phòng';
				$data['tennguoinhan2']  = 'Chọn chuyên viên';
				if($phongbanHD==12)
				{
					$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(11,NULL,NULL,$quyendb);//TP
					$data['nguoinhan2'] = [['PK_iMaCB'=>'cv','sHoTen'=>'Gửi lại chuyên viên']];
					$data['nguoinhan3']	= '';
				}
				elseif($phongbanHD==11)
				{
					$data['tennguoinhan1']  = 'Chọn trưởng phòng';
					$data['tennguoinhan2']  = 'Chọn chuyên viên';
					$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(3,6,$phongbanHD,NULL);//TP
					$data['nguoinhan2'] = '';
					$data['nguoinhan3']	= '';
				}
				else{
					$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(6,NULL,$phongbanHD,NULL);//TP
					$data['nguoinhan2'] = '';
					$data['nguoinhan3']	= '';
				}
				$ten_taikhoan       = 'phophong';
				$ykien 				= 'ykien_pp';
				$ten_trangthai_xem  = '';
				$trangthai_xem 		= '';
				$chude 				= '';
				break;
			case 8: #CV
				$data['tennguoinhan1']  = 'Chọn trưởng phòng';
				$data['tennguoinhan2']  = 'Chọn phó phòng';
				$trangthai =1;
				if($phongbanHD==12)
				{
					$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(11,NULL,12,$quyendb);//TP
					$data['nguoinhan2'] = $this->Mtruyennhan->layNguoiNhan(7,NULL,12,$quyendb);//PTP
				}
				elseif($phongbanHD==11)
				{
					$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(3,6,$phongbanHD,NULL);//TP
					$data['nguoinhan2'] = $this->Mtruyennhan->layNguoiNhan(7,NULL,$phongbanHD,NULL);//PTP
				}
				else{
					$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(6,NULL,$phongbanHD,NULL);//TP
					$data['nguoinhan2'] = $this->Mtruyennhan->layNguoiNhan(7,NULL,$phongbanHD,NULL);//PTP
				}
				$ten_taikhoan       = 'chuyenvien';
				$ykien 				= 'ykien_cv';
				$ten_trangthai_xem  = '';
				$trangthai_xem 		= '';
				$chude 				= '';
				break;
			case 10:#CCP
				$trangthai =4; 
				$data['tennguoinhan1']  = 'Chọn chi cục trưởng';
				$data['tennguoinhan2']  = 'Chọn trưởng phòng';
				$data['tennguoinhan3']  = 'Chọn phó phòng';
				$data['tennguoinhan4']  = 'Chọn chuyên viên';
				$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(6,15,12,NULL);//CCT
				$ten_taikhoan       = 'phochicuc';
				$ykien 				= 'ykien_pcc';
				$ten_trangthai_xem  = '';
				$trangthai_xem 		= '';
				$chude 				= '';
				break;
			case 11:#TP bên chi cục
				$trangthai =333;
				$data['tennguoinhan1']  = 'Chọn chi cục trưởng';
				$data['tennguoinhan2']  = 'Chọn chi cục phó';
				$data['tennguoinhan3']  = 'Chọn phó phòng';
				$data['tennguoinhan4']  = 'Chọn chuyên viên';
				$data['nguoinhan1'] = $this->Mtruyennhan->layNguoiNhan(6,15,12,NULL);//CCT
				$data['nguoinhan2'] = $this->Mtruyennhan->layNguoiNhan(10,NULL,12,NULL);//CCP
				$ten_taikhoan       = 'truongphong';
				$ykien 				= 'ykien_tp';
				$ten_trangthai_xem  = '';
				$trangthai_xem 		= '';
				$chude 				= '';
				break;
			default:
				$trangthai =9;
				break;
		}
		if(_post('gui'))
		{
			$data['content'] = $this->Gui($trangthai);
		}
		if(_post('duyet'))
		{
			$mavanban = _post('duyet');
			$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi','trangthai_chuyen',8);
			$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi','nguoiduyet',$taikhoan);
			$nguoinhanlai = _post('nguoinhanlaiduyet');
			$data_them=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhanlai,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => 'Văn bản đã được duyệt'
					);
			$this->Mdanhmuc->themDuLieu('tbl_luuvet_vbdi',$data_them);
		}
		// $ds= $this->Mtruyennhan->layVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		$vanban            = $this->DSVanBan($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		// pr($ten_trangthai_xem);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		// pr($trangthai);
		$data['trangthai'] = $trangthai;
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách văn bản trình ký';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandichoxuly1';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBan($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude) 
	{
		$config['base_url']             = base_url().'vanbandichoxuly?tinhtrang='.$trangthai_xem.'&chude='.$chude;
		$config['total_rows']           = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandichoxuly');
      	}
      	// pr($ten_taikhoan.'--'.$taikhoan.'--'.$ykien.'--'.$ten_trangthai_xem.'--'.$trangthai_xem);
		$data['info']       = $this->Mtruyennhan->layVanBanChoXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}
	public function Gui($trangthai){
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$hoten   	= $this->_session['sHoTen'];
		$taikhoan   = $this->_session['PK_iMaCB'];
		$mavanban   = _post('gui');
		$ykiengoc   = _post('ykien')[$mavanban];  
		$ykien      = _post('ykien')[$mavanban];  
		$ykien      = '<b>'.$ykien.'</b><p>- '.$hoten.'</p><p>- '.date('Y-m-d H:i:s');
		$thongtin   = $this->Mtruyennhan->layThongTinVBDI($mavanban);
		$trangthai_chuyen = $thongtin['trangthai_chuyen'];
		switch ($trangthai) {
			case '1':
				$nguoinhan1 = _post('nguoinhan1')[$mavanban];
				$nguoinhan2 = _post('nguoinhan2')[$mavanban];
				$data_capnhat=array(
					'trangthai_chuyen' => ($trangthai_chuyen==8)?8:(($nguoinhan1)?3:2),
					'ykien_cv'    => '',
					'phophong'    => ($nguoinhan2)?$nguoinhan2:$thongtin['phophong'],
					'ykien_pp'    => ($nguoinhan2)?$ykien:'',
					'truongphong' => (!empty($nguoinhan1)&&empty($nguoinhan2))?$nguoinhan1:$thongtin['truongphong'],
					'ykien_tp'    => (!empty($nguoinhan1)&&empty($nguoinhan2))?$ykien:'',
					);
				if(!empty($nguoinhan1)&&empty($nguoinhan2))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan1,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan2))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan2,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'         => $ykiengoc
					);
				}
				if(!empty($nguoinhan1)||!empty($nguoinhan2))
				{
					$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
					if($kiemtra>0)
					{
						$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
					}
				}
				
				break;
			case '2':
				$nguoinhan1 = _post('nguoinhan1')[$mavanban];
				$nguoinhan2 = _post('nguoinhan2')[$mavanban];
				if(!empty($nguoinhan1)&&empty($nguoinhan2))// chuyển lên cho trưởng phòng
				{
					$nguoinhan = $nguoinhan1;
					$trangthai = 3;
				}
				if(!empty($nguoinhan2))// chuyển lại cho chuyên viên
				{
					$nguoinhan = $nguoinhan2;
					$trangthai = 1;
				}
				$data_capnhat=array(
					'phophong'         => $taikhoan,
					'ykien_pp'         => '',
					'trangthai_chuyen' => ($trangthai_chuyen==8)?8:(($nguoinhan1)?3:1),
					'chuyenvien'       => ($trangthai==1)?$nguoinhan:$thongtin['chuyenvien'],
					'ykien_cv'         => ($trangthai==1)?$ykien:'',
					'truongphong'      => ($trangthai==3)?$nguoinhan:$thongtin['truongphong'],
					'ykien_tp'         => ($trangthai==3)?$ykien:''
					);
				if(!empty($nguoinhan1)||!empty($nguoinhan2))
				{
					$data=array(
						'FK_iMaVBDi'    => $mavanban,
						'FK_iMaCB_Gui'  => $taikhoan,
						'FK_iMaCB_Nhan' => $nguoinhan,
						'sThoiGian'     => date('Y-m-d H:i:s'),
						'sYKien'        => $ykiengoc
					);
					$this->Mdanhmuc->themDuLieu('tbl_luuvet_vbdi',$data);
					$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
				}
				break;
			case '3':
				$nguoinhan1 = _post('nguoinhan1')[$mavanban];
				$nguoinhan2 = _post('nguoinhan2')[$mavanban];
				$nguoinhan3 = _post('nguoinhan3')[$mavanban];
				$nguoinhan4 = _post('nguoinhan4')[$mavanban];
				$nguoinhan5 = _post('nguoinhan5')[$mavanban];
				$nguoinhan6 = _post('nguoinhan6')[$mavanban];
				$data_capnhat=array(
					'chuyenvien'         => ($nguoinhan5)?$nguoinhan5:$thongtin['chuyenvien'],
					'ykien_cv'           => ($nguoinhan5)?$ykien:'',
					'phophong'           => ($nguoinhan4)?$nguoinhan4:$thongtin['phophong'],
					'ykien_pp'           => ($nguoinhan4)?$ykien:'',
					'truongphong'        => $taikhoan,
					'ykien_tp'           => '',
					'bophantonghop'      => ($nguoinhan3)?$nguoinhan3:$thongtin['bophantonghop'],
					'ykien_bpth'         => ($nguoinhan3)?$ykien:'',
					'trangthai_xem_bpth' => 1,
					'phogiamdoc'         => ($nguoinhan2)?$nguoinhan2:$thongtin['phogiamdoc'],
					'ykien_pgd'          => ($nguoinhan2)?$ykien:'',
					'trangthai_xem_pgd'  => 1,
					'giamdoc'            => ($nguoinhan1)?$nguoinhan1:$thongtin['giamdoc'],
					'ykien_gd'           => ($nguoinhan1)?$ykien:'',
					'trangthai_xem_gd'   => 1,
					'chanhvanphong'      => ($nguoinhan6)?$nguoinhan6:$thongtin['chanhvanphong'],
					'ykien_cvp'          => ($nguoinhan6)?$ykien:'',
					'trangthai_xem_cvp'  => 1,
					'trangthai_chuyen' 	 => ($trangthai_chuyen==8)?8:(($nguoinhan4)?2:(($nguoinhan5)?1:8)),
					);
				if(!empty($nguoinhan1))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan1,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				
				if(!empty($nguoinhan2))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan2,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan3))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan3,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan4))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan4,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan5))
				{
					$data_them[4]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan5,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan6))
				{
					$data_them[5]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan6,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan1)||!empty($nguoinhan2)||!empty($nguoinhan3)||!empty($nguoinhan4)||!empty($nguoinhan5)||!empty($nguoinhan6))
				{
					$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
					if($kiemtra>0)
					{
						$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
					}
				}
				break;
			case '33':
				$data_capnhat=array(
					'ykien_cv'           => ($thongtin['chuyenvien'])?$ykien:'',
					'ykien_pp'           => ($thongtin['phophong'])?$ykien:'',
					'ykien_tp'           => ($thongtin['truongphong'])?$ykien:'',
					'ykien_pcc'          => ($thongtin['phochicuc'])?$ykien:'',
					'ykien_tcc'			 => ($thongtin['truongchicuc'])?$ykien:'',
					'ykien_pgd'          => ($thongtin['phogiamdoc'])?$ykien:'',
					'trangthai_xem_pgd'  => 1,
					'ykien_gd'           => ($thongtin['giamdoc'])?$ykien:'',
					'trangthai_xem_gd'   => 1,
					'ykien_cvp'          => ($thongtin['chanhvanphong'])?$ykien:'',
					'trangthai_xem_cvp'  => 1,
					'trangthai_xem_bpth' => 3
					);
				if(!empty($thongtin['chuyenvien']))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['chuyenvien'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phophong']))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phophong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongphong']))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongphong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phochicuc']))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phochicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongchicuc']))
				{
					$data_them[4]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongchicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phogiamdoc']))
				{
					$data_them[5]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phogiamdoc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['giamdoc']))
				{
					$data_them[6]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['giamdoc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['chanhvanphong']))
				{
					$data_them[7]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['chanhvanphong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
				}
				break;
			case '333':
				$nguoinhan1 = _post('nguoinhan1')[$mavanban];
				$nguoinhan2 = _post('nguoinhan2')[$mavanban];
				$nguoinhan3 = _post('nguoinhan3')[$mavanban];
				$nguoinhan4 = _post('nguoinhan4')[$mavanban];
				$data_capnhat=array(
					'chuyenvien'		=> ($nguoinhan4)?$nguoinhan4:$thongtin['chuyenvien'],
					'ykien_cv'			=> ($nguoinhan4)?$ykien:'',
					'phophong'			=> ($nguoinhan3)?$nguoinhan3:$thongtin['phophong'],
					'ykien_pp'			=> ($nguoinhan3)?$ykien:'',
					'truongphong'		=> $taikhoan,
					'ykien_tp'			=> '',
					'trangthai_chuyen'	=> ($trangthai_chuyen==8)?8:(($nguoinhan3)?2:(($nguoinhan4)?1:8)),
					'phochicuc'			=> ($nguoinhan2)?$nguoinhan2:$thongtin['phochicuc'],
					'ykien_pcc'			=> ($nguoinhan2)?$ykien:'',
					'truongchicuc'		=> ($nguoinhan1)?$nguoinhan1:$thongtin['truongchicuc'],
					'ykien_tcc'			=> ($nguoinhan1)?$ykien:''
					);
				if(!empty($nguoinhan1))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan1,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				
				if(!empty($nguoinhan2))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan2,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan3))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan3,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan4))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan4,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan1)||!empty($nguoinhan2)||!empty($nguoinhan3)||!empty($nguoinhan4))
				{
					$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
					if($kiemtra>0)
					{
						$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
					}
					break;
				}
				
			case '4':
				$nguoinhan1 = _post('nguoinhan1')[$mavanban];
				$nguoinhan2 = _post('nguoinhan2')[$mavanban];
				$nguoinhan3 = _post('nguoinhan3')[$mavanban];
				$nguoinhan4 = _post('nguoinhan4')[$mavanban];
				$data_capnhat=array(
					'chuyenvien'         => ($nguoinhan4)?$nguoinhan4:$thongtin['chuyenvien'],
					'ykien_cv'           => ($nguoinhan4)?$ykien:'',
					'phophong'           => ($nguoinhan3)?$nguoinhan3:$thongtin['phophong'],
					'ykien_pp'           => ($nguoinhan3)?$ykien:'',
					'truongphong'        => ($nguoinhan2)?$nguoinhan2:$thongtin['truongphong'],
					'ykien_tp'           => ($nguoinhan2)?$ykien:'',
					'ykien_pcc'          => '',
					'truongchicuc'       => ($nguoinhan1)?$nguoinhan1:$thongtin['truongchicuc'],
					'ykien_tcc'          => ($nguoinhan1)?$ykien:''
					);
				if(!empty($nguoinhan1))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan1,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				
				if(!empty($nguoinhan2))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan2,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan3))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan3,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan4))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan4,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
				}
				break;
			case '5':
				$nguoinhan1 = _post('nguoinhan1')[$mavanban];
				$nguoinhan2 = _post('nguoinhan2')[$mavanban];
				$nguoinhan3 = _post('nguoinhan3')[$mavanban];
				$nguoinhan4 = _post('nguoinhan4')[$mavanban];
				$nguoinhan5 = _post('nguoinhan5')[$mavanban];
				$nguoinhan6 = _post('nguoinhan6')[$mavanban];
				$nguoinhan7 = isset(_post('nguoinhan7')[$mavanban])?_post('nguoinhan7')[$mavanban]:'';
				$nguoinhan8 = _post('nguoinhan8')[$mavanban];
				$data_capnhat=array(
					'chuyenvien'         => ($nguoinhan5)?$nguoinhan5:$thongtin['chuyenvien'],
					'ykien_cv'           => ($nguoinhan5)?$ykien:'',
					'phophong'           => ($nguoinhan4)?$nguoinhan4:$thongtin['phophong'],
					'ykien_pp'           => ($nguoinhan4)?$ykien:'',
					'truongphong'        => ($nguoinhan8)?$nguoinhan8:$thongtin['truongphong'],
					'ykien_tp'           => ($nguoinhan8)?$ykien:'',
					'phochicuc'          => ($nguoinhan7)?$nguoinhan7:$thongtin['truongphong'],
					'ykien_pcc'          => ($nguoinhan7)?$ykien:'',
					'ykien_tcc'          => '',
					'bophantonghop'      => ($nguoinhan3)?$nguoinhan3:$thongtin['bophantonghop'],
					'ykien_bpth'         => ($nguoinhan3)?$ykien:'',
					'trangthai_xem_bpth' => 1,
					'phogiamdoc'         => ($nguoinhan2)?$nguoinhan2:$thongtin['phogiamdoc'],
					'ykien_pgd'          => ($nguoinhan2)?$ykien:'',
					'trangthai_xem_pgd'  => 1,
					'giamdoc'            => ($nguoinhan1)?$nguoinhan1:$thongtin['giamdoc'],
					'ykien_gd'           => ($nguoinhan1)?$ykien:'',
					'trangthai_xem_gd'   => 1,
					'chanhvanphong'      => ($nguoinhan6)?$nguoinhan6:$thongtin['chanhvanphong'],
					'ykien_cvp'          => ($nguoinhan6)?$ykien:'',
					'trangthai_xem_cvp'  => 1
					);
				if(!empty($nguoinhan1))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan1,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				
				if(!empty($nguoinhan2))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan2,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan3))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan3,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan4))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan4,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan5))
				{
					$data_them[4]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan5,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan6))
				{
					$data_them[5]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan6,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan7))
				{
					$data_them[6]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan7,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($nguoinhan8))
				{
					$data_them[7]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhan8,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
				}
				break;
			case '6':
				$data_capnhat=array(
					'ykien_cv'           => ($thongtin['chuyenvien'])?$ykien:'',
					'ykien_pp'           => ($thongtin['phophong'])?$ykien:'',
					'ykien_tp'           => ($thongtin['truongphong'])?$ykien:'',
					'ykien_pcc'          => ($thongtin['phochicuc'])?$ykien:'',
					'ykien_tcc'          => ($thongtin['truongchicuc'])?$ykien:'',
					'ykien_bpth'         => ($thongtin['bophantonghop'])?$ykien:'',
					'trangthai_xem_bpth' => 1,
					'ykien_gd'           => ($thongtin['giamdoc'])?$ykien:'',
					'trangthai_xem_gd'   => 1,
					'ykien_cvp'          => ($thongtin['chanhvanphong'])?$ykien:'',
					'trangthai_xem_cvp'  => 1,
					'trangthai_xem_pgd'  => 3
					);
				if(!empty($thongtin['chuyenvien']))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['chuyenvien'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phophong']))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phophong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongphong']))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongphong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phochicuc']))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phochicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongchicuc']))
				{
					$data_them[4]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongchicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['bophantonghop']))
				{
					$data_them[5]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['bophantonghop'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['giamdoc']))
				{
					$data_them[6]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['giamdoc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['chanhvanphong']))
				{
					$data_them[7]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['chanhvanphong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
				}
				break;
			case '7':
				$data_capnhat=array(
					'ykien_cv'           => ($thongtin['chuyenvien'])?$ykien:'',
					'ykien_pp'           => ($thongtin['phophong'])?$ykien:'',
					'ykien_tp'           => ($thongtin['truongphong'])?$ykien:'',
					'ykien_pcc'          => ($thongtin['phochicuc'])?$ykien:'',
					'ykien_tcc'          => ($thongtin['truongchicuc'])?$ykien:'',
					'ykien_bpth'         => ($thongtin['bophantonghop'])?$ykien:'',
					'trangthai_xem_bpth' => 1,
					'ykien_pgd'          => ($thongtin['phogiamdoc'])?$ykien:'',
					'trangthai_xem_pgd'  => 1,
					'ykien_cvp'          => ($thongtin['chanhvanphong'])?$ykien:'',
					'trangthai_xem_cvp'  => 1,
					'trangthai_xem_gd'   => 3
					);
				if(!empty($thongtin['chuyenvien']))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['chuyenvien'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phophong']))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phophong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongphong']))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongphong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phochicuc']))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phochicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongchicuc']))
				{
					$data_them[4]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongchicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phogiamdoc']))
				{
					$data_them[5]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phogiamdoc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['bophantonghop']))
				{
					$data_them[6]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['bophantonghop'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['chanhvanphong']))
				{
					$data_them[7]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['chanhvanphong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
				}
				break;
			default:
				# code...
				break;
		}
	}

}

/* End of file Cvanbandichoxuly.php */
/* Location: ./application/controllers/vanbandi/Cvanbandichoxuly.php */