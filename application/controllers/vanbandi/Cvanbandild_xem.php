<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandild_xem extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$vanban            = $this->DSVanBanXem($taikhoan);
		// pr($ten_trangthai_xem);
		$ds                = $vanban['dulieu'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['trangthai'] = $trangthai;
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách văn bản để xem';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandild_xem';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBanXem($taikhoan) 
	{
		$trangthai 						= _get('trangthai');
		if(!empty($trangthai))
		{
			$trangthai = $trangthai;
		}
		else{
			$trangthai = 1; // 1:là chờ xử lý
		}
		$config['base_url']             = base_url().'vanbandild_xem';
		$config['total_rows']           = $this->Mvanbandi->demLDSo_Xem($taikhoan);

		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandild_xem');
      	}
		$data['dulieu']		= $this->Mvanbandi->layLDSo_Xem($taikhoan,$config['per_page'], $data['page']);
		$data['phantrang']	= $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cvanbandild_xem.php */
/* Location: ./application/controllers/vanbandi/Cvanbandild_xem.php */