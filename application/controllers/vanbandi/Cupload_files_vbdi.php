<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupload_files_vbdi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		//ini_set('memory_limit', '2048M'); //2GB
		$data['content'] = '';
		$mavbdi = _get('ma');
		$isodi = _get('isodi');
		$loai = _get('loai');		
		$data['isodi'] = $isodi;
		if(_post('upload'))
		{
			$data['content'] = $this->upload($mavbdi, $isodi, $loai);
		}
		$data['title']    = 'Phát hành văn bản đi - Sở Ngoại vụ Hà Nội';
		$data['url']      = base_url();
		$this->parser->parse('vanbandi/Vupload_files_vbdi',$data);
	}
	public function upload($mavbdi, $isodi, $loai)
	{
		$dir  = 'doc_vabandi_'.date('Y');
        $name = $_FILES['files']['name'];
        $tong = count($name);
		
		if(!empty($name[0]))
		{
            $file = $_FILES;
            for ($i = 0; $i < $tong; $i++) {
				$_FILES['files']['name']     = $file['files']['name'][$i];
				$_FILES['files']['type']     = $file['files']['type'][$i];
				$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
				$_FILES['files']['error']    = $file['files']['error'][$i];
				$_FILES['files']['size']     = $file['files']['size'][$i];

				if(is_dir($dir)==false){
					mkdir($dir,0777, true);		// Create directory if it does not exist
				}
				$time= time();
				$file_name = $_FILES['files']['name'];
				$dinhdang = substr($file_name,-3);
				$tentep    = explode('_', $file_name);
				$sodi      = explode('.', $tentep[0]);				
				
				if ($sodi[0] != $isodi) {					
					echo '<script language="javascript">';
					echo 'alert("Tên file và số văn bản không khớp!\nVui lòng chọn lại file.")';
					echo '</script>';
					return messagebox('Tên file và số văn bản không khớp! ','danger');
				}
				
				if ($dinhdang != 'pdf') {					
					echo '<script language="javascript">';
					echo 'alert("Định dạng file không đúng!\nVui lòng chọn lại file pdf.")';
					echo '</script>';
					return messagebox('Định dạng file không đúng! ','danger');
				}
				
				if(!strstr($sodi[0],'-')){
					$thongtin  = $this->Mdanhmuc->layDuLieu_sendMail($sodi[0],$loai);
				}
				else{
					$thongtin  = '';
				}	
				
				
				if ($thongtin[0]['iGuiMail'] == 1) {					
					echo '<script language="javascript">';
					echo 'alert("Văn bản đã được phát hành.")';
					echo '</script>';
					return messagebox('Văn bản đã được phát hành! ','danger');
				} else {	
					// return messagebox('EMAIL CHƯA ĐƯỢC GỬI! ','danger');
				}
				
				if ($thongtin[0]['PK_iMaVBDi'] == '' || $thongtin[0]['PK_iMaVBDi'] != $mavbdi) {					
					echo '<script language="javascript">';
					echo 'alert("Chọn file chưa đúng!\nVui lòng chọn lại file và nhấn gửi email.")';
					echo '</script>';
					return messagebox('Chọn file chưa đúng! ','danger');
				}
				
				if(!empty($thongtin))
				{					
					foreach ($thongtin as $key => $value) {
						$data=array(
							'FK_iMaVBDi' => $thongtin[$key]['PK_iMaVBDi'],
							'sTenFile'   => $i.$time.'_vbdi_'.clear($file_name),
							'sDuongDan'  => $dir.'/'.$i.$time.'_vbdi_'.clear($file_name),
							'sThoiGian'  => date('Y-m-d H:i:s'),
							'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
						$this->Mdanhmuc->themDuLieu('tbl_files_vbdi',$data);
						$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$thongtin[$key]['PK_iMaVBDi'],'tbl_vanbandi','iFile',1);
					}
				}
				$config['upload_path']   = $dir;
				$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
				$config['overwrite']     = true;
				$config['file_name']     = $i.$time.'_vbdi_'.clear($file_name);
				$this->load->library('upload');
             	$this->upload->initialize($config);
                $checkResult = $this->upload->do_upload('files');
                $fileData = $this->upload->data();
                if(!empty($thongtin))
				{	$k = 0;
					foreach ($thongtin as $key => $value) {
						if ($thongtin[$key]['iGuiMail'] == 1) {	
							continue;
						}
						$this->guiMail($thongtin[$key]['PK_iMaVBDi'], $thongtin[$key]['iGuiMail']);// truyền mã văn bản lấy từ số xuống
						$k = $k + 1;
					}
				}
            }
			if ($k > 0){
				return messagebox('Gửi email THÀNH CÔNG!','info');
			} else {
				return messagebox('Gửi email thất bại!','danger');
			}
        }
        else
        {
        	return messagebox('Bạn chưa chọn File!','danger');
        }
	}
	public function guiMail($ma, $iGuiMail)
	{
		$thongtin = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		$this->createXML($ma);
		$this->sendMail($ma, $iGuiMail);
		if(!empty($thongtin['FK_iMaDV_Ngoai']))
		{
			$this->createXML_N($ma);
			$this->sendMail_N($ma);
		}
	}
	public function createXML_N($ma)
	{
		$dir 	  = 'tmpxml/';
		$xml 	  = new DOMDocument("1.0",'UTF-8');

		$STRMADONVI_NG = $xml->createElement('STRMADONVI');
		$STRMADONVI_NG_Text = $xml->createTextNode('000.00.09.H26');
		$STRMADONVI_NG->appendChild($STRMADONVI_NG_Text);

		$STRTENDONVI_NG = $xml->createElement('STRTENDONVI');
		$STRTENDONVI_NG_Text = $xml->createTextNode('Sở Ngoại vụ Hà Nội');
		$STRTENDONVI_NG->appendChild($STRTENDONVI_NG_Text);

		$thongtin   = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		$noinhan    = '';
		if(!empty($thongtin) && !empty($thongtin['FK_iMaDV_Ngoai']))
		{
			$noinhan    = explode(',', $thongtin['FK_iMaDV_Ngoai']);
		}
		if(!empty($noinhan)){
			$thongtin_nhan = $this->Mvanbandi->layTT_DV($noinhan);	
		}
		else{
			$thongtin_nhan = '';
		}

		$NOINHANVANBAN 	= $xml->createElement("NOINHANVANBAN");
		if(!empty($thongtin_nhan)){
			$i=1;
			foreach ($thongtin_nhan as $key => $value) {
				$STRMADONVI_NN = $xml->createElement('STRMADONVI');
				$STRMADONVI_NN_Text = $xml->createTextNode($value['sMaDinhDanh']);
				$STRMADONVI_NN->appendChild($STRMADONVI_NN_Text);

				$STRTENDONVI_NN = $xml->createElement('STRTENDONVI');
				$STRTENDONVI_NN_Text = $xml->createTextNode($value['sTenDV']);
				$STRTENDONVI_NN->appendChild($STRTENDONVI_NN_Text);

				$STRDIACHI_NN = $xml->createElement('STRDIACHI');
				$STRDIACHI_NN_Text = $xml->createTextNode($value['sDiaChi']);
				$STRDIACHI_NN->appendChild($STRDIACHI_NN_Text);

				$STRDIENTHOAI_NN = $xml->createElement('STRDIENTHOAI');
				$STRDIENTHOAI_NN_Text = $xml->createTextNode($value['sSDT']);
				$STRDIENTHOAI_NN->appendChild($STRDIENTHOAI_NN_Text);

				$STRWEBSITE_NN = $xml->createElement('STRWEBSITE');
				$STRWEBSITE_NN_Text = $xml->createTextNode($value['sWeb']);
				$STRWEBSITE_NN->appendChild($STRWEBSITE_NN_Text);

				$STREMAIL_NN = $xml->createElement('STREMAIL');
				$STREMAIL_NN_Text = $xml->createTextNode($value['sMail']);
				$STREMAIL_NN->appendChild($STREMAIL_NN_Text);

				$NOINHAN = $xml->createElement('NOINHAN');
				$ID = $xml->createAttribute('ID');
				$ID->value = $i++;
				$NOINHAN->appendChild($STRMADONVI_NN);
				$NOINHAN->appendChild($STRTENDONVI_NN);
				$NOINHAN->appendChild($STRDIACHI_NN);
				$NOINHAN->appendChild($STRDIENTHOAI_NN);
				$NOINHAN->appendChild($STRWEBSITE_NN);
				$NOINHAN->appendChild($STREMAIL_NN);
				$NOINHAN->appendChild($ID);
				$NOINHANVANBAN->appendChild($NOINHAN);
			}
		}else{
				$STRMADONVI_NN = $xml->createElement('STRMADONVI');
				$STRMADONVI_NN_Text = $xml->createTextNode('');
				$STRMADONVI_NN->appendChild($STRMADONVI_NN_Text);

				$STRTENDONVI_NN = $xml->createElement('STRTENDONVI');
				$STRTENDONVI_NN_Text = $xml->createTextNode('');
				$STRTENDONVI_NN->appendChild($STRTENDONVI_NN_Text);

				$STRDIACHI_NN = $xml->createElement('STRDIACHI');
				$STRDIACHI_NN_Text = $xml->createTextNode('');
				$STRDIACHI_NN->appendChild($STRDIACHI_NN_Text);

				$STRDIENTHOAI_NN = $xml->createElement('STRDIENTHOAI');
				$STRDIENTHOAI_NN_Text = $xml->createTextNode('');
				$STRDIENTHOAI_NN->appendChild($STRDIENTHOAI_NN_Text);

				$STRWEBSITE_NN = $xml->createElement('STRWEBSITE');
				$STRWEBSITE_NN_Text = $xml->createTextNode('');
				$STRWEBSITE_NN->appendChild($STRWEBSITE_NN_Text);

				$STREMAIL_NN = $xml->createElement('STREMAIL');
				$STREMAIL_NN_Text = $xml->createTextNode('');
				$STREMAIL_NN->appendChild($STREMAIL_NN_Text);
				$NOINHAN = $xml->createElement('NOINHAN');
				$ID = $xml->createAttribute('ID');
				$ID->value = '0';
				$NOINHAN->appendChild($STRMADONVI_NN);
				$NOINHAN->appendChild($STRTENDONVI_NN);
				$NOINHAN->appendChild($STRDIACHI_NN);
				$NOINHAN->appendChild($STRDIENTHOAI_NN);
				$NOINHAN->appendChild($STRWEBSITE_NN);
				$NOINHAN->appendChild($STREMAIL_NN);
				$NOINHAN->appendChild($ID);
				$NOINHANVANBAN->appendChild($NOINHAN);
		}

		$STRLOAIVANBAN = $xml->createElement('STRLOAIVANBAN');
		$STRLOAIVANBAN_Text = $xml->createTextNode($thongtin['sTenLVB']);
		$STRLOAIVANBAN->appendChild($STRLOAIVANBAN_Text);

		$STRKYHIEU = $xml->createElement('STRKYHIEU');
		$STRKYHIEU_Text = $xml->createTextNode($thongtin['iSoVBDi'].'/'.$thongtin['sKyHieu']);
		$STRKYHIEU->appendChild($STRKYHIEU_Text);

		$STRTRICHYEU = $xml->createElement('STRTRICHYEU');
		$STRTRICHYEU_Text = $xml->createTextNode($thongtin['sMoTa']);
		$STRTRICHYEU->appendChild($STRTRICHYEU_Text);

		$STRNGAYKY = $xml->createElement('STRNGAYKY');
		$STRNGAYKY_Text = $xml->createTextNode(date_select($thongtin['sNgayVBDi']));
		$STRNGAYKY->appendChild($STRNGAYKY_Text);

		$STRNGUOIKY = $xml->createElement('STRNGUOIKY');
		$STRNGUOIKY_Text = $xml->createTextNode($thongtin['sHoTen']);
		$STRNGUOIKY->appendChild($STRNGUOIKY_Text);

		$STRNOIGUI = $xml->createElement('STRNOIGUI');
		$STRNOIGUI_Text = $xml->createTextNode('Sở Ngoại vụ Hà Nội');
		$STRNOIGUI->appendChild($STRNOIGUI_Text);

		$INTSOTO = $xml->createElement('INTSOTO');
		$INTSOTO_Text = $xml->createTextNode($thongtin['iSoTrang']);
		$INTSOTO->appendChild($INTSOTO_Text);

		$STRHANXULY = $xml->createElement('STRHANXULY');
		$STRHANXULY_Text = $xml->createTextNode('');
		$STRHANXULY->appendChild($STRHANXULY_Text);

		$STRTHUOCLOAIVANBAN = $xml->createElement('STRTHUOCLOAIVANBAN');
		$STRTHUOCLOAIVANBAN_Text = $xml->createTextNode('0');
		$STRTHUOCLOAIVANBAN->appendChild($STRTHUOCLOAIVANBAN_Text);
		$thongtin_vbden = '';
		if($thongtin['iSoDen']>0)
		{
			$ma = 1;
			$ten= 'Đã trả lời';
			$soden = explode(', ',$thongtin['iSoDen']);
			$thongtin_vbden = $this->Mvanbandi->layTT_VBDen($soden);
			$PHANHOIVANBAN 	= $xml->createElement("PHANHOIVANBAN");
			if(!empty($thongtin_vbden)){
				$ii=1;
				foreach ($thongtin_vbden as $key => $val) {
					$STRSOKYHIEU_VB = $xml->createElement('STRSOKYHIEU');
					$STRSOKYHIEU_VB_Text = $xml->createTextNode($val['sKy_Hieu']);
					$STRSOKYHIEU_VB->appendChild($STRSOKYHIEU_VB_Text);

					$STRNGAYKY_VB = $xml->createElement('STRNGAYKY');
					$STRNGAYKY_VB_Text = $xml->createTextNode($val['sNgay_Ky']);
					$STRNGAYKY_VB->appendChild($STRNGAYKY_VB_Text);

					$STRMACOQUANBANHANH_VB = $xml->createElement('STRMACOQUANBANHANH');
					$STRMACOQUANBANHANH_VB_Text = $xml->createTextNode($val['sMaCoQuan']);
					$STRMACOQUANBANHANH_VB->appendChild($STRMACOQUANBANHANH_VB_Text);

					$STRCOQUANBANHANH_VB = $xml->createElement('STRCOQUANBANHANH');
					$STRCOQUANBANHANH_VB_Text = $xml->createTextNode($val['sTenCoQuan']);
					$STRCOQUANBANHANH_VB->appendChild($STRCOQUANBANHANH_VB_Text);

					$VANBAN = $xml->createElement('VANBAN');
					$ID_VB  = $xml->createAttribute('ID');
					$ID_VB->value = $ii++;
					$VANBAN->appendChild($STRSOKYHIEU_VB);
					$VANBAN->appendChild($STRNGAYKY_VB);
					$VANBAN->appendChild($STRMACOQUANBANHANH_VB);
					$VANBAN->appendChild($STRCOQUANBANHANH_VB);
					$VANBAN->appendChild($ID_VB);
					$PHANHOIVANBAN->appendChild($VANBAN);
				}
			}

		}
		else{
			$ma =0;
			$ten='Văn bản mới';
		}
		$STRMANGHIEPVU = $xml->createElement('STRMANGHIEPVU');
		$STRMANGHIEPVU_Text = $xml->createTextNode($ma);
		$STRMANGHIEPVU->appendChild($STRMANGHIEPVU_Text);

		$STRTENNGHIEPVU = $xml->createElement('STRTENNGHIEPVU');
		$STRTENNGHIEPVU_Text = $xml->createTextNode($ten);
		$STRTENNGHIEPVU->appendChild($STRTENNGHIEPVU_Text);

		$STRLYDO = $xml->createElement('STRLYDO');

		$STRDONVIXULY = $xml->createElement('STRDONVIXULY');

		$STRCANBOXULY = $xml->createElement('STRCANBOXULY');

		$STRDIENTHOAI = $xml->createElement('STRDIENTHOAI');

		$STREMAIL = $xml->createElement('STREMAIL');

		$book 	= $xml->createElement("EXPORTMAIL");
		$NOIGUI 	= $xml->createElement("NOIGUI");
		$NOIGUI->appendChild($STRMADONVI_NG);
		$NOIGUI->appendChild($STRTENDONVI_NG);

		$NOIDUNGVANBAN 	= $xml->createElement("NOIDUNGVANBAN");
		$NOIDUNGVANBAN->appendChild($STRLOAIVANBAN);
		$NOIDUNGVANBAN->appendChild($STRKYHIEU);
		$NOIDUNGVANBAN->appendChild($STRTRICHYEU);
		$NOIDUNGVANBAN->appendChild($STRNGAYKY);
		$NOIDUNGVANBAN->appendChild($STRNGUOIKY);
		$NOIDUNGVANBAN->appendChild($STRNOIGUI);
		$NOIDUNGVANBAN->appendChild($INTSOTO);
		$NOIDUNGVANBAN->appendChild($STRHANXULY);
		$NOIDUNGVANBAN->appendChild($STRTHUOCLOAIVANBAN);

		$NGHIEPVUVANBAN 	= $xml->createElement("NGHIEPVUVANBAN");
		$NGHIEPVUVANBAN->appendChild($STRMANGHIEPVU);
		$NGHIEPVUVANBAN->appendChild($STRTENNGHIEPVU);
		$NGHIEPVUVANBAN->appendChild($STRLYDO);

		$THONGTINNGUOIGUI 	= $xml->createElement("THONGTINNGUOIGUI");
		$THONGTINNGUOIGUI->appendChild($STRDONVIXULY);
		$THONGTINNGUOIGUI->appendChild($STRCANBOXULY);
		$THONGTINNGUOIGUI->appendChild($STRDIENTHOAI);
		$THONGTINNGUOIGUI->appendChild($STREMAIL);

		$book->appendChild($NOIGUI);

		$book->appendChild($NOINHANVANBAN);

		$book->appendChild($NOIDUNGVANBAN);
		$book->appendChild($NGHIEPVUVANBAN);
		if($thongtin['iSoDen']>0 && !empty($thongtin_vbden))
		{
			$book->appendChild($PHANHOIVANBAN);
		}
		$book->appendChild($THONGTINNGUOIGUI);
		
		$xml->appendChild($book);
		$xml->formatOutput = true;
		$xml->save($dir.'XMLOutput_N.sdk') or die("Error");
	}
	public function sendMail_N($ma)
	{
		$thongtin   = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		$file 		= $this->Mvanbandi->layFileLast($ma);
		$name       = 'Sở Ngoại vụ Hà Nội';
		$from_email = 'vanthu_songv@hanoi.gov.vn';
		$subject    = '('.$thongtin['iSoVBDi'].'/'.$thongtin['sKyHieu'].') '.$thongtin['sMoTa']; // nội dung gửi mail
		$message    = $thongtin['sMoTa'];
		$to_email   = 'gnvbdtquatruc@hanoi.gov.vn, vanthu_songv@hanoi.gov.vn';// mail nhận
        //configure email settings
		$config['protocol']  = 'smtp';
		// $config['smtp_host'] = 'mail.thudo.gov.vn';
        // $config['smtp_port'] = '587';
        $config['smtp_host'] = 'ssl://mail.thudo.gov.vn';
        $config['smtp_port'] = '465';
		$config['smtp_user'] = 'vanthu_songv@hanoi.gov.vn';// email gửipthtk_sotc@hanoi.gov.vn
		$config['smtp_pass'] = 'snvhn@2017';// pass cua mail trentinhoc@gmail.com
		$config['mailtype']  = 'html';
		$config['charset']   = 'utf-8';
		$config['wordwrap']  = TRUE;
		$config['newline']   = "\r\n"; //use double quotes
        $this->load->library('email', $config);
        $this->email->initialize($config);
        //send mail
        $this->email->clear(TRUE);
        $this->email->from($from_email, $name);
        $this->email->to($to_email);
        $this->email->subject(catKyTu($subject));
        $this->email->message($message);
        $filename = 'tmpxml/XMLOutput_N.sdk';
        if(!empty($file))
        {
        	$this->email->attach($file['sDuongDan']);
        }
		$this->email->attach($filename);
		if ($this->email->send(FALSE)) {
			$data=array(
					'iGuiMail'    => 1,
					'sNgayGuiMai' => date('Y-m-d H:i:s',time())
		    		);
		    	$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi',$data);
	    }
	}
	public function createXML($ma)
	{
		$thongtin = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		// cáu hình tạo file sdk 
		$xml 	  = new DOMDocument("1.0",'UTF-8');
		$STRLOAIVANBAN  = $xml->createElement("STRLOAIVANBAN");
		$STRLOAIVANBAN_Text = $xml->createTextNode($thongtin['sTenLVB']);
		$STRLOAIVANBAN->appendChild($STRLOAIVANBAN_Text);
		
		$STRKYHIEU   	= $xml->createElement("STRKYHIEU");
		$STRKYHIEU_Text = $xml->createTextNode($thongtin['iSoVBDi'].'/'.$thongtin['sKyHieu']);
		$STRKYHIEU->appendChild($STRKYHIEU_Text);
		 
		$STRTRICHYEU  = $xml->createElement("STRTRICHYEU");
		$STRTRICHYEU_Text = $xml->createTextNode($thongtin['sMoTa']);
		$STRTRICHYEU->appendChild($STRTRICHYEU_Text);
		
		$STRNGAYKY  = $xml->createElement("STRNGAYKY");
		$STRNGAYKY_Text = $xml->createTextNode(date_select($thongtin['sNgayVBDi']));
		$STRNGAYKY->appendChild($STRNGAYKY_Text);
		
		$STRNGUOIKY  = $xml->createElement("STRNGUOIKY");
		$STRNGUOIKY_Text = $xml->createTextNode($thongtin['sHoTen']);
		$STRNGUOIKY->appendChild($STRNGUOIKY_Text);
		
		$STRCHUCDANH  = $xml->createElement("STRCHUCDANH");
		$STRCHUCDANH_Text = $xml->createTextNode($thongtin['sTenCV']);
		$STRCHUCDANH->appendChild($STRCHUCDANH_Text);
		
		$STRNOIGUI  = $xml->createElement("STRNOIGUI");
		$STRNOIGUI_Text = $xml->createTextNode('Sở Ngoại vụ Hà Nội');
		$STRNOIGUI->appendChild($STRNOIGUI_Text);
		
		$STRNGAYHOP  = $xml->createElement("STRNGAYHOP");
		$STRNGAYHOP_Text = $xml->createTextNode(($thongtin['sNgayMoi']!='0000-00-00')?date_select($thongtin['sNgayMoi']):'');
		$STRNGAYHOP->appendChild($STRNGAYHOP_Text);
		
		$STRDIADIEM  = $xml->createElement("STRDIADIEM");
		$STRDIADIEM_Text = $xml->createTextNode($thongtin['sDiaDiemMoi']);
		$STRDIADIEM->appendChild($STRDIADIEM_Text);	
		
		$STRTHOIGIANHOP  = $xml->createElement("STRTHOIGIANHOP");
		$STRTHOIGIANHOP_Text = $xml->createTextNode($thongtin['sGioMoi']);
		$STRTHOIGIANHOP->appendChild($STRTHOIGIANHOP_Text);
		 
		$book 	= $xml->createElement("EXPORTMAIL");
		$book->appendChild($STRLOAIVANBAN);
		$book->appendChild($STRKYHIEU);
		$book->appendChild($STRTRICHYEU);
		$book->appendChild($STRNGAYKY);
		$book->appendChild($STRNGUOIKY);
		$book->appendChild($STRCHUCDANH);
		$book->appendChild($STRNOIGUI);
		$book->appendChild($STRNGAYHOP);
		$book->appendChild($STRDIADIEM);
		$book->appendChild($STRTHOIGIANHOP);
		
		$xml->appendChild($book);
		$xml->formatOutput = true;
		$xml->save("tmpxml/XMLOutput.sdk") or die("Error");
	}
	function sendMail($ma, $iGuiMail)
	{
		$thongtin   = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		$file 		= $this->Mvanbandi->layFileLast($ma);
		if(!empty($thongtin['sNoiNhan']))
		{
			$file 		= $this->Mvanbandi->layFileLast($ma);
			$name       = 'Sở Ngoại vụ Hà Nội';
			$from_email = 'vanthu_songv@hanoi.gov.vn';
			$subject    = '('.$thongtin['iSoVBDi'].'/'.$thongtin['sKyHieu'].') '.$thongtin['sMoTa']; // nội dung gửi mail
			$message    = $thongtin['sMoTa'];
			$to_email   = $thongtin['sNoiNhan'];// mail nhận
			
	        //configure email settings
			$config['protocol']  = 'smtp';
			// $config['smtp_host'] = 'mail.thudo.gov.vn';
			// $config['smtp_port'] = '587';
			$config['smtp_host'] = 'ssl://mail.thudo.gov.vn';
			$config['smtp_port'] = '465';
			$config['smtp_user'] = 'vanthu_songv@hanoi.gov.vn';// email gửipthtk_sotc@hanoi.gov.vn
			$config['smtp_pass'] = 'snvhn@2017';// pass cua mail trentinhoc@gmail.com
			$config['mailtype']  = 'html';
			$config['charset']   = 'utf-8';
			$config['wordwrap']  = TRUE;
			$config['newline']   = "\r\n"; //use double quotes
	        $this->load->library('email', $config);
	        $this->email->initialize($config);
			
	        //send mail
	        $this->email->clear(TRUE);
	        $this->email->from($from_email, $name);
	        $this->email->to($to_email);
	        $this->email->subject(catKyTu($subject));
	        $this->email->message($message);
	        $filename = 'tmpxml/XMLOutput.sdk';
	        if(!empty($file))
	        {
	        	$this->email->attach($file['sDuongDan']);
	        }
				
			$this->email->attach($filename);
			
			if ($this->email->send(FALSE)) {
		    	$data=array(
					'iGuiMail'    => 1,
					'sNgayGuiMai' => date('Y-m-d H:i:s',time())
		    		);
		    	$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi',$data);
		    } else {
				echo 'error!';
				echo '<script language="javascript">';
				echo 'alert("FAIL!!!\n Có lỗi trong quá trình gửi email!")';
				echo '</script>';
				return messagebox('Có lỗi trong quá trình gửi email!','danger');
			}
		}
	}

}

/* End of file Cupload_files_vbdi.php */
/* Location: ./application/controllers/vanbandi/Cupload_files_vbdi.php */