<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/controllers/vanbandi/OfficeConverter.php';
use NcJoes\OfficeConverter\OfficeConverter;
class Cvanbandichoxuly_ld extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
		$this->load->library('user_agent');

	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$data['lanhdaoso'] 	  = $this->Mvanbandi->layNguoiKy();
		if(_post('dongy'))
		{
			$data['content'] = $this->dongy($taikhoan);
		}
		if(_post('kydientu'))
		{
			$data['content'] = $this->kydientu($taikhoan);
		}
		if(_post('tralai'))
		{
			$data['content'] = $this->tralai();
		}
		$vanban            = $this->DSVanBan($taikhoan);
		// pr($ten_trangthai_xem);
		$ds                = $vanban['info'];
		$data['count1'] 			= $vanban['count1'];
		$data['count2'] 			= $vanban['count2'];
		$data['count3'] 			= $vanban['count3'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$trangthai 						= _get('trangthai');
		if(!empty($trangthai))
		{
			$trangthai = $trangthai;
		}
		else{
			$trangthai = 1; // 1:là chờ xử lý
		}
		if($this->agent->is_mobile()){
			$data['giaodienmobie'] = 1;
		}else{
			$data['giaodienmobie'] = 2;
		}
		$data['ci_session'] = $this->session->session_id;
		$data['trangthai'] = $trangthai;
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách VB đi chờ xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandichoxuly_ld';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function tralai()
	{
		$mavanban = _post('tralai');
		$ykien    = nl2br(_post('ykien_'.$mavanban));
		$taikhoan = $this->_session['PK_iMaCB'];
		$ma       = $this->Mvanbandi->layMaMoiNhat_Cu($mavanban,$taikhoan);
		$canbophong = $this->Mvanbandi->laynguoitralai($mavanban,$ma['PK_iMaNhatKy']);
		
		$data['content'] = $this->themDuLieu($mavanban);
		if(!empty($canbophong))
		{
			foreach ($canbophong as $value) {
				$data=array(
					'FK_iMaVBDi'	=> $mavanban,
					'FK_iMaCB_Gui'	=> $taikhoan,
					'FK_iMaCB_Nhan' => $value['FK_iMaCB_Gui'],
					'sYKien'		=> $ykien,
					'sThoiGian'		=> date('Y-m-d H:i:s'),
					'iTrangThai'	=> 1,
					'phong_lanhdao' => 1
				);
				$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
				if($id_insert>0)
				{
					$this->Mvanbandi->capnhatTrangThai_XuLy_TL($mavanban,$taikhoan);
				}
			}
			return messagebox('Văn bản đã được trả lại!','info');
		}
	}
	public function dongy($taikhoan)
	{
		$mavanban = _post('dongy');
		$year = _post('yearup');
		$layma    = $this->Mvanbandi->layMaMoiNhat_CN($mavanban,$taikhoan, $year);//pr($layma);
		//$arr_file    = $this->Mvanbandi->layFileLast($mavanban);
		if(!empty($layma))
		/*{
			$mang_file = explode('.', $arr_file['sDuongDan']);
			$vitri = count($mang_file) - 1;
			$inputFile = realpath($arr_file['sDuongDan']);
			if($mang_file[$vitri] =='doc'){
				$outputFile = str_replace(".doc",".pdf",$inputFile);
				$duong_dan = str_replace(".doc",".pdf",$arr_file['sDuongDan']);
			}else{
				$outputFile = str_replace(".docx",".pdf",$inputFile);
				$duong_dan = str_replace(".docx",".pdf",$arr_file['sDuongDan']);
			}
			$converter = new OfficeConverter($inputFile);
			$file_ky = str_replace( 'doc_uploads_2019/', '',$duong_dan);
			$converter->convertTo($file_ky);
			//them mới file văn bản đi
		    $files= array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => $arr_file['sTenFile'],
						'sDuongDan'  => $duong_dan,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
			$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_files_vbdi',$files); 
			return messagebox('Duyệt văn bản thành công!','info');
		}*/
		$files= array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => '-------------DUYỆT',
						'sDuongDan'  => 'files/images/gdapproved.png',
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
			//$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_files_vbdi',$files);
			$this->Mvanbandi->capnhatTrangThai_XuLy_LD($layma['PK_iMaNhatKy'], $year); 
			
			$ykien    = nl2br(_post('ykien_'.$mavanban));
			if ($ykien == '') {
				$ykien = 'Duyệt văn bản./.';
			}
			$chidaold = array(
					'FK_iMaVBDi'        => $mavanban,
					'sNoiDung'        => $ykien,
					'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
					'sThoiGian'   => date('Y-m-d H:i:s',time()),
				);
			
			$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
			$data['content'] = $this->themDuLieu($mavanban);
			
			return redirect(base_url().'vanbandichoxuly_ld');
			return messagebox('Duyệt văn bản THÀNH CÔNG!','info');
	}
	/*public function dongy($taikhoan)
	{
		$mavanban = _post('dongy');//echo $mavanban.'<br>'.$taikhoan;
		$layma    = $this->Mvanbandi->layMaMoiNhat_CN($mavanban,$taikhoan);
		$arr_file    = $this->Mvanbandi->layFileLast($mavanban);
		if(!empty($layma))
		{
			$mang_file = explode('.', $arr_file['sDuongDan']);
			$vitri = count($mang_file) - 1;
			$inputFile = realpath($arr_file['sDuongDan']);
			if($mang_file[$vitri] =='doc'){
				$outputFile = str_replace(".doc",".pdf",$inputFile);
				$duong_dan = str_replace(".doc",".pdf",$arr_file['sDuongDan']);
			}else{
				$outputFile = str_replace(".docx",".pdf",$inputFile);
				$duong_dan = str_replace(".docx",".pdf",$arr_file['sDuongDan']);
			}
			//pr($inputFile."<br>".$outputFile);
			$inputFile = "file:///" . $inputFile;
      		$output_file = "file:///" . $outputFile;
      		$this->word2pdf($inputFile,$output_file);
		    
		    //them mới file văn bản đi
		    $files= array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => $arr_file['sTenFile'],
						'sDuongDan'  => $duong_dan,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
			$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_files_vbdi',$files); 
			return messagebox('Duyệt văn bản thành công!','info');
		}
	}*/
	public function kydientu($taikhoan)
	{
		$mavanban = $_POST['PK_iMaVBDi'];
		$layma    = $this->Mvanbandi->layMaMoiNhat_CN($mavanban,$taikhoan);
		if(!empty($layma))
		{
			$arr_chuky_sodt    = $this->Mvanbandi->LayAnhChuKy_soDT($this->session->userdata['vanban']['PK_iMaCB']);
			$inputFile = realpath($_POST['FileName']);
	        $inputchuky = realpath($arr_chuky_sodt['anhchuky']);
	        $url  = 'https://192.168.9.6/SignPDF.asmx?WSDL';
	        //'https://app.sotaichinh.hanoi.gov.vn/SignPDF.asmx';
	        //'https://192.168.9.6/SignPDF.asmx?WSDL';
	        //'http://192.168.9.6/SignPDF.asmx?WSDL';
	        //'https://app.sotaichinh.hanoi.gov.vn/SignPDF.asmx';
	        //"http://14.238.6.14/SignPDF.asmx?WSDL";
	        $context = stream_context_create([
	        'ssl' => [
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true,
	        ]]);
	        $client = new SoapClient($url, [
	        'location' => $url,
	        'uri' => $url,
	        'stream_context' => $context
	        ]);
	        $chuky = file_get_contents($inputchuky);
	        $inPdf = file_get_contents($inputFile);
	        $params=array(
	        'inPdf'=>$inPdf,
	        'messageToBeDisplayed'   =>'Da ky thanh cong!',
	        'phone'=>$arr_chuky_sodt['sodienthoaiky'],
	        'signerName'=>$this->session->userdata['vanban']['sHoTen'],
	        'imgBuff'=>$chuky,
	        'width'=>'150',
	        'height'=>'46',
	        'tsaUrl'=>'http://ca.gov.vn/tsa'
	        );
	        $response = $client->__soapCall('Sign', array($params));
	       
	        $myfile = fopen($inputFile, "w") or die("Unable to open file!");
	        if(isset($response->SignResult)) fwrite($myfile, $response->SignResult);
	        fclose($myfile);
			$kiemtra  = $this->Mvanbandi->capnhatTrangThai_XuLy_LD($layma['PK_iMaNhatKy']); // đã duyệt
			
		}
			
		if(isset($response->SignResult)) return messagebox('Ký điện tử văn bản thành công!','info');
		else return messagebox('Ký điện tử văn bản không thành công!','warning');
	}
	public function DSVanBan($taikhoan) 
	{
		$trangthai 						= _get('trangthai');
		if(!empty($trangthai))
		{
			$trangthai = $trangthai;
		}
		else{
			$trangthai = 1; // 1:là chờ xử lý
		}
		$config['base_url']             = base_url().'vanbandichoxuly_ld?trangthai='.$trangthai;
		if($trangthai==3)
		{
			$config['total_rows']           = $this->Mvanbandi->demVBDi_DaXuLy($taikhoan,$trangthai);
		}
		else{
			$config['total_rows']           = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,$trangthai); // 1:là chờ xử lý 2: trả lại
		}
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandichoxuly_ld');
      	}
      	if($trangthai==3)
		{
			$data['info']       = $this->Mvanbandi->layVBDi_DaXuLy($taikhoan,$trangthai,$config['per_page'], $data['page']);
		}
		else{
			$data['info']       = $this->Mvanbandi->layVBDi_ChoXuLy($taikhoan,$trangthai,$config['per_page'], $data['page']);
		}
		$data['count1'] = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,1);
		$data['count2'] = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,2);
		$data['count3'] = $this->Mvanbandi->demVBDi_DaXuLy($taikhoan,3);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}
	function MakePropertyValue($name,$value,$osm){
	    $oStruct = $osm->Bridge_GetStruct("com.sun.star.beans.PropertyValue");
	    $oStruct->Name = $name;
	    $oStruct->Value = $value;
	    return $oStruct;
    }
    function word2pdf($doc_url, $output_url){
    	$doc_url=str_replace('\\', '/', $doc_url);
    	$output_url=str_replace('\\', '/', $output_url);
    	//echo $output_url.'<br>';
    	//Invoke the OpenOffice.org service manager
	    $osm = new COM("com.sun.star.ServiceManager") or die ("Please be sure that OpenOffice.org is installed.\n");
	    //echo $doc_url;
	    //pr($osm);
	    //Set the application to remain hidden to avoid flashing the document onscreen
	    $args = array($this->MakePropertyValue("Hidden",true,$osm));
	    //Launch the desktop
	    $oDesktop = $osm->createInstance("com.sun.star.frame.Desktop");
	    //Load the .doc file, and pass in the "Hidden" property from above
	    $oWriterDoc = $oDesktop->loadComponentFromURL($doc_url,"_blank", 0, $args);
	    //Set up the arguments for the PDF output
	    $export_args = array($this->MakePropertyValue("FilterName","writer_pdf_Export",$osm));
	    //print_r($export_args);
	    //Write out the PDF
	    $oWriterDoc->storeToURL($output_url,$export_args);
	    $oWriterDoc->close(true);
    }
	public function themDuLieu($mavanban)
	{
		
			$time = time();
			$name = $_FILES['files'.$mavanban]['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_vabandi_'.date('Y'),$time,$mavanban);
				$files = array();
				foreach ($name as $key => $value) {
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}					
					$tmp = str_replace('$', '-', $tmp);
					$tmp = $tmp.'.'.$a[$c];
					
					$files[] = array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_vabandi_'.date('Y').'/di_up_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
				}
				$this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
				
			}
	}
	
	public function upload($dir,$time,$mavanban)
	{
        $fileNumber = count($_FILES['files'.$mavanban]['name']);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files'.$mavanban]['name'][$i]) {
                    #initialization new file data
					$_FILES['files'.$mavanban]['name']     = $file['files'.$mavanban]['name'][$i];
					$_FILES['files'.$mavanban]['type']     = $file['files'.$mavanban]['type'][$i];
					$_FILES['files'.$mavanban]['tmp_name'] = $file['files'.$mavanban]['tmp_name'][$i];
					$_FILES['files'.$mavanban]['error']    = $file['files'.$mavanban]['error'][$i];
					$_FILES['files'.$mavanban]['size']     = $file['files'.$mavanban]['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'di_up_'.$time.'_'.clear($_FILES['files'.$mavanban]['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files'.$mavanban);
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cvanbandichoxuly_ld.php */
/* Location: ./application/controllers/vanbandi/Cvanbandichoxuly_ld.php */