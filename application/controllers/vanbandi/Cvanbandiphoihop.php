<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandiphoihop extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		switch ($quyen) {
			case 7: #PTP
				$trangthai =1;
				if($phongbanHD==12)
				{
					$data['nguoinhan'] = $this->Mtruyennhan->layNguoiNhan(8,NULL,$phongbanHD,$quyendb);//TP
				}
				else{
					$data['nguoinhan'] = $this->Mtruyennhan->layNguoiNhan(8,NULL,$phongbanHD,NULL);//TP
				}
				break;
			case 8: #CV
				$trangthai =2;
				break;
			default: # trạng thái hủy
				$trangthai =3;
				break;
		}
		if(_post('gui'))
		{
			$data['content'] = $this->Gui($trangthai);
		}
		$ds= $this->Mtruyennhan->layVanBanChoXuLy_phoihop($taikhoan);
		// pr($taikhoan);
		$data['trangthai'] = $trangthai;
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách văn bản đi phối hợp chờ xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandiphoihop';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function Gui($trangthai)
	{
		$mavanban   = _post('gui');
		$nguoinhan  = _post('nguoinhan')[$mavanban];
		$ykien 		= _post('ykien')[$mavanban];
		$phoihop 	= _post('maphoihop')[$mavanban];
		$taikhoan   = $this->_session['PK_iMaCB'];

		if(!empty($nguoinhan))
		{
			foreach ($nguoinhan as $key => $val) {
				$mang_them[]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $val,
					'sYKien'		=> $ykien,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'iTrangThai'	=> 2
					);
			}
			$mang_capnhat=array(
					'thoigian_xuly' => date('Y-m-d H:i:s'),
					'noidung_xuly'	=> $ykien,
					'iXuLy'			=> 1
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaPH',$phoihop,'tbl_phoihop_vbdi',$mang_capnhat);
			if($kiemtra>0)
			{
				$this->Mdanhmuc->themnhieuDuLieu('tbl_phoihop_vbdi',$mang_them);
			}
		}
	}
}

/* End of file Cvanbandiphoihop.php */
/* Location: ./application/controllers/vanbandi/Cvanbandiphoihop.php */