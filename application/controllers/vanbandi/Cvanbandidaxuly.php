<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandidaxuly extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$lanhdao    = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($lanhdao as $key => $value) {
			$data['lanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		switch ($quyen) {
			case 3:
				$trangthai    = 3;
                $ten_taikhoan = 'truongphong';
                $ykien        = 'ykien_tp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
				break;
            case 6: #TP
                if($chucvu==15) // chức vụ chi cục trưởng
                {
                    $trangthai    = 5; #CCT
                    $ten_taikhoan = 'truongchicuc';
                    $ykien        = 'ykien_tcc';
                }
                if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
                {
                    $trangthai    = 3;
                    $ten_taikhoan = 'truongphong';
                    $ykien        = 'ykien_tp';
                }
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 7: #PTP
                $trangthai =2;
                $ten_taikhoan       = 'phophong';
                $ykien              = 'ykien_pp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 8: #CV
               
                $trangthai =1;
                $ten_taikhoan       = 'chuyenvien';
                $ykien              = 'ykien_cv';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 10:#CCP
                $trangthai =4; 
                $ten_taikhoan       = 'phochicuc';
                $ykien              = 'ykien_pcc';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            case 11:#TP bên chi cục
                $trangthai =333;
                $ten_taikhoan       = 'truongphong';
                $ykien              = 'ykien_tp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';
                break;
            default:
                redirect('welcome.html');
                break;
        }
		$vanban            = $this->DSVanBan($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách VB đi đã xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandidaxuly';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBan($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude) 
	{
		$config['base_url']             = base_url().'vanbandichoxuly?tinhtrang='.$trangthai_xem.'&chude='.$chude;
		$config['total_rows']           = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandichoxuly');
      	}
		$data['info']       = $this->Mtruyennhan->layVanBanDaXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cvanbandidaxuly.php */
/* Location: ./application/controllers/vanbandi/Cvanbandidaxuly.php */