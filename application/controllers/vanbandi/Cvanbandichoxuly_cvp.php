<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandichoxuly_cvp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$action     = _post('action');
		if(!empty($action))
		{ 
			$maVB = _post('maVB');
			if($quyen==3&&$chucvu==6)
			{
				$this->Mtruyennhan->capnhattrangthai($maVB,'chanhvanphong',$taikhoan,'trangthai_xem_cvp');
			}
		}
		$tinhtrang = _get('tinhtrang');
		if(!empty($tinhtrang))
		{
			$tinhtrang = $tinhtrang;
		}
		else{
			$tinhtrang = 1;
		}
		$data['tinhtrang'] = $tinhtrang;
		$chude             = _get('chude');
		$data['chude']     = $chude;
		switch ($quyen) {
			case 3:
				if($chucvu==6) // chức vụ chánh văn phòng
				{
					$trangthai =6;
					$ten_taikhoan       = 'chanhvanphong';
					$ykien 				= 'ykien_cvp';
					$ten_trangthai_xem  = 'trangthai_xem_cvp';
					$trangthai_xem 		= $tinhtrang;
					$chude 				= $chude;
				}
				break;
			default:
				$trangthai =9;
				break;
		}
		if($quyen!=3)
		{
			redirect('welcome.html');
		}
		// pr($data['nguoinhan3']);
		if(_post('gui'))
		{
			$data['content'] = $this->Gui($trangthai);
		}
		if(_post('duyet'))
		{
			$mavanban = _post('duyet');
			$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi','trangthai_chuyen',8);
			$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi','nguoiduyet',$taikhoan);
			$nguoinhanlai = _post('nguoinhanlaiduyet');
			$data_them=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $nguoinhanlai,
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => 'Văn bản đã được duyệt'
					);
			$this->Mdanhmuc->themDuLieu('tbl_luuvet_vbdi',$data_them);
		}
		// $ds= $this->Mtruyennhan->layVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		$vanban            = $this->DSVanBan($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['trangthai'] = $trangthai;
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách văn bản trình ký';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandichoxuly_cvp';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBan($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude) 
	{
		$config['base_url']             = base_url().'vanbandichoxuly_cvp?tinhtrang='.$trangthai_xem.'&chude='.$chude;
		$config['total_rows']           = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandichoxuly_cvp');
      	}
		$data['info']       = $this->Mtruyennhan->layVanBanChoXuLy($ten_taikhoan,$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}
	public function Gui($trangthai){
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$hoten   	= $this->_session['sHoTen'];
		$taikhoan   = $this->_session['PK_iMaCB'];
		$mavanban   = _post('gui');
		$ykiengoc   = _post('ykien')[$mavanban];  
		$ykien      = _post('ykien')[$mavanban];  
		$ykien      = '<b>'.$ykien.'</b><p>- '.$hoten.'</p><p>- '.date('Y-m-d H:i:s');
		$thongtin   = $this->Mtruyennhan->layThongTinVBDI($mavanban);
		switch ($trangthai) {
			case '6':
				$data_capnhat=array(
					'ykien_cv'           => ($thongtin['chuyenvien'])?$ykien:'',
					'ykien_pp'           => ($thongtin['phophong'])?$ykien:'',
					'ykien_tp'           => ($thongtin['truongphong'])?$ykien:'',
					'ykien_pcc'          => ($thongtin['phochicuc'])?$ykien:'',
					'ykien_tcc'          => ($thongtin['truongchicuc'])?$ykien:'',
					'ykien_bpth'         => ($thongtin['bophantonghop'])?$ykien:'',
					'trangthai_xem_bpth' => 1,
					'ykien_pgd'          => ($thongtin['phogiamdoc'])?$ykien:'',
					'trangthai_xem_pgd'  => 1,
					'ykien_gd'           => ($thongtin['giamdoc'])?$ykien:'',
					'trangthai_xem_gd'   => 1,
					'trangthai_xem_cvp'  => 3
					);
				if(!empty($thongtin['chuyenvien']))
				{
					$data_them[0]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['chuyenvien'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phophong']))
				{
					$data_them[1]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phophong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongphong']))
				{
					$data_them[2]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongphong'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phochicuc']))
				{
					$data_them[3]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phochicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['truongchicuc']))
				{
					$data_them[4]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['truongchicuc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['phogiamdoc']))
				{
					$data_them[5]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['phogiamdoc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['bophantonghop']))
				{
					$data_them[6]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['bophantonghop'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				if(!empty($thongtin['giamdoc']))
				{
					$data_them[7]=array(
					'FK_iMaVBDi'    => $mavanban,
					'FK_iMaCB_Gui'  => $taikhoan,
					'FK_iMaCB_Nhan' => $thongtin['giamdoc'],
					'sThoiGian'     => date('Y-m-d H:i:s'),
					'sYKien'        => $ykiengoc
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi',$data_capnhat);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themnhieuDuLieu('tbl_luuvet_vbdi',$data_them);
				}
				break;
			default:
				# code...
				break;
		}
	}

}

/* End of file Cvanbandichoxuly_cvp.php */
/* Location: ./application/controllers/vanbandi/Cvanbandichoxuly_cvp.php */