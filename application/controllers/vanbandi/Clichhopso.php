<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clichhopso extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{

		$data['title']    = 'Lịch họp Sở Ngoại vụ Hà Nội';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vlichhopso';
		$this->load->view('layout_admin/layout',$temp);
	}
	
	
}
/* End of file Cdsvanbanchoso.php */
/* Location: ./application/controllers/vanbandi/Cdsvanbanchoso.php */