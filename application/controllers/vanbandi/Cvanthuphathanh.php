<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanthuphathanh extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{ 
		if($_FILES['uploadfile']['name'] !=''){
			$name = $_FILES['uploadfile']['name'];

			$data_ss=$this->Mvanbandi->GetMaCBFromSession_ci($this->session->session_id);
        	$vanban = unserialize_session_data(strstr($data_ss,'vanban'))['vanban'];
        	//lãnh đạo sở duyệt
			if($vanban['iQuyenHan_DHNB']==9){        	
				$mavanban   = $this->Mvanbandi->layMaVBDiQuaFile('doc_uploads_2019/'.$name);
				$FK_iMaLVB   = $this->Mvanbandi->layMaLoaiVBQuaMaVBDi($mavanban);

				if($FK_iMaLVB==10)
				{
					$solonnhat = $this->Mvanbandi->laySoDiMoi_in_Year(10);
				}
				else{
					$solonnhat = $this->Mvanbandi->laySoDiMoi_in_Year(1);
				}
				$sodi      = $solonnhat['iSoVBDi'] +1;
				$data=array(
					'iSoVBDi'            => $sodi,
					'sNgayVBDi'          => date_insert(date('Y-m-d')),
					'trangthai_xem'      => 2,
					'trangthai_xem_bpth' => 2,
					'trangthai_xem_pgd'  => 2,
					'trangthai_xem_gd'   => 2,
					'trangthai_xem_cvp'  => 2
					);

				$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$mavanban,'vanban_'.date("Y").'.tbl_vanbandi',$data);
				$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'vanban_'.date("Y").'.tbl_vanbandi','iFile',1);
			
				$this->upload('doc_uploads_'.date('Y'),$name,time());
				$this->guiMail($mavanban);// truyền mã văn bản lấy từ số xuống
				$url_fileserver = 'https://'.$_SERVER["SERVER_NAME"].'/dieuhanhnoibo/doc_uploads_'.date('Y').'/'.$name;
			 	$arr = array ("Status"=>"true","Message"=>"",'FileName'=>$name,'path'=>"",'FileServer'=>$url_fileserver);
	    		echo json_encode($arr);
	    	}
	
		}
		

	}
	
	public function upload($dir,$name,$time)
	{
		if(is_dir($dir)==false){
			mkdir($dir);		// Create directory if it does not exist
		}
		$config['upload_path']   = $dir;
		$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
		$config['overwrite']     = true;
		$config['file_name']     = clear($name);//$time.'_'.clear($name);
		$this->load->library('upload');
     	$this->upload->initialize($config);
     	$this->upload->do_upload('uploadfile');// ten file
		$this->upload->data();
	}

	public function guiMail($ma)
	{
		$this->createXML($ma);
		$this->sendMail($ma);
	}
	public function createXML($ma)
	{
		$thongtin = $this->Mvanbandi->layThongTinTheoMaVB1($ma);
		// cáu hình tạo file sdk 
		$xml 	  = new DOMDocument("1.0",'UTF-8');
		$STRLOAIVANBAN  = $xml->createElement("STRLOAIVANBAN");
		$STRLOAIVANBAN_Text = $xml->createTextNode($thongtin['sTenLVB']);
		$STRLOAIVANBAN->appendChild($STRLOAIVANBAN_Text);
		
		$STRKYHIEU   	= $xml->createElement("STRKYHIEU");
		$STRKYHIEU_Text = $xml->createTextNode($thongtin['iSoVBDi'].'/'.$thongtin['sKyHieu']);
		$STRKYHIEU->appendChild($STRKYHIEU_Text);
		 
		$STRTRICHYEU  = $xml->createElement("STRTRICHYEU");
		$STRTRICHYEU_Text = $xml->createTextNode($thongtin['sMoTa']);
		$STRTRICHYEU->appendChild($STRTRICHYEU_Text);
		
		$STRNGAYKY  = $xml->createElement("STRNGAYKY");
		$STRNGAYKY_Text = $xml->createTextNode(date_select($thongtin['sNgayVBDi']));
		$STRNGAYKY->appendChild($STRNGAYKY_Text);
		
		$STRNGUOIKY  = $xml->createElement("STRNGUOIKY");
		$STRNGUOIKY_Text = $xml->createTextNode($thongtin['sHoTen']);
		$STRNGUOIKY->appendChild($STRNGUOIKY_Text);
		
		$STRCHUCDANH  = $xml->createElement("STRCHUCDANH");
		$STRCHUCDANH_Text = $xml->createTextNode($thongtin['sTenCV']);
		$STRCHUCDANH->appendChild($STRCHUCDANH_Text);
		
		$STRNOIGUI  = $xml->createElement("STRNOIGUI");
		$STRNOIGUI_Text = $xml->createTextNode('Sở Tài chính');
		$STRNOIGUI->appendChild($STRNOIGUI_Text);
		
		$STRNGAYHOP  = $xml->createElement("STRNGAYHOP");
		$STRNGAYHOP_Text = $xml->createTextNode(($thongtin['sNgayMoi']!='0000-00-00')?date_select($thongtin['sNgayMoi']):'');
		$STRNGAYHOP->appendChild($STRNGAYHOP_Text);
		
		$STRDIADIEM  = $xml->createElement("STRDIADIEM");
		$STRDIADIEM_Text = $xml->createTextNode($thongtin['sDiaDiemMoi']);
		$STRDIADIEM->appendChild($STRDIADIEM_Text);	
		
		$STRTHOIGIANHOP  = $xml->createElement("STRTHOIGIANHOP");
		$STRTHOIGIANHOP_Text = $xml->createTextNode($thongtin['sGioMoi']);
		$STRTHOIGIANHOP->appendChild($STRTHOIGIANHOP_Text);
		 
		$book 	= $xml->createElement("EXPORTMAIL");
		$book->appendChild($STRLOAIVANBAN);
		$book->appendChild($STRKYHIEU);
		$book->appendChild($STRTRICHYEU);
		$book->appendChild($STRNGAYKY);
		$book->appendChild($STRNGUOIKY);
		$book->appendChild($STRCHUCDANH);
		$book->appendChild($STRNOIGUI);
		$book->appendChild($STRNGAYHOP);
		$book->appendChild($STRDIADIEM);
		$book->appendChild($STRTHOIGIANHOP);
		
		$xml->appendChild($book);
		$xml->formatOutput = true;
		$xml->save("tmpxml/XMLOutput.sdk") or die("Error");
	}
	function sendMail($ma)
	{
		$thongtin   = $this->Mvanbandi->layThongTinTheoMaVB1($ma);
		$file 		= $this->Mvanbandi->layFileLast1($ma);
		if(!empty($thongtin['sNoiNhan']))
		{
			$file 		= $this->Mvanbandi->layFileLast1($ma);
			$name       = 'Văn thư Sở Tài chính';
			$from_email = 'vanthu_sotc@hanoi.gov.vn';
			$subject    = '('.$thongtin['iSoVBDi'].'/'.$thongtin['sKyHieu'].') '.$thongtin['sMoTa']; // nội dung gửi mail
			$message    = $thongtin['sMoTa'];
			$to_email   = $thongtin['sNoiNhan'];// mail nhận
	        //configure email settings
			$config['protocol']  = 'smtp';
			// $config['smtp_host'] = 'mail.thudo.gov.vn';
			// $config['smtp_port'] = '587';
			$config['smtp_host'] = 'ssl://mail.thudo.gov.vn';
			$config['smtp_port'] = '465';
			$config['smtp_user'] = 'vanthu_sotc@hanoi.gov.vn';// email gửipthtk_sotc@hanoi.gov.vn
			$config['smtp_pass'] = 'sotchanoi042018';// pass cua mail trentinhoc@gmail.com
			$config['mailtype']  = 'html';
			$config['charset']   = 'utf-8';
			$config['wordwrap']  = TRUE;
			$config['newline']   = "\r\n"; //use double quotes
	        $this->load->library('email', $config);
	        $this->email->initialize($config);
	        //send mail
	        $this->email->clear(TRUE);
	        $this->email->from($from_email, $name);
	        $this->email->to($to_email);
	        $this->email->subject(catKyTu($subject));
	        $this->email->message($message);
	        $filename = 'tmpxml/XMLOutput.sdk';
	        if(!empty($file))
	        {
	        	$this->email->attach($file['sDuongDan']);
	        }
			$this->email->attach($filename);
			if ($this->email->send(FALSE)) {
		    	$data=array(
					'iGuiMail'    => 1,
					'sNgayGuiMai' => date('Y-m-d')
		    		);
		    	$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'vanban_'.date('Y').'.tbl_vanbandi',$data);
		    }
		}
	}

}

/* End of file Cteptin.php */
/* Location: ./application/controllers/vanbandi/Cteptindi.php */