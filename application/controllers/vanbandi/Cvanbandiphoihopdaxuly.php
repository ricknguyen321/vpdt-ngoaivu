<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandiphoihopdaxuly extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$data['dsvanban'] = $this->Mtruyennhan->layVanBanDaXuLy_phoihop($taikhoan);
		// pr($data['dsvanban']);
		$data['title']    = 'Văn bản đi phối hợp đã xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandiphoihopdaxuly';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cvanbandiphoihopdaxuly.php */
/* Location: ./application/controllers/vanbandi/Cvanbandiphoihopdaxuly.php */