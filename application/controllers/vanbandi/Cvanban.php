<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanban extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		
		$action = _post('action');
		$isoden = _get('isoden');
		$data['isoden'] = $isoden;
		$mvb = _get('mvb');
		$data['mvb'] = $mvb;
		if ($mvb != '') {
			$data['ttvbd'] = $this->Mdanhmuc->layDuLieu('PK_iMaVBDen',$mvb,'tbl_vanbanden');		
		}
		if(!empty($action))
		{
			switch ($action) {
				case 'laychucvu':
					$this->layChucVu();
					break;
				case 'laykyhieu':
					$this->layKyHieu();
					break;
				default:
					# code...
					break;
			}
		}
		$canbo      = $this->Mvanbandi->layCB();
		foreach ($canbo as $key => $cb) {
			$data['mangcb'][$cb['PK_iMaCB']] = $cb['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if($phongbanHD==12)
		{
			$quyenhan		= array(11,7);
			$chucvuphong	= '';
			$quyendb        = $quyendb;
		}
		elseif ($phongbanHD==11) {
			$quyenhan		= array(3,7);
			$chucvuphong	= array(6,12);
			$quyendb 		= '';
		}
		else{
			$quyenhan		= array(6,7);
			$chucvuphong	= '';
			$quyendb 		= '';
		}
		if($quyen==6||($quyen==3&&$chucvu==6))
		{
			$data['lanhdaophong'] = $this->Mvanbandi->layNguoiKy();
		}
		elseif($quyen==11){
			$quyenhan		= array(6,10);
			$chucvuphong	= array(15,16);
			$quyendb        = '';
			$data['lanhdaophong'] = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
		}
		else{
			$data['lanhdaophong'] = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
		}

		$ma = _get('id');
		$data['id'] = $ma;
		if(!empty($ma))
		{
			$this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['domat']      = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_domat');
		$data['dokhan']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_dokhan');
		$data['donvi_ngoai']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_thongtin_donvi');
		$data['loai']		  = _get('loai');
		$data['dsnoinhan']	  = $this->Mvanbandi->layDonViNhanMail();
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dsduthao']     = $this->Mvanbandi->layPhongBanDuThao();
		$data['thongtin']	  = $this->_thongtin;
		$data['mangnguoinhan']= explode(', ',$this->_thongtin[0]['sNoiNhan']);
		
		$giomoi = $this->_thongtin[0]['sGioMoi'];
		$manggio = explode(':',$giomoi);
		if (empty($manggio) || count($manggio) == 1) {
			$manggio = explode('h',$giomoi);
		} 
		$gio = $manggio[0];
		$phut = $manggio[1];
		$data['gio'] 	  = $gio;
		$data['phut'] 	  = $phut;
		// $data['dsdomat']      = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_domat');
		// $data['dsdokhan']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_dokhan');
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi->layNguoiKyHai();
		// nơi nhận mail quận huyện và sở ban ngành
		$data['quanhuyen']  = $this->Mvanbandi->laynoinhanmail(2);
		$data['sobannganh'] = $this->Mvanbandi->laynoinhanmail(1);
		$data['donvi']      = $this->Mvanbandi->laynoinhanmail(3);
		$data['dsemailnhan']      = $this->Mvanbandi->laydsemailnhan();
		$data['title']        = 'Nhập mới văn bản đi';
		$temp['data']         = $data;
		$temp['template']     = 'vanbandi/Vvanban';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');
		if(_post('luudulieu'))
		{
			$ngaymoi = _post('ngaymoi');
			if(!empty($ngaymoi))
			{
				$ngay    = date_insert($ngaymoi);
				$gio = _post('gio');
				$phut = _post('phut');
				$gioimoi = $gio.':'.$phut;
			}
			else{
				$ngay    = '0000-00-00';
				$gioimoi ='';
			}
			$noinhanngoai = _post('nhanmailngoai');
			if(!empty($noinhanngoai))
			{
				$mail_ngoai = explode(',',$noinhanngoai);
				foreach ($mail_ngoai as $key => $m) {
					$kiemtra = stripos($m,'@');
					if(empty($kiemtra)){
						return messagebox('Sai định dạng mail: '.$m.'.\n Mời nhập lại','danger');
					}
				}
			}
			//ricknguyen321
			$emailnhan = _post('emailnhan');		
			if(!empty($emailnhan))
			{
				$emailnhan= implode(', ', $emailnhan);
			}
			
			$noinhanmail  = _post('noinhanmail');
			if(!empty($noinhanmail))
			{
				$noinhanmail= implode(', ', $noinhanmail);
			}
			if(!empty($noinhanngoai) && !empty($noinhanmail))
			{
				$noinhan = $noinhanngoai.', '.$noinhanmail;
			}
			if(!empty($noinhanngoai) && empty($noinhanmail))
			{
				$noinhan = $noinhanngoai;
			}
			if(empty($noinhanngoai) && !empty($noinhanmail))
			{
				$noinhan = $noinhanmail;
			}
			if(empty($noinhanngoai) && empty($noinhanmail))
			{
				$noinhan = '';
			}
			//ricknguyen321
			if(!empty($noinhan) && !empty($emailnhan))
			{
				$noinhan = $noinhan.', '.$emailnhan;
			}			
			if(empty($noinhan) && !empty($emailnhan))
			{
				$noinhan = $emailnhan;
			}
			if(!empty($noinhan)){
				$noinhan = str_replace(';',',',$noinhan);
			}
			if(empty($noinhan))
			{
				return messagebox('Chưa chọn nơi nhận, mời nhập lại văn bản','danger');
			}
			$data=array(
				'sNgayVBDi'     => date_insert(_post('ngaythang')),
				'sNgayMoi'      => $ngay,
				'sGioMoi'       => $gioimoi,
				'sDiaDiemMoi'   => _post('diadiem'),
				'FK_iMaLVB'     => _post('loaivanban'),
				'FK_iMaPB'      => _post('noiduthao'),
				'sMoTa'         => _post('mota'),
				'sNoiNhan'      => $noinhan,
				'sNoiNhanNgoai' => $noinhanngoai,
				'iSoTrang'      => _post('sotrang'),
				'sKyHieu'       => _post('sokyhieu'),
				'sTenLV'        => _post('linhvuc'),
				'FK_iMaCB_Ky'   => _post('nguoiky'),
				'sTenCV'        => _post('chucvu'),
				'iSoDen'        => _post('sotraloi'),
				'iSoVBDi'       => _post('sovanban'),
				'iVBDang'       => _post('iVBDang'),
				'FK_iMaDM'      => _post('domat'),
                'FK_iMaDK'      => _post('dokhan'),
				'iDuThao'       => _post('duthao')
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi',$data);
			$name    = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('doc_vabandi_'.date('Y'), $time);
				$files = array();
				foreach ($name as $key => $value) {
					$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}						
						$tmp = str_replace('$', '-', $tmp);		
						$tmp = $tmp.'.'.$a[$c];
						if ($tmp != '.') {
							$files[] = array(
								'FK_iMaVBDi' => $ma,
								'sTenFile'   => $value,
								'sDuongDan'  => 'doc_vabandi_'.date('Y').'/di_tao_'.$time.'_'.$tmp,
								'sThoiGian'  => date('Y-m-d H:i:s',time()),
								'FK_iMaCB'   => $this->_session['PK_iMaCB']
								);
						}
				}
				$kiemtrafile=$this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
				if($kiemtrafile>0)
				{
					$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi','iFile',1);
				}
			}
			if($kiemtra>0)
			{
				$loai = _get('loai');
				switch ($loai) {
					case 'choso':
						redirect('dsvanbanchoso');
						break;
					case 'di':
						redirect('dsvanban');
						break;
					case 'ubnd':
						redirect('dsvanbanubnd_vt');
						break;
					case 'giaymoi':
						redirect('dsgiaymoidi');
						break;
					default:
						redirect('dsvanban');
						break;
				}
			}
		}
	}
	public function themDuLieu()
	{
		$time = time();
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if(_post('luudulieu'))
		{
			$canbo		= $this->_session['PK_iMaCB'];
			$ngaymoi	= _post('ngaymoi');
			$loaivanban	= _post('loaivanban');
			if($loaivanban==10)
			{
				$ngay    = date_insert($ngaymoi);
				$gio = _post('gio');
				$phut = _post('phut');
				$gioimoi = $gio.':'.$phut;
			}
			else{
				$ngay    = '0000-00-00';
				$gioimoi ='';
			}
			$lanhdaophong = _post('lanhdaophong');
			$noinhanngoai = _post('nhanmailngoai');
			if(!empty($noinhanngoai))
			{
				$mail_ngoai = explode(',',$noinhanngoai);
				foreach ($mail_ngoai as $key => $m) {
					$kiemtra = stripos($m,'@');
					if(empty($kiemtra)){
						return messagebox('Sai định dạng mail: '.$m.'.\n Mời nhập lại','danger');
					}
				}
			}
			$donvingoai   = _post('donvingoai');
			if(!empty($donvingoai)){
				$donvi_ngoai = implode(',', $donvingoai);
			}
			else{
				$donvi_ngoai = '';
			}
			//ricknguyen321
			$emailnhan = _post('emailnhan');		
			if(!empty($emailnhan))
			{
				$emailnhan= implode(', ', $emailnhan);
			}
			$noinhanmail  = _post('noinhanmail');
			if(!empty($noinhanmail))
			{
				$noinhanmail= implode(', ', $noinhanmail);
			}
			if(!empty($noinhanngoai) && !empty($noinhanmail))
			{
				$noinhan = $noinhanngoai.', '.$noinhanmail;
			}
			if(!empty($noinhanngoai) && empty($noinhanmail))
			{
				$noinhan = $noinhanngoai;
			}
			if(empty($noinhanngoai) && !empty($noinhanmail))
			{
				$noinhan = $noinhanmail;
			}
			if(empty($noinhanngoai) && empty($noinhanmail))
			{
				$noinhan = '';
			}	
			//ricknguyen321
			if(!empty($noinhan) && !empty($emailnhan))
			{
				$noinhan = $noinhan.', '.$emailnhan;
			}			
			if(empty($noinhan) && !empty($emailnhan))
			{
				$noinhan = $emailnhan;
			}
			
			if(!empty($noinhan)){
				$noinhan = str_replace(';',',',$noinhan);
			}
			$data=array(
				'sNgayVBDi'        => date_insert(_post('ngaythang')),
				'sNgayMoi'         => $ngay,
				'sGioMoi'          => $gioimoi,
				'sDiaDiemMoi'      => _post('diadiem'),
				'FK_iMaLVB'        => _post('loaivanban'),
				'FK_iMaPB'         => _post('noiduthao'),
				'sMoTa'            => _post('mota'),
				'sNoiNhan'         => $noinhan,
				'sNoiNhanNgoai'    => $noinhanngoai,
				'iSoTrang'         => _post('sotrang'),
				'sKyHieu'          => _post('sokyhieu'),
				'sTenLV'           => _post('linhvuc'),
				'FK_iMaCB_Ky'      => _post('nguoiky'),
				'sTenCV'           => _post('chucvu'),
				'iSoDen'           => _post('sotraloi'),
				'iDuThao'          => _post('duthao'),
				'iVBDang'          => _post('iVBDang'),
				'FK_iMaDM'     	   => _post('domat'),
                'FK_iMaDK'         => _post('dokhan'),
				'FK_iMaCB_Nhap'    => $this->_session['PK_iMaCB'],
				'sNgayNhap'        => date('Y-m-d H:i:s'),
				'trangthai_chuyen' => ($quyen==6||($quyen==3&&$chucvu==6))?8:1,
				'FK_iMaDV_Ngoai'   => $donvi_ngoai
			);
			if(empty($noinhan))
			{
				$this->_thongtin = array($data);
				return messagebox('Chưa chọn nơi nhận, mời nhập lại văn bản','danger');
			}
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_vanbandi',$data);
			}
			else{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa chọn file','danger');
			}
			
			if(!empty($lanhdaophong))
			{
				
				$data_luuvet[0] =array(
					'FK_iMaVBDi'    => $id_insert,
					'FK_iMaCB_Gui'  => $this->_session['PK_iMaCB'],
					'FK_iMaCB_Nhan' => $lanhdaophong,
					'sYKien'       => nl2br(_post('ykien')),
					'sThoiGian'		=> date('Y-m-d H:i:s'),
					'iTrangThai'    => 1,
					'phong_lanhdao' => ($quyen==6||($quyen==3&&$chucvu==6))?2:1
				);
			}
			if($id_insert>0)
			{
				if($quyen!=9)
				{
					$kiemtrathem= $this->Mdanhmuc->themNhieuDuLieu('tbl_luuvet_vbdi',$data_luuvet);
				}
				$name = $_FILES['files']['name'];
				if(!empty($name[0]))
				{
					$this->upload('doc_vabandi_'.date('Y'), $time);
					$files = array();
					foreach ($name as $key => $value) {
						$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}
						$tmp = str_replace('$', '-', $tmp);
						$tmp = $tmp.'.'.$a[$c];
						
						if ($tmp != '.') {
							$files[] = array(
								'FK_iMaVBDi' => $id_insert,
								'sTenFile'   => $value,
								'sDuongDan'  => 'doc_vabandi_'.date('Y').'/di_tao_'.$time.'_'.$tmp,
								'sThoiGian'  => date('Y-m-d H:i:s',time()),
								'FK_iMaCB'   => $this->_session['PK_iMaCB'],
								);
						}
					}
					$kiemtrafile = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
					if($kiemtrafile>0)
					{
						$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$id_insert,'tbl_vanbandi','iFile',1);
					}
				}
				redirect('vanbandiusertao');
				return messagebox('Thêm văn bản đi THÀNH CÔNG','info');
			}
			else{
				return messagebox('Thêm văn bản đi thất bại','danger');
			}
		}
	}
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'di_tao_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	public function layKyHieu()
	{
		$so      = _post('so');
		$maloai  = _post('maloai');
		$maphong = _post('maphong');
		$layLoai = $this->Mdanhmuc->layDuLieu('PK_iMaLVB',$maloai,'tbl_loaivanban');
		$layPhong = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$maphong,'tbl_phongban');
		if(!empty($so))
		{
			/*if ($maloai == 2){
				$sokyhieu = $so.'/NV-'.$layPhong[0]['sVietTat'];
			} else {
				$sokyhieu = $so.'/'.$layLoai[0]['sTenVietTat'].'-NV';
			} */
			
			// khong dung $so
			if ($maloai == 1){
				$sokyhieu = 'NV-'.$layPhong[0]['sVietTat'];
			} else {
				$sokyhieu = $layLoai[0]['sTenVietTat'].'-NV';
			}
		}
		else{
			if ($maloai == 1){
				$sokyhieu = 'NV-'.$layPhong[0]['sVietTat'];
			} else {
				$sokyhieu = $layLoai[0]['sTenVietTat'].'-NV';
			}
		}
		echo json_encode($sokyhieu); exit();
	}
	public function layChucVu()
	{
		$ma= _post('ma');
		$ketqua = $this->Mvanbandi->layChucVu($ma);
		echo json_encode($ketqua['sTenCV']);
		exit();
	}
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */