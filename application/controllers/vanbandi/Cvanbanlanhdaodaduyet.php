<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanlanhdaodaduyet extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$lanhdao    = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($lanhdao as $key => $value) {
			$data['lanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$ds= $this->Mtruyennhan->layvanbandaduyet($phongbanHD);
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách văn bản đi chờ xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbanlanhdaodaduyet';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cvanbanlanhdaodaduyet.php */
/* Location: ./application/controllers/vanbandi/Cvanbanlanhdaodaduyet.php */