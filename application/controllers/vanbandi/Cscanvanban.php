<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cscanvanban extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		$action = _post('action');
		if(!empty($action))
		{
			switch ($action) {
				case 'laychucvu':
					$this->layChucVu();
					break;
				case 'laykyhieu':
					$this->layKyHieu();
					break;
				default:
					# code...
					break;
			}
		}
		if(_post('luudulieu'))
		{
			$data['content'] = $this->themDuLieu();
		}
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dsduthao']     = $this->Mvanbandi->layPhongBanDuThao();
		$data['dsdomat']      = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_domat');
		$data['dsdokhan']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_dokhan');
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi->layNguoiKy();
		$data['title']    = 'Scan văn bản';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vscanvanban';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function themDuLieu()
	{
		$data=array(
			'iSoVBDi'       => _post('sodi'),
			'sNgayVBDi'     => date_insert(_post('ngaythang')),
			'FK_iMaLVB'     => _post('loaivanban'),
			'FK_iMaPB'      => _post('duthao'),
			'sMoTa'         => _post('trichyeu'),
			'sNoiNhan'      => _post('noinhan'),
			'iSoTrang'      => _post('sotrang'),
			'sKyHieu'       => _post('kyhieu'),
			'sTenLV'        => _post('linhvuc'),
			'FK_iMaCB_Ky'   => _post('nguoiky'),
			'sTenCV'        => _post('chucvu'),
			'iSoDen'        => _post('soden'),
			'FK_iMaDM'      => _post('domat'),
			'FK_iMaDK'      => _post('dokhan'),
			'sGhiChu'       => _post('ghichu'),
			'FK_iMaCB_Nhap' => $this->_session['PK_iMaCB']
			);
		$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_vanbandi',$data);
		if($id_insert>0)
		{
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_uploads_'.date('Y'));
				$files = array();
				foreach ($name as $key => $value) {
					$files[] = array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => _post('tenteptin'),
						'sDuongDan'  => 'doc_uploads_'.date('Y').'/'.clear($value),
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
				}
				$this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
			}
			return messagebox('Thêm văn bản đi thành công','info');
		}
		else{
			return messagebox('Thêm văn bản đi thất bại','danger');
		}
	}
	public function upload($dir)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	public function layKyHieu()
	{
		$maloai  = _post('maloai');
		$maphong = _post('maphong');
		if($maloai==10)
		{
			$solonnhat = $this->Mvanbandi->laySoDiMoi(10);
		}
		else{
			$solonnhat = $this->Mvanbandi->laySoDiMoi(1);
		}
		$sodi      = $solonnhat['iSoVBDi'] +1;
		$layLoai = $this->Mdanhmuc->layDuLieu('PK_iMaLVB',$maloai,'tbl_loaivanban');
		$layPhong = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$maphong,'tbl_phongban');
		$sokyhieu = $sodi.'/'.$layLoai[0]['sTenVietTat'].'/STC-'.$layPhong[0]['sVietTat'];
		$mang=array($sodi,$sokyhieu);
		echo json_encode($mang); exit();
	}
	public function layChucVu()
	{
		$ma= _post('ma');
		$ketqua = $this->Mvanbandi->layChucVu($ma);
		echo json_encode($ketqua['sTenCV']);
		exit();
	}

}

/* End of file Cscanvanban.php */
/* Location: ./application/controllers/vanbandi/Cscanvanban.php */