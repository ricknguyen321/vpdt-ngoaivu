<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/controllers/vanbandi/OfficeConverter.php';
use NcJoes\OfficeConverter\OfficeConverter;
class Cvbdidaphchuaduyet extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
		$this->load->library('user_agent');

	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$data['lanhdaoso'] 	  = $this->Mvanbandi->layNguoiKy();
		if(_post('dongy'))
		{
			$data['content'] = $this->dongy($taikhoan);
		}
		if(_post('tralai'))
		{
			$data['content'] = $this->tralai();
		}
		
		$vanban            = $this->DSVanBan($taikhoan);
		// pr($ten_trangthai_xem);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		$data['count'] = $vanban['count'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$trangthai 						= _get('trangthai');
		if(!empty($trangthai))
		{
			$trangthai = $trangthai;
		}
		else{
			$trangthai = 1; // 1:là chờ xử lý
		}
		if($this->agent->is_mobile()){
			$data['giaodienmobie'] = 1;
		}else{
			$data['giaodienmobie'] = 2;
		}
		$data['ci_session'] = $this->session->session_id;
		$data['trangthai'] = $trangthai;
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách VB đi đã phát hành chưa duyệt';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvbdidaphchuaduyet';
		$this->load->view('layout_admin/layout',$temp);
	}
	
	public function dongy($taikhoan)
	{
		$mavanban = _post('dongy');
		$year = _post('yearup');
		$layma    = $this->Mvanbandi->layMaMoiNhat_CN2($mavanban, $year);//pr($layma);
		//$arr_file    = $this->Mvanbandi->layFileLast($mavanban);
		if(!empty($layma))

		$files= array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => '-------------DUYỆT',
						'sDuongDan'  => 'files/images/gdapproved.png',
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
			//$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_files_vbdi',$files);
			
			echo $layma['PK_iMaNhatKy'];
			$this->Mvanbandi->capnhatTrangThai_XuLy_LD($layma['PK_iMaNhatKy'], $year); 
			
			$ykien    = nl2br(_post('ykien_'.$mavanban));
			if ($ykien == '') {
				$ykien = 'Duyệt văn bản./.';
			}
			$chidaold = array(
					'FK_iMaVBDi'        => $mavanban,
					'sNoiDung'        => $ykien,
					'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
					'sThoiGian'   => date('Y-m-d H:i:s',time()),
				);
			
			$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
			
			return redirect(base_url().'vbdidaphchuaduyet');
			return messagebox('Duyệt văn bản THÀNH CÔNG!','info');
	}
	
	public function tralai()
	{
		$mavanban = _post('tralai');
		$ykien    = nl2br(_post('ykien_'.$mavanban));
		$taikhoan = $this->_session['PK_iMaCB'];
		$ma       = $this->Mvanbandi->layMaMoiNhat_Cu($mavanban,$taikhoan);
		$canbophong = $this->Mvanbandi->laynguoitralai($mavanban,$ma['PK_iMaNhatKy']);
		
		//$this->themDuLieu($mavanban);
		if(!empty($canbophong))
		{
			foreach ($canbophong as $value) {
				$data=array(
					'FK_iMaVBDi'	=> $mavanban,
					'FK_iMaCB_Gui'	=> $taikhoan,
					'FK_iMaCB_Nhan' => $value['FK_iMaCB_Gui'],
					'sYKien'		=> $ykien,
					'sThoiGian'		=> date('Y-m-d H:i:s'),
					'iTrangThai'	=> 1,
					'phong_lanhdao' => 1
				);
				$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
				if($id_insert>0)
				{
					$this->Mvanbandi->capnhatTrangThai_XuLy_TL($mavanban,$taikhoan);
				}
			}
			return messagebox('Văn bản đã được trả lại!','info');
		}
	}
	
	public function DSVanBan($taikhoan) 
	{

			$trangthai = 1; // 1:là chờ xử lý

		$config['base_url']             = base_url().'vbdidaphchuaduyet?trangthai='.$trangthai;

			//$config['total_rows']           = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,$trangthai); // 1:là chờ xử lý 2: trả lại

		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';
		
		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vbdidaphchuaduyet');
      	}
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$data['info']       = $this->Mvanbandi->layVBDi_daphathanh_chuaduyet($taikhoan,$quyen,$trangthai,$config['per_page'], $data['page']);
		$config['total_rows'] = $this->Mvanbandi->demVBDi_daphathanh_chuaduyet($taikhoan,$quyen,$trangthai);   
		$data['count'] = $config['total_rows'];
		$this->pagination->initialize($config); 
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}
	function MakePropertyValue($name,$value,$osm){
	    $oStruct = $osm->Bridge_GetStruct("com.sun.star.beans.PropertyValue");
	    $oStruct->Name = $name;
	    $oStruct->Value = $value;
	    return $oStruct;
    }
    function word2pdf($doc_url, $output_url){
    	$doc_url=str_replace('\\', '/', $doc_url);
    	$output_url=str_replace('\\', '/', $output_url);
    	//echo $output_url.'<br>';
    	//Invoke the OpenOffice.org service manager
	    $osm = new COM("com.sun.star.ServiceManager") or die ("Please be sure that OpenOffice.org is installed.\n");
	    //echo $doc_url;
	    //pr($osm);
	    //Set the application to remain hidden to avoid flashing the document onscreen
	    $args = array($this->MakePropertyValue("Hidden",true,$osm));
	    //Launch the desktop
	    $oDesktop = $osm->createInstance("com.sun.star.frame.Desktop");
	    //Load the .doc file, and pass in the "Hidden" property from above
	    $oWriterDoc = $oDesktop->loadComponentFromURL($doc_url,"_blank", 0, $args);
	    //Set up the arguments for the PDF output
	    $export_args = array($this->MakePropertyValue("FilterName","writer_pdf_Export",$osm));
	    //print_r($export_args);
	    //Write out the PDF
	    $oWriterDoc->storeToURL($output_url,$export_args);
	    $oWriterDoc->close(true);
    }
	public function themDuLieu($mavanban)
	{
			$time = time();
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_uploads_'.date('Y'),$time);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$a = explode('.', $value);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}
					$tmp = str_replace('~', '-', $tmp);
					$tmp = str_replace('!', '-', $tmp);
					$tmp = str_replace('@', '-', $tmp);
					$tmp = str_replace('#', '-', $tmp);
					$tmp = str_replace('$', '-', $tmp);
					$tmp = str_replace('%', '-', $tmp);
					$tmp = str_replace('^', '-', $tmp);
					$tmp = str_replace('&', '-', $tmp);
					$tmp = $tmp.'.'.$a[$c];
					
					
					$files[] = array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => $tmp,
						'sDuongDan'  => 'doc_uploads_'.date('Y').'/di_up_'.$time.'_'.clear($tmp),
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
				}
				$this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
				
			}
	}
	
	public function upload($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'di_up_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cvbdidaphchuaduyet.php */
/* Location: ./application/controllers/vanbandi/Cvbdidaphchuaduyet.php */