<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/controllers/vanbandi/OfficeConverter.php';
use NcJoes\OfficeConverter\OfficeConverter;

class Cvanbandichoxuly_tp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if($phongbanHD==12)
		{
			$quyenhan		= array(6,10);
			$chucvuphong	= array(15,16);
			$quyendb        = '';
			$data['lanhdaoso'] 	  = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
		}
		else
		{
			$data['lanhdaoso'] 	  = $this->Mvanbandi->layNguoiKy();
		}
		if(_post('dongy'))
		{
			$data['content'] = $this->dongy($taikhoan);
		}
		if(_post('guilen'))
		{
			$data['content'] = $this->guilen();
		}
		if(_post('tralai')){
			$data['content'] = $this->tralai();
		}
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		$vanban            = $this->DSVanBan($taikhoan);
		// pr($ten_trangthai_xem);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách VB đi chờ xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandichoxuly_tp';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function tralai()
	{
		$mavanban = _post('tralai');
		$ykien    = nl2br(_post('ykien_'.$mavanban));
		$taikhoan = $this->_session['PK_iMaCB'];
		$kiemtraxuly = $this->Mvanbandi->kiemtra_XuLy($mavanban,$taikhoan);
		if($kiemtraxuly>0)
		{
			$this->Mvanbandi->capnhatTrangThai_VB($mavanban);
			$ma       = $this->Mvanbandi->layMaMoiNhat_Cu($mavanban,$taikhoan);
			$canbophong = $this->Mvanbandi->laynguoitralai($mavanban,$ma['PK_iMaNhatKy']);
			if(!empty($canbophong))
			{
				foreach ($canbophong as $value) {
					$data=array(
						'FK_iMaVBDi'	=> $mavanban,
						'FK_iMaCB_Gui'	=> $taikhoan,
						'FK_iMaCB_Nhan' => $value['FK_iMaCB_Gui'],
						'sYKien'		=> $ykien,
						'sThoiGian'		=> date('Y-m-d H:i:s'),
						'iTrangThai'	=> 1,
						'phong_lanhdao' => 1
					);
					$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
				}
				$this->themDuLieu($mavanban);
				return messagebox('Văn bản trả lại thành công!','info');
			}
		}
	}
	
	public function dongy($taikhoan)
	{
		
		$mavanban = _post('dongy');
		$year = _post('yearup');
		$layma    = $this->Mvanbandi->layMaMoiNhat_CN($mavanban,$taikhoan, $year);		
		//echo $layma;
		//$arr_file    = $this->Mvanbandi->layFileLast($mavanban);
			$this->Mvanbandi->capnhatTrangThai_XuLy_LD($layma['PK_iMaNhatKy'], $year); 
			
			$ykien    = nl2br(_post('ykien_'.$mavanban));
			if ($ykien == '') {
				$ykien = 'Duyệt văn bản./.';
			}
			$chidaold = array(
					'FK_iMaVBDi'        => $mavanban,
					'sNoiDung'        => $ykien,
					'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
					'sThoiGian'   => date('Y-m-d H:i:s',time()),
				);
			
			$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
			$data['content'] = $this->themDuLieu($mavanban);
			
			return redirect(base_url().'vanbandichoxuly_tp');
			return messagebox('Duyệt văn bản THÀNH CÔNG!','info');
	}
	
	public function guilen()
	{
		
		$phongban	= $this->_session['FK_iMaPhongHD'];
		$taikhoan = $this->_session['PK_iMaCB'];
		$mavanban	= _post('guilen');
		if (_post('lanhdaoso_'.$mavanban) == '') {
			echo '<script language="javascript">';
			echo 'alert("Chưa chọn lãnh đạo!")';
			echo '</script>';
			return messagebox('Gửi lên thất bại! ','danger');
		}
		$ykien		= nl2br(_post('ykien_'.$mavanban));
		$lanhdaoso	= _post('lanhdaoso_'.$mavanban.'_xem');
		
		if($phongban == 211){
			//chuyen sang file .pdf
			$layma    = $this->Mvanbandi->layMaMoiNhat_CN($mavanban,$taikhoan);
			$arr_file    = $this->Mvanbandi->layFileLast($mavanban);

			if(!empty($layma))

			{
				$mang_file = explode('.', $arr_file['sDuongDan']);
				$vitri = count($mang_file) - 1;

				$inputFile = realpath($arr_file['sDuongDan']);
				if($mang_file[$vitri] =='doc'){
					$outputFile = str_replace(".doc",".pdf",$inputFile);
					$duong_dan = str_replace(".doc",".pdf",$arr_file['sDuongDan']);
				}else{
					$outputFile = str_replace(".docx",".pdf",$inputFile);
					$duong_dan = str_replace(".docx",".pdf",$arr_file['sDuongDan']);
				}

				$converter = new OfficeConverter($inputFile);
				$file_ky = str_replace( 'doc_vabandi_2019/', '',$duong_dan);
				$converter->convertTo($file_ky);

				//them mới file văn bản đi
				$filesb= array(
							'FK_iMaVBDi' => $mavanban,
							'sTenFile'   => $arr_file['sTenFile'],
							'sDuongDan'  => $duong_dan,
							'sThoiGian'  => date('Y-m-d H:i:s',time()),
							'FK_iMaCB'   => $this->_session['PK_iMaCB']
							);
				$data['content'] =  $this->Mdanhmuc->themDuLieu('tbl_files_vbdi',$filesb); 

			}
		// kết thúc chuyển sang file .pdf
		}
		
		$this->themDuLieu($mavanban);
		
		if(!empty($lanhdaoso))
		{
			foreach ($lanhdaoso as $key => $value) {
				$data_them[] = array(
					'FK_iMaVB' => $mavanban,
					'FK_iMaCB' => $value
				);
			}
			$this->Mdanhmuc->themNhieuDuLieu('tbl_lanhdaoxem',$data_them);
		}
		
		$taikhoan = $this->_session['PK_iMaCB'];
		$data=array(
			'FK_iMaVBDi'	=> $mavanban,
			'FK_iMaCB_Gui'	=> $taikhoan,
			'FK_iMaCB_Nhan' => _post('lanhdaoso_'.$mavanban),
			'sYKien'		=> $ykien,
			'sThoiGian'		=> date('Y-m-d H:i:s'),
			'iTrangThai'	=> 1,
			'phong_lanhdao' => ($phongban==12)?1:2
		);
		
		 
		
		$kiemtraxuly = $this->Mvanbandi->kiemtra_XuLy($mavanban,$taikhoan);
		if($kiemtraxuly>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
			if($id_insert>0)
			{
				if($phongban!=12)
				{
					$this->Mvanbandi->capnhatTrangThaiChoSo($mavanban);
				}
				$this->Mvanbandi->capnhatTrangThai_XuLy($mavanban,$id_insert);
				return messagebox('Văn bản trình lãnh đạo thành công!','info');
			}
		}
	}
	public function DSVanBan($taikhoan) 
	{
		$config['base_url']             = base_url().'vanbandichoxuly_tp';
		$config['total_rows']           = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,1); // 1:là chờ xử lý
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandichoxuly_tp');
      	}
		$data['info']       = $this->Mvanbandi->layVBDi_ChoXuLy($taikhoan,1,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}
	
	
	public function themDuLieu($mavanban)
	{
			$time = time();
			$name = $_FILES['files'.$mavanban]['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_vabandi_'.date('Y'),$time,$mavanban);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}
					$tmp = str_replace('$', '-', $tmp);
					$tmp = $tmp.'.'.$a[$c];
										
					$files[] = array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_vabandi_'.date('Y').'/di_up_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
				}
				$this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
				
			}
	}
	
	public function upload($dir,$time,$mavanban)
	{
        $fileNumber = count($_FILES['files'.$mavanban]['name']);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files'.$mavanban]['name'][$i]) {
                    #initialization new file data
					$_FILES['files'.$mavanban]['name']     = $file['files'.$mavanban]['name'][$i];
					$_FILES['files'.$mavanban]['type']     = $file['files'.$mavanban]['type'][$i];
					$_FILES['files'.$mavanban]['tmp_name'] = $file['files'.$mavanban]['tmp_name'][$i];
					$_FILES['files'.$mavanban]['error']    = $file['files'.$mavanban]['error'][$i];
					$_FILES['files'.$mavanban]['size']     = $file['files'.$mavanban]['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'di_up_'.$time.'_'.clear($_FILES['files'.$mavanban]['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files'.$mavanban);
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
			
		}
		$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_luuvet_vbdi');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');
		if($kiemtra>0)
		{
			redirect(base_url().'vanbandichoxuly_pp');
			return messagebox('Xóa văn bản thành công!','info');
		}
				
	}

}

/* End of file Cvanbandichoxuly_tp.php */
/* Location: ./application/controllers/vanbandi/Cvanbandichoxuly_tp.php */