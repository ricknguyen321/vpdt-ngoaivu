<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsgiaymoidi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dsduthao']     = $this->Mvanbandi->layPhongBanDuThao();
		$data['dsnguoinhap']  = $this->Mvanbandi->layVanThu();
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi->layNguoiKy();
		if(_post('guimail'))
		{
			$data['content']=$this->guiMail();
		}
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		$vanban             = $this->DSVanBan();
		$data['dsvanban']	= $vanban['info'];
		if(!empty($data['dsvanban'])){
			foreach ($data['dsvanban'] as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$data['dsvanban'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$data['dsvanban'][$key]['sDuongDan'] = '';
				}
			}
		}
		$data['loaivanban'] = $vanban['loaivanban'];
		$data['noiduthao']  = $vanban['noiduthao'];
		$data['kyhieu']     = $vanban['kyhieu'];
		$data['tungay']  = $vanban['tungay'];
		$data['denngay']  = $vanban['denngay'];
		$data['trichyeu']   = $vanban['trichyeu'];
		$data['nguoikyvb']  = $vanban['nguoiky'];
		$data['nguoinhap']  = $vanban['nguoinhap'];
		$data['email']      = $vanban['email'];
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Danh sách giấy mời đi';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vdsgiaymoidi';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		}
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');
		if($kiemtra>0)
		{
			return messagebox('Xóa văn bản thành công!','info');
		}
	}
	public function guiMail()
	{
		$mavanban = _post('mavanban');
		if(!empty($mavanban))
		{
			foreach ($mavanban as $key => $value) {
				$this->createXML($key);
				$this->sendMail($key);
			}
			return messagebox('Gửi mail lên thành phố thành công','info');
		}
	}
	public function createXML($ma)
	{
		$thongtin = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		// cáu hình tạo file sdk 
		$xml 	  = new DOMDocument("1.0",'UTF-8');
		$STRLOAIVANBAN  = $xml->createElement("STRLOAIVANBAN");
		$STRLOAIVANBAN_Text = $xml->createTextNode($thongtin['sTenLVB']);
		$STRLOAIVANBAN->appendChild($STRLOAIVANBAN_Text);
		
		$STRKYHIEU   	= $xml->createElement("STRKYHIEU");
		$STRKYHIEU_Text = $xml->createTextNode($thongtin['sKyHieu']);
		$STRKYHIEU->appendChild($STRKYHIEU_Text);
		 
		$STRTRICHYEU  = $xml->createElement("STRTRICHYEU");
		$STRTRICHYEU_Text = $xml->createTextNode($thongtin['sMoTa']);
		$STRTRICHYEU->appendChild($STRTRICHYEU_Text);
		
		$STRNGAYKY  = $xml->createElement("STRNGAYKY");
		$STRNGAYKY_Text = $xml->createTextNode(date_select($thongtin['sNgayVBDi']));
		$STRNGAYKY->appendChild($STRNGAYKY_Text);
		
		$STRNGUOIKY  = $xml->createElement("STRNGUOIKY");
		$STRNGUOIKY_Text = $xml->createTextNode($thongtin['sHoTen']);
		$STRNGUOIKY->appendChild($STRNGUOIKY_Text);
		
		$STRCHUCDANH  = $xml->createElement("STRCHUCDANH");
		$STRCHUCDANH_Text = $xml->createTextNode($thongtin['sTenCV']);
		$STRCHUCDANH->appendChild($STRCHUCDANH_Text);
		
		$STRNOIGUI  = $xml->createElement("STRNOIGUI");
		$STRNOIGUI_Text = $xml->createTextNode('Văn phòng Sở Tài chính');
		$STRNOIGUI->appendChild($STRNOIGUI_Text);
		
		$STRNGAYHOP  = $xml->createElement("STRNGAYHOP");
		$STRNGAYHOP_Text = $xml->createTextNode(($thongtin['sNgayMoi']!='0000-00-00')?date_select($thongtin['sNgayMoi']):'');
		$STRNGAYHOP->appendChild($STRNGAYHOP_Text);
		
		$STRDIADIEM  = $xml->createElement("STRDIADIEM");
		$STRDIADIEM_Text = $xml->createTextNode($thongtin['sDiaDiemMoi']);
		$STRDIADIEM->appendChild($STRDIADIEM_Text);	
		
		$STRTHOIGIANHOP  = $xml->createElement("STRTHOIGIANHOP");
		$STRTHOIGIANHOP_Text = $xml->createTextNode($thongtin['sGioMoi']);
		$STRTHOIGIANHOP->appendChild($STRTHOIGIANHOP_Text);
		 
		$book 	= $xml->createElement("EXPORTMAIL");
		$book->appendChild($STRLOAIVANBAN);
		$book->appendChild($STRKYHIEU);
		$book->appendChild($STRTRICHYEU);
		$book->appendChild($STRNGAYKY);
		$book->appendChild($STRNGUOIKY);
		$book->appendChild($STRCHUCDANH);
		$book->appendChild($STRNOIGUI);
		$book->appendChild($STRNGAYHOP);
		$book->appendChild($STRDIADIEM);
		$book->appendChild($STRTHOIGIANHOP);
		
		$xml->appendChild($book);
		$xml->formatOutput = true;
		$xml->save("tmpxml/XMLOutput.sdk") or die("Error");
	}
	function sendMail($ma)
	{
		$thongtin   = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		$file 		= $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		$name       = 'Văn thư Sở Tài chính';
		$from_email = 'dothuonghieusale@gmail.com';
		$subject    = $thongtin['sKyHieu'].$thongtin['sMoTa']; // nội dung gửi mail
		$message    = $thongtin['sMoTa'];
		$to_email   = 'nxhai133@gmail.com';// mail nhận

        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'dothuonghieusale@gmail.com';// email gửi
        $config['smtp_pass'] = '123654haha/*-';// pass cua mail tren
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->load->library('email', $config);
        $this->email->initialize($config);

        //send mail
        $this->email->clear(TRUE);
        $this->email->from($from_email, $name);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        $filename = 'tmpxml/XMLOutput.sdk';
        if(!empty($file))
        {
        	foreach ($file as $key => $val) {
        		$this->email->attach($val['sDuongDan']);
        	}
        }
		$this->email->attach($filename);
		if ($this->email->send()) {
	    	$data=array(
				'iGuiMail'    => 1,
				'sNgayGuiMai' => date('Y-m-d')
	    		);
	    	$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi',$data);
	    } else {
	    echo $this->email->print_debugger();
	    }

	}
	public function DSVanBan() 
	{
		$email      = _get('email');
		$loaivanban = _get('loaivanban');
		$noiduthao  = _get('noiduthao');
		$kyhieu     = _get('kyhieu');
		$tungay   = _get('tungay');
		$denngay   = _get('denngay');
		if(!empty($tungay))
		{
			$tungay = date_insert($tungay);
		}
		else{
			$tungay = '';
		}
		if(!empty($denngay))
		{
			$denngay = date_insert($denngay);
		}
		else{
			$denngay = '';
		}
		$trichyeu   = _get('trichyeu');
		$nguoiky    = _get('nguoiky');
		$nguoinhap  = _get('nguoinhap');
		// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời: default 1 là không phải giấy mời
		$config['base_url']             = base_url().'dsgiaymoidi?loaivanban='.$loaivanban.'&noiduthao='.$noiduthao.'&kyhieu='.$kyhieu.'&tungay='.$tungay.'&denngay='.$denngay.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&nguoinhap='.$nguoinhap;
		$config['total_rows']           = $this->Mvanbandi->demDSVBDi($loaivanban,$noiduthao,$kyhieu,$tungay,$denngay,$trichyeu,$nguoiky,$nguoinhap,NULL,$email,0,1,10);
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dsgiaymoidi');
      	}
		$data['loaivanban'] = $loaivanban;
		$data['noiduthao']  = $noiduthao;
		$data['kyhieu']     = $kyhieu;
		$data['tungay']  = $ngaynhap;
		$data['trichyeu']   = $trichyeu;
		$data['nguoiky']    = $nguoiky;
		$data['nguoinhap']  = $nguoinhap;
		$data['email']  	= $email;
		$data['info']       = $this->Mvanbandi->layDSGM($kyhieu,$tungay,$denngay,$trichyeu,$nguoiky,$nguoinhap,$phongban,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdsgiaymoidi.php */
/* Location: ./application/controllers/vanbandi/Cdsgiaymoidi.php */