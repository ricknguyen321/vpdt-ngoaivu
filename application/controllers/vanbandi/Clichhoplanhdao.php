<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clichhoplanhdao extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		
		for($i=1;$i<=7;$i++)
		{
			$ngaytuan[] = $this->sw_get_current_weekday($i);
		}
		$year                  = date('Y');
		$week                  = date('W');
		$start_date            = strtotime( $year . "W". $week . 1);
		$end_date              = strtotime( $year . "W". $week . 7);
		$data['ngaybd']        = date('d/m/Y',$start_date);
		$data['ngaykt']        = date('d/m/Y',$end_date);
		$data['ngaytrongtuan'] = $ngaytuan;
		$data['lichsang']	   = $this->Mvanbandi->layLichHop2(date('Y-m-d',$start_date),date('Y-m-d',$end_date),'sang');
		if(!empty($data['lichsang']))
		{
			foreach ($data['lichsang'] as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$data['lichsang'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$data['lichsang'][$key]['sDuongDan'] = '';
				}
			}
		}
		$data['lichchieu']	   = $this->Mvanbandi->layLichHop2(date('Y-m-d',$start_date),date('Y-m-d',$end_date),'chieu');
		if(!empty($data['lichchieu']))
		{
			foreach ($data['lichchieu'] as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$data['lichchieu'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$data['lichchieu'][$key]['sDuongDan'] = '';
				}
			}
		}
		$data['title']         = 'Lịch họp lãnh đạo sở chủ trì';
		$data['url']           = base_url();
		$this->parser->parse('vanbandi/Vlichhoplanhdao',$data);
	}
	function sw_get_current_weekday($i) {
	    date_default_timezone_set('Asia/Ho_Chi_Minh');
	    $year = date('Y');
		$week = date('W');
		$start_date = strtotime( $year . "W". $week . $i);
		//$start_date ngày đầu tiên của tuần hiện tại
	    $weekday = date("l",$start_date);
	    $weekday = strtolower($weekday);
	    switch($weekday) {
	        case 'monday':
	            $weekday = 'THỨ HAI';
	            break;
	        case 'tuesday':
	            $weekday = 'THỨ BA';
	            break;
	        case 'wednesday':
	            $weekday = 'THỨ TƯ';
	            break;
	        case 'thursday':
	            $weekday = 'THỨ NĂM';
	            break;
	        case 'friday':
	            $weekday = 'THỨ SÁU';
	            break;
	        case 'saturday':
	            $weekday = 'THỨ BẢY';
	            break;
	        default:
	            $weekday = 'CHỦ NHẬT';
	            break;
	    }
	    return array($weekday,date('d/m/Y',strtotime( $year . "W". $week . $i)));
	}
}

/* End of file Clichhoplanhdao.php */
/* Location: ./application/controllers/vanbandi/Clichhoplanhdao.php */