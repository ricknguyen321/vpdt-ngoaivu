<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctimkiemvanbandi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dsduthao']     = $this->Mvanbandi->layPhongBanDuThao();
		$data['dsnguoinhap']  = $this->Mvanbandi->layVanThu();
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi->layNguoiKy();
		$vanban             = $this->DSVanBan();
		$data['dsvanban']	= $vanban['info'];
		$data['loaivanban'] = $vanban['loaivanban'];
		$data['noiduthao']  = $vanban['noiduthao'];
		$data['kyhieu']     = $vanban['kyhieu'];
		$data['ngaythang']  = $vanban['ngaythang'];
		$data['trichyeu']   = $vanban['trichyeu'];
		$data['nguoikyvb']  = $vanban['nguoiky'];
		$data['nguoinhap']  = $vanban['nguoinhap'];
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Tìm kiếm văn bản đi';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vtimkiemvanbandi';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBan() 
	{
		$loaivanban = _get('loaivanban');
		$noiduthao  = _get('noiduthao');
		$kyhieu     = _get('kyhieu');
		$ngaynhap  = _get('ngaythang');
		if(!empty($ngaynhap))
		{
			$ngaythang = date_insert($ngaynhap);
		}
		else{
			$ngaythang = '';
		}
		$trichyeu   = _get('trichyeu');
		$nguoiky    = _get('nguoiky');
		$nguoinhap  = _get('nguoinhap');
		// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời: default 1 là không phải giấy mời
		$config['base_url']             = base_url().'timkiemvanbandi?loaivanban='.$loaivanban.'&noiduthao='.$noiduthao.'&kyhieu='.$kyhieu.'&ngaythang='.$ngaythang.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&nguoinhap='.$nguoinhap;
		$config['total_rows']           = $this->Mvanbandi->demDSVBDi($loaivanban,$noiduthao,$kyhieu,$ngaythang,$trichyeu,$nguoiky,$nguoinhap,NULL,NULL,1,1);
		$config['per_page']             = 20;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'timkiemvanbandi');
      	}
		$data['loaivanban'] = $loaivanban;
		$data['noiduthao']  = $noiduthao;
		$data['kyhieu']     = $kyhieu;
		$data['ngaythang']  = $ngaynhap;
		$data['trichyeu']   = $trichyeu;
		$data['nguoiky']    = $nguoiky;
		$data['nguoinhap']  = $nguoinhap;
		if(_get('timkiem'))
		{
			$data['info']       = $this->Mvanbandi->layDSVBDi($loaivanban,$noiduthao,$kyhieu,$ngaythang,$trichyeu,$nguoiky,$nguoinhap,NULL,NULL,1,1,$config['per_page'], $data['page']);
			$data['pagination'] = $this->pagination->create_links();
		}
		else{
			$data['info'] 		= '';
		}	$data['pagination'] = '';
		
        return $data;
	}

}

/* End of file Ctimkiemvanbandi.php */
/* Location: ./application/controllers/vanbandi/Ctimkiemvanbandi.php */