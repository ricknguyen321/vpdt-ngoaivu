<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandichoxuly_pp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if($phongbanHD==12)
		{
			$quyenhan		= array(11);
			$chucvuphong	= '';
			$quyendb        = $quyendb;
		}
		elseif ($phongbanHD==11) {
			$quyenhan		= array(3);
			$chucvuphong	= array(6);
			$quyendb 		= '';
		}
		else{
			$quyenhan		= array(6);
			$chucvuphong	= '';
			$quyendb 		= '';
		}
		$data['truongphong'] = $this->Mvanbandi->layLD_Phong($phongbanHD,$quyenhan,$chucvuphong,$quyendb);
		if(_post('guilen'))
		{
			$data['content'] = $this->guilen();
		}
		if(_post('tralai'))
		{
			$data['content'] = $this->tralai();
		}
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		$vanban            = $this->DSVanBan($taikhoan);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách VB đi chờ xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandichoxuly_pp';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function tralai()
	{
		$mavanban = _post('tralai');
		$ykien    = nl2br(_post('ykien_'.$mavanban));
		$taikhoan = $this->_session['PK_iMaCB'];
		$chuyenvien = $this->Mvanbandi->layCV_TraLai($mavanban,$taikhoan);
		$data=array(
			'FK_iMaVBDi'	=> $mavanban,
			'FK_iMaCB_Gui'	=> $taikhoan,
			'FK_iMaCB_Nhan' => $chuyenvien['FK_iMaCB_Gui'],
			'sYKien'		=> $ykien,
			'sThoiGian'		=> date('Y-m-d H:i:s'),
			'iTrangThai'	=> 1,
			'phong_lanhdao' => 1
		);
		$kiemtraxuly = $this->Mvanbandi->kiemtra_XuLy($mavanban,$taikhoan);
		if($kiemtraxuly>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
			if($id_insert>0)
			{
				$this->Mvanbandi->capnhatTrangThai_XuLy($mavanban,$id_insert);
				$this->themDuLieu($mavanban);
				return messagebox('Văn bản trả lại thành công!','info');
			}
		}
		return messagebox('Có lỗi xảy ra!','danger');
	}
	public function guilen()
	{
		$mavanban		= _post('guilen');
		$ykien			= nl2br(_post('ykien_'.$mavanban));
		$truongphong	= _post('truongphong_'.$mavanban);
		$taikhoan		= $this->_session['PK_iMaCB'];
		
		$this->themDuLieu($mavanban);
		
		$data=array(
			'FK_iMaVBDi'	=> $mavanban,
			'FK_iMaCB_Gui'	=> $taikhoan,
			'FK_iMaCB_Nhan' => $truongphong,
			'sYKien'		=> $ykien,
			'sThoiGian'		=> date('Y-m-d H:i:s'),
			'iTrangThai'	=> 1,
			'phong_lanhdao' => 1
		);
		$kiemtraxuly = $this->Mvanbandi->kiemtra_XuLy($mavanban,$taikhoan);
		if($kiemtraxuly>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
			if($id_insert>0)
			{
				$this->Mvanbandi->capnhatTrangThai_XuLy($mavanban,$id_insert);
				return messagebox('Văn bản trình lãnh đạo thành công!','info');
			}
		}
		return messagebox('Có lỗi xảy ra!','danger');
	}
	public function DSVanBan($taikhoan) 
	{
		$config['base_url']             = base_url().'vanbandichoxuly_pp';
		$config['total_rows']           = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,1); // 1:là chờ xử lý
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandichoxuly_pp');
      	}
		$data['info']       = $this->Mvanbandi->layVBDi_ChoXuLy($taikhoan,1,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}
	
	
	public function themDuLieu($mavanban)
	{
			$time = time();
			$name = $_FILES['files'.$mavanban]['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_vabandi_'.date('Y'),$time,$mavanban);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}
					$tmp = str_replace('$', '-', $tmp);
					$tmp = $tmp.'.'.$a[$c];
										
					$files[] = array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_vabandi_'.date('Y').'/di_up_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
				}
				$this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
				
			}
	}
	
	public function upload($dir,$time,$mavanban)
	{
        $fileNumber = count($_FILES['files'.$mavanban]['name']);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files'.$mavanban]['name'][$i]) {
                    #initialization new file data
					$_FILES['files'.$mavanban]['name']     = $file['files'.$mavanban]['name'][$i];
					$_FILES['files'.$mavanban]['type']     = $file['files'.$mavanban]['type'][$i];
					$_FILES['files'.$mavanban]['tmp_name'] = $file['files'.$mavanban]['tmp_name'][$i];
					$_FILES['files'.$mavanban]['error']    = $file['files'.$mavanban]['error'][$i];
					$_FILES['files'.$mavanban]['size']     = $file['files'.$mavanban]['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'di_up_'.$time.'_'.clear($_FILES['files'.$mavanban]['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files'.$mavanban);
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
			
		}
		$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_luuvet_vbdi');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');
		if($kiemtra>0)
		{
			redirect(base_url().'vanbandichoxuly_pp');
			return messagebox('Xóa văn bản thành công!','info');
		}
				
	}

}

/* End of file Cvanbandichoxuly_pp.php */
/* Location: ./application/controllers/vanbandi/Cvanbandichoxuly_pp.php */