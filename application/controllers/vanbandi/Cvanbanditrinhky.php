<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanditrinhky extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		$ds                = $this->Mvanbandi->layVBDi_DaTrinhKy($taikhoan);
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách văn bản trình ký';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbanditrinhky';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		}
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');
		if($kiemtra>0)
		{
			return messagebox('Xóa văn bản thành công!','info');
		}
	}

}

/* End of file Cvanbanditrinhky.php */
/* Location: ./application/controllers/vanbandi/Cvanbanditrinhky.php */