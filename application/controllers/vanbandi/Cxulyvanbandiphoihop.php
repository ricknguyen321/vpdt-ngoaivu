<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxulyvanbandiphoihop extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$maphoihop = $this->uri->segment(2);
		$thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaPH',$maphoihop,'tbl_phoihop_vbdi');
		if($thongtin[0]['iXuLy']==1)
		{
			redirect('vanbandiphoihopdaxuly');
		}
		if(_post('luudulieu'))
		{
			$this->luuDuLieu();
		}
		$data['title']    = 'Danh sách văn bản đi phối hợp chờ xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vxulyvanbandiphoihop';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function luuDuLieu()
	{
		$maphoihop = $this->uri->segment(2);
		$name = $_FILES['files']['name'];
		if(!empty($name[0]))
		{
			$this->upload('doc_uploads_'.date('Y'));
		}
		$data=array(
			'noidung_xuly'  => _post('ykien'),
			'thoigian_xuly' => date('Y-m-d H:i:s'),
			'iXuLy'         => 1,
			'sFile'         => ($name[0])?'doc_uploads_'.date('Y').'/'.clear($name[0]):''
			);
		$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaPH',$maphoihop,'tbl_phoihop_vbdi',$data);
		if($kiemtra>0)
		{
			redirect('vanbandiphoihopdaxuly');
		}
	}
	public function upload($dir)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
}

/* End of file Cxulyvanbandiphoihop.php */
/* Location: ./application/controllers/vanbandi/Cxulyvanbandiphoihop.php */