<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandichoxuly_cct extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb    = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$data['lanhdaoso'] 	  = $this->Mvanbandi->layNguoiKy();
		if(_post('guilen'))
		{
			$data['content'] = $this->guilen();
		}
		if(_post('tralai')){
			$data['content'] = $this->tralai();
		}
		$vanban            = $this->DSVanBan($taikhoan);
		// pr($ten_trangthai_xem);
		$ds                = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		if(!empty($ds)){
			foreach ($ds as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$ds[$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$ds[$key]['sDuongDan'] = '';
				}
			}
		}
		$data['dsvanban'] = $ds;
		$data['title']    = 'Danh sách văn bản trình ký';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vvanbandichoxuly_cct';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function tralai()
	{
		$mavanban = _post('tralai');
		$ykien    = _post('ykien_'.$mavanban);
		$taikhoan = $this->_session['PK_iMaCB'];
		$ma       = $this->Mvanbandi->layMaMoiNhat_Cu($mavanban,$taikhoan);
		$canbophong = $this->Mvanbandi->laynguoitralai($mavanban,$ma['PK_iMaNhatKy']);
		$kiemtraxuly = $this->Mvanbandi->kiemtra_XuLy($mavanban,$taikhoan);
		if($kiemtraxuly>0)
		{
			if(!empty($canbophong))
			{
				foreach ($canbophong as $value) {
					$data=array(
						'FK_iMaVBDi'	=> $mavanban,
						'FK_iMaCB_Gui'	=> $taikhoan,
						'FK_iMaCB_Nhan' => $value['FK_iMaCB_Gui'],
						'sYKien'		=> $ykien,
						'sThoiGian'		=> date('Y-m-d H:i:s'),
						'iTrangThai'	=> 1,
						'phong_lanhdao' => 1
					);
					$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
					if($id_insert>0)
					{
						$this->Mvanbandi->capnhatTrangThai_XuLy_TL($mavanban,$taikhoan);
					}
				}
				return messagebox('Văn bản trình lãnh đạo thành công!','info');
			}
		}
	}
	public function guilen()
	{
		$mavanban	= _post('guilen');
		$ykien		= _post('ykien_'.$mavanban);
		$lanhdaoso	= _post('lanhdaoso_'.$mavanban.'_xem');
		if(!empty($lanhdaoso))
		{
			foreach ($lanhdaoso as $key => $value) {
				$data_them[] = array(
					'FK_iMaVB' => $mavanban,
					'FK_iMaCB' => $value
				);
			}
			$this->Mdanhmuc->themNhieuDuLieu('tbl_lanhdaoxem',$data_them);
		}
		$taikhoan = $this->_session['PK_iMaCB'];
		$data=array(
			'FK_iMaVBDi'	=> $mavanban,
			'FK_iMaCB_Gui'	=> $taikhoan,
			'FK_iMaCB_Nhan' => _post('lanhdaoso_'.$mavanban),
			'sYKien'		=> $ykien,
			'sThoiGian'		=> date('Y-m-d H:i:s'),
			'iTrangThai'	=> 1,
			'phong_lanhdao' => 2
		);
		$kiemtraxuly = $this->Mvanbandi->kiemtra_XuLy($mavanban,$taikhoan);
		if($kiemtraxuly>0)
		{
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_luuvet_vbdi',$data);
			if($id_insert>0)
			{
				$this->Mvanbandi->capnhatTrangThaiChoSo($mavanban);
				$this->Mvanbandi->capnhatTrangThai_XuLy($mavanban,$id_insert);
				return messagebox('Văn bản trình lãnh đạo thành công!','info');
			}
		}
	}
	public function DSVanBan($taikhoan) 
	{
		$config['base_url']             = base_url().'vanbandichoxuly_tp';
		$config['total_rows']           = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,1); // 1:là chờ xử lý
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'vanbandichoxuly_tp');
      	}
		$data['info']       = $this->Mvanbandi->layVBDi_ChoXuLy($taikhoan,1,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cvanbandichoxuly_cct.php */
/* Location: ./application/controllers/vanbandi/Cvanbandichoxuly_cct.php */