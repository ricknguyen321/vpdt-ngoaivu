<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cteptindi extends MY_Controller {
	protected $_thongtin;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$mavanban         = _get('id');
		$mafile           = _get('mafile');
		if(!empty($mafile))
		{
			$data['content'] = $this->capnhatDuLieu($mavanban,$mafile);
		}
		else{
			$data['content'] = $this->themDuLieu($mavanban);
		}
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu($mavanban);
		}
		$data['mavanban'] = $mavanban;
		$data['thongtin'] = $this->_thongtin;
		$data['kyhieu']   = $this->Mdanhmuc->layDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi');
		$data['dsfile']	  = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavanban,'tbl_files_vbdi');
		$data['title']    = 'Tệp tin văn bản đi';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vteptindi';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_vbdi');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		if($kiemtra>0)
		{
			$dulieu = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavanban,'tbl_files_vbdi');
			if(empty($dulieu))
			{
				$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi','iFile',0);
			}
			redirect('teptindi?id='.$mavanban);
		}
	}
	public function capnhatDuLieu($mavanban,$mafile)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaFileDi',$mafile,'tbl_files_vbdi');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenFile' => _post('tenteptin'),
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaFileDi',$mafile,'tbl_files_vbdi',$data);
				if($kiemtra>0)
				{
					redirect('teptindi?id='.$mavanban);
				}
				else{
					return messagebox('Cập nhật tệp tin thất bại','danger');
				}
		}
	}
	public function themDuLieu($mavanban)
	{
		if(_post('luudulieu'))
		{
			$time = time();
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_vabandi_'.date('Y'),$time);
				$files = array();
				foreach ($name as $key => $value) {
					
					//ricknguyen321
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}					
					$tmp = str_replace('$', '-', $tmp);					
					$tmp = $tmp.'.'.$a[$c];
					
					$files[] = array(
						'FK_iMaVBDi' => $mavanban,
						'sTenFile'   => (_post('tenteptin')!='')?_post('tenteptin'):$value,
						'sDuongDan'  => 'doc_vabandi_'.date('Y').'/di_up_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_vbdi',$files);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$mavanban,'tbl_vanbandi','iFile',1);
					return messagebox('Thêm tệp tin THÀNH CÔNG','info');
				}
				else{
					return messagebox('Thêm tệp tin thất bại','danger');
				}
			}
		}
	}
	public function upload($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'di_up_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cteptin.php */
/* Location: ./application/controllers/vanbandi/Cteptindi.php */