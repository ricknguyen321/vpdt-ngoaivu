<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsvanbanchoso extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
		$this->load->library('pagination');
	}
	public function index()
	{
		if(_post('duyet'))
		{
			$ma				= _post('duyet');
			$thongtin		= $this->Mdanhmuc->layDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');//pr($thongtin);
			$loai 			= $thongtin[0]['FK_iMaLVB'];
			$thongtincanbo		= $this->Mdanhmuc->layDuLieu('PK_iMaCB',$thongtin[0]['FK_iMaCB_Nhap'],'tbl_canbo');//pr($thongtincanbo);

			$thongtingui	= $this->Mvanbandi->laythongtinvanbandi($ma);
			//pr($thongtingui);
			$duongdan		= $this->Mvanbandi->layfilevanbandi($ma);//pr($duongdan);
			
			
			
			// kiểm tra phải giấy mời hay không. bằng 10 là giấy mời
			
			$solonnhat = $this->Mvanbandi->laySoDiMoi($loai);
			
			$sodi      = $solonnhat['iSoVBDi'] +1;        
			$ngaythang = _post("ngaythang[$ma]");
			echo $ngaythang;
			$data=array(
				'iSoVBDi'            => $sodi,
				'sNgayVBDi'          => date_insert($ngaythang[0]),
				'trangthai_xem'      => 2,
				'trangthai_xem_bpth' => 2,
				'trangthai_xem_pgd'  => 2,
				'trangthai_xem_gd'   => 2,
				'trangthai_xem_cvp'  => 2
				);
			$kiemtra= $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi',$data);		

			if($kiemtra>0)
			{
				$data['thongbaoso'] = $sodi;
			}
			
			
		}
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		
		
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');//pr($data['dsloaivanban']);
		$data['dsduthao']     = $this->Mvanbandi->layPhongBanDuThao();
		$data['dsnguoinhap']  = $this->Mvanbandi->layVanThu();
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi->layNguoiKy();
		$vanban             = $this->DSVanBan();
		$data['dsvanban']	= $vanban['info'];
		if(!empty($data['dsvanban'])){
			foreach ($data['dsvanban'] as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);

				if(!empty($duongdan))
				{
					$data['dsvanban'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$data['dsvanban'][$key]['sDuongDan'] = '';
				}
			}
		}

		$datetime = new DateTime(date('Y-m-d H:i:s'));
		$datetimeios8601 = $datetime->format(DateTime::ATOM);

		$solonnhatGM = $this->Mvanbandi->laySoDiMoi(10)['iSoVBDi'] +1;
		$solonnhatCV = $this->Mvanbandi->laySoDiMoi(1)['iSoVBDi'] +1;

		$data['datetimeios8601'] = $datetimeios8601;
		$data['solonnhatGM'] = $solonnhatGM;
		$data['solonnhatCV'] = $solonnhatCV;
		$data['ci_session'] = $this->session->session_id;

		$data['loaivanban'] = $vanban['loaivanban'];
		$data['noiduthao']  = $vanban['noiduthao'];
		$data['kyhieu']     = $vanban['kyhieu'];
		$data['ngaythang']  = $vanban['ngaythang'];
		$data['trichyeu']   = $vanban['trichyeu'];
		$data['nguoikyvb']  = $vanban['nguoiky'];
		$data['nguoinhap']  = $vanban['nguoinhap'];
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Danh sách văn bản chờ số';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vdsvanbanchoso';
		$this->load->view('layout_admin/layout',$temp);
	}
	
	public function layQTChuyen($mavb)
	{
		return $this->Mtruyennhan->layQTChuyen($mavb);
		
	}
	
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
			
		}
		$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_luuvet_vbdi');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');
		if($kiemtra>0)
		{
			return messagebox('Xóa văn bản thành công!','info');
		}
				
	}
	public function DSVanBan() 
	{
		
		$loaivanban = _get('loaivanban');
		$noiduthao  = _get('noiduthao');
		$kyhieu     = _get('kyhieu');
		$ngaynhap  = _get('ngaythang');
		if(!empty($ngaynhap))
		{
			$ngaythang = date_insert($ngaynhap);
		}
		else{
			$ngaythang = '';
		}
		$trichyeu   = _get('trichyeu');
		$nguoiky    = _get('nguoiky');
		$nguoinhap  = _get('nguoinhap');
		// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời: default 1 là không phải giấy mời
		$config['base_url']             = base_url().'dsvanbanchoso?loaivanban='.$loaivanban.'&noiduthao='.$noiduthao.'&kyhieu='.$kyhieu.'&ngaythang='.$ngaythang.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&nguoinhap='.$nguoinhap;
		$config['total_rows']           = count($this->Mvanbandi->layVanBanChoSo($loaivanban,$noiduthao,$kyhieu,$ngaythang,$trichyeu,$nguoiky,$nguoinhap,NULL,NULL,0,0,NULL));
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dsvanbanchoso');
      	}
		$data['loaivanban'] = $loaivanban;
		$data['noiduthao']  = $noiduthao;
		$data['kyhieu']     = $kyhieu;
		$data['ngaythang']  = $ngaynhap;
		$data['trichyeu']   = $trichyeu;
		$data['nguoiky']    = $nguoiky;
		$data['nguoinhap']  = $nguoinhap;
		
		$data['info']       = $this->Mvanbandi->layVanBanChoSo($loaivanban,$noiduthao,$kyhieu,$ngaythang,$trichyeu,$nguoiky,$nguoinhap,NULL,NULL,0,0,NULL,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}
}
/* End of file Cdsvanbanchoso.php */
/* Location: ./application/controllers/vanbandi/Cdsvanbanchoso.php */