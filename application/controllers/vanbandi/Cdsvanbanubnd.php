<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsvanbanubnd extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->library('Excel');
	}
	public function index()
	{
		$data['dsphongban'] = $this->Mvanbandi->layPhongBanDuThao();
		$data['dsphongsl']    = '';
		$data['dsthongtinvb'] = '';
		$bd = _get('tungay');
		$kt = _get('denngay');
		($bd)?$tungay   = date_insert(_get('tungay')):$tungay='';
		($kt)?$denngay  = date_insert(_get('denngay')):$denngay='';
		$phongban = _get('phongban');
		if(_post('export'))
		{
			$this->export($tungay,$denngay,$phongban);
		}
		if(_get('loc'))
		{
			$data['dsphongsl']    = $this->Mvanbandi->layPBvaSLVB($tungay,$denngay,$phongban,$this->_session['PK_iMaCB']);
			$data['dsthongtinvb'] = $this->Mvanbandi->layThongTinVB($tungay,$denngay,$phongban,$this->_session['PK_iMaCB']);
		}
		$data['title']      = 'Danh sách văn bản UBND';
		$temp['data']       = $data;
		$temp['template']   = 'vanbandi/Vdsvanbanubnd';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function export($tungay,$denngay,$phongban)
	{
		$objPHPExcel  =new PHPExcel();
		$filename     ='DSVB_SNGV_TMUB'.' - '.date('d-m-Y',time());
        $objPHPExcel->getProperties()->setCreator("Administrator")
                                     ->setLastModifiedBy("Administrator")
                                     ->setTitle("Danh sách các văn bản sở Ngoại vụ đã tham mưu trình UBND thành phố")
                                     ->setSubject("Danh sách các văn bản sở Ngoại vụ đã tham mưu trình UBND thành phố")
                                     ->setDescription("Danh sách các văn bản sở Ngoại vụ đã tham mưu trình UBND thành phố")
                                     ->setKeywords("office 2010 openxml php")
                                     ->setCategory("Information of student");
        //Căn dòng chữ
        $dinhdang = array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
         $dinhdang2 = array(
            // 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
        // chỉnh kích cỡ cột
        $array_column = array(
                'A' => 5,
                'B' => 19,
                'C' => 13,
                'D' => 20,
                'E' => 43,
                'F' => 14,
                'G' => 27
            );
            foreach($array_column as $key => $value){
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setAutoSize(false);
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setWidth($value);
            }

        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(13);
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();

        //Phần tiêu đề (nếu có)
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:G1')->setCellValue('A1','Danh sách các văn bản sở Ngoại vụ đã tham mưu trình UBND thành phố');
        //end sét chiều vào
        $array_content2 = array(
                'A1');
        // định dạng và in đậm
        foreach($array_content2 as $key => $val){
            $objPHPExcel->getActiveSheet()->getStyle($val)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setSize(16);
        }
        //sét chiều cao
        $array_row = array(
				'1' => 35,
				'2' => 25,
				'3' => 25
            );

        foreach($array_row as $key => $value){
            $objPHPExcel->getActiveSheet()->getRowDimension($key)->setRowHeight($value);
        }
        //end sét chiều vào
        $array_content = array(
            'A2' => 'STT',
            'B2' => 'Số ký hiệu',
            'C2' => 'Ngày ký',
            'D2' => 'Loại văn bản',
            'E2' => 'Trích yếu',
            'F2' => 'Người ký',
            'G2' => 'Đơn vị ban hành'
            );
        // định dạng và in đậm
        foreach($array_content as $key => $value){
            $objPHPExcel->getActiveSheet()->setCellValue($key,$value);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getFont()->setBold(true);
        }
        $dsphongsl    = $this->Mvanbandi->layPBvaSLVB($tungay,$denngay,$phongban,$this->_session['PK_iMaCB']);
		$dsthongtinvb = $this->Mvanbandi->layThongTinVB($tungay,$denngay,$phongban,$this->_session['PK_iMaCB']);
        $startRow = 3;
        $stt=1;
        if(!empty($dsphongsl)){
        	foreach ($dsphongsl as $key => $pb) {
        		$objPHPExcel->getActiveSheet()->mergeCells('A'.$startRow.':G'.$startRow)->setCellValue('A'.$startRow,intToRoman($stt).'. '.$pb['sTenPB'].' - Tống số: '.$pb['tong'].' Văn Bản')->getStyle('A'.$startRow)->getFont()->setBold(true);;
        		$objPHPExcel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
        		$objPHPExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(25);
        		$stt2=1;
        		$startRow++;
        		if(!empty($dsthongtinvb)){
        			foreach ($dsthongtinvb as $key => $value) {
        				if($value['FK_iMaPB']==$pb['FK_iMaPB']){
	        				$objPHPExcel->getActiveSheet()->setCellValue('A'.$startRow,$stt2)->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('B'.$startRow,$value['iSoVBDi'].'/'.$value['sKyHieu'])->getStyle('B'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('C'.$startRow,date_select($value['sNgayVBDi']))->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('D'.$startRow,$value['sTenLVB'])->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('E'.$startRow,$value['sMoTa'])->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('F'.$startRow,$value['sHoTen'])->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('G'.$startRow,$value['sTenPB'])->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$startRow++;
			        		$stt2++;
			        	}
        			}
        		}
        		$stt++;
        	}
        }
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();
        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A2".":"."G".$maxR)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=".$filename.".xls");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
	}
}

/* End of file Cdsvanbanubnd.php */
/* Location: ./application/controllers/vanbandi/Cdsvanbanubnd.php */