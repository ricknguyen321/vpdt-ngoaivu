<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhaplichhop extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		$ma = _get('id');
		if ($ma > 0) {
			$data['tailieu'] = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_tlhop');
			if(_post('xoa'))
			{
				$data['content'] = $this->xoaDuLieu($ma);
			}
		}
		if(!empty($ma))
		{
			$this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin']	  = $this->_thongtin;
		
		$data['phongban'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB','','tbl_phongban');
		
		$data['dsph'] = explode(', ',$this->_thongtin[0]['sPhongPH']);
		
		$data['mangld']= explode(', ',$this->_thongtin[0]['FK_iMaCB_Ky']);
		
		$giomoi = $this->_thongtin[0]['sGioMoi'];
		$manggio = explode(':',$giomoi);
		if (empty($manggio) || count($manggio) == 1) {
			$manggio = explode('h',$giomoi);
		} 
		$gio = $manggio[0];
		$phut = $manggio[1];
	
		$data['gio'] 	  = $gio;
		$data['phut'] 	  = $phut;
		//$data['giomoi']	  
		$data['nguoiky'] 	  = $this->Mvanbandi->layNguoiKyHai();
		$data['title']        = 'Nhập mới lịch họp';
		$temp['data']         = $data;
		$temp['template']     = 'vanbandi/Vnhaplichhop';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaVBDi',$ma,'tbl_lichhopso');
		if(_post('luudulieu'))
		{
			$phongph = _post('phongph');
			if(!empty($phongph))
			{
				$phongph= implode(', ', $phongph);
			}
			
			$ngaymoi = _post('ngaymoi');
			if(!empty($ngaymoi))
			{
				$ngay    = date_insert($ngaymoi);
				$gio = _post('gio');
				$phut = _post('phut');
				$gioimoi = $gio.':'.$phut;
			}
			else{
				$ngay    = '';
				$gioimoi ='';
			}
			
			$nguoiky = _post('nguoiky');		
			if(!empty($nguoiky))
			{
				$nguoiky= implode(', ', $nguoiky);
			}
			
			$data=array(
				'FK_iMaPB'		   => $this->_session['FK_iMaPhongHD'],
				'sNgayMoi'         => $ngay,
				'sGioMoi'          => $gioimoi,
				'sDiaDiemMoi'      => _post('diadiem'),
				'sMoTa'            => _post('mota'),
				'sPhongCT'         => _post('phongct'),
				'sPhongPH'         => $phongph,
				'sChiDao'          => nl2br(_post('chidao')),
				'FK_iMaCB_Ky'      => $nguoiky,
				//'FK_iMaCB_Nhap'    => $this->_session['PK_iMaCB'],
				//'sNgayNhap'        => date('Y-m-d H:i:s', time())
			);
				
			if(empty($ngay) || $ngay == '1970-01-01')
			{
				$ngay = date('Y-m-d',time());
				$data=array(
				'FK_iMaPB'		   => $this->_session['FK_iMaPhongHD'],
				'sNgayMoi'         => $ngay,
				'sGioMoi'          => $gioimoi,
				'sDiaDiemMoi'      => _post('diadiem'),
				'sMoTa'            => _post('mota'),
				'sPhongCT'         => _post('phongct'),
				'sPhongPH'         => $phongph,
				'sChiDao'          => nl2br(_post('chidao')),
				'FK_iMaCB_Ky'      => $nguoiky,
				//'FK_iMaCB_Nhap'    => $this->_session['PK_iMaCB'],
				//'sNgayNhap'        => date('Y-m-d H:i:s', time())
			);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa chọn ngày họp','danger');
			}
			if(empty($gioimoi))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập giờ họp','danger');
			}
			if(empty(_post('mota')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập nội dung cuộc họp','danger');
			}
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDi',$ma,'tbl_lichhopso',$data);
			$name    = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('doc_uploads_'.date('Y'), $time);
				$files = array();
				foreach ($name as $key => $value) {
					$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}						
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaVBDi' => $ma,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_uploads_'.date('Y').'/tl_hop_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
				}
				$kiemtrafile=$this->Mdanhmuc->themNhieuDuLieu('tbl_files_tlhop',$files);
				if($kiemtrafile>0)
				{
					$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$ma,'tbl_lichhopso','iFile',1);
				}
			}
			if($kiemtra>0)
			{				
				redirect('lichhopso');						
			}
		}
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_tlhop');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		redirect('nhaplichhop?id='.$mavanban);
		
	}
	public function themDuLieu()
	{
		$time = time();
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if(_post('luudulieu'))
		{
			$canbo		= $this->_session['PK_iMaCB'];
			$ngaymoi	= _post('ngaymoi');
			$phongph = _post('phongph');
			if(!empty($phongph))
			{
				$phongph= implode(', ', $phongph);
			}
			
			$nguoiky = _post('nguoiky');		
			if(!empty($nguoiky))
			{
				$nguoiky= implode(', ', $nguoiky);
			}

			$ngay    = date_insert($ngaymoi);
			//$gioimoi = _post('giomoi');
			$gio = _post('gio');
			$phut = _post('phut');
			$gioimoi = $gio.':'.$phut;
			
			$data=array(
				'FK_iMaPB'		   => $this->_session['FK_iMaPhongHD'],
				'sNgayMoi'         => $ngay,
				'sGioMoi'          => $gioimoi,
				'sDiaDiemMoi'      => _post('diadiem'),
				'sMoTa'            => _post('mota'),
				'sPhongCT'         => _post('phongct'),
				'sPhongPH'         => $phongph,
				'sChiDao'          => nl2br(_post('chidao')),
				'FK_iMaCB_Ky'      => $nguoiky,
				'FK_iMaCB_Nhap'    => $this->_session['PK_iMaCB'],
				'sNgayNhap'        => date('Y-m-d H:i:s', time())
			);
			
			
			if(empty($ngay) || $ngay == '1970-01-01')
			{
				$ngay = date('Y-m-d',time());
				$data=array(
				'FK_iMaPB'		   => $this->_session['FK_iMaPhongHD'],
				'sNgayMoi'         => $ngay,
				'sGioMoi'          => $gioimoi,
				'sDiaDiemMoi'      => _post('diadiem'),
				'sMoTa'            => _post('mota'),
				'sPhongCT'         => _post('phongct'),
				'sChiDao'            => nl2br(_post('chidao')),
				'FK_iMaCB_Ky'      => $nguoiky,
				'FK_iMaCB_Nhap'    => $this->_session['PK_iMaCB'],
				'sNgayNhap'        => date('Y-m-d H:i:s', time())
			);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa chọn ngày họp','danger');
			}
			if(empty($gioimoi))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập giờ họp','danger');
			}
			if(empty(_post('mota')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập nội dung cuộc họp','danger');
			}
			
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_lichhopso',$data);
			
			$name = $_FILES['files']['name'];
			
			
			if($id_insert>0)
			{
				
				$name = $_FILES['files']['name'];
				if(!empty($name[0]))
				{
					$this->upload('doc_uploads_'.date('Y'), $time);
					$files = array();
					foreach ($name as $key => $value) {						
						$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}						
						$tmp = str_replace('$', '-', $tmp);					
						$tmp = $tmp.'.'.$a[$c];
					
						$files[] = array(
							'FK_iMaVBDi' => $id_insert,
							'sTenFile'   => $value,
							'sDuongDan'  => 'doc_uploads_'.date('Y').'/tl_hop_'.$time.'_'.$tmp,
							'sThoiGian'  => date('Y-m-d H:i:s',time()),
							'FK_iMaCB'   => $this->_session['PK_iMaCB'],
							);
					}
					$kiemtrafile = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_tlhop',$files);
					if($kiemtrafile>0)
					{
						$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$id_insert,'tbl_lichhopso','iFile',1);
					}
				}
				echo '<script language="javascript">';
				echo 'alert(Thêm lịch họp THÀNH CÔNG)';  //not showing an alert box.
				echo '</script>';
				redirect('lichhopso');	
			}
			else{
				return messagebox('Thêm lịch họp thất bại','danger');
			}
		}
	}
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'tl_hop_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */