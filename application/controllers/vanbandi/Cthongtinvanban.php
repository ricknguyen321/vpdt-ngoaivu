<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongtinvanban extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mtruyennhan');
	}
	public function index()
	{
		$data['dssoden']    = '';
		$data['title']      = 'Thông tin văn bản đi';
		$mavaban            = _get('id');
		$data['dschuyen']   = $this->Mtruyennhan->layQTChuyen($mavaban);
		$data['dschuyenPH'] = $this->Mtruyennhan->layQTChuyenPH($mavaban);
		$canbo = $this->Mtruyennhan->layDSCB();
		foreach ($canbo as $key => $value) {
			$data['dscanbo'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$data['dsfile']	   = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavaban,'tbl_files_vbdi');
		$data['nguoinhap'] = $this->Mvanbandi->layNguoiNhap($mavaban);
		$thongtin  		   = $this->Mdanhmuc->layDuLieu('PK_iMaVBDi',$mavaban,'tbl_vanbandi');
		
		$data['chidao']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavaban,'tbl_chidao');
		$data['dexuat']         	= $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavaban,'tbl_ykien');
		
			// kiểm tra phải giấy mời hay không. bằng 10 là giấy mời
		if($thongtin[0]['iSoVBDi']>0)
		{
			if($thongtin[0]['FK_iMaLVB']==10)
			{
				$data['thongtin']  = $this->Mvanbandi->layVBChoSo($mavaban,1,10);
			}
			else{
				$data['thongtin']  = $this->Mvanbandi->layVBChoSo($mavaban,1,1);
			}
		}
		else{
			$data['thongtin']  = $this->Mvanbandi->layVBChoSo($mavaban,0,NULL);
		}
		if(empty($data['thongtin']))
		{
			redirect('welcome.html');
		}
		else{
			if(!empty((int)$data['thongtin'][0]['iSoDen'])){
				$soden = explode(',',$data['thongtin'][0]['iSoDen']);
				$data['dssoden'] = $this->Mvanbandi->layMaVBDen($soden);
			}else{
				$data['dssoden'] = '';
			}
		}
		
		if ($this->input->post('luulai')) {
			$chidaold = array(
                'FK_iMaVBDi'        => $mavaban,
                'sNoiDung'        => nl2br($this->input->post('chidao')),
                'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
                'sThoiGian'   => date('Y-m-d H:i:s',time()),
            );
			
			$this->Mdanhmuc->themDuLieu('tbl_chidao',$chidaold);
			return redirect(base_url().'thongtinvanban?id='.$mavaban);
		}
		
		if ($this->input->post('luuykien')) {
			$chidaold = array(
                'FK_iMaVBDi'        => $mavaban,
                'sNoiDung'        => nl2br($this->input->post('dexuat')),
                'FK_iMaCB'       => $this->_session['PK_iMaCB'],                
                'sThoiGian'   => date('Y-m-d H:i:s',time()),
            );
			
			$this->Mdanhmuc->themDuLieu('tbl_ykien',$chidaold);
			return redirect(base_url().'thongtinvanban?id='.$mavaban);
		}
		
		if($this->input->post('xoadexuat')){
            $this->Mdanhmuc->xoaDuLieu('id',$this->input->post('xoadexuat'),'tbl_ykien');
			return redirect(base_url().'thongtinvanban?id='.$mavaban);
        }
		if($this->input->post('xoachidao')){
            $this->Mdanhmuc->xoaDuLieu('id',$this->input->post('xoachidao'),'tbl_chidao');
			return redirect(base_url().'thongtinvanban?id='.$mavaban);
        }
		
		if($this->input->post('luuhoso')){
           $data['content'] = $this->luuhoso();
        }
		$data['dshoso'] = $this->Mdanhmuc->layDuLieu('FK_iMaCB',$this->_session['PK_iMaCB'],'tbl_hoso');
		$dshsvb = $this->Mdanhmuc->layhosovbdi($mavaban);
		$dshsvanban = array();
		foreach ($dshsvb as $key => $value) {
			array_push($dshsvanban, $value['FK_iMaHS']);
		}
		$data['dshosovb'] = $dshsvanban;
		
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vthongtinvanban';
		$this->load->view('layout_admin/layout',$temp);
	}
	
	public function luuhoso(){
		 $idDoc       = $this->input->post('luuhoso');
		 $vanban = $this->Mdanhmuc->layDuLieu('PK_iMaVBDi',$idDoc,'tbl_vanbandi');
		 $manghs = $this->input->post('manghs'.$idDoc);
		 $dshsvb = $this->Mdanhmuc->layhosovbdi($idDoc);
		$dshsvanban = array();
		foreach ($dshsvb as $key => $value) {
			array_push($dshsvanban, $value['FK_iMaHS']);
		}
		
			foreach ($manghs as $key => $value) {
				 if (in_array($value, $dshsvanban)) continue;
				 $data = array(
					'FK_iMaVBDi' => $idDoc,
					'sThoiGian' => date('Y-m-d H:i:s',time()),
					'FK_iMaCB' => $this->_session['PK_iMaCB'],
					'FK_iMaPB' => $this->_session['FK_iMaPhongHD'],
					'FK_iMaHS' => $value,
					'iSoVB' => $vanban[0]['iSoVBDi'],
					'sTenLVB' => $vanban[0]['iSoVBDi'].'/'.$vanban[0]['FK_iMaLVB'],
					'sKyHieu' => $vanban[0]['sKyHieu'],
					'sNgayNhap' => $vanban[0]['sNgayNhap'],
					'sMoTa' => $vanban[0]['sMoTa'],
					);
				 $this->Mdanhmuc->themDuLieu('tbl_hosovanban',$data);
			}
			foreach ($dshsvanban as $key => $value) {
				if(in_array($value,$manghs)) continue;
					//xoa ho so van ban
					$data['content'] = $this->Mdanhmuc->xoaHoSoVBDi($idDoc,$value);
					//return messagebox('Xóa THÀNH CÔNG','info');
			}		
		redirect(base_url().'thongtinvanban?id='.$idDoc);
	}

}

/* End of file Cthongtinvanban.php */
/* Location: ./application/controllers/vanbandi/Cthongtinvanban.php */