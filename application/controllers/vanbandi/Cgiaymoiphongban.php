<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cgiaymoiphongban extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->library('pagination');
	}
	public function index()
	{
		$phongban         = $this->_session['FK_iMaPhongHD'];
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dsduthao']     = $this->Mvanbandi->layPhongBanDuThao();
		$data['dsnguoinhap']  = $this->Mvanbandi->layVanThu();
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['nguoiky'] 	  = $this->Mvanbandi->layNguoiKy();
		$vanban             = $this->DSVanBan();
		$data['vanbandi']	= $vanban['info'];
		if(!empty($data['vanbandi'])){
			foreach ($data['vanbandi'] as $key => $value) {
				$duongdan = $this->Mvanbandi->layFileLast($value['PK_iMaVBDi']);
				if(!empty($duongdan))
				{
					$data['vanbandi'][$key]['sDuongDan'] = $duongdan['sDuongDan'];
				}else{
					$data['vanbandi'][$key]['sDuongDan'] = '';
				}
			}
		}
		$data['loaivanban'] = $vanban['loaivanban'];
		$data['noiduthao']  = $vanban['noiduthao'];
		$data['kyhieu']     = $vanban['kyhieu'];
		$data['tungay']  = $vanban['tungay'];
		$data['denngay']  = $vanban['denngay'];
		$data['trichyeu']   = $vanban['trichyeu'];
		$data['nguoikyvb']  = $vanban['nguoiky'];
		$data['nguoinhap']  = $vanban['nguoinhap'];
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Danh sách giấy mời của phòng';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandi/Vgiaymoiphongban';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBan() 
	{
		$loaivanban = _get('loaivanban');
		$noiduthao  = _get('noiduthao');
		$kyhieu     = _get('kyhieu');
		$tungay  = _get('tungay');
		$denngay  = _get('denngay');
		if(!empty($tungay))
		{
			$tungay = date_insert($tungay);
		}
		else{
			$tungay = '';
		}
		if(!empty($denngay))
		{
			$denngay = date_insert($denngay);
		}
		else{
			$denngay = '';
		}
		$trichyeu   = _get('trichyeu');
		$nguoiky    = _get('nguoiky');
		$nguoinhap  = _get('nguoinhap');
		$phongban   = $this->_session['FK_iMaPhongHD'];
		// lấy danh sách văn bản đang chờ số (Trạng thái:0 là chờ số, Trạng thái:>0 là đã được cấp số) loại văn bảng:10 là giấy mời: default 1 là không phải giấy mời
		$config['base_url']             = base_url().'giaymoiphongban?loaivanban='.$loaivanban.'&noiduthao='.$noiduthao.'&kyhieu='.$kyhieu.'&tungay='.$tungay.'&denngay='.$denngay.'&trichyeu='.$trichyeu.'&nguoiky='.$nguoiky.'&nguoinhap='.$nguoinhap;
		$config['total_rows']           = $this->Mvanbandi->demDSVBDi($loaivanban,$noiduthao,$kyhieu,$tungay,$denngay,$trichyeu,$nguoiky,$nguoinhap,$phongban,NULL,1,10);
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'giaymoiphongban');
      	}
		$data['loaivanban'] = $loaivanban;
		$data['noiduthao']  = $noiduthao;
		$data['kyhieu']     = $kyhieu;
		$data['tungay']  = $vanban['tungay'];
		$data['denngay']  = $vanban['denngay'];
		$data['trichyeu']   = $trichyeu;
		$data['nguoiky']    = $nguoiky;
		$data['nguoinhap']  = $nguoinhap;
		$data['info']       = $this->Mvanbandi->layDSGM($kyhieu,$tungay,$denngay,$trichyeu,$nguoiky,$nguoinhap,$phongban,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cgiaymoiphongban.php */
/* Location: ./application/controllers/vanbandi/Cgiaymoiphongban.php */