<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandenchicuc extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']    = 'Văn bản nhập mới';
		$temp['data']     = $data;
		$temp['template'] = 'vanbandenchicuc/Vvanbandenchicuc';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cvanbandenchicuc.php */
/* Location: ./application/controllers/vanbandenchicuc/Cvanbandenchicuc.php */