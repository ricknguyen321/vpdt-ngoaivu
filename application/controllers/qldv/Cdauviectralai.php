<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviectralai extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->Mketqua->layPhongBanDuThao();
        if(!empty($phongban))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $canbo = $this->MqldvDeatails->layDSCanBoPhong($phongbanHD);
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$lanhdao   = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		if(!empty($lanhdao))
		{
			foreach ($lanhdao as $val) {
				$lanhdao[$val['PK_iMaCB']]=$val['sHoTen'];
			}
		}
		$data['mangPB']		= $mangPB;
		$data['mangCB']		= $mangCB;
		$data['lanhdao']	= $lanhdao;
		$data['dulieu']		= $this->Mchuyennhan->layDauViecTraLai($taikhoan);
		// pr($data['dulieu']);
		$data['title']		= 'Đầu việc trưởng phòng trả lại';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vdauviectralai';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdauviectralai.php */
/* Location: ./application/controllers/qldv/Cdauviectralai.php */