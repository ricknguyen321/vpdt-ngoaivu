<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhgiadauviec extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
	}
	public function index()
	{	
		$taikhoan   = $this->_session['PK_iMaCB'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		switch ($quyen) {
			case 3:
				if($chucvu==6) // chức vụ chánh văn phòng
				{
					$trangthai =5; #TP
				}
				break;
			case 6: #TP
				if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
				{
					$trangthai =5;
				}
				break;
			case 7: #PTP
				$trangthai =6;
				break;
			default:
				redirect('dauviecdagiaiquyet');
				break;
		}
		if(_post('danhgia'))
		{
			$data['content'] = $this->danhgiaVB();
		}
		$vanbancon    = $this->MqldvDeatails->dauviecDaGiaiQuyet($taikhoan);
		$mangcon = array(0);
		if(!empty($vanbancon))
		{
			foreach ($vanbancon as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		$canbo            = $this->MqldvDeatails->dsCB();
		$mangCB           = array();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $value) {
				$mangCB[$value['PK_iMaCB']] = $value['sHoTen'];
			}
		}
		$data['mangCB']         = $mangCB;
		$data['vanbancon']      = $vanbancon;
		$data['vanbanong']      = $this->MqldvDeatails->layVanBanOng($mangcon);
		$data['title']			= 'Đầu việc đã hoàn thành';
		$temp['data']           = $data;
		$temp['template']       = 'qldv/Vdanhgiadauviec';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function danhgiaVB()
	{
		$mavanban = _post('mavanban');
		if(!empty($mavanban))
		{
			foreach ($mavanban as $key => $value) {
				$data[]=array(
					'qlvDetails_id'		=> $value,
					'noidung_danhgia'   => (_post('noidungdanhgia')[$value])?_post('noidungdanhgia')[$value]:'Hoàn thành công việc',
					'thoigian_danhgia'  => date('Y-m-d H:i:s'),
					'qlvDetails_active' => 9
					);
			}
			$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlv_details',$data,'qlvDetails_id');
			if($kiemtra>0)
			{
				return messagebox('Đánh giá hoàn thành công việc thành công','info');
			}
		}
		else{
			return messagebox('Bạn chưa chọn văn bản nào','danger');
		}
	}

}

/* End of file Cdanhgiadauviec.php */
/* Location: ./application/controllers/qldv/Cdanhgiadauviec.php */