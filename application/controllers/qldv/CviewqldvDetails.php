<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class CviewqldvDetails extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        $action = _post('action');
        if(!empty($action))
        {
            switch ($action) {
                case 'getLinhvucSub':
                    $this->getLinhvucSub();
                    break;
                default:
                    break;
            }
        }
        
        $qlv_id = $this->uri->segment(2);
        $qlvDetails_id = $this->uri->segment(3);

        if($qlvDetails_id >0){
            
        }else{
            $data['content'] = $this->themDuLieu($qlv_id);
        }
        $data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail('qlv_id',$qlv_id,'tbl_qlv_details');
        $data['qldvDetail1']    = $this->Mdanhmuc->getQldvDetail1('qlv_id',$qlv_id,'tbl_qlv_details');
        $data['thongtin']       = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlv');
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['file_dk']        = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlvfiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/VviewqldvDetails';
		$this->load->view('layout_admin/layout',$temp);
	}

    public function themDuLieu( $qlv_id){
        if(_post('luulai')){
            $data = array(
                "qlv_id"                => $qlv_id,
                "qlv_id_sub"            => 0,
                "qlvDetails_id_sub"     => 0,
                "coordinate_per"        => _post('coordinate_per'),
                "qlvDetails_date"       => date('Y-m-d H:i:s'),
                "qlvDetails_desc"       => _post('qlvDetails_desc'),
                "qlvDetails_limit_time" => date_insert(_post('qlvDetails_limit_time1')),
                "input_per"             => $this->_session['PK_iMaCB']
                 );
            $id_insert = $this->Mdanhmuc->themDuLieu2('tbl_qlv_details',$data);
 
            $tongdauviec = _post('tong_dv_chitiet');
            for($i=1;$i<=$tongdauviec;$i++){
                $j=$i-1;
                
                if(_post('qlvDetails_limit_time')[$j] ==""){
                    $qlvDetails_limit_time = date_insert(_post('qlvDetails_limit_time1'));
                }else{
                    $qlvDetails_limit_time = date_insert(_post('qlvDetails_limit_time')[$j]);
                }

                $dulieu[]= array(
                "qlv_id"                => $qlv_id,
                "qlv_id_sub"            => $id_insert,
                "qlvDetails_id_sub"     => $id_insert,
                "coordinate_per"        => _post('coordinate_per'),
                "qlvDetails_date"       => date('Y-m-d H:i:s'),
                "qlvDetails_desc"       => _post('noidunghop_'.$i),
                "qlvDetails_limit_time" => $qlvDetails_limit_time,
                "department_id"         => _post('department_id')[$j],
                "main_department"       => _post('main_department')[$j],
                "input_per"             => $this->_session['PK_iMaCB'],
                "linhVuc_id"            => _post('linhVuc_id')[$j],
                "linhvuc_sub"           => _post('linhvuc_sub_'.$i)
                 );
            }
            $this->Mdanhmuc->themNhieuDuLieu('tbl_qlv_details',$dulieu);
        }
    }


    public function getLinhvucSub()
    {
        $ma= _post('ma');
        $lv     = $this->Mdanhmuc->layDuLieu('id_parent',$ma,'tbl_linhvuc_qldv');
        echo json_encode($lv);exit();
    }
	

}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */