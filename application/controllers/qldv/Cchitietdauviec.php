<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchitietdauviec extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_phoihop');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{	
		$taikhoan   = $this->_session['PK_iMaCB'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$ma               = $this->uri->segment(2);
		$data['dexuatrahan'] = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$ma,'tbl_dexuatrahan');
		if(_post('hoanthanh'))
		{
			$data['content'] = $this->hoanthanhVB($ma);
		}
		$data['dschuyennhan'] = $this->Mchuyennhan->layQuyTrinhXuLy($ma,1,1);
		$canbo            = $this->MqldvDeatails->dsCB();
		$mangCB           = array();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $value) {
				$mangCB[$value['PK_iMaCB']] = $value['sHoTen'];
			}
		}
		$phongban   = $this->Mketqua->layPhongBanDuThao();
        if(!empty($phongban))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $data['mangPB']         = $mangPB;
		$data['mangCB']         = $mangCB;
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
		$thongtin               = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$ma,'tbl_qlv_details');
		$qlv_id                 = $thongtin[0]['qlv_id'];
		$qlv_id_sub             = $thongtin[0]['qlv_id_sub'];
		$data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail('qlvDetails_id',$qlv_id_sub,'tbl_qlv_details');
		$data['qldvDetail1']    = $thongtin;
		$data['thongtin']       = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlv');
		$data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['file_dk']        = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlvfiles');
		$data['ketquachinh']	= $this->MqldvDeatails->layKetQuaGiaiQuyet($ma,0);
		$data['ketquaphoihop']	= $this->MqldvDeatails->layKetQuaGiaiQuyet($ma,1);
		$data['title']          = 'Chi tiết đầu việc';
		$kiemtraphong= $this->Mchuyennhan->laythogntinphong($ma,$taikhoan);
		if(!empty($kiemtraphong))
		{
			if($kiemtraphong['CT_PH']==1)
			{
				$nguoicuoi = $this->Mchuyennhan->layNguoiCuoiCung($ma,$taikhoan,1,1);
			}
			else{
				$nguoicuoi = $this->Mchuyennhan->layNguoiCuoiCung($ma,$taikhoan,2,1);
			}
			// pr($nguoicuoi);
			if($nguoicuoi>0)
			{
				$data['anhien']     = '';
			}
			else
			{
				$data['anhien']     = 'hide';
			}
		}
		$temp['data']           = $data;
		$temp['template']       = 'qldv/Vchitietdauviec';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function hoanthanhVB($ma)
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if($quyen==8)
		{
			$trangthai = 1; // chuyên viên
		}
		elseif($quyen==7){
			$trangthai = 2; // phó phòng
		}
		else{
			$trangthai = 3; // trưởng phòng
		}
		$chucvu		= $this->_session['FK_iMaCV'];
		$nguoigui   = $this->Mchuyennhan->layNguoiGui($ma,$taikhoan);
		$name = clear($_FILES['file']['name']);
		$this->Mdanhmuc->setDuLieu('qlvDetails_id',$ma,'tbl_qlvfilesketqua','lancuoi',1);
		$data_them=array(
			'qlvDetails_id'		=> $ma,
			'qlvFile_path'		=> ($name)?'qlv_uploads_'.date('Y').'/'.time().'_'.clear($name):'',
			'qlvFile_date'		=> date('Y-m-d H:i:s'),
			'qlvFile_desc'		=> _post('noidunggiaiquyet'),
			'qlvFile_active'	=> ($name)?1:0,
			'user_id'			=> $this->_session['PK_iMaCB'],
			'department_id'		=> $this->_session['FK_iMaPhongHD'],
			'iTrangThai'		=> $trangthai,
			'phoihop'			=> 0,
			'lancuoi'			=> 1,
			'FK_iMaCB_Duyet'	=> ($nguoigui)?$nguoigui['FK_iMaCB_Gui']:0
			);
		$data_capnhap=array(
			'thoigian_hoanthanh' => date('Y-m-d')
			);
		$this->Mchuyennhan->capnhatTrangThaiNguoiChuyen($ma,$taikhoan);
		$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_qlvfilesketqua',$data_them);
		if($kiemtra>0)
		{
			$this->upload('qlv_uploads_'.date('Y'),$name,'file');
			$this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$ma,'tbl_qlv_details',$data_capnhap);
			return messagebox('Bạn đã hoàn thành đầu việc','info');
		}
	}
	public function upload($dir,$name,$filename)
	{
		if(is_dir($dir)==false){
			mkdir($dir);		// Create directory if it does not exist
		}
		$config['upload_path']   = $dir;
		$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
		$config['overwrite']     = true;	
		$config['file_name']     = time().'_'.clear($name);
		$this->load->library('upload');
		$this->upload->initialize($config);
		$this->upload->do_upload($filename);
	}

}

/* End of file Cchitietdauviec.php */
/* Location: ./application/controllers/qldv/Cchitietdauviec.php */