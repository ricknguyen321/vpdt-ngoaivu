<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbannguoiphoihop extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mqldv_phoihop');
        $this->load->model('qldv/MqldvDeatails');
	}
	public function index()
	{
		$taikhoan         = $this->_session['PK_iMaCB'];
		$vanbancon		  = $this->Mqldv_phoihop->layVanBanChau($taikhoan);
		$mangcon = array(0);
		if(!empty($vanbancon))
		{
			foreach ($vanbancon as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		$data['vanbancon']    = $vanbancon;
		$data['vanbanong']    = $this->MqldvDeatails->layVanBanOng($mangcon);
		$data['title']    = 'Văn bản người phối hợp';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/Vvanbannguoiphoihop';
		$this->load->view('layout_admin/layout',$temp);
	}


}

/* End of file Cvanbannguoiphoihop.php */
/* Location: ./application/controllers/qldv/Cvanbannguoiphoihop.php */