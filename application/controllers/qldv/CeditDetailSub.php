<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CeditDetailSub extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        $action = _post('action');
        if(!empty($action))
        {
            switch ($action) {
                case 'getLinhvucSub':
                    $this->getLinhvucSub();
                    break;
                default:
                    break;
            }
        }
        
        $id_detail = $this->uri->segment(2);
        if($id_detail >0){
            $data['content'] = $this->capnhatDuLieu($id_detail);
        }
        $data['qldvLinhvuc']    = $this->Mdanhmuc->getLinhvuc('tbl_linhvuc_qldv');
        foreach ($data['qldvLinhvuc'] as $key => $value) {
            $data['manglinhvuc'][$value['linhVuc_id']] = $value['linhVuc_name'];
        }
        $data['qldvLinhvucSub'] = $this->Mdanhmuc->getLinhvucSub('tbl_linhvuc_qldv');
        foreach ($data['qldvLinhvucSub'] as $key => $value) {
            $data['manglinhvucsub'][$value['linhVuc_id']] = $value['linhVuc_name'];
        }
		$data['qldvDetailSub'] = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$id_detail,'tbl_qlv_details');
        if(empty($data['qldvDetailSub']))
        {
            redirect('dsdauviecvanban');
        }
        else{
            $taikhoan = $this->_session['PK_iMaCB'];
            if($data['qldvDetailSub'][0]['input_per']!=$taikhoan){
                redirect('dsdauviecvanban');
            }
        }
		$data['sllanhdao'] = $data['qldvDetailSub'][0]['FK_iMaCB_LanhDao'];
		$data['linhvuccon']    = $this->Mdanhmuc->layDuLieu('id_parent',$data['qldvDetailSub'][0]['linhVuc_id'],'tbl_linhvuc_qldv');
		$data['qldvDetail']    = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$data['qldvDetailSub'][0]['qlv_id_sub'],'tbl_qlv_details');
		$data['lanhdao']       = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['main_depart']   = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/VeditDetailSub';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($id_detail)
	{
        $quyen       = $this->_session['iQuyenHan_DHNB'];
        $phongbanHD       = $this->_session['FK_iMaPhongHD'];
        $chucvu = $this->_session['FK_iMaCV']; 
        if($quyen==3 && $chucvu==5)// khác bộ phận tổng hợp
        {
            $truongphong = 0;
        }
        else{
            if($phongbanHD==11)
            {
                $truongphong = 520; // chánh văn phòng
            }
            else{
                $laytruongphong = $this->Mketqua->layTruongPhong($phongbanHD);
                $truongphong    = $laytruongphong['PK_iMaCB'];
            }
        }
		$phongban     = $this->Mketqua->layPhongBanDuThao();
        $mangPB=array();
        if(!empty($phongban ))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
		if(_post('luulai'))
		{
			$phoihop = _post('department_id');
			if(!empty($phoihop))
			{
				$phoihop = implode(',', $phoihop);
			}
			else{
				$phoihop = '';
			}
			$phongchutri  =_post('main_department');
			$textchutri   = 'Chuyển phong '.$mangPB[$phongchutri].' phụ trách giải quyết';
			$phongphoihop = _post('department_id');
			$mangphoihop  = array();
			$textphoihop  = '';
            if(!empty($phongphoihop))
            { 
                $so=0;
                foreach ($phongphoihop as $key => $value) {
                    $so++;
                    $mangphoihop[] = (($so>1)?', ':'').$mangPB[$value];
                    
                }
            }
            if(!empty($mangphoihop))
            {
                $textphoihop = implode('', $mangphoihop);
            }
            if(!empty($textphoihop))
            {
                $noidungchuyen = $textphoihop.' phối hợp';
            }
            else{
                $noidungchuyen = $textphoihop;
            }
            $thongtin_han = $this->MqldvDeatails->layHanThongKe($id_detail);
			$data = array(
				'qlvDetails_desc'			=> _post('noidunghop'),
				'main_department'			=> _post('main_department'),
				'qlvDetails_limit_time'		=> date_insert(_post('qlvDetails_limit_time')),
				'department_id'				=> $phoihop,
				'sGoiY_PB'	                => $noidungchuyen,
				'sTieuDe'					=> _post('tieude'),
				'sNoiDungPhanCong'			=> _post('noidungphancong'),
				'iTrangThaiPhanCong'		=> _post('trangthaiphancong'),
				'sCuTri'					=> _post('cutri'),
				'FK_iMaCB_LanhDao'			=> _post('lanhdao'),
				'han_thongke'				=> ($thongtin_han['han_trangthai']==0)?date_insert(_post('qlvDetails_limit_time')):$thongtin_han['han_thongke'],
                'nguoibitralai' =>  '',
                'truongphongduyet'       => $truongphong
			);
			$kiemtra  = $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$id_detail,'tbl_qlv_details',$data);
            if($quyen==3)
            {
                $mangchuyennhan=array(
                    'FK_iMaQLDV'    => $id_detail,
                    'FK_iMaCB_Gui'  => $this->_session['PK_iMaCB'],
                    'sNoiDungChuyen'=> '',
                    'FK_iMaCB_Nhan' => _post('lanhdao'),
                    'sThoiGian'     => date('Y-m-d H:i:s'),
                    'CT_PH'         => 0,
                    'sHanXuLy'      => (_post('qlvDetails_limit_time')[$i])?date_insert(_post('qlvDetails_limit_time')[$i]):'0000-00-00',
                    'iTrangThai'    => 1
                );
                $this->Mdanhmuc->themDuLieu('tbl_chuyennhan_qldv',$mangchuyennhan);
            }
			$thongtin = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$id_detail,'tbl_qlv_details');
			if($kiemtra>0)
			{
				redirect('viewldvDetails/'.$thongtin[0]['qlv_id']);
			}
		}
	}
	public function getLinhvucSub()
    {
        $ma = _post('ma');
        $lv = $this->Mdanhmuc->layDuLieu('id_parent',$ma,'tbl_linhvuc_qldv');
        echo json_encode($lv);exit();
    }
}

/* End of file CeditDetailSub.php */
/* Location: ./application/controllers/qldv/CeditDetailSub.php */
