<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CaddDetailSub extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        $action = _post('action');
        if(!empty($action))
        {
            switch ($action) {
                case 'getLinhvucSub':
                    $this->getLinhvucSub();
                    break;
                default:
                    break;
            }
        }
        
        $id_detail = $this->uri->segment(2);
        if($id_detail >0){
            $data['content'] = $this->themDuLieu($id_detail);
        }
		$data['qldvDetail']   = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$id_detail,'tbl_qlv_details');
        if(empty($data['qldvDetail']))
        {
            redirect('dsdauviecvanban');
        }
        else{
            $taikhoan = $this->_session['PK_iMaCB'];
            if($data['qldvDetail'][0]['input_per']!=$taikhoan){
                redirect('dsdauviecvanban');
            }
        }
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/VaddDetailSub';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function themDuLieu($id_detail)
	{
		$phongban     = $this->Mketqua->layPhongBanDuThao();
        $mangPB=array();
        if(!empty($phongban ))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $quyen      = $this->_session['iQuyenHan_DHNB'];
        $phongbanHD = $this->_session['FK_iMaPhongHD'];
        $chucvu     = $this->_session['FK_iMaCV']; 
        if($quyen==3 && $chucvu==5)// khác bộ phận tổng hợp
        {
            $truongphong = 0;
        }
        else{
            if($phongbanHD==11)
            {
                $truongphong = 520; // chánh văn phòng
            }
            else{
                $laytruongphong = $this->Mketqua->layTruongPhong($phongbanHD);
                $truongphong    = $laytruongphong['PK_iMaCB'];
            }
        }
		if(_post('luulai'))
		{
			$thongtin    = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$id_detail,'tbl_qlv_details');
	        $tongdauviec = _post('tong_dv_chitiet');
	        if($tongdauviec>0)
                {
                    for($i=0;$i<$tongdauviec;$i++){
                        $phongphoihop = _post('phongphoihop')[$i];
                        $mangphoihop = array();
                        $textphoihop = '';
                        $lanhdao     = _post('lanhdao')[$i];
                        if(!empty($phongphoihop))
                        { 
                            $so=0;
                            $maphoihop = explode(',', $phongphoihop);
                            foreach ($maphoihop as $key => $value) {
                                $so++;
                                $mangphoihop[] = (($so>1)?', ':'').$mangPB[$value];
                                
                            }
                        }
                        if(!empty($mangphoihop))
                        {
                            $textphoihop = implode('', $mangphoihop);
                        }
                        if(!empty($textphoihop))
                        {
                            $noidungchuyen = $textphoihop.' phối hợp';
                        }
                        else{
                            $noidungchuyen = $textphoihop;
                        }
                        $dulieu= array(
                            "qlv_id"                => $thongtin[0]['qlv_id'],
                            "qlv_id_sub"            => $id_detail,
                            "qlvDetails_id_sub"     => $id_detail,
                            "FK_iMaCB_LanhDao"      => $lanhdao,
                            "qlvDetails_date"       => date('Y-m-d H:i:s'),
                            "qlvDetails_desc"       => (_post('noidunghop')[$i]),
                            "qlvDetails_limit_time" => (_post('hanxuly')[$i])?date_insert(_post('hanxuly')[$i]):'0000-00-00',
                            "department_id"         => _post('phongphoihop')[$i],
                            "main_department"       => _post('phongchutri')[$i],
                            "input_per"             => $this->_session['PK_iMaCB'],
                            "linhVuc_id"            => $thongtin[0]['linhVuc_id'],
                            "linhvuc_sub"           => $thongtin[0]['linhvuc_sub'],
                            "sGoiY_PB"              => $noidungchuyen,
                            "han_thongke"           => (_post('hanxuly')[$i])?date_insert(_post('hanxuly')[$i]):'0000-00-00',
                            'sTieuDe'               => _post('tieudeviec')[$i],
                            'sNoiDungPhanCong'      => _post('noidungphancong')[$i],
                            'sCuTri'                => _post('cutri')[$i],
                            'iTrangThaiPhanCong'    => _post('trangthaiphancong')[$i],
                            'truongphongduyet'      => $truongphong
                        );
                        $qldv_id = $this->Mdanhmuc->themDuLieu2('tbl_qlv_details',$dulieu);
                        if($quyen==3)
                        {
                            $mangchuyennhan=array(
                                'FK_iMaQLDV'    => $qldv_id,
                                'FK_iMaCB_Gui'  => $this->_session['PK_iMaCB'],
                                'sNoiDungChuyen'=> '',
                                'FK_iMaCB_Nhan' => $lanhdao,
                                'sThoiGian'     => date('Y-m-d H:i:s'),
                                'CT_PH'         => 0,
                                'sHanXuLy'      => (_post('hanxuly')[$i])?date_insert(_post('hanxuly')[$i]):'0000-00-00',
                                'iTrangThai'    => 1
                            );
                            $this->Mdanhmuc->themDuLieu('tbl_chuyennhan_qldv',$mangchuyennhan);
                        }
                    }
                }
	        redirect('viewldvDetails/'.$thongtin[0]['qlv_id']);
		}
	}
	public function getLinhvucSub()
    {
        $ma = _post('ma');
        $lv = $this->Mdanhmuc->layDuLieu('id_parent',$ma,'tbl_linhvuc_qldv');
        echo json_encode($lv);exit();
    }

}

/* End of file CaddDetailSub.php */
/* Location: ./application/controllers/qldv/CaddDetailSub.php */