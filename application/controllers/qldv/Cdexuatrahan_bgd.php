<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdexuatrahan_bgd extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->Mketqua->layPhongBanDuThao();
        if(!empty($phongban))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $canbo = $this->Mchuyennhan->layCB();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$lanhdao	= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['phogiamdoc'] = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(5),'iQuyenHan_DHNB','tbl_canbo');
		$data['mangPB']		= $mangPB;
		$data['mangCB']		= $mangCB;
		$data['phongban']	= $phongban;
		$data['lanhdao']	= $lanhdao;

		if(_post('dongy'))
		{
			$data['content'] = $this->dongyHan();
		}
		if(_post('tuchoi'))
		{
			$data['content'] = $this->tuchoiHan();
		}
		$data['dulieu']		= $this->Mchuyennhan->layDX_Han_TP($taikhoan);
		if($data['dulieu'])
		{
			foreach ($data['dulieu'] as $key => $value) {
				$data['dulieu'][$key]['chuyennhan'] = $this->Mchuyennhan->layQuyTrinhXuLy($value['qlvDetails_id'],array(0,1),array(1));
			}
		}
		$data['title']		= 'Đề xuất gia hạn';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vdexuatrahan_bgd';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function tuchoiHan()
	{
		$madexuat		= _post('madexuat');
		$mavanban		= _post('tuchoi');
		$hanxudexuat	= date_insert(_post('handexuat'));
		$lydodexuat		= _post('lydodexuat');
		$taikhoan       = $this->_session['PK_iMaCB'];
		$nguoinhan      = $this->Mchuyennhan->layNguoiChuyen($mavanban,$taikhoan);
		$mangthem       = array(
			'FK_iMaVB'			=> $mavanban,
			'Fk_iMaCB_Chuyen'	=> $taikhoan,
			'FK_iMaCB_Nhan'		=> $nguoinhan['Fk_iMaCB_Chuyen'],
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sLyDo_Chuyen'		=> $lydodexuat,
			'sHanDeXuat'		=> $hanxudexuat,
			'iTrangThai'		=> 1
		);
		$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_dexuatrahan',$mangthem);
		if($kiemtra>0)
		{
			$this->Mchuyennhan->capnhat_TrangThai($madexuat,2);
			return messagebox('Từ chối hạn thành công','info');
		}
	}
	public function dongyHan()
	{
		$madexuat		= _post('madexuat');
		$mavanban		= _post('dongy');
		$hanxudexuat	= date_insert(_post('handexuat'));
		$lydodexuat		= _post('lydodexuat');
		$taikhoan       = $this->_session['PK_iMaCB'];
		$mangthem       = array(
			'FK_iMaVB'			=> $mavanban,
			'Fk_iMaCB_Chuyen'	=> $taikhoan,
			'FK_iMaCB_Nhan'		=> 0,
			'sThoiGian_Chuyen'	=> date('Y-m-d H:i:s'),
			'sLyDo_Chuyen'		=> $lydodexuat,
			'sHanDeXuat'		=> $hanxudexuat,
			'iTrangThai'		=> 3
		);
		$mangcapnhat = array(
			'han_thongke' => $hanxudexuat
		);
		$mangcapnhathai=array(
			'sHanXuLy'    => $hanxudexuat
		);
		$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_dexuatrahan',$mangthem);
		if($kiemtra>0)
		{
			$this->Mdanhmuc->capnhatDuLieu('FK_iMaQLDV',$mavanban,'tbl_chuyennhan_qldv',$mangcapnhathai);
			$this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$mavanban,'tbl_qlv_details',$mangcapnhat);
			$this->Mchuyennhan->capnhat_TrangThai($madexuat,3);
			return messagebox('Đề xuất hạn thành công','info');
		}
	}

}

/* End of file Cdexuatrahan_bgd.php */
/* Location: ./application/controllers/qldv/Cdexuatrahan_bgd.php */