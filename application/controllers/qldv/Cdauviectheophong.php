<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviectheophong extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('qldv/Mqldv_phongban');
		$this->load->model('qldv/Mketqua');
	}
	public function index()
	{
		$data['title']    = 'Danh sách đầu việc theo phòng';
		$data['phongban'] = $this->Mketqua->layPhongBanDuThao();
		$mangPB= array();
		foreach ($data['phongban'] as $key => $value) {
			$mangPB[]=$value['PK_iMaPB'];
		}
		$tongdauviec       = $this->Mqldv_phongban->thongkePhongBan($mangPB);
		$thuchien_dunghan  = $this->Mqldv_phongban->layDauViecThongKe($mangPB,1,1,NULL);
		$thuchien_quahan   = $this->Mqldv_phongban->layDauViecThongKe($mangPB,1,NULL,1);
		$hoanthanh_dunghan = $this->Mqldv_phongban->layDauViecThongKe($mangPB,2,1,NULL);
		$hoanthanh_quahan  = $this->Mqldv_phongban->layDauViecThongKe($mangPB,2,NULL,1);
		$mang_tongdauviec       = array();
		$mang_thuchien_dunghan  = array();
		$mang_thuchien_quahan   = array();
		$mang_hoanthanh_dunghan = array();
		$mang_hoanthanh_quahan  = array();
		if(!empty($tongdauviec))
		{
			foreach ($tongdauviec as $key => $value) {
				$mang_tongdauviec[$value['main_department']] = $value['tong'];
			}
		}
		if(!empty($thuchien_dunghan))
		{
			$data['thuchien_dunghan'] = 0;
			foreach ($thuchien_dunghan as $key => $value) {
				$mang_thuchien_dunghan[$value['main_department']] = $value['tong'];
				$data['thuchien_dunghan'] = $data['thuchien_dunghan'] + $value['tong'];
			}
		}
		if(!empty($thuchien_quahan))
		{
			$data['thuchien_quahan'] = 0;
			foreach ($thuchien_quahan as $key => $value) {
				$mang_thuchien_quahan[$value['main_department']] = $value['tong'];
				$data['thuchien_quahan'] = $data['thuchien_quahan'] + $value['tong'];
			}
		}
		if(!empty($hoanthanh_dunghan))
		{
			$data['hoanthanh_dunghan'] = 0;
			foreach ($hoanthanh_dunghan as $key => $value) {
				$mang_hoanthanh_dunghan[$value['main_department']] = $value['tong'];
				$data['hoanthanh_dunghan'] = $data['hoanthanh_dunghan'] + $value['tong'];
			}
		}
		if(!empty($thuchien_quahan))
		{
			$data['thuchien_quahan'] = 0;
			foreach ($thuchien_quahan as $key => $value) {
				$mang_hoanthanh_quahan[$value['main_department']] = $value['tong'];
				$data['thuchien_quahan'] = $data['thuchien_quahan'] + $value['tong'];
			}
		}
		$data['tongdauviec']            = $this->Mqldv_phongban->tongDauViec();
		$data['mang_tongdauviec']       = $mang_tongdauviec;
		$data['mang_thuchien_dunghan']  = $mang_thuchien_dunghan;
		$data['mang_thuchien_quahan']   = $mang_thuchien_quahan;
		$data['mang_hoanthanh_dunghan'] = $mang_hoanthanh_dunghan;
		$data['mang_hoanthanh_quahan']  = $mang_hoanthanh_quahan;
		$temp['data']                   = $data;
		$temp['template']               = 'qldv/Vdauviectheophong';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cdauviectheophong.php */
/* Location: ./application/controllers/qldv/Cdauviectheophong.php */