<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cteptintailieu extends MY_Controller {

	protected $_thongtin;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('qldv/Mketqua');
	}
	public function index()
	{
		$quyen = $this->_session['iQuyenHan_DHNB'];
		if($quyen!=3)
		{
			redirect('danhsachtailieu');
		}
		$mavanban         = _get('ma');
		$data['content'] = $this->themDuLieu($mavanban);
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu($mavanban);
		}
		$data['mavanban'] = $mavanban;
		$data['thongtin'] = $this->_thongtin;
		$data['dsfile']	  = $this->Mketqua->layFileDaDuyet($mavanban,4,NULL);
		$data['title']    = 'Tệp tin tài liệu';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/Vteptintailieu';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('qlvFile_path',$duongdan,'tbl_qlvfilesketqua');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
	}
	public function themDuLieu($mavanban)
	{
		if(_post('luudulieu'))
		{
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$this->Mdanhmuc->setDuLieu('qlvDetails_id',$mavanban,'tbl_qlvfilesketqua','lancuoi',1);
				$this->upload('qlv_uploads_'.date('Y'));
				$files = array();
				foreach ($name as $key => $value) {
					$files[] = array(
						'qlvDetails_id'	=> $mavanban,
						'qlvFile_path'	=> 'qlv_uploads_'.date('Y').'/'.time().clear($value),
						'qlvFile_date'	=> date('Y-m-d H:i:s',time()),
						'user_id'		=> $this->_session['PK_iMaCB'],
						'department_id'	=> $this->_session['FK_iMaPhongHD'],
						'iTrangThai'	=> 4,
						'lancuoi'		=> 2
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_qlvfilesketqua',$files);
				if($kiemtra>0)
				{
					return messagebox('Thêm tệp tin thành công','info');
				}
				else{
					return messagebox('Thêm tệp tin thất bại','danger');
				}
			}
		}
	}
	public function upload($dir)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = time().clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cteptintailieu.php */
/* Location: ./application/controllers/qldv/Cteptintailieu.php */