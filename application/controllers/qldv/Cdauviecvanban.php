<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviecvanban extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('qldv/MqldvDeatails');
	}
	public function index()
	{
		$date = date('Y-m-d');
		$new_date = strtotime ( '3 day' , strtotime ( $date ) ) ;
		$new_date = date ( 'Y-m-d' , $new_date );
		$data['title']    = 'Danh sách đầu việc văn bản';
		$data['dsvanban'] = $this->Mdanhmuc->sapxepDuLieu('qlv_id','tbl_qlv','desc');
		$mangVB=array();
		if(!empty($data['dsvanban'])){
			foreach ($data['dsvanban'] as $key => $value) {
				$mangVB[] = $value['qlv_id'];
			}
		}
		if(!empty($mangVB))
		{
			$tongVBChau         = $this->MqldvDeatails->layTongDauViecChau($mangVB);
			$danglam_tronghan   = $this->MqldvDeatails->layDauViec($mangVB,1,1,NULL,NULL);
			$danglam_quahan     = $this->MqldvDeatails->layDauViec($mangVB,1,NULL,1,NULL);
			$hoanthanh_tronghan = $this->MqldvDeatails->layDauViec($mangVB,2,1,NULL,NULL);
			$hoanthanh_quahan   = $this->MqldvDeatails->layDauViec($mangVB,2,NULL,1,NULL);
			$sapden_han         = $this->MqldvDeatails->layDauViec($mangVB,3,NULL,NULL,$new_date);
		}
		
		$mang_tongVBChau		 = array();
		$mang_danglam_tronghan   = array();
		$mang_danglam_quahan     = array();
		$mang_hoanthanh_tronghan = array();
		$mang_hoanthanh_quahan   = array();
		$mang_sapden_han         = array();

		if(!empty($tongVBChau))
		{
			$data['tong_tongVBChau']=0;
			foreach ($tongVBChau as $key => $value) {
				$mang_tongVBChau[$value['qlv_id']] = $value['tong'];
				$data['tong_tongVBChau']=$data['tong_tongVBChau'] + $value['tong'];
			}
		}
		if(!empty($danglam_tronghan))
		{
			$data['tong_danglam_tronghan']=0;
			foreach ($danglam_tronghan as $key => $value) {
				$mang_danglam_tronghan[$value['qlv_id']] = $value['tong'];
				$data['tong_danglam_tronghan']=$data['tong_danglam_tronghan'] + $value['tong'];
			}
		}
		if(!empty($danglam_quahan))
		{
			$data['tong_danglam_quahan']=0;
			foreach ($danglam_quahan as $key => $value) {
				$mang_danglam_quahan[$value['qlv_id']] = $value['tong'];
				$data['tong_danglam_quahan']=$data['tong_danglam_quahan'] + $value['tong'];
			}
		}

		if(!empty($hoanthanh_tronghan))
		{
			$data['tong_hoanthanh_tronghan']=0;
			foreach ($hoanthanh_tronghan as $key => $value) {
				$mang_hoanthanh_tronghan[$value['qlv_id']] = $value['tong'];
				$data['tong_hoanthanh_tronghan']=$data['tong_hoanthanh_tronghan'] + $value['tong'];
			}
		}

		if(!empty($hoanthanh_quahan))
		{
			$data['tong_hoanthanh_quahan']=0;
			foreach ($hoanthanh_quahan as $key => $value) {
				$mang_hoanthanh_quahan[$value['qlv_id']] = $value['tong'];
				$data['tong_hoanthanh_quahan']=$data['tong_hoanthanh_quahan'] + $value['tong'];
			}
		}

		if(!empty($sapden_han))
		{
			$data['tong_sapden_han']=0;
			foreach ($sapden_han as $key => $value) {
				$mang_sapden_han[$value['qlv_id']] = $value['tong'];
				$data['tong_sapden_han']=$data['tong_sapden_han'] + $value['tong'];
			}
		}
		$data['tongvanban']              = count($data['dsvanban']);
		$data['mang_tongVBChau']         = $mang_tongVBChau;
		$data['mang_danglam_tronghan']   = $mang_danglam_tronghan ;
		$data['mang_danglam_quahan']     = $mang_danglam_quahan;
		$data['mang_hoanthanh_tronghan'] = $mang_hoanthanh_tronghan;
		$data['mang_hoanthanh_quahan']   = $mang_hoanthanh_quahan;
		$data['mang_sapden_han']         = $mang_sapden_han;
		$temp['data']                    = $data;
		$temp['template']                = 'qldv/Vdauviecvanban';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cdauviecvanban.php */
/* Location: ./application/controllers/qldv/Cdauviecvanban.php */