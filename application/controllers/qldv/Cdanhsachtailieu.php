<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachtailieu extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('tailieuphucvuhop/Mtailieuphucvuhop');
        $this->load->library('pagination');
        $this->load->model('qldv/Mketqua');
    }
    public function index()
    {
    	$data['quyen']		= $this->_session['iQuyenHan_DHNB'];
    	$data['khoa']		= $this->Mketqua->layDL('sKhoa','tbl_qlv');
		$data['nhiemky']	= $this->Mketqua->layDL('sNhiemKy','tbl_qlv');
		$data['kyhop']		= $this->Mketqua->layDL('sKyHop','tbl_qlv');
		$data['loaivanban'] = ['Tài liệu','Kiến nghi'];
		$data['linhvuc']	= $this->Mketqua->layLinhVuc();
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		if(_post('luudulieu'))
		{
			$data['content'] = $this->themDuLieu($taikhoan);
		}
		if(_post('xoa'))
		{
			$ma = _post('xoa');
			$this->Mdanhmuc->xoaDuLieu('FK_iMaQLDV',$ma,'tbl_chuyennhan_qldv');
			$this->Mdanhmuc->xoaDuLieu('qlvDetails_id',$ma,'tbl_qlv_details');
			$this->Mdanhmuc->xoaDuLieu('qlvDetails_id',$ma,'tbl_qlvfilesketqua');
		}
		$action = _post('action');
		switch ($action) {
			case 'suatailieu':
				$this->layDuLieu();
				break;
			
			default:
				# code...
				break;
		}
		if(_post('suatailieu'))
		{
			$ma = _post('suatailieu');
			$mang_capnhat=array(
				'qlvDetails_desc'	=> _post('noidung'),
				'sTieuDe'			=> _post('tailieu')
			);
			$this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$ma,'tbl_qlv_details',$mang_capnhat);
		}
		if(_post('capnhatthutu'))
		{
			$ma		=  _post('madauviec');
			$thutu	= _post('stt');
			if(!empty($ma))
			{
				foreach ($ma as $key => $value) {
					$data_capnhat[] =array(
						'qlvDetails_id'	=> $value,
						'sapxep'		=> $thutu[$key]
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatNhieuDuLieu('tbl_qlv_details',$data_capnhat,'qlvDetails_id');
				if($kiemtra>0)
				{
					$data['content'] = messagebox('Sắp xếp thứ tự thành công!','info');
				}
			}
		}
		$dulieu				= $this->layDSTaiLieu();
		$data['dulieu']		= $dulieu['dulieu'];
		$data['g_khoa']		= $dulieu['g_khoa'];
		$data['g_nhiemky']	= $dulieu['g_nhiemky'];
		$data['g_kyhop']	= $dulieu['g_kyhop'];
		$data['g_tieude']	= $dulieu['g_tieude'];
		$data['g_noidung']	= $dulieu['g_noidung'];
		$data['g_linhvuc']	= $dulieu['g_linhvuc'];
		$data['g_lanhdao']	= $dulieu['g_lanhdao'];
		$data['g_phongban']	= $dulieu['g_phongban'];
		$data['phantrang']	= $dulieu['phantrang'];
		if(!empty($data['dulieu']))
		{
			foreach ($data['dulieu'] as $key => $value) {
				$data['dulieu'][$key]['dsfile'] = $this->Mketqua->layFileDaDuyet($value['qlvDetails_id'],4,2);
			}
		}
		$data['loaitailieu']	= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaitailieu');
		$data['lanhdao']		= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($data['lanhdao'] as $key => $value) {
			$data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$data['phongban']		= $this->Mtailieuphucvuhop->layPhongBanDuThao();
		foreach ($data['phongban'] as $key => $value) {
			$data['mangphong'][$value['PK_iMaPB']] = $value['sTenPB'];
		}
		$data['title']			= 'Danh sách tài liệu phục vụ kỳ họp';
		$temp['data']			= $data;
		$temp['template']		= 'qldv/Vdanhsachtailieu';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function layDuLieu()
    {
    	$ma = _post('ma');
    	$dulieu = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$ma,'tbl_qlv_details');
    	echo json_encode($dulieu);
    	exit();
    }
    public function layDSTaiLieu() 
	{
		$data['g_khoa']		= _get('khoa');
		$data['g_nhiemky']	= _get('nhiemky');
		$data['g_kyhop']	= _get('kyhop');
		$data['g_tieude']	= _get('tieude');
		$data['g_noidung']	= _get('noidung');
		$data['g_linhvuc']	= _get('linhvuc');
		$data['g_lanhdao']	= _get('lanhdao');
		$data['g_phongban']	= _get('phongban');

		$config['base_url']             = base_url().'danhsachtailieu?khoa='.$data['g_khoa'].'&nhiemky='.$data['g_nhiemky'].'&kyhop='.$data['g_kyhop'].'&tieude='.$data['g_tieude'].'&noidung='.$data['g_noidung'].'&linhvuc='.$data['g_linhvuc'].'&lanhdao='.$data['g_lanhdao'].'&phongban='.$data['g_phongban'];
		$config['total_rows']           = $this->Mketqua->demTaiLieu(1,$data['g_nhiemky'],$data['g_khoa'],$data['g_kyhop'],$data['g_tieude'],$data['g_noidung'],$data['g_linhvuc'],$data['g_lanhdao'],$data['g_phongban'],NULL);
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'danhsachtailieu');
      	}
		$data['dulieu']		= $this->Mketqua->layTaiLieu(1,$data['g_nhiemky'],$data['g_khoa'],$data['g_kyhop'],$data['g_tieude'],$data['g_noidung'],$data['g_linhvuc'],$data['g_lanhdao'],$data['g_phongban'],NULL,$config['per_page'],$data['page']);
		$data['phantrang']	= $this->pagination->create_links();
        return $data;
	}


}

/* End of file Cdanhsachtailieu.php */
/* Location: ./application/controllers/qldv/Cdanhsachtailieu.php */