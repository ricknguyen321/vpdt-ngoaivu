<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachquahanphong extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
    }
    public function index()
    {
		$data['quyen']		= $this->_session['iQuyenHan_DHNB'];
		$dulieu				= $this->layDSTaiLieu();
		$data['dulieu']		= $dulieu['dulieu'];
		$data['phantrang']	= $dulieu['phantrang'];
		$canbo = $this->Mchuyennhan->layCB();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$mangcon = array(0);
		if(!empty($data['dulieu']))
		{
			foreach ($data['dulieu'] as $key => $value) {
				$mangcon[] = $value['qlv_id'];
				$data['dulieu'][$key]['dsfile'] = $this->Mketqua->layFileDaDuyetPhong($value['qlvDetails_id'],array(3,4),2);
				$data['dulieu'][$key]['chuyennhan'] = $this->Mchuyennhan->layQuyTrinhXuLy($value['qlvDetails_id'],array(0,1),array(1));
			}
		}
		$data['mangCB'] = $mangCB;
		$data['dulieuOng']  = $this->Mchuyennhan->layDauViecOng($mangcon);
		$data['loaitailieu']	= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaitailieu');
		$data['title']			= 'Danh sách đầu việc quá hạn';
		$temp['data']			= $data;
		$temp['template']		= 'qldv/Vdanhsachquahanphong';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function layDSTaiLieu() 
	{
		$phongban = $this->_session['FK_iMaPhongHD'];
		$config['base_url']             = base_url().'danhsachquahanphong';
		$config['total_rows']           = $this->Mketqua->demViecQuaHan($phongban);
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'danhsachquahanphong');
      	}
		$data['dulieu']		= $this->Mketqua->layViecQuaHan($phongban,$config['per_page'],$data['page']);
		$data['phantrang']	= $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdanhsachquahanphong.php */
/* Location: ./application/controllers/qldv/Cdanhsachquahanphong.php */