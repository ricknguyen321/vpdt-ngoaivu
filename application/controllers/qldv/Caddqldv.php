<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Caddqldv extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('qldv/MqldvDeatails');
	}
	public function index()
	{
		$action = _post('action');
		switch ($action) {
			case 'click_getTTVBDen':
				$this->click_getTTVBDen();
				break;
			case 'getTTVBDen':
				$this->getTTVBDen();
				break;
			default:
				# code...
				break;
		}
		$ma = $this->uri->segment(2);
		if(!empty($ma))
		{
			$this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['thongtin']	  = $this->_thongtin;
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		$data['title']        = 'Nhập mới quản lý việc';
		$temp['data']         = $data;
		$temp['template']     = 'qldv/Vaddqldv1';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function click_getTTVBDen()
	{
		$mavanban = _post('mavanban');
		$data= $this->MqldvDeatails->layThongTinVBDen_Click($mavanban);
		if(!empty($data))
		{
			echo json_encode($data); exit();
		}
		else{
			$data='';
			echo json_encode($data); exit();
		}
	}
	public function getTTVBDen()
	{
		$kyhieu = _post('kyhieu');
		$data= $this->MqldvDeatails->layThongTinVBDen($kyhieu);
		if(!empty($data))
		{
			echo json_encode($data); exit();
		}
		else{
			$data='';
			echo json_encode($data); exit();
		}

	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('qlv_id',$ma,'tbl_qlv');
		$taikhoan = $this->_session['PK_iMaCB'];
		if($this->_thongtin[0]['input_per']!=$taikhoan){
			redirect('dsdauviecvanban');
		}
		if(_post('luudulieu'))
		{
			$data=array(
				'qlv_loai'			=> _post('qlv_loai'),
				'qlv_code'			=> _post('qlv_code'),
				'qlv_date'			=> date_insert(_post('qlv_date')),
				'qlv_ld_chutri'		=> _post('qlv_ld_chutri'),
				'qlv_ld_ky'			=> _post('qlv_ld_ky'),
				'qlv_noidung'		=> _post('qlv_noidung'),
				'qlv_active'		=> _post('qlv_active'),
				'qlv_sort'			=> _post('qlv_sort'),
				'qlv_file'			=> _post('qlv_file'),
				'qlv_date_nhap'		=> date_insert(_post('qlv_date_nhap')),
				'input_per'			=> $this->_session['PK_iMaCB'],
				'thongbao_ketluan'	=> _post('thongbao_ketluan'),
				'loaivanban'		=> _post('tailieu'),
				'sKhoa'				=> _post('khoa'),
				'sNhiemKy'			=> _post('nhiemky'),
				'sKyHop'			=> _post('kyhop')
				);
	
			
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_uploads_'.date('Y'));
				$files = array();
				foreach ($name as $key => $value) {
					$files[] = array(
						'qlv_id'         => $id_insert,
						'qlvDetails_id'  => 0,
						'qlvFile_name'   => '',
						'qlvFile_path'   => 'doc_uploads_'.date('Y').'/'.clear($value),
						'qlvFile_date'   => date('Y-m-d'),
						'qlvFile_sort'   => 0,
						'qlvFile_active' => 1,
						'user_id'        => $this->_session['PK_iMaCB'],
						'department_id'  => $this->_session['FK_iMaPhongHD']
						);
				}
				$this->Mdanhmuc->themNhieuDuLieu('tbl_qlvfiles',$files);
			}

			$this->Mdanhmuc->capnhatDuLieu('qlv_id',$ma,'tbl_qlv',$data);
			redirect('viewldvDetails/'.$ma);
		}
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$name = $_FILES['files']['name'];
			if(!empty($name[0])) $qlv_file =1;
			else $qlv_file =2;
			$data=array(
				'qlv_loai'		=>	_post('qlv_loai'),
				'qlv_code'		=> 	_post('qlv_code'),
				'qlv_date'		=>	date_insert(_post('qlv_date')),
				'qlv_ld_chutri'	=> _post('qlv_ld_chutri'),
				'qlv_ld_ky'		=> 	_post('qlv_ld_ky'),
				'qlv_noidung'	=> _post('qlv_noidung'),
				'qlv_active'	=>  1,
				'qlv_sort'		=>	1,
				'qlv_file'		=>	$qlv_file,
				'qlv_date_nhap'	=>	date_insert(_post('qlv_date_nhap')),
				'input_per'		=>	$this->_session['PK_iMaCB'],
				'loaivanban'	=> _post('tailieu'),
				'sKhoa'			=> _post('khoa'),
				'sNhiemKy'		=> _post('nhiemky'),
				'sKyHop'		=> _post('kyhop')
				);
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_qlv',$data);
			if($id_insert>0)
			{
				$name = $_FILES['files']['name'];
				if(!empty($name[0]))
				{
					$this->upload('doc_uploads_'.date('Y'));
					$files = array();
					foreach ($name as $key => $value) {
						$files[] = array(
							'qlv_id'         => 	$id_insert,
							'qlvDetails_id'  => 0,
							'qlvFile_name'   => '',
							'qlvFile_path'   => 'doc_uploads_'.date('Y').'/'.clear($value),
							'qlvFile_date'   => date('Y-m-d'),
							'qlvFile_sort'   => 0,
							'qlvFile_active' => 1,
							'user_id'        => $this->_session['PK_iMaCB'],
							'department_id'  => $this->_session['FK_iMaPhongHD']
							);
					}
					$this->Mdanhmuc->themNhieuDuLieu('tbl_qlvfiles',$files);
				}
				//return messagebox('Thêm quản lý việc thành công','info');
				redirect('addqldvDetails/'.$id_insert);
			}
			else{
				return messagebox('Thêm quản lý việc thất bại','danger');
			}
		}
	}
	public function upload($dir)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */