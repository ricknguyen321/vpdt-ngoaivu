<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviecchoduyet extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->Mketqua->layPhongBanDuThao();
        if(!empty($phongban))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $canbo = $this->MqldvDeatails->layDSCanBoPhong($phongbanHD);
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$lanhdao   = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		if(!empty($lanhdao))
		{
			foreach ($lanhdao as $val) {
				$lanhdao[$val['PK_iMaCB']]=$val['sHoTen'];
			}
		}
		$data['mangPB']		= $mangPB;
		$data['mangCB']		= $mangCB;
		$data['lanhdao']	= $lanhdao;
		if(_post('tralai'))
		{
			$mavanban = _post('tralai');
			$mang = array(
				'truongphongduyet'	=> '',
				'nguoibitralai'		=> _post($mavanban),
				'noidungtralai'		=> _post('noidungtralai'.$mavanban)
			);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$mavanban,'tbl_qlv_details',$mang);
			if($kiemtra>0)
			{
				$data['content'] = messagebox('Trả lại thành công!','info');
			}
		}
		if(_post('duyet'))
		{
			$hanvanban	= _post('hanvanban');
			$lanhdao	= _post('lanhdao');
			$mavanban	= _post('mavanban');
			if(!empty($mavanban))
			{
				foreach ($mavanban as $key => $value) {
					$mangchuyennhan[] = array(
						'FK_iMaQLDV'		=> $value,
						'FK_iMaCB_Gui'		=> $taikhoan,
						'sNoiDungChuyen'	=> '',
						'FK_iMaCB_Nhan'		=> $lanhdao[$value],
						'sThoiGian'			=> date('Y-m-d H:i:s'),
						'CT_PH'				=> 0,
						'sHanXuLy'			=> $hanvanban[$value],
						'FK_iMaPB'			=> 0,
						'iTrangThai'		=> 1
					);
					$this->Mdanhmuc->setDuLieu('qlvDetails_id',$value,'tbl_qlv_details','truongphongduyet',0);
				}
				$kiemtra = $this->Mdanhmuc->themnhieuDuLieu('tbl_chuyennhan_qldv',$mangchuyennhan);
				if($kiemtra>0)
				{
					$data['content'] = messagebox('Duyệt văn bản thành công','info');
				}
			}
		}
		$data['dulieu']		= $this->Mchuyennhan->layDauViecChoDuyet($taikhoan);
		// pr($data['dulieu']);
		$data['title']		= 'Đầu việc chờ duyệt';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vdauviecchoduyet';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdauviecchoduyet.php */
/* Location: ./application/controllers/qldv/Cdauviecchoduyet.php */