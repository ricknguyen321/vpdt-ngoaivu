<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsdauviecchogiaiquyet extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_chuyenlai');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		switch ($quyen) {
			case 3:
				if($this->_session['FK_iMaCV']==6) // chức vụ chánh văn phòng
				{
					$trangthai =5; #TP
					$data['dsPTP'] = $this->MqldvDeatails->layPTPorCV($phongbanHD,7); // quyền phó trưởng phòng
					$data['dsCV']  = $this->MqldvDeatails->layPTPorCV($phongbanHD,8); // quyền chuyên viên
				}
				else{
					$trangthai =0;
				}
				break;
			case 4: #GD
				$trangthai =1;
				break;
			case 5: #PGD
				$trangthai =2;
				break;
			case 6: #TP
				if($chucvu==15) // chức vụ chi cục trưởng
				{
					$trangthai =3; #CCT
					$data['dsCCP'] = $this->MqldvDeatails->layCCP($phongbanHD);
				}
				if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
				{
					$trangthai =5;
					$data['dsPTP'] = $this->MqldvDeatails->layPTPorCV($phongbanHD,7); // quyền phó trưởng phòng
					$data['dsCV']  = $this->MqldvDeatails->layPTPorCV($phongbanHD,8); // quyền chuyên viên
				}
				break;
			case 7: #PTP
				$trangthai =6;
				$data['dsCV']  = $this->MqldvDeatails->layPTPorCV($phongbanHD,8);
				break;
			case 8: #CV
				$trangthai =7;
				break;
			case 10:#CCP
				$trangthai =4; 
				break;
			case 11:#TP bên chi cục
				$trangthai =5; 
				$data['dsPTP'] = $this->MqldvDeatails->layPTPorCV_CC($phongban,7); // quyền phó trưởng phòng
				$data['dsCV']  = $this->MqldvDeatails->layPTPorCV_CC($phongban,8); // quyền chuyên viên
				break;
			default:
				$trangthai =0;
				break;
		}
		if(_post('duyet'))
		{
			$data['content'] = $this->duyetVanBan($quyen);
		}
		if(_post('guidi'))
		{
			$ma = _post('guidi');
			$nguoinhanlai = $this->Mqldv_chuyenlai->layNguoiChuyen($ma);
			switch ($quyen) {
				case 3:
					if($chucvu==6) // chức vụ chánh văn phòng
					{
						if(!empty($nguoinhanlai['process_per']))
						{
							$nguoinhan        = $nguoinhanlai['process_per'];
							$trangthai_active = 2;
						}
						else{
							$trangthai_active = 1;
							$nguoinhan        = $nguoinhanlai['coordinate_per'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					}
					break;
				case 6: #TP
					if($chucvu==15) // chức vụ chi cục trưởng
					{
						if(!empty($nguoinhanlai['process_per']))
						{
							$nguoinhan        = $nguoinhanlai['process_per'];
							$trangthai_active = 2;
						}
						else{
							$trangthai_active = 1;
							$nguoinhan        = $nguoinhanlai['coordinate_per'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_chicuctruong'          => 0,
							'date_chicuctruong'          => NULL,
							'text_chicuctruong'          => NULL,
							'user_chicucpho'             => 0,
							'date_chicucpho'             => NULL,
							'text_chicucpho'             => NULL,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'Han_CCT'                    => NULL,
							'Han_CCP'                     => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					}
					if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
					{
						if(!empty($nguoinhanlai['process_per']))
						{
							$nguoinhan        = $nguoinhanlai['process_per'];
							$trangthai_active = 2;
						}
						else{
							$trangthai_active = 1;
							$nguoinhan        = $nguoinhanlai['coordinate_per'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					}
					break;
				case 10:#CCP
						$trangthai_active = 3;
						$nguoinhan        = $nguoinhanlai['user_truongphong'];
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_chicucpho'             => 0,
							'date_chicucpho'             => NULL,
							'text_chicucpho'             => NULL,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'Han-CCP'                    => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					break;
				case 11:#TP bên chi cục
					if(!empty($nguoinhanlai['user_chicucpho']))
						{
							$nguoinhan        = $nguoinhanlai['user_chicucpho'];
							$trangthai_active = 4;
						}
						else{
							$trangthai_active = 3;
							$nguoinhan        = $nguoinhanlai['user_chicuctruong'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					break;
				default:
					$trangthai =0;
				break;
			}

			$data_them=array(
				'qlvDetails_id'   => $ma,
				'qlv_id'          => $nguoinhanlai['qlv_id'],
				'qlv_id_sub'      => $nguoinhanlai['qlv_id_sub'],
				'FK_iMaCB_chuyen' => $this->_session['PK_iMaCB'],
				'thoigian_chuyen' => date('Y-m-d H:i:s'),
				'noidung_chuyen'  => _post('noidung'),
				'FK_iMaCB_nhan'   => $nguoinhan
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$ma,'tbl_qlv_details',$data);
			if($kiemtra>0)
			{
				$this->Mdanhmuc->themDuLieu('tbl_qlv_luuvet',$data_them);
				$data['content'] =  messagebox('Gửi văn bản trả lại thành công','info');
			}
			
		}
		if($trangthai==3||$trangthai==4||$trangthai==5)// kiểm tra phải trưởng phòng thì dk trả lại
		{
			$data['tralai'] ='co';
		}
		else{
			$data['tralai'] = '';
		}
		$vanbancon = $this->MqldvDeatails->layVanBanChau($taikhoan,$trangthai);
		$mangcon = array(0);
		if(!empty($vanbancon))
		{
			foreach ($vanbancon as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		$data['vanbancon']    = $vanbancon;
		$data['vanbanong']    = $this->MqldvDeatails->layVanBanOngDaXuLy($mangcon);
		$data['phogiamdoc']   = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(5),'iQuyenHan_DHNB','tbl_canbo');
		$mangPGD=array();
		if(!empty($data['phogiamdoc']))
		{
			foreach ($data['phogiamdoc'] as $val) {
				$mangPGD[$val['PK_iMaCB']]=$val['sHoTen'];
			}
		}
		$mangPB=array();
		// quyền giám đốc hoặc phó giám đốc
		if($quyen==4 || $quyen==5)
		{
			$data['phongban']     = $this->Mketqua->layPhongBanDuThao();
	        if(!empty($data['phongban']))
	        {
	            foreach ($data['phongban']  as $value) {
	                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
	            }
	        }
		}
		// quyền chuyên viên
		$mangCB=array();
		if($quyen==8)
		{
			$canbo = $this->MqldvDeatails->layDSCanBoPhong($phongbanHD);
			if(!empty($canbo))
			{
				foreach ($canbo as $key => $val) {
					$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
				}
			}
		}
		// chức vụ chi cục trưởng hoặc phó chi cục
		if($this->_session['FK_iMaCV']==16 || $this->_session['FK_iMaCV']==15)
		{
			$mangPB = array(
				['MaPhong'=>3,'TenPhong'=>'Phòng Một'],
				['MaPhong'=>4,'TenPhong'=>'Phòng Hai'],
				['MaPhong'=>5,'TenPhong'=>'Phòng Ba']);
		}
		$data['chucvu']		  = $chucvu;
        $data['quyen']		  = $quyen;
        $data['mangCB']	      = $mangCB;
		$data['mangPGD']	  = $mangPGD;
		$data['mangPB']	      = $mangPB;
		$data['title']        = 'Đầu việc chờ giải quyết';
		$temp['data']         = $data;
		$temp['template']     = 'qldv/Vdsdauviecchogiaiquyet';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function duyetVanBan($quyen)
	{
		$mavanbanchau = _post('mavanbanchau');
		if(!empty($mavanbanchau))
		{
			if($quyen==4) // quyền giám đốc
			{
				foreach ($mavanbanchau as $key => $value) {
					$phonbanchutri =_post('phongchutri')[$value];
					if($phonbanchutri==11)// kiểm tra phải văn phòng sở
					{
						$truongphong = $this->MqldvDeatails->layTruongPhongVPS();
					}
					elseif($phonbanchutri==12)// kiểm tra phải phòng chi cục không
					{
						$chicuctruong= $this->MqldvDeatails->layCCT();
					}
					else{
						$truongphong = $this->MqldvDeatails->layTruongPhong($phonbanchutri);
					}
					$thongtin_han = $this->MqldvDeatails->layHanThongKe($value);
					$maPGD = _post('phogiamdoc')[$value];
					$mangcapnhat[]=array(
						'qlvDetails_id'          => $value,
						'user_truongphong'       => ($maPGD)?0:(($phonbanchutri==12)?0:$truongphong['PK_iMaCB']),
						'user_chicuctruong'      => ($phonbanchutri==12)?$chicuctruong['PK_iMaCB']:0,
						'process_per'            => ($maPGD)?$maPGD:0,
						'date_coordinate_per'    => date('Y-m-d H:i:s'),
						'coordinate_per_text'    => ($maPGD)?_post('noidungchuyen_PGD')[$value]:_post('phongchutri_'.$value),
						'main_department'        => (_post('phongchutri')[$value])?_post('phongchutri')[$value]:0,
						'department_id'          => (isset(_post('phongphoihop')[$value]))?implode(",",_post('phongphoihop')[$value]):0,
						'han_GD'                 => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):NULL,
						'noidung_chuyenphongban' => _post('phongphoihop_'.$value),
						'qlvDetails_active'      => ($maPGD)?2:(($phonbanchutri==12)?3:5),
						'han_thongke'            => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):$thongtin_han['han_thongke'],
						'han_trangthai'          => (_post('hanxuly')[$value])?1:0,
						'noidung_tralai'         => '',
						'hoten_tralai'           => 0,
						'thoigian_tralai'        => '0000-00-00 00:00:00'
					);
					$mangthem[]=array(
						'qlvDetails_id'    => $value,
						'qlv_id'           => _post('vanbanong')[$value],
						'qlv_id_sub'       => _post('vanbancha')[$value],
						'FK_iMaCB_chuyen'  => $this->_session['PK_iMaCB'],
						'thoigian_chuyen'  => date('Y-m-d H:i:s'),
						'han_vanban'	   => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):'0000-00-00',
						'noidung_chuyen'   => ($maPGD)?_post('noidungchuyen_PGD')[$value]:_post('phongchutri_'.$value),
						'FK_iMaPB_CT'      => (_post('phongchutri')[$value])?_post('phongchutri')[$value]:0,
						'FK_iMaPB_PH'      => (isset(_post('phongphoihop')[$value]))?implode(",",_post('phongphoihop')[$value]):0,
						'sNoiDungPhong_PH' => _post('phongphoihop_'.$value),
						'FK_iMaCB_nhan'    => ($maPGD)?$maPGD:(($phonbanchutri==12)?$chicuctruong['PK_iMaCB']:$truongphong['PK_iMaCB'])
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlv_details',$mangcapnhat,'qlvDetails_id');
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themNhieuDuLieu('tbl_qlv_luuvet',$mangthem);
					return messagebox('Xử lý đầu việc thành công','info');
				}
			}
			if($quyen==5) // quyền phó giám đốc
			{
				foreach ($mavanbanchau as $key => $value) {
					$phonbanchutri =_post('phongchutri')[$value];
					if($phonbanchutri==11)// kiểm tra phải văn phòng sở
					{
						$truongphong = $this->MqldvDeatails->layTruongPhongVPS();
					}
					elseif($phonbanchutri==12)// kiểm tra phải phòng chi cục không
					{
						$chicuctruong= $this->MqldvDeatails->layCCT();
					}
					else{
						$truongphong = $this->MqldvDeatails->layTruongPhong(_post('phongchutri')[$value]);
					}
					$thongtin_han = $this->MqldvDeatails->layHanThongKe($value);
					$mangcapnhat[]=array(
						'qlvDetails_id'          => $value,
						'user_truongphong'       => ($phonbanchutri==12)?0:$truongphong['PK_iMaCB'],
						'user_chicuctruong'      => ($phonbanchutri==12)?$chicuctruong['PK_iMaCB']:0,
						'date_process_per'		 => date('Y-m-d H:i:s'),
						'process_per_text'       => _post('phongchutri_'.$value),
						'main_department'        => (_post('phongchutri')[$value])?_post('phongchutri')[$value]:0,
						'department_id'          => (isset(_post('phongphoihop')[$value]))?implode(",",_post('phongphoihop')[$value]):0,
						'han_PGD'                => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):NULL,
						'noidung_chuyenphongban' => _post('phongphoihop_'.$value),
						'qlvDetails_active'		 => ($phonbanchutri==12)?3:5,
						'han_thongke'			 => ($thongtin_han['han_trangthai']==0)?((_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):$thongtin_han['han_thongke']):$thongtin_han['han_thongke'],
						'han_trangthai'			 => 1,
						'noidung_tralai'         => '',
						'hoten_tralai'           => 0,
						'thoigian_tralai'        => '0000-00-00 00:00:00'
					);
					$mangthem[]=array(
						'qlvDetails_id'    => $value,
						'qlv_id'           => _post('vanbanong')[$value],
						'qlv_id_sub'       => _post('vanbancha')[$value],
						'FK_iMaCB_chuyen'  => $this->_session['PK_iMaCB'],
						'thoigian_chuyen'  => date('Y-m-d H:i:s'),
						'han_vanban'	   => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):'0000-00-00',
						'noidung_chuyen'   => _post('phongchutri_'.$value),
						'FK_iMaPB_CT'      => (_post('phongchutri')[$value])?_post('phongchutri')[$value]:0,
						'FK_iMaPB_PH'      => (isset(_post('phongphoihop')[$value]))?implode(",",_post('phongphoihop')[$value]):0,
						'sNoiDungPhong_PH' => _post('phongphoihop_'.$value),
						'FK_iMaCB_nhan'    => ($phonbanchutri==12)?$chicuctruong['PK_iMaCB']:$truongphong['PK_iMaCB']
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlv_details',$mangcapnhat,'qlvDetails_id');
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themNhieuDuLieu('tbl_qlv_luuvet',$mangthem);
					return messagebox('Xử lý đầu việc thành công','info');
				}
			}
			if($quyen==6 || $quyen==3 || $quyen==11) // quyền trưởng phòng
			{
				if($this->_session['FK_iMaCV']==15)
				{
					foreach ($mavanbanchau as $key => $value) {
						$chicucpho     =_post('chicucpho')[$value];
						$phongbanchutri =_post('phongchutriCC')[$value];
						if(!empty($phongbanchutri))
						{
							$truongphong = $this->MqldvDeatails->layTruongPhongCC($phongbanchutri);
						}
						else{
							$truongphong ='';
						}
						$mangcapnhat[]=array(
							'qlvDetails_id'          => $value,
							'user_chicucpho'         => ($chicucpho)?$chicucpho:0,
							'user_truongphong'		 => ($chicucpho)?0:(($truongphong)?$truongphong['PK_iMaCB']:0),
							'FK_iMaPB_CC_CT'         => ($phongbanchutri)?$phongbanchutri:0,
							'date_chicuctruong'      => date('Y-m-d H:i:s'),
							'text_chicuctruong'      => ($chicucpho)?_post('noidungchuyen_CCP'.$value):_post('noidungchuyen_CCCT'.$value),
							'text_chicucpho'         => ($phongbanchutri)?_post('noidungchuyen_CCCT'.$value):NULL,
							'FK_iMaPB_CC_PH'         => (isset(_post('phongphoihopCC')[$value]))?implode(",",_post('phongphoihopCC')[$value]):0,
							'Han_CCT'                => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):NULL,
							'noidung_phoihop_chicuc' => _post('noidungchuyen_CCPH'.$value),
							'qlvDetails_active'		 => ($chicucpho)?4:5,
							'noidung_tralai'         => '',
							'hoten_tralai'           => 0,
							'thoigian_tralai'        => '0000-00-00 00:00:00'
						);
						$mangthem[]=array(
							'qlvDetails_id'    => $value,
							'qlv_id'           => _post('vanbanong')[$value],
							'qlv_id_sub'       => _post('vanbancha')[$value],
							'FK_iMaCB_chuyen'  => $this->_session['PK_iMaCB'],
							'thoigian_chuyen'  => date('Y-m-d H:i:s'),
							'han_vanban'	   => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):'0000-00-00',
							'noidung_chuyen'   => ($chicucpho)?_post('noidungchuyen_CCP'.$value):_post('noidungchuyen_CCCT'.$value),
							'FK_iMaPB_CT'      => ($phongbanchutri)?$phongbanchutri:0,
							'FK_iMaPB_PH'      => (isset(_post('phongphoihopCC')[$value]))?implode(",",_post('phongphoihopCC')[$value]):0,
							'sNoiDungPhong_PH' => _post('noidungchuyen_CCPH'.$value),
							'FK_iMaCB_nhan'    => ($chicucpho)?$chicucpho:$truongphong['PK_iMaCB']
						);
					}
					$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlv_details',$mangcapnhat,'qlvDetails_id');
					if($kiemtra>0)
					{
						$this->Mdanhmuc->themNhieuDuLieu('tbl_qlv_luuvet',$mangthem);
						return messagebox('Xử lý đầu việc thành công','info');
					}
				}
				else{
					foreach ($mavanbanchau as $key => $value) {
						$maPTP = _post('photruongphong')[$value];
						$mangcapnhat[]=array(
							'qlvDetails_id'              => $value,
							'date_truongphong'           => date('Y-m-d H:i:s'),
							'user_photruongphong'        => ($maPTP)?$maPTP:0,
							'text_photruongphong '       => ($maPTP)?((_post('noidungchuyen_CV'.$value))?_post('noidungchuyen_CV'.$value):NULL):NULL,
							'text_truongphong'           => ($maPTP)?_post('noidungchuyen_PTP'.$value):_post('noidungchuyen_CV'.$value),
							'user_chuyenvien'            => (isset(_post('chuyenvien')[$value]))?_post('chuyenvien')[$value]:0,
							'han_TP'                     => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):NULL,
							'qlvDetails_active'          => ($maPTP)?6:7,
							'user_chuyenvien_phoihop'    => (isset(_post('chuyenvienPH')[$value]))?implode(",",_post('chuyenvienPH')[$value]):0,
							'noidung_chuyenvien_phoihop' => _post('noidungchuyen_CVPH'.$value),
							'user_danhgia'               => $this->_session['PK_iMaCB'],
							'noidung_tralai'             => '',
							'hoten_tralai'               => 0,
							'thoigian_tralai'            => '0000-00-00 00:00:00'
						);
						$mangthem[]=array(
							'qlvDetails_id'    => $value,
							'qlv_id'           => _post('vanbanong')[$value],
							'qlv_id_sub'       => _post('vanbancha')[$value],
							'FK_iMaCB_chuyen'  => $this->_session['PK_iMaCB'],
							'thoigian_chuyen'  => date('Y-m-d H:i:s'),
							'han_vanban'	   => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):'0000-00-00',
							'noidung_chuyen'   => ($maPTP)?_post('noidungchuyen_PTP'.$value):_post('noidungchuyen_CV'.$value),
							'FK_iMaCB_nhan'    => ($maPTP)?$maPTP:_post('chuyenvien')[$value]
						);
					}
					$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlv_details',$mangcapnhat,'qlvDetails_id');
					if($kiemtra>0)
					{
						$this->Mdanhmuc->themNhieuDuLieu('tbl_qlv_luuvet',$mangthem);
						return messagebox('Xử lý đầu việc thành công','info');
					}
				}
			}
			if($quyen==7) // quyền phó trưởng phòng
			{
				foreach ($mavanbanchau as $key => $value) {
					$maPTP = _post('photruongphong')[$value];
					$mangcapnhat[]=array(
						'qlvDetails_id'              => $value,
						'date_photruongphong'        => date('Y-m-d H:i:s'),
						'user_chuyenvien'            => _post('chuyenvien')[$value],
						'text_photruongphong'        => _post('noidungchuyen_CV'.$value),
						'han_PTP'                    => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):NULL,
						'qlvDetails_active'          => 7,
						'user_chuyenvien_phoihop'    => (_post('chuyenvienPH')[$value])?implode(",",_post('chuyenvienPH')[$value]):0,
						'noidung_chuyenvien_phoihop' => _post('noidungchuyen_CVPH'.$value),
						'user_danhgia'               => $this->_session['PK_iMaCB']
					);
					$mangthem[]=array(
						'qlvDetails_id'    => $value,
						'qlv_id'           => _post('vanbanong')[$value],
						'qlv_id_sub'       => _post('vanbancha')[$value],
						'FK_iMaCB_chuyen'  => $this->_session['PK_iMaCB'],
						'thoigian_chuyen'  => date('Y-m-d H:i:s'),
						'han_vanban'	   => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):'0000-00-00',
						'noidung_chuyen'   => _post('noidungchuyen_CV'.$value),
						'FK_iMaCB_nhan'    => _post('chuyenvien')[$value]
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlv_details',$mangcapnhat,'qlvDetails_id');
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themNhieuDuLieu('tbl_qlv_luuvet',$mangthem);
					return messagebox('Xử lý đầu việc thành công','info');
				}
			}
			if($quyen==10)#CCP
			{
				foreach ($mavanbanchau as $key => $value) {
					$phongbanchutri =_post('phongchutriCC')[$value];
					$truongphong = $this->MqldvDeatails->layTruongPhongCC($phongbanchutri);
					$mangcapnhat[]=array(
						'qlvDetails_id'          => $value,
						'user_truongphong'       => $truongphong['PK_iMaCB'],
						'FK_iMaPB_CC_CT'         => $phongbanchutri,
						'date_chicucpho'         => date('Y-m-d H:i:s'),
						'text_chicucpho'         => _post('noidungchuyen_CCCT'.$value),
						'FK_iMaPB_CC_PH'         => (isset(_post('phongphoihopCC')[$value]))?implode(",",_post('phongphoihopCC')[$value]):0,
						'Han_CCP'                => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):NULL,
						'noidung_phoihop_chicuc' => _post('noidungchuyen_CCPH'.$value),
						'qlvDetails_active'      => 5,
						'noidung_tralai'         => '',
						'hoten_tralai'           => 0,
						'thoigian_tralai'        => '0000-00-00 00:00:00'
					);
					$mangthem[]=array(
						'qlvDetails_id'    => $value,
						'qlv_id'           => _post('vanbanong')[$value],
						'qlv_id_sub'       => _post('vanbancha')[$value],
						'FK_iMaCB_chuyen'  => $this->_session['PK_iMaCB'],
						'thoigian_chuyen'  => date('Y-m-d H:i:s'),
						'han_vanban'	   => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):'0000-00-00',
						'noidung_chuyen'   => _post('noidungchuyen_CCCT'.$value),
						'FK_iMaPB_CT'      => $phongbanchutri,
						'FK_iMaPB_PH'      => (isset(_post('phongphoihopCC')[$value]))?implode(",",_post('phongphoihopCC')[$value]):0,
						'sNoiDungPhong_PH' => _post('noidungchuyen_CCPH'.$value),
						'FK_iMaCB_nhan'    => $truongphong['PK_iMaCB']
					);
				}
				$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlv_details',$mangcapnhat,'qlvDetails_id');
				if($kiemtra>0)
				{
					$this->Mdanhmuc->themNhieuDuLieu('tbl_qlv_luuvet',$mangthem);
					return messagebox('Xử lý đầu việc thành công','info');
				}
			}
			
		}
		else
		{
			return messagebox('Bạn chưa chọn văn bản nào','danger');
		}
	}

}

/* End of file Cdsdauviecchogiaiquyet.php */
/* Location: ./application/controllers/qldv/Cdsdauviecchogiaiquyet.php */