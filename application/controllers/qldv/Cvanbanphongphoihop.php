<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanphongphoihop extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_phoihop');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->_session['iQuyenDB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		switch ($quyen) {
			case 3:
				if($this->_session['FK_iMaCV']==6) // chức vụ chánh văn phòng quyền phó trưởng phòng
				{
					$trangthai =5; #TP
				}
				else{
					$trangthai =0; #TH
				}
				break;
			case 6: #TP
				if($chucvu==15) // chức vụ chi cục trưởng
				{
					$trangthai =5; #CCT
				}
				if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
				{
					$trangthai =5;
				}
				break;
			default:
				$trangthai =0;
				break;
		}
		if(_post('duyet'))
		{
			$data['content'] = $this->duyetVanBan();
		}
		$data['dsCV']  = $this->Mqldv_phoihop->layPTPandCV($phongbanHD,array(7,8)); // quyền chuyên viên
		$mang_CB=array();
		foreach ($data['dsCV'] as $key => $value) {
			$mang_CB[$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$data['mangCB'] = $mang_CB;
		$vanbancon = $this->Mqldv_phoihop->layVanBanChau_PB($phongbanHD);
		$mangcon = array(0);
		if(!empty($vanbancon))
		{
			foreach ($vanbancon as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		$data['truongphong']  = $this->MqldvDeatails->layTruongPhong($phongbanHD);
		$data['vanbancon']    = $vanbancon;
		$data['vanbanong']    = $this->MqldvDeatails->layVanBanOngDaXuLy($mangcon);
		$data['chucvu']		  = $chucvu;
        $data['quyen']		  = $quyen;
        $data['trangthai']    = $trangthai;
		$data['title']        = 'Văn bản phòng phối hợp';
		$temp['data']         = $data;
		$temp['template']     = 'qldv/Vvanbanphongphoihop';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function duyetVanBan()
	{
		$mavanbanchau = _post('mavanbanchau');
		if(!empty($mavanbanchau))
		{
			foreach ($mavanbanchau as $key => $value) {
				$mangthem[]=array(
					'qlvDetails_id' => $value,
					'user_chuyen'   => $this->_session['PK_iMaCB'],
					'FK_iMaPB'      => $this->_session['FK_iMaPhongHD'],
					'user_nhan'     => (_post('chuyenvienPH')[$value][0])?implode(",",_post('chuyenvienPH')[$value]):0,
					'thoigian_nhan' => date('Y-m-d H:i:s'),
					'noidung_nhan'  => _post('noidungchuyen_CVPH'.$value),
					'han_giaiquyet' => (_post('hanxuly')[$value])?date_insert(_post('hanxuly')[$value]):'0000-00-00',
				);
			}
			$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_qldv_phongphoihop',$mangthem);
			if($kiemtra>0)
			{
				return messagebox('Xử lý đầu việc thành công','info');
			}
		}
		else
		{
			return messagebox('Bạn chưa chọn văn bản nào','danger');
		}
	}

}

/* End of file Cvanbanphongphoihop.php */
/* Location: ./application/controllers/qldv/Cvanbanphongphoihop.php */