<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdadexuatrahan_cv extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->Mketqua->layPhongBanDuThao();
        if(!empty($phongban))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $canbo = $this->Mchuyennhan->layCB();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$lanhdao	= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['phogiamdoc'] = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(5),'iQuyenHan_DHNB','tbl_canbo');
		$data['mangPB']		= $mangPB;
		$data['mangCB']		= $mangCB;
		$data['phongban']	= $phongban;
		$data['lanhdao']	= $lanhdao;

		$data['dulieu']		= $this->Mchuyennhan->layDaDX_Han($taikhoan);
		// pr($data['dulieu']);
		if($data['dulieu'])
		{
			foreach ($data['dulieu'] as $key => $value) {
				$data['dulieu'][$key]['chuyennhan'] = $this->Mchuyennhan->layQuyTrinhXuLy($value['qlvDetails_id'],array(0,1),array(1));
			}
		}
		$data['title']		= 'Đã đề xuất gia hạn';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vdadexuatrahan_cv';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdadexuatrahan_cv.php */
/* Location: ./application/controllers/qldv/Cdadexuatrahan_cv.php */