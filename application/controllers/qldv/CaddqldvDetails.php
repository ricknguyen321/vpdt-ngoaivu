<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class CaddqldvDetails extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{
        $action = _post('action');
        if(!empty($action))
        {
            switch ($action) {
                case 'getLinhvucSub':
                    $this->getLinhvucSub();
                    break;
                default:
                    break;
            }
        }
        
        $qlv_id = $this->uri->segment(2);
        $qlvDetails_id = $this->uri->segment(3);

        if($qlvDetails_id >0){
            
        }else{
            $data['content'] = $this->themDuLieu($qlv_id);
        }
        $data['qldvDetail']     = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$qlvDetails_id,'tbl_qlv_details');
        $data['thongtin']       = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlv');
        if(empty($data['thongtin']))
        {
            redirect('dsdauviecvanban');
        }
        else{
            $taikhoan = $this->_session['PK_iMaCB'];
            if($data['thongtin'][0]['input_per']!=$taikhoan){
                redirect('dsdauviecvanban');
            }
        }
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['file_dk']        = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlvfiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/VaddqldvDetails';
		$this->load->view('layout_admin/layout',$temp);
	}

    public function themDuLieu($qlv_id){
        $phongban     = $this->Mketqua->layPhongBanDuThao();
        $mangPB=array();
        if(!empty($phongban ))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        if(_post('luulai')){
            $data = array(
                "qlv_id"                => $qlv_id,
                "qlv_id_sub"            => 0,
                "qlvDetails_id_sub"     => 0,
                "linhVuc_id"            => 0,
                "linhvuc_sub"           => 0,
                "qlvDetails_date"       => date('Y-m-d H:i:s'),
                "qlvDetails_desc"       => _post('qlvDetails_desc'),
                "qlvDetails_limit_time" => '0000-00-00',
                "input_per"             => $this->_session['PK_iMaCB'],
                "qlvDetails_active"     => 0,
                "han_thongke"           => '0000-00-00'
                );
            $id_insert   = $this->Mdanhmuc->themDuLieu2('tbl_qlv_details',$data);
            $tongdauviec = _post('tong_dv_chitiet');
            $quyen       = $this->_session['iQuyenHan_DHNB'];
            $phongbanHD       = $this->_session['FK_iMaPhongHD'];
            $chucvu = $this->_session['FK_iMaCV']; 
            if($quyen==3 && $chucvu==5)// khác bộ phận tổng hợp
            {
                $truongphong = 0;
            }
            else{
                if($phongbanHD==11)
                {
                    $truongphong = 520; // chánh văn phòng
                }
                else{
                    $laytruongphong = $this->Mketqua->layTruongPhong($phongbanHD);
                    $truongphong    = $laytruongphong['PK_iMaCB'];
                }
            }
            if($tongdauviec>0)
            {
                for($i=0;$i<$tongdauviec;$i++){
                    $phongphoihop = _post('phongphoihop')[$i];
                    $mangphoihop = array();
                    $textphoihop = '';
                    $lanhdao     = _post('lanhdao')[$i];
                    if(!empty($phongphoihop))
                    { 
                        $so=0;
                        $maphoihop = explode(',', $phongphoihop);
                        foreach ($maphoihop as $key => $value) {
                            $so++;
                            $mangphoihop[] = (($so>1)?', ':'').$mangPB[$value];
                            
                        }
                    }
                    if(!empty($mangphoihop))
                    {
                        $textphoihop = implode('', $mangphoihop);
                    }
                    if(!empty($textphoihop))
                    {
                        $noidungchuyen = $textphoihop.' phối hợp';
                    }
                    else{
                        $noidungchuyen = $textphoihop;
                    }
                    $dulieu= array(
                        "qlv_id"                 => $qlv_id,
                        "qlv_id_sub"             => $id_insert,
                        "qlvDetails_id_sub"      => $id_insert,
                        'FK_iMaCB_LanhDao'       => $lanhdao,
                        "qlvDetails_date"        => date('Y-m-d H:i:s'),
                        "qlvDetails_desc"        => (_post('noidunghop')[$i]),
                        "qlvDetails_limit_time"  => (_post('hanxuly')[$i])?date_insert(_post('hanxuly')[$i]):'0000-00-00',
                        "department_id"          => _post('phongphoihop')[$i],
                        "main_department"        => _post('phongchutri')[$i],
                        "input_per"              => $this->_session['PK_iMaCB'],
                        "linhVuc_id"             => 0,
                        "linhvuc_sub"            => 0,
                        "sGoiY_PB"               => $noidungchuyen,
                        "han_thongke"            => (_post('hanxuly')[$i])?date_insert(_post('hanxuly')[$i]):'0000-00-00',
                        'sTieuDe'                => _post('tieudeviec')[$i],
                        'sNoiDungPhanCong'       => _post('noidungphancong')[$i],
                        'sCuTri'                 => _post('cutri')[$i],
                        'iTrangThaiPhanCong'     => _post('trangthaiphancong')[$i],
                        'truongphongduyet'       => $truongphong
                    );
                    $qldv_id = $this->Mdanhmuc->themDuLieu2('tbl_qlv_details',$dulieu);
                    if($quyen==3)
                    {
                        $mangchuyennhan=array(
                            'FK_iMaQLDV'    => $qldv_id,
                            'FK_iMaCB_Gui'  => $this->_session['PK_iMaCB'],
                            'sNoiDungChuyen'=> '',
                            'FK_iMaCB_Nhan' => $lanhdao,
                            'sThoiGian'     => date('Y-m-d H:i:s'),
                            'CT_PH'         => 0,
                            'sHanXuLy'      => (_post('hanxuly')[$i])?date_insert(_post('hanxuly')[$i]):'0000-00-00',
                            'iTrangThai'    => 1
                        );
                        $this->Mdanhmuc->themDuLieu('tbl_chuyennhan_qldv',$mangchuyennhan);
                    }
                    
                }
                
            }
            redirect('viewldvDetails/'.$qlv_id);
        }
    }


    public function getLinhvucSub()
    {
        $ma = _post('ma');
        $lv = $this->Mdanhmuc->layDuLieu('id_parent',$ma,'tbl_linhvuc_qldv');
        echo json_encode($lv);exit();
    }

}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */