<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviecchoxulyphph_pp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mchuyennhan');
        $this->load->library('pagination');
	}
	public function index()
	{
		$data['khoa']		= $this->Mketqua->layDL('sKhoa','tbl_qlv');
		$data['nhiemky']	= $this->Mketqua->layDL('sNhiemKy','tbl_qlv');
		$data['kyhop']		= $this->Mketqua->layDL('sKyHop','tbl_qlv');
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$phongban   = $this->Mketqua->layPhongBanDuThao();
        if(!empty($phongban))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $canbo = $this->Mchuyennhan->layCB();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$lanhdao	= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['chuyenvien']	= $this->Mchuyennhan->layCanBo($phongbanHD,8,NULL,NULL);
		$data['phogiamdoc'] = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(5),'iQuyenHan_DHNB','tbl_canbo');
		$data['mangPB']		= $mangPB;
		$data['mangCB']		= $mangCB;
		$data['phongban']	= $phongban;
		$data['lanhdao']	= $lanhdao;
		if(_post('duyet'))
		{
			$data['content'] = $this->duyetDauViec();
		}

		$dulieu				= $this->layDSTaiLieu();
		$data['dulieu']		= $dulieu['dulieu'];
		$mangcon = array(0);
		if($data['dulieu'])
		{
			foreach ($data['dulieu'] as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		$data['dulieuOng']  = $this->Mchuyennhan->layDauViecOng($mangcon);
		$data['g_khoa']		= $dulieu['g_khoa'];
		$data['g_nhiemky']	= $dulieu['g_nhiemky'];
		$data['g_kyhop']	= $dulieu['g_kyhop'];
		$data['g_tieude']	= $dulieu['g_tieude'];
		$data['g_noidung']	= $dulieu['g_noidung'];
		$data['g_linhvuc']	= $dulieu['g_linhvuc'];
		$data['g_lanhdao']	= $dulieu['g_lanhdao'];
		$data['g_phongban']	= $dulieu['g_phongban'];
		// $data['phantrang']	= $dulieu['phantrang'];
		// pr($data['dulieu']);
		$data['title']		= 'Đầu việc chờ xử lý';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vdauviecchoxulyphph_pp';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function duyetDauViec()
	{
		$phongbanHD	= $this->_session['FK_iMaPhongHD'];
		$taikhoan	= $this->_session['PK_iMaCB'];
		$mavanban	=  _post('mavanban');
		if(!empty($mavanban))
		{
			foreach ($mavanban as $key => $value) {
				$chuyenvienph		= _post('chuyenvienph_'.$value);
				$tenchuyenvienph	= _post('tenchuyenvienph_'.$value);
				$hanxuly			= _post('hanxuly_'.$value);
					if(!empty($chuyenvienph))
					{
						$chuyenvienphoihop = explode(',',$chuyenvienph);
						foreach ($chuyenvienphoihop as $key => $vall) {
							$mangchyenvienphoihop[] = array(
								'FK_iMaQLDV'		=> $value,
								'FK_iMaCB_Gui'		=> $taikhoan,
								'sNoiDungChuyen'	=> $tenchuyenvienph,
								'FK_iMaCB_Nhan'		=> $vall,
								'sThoiGian'			=> date('Y-m-d H:i:s'),
								'CT_PH'				=> 2,
								'FK_iMaPB'			=> $phongbanHD,
								'sHanXuLy'			=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
								'iTrangThai'		=> 1,
								'iTraLai'			=> 1,
								'phoihop_chutri'	=> 2
							);
						}
						$this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_qldv',$mangchyenvienphoihop);
						$mangchyenvienphoihop=array();
					}
					$mangcapnhat = array(
						'FK_iMaCVPH_PPH'	=> $chuyenvienph,
						'sGoiY_CVPH_PPH'	=> $tenchuyenvienph
					);
					$this->Mdanhmuc->capnhatDuLieu('FK_iMaVB',$value,'tbl_phoihop_qldv',$mangcapnhat);
					$this->Mchuyennhan->capnhatTrangThaiNguoiChuyen($value,$taikhoan);
			}
			return messagebox('Duyệt đầu việc thành công!','info');
		}
	}
	public function layDSTaiLieu() 
	{
		$taikhoan			= $this->_session['PK_iMaCB'];
		$data['g_khoa']		= _get('khoa');
		$data['g_nhiemky']	= _get('nhiemky');
		$data['g_kyhop']	= _get('kyhop');
		$data['g_tieude']	= _get('tieude');
		$data['g_noidung']	= _get('noidung');
		$data['g_linhvuc']	= _get('linhvuc');
		$data['g_lanhdao']	= _get('lanhdao');
		$data['g_phongban']	= _get('phongban');
		$data['dulieu']		= $this->Mchuyennhan->layDauViecChoXuLy($taikhoan,1,2,2,NULL,$data['g_nhiemky'],$data['g_khoa'],$data['g_kyhop'],$data['g_tieude'],$data['g_noidung'],$data['g_linhvuc'],$data['g_lanhdao'],$data['g_phongban'],NULL,NULL);
        return $data;
	}

}

/* End of file Cdauviecchoxulyphph_pp.php */
/* Location: ./application/controllers/qldv/Cdauviecchoxulyphph_pp.php */