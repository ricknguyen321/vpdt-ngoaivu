<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cketquatralai extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
		$taikhoan			= $this->_session['PK_iMaCB'];
		if(_post('luudulieu'))
		{
			$data['content'] = $this->themDuLieu();
		}
		$data['dulieu']		= $this->Mketqua->ketquaTraLai($taikhoan);
		$data['title']		= 'Kết quả trả lại';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vketquatralai';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function themDuLieu()
	{
		$mafile_an = _post('mafile_an');
		$thongtin  = $this->Mdanhmuc->layDuLieu('qlvFile_id',$mafile_an,'tbl_qlvfilesketqua');
		$duongdan  = $thongtin[0]['qlvFile_path'];
		$name = $_FILES['files']['name'];
		if(!empty($name[0]))
		{
			$this->upload('qlv_uploads_'.date('Y'));
			$files = array();
			foreach ($name as $key => $value) {
				$files[] = array(
					'qlvFile_id'	=> $mafile_an,
					'qlvFile_path'	=> 'qlv_uploads_'.date('Y').'/'.time().clear($value),
					'qlvFile_date'	=> date('Y-m-d H:i:s',time()),
					'iTrangThai'	=> 1,
					'lancuoi'		=> 1
					);
			}
			$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlvfilesketqua',$files,'qlvFile_id');
			if($kiemtra>0)
			{
				if(file_exists($duongdan))
				{
					unlink($duongdan);
				}
				return messagebox('Thêm tệp tin thành công','info');
			}
			else{
				return messagebox('Thêm tệp tin thất bại','danger');
			}
		}
	}
	public function upload($dir)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = time().clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cketquatralai.php */
/* Location: ./application/controllers/qldv/Cketquatralai.php */