<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviecdahoanthanh extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
	}
	public function index()
	{	
		$taikhoan   = $this->_session['PK_iMaCB'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		switch ($quyen) {
			case 3:
				if($chucvu==6) // chức vụ chánh văn phòng
				{
					$trangthai =5; #TP
				}
				break;
			case 4: #GD
				$trangthai =1;
				break;
			case 5: #PGD
				$trangthai =2;
				break;
			case 6: #TP
				if($chucvu==15) // chức vụ chi cục trưởng
				{
					$trangthai =3; #CCT
				}
				if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
				{
					$trangthai =5;
				}
				break;
			case 7: #PTP
				$trangthai =6;
				break;
			case 8: #CV
				$trangthai =7;
				break;
			case 10:#CCP
				$trangthai =4; 
				break;
			case 11:#TP bên chi cục
				$trangthai =5;
				break;
			default:
				$trangthai =0;
				break;
		}
		$vanbancon    = $this->MqldvDeatails->dauviecDaHoanThanh($taikhoan,$trangthai);
		$mangcon = array(0);
		if(!empty($vanbancon))
		{
			foreach ($vanbancon as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		$canbo            = $this->MqldvDeatails->dsCB();
		$mangCB           = array();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $value) {
				$mangCB[$value['PK_iMaCB']] = $value['sHoTen'];
			}
		}
		$data['mangCB']         = $mangCB;
		$data['vanbancon']      = $vanbancon;
		$data['vanbanong']      = $this->MqldvDeatails->layVanBanOng($mangcon);
		$data['title']			= 'Đầu việc đã hoàn thành';
		$temp['data']           = $data;
		$temp['template']       = 'qldv/Vdauviecdahoanthanh';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdauviecdahoanthanh.php */
/* Location: ./application/controllers/qldv/Cdauviecdahoanthanh.php */