<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CeditDetail extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
	}
	public function index()
	{
        $action = _post('action');
        if(!empty($action))
        {
            switch ($action) {
                case 'getLinhvucSub':
                    $this->getLinhvucSub();
                    break;
                default:
                    break;
            }
        }
        
        $id_detail = $this->uri->segment(2);
        if($id_detail >0){
            $data['content'] = $this->capnhatDuLieu($id_detail);
        }
		$data['qldvDetail']		= $this->Mdanhmuc->layDuLieu('qlvDetails_id',$id_detail,'tbl_qlv_details');
		$ma						= $data['qldvDetail'][0]['linhVuc_id'];
		$qlv_id					= $data['qldvDetail'][0]['qlv_id'];
		$data['thongtin']		= $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlv');
		if(empty($data['thongtin']))
        {
            redirect('dsdauviecvanban');
        }
        else{
            $taikhoan = $this->_session['PK_iMaCB'];
            if($data['thongtin'][0]['input_per']!=$taikhoan){
                redirect('dsdauviecvanban');
            }
        }
		$data['dsloaivanban']	= $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['file_dk']		= $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlvfiles');
		$data['lanhdao']		= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(5),'iQuyenHan_DHNB','tbl_canbo');
		$data['lanhdaogd']		= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4),'iQuyenHan_DHNB','tbl_canbo');
		$data['title']			= 'Chỉnh sửa đầu việc con';
		$temp['data']			= $data;
		$temp['template']		= 'qldv/VeditDetail';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($id_detail)
	{
		if(_post('luulai'))
		{	
			$data=array(
				'qlvDetails_desc'	=> _post('qlvDetails_desc'),
				'linhVuc_id'		=> 0,
				'linhvuc_sub'		=> 0
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$id_detail,'tbl_qlv_details',$data);
			if($kiemtra>0)
			{
				redirect('viewldvDetails/'.$id_detail);
			}
		}
	}
	 public function getLinhvucSub()
    {
        $ma = _post('ma');
        $lv = $this->Mdanhmuc->layDuLieu('id_parent',$ma,'tbl_linhvuc_qldv');
        echo json_encode($lv);exit();
    }

}

/* End of file CeditDetail.php */
/* Location: ./application/controllers/qldv/CeditDetail.php */