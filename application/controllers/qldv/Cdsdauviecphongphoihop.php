<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsdauviecphongphoihop extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mchuyennhan');
        $this->load->library('pagination');
	}
	public function index()
	{
		$data['khoa']		= $this->Mketqua->layDL('sKhoa','tbl_qlv');
		$data['nhiemky']	= $this->Mketqua->layDL('sNhiemKy','tbl_qlv');
		$data['kyhop']		= $this->Mketqua->layDL('sKyHop','tbl_qlv');
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$quyendb 	= $this->_session['iQuyenDB'];
		$phongban   = $this->Mketqua->layPhongBanDuThao();
        if(!empty($phongban))
        {
            foreach ($phongban  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $canbo = $this->Mchuyennhan->layCB();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$lanhdao	= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['phogiamdoc'] = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(5),'iQuyenHan_DHNB','tbl_canbo');
		$data['mangPB']		= $mangPB;
		$data['mangCB']		= $mangCB;
		$data['phongban']	= $phongban;
		$data['lanhdao']	= $lanhdao;
		$dulieu				= $this->layDSTaiLieu();
		$data['dulieu']		= $dulieu['dulieu'];
		$mangcon = array(0);
		if($phongbanHD==12)
		{
			$mangcaphai=array(0,$quyendb);
		}
		else{
			$mangcaphai = NULL;
		}
		if($data['dulieu'])
		{
			foreach ($data['dulieu'] as $key => $value) {
				$mangcon[] = $value['qlv_id'];
				$data['dulieu'][$key]['chuyennhan'] = $this->Mchuyennhan->layQuyTrinhXuLyPH($value['qlvDetails_id'],array(0,2),array($phongbanHD),array(1),1);
			}
		}
		$data['dulieuOng']  = $this->Mchuyennhan->layDauViecOng($mangcon);
		$data['g_khoa']		= $dulieu['g_khoa'];
		$data['g_nhiemky']	= $dulieu['g_nhiemky'];
		$data['g_kyhop']	= $dulieu['g_kyhop'];
		$data['g_tieude']	= $dulieu['g_tieude'];
		$data['g_noidung']	= $dulieu['g_noidung'];
		$data['g_linhvuc']	= $dulieu['g_linhvuc'];
		$data['g_lanhdao']	= $dulieu['g_lanhdao'];
		$data['g_phongban']	= $dulieu['g_phongban'];
		// pr($data['dulieu']);
		$data['title']		= 'Đầu việc phòng chủ trì';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vdsdauviecphongphoihop';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function layDSTaiLieu() 
	{
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		$data['g_khoa']		= _get('khoa');
		$data['g_nhiemky']	= _get('nhiemky');
		$data['g_kyhop']	= _get('kyhop');
		$data['g_tieude']	= _get('tieude');
		$data['g_noidung']	= _get('noidung');
		$data['g_linhvuc']	= _get('linhvuc');
		$data['g_lanhdao']	= _get('lanhdao');
		$data['g_phongban']	= _get('phongban');
		$data['dulieu']		= $this->Mchuyennhan->layDauViecCuaPhongPH($phongbanHD,2,NULL,$data['g_nhiemky'],$data['g_khoa'],$data['g_kyhop'],$data['g_tieude'],$data['g_noidung'],$data['g_linhvuc'],$data['g_lanhdao'],$data['g_phongban'],NULL,NULL);
        return $data;
	}

}

/* End of file Cdsdauviecphongphoihop.php */
/* Location: ./application/controllers/qldv/Cdsdauviecphongphoihop.php */