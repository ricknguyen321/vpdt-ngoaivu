<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauvieclinhvuc extends MY_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('qldv/Mqldv_linhvuc');
		$this->load->model('qldv/Mqldv_phongban');
	}
	public function index()
	{
		$data['title']    = 'Danh sách đầu việc theo lĩnh vực';
		$linhvuccha = $this->Mqldv_linhvuc->layLinhVucCha();
		if(!empty($linhvuccha))
		{
			foreach ($linhvuccha as $key => $value) {
				$linhvuccha[$key]['linhvuccon'] = $this->Mqldv_linhvuc->layLinhVucCon($value['linhVuc_id']);				
			}
		}
		$linhvuccon =$this->Mqldv_linhvuc->LinhVucCon();
		$mangLV=array();
		if(!empty($linhvuccon))
		{
			foreach ($linhvuccon as $k => $val) {
				$mangLV[] = $val['linhVuc_id'];
			}
		}
		$tongdauviec_linhvuc_con = $this->Mqldv_linhvuc->layDauViecLinhVuc($mangLV);
		$thuchien_dunghan        = $this->Mqldv_linhvuc->layDauViecThongKe($mangLV,1,1,NULL);
		$thuchien_quahan         = $this->Mqldv_linhvuc->layDauViecThongKe($mangLV,1,NULL,1);
		$hoanthanh_dunghan       = $this->Mqldv_linhvuc->layDauViecThongKe($mangLV,2,1,NULL);
		$hoanthanh_quahan        = $this->Mqldv_linhvuc->layDauViecThongKe($mangLV,2,NULL,1);

		$mang_tong_con          = array();
		$mang_thuchien_dunghan  = array();
		$mang_thuchien_quahan   = array();
		$mang_hoanthanh_dunghan = array();
		$mang_hoanthanh_quahan  = array();

		if(!empty($tongdauviec_linhvuc_con))
		{
			foreach ($tongdauviec_linhvuc_con as $key => $val) {
				$mang_tong_con[$val['linhvuc_sub']] =$val['tong'];
			}
		}
		if(!empty($thuchien_dunghan))
		{
			$data['thuchien_dunghan'] = 0;
			foreach ($thuchien_dunghan as $key => $value) {
				$mang_thuchien_dunghan[$value['linhvuc_sub']] = $value['tong'];
				$data['thuchien_dunghan'] = $data['thuchien_dunghan'] + $value['tong'];
			}
		}
		if(!empty($thuchien_quahan))
		{
			$data['thuchien_quahan'] = 0;
			foreach ($thuchien_quahan as $key => $value) {
				$mang_thuchien_quahan[$value['linhvuc_sub']] = $value['tong'];
				$data['thuchien_quahan'] = $data['thuchien_quahan'] + $value['tong'];
			}
		}
		if(!empty($hoanthanh_dunghan))
		{
			$data['hoanthanh_dunghan'] = 0;
			foreach ($hoanthanh_dunghan as $key => $value) {
				$mang_hoanthanh_dunghan[$value['linhvuc_sub']] = $value['tong'];
				$data['hoanthanh_dunghan'] = $data['hoanthanh_dunghan'] + $value['tong'];
			}
		}
		if(!empty($thuchien_quahan))
		{
			$data['thuchien_quahan'] = 0;
			foreach ($thuchien_quahan as $key => $value) {
				$mang_hoanthanh_quahan[$value['linhvuc_sub']] = $value['tong'];
				$data['thuchien_quahan'] = $data['thuchien_quahan'] + $value['tong'];
			}
		}
		$data['tongdauviec']            = $this->Mqldv_phongban->tongDauViec();
		$data['mang_tongdauviec']       = $mang_tong_con;
		$data['mang_thuchien_dunghan']  = $mang_thuchien_dunghan;
		$data['mang_thuchien_quahan']   = $mang_thuchien_quahan;
		$data['mang_hoanthanh_dunghan'] = $mang_hoanthanh_dunghan;
		$data['mang_hoanthanh_quahan']  = $mang_hoanthanh_quahan;
		$data['linhvuc']                = $linhvuccha;
		$temp['data']                   = $data;
		$temp['template']               = 'qldv/Vdauvieclinhvuc';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cdauvieclinhvuc.php */
/* Location: ./application/controllers/qldv/Cdauvieclinhvuc.php */