<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchitietdauviecphoihop extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_phoihop');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{	
		$taikhoan   = $this->_session['PK_iMaCB'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		switch ($quyen) {
			case 3:
				if($chucvu==6) // chức vụ chánh văn phòng
				{
					$trangthai =5; #TP
				}
				break;
			case 6: #TP
				if($chucvu==15) // chức vụ chi cục trưởng
				{
					$trangthai =5; #CCT
				}
				if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
				{
					$trangthai =5;
				}
				break;
			default:
				$trangthai =9;
				break;
		}
		$ma               = $this->uri->segment(2);
		if(_post('hoanthanh'))
		{
			$data['content'] = $this->hoanthanhVB($ma);
		}
		if($phongbanHD==12)
		{
			$quyendb= $this->_session['iQuyenDB'];
			$phongchicuc = array(0,$quyendb);
		}
		else{
			$phongchicuc = NULL;
		}
		$data['dsvanban'] = $this->Mchuyennhan->layQuyTrinhXuLyPH($ma,array(0,2),array($phongbanHD),array(1),1);
		$canbo            = $this->MqldvDeatails->dsCB();
		$mangCB           = array();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $value) {
				$mangCB[$value['PK_iMaCB']] = $value['sHoTen'];
			}
		}
		$data['mangCB']         = $mangCB;
		$thongtin               = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$ma,'tbl_qlv_details');
		$qlv_id                 = $thongtin[0]['qlv_id'];
		$qlv_id_sub             = $thongtin[0]['qlv_id_sub'];
		$data['qldvLinhvuc']    = $this->Mdanhmuc->getLinhvuc('tbl_linhvuc_qldv');
        foreach ($data['qldvLinhvuc'] as $key => $value) {
            $data['manglinhvuc'][$value['linhVuc_id']] = $value['linhVuc_name'];
        }
        $data['qldvLinhvucSub'] = $this->Mdanhmuc->getLinhvucSub('tbl_linhvuc_qldv');
        foreach ($data['qldvLinhvucSub'] as $key => $value) {
            $data['manglinhvucsub'][$value['linhVuc_id']] = $value['linhVuc_name'];
        }
		$data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail('qlvDetails_id',$qlv_id_sub,'tbl_qlv_details');
		$data['qldvDetail1']    = $thongtin;
		$data['thongtin']       = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlv');
		$data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['file_dk']        = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlvfiles');
		$data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
		$data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
		$data['ketquachinh']	= $this->MqldvDeatails->layKetQuaGiaiQuyet($ma,0);
		$data['ketquaphoihop']	= $this->MqldvDeatails->layKetQuaGiaiQuyet($ma,1);
		$data['title']          = 'Chi tiết đầu việc';
		
		
		// pr($nguoicuoi);
		$kiemtraphong= $this->Mchuyennhan->laythogntinphong($ma,$taikhoan);
		if(!empty($kiemtraphong))
		{
			if($kiemtraphong['CT_PH']==1)
			{
				$nguoicuoi = $this->Mchuyennhan->layNguoiCTPH_GiaiQuyet($ma,$taikhoan);
			}
			else{
				$nguoicuoi = $this->Mchuyennhan->layNguoiPH_GiaiQuyet($ma,$taikhoan);
			}
			if($nguoicuoi>0)
			{
				$data['anhien']     = '';
			}
			else
			{
				$data['anhien']     = 'hide';
			}
		}
		$temp['data']           = $data;
		$temp['template']       = 'qldv/Vchitietdauviecphoihop';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function hoanthanhVB($ma)
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$name = $_FILES['file']['name'];
		$data_them=array(
			'qlvDetails_id' => $ma,
			'qlvFile_path'  => ($name)?'qlv_uploads_'.date('Y').'/'.clear($name):'',
			'qlvFile_date'	=> date('Y-m-d H:i:s'),
			'qlvFile_desc'	=> _post('noidunggiaiquyet'),
			'qlvFile_active'=> ($name)?1:0,
			'user_id'		=> $this->_session['PK_iMaCB'],
			'department_id' => $this->_session['FK_iMaPhongHD'],
			'phoihop'		=> 1,
			'iTrangThai'	=> 1,
			'FK_iMaCB_Duyet'=> 0,
			'lancuoi'		=> 2
			);
		$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_qlvfilesketqua',$data_them);
		if($kiemtra>0)
		{
			$kiemtraphong= $this->Mchuyennhan->laythogntinphong($ma,$taikhoan);
			if(!empty($kiemtraphong))
			{
				if($kiemtraphong['CT_PH']==2)
				{
					$this->Mchuyennhan->capnhatHoanThanh($ma,$kiemtraphong['FK_iMaPB']);
				}
			}
			$this->Mchuyennhan->capnhatTrangThaiNguoiChuyen($ma,$taikhoan);
			$this->upload('qlv_uploads_'.date('Y'),$name,'file');
			return messagebox('Bạn đã hoàn thành đầu việc','info');
		}
	}
	public function upload($dir,$name,$filename)
	{
		if(is_dir($dir)==false){
			mkdir($dir);		// Create directory if it does not exist
		}
		$config['upload_path']   = $dir;
		$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
		$config['overwrite']     = true;	
		$config['file_name']     = $name;
		$this->load->library('upload');
		$this->upload->initialize($config);
		$this->upload->do_upload($filename);
	}


}

/* End of file Cchitietdauviecphoihop.php */
/* Location: ./application/controllers/qldv/Cchitietdauviecphoihop.php */