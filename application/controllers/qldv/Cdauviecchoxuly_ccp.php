<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviecchoxuly_ccp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mchuyennhan');
        $this->load->library('pagination');
	}
	public function index()
	{
		$data['khoa']		= $this->Mketqua->layDL('sKhoa','tbl_qlv');
		$data['nhiemky']	= $this->Mketqua->layDL('sNhiemKy','tbl_qlv');
		$data['kyhop']		= $this->Mketqua->layDL('sKyHop','tbl_qlv');
		$taikhoan   = $this->_session['PK_iMaCB'];
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$data['phongbanchicuc']  = [['PK_iMaPB'=>3,'sTenPB'=>'Phòng một'],['PK_iMaPB'=>4,'sTenPB'=>'Phòng hai'],['PK_iMaPB'=>5,'sTenPB'=>'Phòng ba']];
        if(!empty($data['phongbanchicuc']))
        {
            foreach ($data['phongbanchicuc']  as $value) {
                $mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
            }
        }
        $canbo = $this->Mchuyennhan->layCB();
		if(!empty($canbo))
		{
			foreach ($canbo as $key => $val) {
				$mangCB[$val['PK_iMaCB']] = $val['sHoTen'];
			}
		}
		$lanhdao   = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['phongbanchicuc']  = [['PK_iMaPB'=>3,'sTenPB'=>'Phòng một'],['PK_iMaPB'=>4,'sTenPB'=>'Phòng hai'],['PK_iMaPB'=>5,'sTenPB'=>'Phòng ba']];
		$data['mangPB']		= $mangPB;
		$data['mangCB']		= $mangCB;
		$data['lanhdao']	= $lanhdao;
		if(_post('duyet'))
		{
			$data['content'] = $this->duyetDauViec();
		}

		$dulieu				= $this->layDSTaiLieu();
		$data['dulieu']		= $dulieu['dulieu'];
		// pr($data['dulieu']);
		$mangcon = array(0);
		if($data['dulieu'])
		{
			foreach ($data['dulieu'] as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		$data['dulieuOng']  = $this->Mchuyennhan->layDauViecOng($mangcon);
		$data['g_khoa']		= $dulieu['g_khoa'];
		$data['g_nhiemky']	= $dulieu['g_nhiemky'];
		$data['g_kyhop']	= $dulieu['g_kyhop'];
		$data['g_tieude']	= $dulieu['g_tieude'];
		$data['g_noidung']	= $dulieu['g_noidung'];
		$data['g_linhvuc']	= $dulieu['g_linhvuc'];
		$data['g_lanhdao']	= $dulieu['g_lanhdao'];
		$data['g_phongban']	= $dulieu['g_phongban'];
		$data['phantrang']	= $dulieu['phantrang'];
		// pr($data['dulieu']);
		$data['title']		= 'Đầu việc chờ xử lý';
		$temp['data']		= $data;
		$temp['template']	= 'qldv/Vdauviecchoxuly_ccp';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function duyetDauViec()
	{
		$phongbanHD = $this->_session['FK_iMaPhongHD'];
		$taikhoan = $this->_session['PK_iMaCB'];
		$mavanban =  _post('mavanban');
		if(!empty($mavanban))
		{
			foreach ($mavanban as $key => $value) {
				$phongchutri		= _post('phongchutri_'.$value);
				$tenchutri 			= _post('chutri_'.$value);
				$phongphoihop		= _post('phongph_'.$value);
				$tenphongphoihop	= _post('tenphongph_'.$value);
				$hanxuly			= _post('hanxuly_'.$value);
				if(!empty($phongchutri))
				{
					if($phongchutri==3)
                    {
                        $truongphong = 594; // phòng một Đặng Thanh Vân
                    }
                    elseif($phongchutri==4){
                        $truongphong = 327; // phòng hai Trần Bích Hồng
                    }
                    else{
                    	$truongphong = 257; // phòng ba Hoàng Đình Tiến
                    }
					$mangchuyennhan = array(
						'FK_iMaQLDV'		=> $value,
						'FK_iMaCB_Gui'		=> $taikhoan,
						'sNoiDungChuyen'	=> $tenchutri.$tenphongphoihop,
						'FK_iMaCB_Nhan'		=> $truongphong,
						'sThoiGian'			=> date('Y-m-d H:i:s'),
						'CT_PH'				=> 1,
						'FK_iMaPB'			=> $phongbanHD,
						'FK_iMaPB_CapHai'	=> $phongchutri,
						'sHanXuLy'			=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
						'iTrangThai'		=> 1,
						'iTraLai'			=> 1,
						'phoihop_chutri'	=> 1
					);
					$this->Mdanhmuc->themDuLieu('tbl_chuyennhan_qldv',$mangchuyennhan);
					if(!empty($phongphoihop))
					{
						$phoihop = explode(',',$phongphoihop);
						foreach ($phoihop as $key => $val) {
							if($val==3)
		                    {
		                        $truongphong = 594; // phòng một Đặng Thanh Vân
		                    }
		                    elseif($val==4){
		                        $truongphong = 327; // phòng hai Trần Bích Hồng
		                    }
		                    else{
		                    	$truongphong = 257; // phòng ba Hoàng Đình Tiến
		                    }
							$mangphoihop[] = array(
								'FK_iMaQLDV'		=> $value,
								'FK_iMaCB_Gui'		=> $taikhoan,
								'sNoiDungChuyen'	=> $tenchutri.$tenphongphoihop,
								'FK_iMaCB_Nhan'		=> $truongphong,
								'sThoiGian'			=> date('Y-m-d H:i:s'),
								'CT_PH'				=> 1,
								'FK_iMaPB'			=> $phongbanHD,
								'FK_iMaPB_CapHai'	=> $val,
								'sHanXuLy'			=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
								'iTrangThai'		=> 1,
								'iTraLai'			=> 1,
								'phoihop_chutri'	=> 2
							);
						}
						$this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_qldv',$mangphoihop);
						$mangphoihop=array();
					}
				}
				$this->Mchuyennhan->capnhatTrangThaiNguoiChuyen($value,$taikhoan);
				$mangcapnhat = array(
					'FK_iMaPB_CT_CC'	=> $phongchutri,
					'FK_iMaPB_PH_CC'	=> $phongphoihop,
					'sGoiY_PB_CC'		=> $tenphongphoihop,
				);
				$this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$value,'tbl_qlv_details',$mangcapnhat);
			}
			return messagebox('Duyệt đầu việc thành công!','info');
		}
	}
	public function layDSTaiLieu() 
	{
		$taikhoan			= $this->_session['PK_iMaCB'];
		$data['g_khoa']		= _get('khoa');
		$data['g_nhiemky']	= _get('nhiemky');
		$data['g_kyhop']	= _get('kyhop');
		$data['g_tieude']	= _get('tieude');
		$data['g_noidung']	= _get('noidung');
		$data['g_linhvuc']	= _get('linhvuc');
		$data['g_lanhdao']	= _get('lanhdao');
		$data['g_phongban']	= _get('phongban');

		$config['base_url']             = base_url().'dauviecchoxuly_pgd?khoa='.$data['g_khoa'].'&nhiemky='.$data['g_nhiemky'].'&kyhop='.$data['g_kyhop'].'&tieude='.$data['g_tieude'].'&noidung='.$data['g_noidung'].'&linhvuc='.$data['g_linhvuc'].'&lanhdao='.$data['g_lanhdao'].'&phongban='.$data['g_phongban'];
		$config['total_rows']           = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,0,1,NULL,$data['g_nhiemky'],$data['g_khoa'],$data['g_kyhop'],$data['g_tieude'],$data['g_noidung'],$data['g_linhvuc'],$data['g_lanhdao'],$data['g_phongban']);
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dauviecchoxuly_pgd');
      	}
		$data['dulieu']		= $this->Mchuyennhan->layDauViecChoXuLy($taikhoan,1,1,1,NULL,$data['g_nhiemky'],$data['g_khoa'],$data['g_kyhop'],$data['g_tieude'],$data['g_noidung'],$data['g_linhvuc'],$data['g_lanhdao'],$data['g_phongban'],$config['per_page'],$data['page']);
		$data['phantrang']	= $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdauviecchoxuly_ccp.php */
/* Location: ./application/controllers/qldv/Cdauviecchoxuly_ccp.php */