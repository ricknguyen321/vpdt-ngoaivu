<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cduyetketqua extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
		$data['khoa']		= $this->Mketqua->layDL('sKhoa','tbl_qlv');
		$data['nhiemky']	= $this->Mketqua->layDL('sNhiemKy','tbl_qlv');
		$data['kyhop']		= $this->Mketqua->layDL('sKyHop','tbl_qlv');
		$data['loaivanban'] = ['Tài liệu','Kiến nghi'];
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		// pr($quyen);
		if($quyen==3)
		{
			$trangthai = 4;
			$lancuoi   = 2;
		}
		elseif($quyen==6){
			$trangthai = 3;
			$lancuoi   = 2;
		}
		elseif($quyen==7||$quyen==11)
		{
			$trangthai = 2;
			$lancuoi   = 2;
		}
		else{
			redirect('dsdauviecvanban');
		}
		if(_post('duyet'))
		{
			$mavanban = _post('mafile');
			foreach ($mavanban as $key => $val) {
				$mangcapnhat[]=array(
					'qlvFile_id' => $val,
					'iTrangThai' => $trangthai,
					'lancuoi'	 => $lancuoi
				);
			}
			$kiemtra = $this->Mdanhmuc->capnhatnhieuDuLieu('tbl_qlvfilesketqua',$mangcapnhat,'qlvFile_id');
			if($kiemtra>0)
			{
				$data['content'] = messagebox('Duyệt kết quả hoàn thành đầu việc thành công!','info');
			}
		}
		if(_post('guitralai'))
		{
			$mafile_an		= _post('mafile_an');
			$noidungtralai	= _post('noidungtralai');
			$hoten			= $this->_session['sHoTen'];
			$noidung = $noidungtralai.'<p>Người trả lại: <b>'.$hoten.'</b></p>';

			$data_capnhat = array(
				'iTrangThai' => 0,
				'sNoiDungTraLai' => $noidung
			);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('qlvFile_id',$mafile_an,'tbl_qlvfilesketqua',$data_capnhat);
			if($kiemtra>0)
			{
				$data['content'] = messagebox('Kết quả đã được trả lại','info');
			}
		}
		$data['phongban']    = $this->Mketqua->layPhongBanDuThao();
		foreach ($data['phongban'] as $key => $value) {
			$data['mangphongban'][$value['PK_iMaPB']] = $value['sTenPB'];
		}
		$dulieu					= $this->layDanhSach();
		$data['g_khoa']			= $dulieu['g_khoa'];
		$data['g_nhiemky']		= $dulieu['g_nhiemky'];
		$data['g_kyhop']		= $dulieu['g_kyhop'];
		$data['g_loaivanban']	= $dulieu['g_loaivanban'];
		$data['dulieu']			= $dulieu['dulieu'];
		$data['title']    = 'Kết quả hoàn thành';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/Vduyetketqua';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function layDanhSach()
	{
		$data['g_khoa']			= _get('khoa');
		$data['g_nhiemky']		= _get('nhiemky');
		$data['g_kyhop']		= _get('kyhop');
		$data['g_loaivanban']	= _get('loaivanban');
		$phongban				= $this->_session['FK_iMaPhongHD'];
		$taikhoan				= $this->_session['PK_iMaCB'];
		$quyen					= $this->_session['iQuyenHan_DHNB'];
		if($quyen==3)
		{
			$trangthai = 3;
			$phongban  = '';// tổng hợp xem tất các phòng
			$cabo = $taikhoan;
		}
		elseif($quyen==6){
			$trangthai = 2;
			$cabo = '';
		}
		elseif($quyen==7||$quyen==11)
		{
			$trangthai = 1;
			$cabo = $taikhoan;
		}
		// $tong =  $this->Mketqua->demTTKT($trangthai,$phongban,$cabo);
		$data['dulieu'] = $this->Mketqua->layTTKT($trangthai,$phongban,$cabo,$data['g_khoa'],$data['g_nhiemky'],$data['g_kyhop'],$data['g_loaivanban']);
		return $data;
	}
}

/* End of file Cduyetketqua.php */
/* Location: ./application/controllers/qldv/Cduyetketqua.php */