<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviecchitiettong extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_chitiet');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
		$trangthai = _get('trangthai');
		$dunghan   = _get('dunghan');
		$quahan    = _get('quahan');
		$qlv_id    = '';
        if(empty($trangthai) && empty($dunghan) && empty($quahan)) // tổng đầu việc
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        		$mangONG[]=$value['qlv_id'];
        	}
        	$data['vanbanong']     = $this->Mqldv_chitiet->layDauViecOng($mangONG);
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        if($trangthai==1 && $dunghan==1 && empty($quahan)) // đang lm đúng hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        		$mangONG[]=$value['qlv_id'];
        	}
        	$data['vanbanong']     	= $this->Mqldv_chitiet->layDauViecOng($mangONG);
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);

        }
        if($trangthai==1 && empty($dunghan) && $quahan==1) // đang lm quá hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        		$mangONG[]=$value['qlv_id'];
        	}
        	$data['vanbanong']     = $this->Mqldv_chitiet->layDauViecOng($mangONG);
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);

        }
        if($trangthai==2 && $dunghan==1 && empty($quahan)) // làm xong đúng hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        		$mangONG[]=$value['qlv_id'];
        	}
        	$data['vanbanong']     = $this->Mqldv_chitiet->layDauViecOng($mangONG);
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        if($trangthai==2 && empty($dunghan) && $quahan==1) // làm xong quá hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        		$mangONG[]=$value['qlv_id'];
        	}
        	$data['vanbanong']     = $this->Mqldv_chitiet->layDauViecOng($mangONG);
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        if($trangthai==3 && empty($dunghan) && empty($quahan)) // làm xong quá hạn
        {
        	$date = date('Y-m-d');
			$new_date = strtotime ( '3 day' , strtotime ( $date ) ) ;
			$new_date = date ( 'Y-m-d' , $new_date );
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,$new_date);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        		$mangONG[]=$value['qlv_id'];
        	}
        	$data['vanbanong']     = $this->Mqldv_chitiet->layDauViecOng($mangONG);
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $phongban = $this->Mketqua->layPhongBanDuThao();
        foreach ($phongban as $key => $value) {
        	$mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['mangPB']   = $mangPB;
		$data['title']    = 'Đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/Vdauviecchitiettong';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdauviecchitiettong.php */
/* Location: ./application/controllers/qldv/Cdauviecchitiettong.php */