<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsdauviecdagiaiquyet extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_chuyenlai');
	}
	public function index()
	{
		$taikhoan   = $this->_session['PK_iMaCB'];
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		$chucvu		= $this->_session['FK_iMaCV'];
		switch ($quyen) {
			case 3:
				if($chucvu==6) // chức vụ chánh văn phòng
				{
					$trangthai =5; #TP
				}
				else{
					$trangthai =0; #TH
				}
				break;
			case 4: #GD
				$trangthai =1;
				break;
			case 5: #PGD
				$trangthai =2;
				break;
			case 6: #TP
				if($chucvu==15) // chức vụ chi cục trưởng
				{
					$trangthai =3; #CCT
				}
				if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
				{
					$trangthai =5;
				}
				break;
			case 7: #PTP
				$trangthai =6;
				break;
			case 8: #CV
				$trangthai =7;
				break;
			case 10:#CCP
				$trangthai =4; 
				break;
			case 11:#TP bên chi cục
				$trangthai =5;
				break;
			default:
				$trangthai =0;
				break;
		}
		if(_post('guidi'))
		{
			$ma = _post('guidi');
			$nguoinhanlai = $this->Mqldv_chuyenlai->layNguoiChuyen($ma);
			switch ($quyen) {
				case 3:
					if($chucvu==6) // chức vụ chánh văn phòng
					{
						if(!empty($nguoinhanlai['process_per']))
						{
							$nguoinhan        = $nguoinhanlai['process_per'];
							$trangthai_active = 2;
						}
						else{
							$trangthai_active = 1;
							$nguoinhan        = $nguoinhanlai['coordinate_per'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					}
					break;
				case 6: #TP
					if($chucvu==15) // chức vụ chi cục trưởng
					{
						if(!empty($nguoinhanlai['process_per']))
						{
							$nguoinhan        = $nguoinhanlai['process_per'];
							$trangthai_active = 2;
						}
						else{
							$trangthai_active = 1;
							$nguoinhan        = $nguoinhanlai['coordinate_per'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_chicuctruong'          => 0,
							'date_chicuctruong'          => NULL,
							'text_chicuctruong'          => NULL,
							'user_chicucpho'             => 0,
							'date_chicucpho'             => NULL,
							'text_chicucpho'             => NULL,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'Han_CCT'                    => NULL,
							'Han_CCP'                     => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					}
					if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
					{
						if(!empty($nguoinhanlai['process_per']))
						{
							$nguoinhan        = $nguoinhanlai['process_per'];
							$trangthai_active = 2;
						}
						else{
							$trangthai_active = 1;
							$nguoinhan        = $nguoinhanlai['coordinate_per'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					}
					break;
				case 10:#CCP
						$trangthai_active = 3;
						$nguoinhan        = $nguoinhanlai['user_truongphong'];
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_chicucpho'             => 0,
							'date_chicucpho'             => NULL,
							'text_chicucpho'             => NULL,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'Han_CCP'                     => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					break;
				case 11:#TP bên chi cục
					if(!empty($nguoinhanlai['user_chicucpho']))
						{
							$nguoinhan        = $nguoinhanlai['user_chicucpho'];
							$trangthai_active = 4;
						}
						else{
							$trangthai_active = 3;
							$nguoinhan        = $nguoinhanlai['user_chicuctruong'];
						}
						$data=array(
							'noidung_tralai'             => _post('noidung'),
							'hoten_tralai'               => _post('nguoitra'),
							'thoigian_tralai'            => date('Y-m-d H:i:s'),
							'qlvDetails_active'          => $trangthai_active,
							'user_truongphong'           => 0,
							'date_truongphong'           => NULL,
							'text_truongphong'           => NULL,
							'user_photruongphong'        => 0,
							'text_photruongphong'        => NULL,
							'date_photruongphong'        => NULL,
							'user_chuyenvien'            => 0,
							'user_chuyenvien_phoihop'    => 0,
							'noidung_chuyenvien_phoihop' => NULL,
							'han_TP'                     => NULL,
							'han_PTP'                    => NULL
						);
					break;
				default:
					$trangthai =0;
				break;
			}

			$data_them=array(
				'qlvDetails_id'   => $ma,
				'qlv_id'          => $nguoinhanlai['qlv_id'],
				'qlv_id_sub'      => $nguoinhanlai['qlv_id_sub'],
				'FK_iMaCB_chuyen' => $this->_session['PK_iMaCB'],
				'thoigian_chuyen' => date('Y-m-d H:i:s'),
				'noidung_chuyen'  => _post('noidung'),
				'FK_iMaCB_nhan'   => $nguoinhan
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$ma,'tbl_qlv_details',$data);
			if($kiemtra>0)
			{
				$this->Mdanhmuc->themDuLieu('tbl_qlv_luuvet',$data_them);
				$data['content'] =  messagebox('Gửi văn bản trả lại thành công','info');
			}
			
		}
		$vanbancon    = $this->MqldvDeatails->layVanBanDaXuLy($taikhoan,$trangthai);
		$mangcon = array(0);
		if(!empty($vanbancon))
		{
			foreach ($vanbancon as $key => $value) {
				$mangcon[] = $value['qlv_id'];
			}
		}
		if($trangthai==3||$trangthai==4||$trangthai==5)// kiểm tra phải trưởng phòng thì dk trả lại
		{
			$data['tralai'] ='co';
		}
		else{
			$data['tralai'] = '';
		}
		$data['vanbancon']    = $vanbancon;
		$data['vanbanong']    = $this->MqldvDeatails->layVanBanOng($mangcon);
		$data['title']        = 'Đầu việc đã giải quyết';
		$temp['data']         = $data;
		$temp['template']     = 'qldv/Vdsdauviecdagiaiquyet';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdsdauviecdagiaiquyet.php */
/* Location: ./application/controllers/qldv/Cdsdauviecdagiaiquyet.php */