<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviecchitiet extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_chitiet');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
		$qlv_id    = _get('qlv_id');
		$trangthai = _get('trangthai');
		$dunghan   = _get('dunghan');
		$quahan    = _get('quahan');
        if(!empty($qlv_id) && empty($trangthai) && empty($dunghan) && empty($quahan)) // tổng đầu việc
        {
        	$data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail('qlv_id',$qlv_id,'tbl_qlv_details');
        	$data['qldvDetail1']    = $this->Mdanhmuc->getQldvDetail1('qlv_id',$qlv_id,'tbl_qlv_details');
        }
        if(!empty($qlv_id) && $trangthai==1 && $dunghan==1 && empty($quahan)) // đang lm đúng hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        	}
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);

        }
        if(!empty($qlv_id) && $trangthai==1 && empty($dunghan) && $quahan==1) // đang lm quá hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        	}
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        if(!empty($qlv_id) && $trangthai==2 && $dunghan==1 && empty($quahan)) // làm xong đúng hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        	}
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        if(!empty($qlv_id) && $trangthai==2 && empty($dunghan) && $quahan==1) // làm xong quá hạn
        {
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,NULL);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        	}
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        if(!empty($qlv_id) && $trangthai==3 && empty($dunghan) && empty($quahan)) // làm xong quá hạn
        {
        	$date = date('Y-m-d');
			$new_date = strtotime ( '3 day' , strtotime ( $date ) ) ;
			$new_date = date ( 'Y-m-d' , $new_date );
        	$data['qldvDetail1']    = $this->Mqldv_chitiet->layDauViecChiTiet($qlv_id,$trangthai,$dunghan,$quahan,$new_date);
        	foreach ($data['qldvDetail1'] as $key => $value) {
        		$mangCHA[]=$value['qlv_id_sub'];
        	}
        	$data['qldvDetail']     = $this->Mqldv_chitiet->layDauViecCha($mangCHA);
        }
        $data['thongtin']       = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlv');
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['file_dk']        = $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_qlvfiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $phongban = $this->Mketqua->layPhongBanDuThao();
        foreach ($phongban as $key => $value) {
        	$mangPB[$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['mangPB']   = $mangPB;
		$data['title']    = 'Đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'qldv/Vdauviecchitiet';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdauviecchitiet.php */
/* Location: ./application/controllers/qldv/Cdauviecchitiet.php */