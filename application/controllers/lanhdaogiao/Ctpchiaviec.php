<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Ctpchiaviec extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        if(_post('luulai') > 0){
            $mangcbphong = array();
            $arr_nk = array();
            $danhsachphong  = $this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
            foreach ($danhsachphong as $key => $value) {
                $mangcbphong[$value['PK_iMaCB']] = $value['sHoTen'];
            }

            $value = _post('luulai');
            $active =2;
            $cvphoihop='';
            if(_post('FK_iMaCB_CV_PH'.$value) !='')$cvphoihop = implode(',', _post('FK_iMaCB_CV_PH'.$value));

            if(_post('FK_iMaCB_PP_CT'.$value) > 0)$active =5;
            else if (_post('FK_iMaCB_CV_CT'.$value) > 0)$active =6;                   
            $mangcapnhat = array(
                    'FK_iMaCB_PP_CT'    => _post('FK_iMaCB_PP_CT'.$value),
                    'FK_iMaCB_CV_CT'    => _post('FK_iMaCB_CV_CT'.$value),
                    'FK_iMaCB_CV_PH'    => $cvphoihop,
                    'qlvDetails_active' => $active
                );

            if($this->_session['FK_iMaPhongHD'] ==12){
                if(_post('FK_iMaCB_CCP'.$value) > 0)$active =3;
                else if(_post('FK_iMaCB_CC_TP'.$value) > 0)$active =4;
                else if(_post('FK_iMaCB_PP_CT'.$value) > 0)$active =5;
                else if (_post('FK_iMaCB_CV_CT'.$value) > 0)$active =6;
                $mangcapnhat = array(
                    'FK_iMaCB_CCP'      => _post('FK_iMaCB_CCP'.$value),
                    'FK_iMaCB_CC_TP'    => _post('FK_iMaCB_CC_TP'.$value),
                    'FK_iMaCB_PP_CT'    => _post('FK_iMaCB_PP_CT'.$value),
                    'FK_iMaCB_CV_CT'    => _post('FK_iMaCB_CV_CT'.$value),
                    'FK_iMaCB_CV_PH'    => $cvphoihop,
                    'qlvDetails_active' => $active 
                );
            }
            if($active==3){
                $kehoach_data_cv = array(
                'vanban_id' => 0,
                'kh_noidung' => _post('qlvDetails_desc'.$value),
                'date_nhap' => _post('qlvDetails_date'.$value),
                'vanban_skh' => $value,
                'tuan' => (int)date("W"),
                'ngay_nhan' => date('Y-m-d H:i:s',time()),
                'ngay_han' => _post('han_thongke'.$value),
                'canbo_id' => _post('FK_iMaCB_CCP'.$value),
                'thuc_hien' => '1',
                'loai_kh' => 1,
                'chucvu'  => 10,
                'lanhdao_id' => $this->_session['PK_iMaCB'],
                'user_input' => $this->_session['PK_iMaCB'],
                'phong_id' => $this->_session['FK_iMaPhongHD'],
                'lanhdao_so' => _post('lanhdao_id'.$value)
                );

                $arr_nk = array(
                    'nguoi_giao' => $mangcbphong[$this->_session['PK_iMaCB']] ,
                    'nguoi_nhan' => $mangcbphong[_post('FK_iMaCB_CCP'.$value)] ,
                    'thoigian' => date('Y-m-d H:i:s',time()),
                    'id_vanban' => $value
                );
            }
            if($active==4){
                $kehoach_data_cv = array(
                'vanban_id' => 0,
                'kh_noidung' => _post('qlvDetails_desc'.$value),
                'date_nhap' => _post('qlvDetails_date'.$value),
                'vanban_skh' => $value,
                'tuan' => (int)date("W"),
                'ngay_nhan' => date('Y-m-d H:i:s',time()),
                'ngay_han' => _post('han_thongke'.$value),
                'canbo_id' => _post('FK_iMaCB_CC_TP'.$value),
                'thuc_hien' => '1',
                'loai_kh' => 1,
                'chucvu'  => 11,
                'lanhdao_id' => $this->_session['PK_iMaCB'],
                'user_input' => $this->_session['PK_iMaCB'],
                'phong_id' => $this->_session['FK_iMaPhongHD'],
                'lanhdao_so' => _post('lanhdao_id'.$value)
            );

            $arr_nk = array(
                    'nguoi_giao' => $mangcbphong[$this->_session['PK_iMaCB']] ,
                    'nguoi_nhan' => $mangcbphong[_post('FK_iMaCB_CC_TP'.$value)] ,
                    'thoigian' => date('Y-m-d H:i:s',time()),
                    'id_vanban' => $value
                );
            }
                
            if($active==5){
                $kehoach_data_cv = array(
                'vanban_id' => 0,
                'kh_noidung' => _post('qlvDetails_desc'.$value),
                'date_nhap' => _post('qlvDetails_date'.$value),
                'vanban_skh' => $value,
                'tuan' => (int)date("W"),
                'ngay_nhan' => date('Y-m-d H:i:s',time()),
                'ngay_han' => _post('han_thongke'.$value),
                'canbo_id' => _post('FK_iMaCB_PP_CT'.$value),
                'thuc_hien' => '1',
                'loai_kh' => 1,
                'chucvu'  => 7,
                'lanhdao_id' => $this->_session['PK_iMaCB'],
                'user_input' => $this->_session['PK_iMaCB'],
                'phong_id' => $this->_session['FK_iMaPhongHD'],
                'lanhdao_so' => _post('lanhdao_id'.$value)
            );
            $arr_nk = array(
                'nguoi_giao' => $mangcbphong[$this->_session['PK_iMaCB']] ,
                'nguoi_nhan' => $mangcbphong[_post('FK_iMaCB_PP_CT'.$value)] ,
                'thoigian' => date('Y-m-d H:i:s',time()),
                'id_vanban' => $value
            );
            }
                
            if($active==6){
                $kehoach_data_cv = array(
                'vanban_id' => 0,
                'kh_noidung' => _post('qlvDetails_desc'.$value),
                'date_nhap' => _post('qlvDetails_date'.$value),
                'vanban_skh' => $value,
                'tuan' => (int)date("W"),
                'ngay_nhan' => date('Y-m-d H:i:s',time()),
                'ngay_han' => _post('han_thongke'.$value),
                'canbo_id' => _post('FK_iMaCB_CV_CT'.$value),
                'thuc_hien' => '3',
                'loai_kh' => 1,
                'chucvu'  => 8,
                'lanhdao_id' => $this->_session['PK_iMaCB'],
                'user_input' => $this->_session['PK_iMaCB'],
                'phong_id' => $this->_session['FK_iMaPhongHD'],
                'lanhdao_so' => _post('lanhdao_id'.$value)              
            ); 
            $arr_nk = array(
                    'nguoi_giao' => $mangcbphong[$this->_session['PK_iMaCB']] ,
                    'nguoi_nhan' => $mangcbphong[_post('FK_iMaCB_CV_CT'.$value)] ,
                    'thoigian' => date('Y-m-d H:i:s',time()),
                    'id_vanban' => $value
            );

            $taikhoan   = $this->_session['PK_iMaCB'];
            $quyen      = $this->_session['iQuyenHan_DHNB'];
            $chucvu     = $this->_session['FK_iMaCV'];
            $phongbanHD = $this->_session['FK_iMaPhongHD'];

            $arr_cvphoihop = _post('FK_iMaCB_CV_PH'.$value);
            if(count($arr_cvphoihop) > 0){
                foreach ($arr_cvphoihop as $key => $val) {
                    $mangcvphoihop[] = array(
                        'FK_iMaQLDV'        => $value,
                        'FK_iMaCB_Gui'      => $taikhoan,
                        'sNoiDungChuyen'    => '',
                        'FK_iMaCB_Nhan'     => $val,
                        'sThoiGian'         => date('Y-m-d H:i:s'),
                        'CT_PH'             => 1,
                        'FK_iMaPB'          => $phongbanHD,
                        'sHanXuLy'          => _post('han_thongke'.$value),
                        'iTrangThai'        => 1,
                        'iTraLai'           => 1,
                        'phoihop_chutri'    => 2
                    );
                }
                $this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_lanhdaogiao',$mangcvphoihop);
            }
                
            }

            if($active>2){
                $this->Mdanhmuc->themDuLieu('kehoach',$kehoach_data_cv);
                $this->Mdanhmuc->themDuLieu('tbl_lanhdaogiao_nhatky',$arr_nk);   
            
                $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$value,'tbl_lanhdaogiao_details',$mangcapnhat);
            }
        }        
        
        $data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail2('main_department',$this->_session['FK_iMaPhongHD'],'qlvDetails_active',2,'tbl_lanhdaogiao_details');
        $qlv_id_arr = array();
       
        for($i=0;$i<count($data['qldvDetail']);$i++){
           array_push($qlv_id_arr,$data['qldvDetail'][$i]['qlv_id']);
           $str_phongphoihop = $data['qldvDetail'][$i]['department_id']; 
           if($str_phongphoihop !=""){
                $arr_phongphoihop = explode(',', $str_phongphoihop);
                for($j=0;$j<count($arr_phongphoihop);$j++){
                  $data['qldvDetail'][$i]['phoihop'][$j]  = array($j => $arr_phongphoihop[$j]);
                }
           }
        }
        //tổng đầu việc giao phòng
        $tongviec = count($qlv_id_arr);
        $data['tongviec'] = $tongviec;
        if($tongviec < 1)$qlv_id_arr = array(0);
        
        $data['thongtin']       = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiao');//pr();
       
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['dsphong']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_phongban');
        $data['file_dk']        = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiaofiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $data['cbphong']        = $this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
        $data['maphong'] = $this->_session['FK_iMaPhongHD'];
        
        foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        

        foreach ($data['dsphong'] as $key => $value) {
            $data['mangdsphong'][$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'lanhdaogiao/Vtpchiaviec';
		$this->load->view('layout_admin/layout',$temp);
	}
}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */