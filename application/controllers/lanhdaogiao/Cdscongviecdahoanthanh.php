<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Cdscongviecdahoanthanh extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        $taikhoan   = $this->_session['PK_iMaCB'];
        $quyen      = $this->_session['iQuyenHan_DHNB'];
        $chucvu     = $this->_session['FK_iMaCV'];
        $phongbanHD = $this->_session['FK_iMaPhongHD'];

        if($quyen == 3 or $quyen == 6)
        $data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail2('main_department',$this->_session['FK_iMaPhongHD'],'qlvDetails_active ',9,'tbl_lanhdaogiao_details');
        if($quyen == 7)
        $data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail2('FK_iMaCB_PP_CT',$this->_session['PK_iMaCB'],'qlvDetails_active',9,'tbl_lanhdaogiao_details');
        if($quyen == 8)
        $data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail2('FK_iMaCB_CV_CT',$this->_session['PK_iMaCB'],'qlvDetails_active',9,'tbl_lanhdaogiao_details');
        if($quyen == 10)
        $data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail2('FK_iMaCB_CCP',$this->_session['PK_iMaCB'],'qlvDetails_active ',9,'tbl_lanhdaogiao_details');
        if($quyen == 11)
        $data['qldvDetail']     = $this->Mdanhmuc->getQldvDetail2('FK_iMaCB_CC_TP',$this->_session['PK_iMaCB'],'qlvDetails_active',9,'tbl_lanhdaogiao_details');


        $qlv_id_arr = array();
       
        for($i=0;$i<count($data['qldvDetail']);$i++){
           array_push($qlv_id_arr,$data['qldvDetail'][$i]['qlv_id']);
           $str_phongphoihop = $data['qldvDetail'][$i]['department_id']; 
           if($str_phongphoihop !=""){
                $arr_phongphoihop = explode(',', $str_phongphoihop);
                for($j=0;$j<count($arr_phongphoihop);$j++){
                  $data['qldvDetail'][$i]['phoihop'][$j]  = array($j => $arr_phongphoihop[$j]);
                }
           }
        }
        //tổng đầu việc giao phòng
        $tongviec = count($qlv_id_arr);
        $data['tongviec'] = $tongviec;
        if($tongviec < 1)$qlv_id_arr = array(0);
        
        $data['thongtin']       = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiao');//pr();
       
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['dsphong']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_phongban');
        $data['file_dk']        = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiaofiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $data['cbphong']        = $this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
        $data['maphong'] = $this->_session['FK_iMaPhongHD'];
        
        foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        

        foreach ($data['dsphong'] as $key => $value) {
            $data['mangdsphong'][$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Danh sách công việc phòng đang thực hiện';
		$temp['data']     = $data;
		$temp['template'] = 'lanhdaogiao/Vdscongviecdahoanthanh';
		$this->load->view('layout_admin/layout',$temp);
	}
}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */