<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Ctpphdangthuchien extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        if(_post('luulai') > 0){
            $value = _post('luulai');
            $cvphoihop = _post('FK_iMaCB_CV_PH'.$value);

            $taikhoan   = $this->_session['PK_iMaCB'];
            $quyen      = $this->_session['iQuyenHan_DHNB'];
            $chucvu     = $this->_session['FK_iMaCV'];
            $phongbanHD = $this->_session['FK_iMaPhongHD'];

            if(count($cvphoihop) > 0){
                // xóa dữ liệu cũ truoc khi chèn thêm dữ liệu mới
                $this->Mdanhmuc->xoaDuLieu2('FK_iMaCB_Gui',$taikhoan,'FK_iMaQLDV',$value,'tbl_chuyennhan_lanhdaogiao');
                foreach ($cvphoihop as $key => $val) {
                    $mangcvphoihop[] = array(
                        'FK_iMaQLDV'        => $value,
                        'FK_iMaCB_Gui'      => $taikhoan,
                        'sNoiDungChuyen'    => _post('sNoiDungChuyen'),
                        'FK_iMaCB_Nhan'     => $val,
                        'sThoiGian'         => date('Y-m-d H:i:s'),
                        'CT_PH'             => 1,
                        'FK_iMaPB'          => $phongbanHD,
                        'sHanXuLy'          => _post('han_thongke'.$value),
                        'iTrangThai'        => 1,
                        'iTraLai'           => 1,
                        'phoihop_chutri'    => 2
                    );
                }
                $this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_lanhdaogiao',$mangcvphoihop);

                $phongph   = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$value,'tbl_lanhdaogiao_details');
                $str_phoihop = '';
                $dscvph ='';
                foreach ($phongph as $key => $value1){
                    $str_phoihop .= $value1['phongdaphoihop'];
                    $dscvph .= $value1['FK_iMaCB_CV_PH'];
                }
                if($str_phoihop !='')
                    $str_phoihop =  $this->_session['FK_iMaPhongHD'].','.$str_phoihop;
                else $str_phoihop =  $this->_session['FK_iMaPhongHD'];

                if($dscvph !=''){
                    $arr_dscvph = explode(',', $dscvph);
                    $arr_cbphong =$this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
                    for($i=0;$i<count($arr_dscvph);$i++){
                        for($j=0;$j<count($arr_cbphong);$j++){
                            if($arr_cbphong[$j]['PK_iMaCB'] == $arr_dscvph[$i]){
                                $dscvph=str_replace($arr_dscvph[$i],'0',$dscvph);
                            }
                        }
                    }

                    $dscvph = implode(',', $cvphoihop) .','.$dscvph;
                }
                else $dscvph =  implode(',', $cvphoihop);

                $mangcapnhat = array('phongdaphoihop'=>$str_phoihop,'FK_iMaCB_CV_PH'=>$dscvph );

                $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$value,'tbl_lanhdaogiao_details',$mangcapnhat);

            } 
        }  
            
        
        $data['qldvDetail']     = $this->Mdanhmuc->timkiemDuLieu('phongdaphoihop',$this->_session['FK_iMaPhongHD'],'tbl_lanhdaogiao_details');
        //pr($data['qldvDetail']);
        $qlv_id_arr = array();
       
        for($i=0;$i<count($data['qldvDetail']);$i++){
           array_push($qlv_id_arr,$data['qldvDetail'][$i]['qlv_id']);
           $str_phongphoihop = $data['qldvDetail'][$i]['department_id']; 
           if($str_phongphoihop !=""){
                $arr_phongphoihop = explode(',', $str_phongphoihop);
                for($j=0;$j<count($arr_phongphoihop);$j++){
                  $data['qldvDetail'][$i]['phoihop'][$j]  = array($j => $arr_phongphoihop[$j]);
                }
           }
        }
        //tổng đầu việc giao phòng
        $tongviec = count($qlv_id_arr);
        $data['tongviec'] = $tongviec;
        if($tongviec < 1)$qlv_id_arr = array(0);
        
        $data['thongtin']       = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiao');//pr();
       
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['dsphong']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_phongban');
        $data['file_dk']        = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiaofiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $data['cbphong']        = $this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
        $data['maphong'] = $this->_session['FK_iMaPhongHD'];
        
        foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        

        foreach ($data['dsphong'] as $key => $value) {
            $data['mangdsphong'][$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'lanhdaogiao/Vtpphdangthuchien';
		$this->load->view('layout_admin/layout',$temp);
	}
}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */