<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CeditlanhdaogiaoDetail extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        $action = _post('action');
        if(!empty($action))
        {
            switch ($action) {
                case 'getLinhvucSub':
                    $this->getLinhvucSub();
                    break;
                default:
                    break;
            }
        }
        
        $id_detail = $this->uri->segment(2);
        $mavanban = $this->uri->segment(3);
        if($id_detail >0){
            $data['content'] = $this->capnhatDuLieu($id_detail,$mavanban);
        }
		$data['qldvDetail']		= $this->Mdanhmuc->layDuLieu('qlvDetails_id',$id_detail,'tbl_lanhdaogiao_details');//pr($data['qldvDetail']);


		$ma						= $data['qldvDetail'][0]['linhVuc_id'];
		$qlv_id					= $data['qldvDetail'][0]['qlv_id'];
		$data['thongtin']		= $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_lanhdaogiao');
		if(empty($data['thongtin']))
        {
            redirect('chitietcvlanhdaogiao');
        }
        else{
            $taikhoan = $this->_session['PK_iMaCB'];
            if($data['thongtin'][0]['input_per']!=$taikhoan){
                redirect('chitietcvlanhdaogiao');
            }
        }
		$data['dsloaivanban']	= $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['file_dk']		= $this->Mdanhmuc->layDuLieu('qlv_id',$qlv_id,'tbl_lanhdaogiaofiles');
		$data['lanhdao']		= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['lanhdaogd']		= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4),'iQuyenHan_DHNB','tbl_canbo');
		$data['title']			= 'Chỉnh sửa nội dung công việc';
		$data['main_depart']    = $this->Mketqua->layPhongBanDuThao();

		$temp['data']			= $data;
		$temp['template']		= 'lanhdaogiao/VeditlanhdaogiaoDetail';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($id_detail,$qlv_id)
	{
		if(_post('luulai'))
		{	
			$phoihop = _post('department_id');
			if(!empty($phoihop))
			{
				$phoihop = implode(',', $phoihop);
			}
			else{
				$phoihop = '';
			}
			$data=array(
				'qlvDetails_desc'	=> _post('qlvDetails_desc'),
				'main_department'   => _post('main_department'),
				'FK_iMaCB_LanhDao'  => _post('lanhdao'),
				'department_id'		=>$phoihop,
				'han_thongke'		=>date('Y-m-d',strtotime(str_replace('/', '-', _post('hanxuly')))),
				'linhVuc_id'		=> 0,
				'linhvuc_sub'		=> 0,
				'ghichu'			=> _post('ghichu')
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$id_detail,'tbl_lanhdaogiao_details',$data);
			if($kiemtra>0)
			{
				redirect('viewlanhdaogiaoDetails/'.$qlv_id);
			}
		}
	}
	 public function getLinhvucSub()
    {
        $ma = _post('ma');
        $lv = $this->Mdanhmuc->layDuLieu('id_parent',$ma,'tbl_linhvuc_qldv');
        echo json_encode($lv);exit();
    }

}

/* End of file CeditDetail.php */
/* Location: ./application/controllers/qldv/CeditDetail.php */