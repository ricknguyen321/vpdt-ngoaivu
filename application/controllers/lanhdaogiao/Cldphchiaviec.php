<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Cldphchiaviec extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        if(_post('luulai') > 0){
            $value = _post('luulai');
            $cvphoihop = _post('FK_iMaCB_CV_PH'.$value);

            $taikhoan   = $this->_session['PK_iMaCB'];
            $quyen      = $this->_session['iQuyenHan_DHNB'];
            $chucvu     = $this->_session['FK_iMaCV'];
            $phongbanHD = $this->_session['FK_iMaPhongHD'];

            if(count($cvphoihop) > 0){
                foreach ($cvphoihop as $key => $val) {
                    $mangcvphoihop[] = array(
                        'FK_iMaQLDV'        => $value,
                        'FK_iMaCB_Gui'      => $taikhoan,
                        'sNoiDungChuyen'    => '',
                        'FK_iMaCB_Nhan'     => $val,
                        'sThoiGian'         => date('Y-m-d H:i:s'),
                        'CT_PH'             => 1,
                        'FK_iMaPB'          => $phongbanHD,
                        'sHanXuLy'          => _post('han_thongke'.$value),
                        'iTrangThai'        => 1,
                        'iTraLai'           => 1,
                        'phoihop_chutri'    => 2
                    );
                }
                $this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_lanhdaogiao',$mangcvphoihop);

                $phongph   = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$value,'tbl_lanhdaogiao_details');
                $dscvph ='';
                foreach ($phongph as $key => $value1){
                    $dscvph .= $value1['FK_iMaCB_CV_PH'];
                }
                if($dscvph !='')
                    $dscvph = implode(',', $cvphoihop) .','.$dscvph;
                else $dscvph =  implode(',', $cvphoihop);

                $mangcapnhat = array('FK_iMaCB_CV_PH'=>$dscvph );
                $this->Mdanhmuc->capnhatDuLieu('qlvDetails_id',$value,'tbl_lanhdaogiao_details',$mangcapnhat);

                $str_idchuyennhan = $this->Mdanhmuc->layDuLieu2_2('FK_iMaCB_Nhan',$this->_session['PK_iMaCB'],'iTrangThai',1,'FK_iMaQLDV',$value,'tbl_chuyennhan_lanhdaogiao')[0]['PK_CN_QLDV'];

                $capnhatchuyennhan = array('iTrangThai'=>3 );
                $this->Mdanhmuc->capnhatDuLieu('PK_CN_QLDV',$str_idchuyennhan,'tbl_chuyennhan_lanhdaogiao',$capnhatchuyennhan);

            } 
        }        
        
        if($this->_session['iQuyenHan_DHNB'] !=8){
            $arr_str = $this->Mdanhmuc->layDuLieu2('FK_iMaCB_Nhan',$this->_session['PK_iMaCB'],'iTrangThai',1,'tbl_chuyennhan_lanhdaogiao');
            $arr_ldg = array();
            for($i=0;$i<count($arr_str);$i++){
               array_push($arr_ldg,$arr_str[$i]['FK_iMaQLDV']);
            }
            if(count($arr_ldg)<1)$arr_ldg=array(0);
            $data['qldvDetail']     = $this->Mdanhmuc->getWhereInLanhdaogiao('qlvDetails_id',$arr_ldg,'qlvDetails_id','tbl_lanhdaogiao_details');
           
        }else{
            $data['qldvDetail']     = array(0);
        }
        
        //pr($data['qldvDetail']);
        $qlv_id_arr = array();
       
        for($i=0;$i<count($data['qldvDetail']);$i++){
           array_push($qlv_id_arr,$data['qldvDetail'][$i]['qlv_id']);
           $str_phongphoihop = $data['qldvDetail'][$i]['department_id']; 
           if($str_phongphoihop !=""){
                $arr_phongphoihop = explode(',', $str_phongphoihop);
                for($j=0;$j<count($arr_phongphoihop);$j++){
                  $data['qldvDetail'][$i]['phoihop'][$j]  = array($j => $arr_phongphoihop[$j]);
                }
           }
        }
        //tổng đầu việc giao phòng
        $tongviec = count($qlv_id_arr);
        $data['tongviec'] = $tongviec;
        if($tongviec < 1)$qlv_id_arr = array(0);
        
        $data['thongtin']       = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiao');//pr();
       
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['dsphong']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_phongban');
        $data['file_dk']        = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiaofiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        $data['cbphong']        = $this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
        $data['maphong'] = $this->_session['FK_iMaPhongHD'];
        
        foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        

        foreach ($data['dsphong'] as $key => $value) {
            $data['mangdsphong'][$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Công việc lãnh đạo giao phối hợp chờ xử lý';
		$temp['data']     = $data;
		$temp['template'] = 'lanhdaogiao/Vldphchiaviec';
		$this->load->view('layout_admin/layout',$temp);
	}
}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */