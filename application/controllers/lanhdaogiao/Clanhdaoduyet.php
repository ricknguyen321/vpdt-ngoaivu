<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Clanhdaoduyet extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mketqua');
	}
	public function index()
	{
        $action = _post('action');
        if(!empty($action))
        {
            switch ($action) {
                case 'getLinhvucSub':
                    $this->getLinhvucSub();
                    break;
                default:
                    break;
            }
        }
        if(_post('duyetcongviec')){
           $arr = explode(',', _post('string_kh_id')) ;
            $data = array(                
                "qlv_time_duyet"       => date('Y-m-d H:i:s'),
                "qlv_active"           => 2
                 );
            $data1 = array(                
                "qlvDetails_time_duyetGD"       => date('Y-m-d H:i:s'),
                "qlvDetails_active"    => 2
                 );
            for($i=0;$i<count($arr);$i++){
                if($arr[$i] >0){
                    $this->Mdanhmuc->capnhatDuLieu('qlv_id',$arr[$i],'tbl_lanhdaogiao',$data); 
                   $this->Mdanhmuc->capnhatDuLieu('qlv_id',$arr[$i],'tbl_lanhdaogiao_details',$data1);
                }
            }
            redirect('lanhdaoduyet');
        }
        
        $data['thongtin']       = $this->Mdanhmuc->layDuLieu2('qlv_active',1,'lanhdao_id',$this->_session['PK_iMaCB'],'tbl_lanhdaogiao');

        $arr_qlv_id = array();
        for($i=0;$i<count($data['thongtin']);$i++){
            array_push($arr_qlv_id,$data['thongtin'][$i]['qlv_id']);
        }
        if(count($arr_qlv_id) < 1)$arr_qlv_id = array(0);
        $data['qldvDetail']     = $this->Mdanhmuc->getWhereIn1('qlv_id',$arr_qlv_id,'qlv_id','tbl_lanhdaogiao_details');

        for($i=0;$i<count($data['qldvDetail']);$i++){
           $str_phongphoihop = $data['qldvDetail'][$i]['department_id']; 
           if($str_phongphoihop !=""){
                $arr_phongphoihop = explode(',', $str_phongphoihop);
                for($j=0;$j<count($arr_phongphoihop);$j++){
                  $data['qldvDetail'][$i]['phoihop'][$j]  = array($j => $arr_phongphoihop[$j]);
                }
           }
        }

        $data['qldvDetail1']    = $this->Mdanhmuc->getQldvDetail_in('qlv_id',$arr_qlv_id,'tbl_lanhdaogiao_details');
        
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['dsphong']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_phongban');

        $data['file_dk']        = $this->Mdanhmuc->getWhereIn1('qlv_id',$arr_qlv_id,'qlvFile_id','tbl_lanhdaogiaofiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
        foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }

        foreach ($data['dsphong'] as $key => $value) {
            $data['mangdsphong'][$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'lanhdaogiao/Vlanhdaoduyet';
		$this->load->view('layout_admin/layout',$temp);
	}

    public function themDuLieu( $qlv_id){
        if(_post('luulai')){
            $data = array(
                "qlv_id"                => $qlv_id,
                "qlv_id_sub"            => 0,
                "qlvDetails_id_sub"     => 0,
                "coordinate_per"        => _post('coordinate_per'),
                "qlvDetails_date"       => date('Y-m-d H:i:s'),
                "qlvDetails_desc"       => _post('qlvDetails_desc'),
                "qlvDetails_limit_time" => date_insert(_post('qlvDetails_limit_time1')),
                "input_per"             => $this->_session['PK_iMaCB']
                 );
            $id_insert = $this->Mdanhmuc->themDuLieu2('tbl_lanhdaogiao_details',$data);
 
            $tongdauviec = _post('tong_dv_chitiet');
            for($i=1;$i<=$tongdauviec;$i++){
                $j=$i-1;
                
                if(_post('qlvDetails_limit_time')[$j] ==""){
                    $qlvDetails_limit_time = date_insert(_post('qlvDetails_limit_time1'));
                }else{
                    $qlvDetails_limit_time = date_insert(_post('qlvDetails_limit_time')[$j]);
                }

                $dulieu[]= array(
                "qlv_id"                => $qlv_id,
                "qlv_id_sub"            => $id_insert,
                "qlvDetails_id_sub"     => $id_insert,
                "coordinate_per"        => _post('coordinate_per'),
                "qlvDetails_date"       => date('Y-m-d H:i:s'),
                "qlvDetails_desc"       => _post('noidunghop_'.$i),
                "qlvDetails_limit_time" => $qlvDetails_limit_time,
                "department_id"         => _post('department_id')[$j],
                "main_department"       => _post('main_department')[$j],
                "input_per"             => $this->_session['PK_iMaCB'],
                "linhVuc_id"            => _post('linhVuc_id')[$j],
                "linhvuc_sub"           => _post('linhvuc_sub_'.$i)
                 );
            }
            $this->Mdanhmuc->themNhieuDuLieu('tbl_lanhdaogiao_details',$dulieu);
        }
    }


    public function getLinhvucSub()
    {
        $ma= _post('ma');
        $lv     = $this->Mdanhmuc->layDuLieu('id_parent',$ma,'tbl_linhvuc_qldv');
        echo json_encode($lv);exit();
    }
	

}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */