<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * appointment: cuộc hẹn
 */
class Ccvphthuchien extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_phoihop');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
	}
	public function index()
	{
        if(_post('luulai') > 0){
            $mangcbphong = array();
            $danhsachphong  = $this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
            foreach ($danhsachphong as $key => $value) {
                $mangcbphong[$value['PK_iMaCB']] = $value['sHoTen'];
            }

            $value = _post('luulai');
            
            $arr_nk = array(
                'nguoi_giao' => $mangcbphong[$this->_session['PK_iMaCB']] ,
                'nguoi_nhan' => $mangcbphong[$this->_session['PK_iMaCB']] ,
                'thoigian' => date('Y-m-d H:i:s',time()),
                'id_vanban' => $value
            );

            $this->Mdanhmuc->themDuLieu('tbl_lanhdaogiao_nhatky',$arr_nk);
            $this->hoanthanhVB($value);
            if($this->_session['iQuyenHan_DHNB'] == 8)redirect('dscvphxuly');
            if($this->_session['iQuyenHan_DHNB'] != 8)redirect('ldphchiaviec');
        }        
        
        $ma = $this->uri->segment(2);
        $data['fileketqua']     = $this->Mdanhmuc->layDuLieu('qlvDetails_id',$ma,'tbl_lanhdaogiaofilesketqua');


        $data['canbophoihop']     = $this->Mdanhmuc->laydulieu('FK_iMaQLDV',$ma,'tbl_chuyennhan_lanhdaogiao');

        $data['qldvDetail']     = $this->Mdanhmuc->laydulieu('qlvDetails_id',$ma,'tbl_lanhdaogiao_details');
        $qlv_id_arr = array();
        $qlvDetail_id_arr  = array();
        for($i=0;$i<count($data['qldvDetail']);$i++){
           array_push($qlv_id_arr,$data['qldvDetail'][$i]['qlv_id']);
           array_push($qlvDetail_id_arr,$data['qldvDetail'][$i]['qlvDetails_id']);
           $str_phongphoihop = $data['qldvDetail'][$i]['department_id']; 
           if($str_phongphoihop !=""){
                $arr_phongphoihop = explode(',', $str_phongphoihop);
                for($j=0;$j<count($arr_phongphoihop);$j++){
                  $data['qldvDetail'][$i]['phoihop'][$j]  = array($j => $arr_phongphoihop[$j]);
                }
           }
        }
        //tổng đầu việc giao phòng
        $tongviec = count($qlv_id_arr);
        $data['tongviec'] = $tongviec;
        if($tongviec < 1)$qlv_id_arr = array(0);
        
        $data['thongtin']       = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiao');//pr();

        $data['nhatky']       = $this->Mdanhmuc->getWhereInLanhdaogiao1('id_vanban',$qlvDetail_id_arr,'id','tbl_lanhdaogiao_nhatky');//pr($data['nhatky']);
       
        $data['dsloaivanban']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
        $data['dsphong']   = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_phongban');
        $data['file_dk']        = $this->Mdanhmuc->getWhereInLanhdaogiao('qlv_id',$qlv_id_arr,'qlv_id','tbl_lanhdaogiaofiles');
        $data['lanhdao']        = $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(3,4,5,6,7,8,9,10,11),'iQuyenHan_DHNB','tbl_canbo');
        $data['cbphong']        = $this->Mdanhmuc->layDuLieu2('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'iTrangThai',0,'tbl_canbo');
        $data['maphong'] = $this->_session['FK_iMaPhongHD'];
        
        foreach ($data['lanhdao'] as $key => $value) {
            $data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
        }
        

        foreach ($data['dsphong'] as $key => $value) {
            $data['mangdsphong'][$value['PK_iMaPB']] = $value['sTenPB'];
        }
        $data['main_depart']    = $this->Mketqua->layPhongBanDuThao();
        
		$data['title']    = 'Nhập mới đầu việc chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'lanhdaogiao/Vcvphthuchien';
		$this->load->view('layout_admin/layout',$temp);
	}

    public function hoanthanhVB($ma)
    {
        $taikhoan   = $this->_session['PK_iMaCB'];
        $quyen      = $this->_session['iQuyenHan_DHNB'];
        if($quyen==8)
        {
            $trangthai = 1; // chuyên viên
        }
        elseif($quyen==7){
            $trangthai = 2; // phó phòng
        }
        else{
            $trangthai = 3; // trưởng phòng
        }
        $chucvu     = $this->_session['FK_iMaCV'];
        $nguoigui   = $this->Mchuyennhan->layNguoiGui1($ma,$taikhoan);
        $name = clear($_FILES['file']['name']);
        $this->Mdanhmuc->setDuLieu('qlvDetails_id',$ma,'tbl_lanhdaogiaofilesketqua','lancuoi',1);
        $data_them=array(
            'qlvDetails_id'     => $ma,
            'qlvFile_path'      => ($name)?'lanhdaogiao_uploads_'.date('Y').'/'.time().'_'.clear($name):'',
            'qlvFile_date'      => date('Y-m-d H:i:s'),
            'qlvFile_desc'      => _post('noidunggiaiquyet'),
            'qlvFile_active'    => ($name)?1:0,
            'user_id'           => $this->_session['PK_iMaCB'],
            'department_id'     => $this->_session['FK_iMaPhongHD'],
            'iTrangThai'        => $trangthai,
            'phoihop'           => 2,
            'lancuoi'           => 1,
            'FK_iMaCB_Duyet'    => ($nguoigui)?$nguoigui['FK_iMaCB_Gui']:0
            );
        
        $this->Mchuyennhan->capnhatTrangThaiNguoiChuyen1($ma,$taikhoan);
        $kiemtra = $this->Mdanhmuc->themDuLieu('tbl_lanhdaogiaofilesketqua',$data_them);
        if($kiemtra>0)
        {
            $this->upload('lanhdaogiao_uploads_'.date('Y'),$name,'file');
            return messagebox('Bạn đã hoàn thành đầu việc','info');
        }
    }
    public function upload($dir,$name,$filename)
    {
        if(is_dir($dir)==false){
            mkdir($dir);        // Create directory if it does not exist
        }
        $config['upload_path']   = $dir;
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
        $config['overwrite']     = true;    
        $config['file_name']     = time().'_'.clear($name);
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload($filename);
    }
}

/* End of file Cnhapgiaymoi.php */
/* Location: ./application/controllers/vanbanden/Cnhapgiaymoi.php */