<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Example_api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Mdangnhap');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function users_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $user = NULL;

        if (!empty($users))
        {
            foreach ($users as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $user = $value;
                }
            }
        }

        if (!empty($user))
        {
            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function users_post()
    {
        $taikhoan = $this->post('taikhoan');
        $matkhau  = md5($this->post('matkhau'));
        $nam = $this->post('nam');
        if($nam > 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default_'.$nam, TRUE);
        }
        $result =$this->Mdangnhap->checkLogin($taikhoan,$matkhau);
        if(!empty($result))
        {
            $result1 = array(
                'PK_iMaCB' => $result['PK_iMaCB'],
                'sHoTen' => $result['sHoTen'],
                'FK_iMaPhongHD' => $result['FK_iMaPhongHD'],
                'FK_iMaCV' => $result['FK_iMaCV'],
                'sTaiKhoan' => $result['sTaiKhoan'],
                'iQuyenHan_DHNB' => $result['iQuyenHan_DHNB'],
                'iPhanMem' => $result['iPhanMem'],
                'iQuyenHan_TNVB' => $result['iQuyenHan_TNVB'],
                'iLoaiPhanMem' => $result['iLoaiPhanMem'],
                'iQuyenDB' => $result['iQuyenDB'],
                'ios' => 1
                );
            $session_data=array('vanban'=>$result1,'nam'=> $nam);
            $this->session->set_userdata($session_data);
            $set_session = array(
                'PK_iMaCB' => $result['PK_iMaCB'],
                'sHoTen' => $result['sHoTen'],
                'FK_iMaPhongHD' => $result['FK_iMaPhongHD'],
                'FK_iMaCV' => $result['FK_iMaCV'],
                'sTaiKhoan' => $result['sTaiKhoan'],
                'iQuyenHan_DHNB' => $result['iQuyenHan_DHNB'],
                'iPhanMem' => $result['iPhanMem'],
                'iQuyenHan_TNVB' => $result['iQuyenHan_TNVB'],
                'token'=> sha1(session_id())
            );
            $session_result=array('vanban' => $set_session);

        }else{
            $session_result = 'User could not be found';
        }
        $this->set_response($session_result, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }

    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
