<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Welcomejs extends REST_Controller {
    protected $_session;
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Vanbanden/Mdmadmin','Mdmadmin');
        $this->Mdmadmin = new Mdmadmin();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Mdangnhap','Mdangnhap');
        $this->Mdangnhap = new Mdangnhap();
        $this->load->model('thongtinnoibo/Msoanthaothongtin');
        $this->load->model('vanbandi/Mvanbandi');
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mdmadmin','Mdmadmin');
        $this->Mdmadmin = new Mdmadmin();
        $this->load->model('vanbandi/Mtruyennhan');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_phoihop');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
        $this->load->model('Vanbanden/Mvanbanphoihop');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    

            
    public function users_get()
    {
        // Users from a data store e.g. database

        $vanbans = [
            ['id' => '111not enough authority']
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($vanbans)
            {
                // Set the response and exit
                $this->response($vanbans, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $vanban = NULL;

        if (!empty($vanbans))
        {
            foreach ($vanbans as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $vanban = $value;
                }
            }
        }

        if (!empty($vanban))
        {
            $this->set_response($vanban, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function users_post()
    {
        $token = $this->post('token');
        $data_ss = $this->Mdangnhap->session_ci(sha1($token))['data'];
        $vanban = unserialize_session_data(strstr($data_ss,'vanban'))['vanban'];
        $year = unserialize_session_data(strstr($data_ss,'vanban'))['nam'];
        if($this->post('nam') >0)$year = $this->post('nam');
        if($year > 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default_'.$year, TRUE);
        }
        $taikhoan = $vanban['PK_iMaCB'];
        $phongban       = $vanban['FK_iMaPhongHD'];
        $quyen          = $vanban['iQuyenHan_DHNB'];
        $phongbanchicuc = $vanban['iQuyenDB'];
        if($phongban==12)
        {
            if($quyen==6 || $quyen==10)
            {
                $phongchicuc    = NULL;
            }
            else{
                $phongchicuc = $phongbanchicuc;
            }

        }else{
            $phongchicuc    = NULL;
        }
        $chucvu         = $vanban['FK_iMaCV'];
        $tinmoi      = $this->Msoanthaothongtin->layVanBanMoiNhan($vanban['PK_iMaCB'],0);
        $tindaxem   = $this->Msoanthaothongtin->demDuLieu($vanban['PK_iMaCB'],1);
        $tindagui   = $this->Mdanhmuc->layDuLieu('FK_iMaCB_Nhap',$vanban['PK_iMaCB'],'tbl_vanban');
        $week = (int)date("W", strtotime(date('Y-m-d')));
        $month = (int)date('m');
        
        if($month == 1 || $month == 2 || $month == 3)$quy = 1;
        if($month == 4 || $month == 5 || $month == 6)$quy = 2;
        if($month == 7 || $month == 8 || $month == 9)$quy = 3;
        if($month == 10 || $month == 11 || $month == 12)$quy = 4;
        // văn bản đi sửa ngày 03/04/2018
        $vanbandichoxuly = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,1);
        $vanbandidaxuly  = $this->Mvanbandi->demVBDi_DaXuLy($taikhoan,2);
        $vanbandi_phong  = $this->Mvanbandi->demDSVB_Phong($phongban,1);
        $giaymoidi_phong = $this->Mvanbandi->demDSVB_Phong($phongban,10);
        $vanbanditrinhky = $this->Mvanbandi->demVBDi_DaTrinhKy($taikhoan);
        // yêu cầu đề xuất phối hợp 20/04/2018
        $dexuatphoihop     = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,1);
        $dexuatphoihopxuly = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,2);
        /** @var Đầu việc $Quyen */
        $dauvieclanhdaochoxuly  = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauvieclanhdaodaxuly   = $this->Mchuyennhan->demDauViecDaXuLy($taikhoan,2,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecchutrichoxuly   = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecchutriphchoxuly = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecphoihopchoxuly  = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecchutridaxuly    = $this->Mchuyennhan->demDauViecDaXuLy($taikhoan,2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecphongchutri    = $this->Mchuyennhan->demDauViecCuaPhong($phongban,1,$phongchicuc);
        $dauviecphongphoihop    = $this->Mchuyennhan->demDauViecCuaPhong($phongban,2,$phongchicuc);
        $dauviecchoduyet       = $this->Mchuyennhan->demDauViecChoDuyet($taikhoan);
        $dauviectralai         = $this->Mchuyennhan->demDauViecTraLai($taikhoan);
        $week = (int)date("W", strtotime(date('Y-m-d')));
        // tổng văn vản quá hạn
        if($vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5){
            $FK_iMaPhongHD = NULL;
            $tong_cho_ldduyet = $this->Mdanhmuc->get_kehoach7('lanhdao_id ='.$vanban['PK_iMaCB'].'  ','kehoachtrinhld','kh_id');
        }else{
            $FK_iMaPhongHD = $vanban['FK_iMaPhongHD'];
            $tong_cho_ldduyet = $this->Mdanhmuc->get_kehoach7('(phong_id ='.$vanban['FK_iMaPhongHD'].'  or phong_ph like "%'.$vanban['FK_iMaPhongHD'].'%" ) ','kehoachtrinhld','kh_id');

        }
        $tongvbquahan = count($this->Mdanhmuc->getDocCT(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,$FK_iMaPhongHD,NULL,$vanban['PK_iMaCB'],$vanban['iQuyenHan_DHNB']));

// thong bao của vp cho văn phòng điện tử
        $data['thongbao'] = $this->Mdanhmuc->layDuLieu2_2('iTrangThai !=',1,'sNgayBatDau >= ',date('Y-m-d'),'sNgayKetThuc <= ',date('Y-m-d'),'tbl_thongbao');
        $thongbao_vpdt ="";
        if(count($data['thongbao']) <=0)$thongbao_vpdt ="";
        if(count($data['thongbao']) ==1 ){
            $thongbao_vpdt =$data['thongbao'][0]['sNoiDung'];
        }
        if(count($data['thongbao']) > 1 ){
            $thongbao_vpdt =$data['thongbao'][0]['sNoiDung'] ."; ".$data['thongbao'][1]['sNoiDung'];
            
        }
//        pr($vanban);
        if(!empty($vanban))
        {
            /** CHANH VAN PHONG **/
            if($vanban['iQuyenHan_DHNB'] == 3){
                // văn bản chưa phân loại
                $plvb = $this->Mdmadmin->getDocAwait('2');
                $plvbctd = $this->Mdmadmin->getDocAwaitCTD('2');
                // văn bản đã phân loại
                $docawaitdir = $this->Mdmadmin->getDocDirAwait('2');
                // văn bản chuyển lại
                $vbcl = $this->Mdmadmin->getDocReject();
                // phòng chủ trì
                $pct = $this->Mdmadmin->getDocAwait('5',NULL,$vanban['FK_iMaPhongHD']);
                // phong phối hợp
                $pph = $this->Mdmadmin->getDocAwaitPPH(3,$vanban['FK_iMaPhongHD'],NULL,'2','5','0');
                $pph = $pph;
                $phoihopxl = $this->Mdmadmin->getDocAwaitPPHDXL(3,$vanban['FK_iMaPhongHD'],NULL,'1','5',1);
                $phdxl = $phoihopxl;
                // văn bản phòng đã xử lý
//            $data['pdxy'] = count($this->Mvanbanchoxuly->getDocComplete($vanban['FK_iMaPhongHD']));
                $vanbanchidao = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB']);
                // phân loại giấy mời
                $plgm = $this->Mdmadmin->getDocAwait('2',NULL,NULL,NULL,'1');
                // giấy mười đã phân loại
                $gmdpl = $this->Mdmadmin->getDocDirAwait('1');
                // giấy mời chờ xử lý
                $gmcxl = $this->Mdmadmin->getDocAwait1('5',NULL,$vanban['FK_iMaPhongHD'],NULL,'1');
                // giấy mời phối hợp chờ xử lý
                $gmphcxl = $this->Mdmadmin->getDocAwaitPPH(3,$vanban['FK_iMaPhongHD'],NULL,'1','5',0);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,NULL,1);
                $gmphcxl = $gmphcxl;
                // giấy mời đã xử lý
                $gmdxl = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB'],NULL,'1');
//            pr($data['gmdxl']);
                $vbqt = $this->Mdmadmin->getVBQT($vanban['PK_iMaCB']);
                $vbqtdgq = $this->Mdmadmin->getVBQTGQ($vanban['PK_iMaCB']);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $vbcduyet = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB']);
                $dschutri = $this->Mdmadmin->getDocCT($vanban['FK_iMaPhongHD']);
                $tuchoigm = $this->Mdmadmin->PPPHTuChoi($vanban['FK_iMaPhongHD'],1);
                $vbduyetgiaymoi = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB'],1);
                $dsphoihop = $this->Mdmadmin->getDocAwaitDSCTPPH($vanban['FK_iMaPhongHD']);
                $tocongtac = $this->Mdmadmin->getDocAwaitTCT('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
                $tuchoipp = $this->Mdmadmin->PPPHTuChoi($vanban['FK_iMaPhongHD']);
                $bcchoduyet = $this->Mdmadmin->getDuyetBaoCao(NULL,$vanban['FK_iMaPhongHD']);
                $dsgiahan = $this->Mdmadmin->dsGiaHan($vanban['PK_iMaCB']);
                $congtacdang = $this->Mdmadmin->getCTD('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL);


                /** @var văn bản đi $Quyen */
                $taikhoan       = $vanban['PK_iMaCB'];
                $chucvu         = $vanban['FK_iMaCV'];
                $quyen          = $vanban['iQuyenHan_DHNB'];
                $phongban       = $vanban['FK_iMaPhongHD'];
                $phongbanchicuc = $vanban['iQuyenDB'];
                $trangthaikq    = 6;
                $phongchicuc    = NULL;
                $canbo          = '';// không ai cả
                $trangthaikq = 1;
                $trangthai_tk       = 5;
                $trangthai =3; #TP
                $trangthaiduyet = 2;
                $ten_taikhoan       = 'truongphong';
                $ykien              = 'ykien_tp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';


                $trangthai2 =999; #TP
                $ten_taikhoan1       = 'chanhvanphong';
                $ykien1              = 'ykien_cvp';
                $ten_trangthai_xem1  = 'trangthai_xem_cvp';
                $trangthai_xem1      = 1;
                $chude1              = '';
                $vbdichoxulycvp    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan1,(int)$taikhoan,$ykien1,$ten_trangthai_xem1,$trangthai_xem1,$chude1);
                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly    = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $duyetketqua            = $this->Mketqua->demTTKT($trangthaiduyet,$phongban,$taikhoan);
                $tong_sl_cv = $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND ( active = 1 or active = 2 or  active = 4) and phong_id = ".$vanban['FK_iMaPhongHD']." and tuan =". $week);

                $tonglanhdaogiao = $this->Mdanhmuc->countphongthuly($vanban['FK_iMaPhongHD']);

                $tongphlanhdaogiao = $this->Mdanhmuc->countphongphoihop($vanban['FK_iMaPhongHD']);
                
                $Quyen = array(
                    'I' => array(
                        '1' => array(
                            'TieuDe' => 'Phân loại văn bản',
                            'Link' => base_url().'vanbanchoxuly',
                            'SoLuong' => $plvb,
                            'icon' => 'fa fa-sitemap',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Phân loại văn bản công tác Đảng',
                            'Link' => base_url().'congtacdang_cvp',
                            'SoLuong' => $plvbctd,
                            'icon' => 'fa fa-sitemap'
                        ),
                        '3' => array(
                            'TieuDe' => 'Văn bản đã phân loại (lãnh đạo chưa xử lý)',
                            'Link' => base_url().'vanbandaphanloai',
                            'SoLuong' => $docawaitdir,
                            'icon' => 'fa fa-sitemap'
                        ),
                        '4' => array(
                            'TieuDe' => 'Văn bản các phòng chuyển lại',
                            'Link' => base_url().'vanbanchuyenlai',
                            'SoLuong' => $vbcl,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '5' => array(
                            'TieuDe' => 'Danh sách văn bản quá hạn của phòng',
                            'Link' => base_url().'dsvanbanquahan',
                            'SoLuong' => $tongvbquahan,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'DS văn bản giao phòng xử lý',
                            'SoLuong' => $pct+$tocongtac+$pph,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'VB giao phòng chủ trì chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_tp',
                                    'SoLuong2' => $pct,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'VB tổ công tác chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_tct',
                                    'SoLuong2' => $tocongtac,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                    'Link2' => base_url().'congtacdang_tp',
                                    'SoLuong2' => $congtacdang,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '4' => array(
                                    'TieuDe2' => 'VB giao phòng phối hợp chờ xử lý',
                                    'Link2' => base_url().'phongphoihop',
                                    'SoLuong2' => $pph,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '7' => array(
                            'TieuDe' => ' DS công việc lãnh đạo sở giao:',
                            'SoLuong' => $tonglanhdaogiao+$tongphlanhdaogiao,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => ' Danh sách công việc lãnh đạo sở giao phòng',
                                    'Link2' => base_url().'tpchiaviec',
                                    'SoLuong2' => $tonglanhdaogiao,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => ' DS công việc lãnh đạo sở giao phòng phối hợp',
                                    'Link2' => base_url().'tpphchiaviec',
                                    'SoLuong2' => $tongphlanhdaogiao,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => ' DS công việc LĐ Sở giao phòng đang thực hiện',
                                    'Link2' => base_url().'dsphongdangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '4' => array(
                                    'TieuDe2' => ' DS công việc phối hợp LĐ Sở giao phòng đang thực hiện',
                                    'Link2' => base_url().'tpphdangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '8' => array(
                            'TieuDe' => 'DS văn bản đã chỉ đạo',
                            'Link' => base_url().'vanbandachidao',
                            'SoLuong' => $vanbanchidao,
                            'icon' => 'fa fa-newspaper-o',
                        ),
                        '9' => array(
                            'TieuDe' => 'DSVB đã hoàn thành chờ LĐ phòng phê duyệt',
                            'Link' => base_url().'vanbanchopheduyet',
                            'SoLuong' => $vbcduyet,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '10' => array(
                            'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                            'Link' => base_url().'dsdexuatgiahan',
                            'SoLuong' => $dsgiahan,
                            'icon' => 'fa fa-clock-o',
                            'thongbao' => 1
                        ),
                        '11' => array(
                            'TieuDe' => 'Văn bản phối hợp trả lại',
                            'Link' => base_url().'phoihopchuyenlai_tp',
                            'SoLuong' => $tuchoipp,
                            'icon' => 'fa fa-reply',
                            'thongbao' => 1
                        ),
                        '12' => array(
                            'TieuDe' => 'DSVB quan trọng của Sở',
                            'SoLuong' => $vbqt + $vbqtdgq,
                            'icon' => 'fa fa-list-alt',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở chưa hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong',
                                    'SoLuong2' => $vbqt,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở đã hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong?cacloaivanban=2',
                                    'SoLuong2' => $vbqtdgq,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '13' => array(
                            'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                            'SoLuong' => $dschutri + $dsphoihop,
                            'icon' => 'fa fa-list-alt',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                    'Link2' => base_url().'dsphongchutri',
                                    'SoLuong2' => $dschutri,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                    'Link2' => base_url().'dsphoihop',
                                    'SoLuong2' => $dsphoihop,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '14' => array(
                            'TieuDe' => 'Tổng số lượng đơn thư tố cáo giao phòng xử lý',
                            'SoLuong' => $dsdonthu + $dsdonthuht,
                            'icon' => 'fa fa-list-alt',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                    'Link2' => base_url().'dsdonthu',
                                    'SoLuong2' => $dsdonthu,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                    'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                    'SoLuong2' => $dsdonthuht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '15' => array(
                            'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                            'Link' => base_url().'dexuatphoihop_tp',
                            'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '16' => array(
                            'TieuDe' => 'Công việc phối hợp chờ xử lý',
                            'Link' => base_url().'dexuatphoihop_tp_ph',
                            'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        )
                    ),
                    'II' => array(
                        '1' => array(
                            'TieuDe' => 'Giấy mời chờ phân loại',
                            'Link' => base_url().'giaymoi_cvp',
                            'SoLuong' => $plgm,
                            'icon' => 'fa fa-sitemap',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Giấy mời đã phân loại',
                            'Link' => base_url().'giaymoidaphanloai',
                            'SoLuong' => $gmdpl,
                            'icon' => 'fa fa-sitemap'
                        ),
                        '3' => array(
                            'TieuDe' => ' Xem lịch công tác của LĐ Sở',
                            'Link' => base_url().'lichcongtac',
                            'SoLuong' => '',
                            'icon' => 'fa fa-calendar'
                        ),
                        '4' => array(
                            'TieuDe' => 'DS giấy mời giao phòng xử lý',
                            'SoLuong' => $gmcxl+$gmphcxl,
                            'icon' => 'fa fa-file-text-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Giấy mời phòng chủ trì chờ xử lý',
                                    'Link2' => base_url().'giaymoi_tp',
                                    'SoLuong2' => $gmcxl,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Giấy mời phòng phối hợp chờ xử lý',
                                    'Link2' => base_url().'giaymoiphoihop',
                                    'SoLuong2' => $gmphcxl,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '5' => array(
                            'TieuDe' => 'DS giấy mời đã chỉ đạo',
                            'SoLuong' => $gmdxl + $phdxl,
                            'icon' => 'fa fa-file-text-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Giấy mời giao phòng chủ trì đã chỉ đạo',
                                    'Link2' => base_url().'giaymoidaxuly',
                                    'SoLuong2' => $gmdxl,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Giấy mời giao phòng phối hợp đã chỉ đạo',
                                    'Link2' => base_url().'giaymoiphoihopdaxuly',
                                    'SoLuong2' => $phdxl,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '6' => array(
                            'TieuDe' => 'DSGM đã hoàn thành chờ LĐ phòng phê duyệt',
                            'Link' => base_url().'giaymoichoduyet',
                            'SoLuong' => $vbduyetgiaymoi,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '7' => array(
                            'TieuDe' => 'Giấy mời phối hợp trả lại',
                            'Link' => base_url().'giaymoiphoihopchuyenlai_tp',
                            'SoLuong' => $tuchoigm,
                            'icon' => 'fa fa-reply',
                            'thongbao' => 1
                        ),
                        '8' => array(
                            'TieuDe' => 'Xem lịch công tác của phòng',
                            'Link' => base_url().'lichcongtac',
                            'SoLuong' => '',
                            'icon' => 'fa fa-calendar'
                        ),
                        '9' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                            'Link' => base_url().'dssoanthaodagui',
                            'SoLuong' => $dagui,
                            'icon' => 'fa fa-paper-plane-o'
                        ),
                        '10' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                            'Link' => base_url().'dsgiaymoiketluanmoi',
                            'SoLuong' => $chuaxem,
                            'icon' => 'fa fa-flag',
                            'thongbao' => 1
                        ),
                        '11' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                            'Link' => base_url().'dsgiaymoiketluandaxem',
                            'SoLuong' => $daxem,
                            'icon' => 'fa fa-flag-o'
                        )
                    ),
                    'III' => array(
                        '1' => array(
                            'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                            'Link' => base_url().'vanbandichoxuly_tp',
                            'SoLuong' => $vanbandichoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Văn bản chủ trì đã xử lý',
                            'Link' => base_url().'vanbandidaxuly_tp',
                            'SoLuong' => $vanbandidaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'DS Văn bản của phòng',
                            'Link' => base_url().'vanbanphongban',
                            'SoLuong' => $vanbandi_phong,
                            'icon' => 'fa fa-list-alt'
                        ),
                        '4' => array(
                            'TieuDe' => 'DS Giấy mời của phòng',
                            'Link' => base_url().'giaymoiphongban',
                            'SoLuong' => $giaymoidi_phong,
                            'icon' => 'fa fa-list-alt'
                        ), 
                        '5' => array(
                            'TieuDe' => 'Nhập văn bản đi',
                            'Link' => base_url().'vanban',
                            'SoLuong' => '',
                            'icon' => 'fa fa-list-alt'
                        )

                    ),
                    'IV' => array(
                        '1' => array(
                            'TieuDe' => 'Thông tin mới',
                            'Link' => base_url().'thongtinmoi',
                            'SoLuong' => count($tinmoi),
                            'icon' => 'fa fa-info-circle',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Thông tin đã gửi đi',
                            'Link' => base_url().'thongtinguidi',
                            'SoLuong' => count($tindagui),
                            'icon' => 'fa fa-info-circle'
                        ),
                        '3' => array(
                            'TieuDe' => 'Thông tin đã xem',
                            'Link' => base_url().'thongtindaxem',
                            'SoLuong' => $tindaxem,
                            'icon' => 'fa fa-info-circle'
                        ),
                        '4' => array(
                            'TieuDe' => 'Soạn thảo thông tin',
                            'Link' => base_url().'soanthaothongtin',
                            'SoLuong' => '',
                            'icon'=> 'fa fa-edit'
                        )

                    ),
                    'V' => array(
                        '1' => array(
                            'TieuDe' => 'Đầu việc phòng chủ trì chờ xử lý',
                            'Link' => base_url().'dauviecchoxuly_tp',
                            'SoLuong' => $dauviecchutrichoxuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Đầu việc phòng phối hợp chờ xử lý',
                            'Link' => base_url().'dauviecchoxulyph_tp',
                            'SoLuong' => $dauviecphoihopchoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Đầu việc đã chỉ đạo (Chưa hoàn thành)',
                            'Link' => base_url().'dauviecdaxuly_tp',
                            'SoLuong' => $dauviecchutridaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Đầu việc phòng chủ trì',
                            'Link' => base_url().'dsdauviecphongchutri',
                            'SoLuong' => $dauviecphongchutri,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Đầu việc phòng phối hợp',
                            'Link' => base_url().'dsdauviecphongphoihop',
                            'SoLuong' => $dauviecphongphoihop,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Đầu việc hoàn thành chờ lãnh đạo phòng duyệt',
                            'Link' => base_url().'duyetketqua',
                            'SoLuong' => $duyetketqua,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Đầu việc chờ phê duyệt gửi lãnh đạo Sở',
                            'Link' => base_url().'dauviecchoduyet',
                            'SoLuong' => $dauviecchoduyet,
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VI' => array(
                        '1' => array(
                                'TieuDe' => 'Nhập kế hoạch-công tác phòng trình LĐ sở',
                                'Link' => base_url().'trinhlanhdao',
                                'SoLuong' => $tong_sl_cv,
                                'icon' => 'fa fa-newspaper-o'
                                ),
                            '2' => array(
                                'TieuDe' => ' Nhận xét, chấm điểm công việc tuần của cấp dưới',
                                'Link' => base_url().'dskehoachcongtac/'.$week,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => ' Phân loại công chức của phòng theo tháng',
                                'Link' => base_url().'dskehoachcongtacthang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => ' Trưởng phòng tự đánh giá, chấm điểm tháng của cá nhân',
                                'Link' => base_url().'xemkehoachcongtac_lanhdao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => ' Xem kết quả phân loại của các trưởng phòng',
                                'Link' => base_url().'dskehoachcongtacthangtp',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => ' Xem kết quả công tác tuần của các phòng trong sở',
                                'Link' => base_url().'dskehoachcongtactuanld',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => ' Thống kê kết quả công tác tuần trong toàn sở',
                                'Link' => base_url().'thongkebaocao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '8' => array(
                                'TieuDe' => ' Thống kê kết quả công tác tháng trong toàn sở',
                                'Link' => base_url().'thongkebaocaothang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '9' => array(
                            'TieuDe' => 'Đánh giá, phê duyệt kết quả  phân loại tháng của sở',
                            'Link' => base_url().'lanhdaonhanxetthang',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )

                );
                $result = $Quyen;

            }
            /** GIAM DOC **/
            if($vanban['iQuyenHan_DHNB'] == 4){
                $month1 = (int)date('m') - 1;
                $vbcxlcvp = $this->Mdmadmin->getDocAwait('3');
                $plvbctd = $this->Mdmadmin->getDocAwaitCTD('3');
                $dstocongtac = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB']);
                $dstocongtac_ht = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB'],2);
                $vbqt = $this->Mdmadmin->getVBQT($vanban['PK_iMaCB']);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,NULL,1);
                $vbqtdgq = $this->Mdmadmin->getVBQTGQ($vanban['PK_iMaCB']);
//            pr($data['vbqt']);
                $gmcxl = $this->Mdmadmin->getDocAwait('3',NULL,NULL,NULL,'1');
                $gmdxl = $this->Mdmadmin->getDocGDAwait($vanban['PK_iMaCB']);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $dsgiahan = $this->Mdmadmin->dsGiaHan($vanban['PK_iMaCB']);
                $vanbandaxuly = $this->Mdmadmin->getDocGoGDAwait($vanban['PK_iMaCB']);
                $vanbandaxuly_stcph = $this->Mdmadmin->getDocGoGDAwait($vanban['PK_iMaCB'],1);

                $trangthai_tk       = 1;
                $trangthai =7;
                $ten_taikhoan       = 'giamdoc';
                $ykien              = 'ykien_gd';
                $ten_trangthai_xem  = 'trangthai_xem_gd';
                $trangthai_xem      = 1;
                $chude              = '';

                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);

                $dggiaochoduyet = $this->Mdanhmuc->countlanhdaogiao($vanban['PK_iMaCB']);             
                $Quyen = array(
                    'I' => array(
                        '1' => array(
                            'TieuDe' => 'Danh sách văn bản quá hạn do lãnh đạo chỉ đạo',
                            'Link' => base_url().'dsvanbanquahan/0/'.$vanban['PK_iMaCB'],
                            'SoLuong' => $tongvbquahan,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'DSVB chờ Lãnh đạo xử lý',
                            'SoLuong' => (int)$vbcxlcvp+(int)$plvbctd,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Văn bản đến chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_gd',
                                    'SoLuong2' => $vbcxlcvp,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Văn bản công tác Đảng chờ xử lý',
                                    'Link2' => base_url().'congtacdang_gd',
                                    'SoLuong2' => $plvbctd,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '3' => array(
                            'TieuDe' => ' Danh sách đầu việc do lãnh đạo chỉ đạo chờ duyệt ',
                            'Link' => base_url().'dslanhdaoduyet',
                            'SoLuong' => $dggiaochoduyet,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '4' => array(
                            'TieuDe' => 'DS văn bản đã chỉ đạo',
                            'SoLuong' => (int)$vanbandaxuly+(int)$vanbandaxuly_stcph,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Văn bản Lãnh đạo đã chỉ đạo',
                                    'Link2' => base_url().'vanbandaxuly_gd',
                                    'SoLuong2' => $vanbandaxuly,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Văn bản STC phối hợp đã chỉ đạo',
                                    'Link2' => base_url().'vanbandaxulystcph_gd',
                                    'SoLuong2' => $vanbandaxuly_stcph,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '5' => array(
                            'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                            'Link' => base_url().'dsdexuatgiahan',
                            'SoLuong' => $dsgiahan,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '6' => array(
                            'TieuDe' => 'Tổng số lượng Tổ công tác giao phòng xử lý',
                            'SoLuong' => (int)$dstocongtac+(int)$dstocongtac_ht,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                    'Link2' => base_url().'dstocongtac',
                                    'SoLuong2' => $dstocongtac,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                    'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                    'SoLuong2' => $dstocongtac_ht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '7' => array(
                            'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao phòng xử lý',
                            'SoLuong' => (int)$dsdonthu+(int)$dsdonthuht,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                    'Link2' => base_url().'dsdonthu',
                                    'SoLuong2' => $dsdonthu,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                    'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                    'SoLuong2' => $dsdonthuht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '8' => array(
                            'TieuDe' => 'DSVB quan trọng của Sở',
                            'SoLuong' => (int)$vbqt+(int)$vbqtdgq,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở chưa hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong',
                                    'SoLuong2' => $vbqt,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở đã hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong?cacloaivanban=2',
                                    'SoLuong2' => $vbqtdgq,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        )
                    ),
                    'II' => array(
                        '1' => array(
                            'TieuDe' => 'Giấy mời chờ xử lý',
                            'Link' => base_url().'giaymoi_gd',
                            'SoLuong' => $gmcxl,
                            'icon' => 'fa fa-file-text-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Giấy mời đã xử lý',
                            'Link' => base_url().'giaymoidaxuly_gd',
                            'SoLuong' => $gmdxl,
                            'icon' => 'fa fa-file-text-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Xem lịch công tác',
                            'Link' => base_url().'lichcongtac?thoigian=1',
                            'SoLuong' => '',
                            'icon' => 'fa fa-calendar'
                        ),
                        '4' => array(
                            'TieuDe' => 'Danh sách giấy mời đến của sở',
                            'Link' => base_url().'dsgiaymoi',
                            'SoLuong' => '',
                            'icon' => ''
                        ),
                        '5' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                            'Link' => base_url().'dssoanthaodagui',
                            'SoLuong' => $dagui,
                            'icon' => 'fa fa-share-square-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                            'Link' => base_url().'dsgiaymoiketluanmoi',
                            'SoLuong' => $chuaxem,
                            'icon' => 'fa fa-flag',
                            'thongbao' => 1
                        ),
                        '7' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                            'Link' => base_url().'dsgiaymoiketluandaxem',
                            'SoLuong' => $daxem,
                            'icon' => 'fa fa-flag-o'
                        )
                    ),
                    'III' => array(
                        '1' => array(
                            'TieuDe' => 'Văn bản trình ký',
                            'Link' => base_url().'vanbandichoxuly_ld',
                            'SoLuong' => $vanbandichoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Lịch họp của sở chủ trì',
                            'Link' => base_url().'lichhoplanhdao',
                            'SoLuong' => '',
                            'icon' => 'fa fa-list-alt'
                        )
                    ),
                    'IV' => array(
                        '1' => array(
                            'TieuDe' => 'Thông tin mới',
                            'Link' => base_url().'thongtinmoi',
                            'SoLuong' => count($tinmoi),
                            'icon' => 'fa fa-info-circle',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Thông tin đã gửi đi',
                            'Link' => base_url().'thongtinguidi',
                            'SoLuong' => count($tindagui),
                            'icon' => 'fa fa-info-circle'
                        ),
                        '3' => array(
                            'TieuDe' => 'Thông tin đã xem',
                            'Link' => base_url().'thongtindaxem',
                            'SoLuong' => $tindaxem,
                            'icon' => 'fa fa-info-circle'
                        ),
                        '4' => array(
                            'TieuDe' => 'Soạn thảo thông tin',
                            'Link' => base_url().'soanthaothongtin',
                            'SoLuong' => '',
                            'icon' => 'fa fa-edit'
                        )

                    ),
                    'V' => array(
                        '1' => array(
                            'TieuDe' => 'Đầu việc chờ xử lý',
                            'Link' => base_url().'dauviecchoxuly_gd',
                            'SoLuong' => $dauvieclanhdaochoxuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Đầu việc đã xử lý (Chưa hoàn thành)',
                            'Link' => base_url().'dauviecdaxuly_gd',
                            'SoLuong' => $dauvieclanhdaodaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Danh sách đầu việc của Sở',
                            'Link' => base_url().'dauviecdaxuly_tp',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Danh sách tài liệu họp HĐND của tổng hợp',
                            'Link' => base_url().'danhsachtailieu',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Danh sách tài liệu họp HĐND của phòng',
                            'Link' => base_url().'danhsachtailieuphong',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Danh sách kiến nghị HĐND',
                            'Link' => base_url().'danhsachkiennghi',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VI' => array(
                        '1' => array(
                            'TieuDe' => ' GĐ sở tự nhận xét đánh giá kết quả tháng',
                            'Link' => base_url().'xemkehoachcongtac_GD/519/'.$month1,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => ' Xem kế hoạch, kết quả công tác tuần của các phòng',
                            'Link' => base_url().'dskehoachcongtactuanld',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Xem kết quả phân loại tháng của các phòng',
                            'Link' => base_url().'dskehoachcongtacthangld',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => ' Đánh giá, chấm điểm, phân loại PGĐ, trưởng phòng',
                            'Link' => base_url().'dskehoachcongtacthangtp',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Thống kê kết quả công tác tuần trong toàn sở',
                            'Link' => base_url().'thongkebaocao',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Thống kê kết quả công tác Tháng trong toàn sở',
                            'Link' => base_url().'thongkebaocaothang',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Đánh giá, phê duyệt kết quả  phân loại tháng của sở',
                            'Link' => base_url().'lanhdaonhanxetthang',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '8' => array(
                            'TieuDe' => 'DS cán bộ tham mưu, đề xuất sáng tạo đổi mới trong tháng',
                            'Link' => base_url().'dssangtao',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )

                );
                $result = $Quyen;
            }
            /** PHO GIAM DOC **/
            if($vanban['iQuyenHan_DHNB'] == 5){
                $month1 = (int)date('m') - 1;
                $vbcxlcvp = $this->Mdmadmin->getDocAwait('4',$vanban['PK_iMaCB']);
                $plvbctd = $this->Mdmadmin->getDocAwaitCTD('4',$vanban['PK_iMaCB']);
                $theodoivanban = $this->Mdmadmin->getDocAwait('5',$vanban['PK_iMaCB'],NULL,NULL,NULL,'1');
                $vbqt = $this->Mdmadmin->getVBQT($vanban['PK_iMaCB']);
                $dstocongtac = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB']);
                $dstocongtac_ht = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB'],2);
                // danh sách đơn thư chưa ht - hoàn thành
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,NULL,1);
                //văn bản quan trọng
                $vbqtdgq = $this->Mdmadmin->getVBQTGQ($vanban['PK_iMaCB']);
                //giấy mời chờ xử lý
                $gmcxl = $this->Mdmadmin->getDocAwait('4',$vanban['PK_iMaCB'],NULL,NULL,'1');
                $gmdxl = $this->Mdmadmin->getDocGDAwait($vanban['PK_iMaCB']);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $dsgiahan = $this->Mdmadmin->dsGiaHan($vanban['PK_iMaCB']);
                $vanbandaxuly = $this->Mdmadmin->getDocGoGDAwait($vanban['PK_iMaCB']);
                $vanbandaxuly_stcph = $this->Mdmadmin->getDocGoGDAwait($vanban['PK_iMaCB'],1);
                $getDocAwaitPH = $this->Mdmadmin->getDocAwaitBGD($vanban['FK_iMaPhongHD'],NULL,$vanban['PK_iMaCB']);
                $getAppoAwaitPH = $this->Mdmadmin->getDocAwaitBGD($vanban['FK_iMaPhongHD'],1,$vanban['PK_iMaCB']);
                $getDocSeenBGD = $this->Mdmadmin->getDocSeenBGD($this->_session['FK_iMaPhongHD'],$vanban['PK_iMaCB']);
                $getDocSeenBGD_GM = $this->Mdmadmin->getDocSeenBGD($this->_session['FK_iMaPhongHD'],$vanban['PK_iMaCB'],1);

                $trangthai_tk       = 2;
                $trangthai =6;
                $ten_taikhoan       = 'phogiamdoc';
                $ykien              = 'ykien_pgd';
                $ten_trangthai_xem  = 'trangthai_xem_pgd';
                $trangthai_xem      = 1;
                $chude              = '';

                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);

                $dggiaochoduyet = $this->Mdanhmuc->countlanhdaogiao($vanban['PK_iMaCB']);

                $Quyen = array(
                    'I' => array(
                        '1' => array(
                            'TieuDe' => 'Danh sách văn bản quá hạn do lãnh đạo chỉ đạo',
                            'Link' => base_url().'dsvanbanquahan/0/'.$vanban['PK_iMaCB'],
                            'SoLuong' => $tongvbquahan,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'DSVB chờ Lãnh đạo xử lý',
                            'SoLuong' => $vbcxlcvp + $plvbctd+$getDocAwaitPH,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Văn bản đến chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_pgd',
                                    'SoLuong2' => $vbcxlcvp+$getDocAwaitPH,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Văn bản công tác Đảng chờ xử lý',
                                    'Link2' => base_url().'congtacdang_pgd',
                                    'SoLuong2' => $plvbctd,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '3' => array(
                            'TieuDe' => ' Danh sách đầu việc lãnh đạo đôn đốc, chỉ đạo',
                            'Link' => base_url().'dslanhdaoduyet',
                            'SoLuong' => $dggiaochoduyet,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '4' => array(
                            'TieuDe' => 'DS văn bản đã chỉ đạo',
                            'SoLuong' => $vanbandaxuly + $vanbandaxuly_stcph+$getDocSeenBGD,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Văn bản Lãnh đạo đã chỉ đạ',
                                    'Link2' => base_url().'vanbandaxuly_gd',
                                    'SoLuong2' => $vanbandaxuly,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Văn bản STC phối hợp đã chỉ đạo',
                                    'Link2' => base_url().'vanbandaxulystcph_gd',
                                    'SoLuong2' => $vanbandaxuly_stcph,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => 'DSVB xem để biết',
                                    'Link2' => base_url().'dsvb_bgd',
                                    'SoLuong2' => $getDocSeenBGD,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '5' => array(
                            'TieuDe' => 'DSVB Phó giám đốc biết để đôn đốc',
                            'Link' => base_url().'vanbantheodoi',
                            'SoLuong' => $theodoivanban,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '6' => array(
                            'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                            'Link' => base_url().'dsdexuatgiahan',
                            'SoLuong' => $dsgiahan,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '7' => array(
                            'TieuDe' => 'Tổng số lượng Tổ công tác giao phòng xử lý',
                            'SoLuong' => $dstocongtac + $dstocongtac_ht,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                    'Link2' => base_url().'dstocongtac',
                                    'SoLuong2' => $dstocongtac,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                    'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                    'SoLuong2' => $dstocongtac_ht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '8' => array(
                            'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao phòng xử lý',
                            'SoLuong' => $dsdonthu + $dsdonthuht,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                    'Link2' => base_url().'dsdonthu',
                                    'SoLuong2' => $dsdonthu,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                    'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                    'SoLuong2' => $dsdonthuht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '9' => array(
                            'TieuDe' => 'DSVB quan trọng của Sở',
                            'SoLuong' => $vbqt + $vbqtdgq,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở chưa hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong',
                                    'SoLuong2' => $vbqt,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở đã hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong?cacloaivanban=2',
                                    'SoLuong2' => $vbqtdgq,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        )
                    ),
                    'II' => array(
                        '1' => array(
                            'TieuDe' => 'Giấy mời chờ Lãnh đạo xử lý',
                            'Link' => base_url().'giaymoi_pgd',
                            'SoLuong' => $gmcxl+$getAppoAwaitPH,
                            'icon' => 'fa fa-file-text-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Giấy mời đã chỉ đạo',
                            'SoLuong' => $gmdxl + $getDocSeenBGD_GM,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Giấy mời Lãnh đạo đã chỉ đạo',
                                    'Link2' => base_url().'giaymoidaxuly_gd',
                                    'SoLuong2' => $gmdxl,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DSGM xem để biết',
                                    'Link2' => base_url().'dsgm_bgd',
                                    'SoLuong2' => $getDocSeenBGD_GM,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '3' => array(
                            'TieuDe' => 'Xem lịch công tác',
                            'Link' => base_url().'lichcongtac'.(($vanban['PK_iMaCB'] == 534) ? '?thoigian=1':'?thoigian=1'),
                            'SoLuong' => '',
                            'icon' => 'fa fa-calendar'
                        ),
                        '4' => array(
                            'TieuDe' => 'Danh sách giấy mời đến của sở',
                            'Link' => base_url().'dsgiaymoi',
                            'SoLuong' => '',
                            'icon' => ''
                        ),
                        '5' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                            'Link' => base_url().'dssoanthaodagui',
                            'SoLuong' => $dagui,
                            'icon' => 'fa fa-share-square-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                            'Link' => base_url().'dsgiaymoiketluanmoi',
                            'SoLuong' => $chuaxem,
                            'icon' => 'fa fa-flag',
                            'thongbao' => 1
                        ),
                        '7' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                            'Link' => base_url().'dsgiaymoiketluandaxem',
                            'SoLuong' => $daxem,
                            'icon' => 'fa fa-flag-o'
                        )
                    ),
                    'III' => array(
                        '1' => array(
                            'TieuDe' => 'Văn bản trình ký',
                            'Link' => base_url().'vanbandichoxuly_ld',
                            'SoLuong' => $vanbandichoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Lịch họp của sở chủ trì',
                            'Link' => base_url().'lichhoplanhdao',
                            'SoLuong' => '',
                            'icon' => 'fa fa-list-alt'
                        )
                    ),
                    'IV' => array(
                        '1' => array(
                            'TieuDe' => 'Thông tin mới',
                            'Link' => base_url().'thongtinmoi',
                            'SoLuong' => count($tinmoi),
                            'icon' => 'fa fa-info-circle',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Thông tin đã gửi đi',
                            'Link' => base_url().'thongtinguidi',
                            'SoLuong' => count($tindagui),
                            'icon' => 'fa fa-info-circle'
                        ),
                        '3' => array(
                            'TieuDe' => 'Thông tin đã xem',
                            'Link' => base_url().'thongtindaxem',
                            'SoLuong' => $tindaxem,
                            'icon' => 'fa fa-info-circle'
                        ),
                        '4' => array(
                            'TieuDe' => 'Soạn thảo thông tin',
                            'Link' => base_url().'soanthaothongtin',
                            'SoLuong' => '',
                            'icon' => 'fa fa-edit'
                        )

                    ),
                    'V' => array(
                        '1' => array(
                            'TieuDe' => 'Đầu việc chờ xử lý',
                            'Link' => base_url().'dauviecchoxuly_gd',
                            'SoLuong' => $dauvieclanhdaochoxuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Đầu việc đã xử lý (Chưa hoàn thành)',
                            'Link' => base_url().'dauviecdaxuly_gd',
                            'SoLuong' => $dauvieclanhdaodaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Danh sách đầu việc của Sở',
                            'Link' => base_url().'dauviecdaxuly_tp',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Danh sách tài liệu họp HĐND của tổng hợp',
                            'Link' => base_url().'danhsachtailieu',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Danh sách tài liệu họp HĐND của phòng',
                            'Link' => base_url().'danhsachtailieuphong',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Danh sách kiến nghị HĐND',
                            'Link' => base_url().'danhsachkiennghi',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VI' => array(
                        '1' => array(
                            'TieuDe' => ' PGĐ sở nhận xét thực hiện kết quả tháng của GĐ',
                            'Link' => base_url().'xemkehoachcongtac_GD/519/'.$month1,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Xem kế hoạch, kết quả công tác tuần của các phòng',
                            'Link' => base_url().'dskehoachcongtactuanld',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Xem kết quả phân loại tháng của các phòng',
                            'Link' => base_url().'dskehoachcongtacthangld',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ), 
                        '4' => array(
                            'TieuDe' => 'Phó Giám đốc tự chấm điểm tháng của cá nhân',
                            'Link' => base_url().'xemkehoachcongtac_lanhdao',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                         
                        '5' => array(
                            'TieuDe' => 'PGĐ nhận xét tháng với các PGĐ và trưởng phòng',
                            'Link' => base_url().'dskehoachcongtacthangtp',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Thống kê kết quả công tác tuần trong toàn sở',
                            'Link' => base_url().'thongkebaocao',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Thống kê kết quả công tác tháng trong toàn sở',
                            'Link' => base_url().'thongkebaocaothang',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )

                );
                $result = $Quyen;
            }
            /** TRUONG PHONG **/
            if($vanban['iQuyenHan_DHNB'] == 6){
                $vbcxlcvp = $this->Mdmadmin->getDocAwait('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
                $congtacdang = $this->Mdmadmin->getCTD('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL);
                $tocongtac = $this->Mdmadmin->getDocAwaitTCT('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,$vanban['FK_iMaPhongHD'],NULL,1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,1);
                // van ban phoi hop
                $pph = $this->Mdmadmin->getDocAwaitPPH(6,$vanban['FK_iMaPhongHD'],NULL,'2','5',0);
                $vanbanchidao = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB']);
                $gmcxl = $this->Mdmadmin->getDocAwait1('5',NULL,$vanban['FK_iMaPhongHD'],NULL,'1');
                // giay moi da xu ly
                $gmdxl = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB'],$vanban['FK_iMaPhongHD'],'1');
                $gmdht = $this->Mdmadmin->getDocTPCDHT($vanban['PK_iMaCB'],$vanban['FK_iMaPhongHD'],'1');
                //giay moi phoi hop cho xu ly
                $gmph = $this->Mdmadmin->getDocAwaitPPH(6,$vanban['FK_iMaPhongHD'],NULL,'1','5',0);
                $phoihopxl = $this->Mdmadmin->getDocAwaitPPHDXL(6,$vanban['FK_iMaPhongHD'],NULL,'1','5',1);
                $phdxl = $phoihopxl;
                // van ban cho duyet hoan thanh
                $vbcduyet = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB']);
                $vbduyetgiaymoi = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB'],1);
                $dstocongtac = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB']);
                $dstocongtac_ht = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB'],2);
//            pr($data['vanbanchidao']);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $vbqt = $this->Mdmadmin->getVBQT($vanban['PK_iMaCB']);
                $vbqtdgq = $this->Mdmadmin->getVBQTGQ($vanban['PK_iMaCB']);
                $tuchoipp = $this->Mdmadmin->PPPHTuChoi($vanban['FK_iMaPhongHD']);
                $tuchoigm = $this->Mdmadmin->PPPHTuChoi($vanban['FK_iMaPhongHD'],1);
                $dschutri = $this->Mdmadmin->getDocCT($vanban['FK_iMaPhongHD']);
                $dsgiahan = $this->Mdmadmin->dsGiaHan($vanban['PK_iMaCB']);
                $dsdadexuat = $this->Mdmadmin->layVBGiaHanTuChoi(NULL,$vanban['FK_iMaPhongHD']);
                $dsphoihop = $this->Mdmadmin->getDocAwaitDSCTPPH($vanban['FK_iMaPhongHD']);
                $vanbandaxuly_stcph = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB'],NULL,NULL,1);
                $bcchoduyet = $this->Mdmadmin->getDuyetBaoCao(NULL,$vanban['FK_iMaPhongHD']);
                if($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] == 15){
                    $vanbanchidao_cc = $this->Mdmadmin->getConCac($vanban['FK_iMaPhongHD']);
                    $vanbandaxuly_stcphcc = $this->Mdmadmin->getConCac($vanban['FK_iMaPhongHD'],NULL,1);
                    $tocongtac = $this->Mdmadmin->getDocAwaitTCT('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
                    $gmdxl_cc = $this->Mdmadmin->getConCac($vanban['FK_iMaPhongHD'],'1');
                    $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                    $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                    $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                }

                $trangthaikq = 1;
                $trangthaiduyet = 2;
                if($chucvu==15) // chức vụ chi cục trưởng
                {
                    $trangthai_tk       = 3;
                    $trangthai    = 5; #CCT
                    $ten_taikhoan = 'truongchicuc';
                    $ykien        = 'ykien_tcc';
                }
                if($chucvu==3 || $chucvu==7 ) // chức vụ trưởng phòng
                {
                    $trangthai_tk       = 5;
                    $trangthai    = 3;
                    $ten_taikhoan = 'truongphong';
                    $ykien        = 'ykien_tp';
                }
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';

                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $duyetketqua            = $this->Mketqua->demTTKT($trangthaiduyet,$phongban,$taikhoan);
                $tong_sl_cv = $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND ( active = 1 or active = 2 or  active = 4) and phong_id = ".$vanban['FK_iMaPhongHD']." and tuan =". $week);

                $tonglanhdaogiao = $this->Mdanhmuc->countphongthuly($vanban['FK_iMaPhongHD']);

                $tongphlanhdaogiao = $this->Mdanhmuc->countphongphoihop($vanban['FK_iMaPhongHD']);

                /** TRUONG PHONG **/
                if($vanban['iQuyenHan_DHNB'] == 6 && $vanban['FK_iMaCV'] != 15){
                    $Quyen = array(
                        'I' => array(
                            '1' => array(
                                'TieuDe' => 'Danh sách văn bản quá hạn của phòng',
                                'Link' => base_url().'dsvanbanquahan',
                                'SoLuong' => $tongvbquahan,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '2' => array(
                                'TieuDe' => 'DS văn bản giao phòng xử lý',
                                'SoLuong' => $vbcxlcvp + $congtacdang + $tocongtac+ $pph,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'VB giao phòng chủ trì chờ xử lý',
                                        'Link2' => base_url().'vanbanchoxuly_tp',
                                        'SoLuong2' => $vbcxlcvp,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                        'Link2' => base_url().'congtacdang_tp',
                                        'SoLuong2' => $congtacdang,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'VB tổ công tác chờ xử lý',
                                        'Link2' => base_url().'vanbanchoxuly_tct',
                                        'SoLuong2' => $tocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '4' => array(
                                        'TieuDe2' => 'VB giao phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'phongphoihop',
                                        'SoLuong2' => $pph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '3' => array(
                                'TieuDe' => ' DS công việc lãnh đạo sở giao:',
                                'SoLuong' => $tonglanhdaogiao+$tongphlanhdaogiao,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => ' Danh sách công việc lãnh đạo sở giao phòng',
                                        'Link2' => base_url().'tpchiaviec',
                                        'SoLuong2' => $tonglanhdaogiao,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => ' DS công việc lãnh đạo sở giao phòng phối hợp',
                                        'Link2' => base_url().'tpphchiaviec',
                                        'SoLuong2' => $tongphlanhdaogiao,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => ' DS công việc LĐ Sở giao phòng đang thực hiện',
                                        'Link2' => base_url().'dsphongdangthuchien',
                                        'SoLuong2' => '',
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '4' => array(
                                        'TieuDe2' => ' DS công việc phối hợp LĐ Sở giao phòng đang thực hiện',
                                        'Link2' => base_url().'tpphdangthuchien',
                                        'SoLuong2' => '',
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '4' => array(
                                'TieuDe' => 'DS văn bản đã chỉ đạo',
                                'SoLuong' => $vanbanchidao + $vanbandaxuly_stcph,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Văn bản phòng chủ trì đã chỉ đạo',
                                        'Link2' => base_url().'vanbandachidao',
                                        'SoLuong2' => $vanbanchidao,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Văn bản STC phối hợp đã chỉ đạo',
                                        'Link2' => base_url().'vanbandaxulystcph_tp',
                                        'SoLuong2' => $vanbandaxuly_stcph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '5' => array(
                                'TieuDe' => 'DSVB đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'vanbanchopheduyet',
                                'SoLuong' => $vbcduyet,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '6' => array(
                                'TieuDe' => 'Văn bản phối hợp trả lại',
                                'Link' => base_url().'phoihopchuyenlai_tp',
                                'SoLuong' => $tuchoipp,
                                'icon' => 'fa fa-reply',
                                'thongbao' => 1
                            ),
                            '7' => array(
                                'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                                'Link' => base_url().'dsdexuatgiahan',
                                'SoLuong' => $dsgiahan,
                                'icon' => 'fa fa-clock-o',
                                'thongbao' => 1
                            ),
                            '8' => array(
                                'TieuDe' => 'DSVB đã đề xuất gia hạn giải quyết',
                                'Link' => base_url().'dshandadexuat',
                                'SoLuong' => $dsdadexuat,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '9' => array(
                                'TieuDe' => 'Tổng số lượng Tổ công tác giao phòng xử lý',
                                'SoLuong' => $dstocongtac + $dstocongtac_ht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                        'Link2' => base_url().'dstocongtac',
                                        'SoLuong2' => $dstocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                        'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                        'SoLuong2' => $dstocongtac_ht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '10' => array(
                                'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao phòng xử lý',
                                'SoLuong' => $dsdonthu + $dsdonthuht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                        'Link2' => base_url().'dsdonthu',
                                        'SoLuong2' => $dsdonthu,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                        'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                        'SoLuong2' => $dsdonthuht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '11' => array(
                                'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                                'SoLuong' => $dschutri + $dsphoihop,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                        'Link2' => base_url().'dsphongchutri',
                                        'SoLuong2' => $dschutri,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                        'Link2' => base_url().'dsphoihop',
                                        'SoLuong2' => $dsphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '12' => array(
                                'TieuDe' => 'DSVB quan trọng của Sở',
                                'SoLuong' => $vbqt + $vbqtdgq,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'DSVB quan trọng của Sở chưa hoàn thành',
                                        'Link2' => base_url().'vanbanquantrong',
                                        'SoLuong2' => $vbqt,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'DSVB quan trọng của Sở đã hoàn thành',
                                        'Link2' => base_url().'vanbanquantrong?cacloaivanban=2',
                                        'SoLuong2' => $vbqtdgq,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '13' => array(
                                'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                                'Link' => base_url().'dexuatphoihop_tp',
                                'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '14' => array(
                                'TieuDe' => 'Công việc phối hợp chờ xử lý',
                                'Link' => base_url().'dexuatphoihop_tp_ph',
                                'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            )
                        ),
                        'II' => array(
                            '1' => array(
                                'TieuDe' => 'DS giấy mời giao phòng xử lý',
                                'SoLuong' => $gmcxl + $gmph,
                                'icon' => 'fa fa-file-text-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Giấy mời phòng chủ trì chờ xử lý',
                                        'Link2' => base_url().'giaymoi_tp',
                                        'SoLuong2' => $gmcxl,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Giấy mời phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'giaymoiphoihop',
                                        'SoLuong2' => $gmph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),

                            '2' => array(
                                'TieuDe' => 'DS giấy mời đã chỉ đạo',
                                'SoLuong' => $gmdxl + $phdxl + $gmdht,
                                'icon' => 'fa fa-file-text-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'GM giao phòng chủ trì đã chỉ đạo',
                                        'Link2' => base_url().'giaymoidaxuly',
                                        'SoLuong2' => $gmdxl,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'GM giao phòng chủ trì đã hoàn thành',
                                        'Link2' => base_url().'giaymoihoanthanh',
                                        'SoLuong2' => $gmdht,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'GM giao phòng phối hợp đã chỉ đạo',
                                        'Link2' => base_url().'giaymoiphoihopdaxuly',
                                        'SoLuong2' => $phdxl,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),

                            '3' => array(
                                'TieuDe' => 'GM đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'giaymoichoduyet',
                                'SoLuong' => $vbduyetgiaymoi,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '4' => array(
                                'TieuDe' => 'Giấy mời phối hợp trả lại',
                                'Link' => base_url().'giaymoiphoihopchuyenlai_tp',
                                'SoLuong' => $tuchoigm,
                                'icon' => 'fa fa-reply',
                                'thongbao' => 1
                            ),
                            '5' => array(
                                'TieuDe' => 'Xem lịch công tác',
                                'Link' => base_url().'lichcongtac',
                                'SoLuong' => '',
                                'icon' => 'fa fa-calendar'
                            ),
                            '6' => array(
                                'TieuDe' => 'Báo cáo cuộc họp chờ phê duyệt',
                                'Link' => base_url().'duyetbaocao',
                                'SoLuong' => $bcchoduyet,
                                'icon' => 'fa fa-check-square-o'
                            ),
                            '7' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                                'Link' => base_url().'dssoanthaodagui',
                                'SoLuong' => $dagui,
                                'icon' => 'fa fa-share-square-o'
                            ),
                            '8' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                                'Link' => base_url().'dsgiaymoiketluanmoi',
                                'SoLuong' => $chuaxem,
                                'icon' => 'fa fa-flag',
                                'thongbao' => 1
                            ),
                            '9' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                                'Link' => base_url().'dsgiaymoiketluandaxem',
                                'SoLuong' => $daxem,
                                'icon' => 'fa fa-flag-o'
                            )
                        ),
                        'III' => array(
                            '1' => array(
                                'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                                'Link' => base_url().'vanbandichoxuly_tp',
                                'SoLuong' => $vanbandichoxuly,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Văn bản chủ trì đã xử lý',
                                'Link' => base_url().'vanbandidaxuly_tp',
                                'SoLuong' => $vanbandidaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'DS Văn bản của phòng',
                                'Link' => base_url().'vanbanphongban',
                                'SoLuong' => $vanbandi_phong,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '4' => array(
                                'TieuDe' => 'DS Giấy mời của phòng',
                                'Link' => base_url().'giaymoiphongban',
                                'SoLuong' => $vanbanditrinhky,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '5' => array(
                                'TieuDe' => 'Nhập văn bản đi',
                                'Link' => base_url().'vanban',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )
                        ),
                        'IV' => array(
                            '1' => array(
                                'TieuDe' => 'Thông tin mới',
                                'Link' => base_url().'thongtinmoi',
                                'SoLuong' => count($tinmoi),
                                'icon' => 'fa fa-info-circle',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Thông tin đã gửi đi',
                                'Link' => base_url().'thongtinguidi',
                                'SoLuong' => count($tindagui),
                                'icon' => 'fa fa-info-circle'
                            ),
                            '3' => array(
                                'TieuDe' => 'Thông tin đã xem',
                                'Link' => base_url().'thongtindaxem',
                                'SoLuong' => $tindaxem,
                                'icon' => 'fa fa-info-circle'
                            ),
                            '4' => array(
                                'TieuDe' => 'Soạn thảo thông tin',
                                'Link' => base_url().'soanthaothongtin',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )

                        ),
                        'V' => array(
                            '1' => array(
                                'TieuDe' => 'Đầu việc phòng chủ trì chờ xử lý',
                                'Link' => base_url().'dauviecchoxuly_tp',
                                'SoLuong' => $dauviecchutrichoxuly,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Đầu việc phòng phối hợp chờ xử lý',
                                'Link' => base_url().'dauviecchoxulyph_tp',
                                'SoLuong' => $dauviecphoihopchoxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Đầu việc đã chỉ đạo (Chưa hoàn thành)',
                                'Link' => base_url().'dauviecdaxuly_tp',
                                'SoLuong' => $dauviecchutridaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => 'Đầu việc phòng chủ trì',
                                'Link' => base_url().'dsdauviecphongchutri',
                                'SoLuong' => $dauviecphongchutri,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => 'Đầu việc phòng phối hợp',
                                'Link' => base_url().'dsdauviecphongphoihop',
                                'SoLuong' => $dauviecphongphoihop,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Đầu việc hoàn thành chờ lãnh đạo phòng duyệt',
                                'Link' => base_url().'duyetketqua',
                                'SoLuong' => $duyetketqua,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => 'Đầu việc chờ phê duyệt gửi lãnh đạo Sở',
                                'Link' => base_url().'dauviecchoduyet',
                                'SoLuong' => $dauviecchoduyet,
                                'icon' => 'fa fa-newspaper-o'
                            )

                        ),
                        'VI' => array(
                            '1' => array(
                                'TieuDe' => ' Duyệt, giao KH, đánh giá công việc tuần của cấp dưới',
                                'Link' => base_url().'kehoachcongtac',
                                'SoLuong' => $tong_sl_cv,
                                'icon' => 'fa fa-newspaper-o'
                                ),
                            '2' => array(
                                'TieuDe' => ' Nhận xét, chấm điểm công việc tuần của cấp dưới',
                                'Link' => base_url().'dskehoachcongtac/'.$week,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => ' Phân loại công chức của phòng theo tháng',
                                'Link' => base_url().'dskehoachcongtacthang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => ' Trưởng phòng tự đánh giá, chấm điểm tháng của cá nhân',
                                'Link' => base_url().'xemkehoachcongtac_lanhdao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => ' Xem kết quả phân loại của các trưởng phòng',
                                'Link' => base_url().'dskehoachcongtacthangtp',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => ' Xem kết quả công tác tuần của các phòng trong sở',
                                'Link' => base_url().'dskehoachcongtactuanld',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => ' Thống kê kết quả công tác tuần trong toàn sở',
                                'Link' => base_url().'thongkebaocao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '8' => array(
                                'TieuDe' => ' Thống kê kết quả công tác tháng trong toàn sở',
                                'Link' => base_url().'thongkebaocaothang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            )

                        ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )
                    );
                }else{
                    $Quyen = array(
                        'I' => array(
                            '1' => array(
                                'TieuDe' => 'Danh sách văn bản quá hạn của phòng',
                                'Link' => base_url().'dsvanbanquahan',
                                'SoLuong' => $tongvbquahan,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '2' => array(
                                'TieuDe' => 'DS văn bản giao phòng xử lý',
                                'SoLuong' => $vbcxlcvp + $congtacdang + $tocongtac+ $pph,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'VB giao phòng chủ trì chờ xử lý',
                                        'Link2' => base_url().'vanbanchoxuly_cc',
                                        'SoLuong2' => $vbcxlcvp,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                        'Link2' => base_url().'congtacdang_cc',
                                        'SoLuong2' => $congtacdang,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'VB tổ công tác chờ xử lý',
                                        'Link2' => base_url().'vanbanchoxuly_tct',
                                        'SoLuong2' => $tocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '4' => array(
                                        'TieuDe2' => 'VB giao phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'phongphoihop',
                                        'SoLuong2' => $pph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '3' => array(
                                'TieuDe' => ' DS công việc lãnh đạo sở giao:',
                                'SoLuong' => $tonglanhdaogiao+$tongphlanhdaogiao,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => ' Danh sách công việc lãnh đạo sở giao phòng',
                                        'Link2' => base_url().'tpchiaviec',
                                        'SoLuong2' => $tonglanhdaogiao,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => ' DS công việc lãnh đạo sở giao phòng phối hợp',
                                        'Link2' => base_url().'tpphchiaviec',
                                        'SoLuong2' => $tongphlanhdaogiao,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => ' DS công việc LĐ Sở giao phòng đang thực hiện',
                                        'Link2' => base_url().'dsphongdangthuchien',
                                        'SoLuong2' => '',
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '4' => array(
                                        'TieuDe2' => ' DS công việc phối hợp LĐ Sở giao phòng đang thực hiện',
                                        'Link2' => base_url().'tpphdangthuchien',
                                        'SoLuong2' => '',
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '4' => array(
                                'TieuDe' => 'DS văn bản đã chỉ đạo',
                                'SoLuong' => $vanbanchidao_cc + $vanbandaxuly_stcphcc,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Văn bản phòng chủ trì đã chỉ đạo',
                                        'Link2' => base_url().'vanbandaxuly_cc',
                                        'SoLuong2' => $vanbanchidao_cc,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Văn bản STC phối hợp đã chỉ đạo',
                                        'Link2' => base_url().'vanbandaxulystcph_cc',
                                        'SoLuong2' => $vanbandaxuly_stcphcc,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '5' => array(
                                'TieuDe' => 'DSVB đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'vanbanchopheduyet',
                                'SoLuong' => $vbcduyet,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '6' => array(
                                'TieuDe' => 'Văn bản phối hợp trả lại',
                                'Link' => base_url().'phoihopchuyenlai_tp',
                                'SoLuong' => $tuchoipp,
                                'icon' => 'fa fa-reply',
                                'thongbao' => 1
                            ),
                            '7' => array(
                                'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                                'Link' => base_url().'dsdexuatgiahan',
                                'SoLuong' => $dsgiahan,
                                'icon' => 'fa fa-clock-o',
                                'thongbao' => 1
                            ),
                            '8' => array(
                                'TieuDe' => 'DSVB đã đề xuất gia hạn giải quyết',
                                'Link' => base_url().'dshandadexuat',
                                'SoLuong' => $dsdadexuat,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '9' => array(
                                'TieuDe' => 'Tổng số lượng Tổ công tác giao phòng xử lý',
                                'SoLuong' => $dstocongtac + $dstocongtac_ht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                        'Link2' => base_url().'dstocongtac',
                                        'SoLuong2' => $dstocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                        'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                        'SoLuong2' => $dstocongtac_ht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '10' => array(
                                'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao phòng xử lý',
                                'SoLuong' => $dsdonthu + $dsdonthuht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                        'Link2' => base_url().'dsdonthu',
                                        'SoLuong2' => $dsdonthu,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                        'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                        'SoLuong2' => $dsdonthuht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '11' => array(
                                'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                                'SoLuong' => $dschutri + $dsphoihop,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                        'Link2' => base_url().'dsphongchutri',
                                        'SoLuong2' => $dschutri,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                        'Link2' => base_url().'dsphoihop',
                                        'SoLuong2' => $dsphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '12' => array(
                                'TieuDe' => 'DSVB quan trọng của Sở',
                                'SoLuong' => $vbqt + $vbqtdgq,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'DSVB quan trọng của Sở chưa hoàn thành',
                                        'Link2' => base_url().'vanbanquantrong',
                                        'SoLuong2' => $vbqt,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'DSVB quan trọng của Sở đã hoàn thành',
                                        'Link2' => base_url().'vanbanquantrong?cacloaivanban=2',
                                        'SoLuong2' => $vbqtdgq,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '13' => array(
                                'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                                'Link' => base_url().'dexuatphoihop_tp',
                                'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '14' => array(
                                'TieuDe' => 'Công việc phối hợp chờ xử lý',
                                'Link' => base_url().'dexuatphoihop_tp_ph',
                                'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            )
                        ),
                        'II' => array(
                            '1' => array(
                                'TieuDe' => 'DS giấy mời giao phòng xử lý',
                                'SoLuong' => $gmcxl + $gmph,
                                'icon' => 'fa fa-file-text-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Giấy mời phòng chủ trì chờ xử lý',
                                        'Link2' => base_url().'giaymoi_cc',
                                        'SoLuong2' => $gmcxl,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Giấy mời phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'giaymoiphoihop',
                                        'SoLuong2' => $gmph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),

                            '2' => array(
                                'TieuDe' => 'DS giấy mời đã chỉ đạo',
                                'SoLuong' => $gmdxl_cc + $phdxl,
                                'icon' => 'fa fa-file-text-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'GM giao phòng chủ trì đã chỉ đạo',
                                        'Link2' => base_url().'giaymoidaxuly_cc',
                                        'SoLuong2' => $gmdxl_cc,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'GM giao phòng phối hợp đã chỉ đạo',
                                        'Link2' => base_url().'giaymoiphoihopdaxuly',
                                        'SoLuong2' => $phdxl,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),

                            '3' => array(
                                'TieuDe' => 'GM đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'giaymoichoduyet',
                                'SoLuong' => $vbduyetgiaymoi,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '4' => array(
                                'TieuDe' => 'Giấy mời phối hợp trả lại',
                                'Link' => base_url().'giaymoiphoihopchuyenlai_tp',
                                'SoLuong' => $tuchoigm,
                                'icon' => 'fa fa-reply',
                                'thongbao' => 1
                            ),
                            '5' => array(
                                'TieuDe' => 'Xem lịch công tác',
                                'Link' => base_url().'lichcongtac',
                                'SoLuong' => '',
                                'icon' => 'fa fa-calendar'
                            ),
                            '6' => array(
                                'TieuDe' => 'Báo cáo cuộc họp chờ phê duyệt',
                                'Link' => base_url().'duyetbaocao',
                                'SoLuong' => $bcchoduyet,
                                'icon' => 'fa fa-check-square-o'
                            ),
                            '7' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                                'Link' => base_url().'dssoanthaodagui',
                                'SoLuong' => $dagui,
                                'icon' => 'fa fa-share-square-o'
                            ),
                            '8' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                                'Link' => base_url().'dsgiaymoiketluanmoi',
                                'SoLuong' => $chuaxem,
                                'icon' => 'fa fa-flag',
                                'thongbao' => 1
                            ),
                            '9' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                                'Link' => base_url().'dsgiaymoiketluandaxem',
                                'SoLuong' => $daxem,
                                'icon' => 'fa fa-flag-o'
                            )
                        ),
                        'III' => array(
                            '1' => array(
                                'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                                'Link' => base_url().'vanbandichoxuly_cct',
                                'SoLuong' => $vanbandichoxuly,
                                'icon' => 'fa fa-newspaper-o',
                            ),
                            '2' => array(
                                'TieuDe' => 'Văn bản chủ trì đã xử lý',
                                'Link' => base_url().'vanbandidaxuly_cct',
                                'SoLuong' => $vanbandidaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'DS Văn bản của phòng',
                                'Link' => base_url().'vanbanphongban',
                                'SoLuong' => $vanbandi_phong,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '4' => array(
                                'TieuDe' => 'DS Giấy mời của phòng',
                                'Link' => base_url().'giaymoiphongban',
                                'SoLuong' => $giaymoidi_phong,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '5' => array(
                                'TieuDe' => 'Nhập văn bản đi',
                                'Link' => base_url().'vanban',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )
                        ),
                        'IV' => array(
                            '1' => array(
                                'TieuDe' => 'Thông tin mới',
                                'Link' => base_url().'thongtinmoi',
                                'SoLuong' => count($tinmoi),
                                'icon' => 'fa fa-info-circle',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Thông tin đã gửi đi',
                                'Link' => base_url().'thongtinguidi',
                                'SoLuong' => count($tindagui),
                                'icon' => 'fa fa-info-circle'
                            ),
                            '3' => array(
                                'TieuDe' => 'Thông tin đã xem',
                                'Link' => base_url().'thongtindaxem',
                                'SoLuong' => $tindaxem,
                                'icon' => 'fa fa-info-circle'
                            ),
                            '4' => array(
                                'TieuDe' => 'Soạn thảo thông tin',
                                'Link' => base_url().'soanthaothongtin',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )

                        ),
                        'V' => array(
                            '1' => array(
                                'TieuDe' => 'Đầu việc phòng chủ trì chờ xử lý',
                                'Link' => base_url().'dauviecchoxuly_tp',
                                'SoLuong' => $dauviecchutrichoxuly,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Đầu việc phòng phối hợp chờ xử lý',
                                'Link' => base_url().'dauviecchoxulyph_tp',
                                'SoLuong' => $dauviecphoihopchoxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Đầu việc đã chỉ đạo (Chưa hoàn thành)',
                                'Link' => base_url().'dauviecdaxuly_tp',
                                'SoLuong' => $dauviecchutridaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => 'Đầu việc phòng chủ trì',
                                'Link' => base_url().'dsdauviecphongchutri',
                                'SoLuong' => $dauviecphongchutri,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => 'Đầu việc phòng phối hợp',
                                'Link' => base_url().'dsdauviecphongphoihop',
                                'SoLuong' => $dauviecphongphoihop,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Đầu việc hoàn thành chờ lãnh đạo phòng duyệt',
                                'Link' => base_url().'duyetketqua',
                                'SoLuong' => $duyetketqua,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => 'Đầu việc chờ phê duyệt gửi lãnh đạo Sở',
                                'Link' => base_url().'dauviecchoduyet',
                                'SoLuong' => $dauviecchoduyet,
                                'icon' => 'fa fa-newspaper-o'
                            )

                        ),
                        'VI' => array(
                            /*'1' => array(
                                'TieuDe' => 'Nhập kế hoạch-công tác phòng trình LĐ sở',
                                'Link' => base_url().'trinhlanhdao',
                                'SoLuong' => $tong_cho_ldduyet,
                                'icon' => 'fa fa-newspaper-o'
                                ),*/
                            '1' => array(
                                'TieuDe' => 'Nhập kế hoạch-đánh giá công việc tuần',
                                'Link' => base_url().'kehoachcongtac',
                                'SoLuong' => $tong_sl_cv,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '2' => array(
                                'TieuDe' => 'Đánh giá kết quả công tác tuần của phòng',
                                'Link' => base_url().'dskehoachcongtac/'.$week,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Đánh giá kết quả phân loại tháng của phòng',
                                'Link' => base_url().'dskehoachcongtacthang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => 'Đánh giá kết quả phân loại quý của phòng',
                                'Link' => base_url().'dskehoachcongtacquy',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => 'Tự đánh giá kết quả tháng của cá nhân',
                                'Link' => base_url().'xemkehoachcongtac_lanhdao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Xem kết quả công tác tháng của sở',
                                'Link' => base_url().'dskehoachcongtacthangtp',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            )

                        ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )
                    );
                }
                $result = $Quyen;
            }
            /** PHO PHONG **/
            if($vanban['iQuyenHan_DHNB'] == 7){
                $vbcxlcvp = $this->Mdmadmin->getDocAwait('6',NULL,NULL,$vanban['PK_iMaCB'],NULL,NULL,NULL,1);
                $tocongtac = $this->Mdmadmin->getDocAwaitTCT('6',NULL,NULL,$vanban['PK_iMaCB'],NULL,NULL,NULL,1);
                $congtacdang = $this->Mdmadmin->getCTD('6',NULL,NULL,$vanban['PK_iMaCB'],NULL,NULL,NULL);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,$vanban['PK_iMaCB'],1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,NULL,$vanban['PK_iMaCB'],NULL,1);
                $phophongphoihop = $this->Mdmadmin->getDocAwaitPPPH($vanban['iQuyenHan_DHNB'],$vanban['FK_iMaPhongHD'],$vanban['PK_iMaCB'],NULL);
                $gm_phophongph = $this->Mdmadmin->getDocAwaitPPPH($vanban['iQuyenHan_DHNB'],$vanban['FK_iMaPhongHD'],$vanban['PK_iMaCB'],1);
                $theodoivanban = $this->Mdmadmin->getDocAwait('7',NULL,NULL,$vanban['PK_iMaCB'],NULL,NULL,'1');
                $pph = $this->Mdmadmin->getDocAwaitPPH(7,$vanban['FK_iMaPhongHD'],$vanban['PK_iMaCB'],'2',NULL,NULL,'2');
                $gmph = $this->Mdmadmin->getDocAwaitPPH(7,$vanban['FK_iMaPhongHD'],$vanban['PK_iMaCB'],'1',NULL,NULL,'2');
                $vanbanchidao = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB']);
                $gmcxl = $this->Mdmadmin->getDocAwait('6',NULL,NULL,$vanban['PK_iMaCB'],'1');
                $gmdxl = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB'],$vanban['FK_iMaPhongHD'],'1');
                $vbcduyet = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB']);
                $vbduyetgiaymoi = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB'],1);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $dschutri = $this->Mdmadmin->getDocCT($vanban['FK_iMaPhongHD']);
                $dstocongtac = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB']);
                $dstocongtac_ht = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB'],2);
                $dsgiahan = $this->Mdmadmin->dsGiaHan($vanban['PK_iMaCB']);
                $dsdadexuat = $this->Mdmadmin->layVBGiaHanTuChoi(NULL,NULL,$vanban['PK_iMaCB']);
                $dsphoihop = $this->Mdmadmin->getDocAwaitDSCTPPH($vanban['FK_iMaPhongHD']);
                $canboxuly = $this->Mdmadmin->countDocGo($vanban['PK_iMaCB']);
                $vanbandaxuly_stcph = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB'],NULL,NULL,1);
                $daguibituchoi = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB'],2);



                $canbo              = $taikhoan;
                $trangthaiduyet     = 1;
                $trangthai_tk       = 6;
                $trangthai =2;
                $ten_taikhoan       = 'phophong';
                $ykien              = 'ykien_pp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';

                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $duyetketqua            = $this->Mketqua->demTTKT($trangthaiduyet,$phongban,$taikhoan);
                $tong_sl_cv = $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND (((active = 0 and lanhdao_id = ".$vanban['PK_iMaCB'].") or (active = 3 and canbo_id = ".$vanban['PK_iMaCB'].")  or (active = 4 and (ketluan_pp IS NULL or ketluan_pp ='') and lanhdao_id = ".$vanban['PK_iMaCB'].")) or ( active <= 3 and phoihop like '%[".$vanban['PK_iMaCB']."]%')) and tuan =". $week );

                $tongphophonglanhdaogiao = $this->Mdanhmuc->countphophongthuly($vanban['PK_iMaCB']);

                $tongphoihopthuly = $this->Mdanhmuc->countphoihopthuly($vanban['PK_iMaCB']);

                if($vanban['FK_iMaPhongHD'] == 12){
                    $Quyen = array(
                        'I' => array(
                            '1' => array(
                                'TieuDe' => 'Danh sách văn bản quá hạn',
                                'Link' => base_url().'dsvanbanquahan',
                                'SoLuong' => $tongvbquahan,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '2' => array(
                                'TieuDe' => 'DS văn bản chờ xử lý',
                                'SoLuong' => $vbcxlcvp + $congtacdang + $tocongtac+ $pph + $phophongphoihop + $theodoivanban,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'VB chủ trì chờ xử lý',
                                        'Link2' => base_url().'vanbanchoxuly_pp',
                                        'SoLuong2' => $vbcxlcvp,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                        'Link2' => base_url().'congtacdang_pp',
                                        'SoLuong2' => $congtacdang,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'VB tổ công tác chờ xử lý',
                                        'Link2' => base_url().'tocongtac_pp',
                                        'SoLuong2' => $tocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '4' => array(
                                        'TieuDe2' => 'VB phó phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'phophongphoihop',
                                        'SoLuong2' => $phophongphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '5' => array(
                                        'TieuDe2' => 'VB Phó phòng biết để đôn đốc',
                                        'Link2' => base_url().'vanbantheodoi_pp',
                                        'SoLuong2' => $theodoivanban,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '6' => array(
                                        'TieuDe2' => 'VB phối hợp chờ xử lý',
                                        'Link2' => base_url().'phongphoihop',
                                        'SoLuong2' => $pph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '3' => array(
                                'TieuDe' => ' DS văn bản lãnh đạo sở giao chờ xử lý:',
                                'SoLuong' => $tongphophonglanhdaogiao + $tongphoihopthuly,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                    'Cap2' => array(
                                        '1' => array(
                                            'TieuDe2' => 'DS công việc lãnh đạo sở giao chờ xử lý',
                                            'Link2' => base_url().'ppchiaviec',
                                            'SoLuong2' => $tongphophonglanhdaogiao,
                                            'icon' => 'fa fa-caret-right'
                                        ),
                                        '2' => array(
                                            'TieuDe2' => 'DS CV lãnh đạo giao phối hợp chờ xử lý',
                                            'Link2' => base_url().'ldphchiaviec',
                                            'SoLuong2' => $tongphoihopthuly,
                                            'icon' => 'fa fa-caret-right'
                                        ),
                                        '3' => array(
                                            'TieuDe2' => 'DS CV LĐ Sở giao đang thực hiện',
                                            'Link2' => base_url().'dsppdangchidao',
                                            'SoLuong2' => '',
                                            'icon' => 'fa fa-caret-right'
                                        ),
                                        '4' => array(
                                            'TieuDe2' => 'DS CV phối hợp LĐ Sở giao đang thực hiện',
                                            'Link2' => base_url().'ldphdangthuchien',
                                            'SoLuong2' => '',
                                            'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '4' => array(
                                'TieuDe' => 'DS văn bản đã chỉ đạo',
                                'SoLuong' => $vanbanchidao + $vanbandaxuly_stcph,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Văn bản chủ trì đã chỉ đạo',
                                        'Link2' => base_url().'vanbandachidao',
                                        'SoLuong2' => $vanbanchidao,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Văn bản STC phối hợp đã chỉ đạo',
                                        'Link2' => base_url().'vanbandaxulystcph_tp',
                                        'SoLuong2' => $vanbandaxuly_stcph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '5' => array(
                                'TieuDe' => 'DSVB đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'vanbanchopheduyet',
                                'SoLuong' => $vbcduyet,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '6' => array(
                                'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                                'Link' => base_url().'dsdexuatgiahan',
                                'SoLuong' => $dsgiahan,
                                'icon' => 'fa fa-clock-o',
                                'thongbao' => 1
                            ),
                            '7' => array(
                                'TieuDe' => 'DSVB đã đề xuất gia hạn giải quyết',
                                'Link' => base_url().'dshandadexuat',
                                'SoLuong' => $dsdadexuat,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '8' => array(
                                'TieuDe' => 'Văn bản người dùng xử lý',
                                'Link' => base_url().'dsvanbanden?id='.$vanban['PK_iMaCB'],
                                'SoLuong' => $canboxuly,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '9' => array(
                                'TieuDe' => 'Tổng số lượng Tổ công tác giao phòng xử lý',
                                'SoLuong' => $dstocongtac + $dstocongtac_ht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                        'Link2' => base_url().'dstocongtac',
                                        'SoLuong2' => $dstocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                        'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                        'SoLuong2' => $dstocongtac_ht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '10' => array(
                                'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao phòng xử lý',
                                'SoLuong' => $dsdonthu + $dsdonthuht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                        'Link2' => base_url().'dsdonthu',
                                        'SoLuong2' => $dsdonthu,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                        'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                        'SoLuong2' => $dsdonthuht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '11' => array(
                                'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                                'SoLuong' => $dschutri + $dsphoihop,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                        'Link2' => base_url().'dsphongchutri',
                                        'SoLuong2' => $dschutri,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                        'Link2' => base_url().'dsphoihop',
                                        'SoLuong2' => $dsphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '12' => array(
                                'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                                'Link' => base_url().'dexuatphoihop_pp',
                                'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '13' => array(
                                'TieuDe' => 'Công việc phối hợp chờ xử lý',
                                'Link' => base_url().'dexuatphoihop_pp_ph',
                                'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            )
                        ),
                        'II' => array(
                            '1' => array(
                                'TieuDe' => 'DS giấy mời chờ xử lý',
                                'SoLuong' => $gmcxl + $gmph + $gm_phophongph,
                                'icon' => 'fa fa-file-text-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'GM giao chủ trì chờ xử lý',
                                        'Link2' => base_url().'giaymoi_pp',
                                        'SoLuong2' => $gmcxl,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'GM phó phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'giaymoiphophongphoihop',
                                        'SoLuong2' => $gm_phophongph,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'Giấy mời phối hợp chờ xử lý',
                                        'Link2' => base_url().'giaymoiphoihop',
                                        'SoLuong2' => $gmph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),

                            '2' => array(
                                'TieuDe' => 'DS giấy mời đã chỉ đạo',
                                'Link' => base_url().'giaymoidaxuly',
                                'SoLuong' => $gmdxl,
                                'icon' => 'fa fa-file-text-o'
                            ),

                            '3' => array(
                                'TieuDe' => 'DSGM đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'giaymoichoduyet',
                                'SoLuong' => $vbduyetgiaymoi,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '4' => array(
                                'TieuDe' => 'Xem lịch công tác',
                                'Link' => base_url().'lichcongtac',
                                'SoLuong' => '',
                                'icon' => 'fa fa-calendar'
                            ),
                            '5' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                                'Link' => base_url().'dssoanthaodagui',
                                'SoLuong' => $dagui,
                                'icon' => 'fa fa-share-square-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Giấy mời đã gửi đi bị từ chối',
                                'Link' => base_url().'dssoanthaobituchoi',
                                'SoLuong' => $daguibituchoi,
                                'icon' => 'fa fa-ban',
                                'hienthi' => 1
                            ),
                            '7' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                                'Link' => base_url().'dsgiaymoiketluanmoi',
                                'SoLuong' => $chuaxem,
                                'icon' => 'fa fa-flag',
                                'thongbao' => 1
                            ),
                            '8' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                                'Link' => base_url().'dsgiaymoiketluandaxem',
                                'SoLuong' => $daxem,
                                'icon' => 'fa fa-flag-o'
                            )
                        ),
                        'III' => array(
                            '1' => array(
                                'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                                'Link' => base_url().'vanbandichoxuly_pp',
                                'SoLuong' => $vanbandichoxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '2' => array(
                                'TieuDe' => 'Văn bản chủ trì đã xử lý',
                                'Link' => base_url().'vanbandidaxuly_pp',
                                'SoLuong' => $vanbandidaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Văn bản đã trình ký',
                                'Link' => base_url().'vanbanditrinhky',
                                'SoLuong' => $vanbanditrinhky,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => 'DS Văn bản của phòng',
                                'Link' => base_url().'vanbanphongban',
                                'SoLuong' => $vanbandi_phong,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '5' => array(
                                'TieuDe' => 'DS Giấy mời của phòng',
                                'Link' => base_url().'giaymoiphongban',
                                'SoLuong' => $giaymoidi_phong,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '6' => array(
                                'TieuDe' => 'Nhập văn bản đi',
                                'Link' => base_url().'vanban',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )
                        ),
                        'IV' => array(
                            '1' => array(
                                'TieuDe' => 'Thông tin mới',
                                'Link' => base_url().'thongtinmoi',
                                'SoLuong' => count($tinmoi),
                                'icon' => 'fa fa-info-circle',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Thông tin đã gửi đi',
                                'Link' => base_url().'thongtinguidi',
                                'SoLuong' => count($tindagui),
                                'icon' => 'fa fa-info-circle'
                            ),
                            '3' => array(
                                'TieuDe' => 'Thông tin đã xem',
                                'Link' => base_url().'thongtindaxem',
                                'SoLuong' => $tindaxem,
                                'icon' => 'fa fa-info-circle'
                            ),
                            '4' => array(
                                'TieuDe' => 'Soạn thảo thông tin',
                                'Link' => base_url().'soanthaothongtin',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )

                        ),
                        'V' => array(
                            '1' => array(
                                'TieuDe' => 'DS đầu việc giao phòng xử lý',
                                'SoLuong' => $dauviecchutrichoxuly + $dauviecphoihopchoxuly + $dauviecchutriphchoxuly,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Đầu việc phòng chủ trì chờ xử lý',
                                        'Link2' => base_url().'dauviecchoxuly_pp_cc',
                                        'SoLuong2' => $dauviecchutrichoxuly,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => ' Đầu việc phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'dauviecchoxulyph_pp_cc',
                                        'SoLuong2' => $dauviecphoihopchoxuly,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'Đầu việc phó phòng phối hợp (Phòng CT)',
                                        'Link2' => base_url().'dauviecchoxulyphct_pp_cc',
                                        'SoLuong2' => $dauviecchutriphchoxuly,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '2' => array(
                                'TieuDe' => 'DS đầu việc đã chỉ đạo',
                                'Link' => base_url().'dauviecdaxuly_pp_cc',
                                'SoLuong' => $dauviecchutridaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Tổng số đầu việc giao phòng xử lý',
                                'SoLuong' => $dauviecphongchutri + $dauviecphongphoihop,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số lượng đầu việc phòng chủ trì',
                                        'Link2' => base_url().'dsdauviecphongchutri',
                                        'SoLuong2' => $dauviecphongchutri,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số lượng đầu việc phòng phối hợp',
                                        'Link2' => base_url().'dsdauviecphongphoihop',
                                        'SoLuong2' => $dauviecphongphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '4' => array(
                                'TieuDe' => 'Đầu việc hoàn thành chờ phó phòng duyệt',
                                'Link' => base_url().'duyetketqua',
                                'SoLuong' => $duyetketqua,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => 'Đầu việc hoàn thành trả lại',
                                'Link' => base_url().'dauviecchoduyet',
                                'SoLuong' => 0,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Đầu việc trả lại',
                                'Link' => base_url().'dauviectralai',
                                'SoLuong' => $dauviectralai,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => 'Nhập mới',
                                'Link' => base_url().'addqldv',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            )

                        ),
                        'VI' => array(
                            '1' => array(
                                'TieuDe' => ' Nhập, giao, nhận xét kế hoạch-kết quả công việc tuần',
                                'Link' => base_url().'kehoachcongtac',
                                'SoLuong' => $tong_sl_cv,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Tự đánh giá kết quả công tác tuần của cá nhân',
                                'Link' => base_url().'xemkehoachcongtac/'.$week,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Tự đánh giá kết quả công tác tháng của cá nhân',
                                'Link' => base_url().'xemkehoachcongtacthang/'.$taikhoan.'/'.$month,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => ' Xem, nhận xét kết quả công tác tuần của chuyên viên ',
                                'Link' => base_url().'dskehoachcongtac/'.$week,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => ' Xem kết quả phân loại tháng của phòng',
                                'Link' => base_url().'dskehoachcongtacthang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Xem kết quả công tác tuần của các phòng trong toàn sở',
                                'Link' => base_url().'dskehoachcongtactuanld',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => 'Xem kết quả công tác tháng của toàn sở',
                                'Link' => base_url().'dskehoachcongtacthangtp',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '8' => array(
                                'TieuDe' => 'Thống kê kết quả công tác tuần trong toàn sở',
                                'Link' => base_url().'thongkebaocao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '9' => array(
                                'TieuDe' => 'Thống kê kết quả công tác tháng trong toàn sở',
                                'Link' => base_url().'thongkebaocaothang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            )                            
                        ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )
                    );
                }else{
                    $Quyen = array(
                        'I' => array(
                            '1' => array(
                                'TieuDe' => 'Danh sách văn bản quá hạn',
                                'Link' => base_url().'dsvanbanquahan',
                                'SoLuong' => $tongvbquahan,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '2' => array(
                                'TieuDe' => 'DS văn bản chờ xử lý',
                                'SoLuong' => $vbcxlcvp + $congtacdang + $tocongtac+ $pph + $phophongphoihop + $theodoivanban,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'VB chủ trì chờ xử lý',
                                        'Link2' => base_url().'vanbanchoxuly_pp',
                                        'SoLuong2' => $vbcxlcvp,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                        'Link2' => base_url().'congtacdang_pp',
                                        'SoLuong2' => $congtacdang,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'VB tổ công tác chờ xử lý',
                                        'Link2' => base_url().'tocongtac_pp',
                                        'SoLuong2' => $tocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '4' => array(
                                        'TieuDe2' => 'VB phó phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'phophongphoihop',
                                        'SoLuong2' => $phophongphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '5' => array(
                                        'TieuDe2' => 'VB Phó phòng biết để đôn đốc',
                                        'Link2' => base_url().'vanbantheodoi_pp',
                                        'SoLuong2' => $theodoivanban,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '6' => array(
                                        'TieuDe2' => 'VB phối hợp chờ xử lý',
                                        'Link2' => base_url().'phongphoihop',
                                        'SoLuong2' => $pph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '3' => array(
                                'TieuDe' => ' DS văn bản lãnh đạo sở giao chờ xử lý:',
                                'SoLuong' => $tongphophonglanhdaogiao + $tongphoihopthuly,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                    'Cap2' => array(
                                        '1' => array(
                                            'TieuDe2' => 'DS công việc lãnh đạo sở giao chờ xử lý',
                                            'Link2' => base_url().'ppchiaviec',
                                            'SoLuong2' => $tongphophonglanhdaogiao,
                                            'icon' => 'fa fa-caret-right'
                                        ),
                                        '2' => array(
                                            'TieuDe2' => 'DS CV lãnh đạo giao phối hợp chờ xử lý',
                                            'Link2' => base_url().'ldphchiaviec',
                                            'SoLuong2' => $tongphoihopthuly,
                                            'icon' => 'fa fa-caret-right'
                                        ),
                                        '3' => array(
                                            'TieuDe2' => 'DS CV LĐ Sở giao đang thực hiện',
                                            'Link2' => base_url().'dsppdangchidao',
                                            'SoLuong2' => '',
                                            'icon' => 'fa fa-caret-right'
                                        ),
                                        '4' => array(
                                            'TieuDe2' => 'DS CV phối hợp LĐ Sở giao đang thực hiện',
                                            'Link2' => base_url().'ldphdangthuchien',
                                            'SoLuong2' => '',
                                            'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '4' => array(
                                'TieuDe' => 'DS văn bản đã chỉ đạo',
                                'SoLuong' => $vanbanchidao + $vanbandaxuly_stcph,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Văn bản chủ trì đã chỉ đạo',
                                        'Link2' => base_url().'vanbandachidao',
                                        'SoLuong2' => $vanbanchidao,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Văn bản STC phối hợp đã chỉ đạo',
                                        'Link2' => base_url().'vanbandaxulystcph_tp',
                                        'SoLuong2' => $vanbandaxuly_stcph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '5' => array(
                                'TieuDe' => 'DSVB đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'vanbanchopheduyet',
                                'SoLuong' => $vbcduyet,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '6' => array(
                                'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                                'Link' => base_url().'dsdexuatgiahan',
                                'SoLuong' => $dsgiahan,
                                'icon' => 'fa fa-clock-o',
                                'thongbao' => 1
                            ),
                            '7' => array(
                                'TieuDe' => 'DSVB đã đề xuất gia hạn giải quyết',
                                'Link' => base_url().'dshandadexuat',
                                'SoLuong' => $dsdadexuat,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '8' => array(
                                'TieuDe' => 'Văn bản người dùng xử lý',
                                'Link' => base_url().'dsvanbanden?id='.$vanban['PK_iMaCB'],
                                'SoLuong' => $canboxuly,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '9' => array(
                                'TieuDe' => 'Tổng số lượng Tổ công tác giao phòng xử lý',
                                'SoLuong' => $dstocongtac + $dstocongtac_ht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                        'Link2' => base_url().'dstocongtac',
                                        'SoLuong2' => $dstocongtac,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                        'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                        'SoLuong2' => $dstocongtac_ht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '10' => array(
                                'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao phòng xử lý',
                                'SoLuong' => $dsdonthu + $dsdonthuht,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                        'Link2' => base_url().'dsdonthu',
                                        'SoLuong2' => $dsdonthu,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                        'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                        'SoLuong2' => $dsdonthuht,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '11' => array(
                                'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                                'SoLuong' => $dschutri + $dsphoihop,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                        'Link2' => base_url().'dsphongchutri',
                                        'SoLuong2' => $dschutri,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                        'Link2' => base_url().'dsphoihop',
                                        'SoLuong2' => $dsphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                             '12' => array(
                                'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                                'Link' => base_url().'dexuatphoihop_pp',
                                'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '13' => array(
                                'TieuDe' => 'Công việc phối hợp chờ xử lý',
                                'Link' => base_url().'dexuatphoihop_pp_ph',
                                'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            )
                        ),
                        'II' => array(
                            '1' => array(
                                'TieuDe' => 'DS giấy mời chờ xử lý',
                                'SoLuong' => $gmcxl + $gmph + $gm_phophongph,
                                'icon' => 'fa fa-file-text-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'GM giao chủ trì chờ xử lý',
                                        'Link2' => base_url().'giaymoi_pp',
                                        'SoLuong2' => $gmcxl,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'GM phó phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'giaymoiphophongphoihop',
                                        'SoLuong2' => $gm_phophongph,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'Giấy mời phối hợp chờ xử lý',
                                        'Link2' => base_url().'giaymoiphoihop',
                                        'SoLuong2' => $gmph,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),

                            '2' => array(
                                'TieuDe' => 'DS giấy mời đã chỉ đạo',
                                'Link' => base_url().'giaymoidaxuly',
                                'SoLuong' => $gmdxl,
                                'icon' => 'fa fa-file-text-o'
                            ),

                            '3' => array(
                                'TieuDe' => 'DSGM đã hoàn thành chờ LĐ phòng phê duyệt',
                                'Link' => base_url().'giaymoichoduyet',
                                'SoLuong' => $vbduyetgiaymoi,
                                'icon' => 'fa fa-check-square-o',
                                'thongbao' => 1
                            ),
                            '4' => array(
                                'TieuDe' => 'Xem lịch công tác',
                                'Link' => base_url().'lichcongtac',
                                'SoLuong' => '',
                                'icon' => 'fa fa-calendar'
                            ),
                            '5' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                                'Link' => base_url().'dssoanthaodagui',
                                'SoLuong' => $dagui,
                                'icon' => 'fa fa-share-square-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Giấy mời đã gửi đi bị từ chối',
                                'Link' => base_url().'dssoanthaobituchoi',
                                'SoLuong' => $daguibituchoi,
                                'icon' => 'fa fa-ban',
                                'hienthi' => 1
                            ),
                            '7' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                                'Link' => base_url().'dsgiaymoiketluanmoi',
                                'SoLuong' => $chuaxem,
                                'icon' => 'fa fa-flag',
                                'thongbao' => 1
                            ),
                            '8' => array(
                                'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                                'Link' => base_url().'dsgiaymoiketluandaxem',
                                'SoLuong' => $daxem,
                                'icon' => 'fa fa-flag-o'
                            )
                        ),
                        'III' => array(
                            '1' => array(
                                'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                                'Link' => base_url().'vanbandichoxuly_pp',
                                'SoLuong' => $vanbandichoxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '2' => array(
                                'TieuDe' => 'Văn bản chủ trì đã xử lý',
                                'Link' => base_url().'vanbandidaxuly_pp',
                                'SoLuong' => $vanbandidaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Văn bản đã trình ký',
                                'Link' => base_url().'vanbanditrinhky',
                                'SoLuong' => $vanbanditrinhky,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '4' => array(
                                'TieuDe' => 'DS Văn bản của phòng',
                                'Link' => base_url().'vanbanphongban',
                                'SoLuong' => $vanbandi_phong,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '5' => array(
                                'TieuDe' => 'DS Giấy mời của phòng',
                                'Link' => base_url().'giaymoiphongban',
                                'SoLuong' => $giaymoidi_phong,
                                'icon' => 'fa fa-list-alt'
                            ),
                            '6' => array(
                                'TieuDe' => 'Nhập văn bản đi',
                                'Link' => base_url().'vanban',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )
                        ),
                        'IV' => array(
                            '1' => array(
                                'TieuDe' => 'Thông tin mới',
                                'Link' => base_url().'thongtinmoi',
                                'SoLuong' => count($tinmoi),
                                'icon' => 'fa fa-info-circle',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Thông tin đã gửi đi',
                                'Link' => base_url().'thongtinguidi',
                                'SoLuong' => count($tindagui),
                                'icon' => 'fa fa-info-circle'
                            ),
                            '3' => array(
                                'TieuDe' => 'Thông tin đã xem',
                                'Link' => base_url().'thongtindaxem',
                                'SoLuong' => $tindaxem,
                                'icon' => 'fa fa-info-circle'
                            ),
                            '4' => array(
                                'TieuDe' => 'Soạn thảo thông tin',
                                'Link' => base_url().'soanthaothongtin',
                                'SoLuong' => '',
                                'icon' => 'fa fa-edit'
                            )

                        ),
                        'V' => array(
                            '1' => array(
                                'TieuDe' => 'DS đầu việc giao phòng xử lý',
                                'SoLuong' => $dauviecchutrichoxuly + $dauviecphoihopchoxuly + $dauviecchutriphchoxuly,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1,
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Đầu việc phòng chủ trì chờ xử lý',
                                        'Link2' => base_url().'dauviecchoxuly_pp',
                                        'SoLuong2' => $dauviecchutrichoxuly,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => ' Đầu việc phòng phối hợp chờ xử lý',
                                        'Link2' => base_url().'dauviecchoxulyph_pp',
                                        'SoLuong2' => $dauviecphoihopchoxuly,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '3' => array(
                                        'TieuDe2' => 'Đầu việc phó phòng phối hợp (Phòng CT)',
                                        'Link2' => base_url().'dauviecchoxulyphct_pp',
                                        'SoLuong2' => $dauviecchutriphchoxuly,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '2' => array(
                                'TieuDe' => 'DS đầu việc đã chỉ đạo',
                                'Link' => base_url().'dauviecdaxuly_pp',
                                'SoLuong' => $dauviecchutridaxuly,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Tổng số đầu việc giao phòng xử lý',
                                'SoLuong' => $dauviecphongchutri + $dauviecphongphoihop,
                                'icon' => 'fa fa-newspaper-o',
                                'Cap2' => array(
                                    '1' => array(
                                        'TieuDe2' => 'Số lượng đầu việc phòng chủ trì',
                                        'Link2' => base_url().'dsdauviecphongchutri',
                                        'SoLuong2' => $dauviecphongchutri,
                                        'icon' => 'fa fa-caret-right'
                                    ),
                                    '2' => array(
                                        'TieuDe2' => 'Số lượng đầu việc phòng phối hợp',
                                        'Link2' => base_url().'dsdauviecphongphoihop',
                                        'SoLuong2' => $dauviecphongphoihop,
                                        'icon' => 'fa fa-caret-right'
                                    )
                                )
                            ),
                            '4' => array(
                                'TieuDe' => 'Đầu việc hoàn thành chờ phó phòng duyệt',
                                'Link' => base_url().'duyetketqua',
                                'SoLuong' => $duyetketqua,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => 'Đầu việc hoàn thành trả lại',
                                'Link' => base_url().'dauviecchoduyet',
                                'SoLuong' => 0,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => 'Đầu việc trả lại',
                                'Link' => base_url().'dauviectralai',
                                'SoLuong' => $dauviectralai,
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => 'Nhập mới',
                                'Link' => base_url().'addqldv',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            )

                        ),
                        'VI' => array(
                            '1' => array(
                                'TieuDe' => 'Nhập kế hoạch-đánh giá công việc tuần',
                                'Link' => base_url().'kehoachcongtac',
                                'SoLuong' => $tong_sl_cv,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                            '2' => array(
                                'TieuDe' => 'Tự đánh giá kết quả công tác tuần của cá nhân',
                                'Link' => base_url().'xemkehoachcongtac/'.$week,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '3' => array(
                                'TieuDe' => 'Tự đánh giá kết quả công tác tháng của cá nhân',
                                'Link' => base_url().'xemkehoachcongtacthang/'.$taikhoan.'/'.$month,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            
                            '4' => array(
                                'TieuDe' => 'Xem kết quả công tác tuần của phòng',
                                'Link' => base_url().'dskehoachcongtac/'.$week,
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '5' => array(
                                'TieuDe' => 'Xem kết quả phân loại tháng của phòng',
                                'Link' => base_url().'dskehoachcongtacthang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '6' => array(
                                'TieuDe' => ' Xem kết quả công tác tuần của các phòng trong toàn sở',
                                'Link' => base_url().'dskehoachcongtactuanld',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '7' => array(
                                'TieuDe' => ' Xem kết quả công tác tháng của toàn sở',
                                'Link' => base_url().'dskehoachcongtacthangtp',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '8' => array(
                                'TieuDe' => ' Thống kê kết quả công tác tuần trong toàn sở',
                                'Link' => base_url().'thongkebaocao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '9' => array(
                                'TieuDe' => ' Thống kê kết quả công tác tháng trong toàn sở',
                                'Link' => base_url().'thongkebaocaothang',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            ),
                            '10' => array(
                                'TieuDe' => ' DS cán bộ tham mưu, đề xuất sáng tạo đổi mới trong tháng',
                                'Link' => base_url().'dssangtao',
                                'SoLuong' => '',
                                'icon' => 'fa fa-newspaper-o'
                            )


                        ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )
                    );
                }
                $result = $Quyen;
            }
            /** CHUYEN VIEN **/
            if($vanban['iQuyenHan_DHNB'] == 8){
                $getDocAwaitPPH = $this->Mdmadmin->getDocAwaitCVPPH(NULL,NULL,$vanban['PK_iMaCB'],'2',NULL,NULL,1);
                $vbcxlcvp = $this->Mdmadmin->getDocAwait('7',NULL,NULL,$vanban['PK_iMaCB'],NULL,NULL,NULL,1);
                $congtacdang = $this->Mdmadmin->getCTD('7',NULL,NULL,$vanban['PK_iMaCB'],NULL,NULL,NULL);
                $tocongtac = $this->Mdmadmin->getDocAwaitTCT('7',NULL,NULL,$vanban['PK_iMaCB'],NULL,NULL,NULL,1);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,$vanban['PK_iMaCB'],1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,NULL,$vanban['PK_iMaCB'],NULL,1);
                $vbph = $getDocAwaitPPH;
                $gmcxl = $this->Mdmadmin->getDocAwait('7',NULL,NULL,$vanban['PK_iMaCB'],'1');
                $gmdxl = $this->Mdmadmin->getDocComplete(NULL,$vanban['PK_iMaCB']);
                $gmph = $this->Mdmadmin->getDocAwaitCVPPH(NULL,NULL,$vanban['PK_iMaCB'],'1',NULL,NULL,1);
//            pr($data['gmph']);
                $vbcduyet = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, $vanban['PK_iMaCB'], 2);
                $vbhoanthanh = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, $vanban['PK_iMaCB'], 1);
                $vbduyetgiaymoi = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, $vanban['PK_iMaCB'], 2,NULL,NULL,1);
//            pr($data['gmdxl']);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $dschutri = $this->Mdmadmin->getDocCT($vanban['FK_iMaPhongHD']);
                $dstocongtac = $this->Mdmadmin->getToCongTacHT(NULL,NULL,$vanban['PK_iMaCB']);
                $dstocongtac_ht = $this->Mdmadmin->getToCongTacHT(NULL,2,$vanban['PK_iMaCB']);
                $dsdadexuat = $this->Mdmadmin->layVBGiaHanTuChoi(NULL,NULL,$vanban['PK_iMaCB']);
                $dsphoihop = $this->Mdmadmin->getDocAwaitDSCTPPH($vanban['FK_iMaPhongHD']);
                $dsphoihopcv = $this->Mdmadmin->getListCVPH(NULL,$vanban['PK_iMaCB']);
                $canboxuly = $this->Mdmadmin->countDocGo($vanban['PK_iMaCB']);
                $daguibituchoi = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB'],2);

                $trangthai_tk       = 7;
                $trangthai =1;
                $ten_taikhoan       = 'chuyenvien';
                $ykien              = 'ykien_cv';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';

                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $duyetketqua            = $this->Mketqua->demTTKT($trangthaiduyet,$phongban,$taikhoan);
                $tong_sl_cv = $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND (( active = 3 and canbo_id = ".$vanban['PK_iMaCB'].") or ( active <= 3 and phoihop like '%[".$vanban['PK_iMaCB']."]%')) and tuan =". $week);

                $tongchuyenvienlanhdaogiao = $this->Mdanhmuc->countchuyenvienthuly($vanban['PK_iMaCB']);

                $tongphoihopthuly = $this->Mdanhmuc->countphoihopthuly($vanban['PK_iMaCB']);

                $Quyen = array(
                    'I' => array(
                        '1' => array(
                            'TieuDe' => 'Danh sách văn bản quá hạn',
                            'Link' => base_url().'dsvanbanquahan',
                            'SoLuong' => $tongvbquahan,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'DS văn bản chờ xử lý',
                            'SoLuong' => $vbcxlcvp + $congtacdang + $vbph,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Văn bản chủ trì chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_cv',
                                    'SoLuong2' => $vbcxlcvp,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                    'Link2' => base_url().'congtacdang_cv',
                                    'SoLuong2' => $congtacdang,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => 'Văn bản phối hợp chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_cvph',
                                    'SoLuong2' => $vbph,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '3' => array(
                            'TieuDe' => 'DS công việc lãnh đạo sở giao:',
                            'SoLuong' => $tongchuyenvienlanhdaogiao + $tongphoihopthuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => ' DS công việc lãnh đạo sở giao chờ xử lý',
                                    'Link2' => base_url().'dschuyenvienxuly',
                                    'SoLuong2' => $tongchuyenvienlanhdaogiao,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DS CV lãnh đạo giao phối hợp chờ xử lý',
                                    'Link2' => base_url().'dscvphxuly',
                                    'SoLuong2' => $tongphoihopthuly,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '4' => array(
                            'TieuDe' => 'DS văn bản hoàn thành',
                            'SoLuong' => $vbcduyet + $vbhoanthanh,
                            'icon' => 'fa fa-check-square-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Văn bản hoàn thành chờ phê duyệt',
                                    'Link2' => base_url().'vanbanchopheduyet',
                                    'SoLuong2' => $vbcduyet,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Văn bản hoàn thành đã duyệt',
                                    'Link2' => base_url().'vanbanchopheduyet?cacloaivanban=2',
                                    'SoLuong2' => $vbhoanthanh,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '5' => array(
                            'TieuDe' => 'DS văn bản người dùng',
                            'SoLuong' => $canboxuly + $dsphoihopcv,
                            'icon' => 'fa fa-list-alt',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Văn bản người dùng chủ trì xử lý',
                                    'Link2' => base_url().'dsvanbanden?id='.$vanban['PK_iMaCB'],
                                    'SoLuong2' => $canboxuly,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Văn bản người dùng phối hợp',
                                    'Link2' => base_url().'dsnguoidungphoihop',
                                    'SoLuong2' => $dsphoihopcv,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '6' => array(
                            'TieuDe' => 'Tổng số lượng Tổ công tác giao xử lý',
                            'SoLuong' => $dstocongtac + $dstocongtac_ht,
                            'icon' => 'fa fa-newspaper-o',
                            'hienthi' => ((int)($dstocongtac + $dstocongtac_ht) != 0) ? 1 : 0,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                    'Link2' => base_url().'dstocongtac',
                                    'SoLuong2' => $dstocongtac,
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($dstocongtac) ? 1 : 0
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                    'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                    'SoLuong2' => $dstocongtac_ht,
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($dstocongtac_ht) ? 1 : 0
                                )
                            )
                        ),
                        '7' => array(
                            'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao xử lý',
                            'SoLuong' => $dsdonthu + $dsdonthuht,
                            'icon' => 'fa fa-newspaper-o',
                            'hienthi' => ((int)($dsdonthu + $dsdonthuht) != 0) ? 1 : 0,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                    'Link2' => base_url().'dsdonthu',
                                    'SoLuong2' => $dsdonthu,
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($dsdonthu) ? 1 : 0
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                    'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                    'SoLuong2' => $dsdonthuht,
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($dsdonthuht) ? 1 : 0
                                )
                            )
                        ),
                        '8' => array(
                            'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                            'SoLuong' => $dschutri + $dsphoihop,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                    'Link2' => base_url().'dsphongchutri',
                                    'SoLuong2' => $dschutri,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                    'Link2' => base_url().'dsphoihop',
                                    'SoLuong2' => $dsphoihop,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '9' => array(
                                'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                                'Link' => base_url().'dexuatphoihop_cv',
                                'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                        '10' => array(
                            'TieuDe' => 'Công việc phối hợp chờ xử lý',
                            'Link' => base_url().'dexuatphoihop_cv_ph',
                            'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        )
                    ),
                    'II' => array(
                        '1' => array(
                            'TieuDe' => 'DS giấy mời chờ xử lý',
                            'SoLuong' => $gmcxl + $gmph,
                            'thongbao' => 1,
                            'icon' => 'fa fa-file-text-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'GM giao chủ trì chờ xử lý',
                                    'Link2' => base_url().'giaymoi_cv',
                                    'SoLuong2' => $gmcxl,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Giấy mời phối hợp chờ xử lý',
                                    'Link2' => base_url().'giaymoi_cvph',
                                    'SoLuong2' => $gmph,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),

                        '2' => array(
                            'TieuDe' => 'Giấy mời đã xử lý',
                            'Link' => base_url().'giaymoidagiaiquyet',
                            'SoLuong' => $gmdxl,
                            'icon' => 'fa fa-file-text-o'
                        ),

                        '3' => array(
                            'TieuDe' => 'Giấy mời hoàn thành chờ phê duyệt',
                            'Link' => base_url().'giaymoichoduyet',
                            'SoLuong' => $vbduyetgiaymoi,
                            'icon' => 'fa fa-check-square-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Xem lịch công tác',
                            'Link' => base_url().'lichcongtac',
                            'SoLuong' => '',
                            'icon' => 'fa fa-calendar'
                        ),
                        '5' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                            'Link' => base_url().'dssoanthaodagui',
                            'SoLuong' => $dagui,
                            'icon' => 'fa fa-share-square-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Giấy mời đã gửi đi bị từ chối',
                            'Link' => base_url().'dssoanthaobituchoi',
                            'SoLuong' => $daguibituchoi,
                            'icon' => 'fa fa-ban',
                            'hienthi' => ($daguibituchoi) ? 1 : 0
                        ),
                        '7' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                            'Link' => base_url().'dsgiaymoiketluanmoi',
                            'SoLuong' => $chuaxem,
                            'icon' => 'fa fa-flag'
                        ),
                        '8' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                            'Link' => base_url().'dsgiaymoiketluandaxem',
                            'SoLuong' => $daxem,
                            'icon' => 'fa fa-flag-o'
                        )
                    ),
                    'III' => array(
                        '1' => array(
                            'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                            'Link' => base_url().'vanbandichoxuly_cv',
                            'SoLuong' => $vanbandichoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Văn bản chủ trì đã xử lý',
                            'Link' => base_url().'vanbandidaxuly_cv',
                            'SoLuong' => $vanbandidaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Văn bản đã trình ký',
                            'Link' => base_url().'vanbanditrinhky',
                            'SoLuong' => $vanbanditrinhky,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'DS Văn bản của phòng',
                            'Link' => base_url().'vanbanphongban',
                            'SoLuong' => $vanbandi_phong,
                            'icon' => 'fa fa-list-alt'
                        ),
                        '5' => array(
                            'TieuDe' => 'DS Giấy mời của phòng',
                            'Link' => base_url().'giaymoiphongban',
                            'SoLuong' => $giaymoidi_phong,
                            'icon' => 'fa fa-list-alt'
                        ),
                        '6' => array(
                            'TieuDe' => 'Nhập văn bản đi',
                            'Link' => base_url().'vanban',
                            'SoLuong' => '',
                            'icon' => 'fa fa-edit'
                        )
                    ),
                    'IV' => array(
                        '1' => array(
                            'TieuDe' => 'Thông tin mới',
                            'Link' => base_url().'thongtinmoi',
                            'SoLuong' => count($tinmoi),
                            'icon' => 'fa fa-info-circle',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Thông tin đã gửi đi',
                            'Link' => base_url().'thongtinguidi',
                            'SoLuong' => count($tindagui),
                            'icon' => 'fa fa-info-circle'
                        ),
                        '3' => array(
                            'TieuDe' => 'Thông tin đã xem',
                            'Link' => base_url().'thongtindaxem',
                            'SoLuong' => $tindaxem,
                            'icon' => 'fa fa-info-circle'
                        ),
                        '4' => array(
                            'TieuDe' => 'Soạn thảo thông tin',
                            'Link' => base_url().'soanthaothongtin',
                            'SoLuong' => '',
                            'icon' => 'fa fa-edit'
                        )

                    ),
                    'V' => array(
                        '1' => array(
                            'TieuDe' => 'DS đầu việc giao phòng xử lý',
                            'SoLuong' => $dauviecchutrichoxuly + $dauviecphoihopchoxuly + $dauviecchutriphchoxuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Đầu việc phòng chủ trì chờ xử lý',
                                    'Link2' => base_url().'dauviecchoxuly_cv',
                                    'SoLuong2' => $dauviecchutrichoxuly,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => ' Đầu việc phòng phối hợp chờ xử lý',
                                    'Link2' => base_url().'dauviecchoxulyph_cv',
                                    'SoLuong2' => $dauviecphoihopchoxuly,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => 'Đầu việc phó phòng phối hợp (Phòng CT)',
                                    'Link2' => base_url().'dauviecchoxulyphct_cv',
                                    'SoLuong2' => $dauviecchutriphchoxuly,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '2' => array(
                            'TieuDe' => 'Tổng số đầu việc giao phòng xử lý',
                            'SoLuong' => $dauviecphongchutri + $dauviecphongphoihop,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số lượng đầu việc phòng chủ trì',
                                    'Link2' => base_url().'dsdauviecphongchutri',
                                    'SoLuong2' => $dauviecphongchutri,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số lượng đầu việc phòng phối hợp',
                                    'Link2' => base_url().'dsdauviecphongphoihop',
                                    'SoLuong2' => $dauviecphongphoihop,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '3' => array(
                            'TieuDe' => 'Đầu việc hoàn thành trả lại',
                            'Link' => base_url().'dauviecchoduyet',
                            'SoLuong' => $duyetketqua,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Đầu việc trả lại',
                            'Link' => base_url().'dauviectralai',
                            'SoLuong' => $dauviectralai,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Nhập mới',
                            'Link' => base_url().'addqldv',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VI' => array(
                        '1' => array(
                            'TieuDe' => 'Nhập kế hoạch-đánh giá công việc tuần',
                            'Link' => base_url().'kehoachcongtac',
                            'SoLuong' => $tong_sl_cv,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác tuần của cá nhân',
                            'Link' => base_url().'xemkehoachcongtac/'.$week,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác tháng của cá nhân',
                            'Link' => base_url().'xemkehoachcongtacthang/'.$taikhoan.'/'.$month,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác quý của cá nhân',
                            'Link' => base_url().'xemkehoachcongtacquy/'.$taikhoan.'/'.$quy,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Xem kết quả công tác tuần của phòng',
                            'Link' => base_url().'dskehoachcongtac/'.$week,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Xem kết quả phân loại tháng của phòng',
                            'Link' => base_url().'dskehoachcongtacthang',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Xem kết quả phân loại quý của phòng',
                            'Link' => base_url().'dskehoachcongtacquy',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )
                );
                $result = $Quyen;
            }

            /** PHO CHI CUC **/
            if($vanban['iQuyenHan_DHNB'] == 10){
                $vbcxlcvp = $this->Mdmadmin->getDocConcacAwait('6','2',NULL,NULL,$vanban['PK_iMaCB']);
                $vanbanchidao_pcc = $this->Mdmadmin->getPConCac($vanban['PK_iMaCB']);
                $vanbandaxuly_stcphccp = $this->Mdmadmin->getPConCac($vanban['PK_iMaCB'],NULL,1);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,$vanban['PK_iMaCB']);
                $gmdxl_pcc = $this->Mdmadmin->getPConCac($vanban['PK_iMaCB'],'1');
                $gmcxl = $this->Mdmadmin->getDocConcacAwait('6','2',NULL,NULL,$vanban['PK_iMaCB'],'1');
                $vbcduyet = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB']);
                $vbduyetgiaymoi = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB'],1);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $dschutri = $this->Mdmadmin->getDocCT($vanban['FK_iMaPhongHD']);
                $dsphoihop = $this->Mdmadmin->getDocAwaitDSCTPPH($vanban['FK_iMaPhongHD']);
                $daguibituchoi = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB'],2);

                $trangthaikq = 1;
                $trangthai_tk       = 4;
                $trangthai =4;
                $ten_taikhoan       = 'phochicuc';
                $ykien              = 'ykien_pcc';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';

                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $tong_sl_cv = $this->Mdanhmuc->count_kehoach(" active =1 and vanban_id IS NULL AND canbo_id = ".$vanban['PK_iMaCB']." and tuan =". $week);

                $tongccplanhdaogiao = $this->Mdanhmuc->countccpthuly($vanban['PK_iMaCB']);

                $tongphoihopthuly = $this->Mdanhmuc->countphoihopthuly($vanban['PK_iMaCB']);

                $Quyen = array(
                    'I' => array(
                        '1' => array(
                            'TieuDe' => 'Danh sách văn bản quá hạn của phòng',
                            'Link' => base_url().'vanbanchoxuly_ccp',
                            'SoLuong' => (int)$tongvbquahan,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Văn bản phòng chủ trì chờ xử lý',
                            'Link' => base_url().'vanbanchoxuly_ccp',
                            'SoLuong' => (int)$vbcxlcvp,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '3' => array(
                            'TieuDe' => ' DS công việc lãnh đạo sở giao:',
                            'SoLuong' => $tongccplanhdaogiao+$tongphoihopthuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Danh sách công việc lãnh đạo sở giao phòng',
                                    'Link2' => base_url().'ccpchiaviec',
                                    'SoLuong2' => $tongccplanhdaogiao,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DS công việc lãnh đạo sở giao phòng phối hợp',
                                    'Link2' => base_url().'ldphchiaviec',
                                    'SoLuong2' => $tongphoihopthuly,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => 'DS công việc LĐ Sở giao phòng đang thực hiện',
                                    'Link2' => base_url().'ccpchidaodangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '4' => array(
                                    'TieuDe2' => 'DS công việc phối hợp LĐ Sở giao phòng đang thực hiện',
                                    'Link2' => base_url().'ldphdangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '4' => array(
                            'TieuDe' => 'Văn bản đã chỉ đạo (chưa hoàn thành)',
                            'Link' => base_url().'vanbandaxuly_ccp',
                            'SoLuong' => (int)$vanbanchidao_pcc,
                            'icon' => 'fa fa-newspaper-o',
                        ),
                        '5' => array(
                            'TieuDe' => 'Văn bản đã chỉ đạo (STC phối hợp)',
                            'Link' => base_url().'vanbandaxulystcph_ccp',
                            'SoLuong' => (int)$vanbandaxuly_stcphccp,
                            'icon' => 'fa fa-newspaper-o',
                        ),
                        '6' => array(
                            'TieuDe' => 'Văn bản hoàn thành chờ phê duyệt',
                            'Link' => base_url().'vanbanchopheduyet',
                            'SoLuong' => (int)$vbcduyet,
                            'icon' => 'fa fa-check-square-o',
                        ),
                        '7' => array(
                            'TieuDe' => 'Danh sách văn bản Đơn thư KNTC',
                            'Link' => base_url().'dsdonthu',
                            'SoLuong' => (int)$dsdonthu,
                            'icon' => 'fa fa-list-alt',
                        ),
                        '8' => array(
                            'TieuDe' => 'Văn bản phòng chủ trì',
                            'Link' => base_url().'dsphongchutri',
                            'SoLuong' => (int)$dschutri,
                            'icon' => 'fa fa-list-alt',
                        ),
                        '9' => array(
                            'TieuDe' => 'Văn bản phòng phối hợp',
                            'Link' => base_url().'dsphoihop',
                            'SoLuong' => (int)$dsphoihop,
                            'icon' => 'fa fa-list-alt',
                        )
                    ),
                    'II' => array(
                        '1' => array(
                            'TieuDe' => 'Giấy mời chờ xử lý',
                            'Link' => base_url().'giaymoi_ccp',
                            'SoLuong' => $gmcxl,
                            'icon' => 'fa fa-file-text-o',
                            'thongbao' => 1
                        ),

                        '2' => array(
                            'TieuDe' => 'Giấy mời đã xử lý',
                            'Link' => base_url().'giaymoidaxuly_ccp',
                            'SoLuong' => $gmdxl_pcc,
                            'icon' => 'fa fa-file-text-o'
                        ),

                        '3' => array(
                            'TieuDe' => 'Giấy mời hoàn thành chờ phê duyệt',
                            'Link' => base_url().'giaymoichoduyet',
                            'SoLuong' => $vbduyetgiaymoi,
                            'icon' => 'fa fa-check-square-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Xem lịch công tác',
                            'Link' => base_url().'lichcongtac',
                            'SoLuong' => '',
                            'icon' => 'fa fa-calendar'
                        ),
                        '5' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                            'Link' => base_url().'dssoanthaodagui',
                            'SoLuong' => $dagui,
                            'icon' => 'fa fa-share-square-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Giấy mời đã gửi đi bị từ chối',
                            'Link' => base_url().'dssoanthaobituchoi',
                            'SoLuong' => $daguibituchoi,
                            'icon' => 'fa fa-ban',
                            'hienthi' => ($daguibituchoi) ? 1 : 0
                        ),
                        '7' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                            'Link' => base_url().'dsgiaymoiketluanmoi',
                            'SoLuong' => $chuaxem,
                            'icon' => 'fa fa-flag',
                            'thongbao' => 1
                        ),
                        '8' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                            'Link' => base_url().'dsgiaymoiketluandaxem',
                            'SoLuong' => $daxem,
                            'icon' => 'fa fa-flag-o'
                        )
                    ),
                    'III' => array(
                        '1' => array(
                            'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                            'Link' => base_url().'vanbandichoxuly_ccp',
                            'SoLuong' => $vanbandichoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Văn bản chủ trì đã xử lý',
                            'Link' => base_url().'vanbandidaxuly_ccp',
                            'SoLuong' => $vanbandidaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'DS Văn bản của phòng',
                            'Link' => base_url().'vanbanphongban',
                            'SoLuong' => $vanbandi_phong,
                            'icon' => 'fa fa-list-alt'
                        ),
                        '4' => array(
                            'TieuDe' => 'DS Giấy mời của phòng',
                            'Link' => base_url().'giaymoiphongban',
                            'SoLuong' => $giaymoidi_phong,
                            'icon' => 'fa fa-list-alt'
                        )
                    ),
                    'IV' => array(
                        '1' => array(
                            'TieuDe' => 'Thông tin mới',
                            'Link' => base_url().'thongtinmoi',
                            'SoLuong' => count($tinmoi),
                            'icon' => 'fa fa-info-circle',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Thông tin đã gửi đi',
                            'Link' => base_url().'thongtinguidi',
                            'SoLuong' => count($tindagui),
                            'icon' => 'fa fa-info-circle'
                        ),
                        '3' => array(
                            'TieuDe' => 'Thông tin đã xem',
                            'Link' => base_url().'thongtindaxem',
                            'SoLuong' => $tindaxem,
                            'icon' => 'fa fa-info-circle'
                        ),
                        '4' => array(
                            'TieuDe' => 'Soạn thảo thông tin',
                            'Link' => base_url().'soanthaothongtin',
                            'SoLuong' => '',
                            'icon' => 'fa fa-edit'
                        )

                    ),
                    'V' => array(
                        '1' => array(
                            'TieuDe' => 'Đầu việc phòng chủ trì chờ xử lý',
                            'Link' => base_url().'dauviecchoxuly_ccp',
                            'SoLuong' => $dauviecchutrichoxuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Đầu việc phòng phối hợp chờ xử lý',
                            'Link' => base_url().'dauviecchoxulyph_ccp',
                            'SoLuong' => $dauviecphoihopchoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Đầu việc đã chỉ đạo (Chưa hoàn thành)',
                            'Link' => base_url().'dauviecdaxuly_ccp',
                            'SoLuong' => $dauviecchutridaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Đầu việc phòng chủ trì',
                            'Link' => base_url().'dsdauviecphongchutri',
                            'SoLuong' => $dauviecphongchutri,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Đầu việc phòng phối hợp',
                            'Link' => base_url().'dsdauviecphongphoihop',
                            'SoLuong' => $dauviecphongphoihop,
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VI' => array(
                        '1' => array(
                            'TieuDe' => 'Nhập kế hoạch-đánh giá công việc tuần',
                            'Link' => base_url().'kehoachcongtac',
                            'SoLuong' => $tong_sl_cv,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác tuần của cá nhân',
                            'Link' => base_url().'xemkehoachcongtac/'.$week,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác tháng của cá nhân',
                            'Link' => base_url().'xemkehoachcongtacthang/'.$taikhoan.'/'.$month,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác quý của cá nhân',
                            'Link' => base_url().'xemkehoachcongtacquy/'.$taikhoan.'/'.$quy,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Xem kết quả công tác tuần của phòng',
                            'Link' => base_url().'dskehoachcongtac/'.$week,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Xem kết quả phân loại tháng của phòng',
                            'Link' => base_url().'dskehoachcongtacthang',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Xem kết quả phân loại quý của phòng',
                            'Link' => base_url().'dskehoachcongtacquy',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )
                );
                $result = $Quyen;
            }
            /** TRUONG PHONG CHI CUC **/
            if($vanban['iQuyenHan_DHNB'] == 11){
                $vbcxlcvp = $this->Mdmadmin->getDocConcacAwait('6','3',NULL,$vanban['PK_iMaCB']);
                $congtacdang = $this->Mdmadmin->getDocCcDangAwait('6','3',NULL,$vanban['PK_iMaCB']);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,$vanban['PK_iMaCB'],1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,NULL,$vanban['PK_iMaCB'],NULL,1);
                // van ban phoi hop
                $pph = $this->Mdmadmin->getDocAwaitPPH(11,$vanban['FK_iMaPhongHD'],$vanban['PK_iMaCB'],'2',NULL,NULL,'1');
                $vanbanchidao = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB']);
                $gmcxl = $this->Mdmadmin->getDocConcacAwait('6','3',NULL,$vanban['PK_iMaCB'],NULL,'1');
                // giay moi da xu ly
                $gmdxl = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB'],$vanban['FK_iMaPhongHD'],'1');
                //giay moi phoi hop cho xu ly
                $gmph = $this->Mdmadmin->getDocAwaitPPH(11,$vanban['FK_iMaPhongHD'],$vanban['PK_iMaCB'],'1','1');
//            pr($this->Mgiaymoichoxuly->getDocAwaitPPH(NULL,$vanban['FK_iMaPhongHD'],NULL,'5'));
                $phdxl = $this->Mdmadmin->getDocAwaitPPHDXL($vanban['iQuyenHan_DHNB'],$vanban['FK_iMaPhongHD'],NULL,'1','6');
                // van ban cho duyet hoan thanh
                $vbcduyet = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB']);
                $vbduyetgiaymoi = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB'],1);
//            pr($data['vanbanchidao']);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $dschutri = $this->Mdmadmin->getDocCT($vanban['FK_iMaPhongHD']);
                $dstocongtac = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB']);
                $dstocongtac_ht = $this->Mdmadmin->getToCongTacHT($vanban['PK_iMaCB'],2);
                $dsphoihop = $this->Mdmadmin->getDocAwaitDSCTPPH($vanban['FK_iMaPhongHD']);
                $dsgiahan = $this->Mdmadmin->dsGiaHan($vanban['PK_iMaCB']);
                $daguibituchoi = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB'],2);

                $canbo              = $taikhoan;
                $trangthaiduyet     = 1;
                $trangthai_tk       = 555;
                $trangthai =333;
                $ten_taikhoan       = 'truongphong';
                $ykien              = 'ykien_tp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';

                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly     = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $duyetketqua            = $this->Mketqua->demTTKT($trangthaiduyet,$phongban,$taikhoan);
                $tong_sl_cv = $this->Mdanhmuc->count_kehoach(" active =1 and vanban_id IS NULL AND canbo_id = ".$vanban['PK_iMaCB']." and tuan =". $week);

                $tongtpcclanhdaogiao = $this->Mdanhmuc->counttpccthuly($vanban['PK_iMaCB']);
                $tongphoihopthuly = $this->Mdanhmuc->countphoihopthuly($vanban['PK_iMaCB']);

                // văn bản cấp hai
        /*$donvi                      = $vanban['FK_iMaPhongHD'];
        $phong_caphai               = $vanban['phong_caphai'];
        $lanhdaochoxuly     = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,1,1,1,NULL,NULL);        
        $lanhdaodaxuly      = $this->Mvanban_caphai->demDSVB_Phong_ChuaXuLy($donvi,$taikhoan);
        
        $phongct_choxuly    = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,2,1,1,NULL,NULL);
        $phongph_choxuly    = $this->Mvanban_caphai->demDSVB_Phong_PH($donvi,$taikhoan,3,1,1,NULL,NULL);
        $phongct_daxuly     = $this->Mvanban_caphai->demDSVB_Phong_ChuaGiaiQuyet($donvi,$taikhoan,NULL);
        $phongph_daxuly     = $this->Mvanban_caphai->demDSVB_Phong_PH_ChuaGiaiQuyet($donvi,$taikhoan,NULL);

        $vanbanchoduyet     = $this->Mvanban_caphai->demDSVB_ChoDuyet($taikhoan);

        $vanbanph_choxuly = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,NULL,1,1,NULL,2);
        $vanbanph_daxuly  = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,NULL,2,1,NULL,2);

        $phongct_choxuly_ppcv    = $this->Mvanban_caphai->demDSVB_Phong_CT($donvi,$taikhoan,2,1,1,NULL,1);
        $phongct_daxuly_ppcv    = $this->Mvanban_caphai->demDSVB_Phong_ChuaGiaiQuyet($donvi,$taikhoan,1);
        
        $phongph_choxuly_ppcv    = $this->Mvanban_caphai->demDSVB_Phong_PH($donvi,$taikhoan,3,1,1,NULL,1);
        $phongph_daxuly_ppcv     = $this->Mvanban_caphai->demDSVB_Phong_PH_ChuaGiaiQuyet($donvi,$taikhoan,1);

        $vanbanphongchutri     = $this->Mvanban_caphai->demDSVB_Phong($donvi,$phong_caphai,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $vanbanphongphoihop     = $this->Mvanban_caphai->demDSVB_Phong($donvi,$phong_caphai,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $vanbantongthe_caphai   = $this->Mvanban_caphai->demVBDen_caphai($donvi,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        // văn bản đi cấp hai
        $nam   = date('Y');
        $vanbandi_caphai      = $this->Mvanbandi_caphai->demDSVB_ChoSo(2,$donvi,$nam,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $vanbandichoso_caphai = $this->Mvanbandi_caphai->demDSVB_ChoSo(1,$donvi,$nam,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $vanbandiphong_caphai = $this->Mvanbandi_caphai->demDSVB_ChoSo(2,$donvi,$nam,NULL,NULL,$phong_caphai,NULL,NULL,NULL,NULL);*/


                $Quyen = array(
                    'I' => array(
                        '1' => array(
                            'TieuDe' => 'Danh sách văn bản quá hạn của phòng',
                            'Link' => base_url().'vanbanchoxuly_ccp',
                            'SoLuong' => $tongvbquahan,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'DS văn bản giao phòng xử lý',
                            'SoLuong' => $vbcxlcvp + $congtacdang + $pph + $tocongtac,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'VB giao phòng chủ trì chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_tp',
                                    'SoLuong2' => $vbcxlcvp,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                    'Link2' => base_url().'congtacdang_tp',
                                    'SoLuong2' => $congtacdang,
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($congtacdang) ? 1 : 0
                                ),
                                '3' => array(
                                    'TieuDe2' => 'VB tổ công tác chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_tct',
                                    'SoLuong2' => $tocongtac,
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($tocongtac) ? 1 : 0
                                ),
                                '4' => array(
                                    'TieuDe2' => 'VB giao phòng phối hợp chờ xử lý',
                                    'Link2' => base_url().'phongphoihop',
                                    'SoLuong2' => $pph,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '3' => array(
                            'TieuDe' => ' DS công việc lãnh đạo sở giao phòng xử lý:',
                            'SoLuong' => $tongtpcclanhdaogiao + $tongphoihopthuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'DS công việc lãnh đạo giao chờ xử lý',
                                    'Link2' => base_url().'tpccchiaviec',
                                    'SoLuong2' => $tongtpcclanhdaogiao,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DS CV lãnh đạo giao phối hợp chờ xử lý',
                                    'Link2' => base_url().'ldphchiaviec',
                                    'SoLuong2' => $tongphoihopthuly,
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($congtacdang) ? 1 : 0
                                ),
                                '3' => array(
                                    'TieuDe2' => 'DS CV LĐ Sở giao đang thực hiện',
                                    'Link2' => base_url().'tpccdangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right',
                                    'hienthi' => ($tocongtac) ? 1 : 0
                                ),
                                '4' => array(
                                    'TieuDe2' => 'DS CV phối hợp LĐ Sở giao đang thực hiện',
                                    'Link2' => base_url().'tpccdangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '4' => array(
                            'TieuDe' => 'DSVB đã chỉ đạo',
                            'Link' => base_url().'vanbandachidao',
                            'SoLuong' => $vanbanchidao,
                            'icon' => 'fa fa-newspaper-o',
                        ),
                        '5' => array(
                            'TieuDe' => 'Văn bản hoàn thành chờ Lãnh đạo phê duyệt',
                            'Link' => base_url().'vanbanchopheduyet',
                            'SoLuong' => $vbcduyet,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '6' => array(
                            'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                            'Link' => base_url().'dsdexuatgiahan',
                            'SoLuong' => $dsgiahan,
                            'icon' => 'fa fa-clock-o',
                            'thongbao' => 1
                        ),
                        '7' => array(
                            'TieuDe' => 'Tổng số lượng Tổ công tác giao phòng xử lý',
                            'SoLuong' => $dstocongtac + $dstocongtac_ht,
                            'icon' => 'fa fa-newspaper-o',
                            'hienthi' => ((int)($dstocongtac + $dstocongtac_ht) == 0) ? 0 : 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Tổ công tác chưa hoàn thành',
                                    'Link2' => base_url().'dstocongtac',
                                    'SoLuong2' => $dstocongtac,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Tổ công tác hoàn thành',
                                    'Link2' => base_url().'dstocongtac?cacloaivanban=2',
                                    'SoLuong2' => $dstocongtac_ht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '8' => array(
                            'TieuDe' => 'Tổng số lượng Đơn thư KNTC giao phòng xử lý',
                            'SoLuong' => $dsdonthu + $dsdonthuht,
                            'icon' => 'fa fa-newspaper-o',
                            'hienthi' => ((int)($dsdonthu + $dsdonthuht) == 0) ? 0 : 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                    'Link2' => base_url().'dsdonthu',
                                    'SoLuong2' => $dsdonthu,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                    'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                    'SoLuong2' => $dsdonthuht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '9' => array(
                            'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                            'SoLuong' => $dschutri + $dsphoihop,
                            'icon' => 'fa fa-newspaper-o',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                    'Link2' => base_url().'dsphongchutri',
                                    'SoLuong2' => $dschutri,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                    'Link2' => base_url().'dsphoihop',
                                    'SoLuong2' => $dsphoihop,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '10' => array(
                                'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                                'Link' => base_url().'dexuatphoihop_tpcc',
                                'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                                'icon' => 'fa fa-newspaper-o',
                                'thongbao' => 1
                            ),
                        '11' => array(
                            'TieuDe' => 'Công việc phối hợp chờ xử lý',
                            'Link' => base_url().'dexuatphoihop_tpcc_ph',
                            'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        )
                    ),
                    'II' => array(
                        '1' => array(
                            'TieuDe' => 'DS giấy mời giao phòng xử lý',
                            'SoLuong' => $gmcxl + $gmph,
                            'icon' => 'fa fa-file-text-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'GM giao chủ trì chờ xử lý',
                                    'Link2' => base_url().'giaymoi_tp',
                                    'SoLuong2' => $gmcxl,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'GM giao phòng phối hợp chờ xử lý',
                                    'Link2' => base_url().'giaymoiphoihop',
                                    'SoLuong2' => $gmph,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '2' => array(
                            'TieuDe' => 'Giấy mời đã chỉ đạo',
                            'Link' => base_url().'giaymoidaxuly',
                            'SoLuong' => $gmdxl,
                            'icon' => 'fa fa-file-text-o'
                        ),

                        '3' => array(
                            'TieuDe' => 'Giấy mời hoàn thành chờ phê duyệt',
                            'Link' => base_url().'giaymoichoduyet',
                            'SoLuong' => $vbduyetgiaymoi,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '4' => array(
                            'TieuDe' => 'Giấy mời phối hợp đã xử lý',
                            'Link' => base_url().'giaymoiphoihopdaxuly',
                            'SoLuong' => $phdxl,
                            'icon' => 'fa fa-file-text-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Xem lịch công tác',
                            'Link' => base_url().'lichcongtac',
                            'SoLuong' => '',
                            'icon' => 'fa fa-calendar'
                        ),
                        '6' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã gửi đi',
                            'Link' => base_url().'dssoanthaodagui',
                            'SoLuong' => $dagui,
                            'icon' => 'fa fa-share-square-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Giấy mời đã gửi đi bị từ chối',
                            'Link' => base_url().'dssoanthaobituchoi',
                            'SoLuong' => $daguibituchoi,
                            'icon' => 'fa fa-ban',
                            'hienthi' => ($daguibituchoi) ? 1 : 0
                        ),
                        '8' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp mới',
                            'Link' => base_url().'dsgiaymoiketluanmoi',
                            'SoLuong' => $chuaxem,
                            'icon' => 'fa fa-flag',
                            'thongbao' => 1
                        ),
                        '9' => array(
                            'TieuDe' => 'Báo cáo kết quả cuộc họp đã xem',
                            'Link' => base_url().'dsgiaymoiketluandaxem',
                            'SoLuong' => $daxem,
                            'icon' => 'fa fa-flag-o'
                        )
                    ),
                    'III' => array(
                        '1' => array(
                            'TieuDe' => 'Văn bản chủ trì chờ xử lý',
                            'Link' => base_url().'vanbandichoxuly_tp',
                            'SoLuong' => $vanbandichoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '2' => array(
                            'TieuDe' => 'Văn bản chủ trì đã xử lý',
                            'Link' => base_url().'vanbandidaxuly_tp',
                            'SoLuong' => $vanbandidaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'DS Văn bản của phòng',
                            'Link' => base_url().'vanbanphongban',
                            'SoLuong' => $vanbandi_phong,
                            'icon' => 'fa fa-list-alt'
                        ),
                        '4' => array(
                            'TieuDe' => 'DS Giấy mời của phòng',
                            'Link' => base_url().'giaymoiphongban',
                            'SoLuong' => $giaymoidi_phong,
                            'icon' => 'fa fa-list-alt'
                        ),
                        '5' => array(
                            'TieuDe' => 'Nhập văn bản đi',
                            'Link' => base_url().'vanban',
                            'SoLuong' => '',
                            'icon' => 'fa fa-edit'
                        )
                    ),
                    'IV' => array(
                        '1' => array(
                            'TieuDe' => 'Thông tin mới',
                            'Link' => base_url().'thongtinmoi',
                            'SoLuong' => count($tinmoi),
                            'icon' => 'fa fa-info-circle',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Thông tin đã gửi đi',
                            'Link' => base_url().'thongtinguidi',
                            'SoLuong' => count($tindagui),
                            'icon' => 'fa fa-info-circle'
                        ),
                        '3' => array(
                            'TieuDe' => 'Thông tin đã xem',
                            'Link' => base_url().'thongtindaxem',
                            'SoLuong' => $tindaxem,
                            'icon' => 'fa fa-info-circle'
                        ),
                        '4' => array(
                            'TieuDe' => 'Soạn thảo thông tin',
                            'Link' => base_url().'soanthaothongtin',
                            'SoLuong' => '',
                            'icon' => 'fa fa-edit'
                        )

                    ),
                    'V' => array(
                        '1' => array(
                            'TieuDe' => 'Đầu việc phòng chủ trì chờ xử lý',
                            'Link' => base_url().'dauviecchoxuly_tp_cc',
                            'SoLuong' => $dauviecchutrichoxuly,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Đầu việc phòng phối hợp chờ xử lý',
                            'Link' => base_url().'dauviecchoxulyph_tp_cc',
                            'SoLuong' => $dauviecphoihopchoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Đầu việc trưởng phòng phối hợp (Phòng CT',
                            'Link' => base_url().'dauviecchoxulyphct_tp_cc',
                            'SoLuong' => $dauviecchutriphchoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Đầu việc đã chỉ đạo (Chưa hoàn thành)',
                            'Link' => base_url().'dauviecdaxuly_tp_cc',
                            'SoLuong' => $dauviecchutridaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Đầu việc phòng chủ trì',
                            'Link' => base_url().'dsdauviecphongchutri',
                            'SoLuong' => $dauviecphongchutri,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Đầu việc phòng phối hợp',
                            'Link' => base_url().'dsdauviecphongphoihop',
                            'SoLuong' => $dauviecphongphoihop,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '8' => array(
                            'TieuDe' => 'Đầu việc hoàn thành chờ duyệt',
                            'Link' => base_url().'duyetketqua',
                            'SoLuong' => $duyetketqua,
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VI' => array(
                        '1' => array(
                            'TieuDe' => 'Nhập kế hoạch-đánh giá công việc tuần',
                            'Link' => base_url().'kehoachcongtac',
                            'SoLuong' => $tong_sl_cv,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác tuần của cá nhân',
                            'Link' => base_url().'xemkehoachcongtac/'.$week,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác tháng của cá nhân',
                            'Link' => base_url().'xemkehoachcongtacthang/'.$taikhoan.'/'.$month,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '4' => array(
                            'TieuDe' => 'Tự đánh giá kết quả công tác quý của cá nhân',
                            'Link' => base_url().'xemkehoachcongtacquy/'.$taikhoan.'/'.$quy,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '5' => array(
                            'TieuDe' => 'Xem kết quả công tác tuần của phòng',
                            'Link' => base_url().'dskehoachcongtac/'.$week,
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'Xem kết quả phân loại tháng của phòng',
                            'Link' => base_url().'dskehoachcongtacthang',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '7' => array(
                            'TieuDe' => 'Xem kết quả phân loại quý của phòng',
                            'Link' => base_url().'dskehoachcongtacquy',
                            'SoLuong' => '',
                            'icon' => 'fa fa-newspaper-o'
                        )

                    ),
                    'VII' => array(
                        '1' => array(
                            'TieuDe' => 'Danh sách văn bản tổng thể',
                            'Link' => base_url().'danhsachtongthe_caphai',
                            'SoLuong' => $vanbantongthe_caphai,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => '  Văn bản chờ chỉ đạo',
                            'Link' => base_url().'vanbanchoxuly_ld',
                            'SoLuong' => $lanhdaochoxuly,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '3' => array(
                            'TieuDe' => ' Văn bản đã chỉ đạo (Phòng chưa xử lý) ',
                            'Link' => base_url().'vanbandaxuly_ld',
                            'SoLuong' => $lanhdaodaxuly,
                            'icon' => 'fa fa-newspaper-o'
                        )
                    ),
                    'VIII' => array(
                        '1' => array(
                            'thongbao' => $thongbao_vpdt
                        )
                    )
                );
                $result = $Quyen;
            }


        }else{
            $result = false;
        }
        $this->set_response($result, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }

    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
