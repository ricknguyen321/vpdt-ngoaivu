<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Welcomejs extends REST_Controller {
    protected $_session;
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Vanbanden/Mdmadmin','Mdmadmin');
        $this->Mdmadmin = new Mdmadmin();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('Mdangnhap','Mdangnhap');
        $this->Mdangnhap = new Mdangnhap();
        $this->load->model('thongtinnoibo/Msoanthaothongtin');
        $this->load->model('vanbandi/Mvanbandi');
        $this->load->model('Vanbanden/Mvanbanchoxuly','Mvanbanchoxuly');
        $this->Mvanbanchoxuly = new Mvanbanchoxuly();
        $this->load->model('Vanbanden/Mvanbanden','Mvanbanden');
        $this->Mvanbanden = new Mvanbanden();
        $this->load->model('Vanbanden/Mgiaymoichoxuly','Mgiaymoichoxuly');
        $this->Mgiaymoichoxuly = new Mgiaymoichoxuly();
        $this->load->model('Vanbanden/Mdmadmin','Mdmadmin');
        $this->Mdmadmin = new Mdmadmin();
        $this->load->model('vanbandi/Mtruyennhan');
        $this->load->model('qldv/MqldvDeatails');
        $this->load->model('qldv/Mqldv_phoihop');
        $this->load->model('qldv/Mketqua');
        $this->load->model('qldv/Mchuyennhan');
        $this->load->model('Vanbanden/Mvanbanphoihop');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    

            
    public function users_get()
    {
        // Users from a data store e.g. database

        $vanbans = [
            ['id' => '111not enough authority']
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($vanbans)
            {
                // Set the response and exit
                $this->response($vanbans, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $vanban = NULL;

        if (!empty($vanbans))
        {
            foreach ($vanbans as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $vanban = $value;
                }
            }
        }

        if (!empty($vanban))
        {
            $this->set_response($vanban, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function users_post()
    {
        $token = $this->post('token');
        $data_ss = $this->Mdangnhap->session_ci(sha1($token))['data'];
        $vanban = unserialize_session_data(strstr($data_ss,'vanban'))['vanban'];
        $year = unserialize_session_data(strstr($data_ss,'vanban'))['nam'];
        if($this->post('nam') >0)$year = $this->post('nam');
        if($year > 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default_'.$year, TRUE);
        }//pr($vanban);
        $taikhoan = $vanban['PK_iMaCB'];
        $phongban       = $vanban['FK_iMaPhongHD'];
        $quyen          = $vanban['iQuyenHan_DHNB'];
        $phongbanchicuc = $vanban['iQuyenDB'];
        if($phongban==12)
        {
            if($quyen==6 || $quyen==10)
            {
                $phongchicuc    = NULL;
            }
            else{
                $phongchicuc = $phongbanchicuc;
            }

        }else{
            $phongchicuc    = NULL;
        }
        $chucvu         = $vanban['FK_iMaCV'];
        $tinmoi      = $this->Msoanthaothongtin->layVanBanMoiNhan($vanban['PK_iMaCB'],0);
        $tindaxem   = $this->Msoanthaothongtin->demDuLieu($vanban['PK_iMaCB'],1);
        $tindagui   = $this->Mdanhmuc->layDuLieu('FK_iMaCB_Nhap',$vanban['PK_iMaCB'],'tbl_vanban');
        $week = (int)date("W", strtotime(date('Y-m-d')));
        $month = (int)date('m');
        
        if($month == 1 || $month == 2 || $month == 3)$quy = 1;
        if($month == 4 || $month == 5 || $month == 6)$quy = 2;
        if($month == 7 || $month == 8 || $month == 9)$quy = 3;
        if($month == 10 || $month == 11 || $month == 12)$quy = 4;
        // văn bản đi sửa ngày 03/04/2018
        $vanbandichoxuly = $this->Mvanbandi->demVBDi_ChoXuLy($taikhoan,1);
        $vanbandidaxuly  = $this->Mvanbandi->demVBDi_DaXuLy($taikhoan,2);
        $vanbandi_phong  = $this->Mvanbandi->demDSVB_Phong($phongban,1);
        $giaymoidi_phong = $this->Mvanbandi->demDSVB_Phong($phongban,10);
        $vanbanditrinhky = $this->Mvanbandi->demVBDi_DaTrinhKy($taikhoan);
        // yêu cầu đề xuất phối hợp 20/04/2018
        $dexuatphoihop     = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,1);
        $dexuatphoihopxuly = $this->Mvanbanphoihop->demTTVB_DeXuat($taikhoan,1,2);
        /** @var Đầu việc $Quyen */
        $dauvieclanhdaochoxuly  = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauvieclanhdaodaxuly   = $this->Mchuyennhan->demDauViecDaXuLy($taikhoan,2,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecchutrichoxuly   = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecchutriphchoxuly = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecphoihopchoxuly  = $this->Mchuyennhan->demDauViecChoXuLy($taikhoan,1,2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecchutridaxuly    = $this->Mchuyennhan->demDauViecDaXuLy($taikhoan,2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        $dauviecphongchutri    = $this->Mchuyennhan->demDauViecCuaPhong($phongban,1,$phongchicuc);
        $dauviecphongphoihop    = $this->Mchuyennhan->demDauViecCuaPhong($phongban,2,$phongchicuc);
        $dauviecchoduyet       = $this->Mchuyennhan->demDauViecChoDuyet($taikhoan);
        $dauviectralai         = $this->Mchuyennhan->demDauViecTraLai($taikhoan);
        $week = (int)date("W", strtotime(date('Y-m-d')));
        // tổng văn vản quá hạn
        if($vanban['iQuyenHan_DHNB'] == 4 || $vanban['iQuyenHan_DHNB'] == 5){
            $FK_iMaPhongHD = NULL;
            $tong_cho_ldduyet = $this->Mdanhmuc->get_kehoach7('lanhdao_id ='.$vanban['PK_iMaCB'].'  ','kehoachtrinhld','kh_id');
            $sodienthoaiky = $vanban['sodienthoaiky'];
        }else{
            $FK_iMaPhongHD = $vanban['FK_iMaPhongHD'];
            $tong_cho_ldduyet = $this->Mdanhmuc->get_kehoach7('(phong_id ='.$vanban['FK_iMaPhongHD'].'  or phong_ph like "%'.$vanban['FK_iMaPhongHD'].'%" ) ','kehoachtrinhld','kh_id');

        }
        $tongvbquahan = count($this->Mdanhmuc->getDocCT(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,$FK_iMaPhongHD,NULL,$vanban['PK_iMaCB'],$vanban['iQuyenHan_DHNB']));

// thong bao của vp cho văn phòng điện tử
        $data['thongbao'] = $this->Mdanhmuc->layDuLieu2_2('iTrangThai !=',1,'sNgayBatDau >= ',date('Y-m-d'),'sNgayKetThuc <= ',date('Y-m-d'),'tbl_thongbao');
        $thongbao_vpdt ="";
        if(count($data['thongbao']) <=0)$thongbao_vpdt ="";
        if(count($data['thongbao']) ==1 ){
            $thongbao_vpdt =$data['thongbao'][0]['sNoiDung'];
        }
        if(count($data['thongbao']) > 1 ){
            $thongbao_vpdt =$data['thongbao'][0]['sNoiDung'] ."; ".$data['thongbao'][1]['sNoiDung'];
            
        }
//        pr($vanban);
        if(!empty($vanban))
        {
            /** CHANH VAN PHONG **/
            if($vanban['iQuyenHan_DHNB'] == 3){
                // văn bản chưa phân loại
                $plvb = $this->Mdmadmin->getDocAwait('2');
                $plvbctd = $this->Mdmadmin->getDocAwaitCTD('2');
                // văn bản đã phân loại
                $docawaitdir = $this->Mdmadmin->getDocDirAwait('2');
                // văn bản chuyển lại
                $vbcl = $this->Mdmadmin->getDocReject();
                // phòng chủ trì
                $pct = $this->Mdmadmin->getDocAwait('5',NULL,$vanban['FK_iMaPhongHD']);
                // phong phối hợp
                $pph = $this->Mdmadmin->getDocAwaitPPH(3,$vanban['FK_iMaPhongHD'],NULL,'2','5','0');
                $pph = $pph;
                $phoihopxl = $this->Mdmadmin->getDocAwaitPPHDXL(3,$vanban['FK_iMaPhongHD'],NULL,'1','5',1);
                $phdxl = $phoihopxl;
                // văn bản phòng đã xử lý
//            $data['pdxy'] = count($this->Mvanbanchoxuly->getDocComplete($vanban['FK_iMaPhongHD']));
                $vanbanchidao = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB']);
                // phân loại giấy mời
                $plgm = $this->Mdmadmin->getDocAwait('2',NULL,NULL,NULL,'1');
                // giấy mười đã phân loại
                $gmdpl = $this->Mdmadmin->getDocDirAwait('1');
                // giấy mời chờ xử lý
                $gmcxl = $this->Mdmadmin->getDocAwait('5',NULL,$vanban['FK_iMaPhongHD'],NULL,'1');
                // giấy mời phối hợp chờ xử lý
                $gmphcxl = $this->Mdmadmin->getDocAwaitPPH(3,$vanban['FK_iMaPhongHD'],NULL,'1','5',0);
                $dsdonthu = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,1);
                $dsdonthuht = $this->Mdmadmin->getDonThu(NULL,NULL,NULL,NULL,1);
                $gmphcxl = $gmphcxl;
                // giấy mời đã xử lý
                $gmdxl = $this->Mdmadmin->getDocTPCDAwait($vanban['PK_iMaCB'],NULL,'1');
//            pr($data['gmdxl']);
                $vbqt = $this->Mdmadmin->getVBQT($vanban['PK_iMaCB']);
                $vbqtdgq = $this->Mdmadmin->getVBQTGQ($vanban['PK_iMaCB']);
                $chuaxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],0);
                $daxem = $this->Mdmadmin->getMeeting($vanban['PK_iMaCB'],1);
                $dagui = $this->Mdmadmin->getSoanThaoDaGui($vanban['PK_iMaCB']);
                $vbcduyet = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB']);
                $dschutri = $this->Mdmadmin->getDocCT($vanban['FK_iMaPhongHD']);
                $tuchoigm = $this->Mdmadmin->PPPHTuChoi($vanban['FK_iMaPhongHD'],1);
                $vbduyetgiaymoi = $this->Mdmadmin->getDocPheDuyet(NULL, NULL, NULL, NULL, '2',NULL,$vanban['PK_iMaCB'],1);
                $dsphoihop = $this->Mdmadmin->getDocAwaitDSCTPPH($vanban['FK_iMaPhongHD']);
                $tocongtac = $this->Mdmadmin->getDocAwaitTCT('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL,1);
                $tuchoipp = $this->Mdmadmin->PPPHTuChoi($vanban['FK_iMaPhongHD']);
                $bcchoduyet = $this->Mdmadmin->getDuyetBaoCao(NULL,$vanban['FK_iMaPhongHD']);
                $dsgiahan = $this->Mdmadmin->dsGiaHan($vanban['PK_iMaCB']);
                $congtacdang = $this->Mdmadmin->getCTD('5',NULL,$vanban['FK_iMaPhongHD'],NULL,NULL,NULL,NULL);


                /** @var văn bản đi $Quyen */
                $taikhoan       = $vanban['PK_iMaCB'];
                $chucvu         = $vanban['FK_iMaCV'];
                $quyen          = $vanban['iQuyenHan_DHNB'];
                $phongban       = $vanban['FK_iMaPhongHD'];
                $phongbanchicuc = $vanban['iQuyenDB'];
                $trangthaikq    = 6;
                $phongchicuc    = NULL;
                $canbo          = '';// không ai cả
                $trangthaikq = 1;
                $trangthai_tk       = 5;
                $trangthai =3; #TP
                $trangthaiduyet = 2;
                $ten_taikhoan       = 'truongphong';
                $ykien              = 'ykien_tp';
                $ten_trangthai_xem  = '';
                $trangthai_xem      = '';
                $chude              = '';


                $trangthai2 =999; #TP
                $ten_taikhoan1       = 'chanhvanphong';
                $ykien1              = 'ykien_cvp';
                $ten_trangthai_xem1  = 'trangthai_xem_cvp';
                $trangthai_xem1      = 1;
                $chude1              = '';
                $vbdichoxulycvp    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan1,(int)$taikhoan,$ykien1,$ten_trangthai_xem1,$trangthai_xem1,$chude1);
                $vbdichoxuly    = $this->Mtruyennhan->demdemVanBanChoXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $vbdidaxuly    = $this->Mtruyennhan->demdemVanBanDaXuLy($ten_taikhoan,(int)$taikhoan,$ykien,$ten_trangthai_xem,$trangthai_xem,$chude);
                $duyetketqua            = $this->Mketqua->demTTKT($trangthaiduyet,$phongban,$taikhoan);
                $tong_sl_cv = $this->Mdanhmuc->count_kehoach(" vanban_id IS NULL AND ( active = 1 or active = 2 or  active = 4) and phong_id = ".$vanban['FK_iMaPhongHD']." and tuan =". $week);

                $tonglanhdaogiao = $this->Mdanhmuc->countphongthuly($vanban['FK_iMaPhongHD']);

                $tongphlanhdaogiao = $this->Mdanhmuc->countphongphoihop($vanban['FK_iMaPhongHD']);
                
                $Quyen = array(
                    'I' => array(
                        '1' => array(
                            'TieuDe' => 'Phân loại văn bản',
                            'Link' => base_url().'vanbanchoxuly',
                            'SoLuong' => $plvb,
                            'icon' => 'fa fa-sitemap',
                            'thongbao' => 1
                        ),
                        '2' => array(
                            'TieuDe' => 'Phân loại văn bản công tác Đảng',
                            'Link' => base_url().'congtacdang_cvp',
                            'SoLuong' => $plvbctd,
                            'icon' => 'fa fa-sitemap'
                        ),
                        '3' => array(
                            'TieuDe' => 'Văn bản đã phân loại (lãnh đạo chưa xử lý)',
                            'Link' => base_url().'vanbandaphanloai',
                            'SoLuong' => $docawaitdir,
                            'icon' => 'fa fa-sitemap'
                        ),
                        '4' => array(
                            'TieuDe' => 'Văn bản các phòng chuyển lại',
                            'Link' => base_url().'vanbanchuyenlai',
                            'SoLuong' => $vbcl,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '5' => array(
                            'TieuDe' => 'Danh sách văn bản quá hạn của phòng',
                            'Link' => base_url().'dsvanbanquahan',
                            'SoLuong' => $tongvbquahan,
                            'icon' => 'fa fa-newspaper-o'
                        ),
                        '6' => array(
                            'TieuDe' => 'DS văn bản giao phòng xử lý',
                            'SoLuong' => $pct+$tocongtac+$pph,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'VB giao phòng chủ trì chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_tp',
                                    'SoLuong2' => $pct,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'VB tổ công tác chờ xử lý',
                                    'Link2' => base_url().'vanbanchoxuly_tct',
                                    'SoLuong2' => $tocongtac,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => 'VB công tác Đảng chờ xử lý',
                                    'Link2' => base_url().'congtacdang_tp',
                                    'SoLuong2' => $congtacdang,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '4' => array(
                                    'TieuDe2' => 'VB giao phòng phối hợp chờ xử lý',
                                    'Link2' => base_url().'phongphoihop',
                                    'SoLuong2' => $pph,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '7' => array(
                            'TieuDe' => ' DS công việc lãnh đạo sở giao:',
                            'SoLuong' => $tonglanhdaogiao+$tongphlanhdaogiao,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1,
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => ' Danh sách công việc lãnh đạo sở giao phòng',
                                    'Link2' => base_url().'tpchiaviec',
                                    'SoLuong2' => $tonglanhdaogiao,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => ' DS công việc lãnh đạo sở giao phòng phối hợp',
                                    'Link2' => base_url().'tpphchiaviec',
                                    'SoLuong2' => $tongphlanhdaogiao,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '3' => array(
                                    'TieuDe2' => ' DS công việc LĐ Sở giao phòng đang thực hiện',
                                    'Link2' => base_url().'dsphongdangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '4' => array(
                                    'TieuDe2' => ' DS công việc phối hợp LĐ Sở giao phòng đang thực hiện',
                                    'Link2' => base_url().'tpphdangthuchien',
                                    'SoLuong2' => '',
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '8' => array(
                            'TieuDe' => 'DS văn bản đã chỉ đạo',
                            'Link' => base_url().'vanbandachidao',
                            'SoLuong' => $vanbanchidao,
                            'icon' => 'fa fa-newspaper-o',
                        ),
                        '9' => array(
                            'TieuDe' => 'DSVB đã hoàn thành chờ LĐ phòng phê duyệt',
                            'Link' => base_url().'vanbanchopheduyet',
                            'SoLuong' => $vbcduyet,
                            'icon' => 'fa fa-check-square-o',
                            'thongbao' => 1
                        ),
                        '10' => array(
                            'TieuDe' => 'Duyệt DSVB gia hạn giải quyết',
                            'Link' => base_url().'dsdexuatgiahan',
                            'SoLuong' => $dsgiahan,
                            'icon' => 'fa fa-clock-o',
                            'thongbao' => 1
                        ),
                        '11' => array(
                            'TieuDe' => 'Văn bản phối hợp trả lại',
                            'Link' => base_url().'phoihopchuyenlai_tp',
                            'SoLuong' => $tuchoipp,
                            'icon' => 'fa fa-reply',
                            'thongbao' => 1
                        ),
                        '12' => array(
                            'TieuDe' => 'DSVB quan trọng của Sở',
                            'SoLuong' => $vbqt + $vbqtdgq,
                            'icon' => 'fa fa-list-alt',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở chưa hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong',
                                    'SoLuong2' => $vbqt,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'DSVB quan trọng của Sở đã hoàn thành',
                                    'Link2' => base_url().'vanbanquantrong?cacloaivanban=2',
                                    'SoLuong2' => $vbqtdgq,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '13' => array(
                            'TieuDe' => 'Tổng số văn bản giao phòng xử lý',
                            'SoLuong' => $dschutri + $dsphoihop,
                            'icon' => 'fa fa-list-alt',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng chủ trì',
                                    'Link2' => base_url().'dsphongchutri',
                                    'SoLuong2' => $dschutri,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số lượng văn bản giao phòng phối hợp',
                                    'Link2' => base_url().'dsphoihop',
                                    'SoLuong2' => $dsphoihop,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '14' => array(
                            'TieuDe' => 'Tổng số lượng đơn thư tố cáo giao phòng xử lý',
                            'SoLuong' => $dsdonthu + $dsdonthuht,
                            'icon' => 'fa fa-list-alt',
                            'Cap2' => array(
                                '1' => array(
                                    'TieuDe2' => 'Số Lượng  Đơn thư KNTC chưa hoàn thành',
                                    'Link2' => base_url().'dsdonthu',
                                    'SoLuong2' => $dsdonthu,
                                    'icon' => 'fa fa-caret-right'
                                ),
                                '2' => array(
                                    'TieuDe2' => 'Số Lượng Đơn thư KNTC hoàn thành',
                                    'Link2' => base_url().'dsdonthu?cacloaivanban=2',
                                    'SoLuong2' => $dsdonthuht,
                                    'icon' => 'fa fa-caret-right'
                                )
                            )
                        ),
                        '15' => array(
                            'TieuDe' => 'Đề xuất công việc phối hợp chờ duyệt',
                            'Link' => base_url().'dexuatphoihop_tp',
                            'SoLuong' => ($dexuatphoihop)?$dexuatphoihop:0,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        ),
                        '16' => array(
                            'TieuDe' => 'Công việc phối hợp chờ xử lý',
                            'Link' => base_url().'dexuatphoihop_tp_ph',
                            'SoLuong' => ($dexuatphoihopxuly)?$dexuatphoihopxuly:0,
                            'icon' => 'fa fa-newspaper-o',
                            'thongbao' => 1
                        )
                    ),
                    'II' => array(
                        '1' => array(
                            'TieuDe' => 'Giấy mời chờ phân loại',
                            'Link' => base_url().'giaymoi_cvp',
                            'SoLuong' => $plgm,
                            'icon' => 'fa fa-sitemap',
                            'thongbao' => 1
                        ),
                        '2' => array(
                          