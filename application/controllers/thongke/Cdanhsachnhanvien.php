<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachnhanvien extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}

	public function index()
	{
		$data['title']        = 'Danh sách nhân viên';
		$temp['data']         = $data;
		$temp['template']     = 'thongke/Vdanhsachnhanvien';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cdanhsachnhanvien.php */
/* Location: ./application/controllers/thongke/Cdanhsachnhanvien.php */