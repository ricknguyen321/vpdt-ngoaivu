<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctonghop_moi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongke/Mtonghop_moi');
		$this->load->library('Excel');
		$this->load->model('Vanbanden/Mvanbanphoihop');
	}

	public function index()
	{
		$nam = $_SESSION['nam'];
		if($nam < 2000) $nam = date('Y');
		$data['nam']		= [$nam,(int)$nam-1];
		$data['title']      = 'Báo cáo tổng hợp';
		$data['ngaybd']		= '';
		$data['ngaykt']		= '';
		$data['phongban']	= '';
		$data['vbtt']		= '';
		$data['vbstc']		= '';
		$data['nam_ht']     = $nam;
		$quyen = $this->_session['iQuyenHan_DHNB'];
		$ma    = $this->_session['sTenPB'];
		$ten   = $this->_session['FK_iMaPhongHD'];
		if(_post('khoiphuc'))
		{
			$data['content'] = $this->khoiphuc();
		}
		if($this->input->post('tuchoi')){
            $data['content'] = $this->Reject();
        }
		if($quyen==2 || $quyen==9 || $quyen==3 || $quyen==4||$quyen==5)
		{
			$data['dsphongban']   = $this->Mtonghop_moi->layPhongBanDuThao();
			foreach ($data['dsphongban'] as $key => $val) {
				$data['phongviettat'][$val['PK_iMaPB']] = $val['sVietTat'];
			}
			$data['tenluachon']   = 'Chọn tất cả các phòng ban';
			$data['required']     = '';
		}
		else
		{
			$data['dsphong']   = $this->Mtonghop_moi->layPhongBanDuThao();
			foreach ($data['dsphong'] as $key => $val) {
				$data['phongviettat'][$val['PK_iMaPB']] = $val['sVietTat'];
			}
			$data['dsphongban']   = [['sTenPB'=> $ma,'PK_iMaPB'=> $ten]];
			$data['tenluachon']   = 'Chọn phòng ban';
			$data['required']     = 'required';
		}
		if(_get('loc') || _get('xuat_excel'))
		{
			$hoanthanh			= $this->viecdahoanthanh();
			$chuahoanthanh		= $this->viecchuahoanthanh();
			// thông tin đổ ra view
			$data['ngaybd']		= $hoanthanh['ngaybd'];
			$data['ngaykt']		= $hoanthanh['ngaykt'];
			$data['phongban']	= $hoanthanh['phongban'];
			$data['hoanthanh']	= $hoanthanh['hoanthanh'];
			$data['vbtt']		= $hoanthanh['vbtt'];
			$data['vbstc']		= $hoanthanh['vbstc'];
			$data['nam_ht']		= $hoanthanh['nam'];
			// lấy file văn bản
			if(!empty($data['hoanthanh']))
			{
				foreach ($data['hoanthanh'] as $key => $value) {
					$data['hoanthanh'][$key]['ketqua'] = $this->Mtonghop_moi->layFileKetQua($value['PK_iMaVBDen'],$value['PK_iMaPhongCT']);
					$data['hoanthanh'][$key]['file']   = $this->Mtonghop_moi->layFileCuoi($value['PK_iMaVBDen']);
				}
			}
			// tổng việc đã hoàn thành trong hạn
			$tonghoanthanh  = $hoanthanh['tonghoanthanh'];
			if(!empty($tonghoanthanh))
			{
				foreach ($tonghoanthanh as $key => $value) {
					$data['tonghoanthanh'][$value['PK_iMaPhongCT']] = $value['tong'];
				}
			}
			// tổng việc đã hoàn thành đúng hạn
			$tonghoanthanhdung  = $hoanthanh['tonghoanthanhdung'];
			if(!empty($tonghoanthanhdung))
			{
				foreach ($tonghoanthanhdung as $key => $value) {
					$data['tonghoanthanhdung'][$value['PK_iMaPhongCT']] = $value['tong'];
				}
			}
			
			// tổng việc đã hoàn thành quá hạn
			$tonghoanthanhqua  = $hoanthanh['tonghoanthanhqua'];
			if(!empty($tonghoanthanhqua))
			{
				foreach ($tonghoanthanhqua as $key => $value) {
					$data['tonghoanthanhqua'][$value['PK_iMaPhongCT']] = $value['tong'];
				}
			}
			
			// =============================================================================================================
			// tổng việc đang còn dang dở
			$tongchuahoanthanh  = $chuahoanthanh['tongchuahoanthanh'];
			if(!empty($tongchuahoanthanh))
			{
				foreach ($tongchuahoanthanh as $key => $value) {
					$data['tongchuahoanthanh'][$value['PK_iMaPhongCT']] = $value['tong'];
				}
			}
			// tổng việc chưa hoàn thành đúng hạn
			$tongchuahoanthanhdung  = $chuahoanthanh['tongchuahoanthanhdung'];
			if(!empty($tongchuahoanthanhdung))
			{
				foreach ($tongchuahoanthanhdung as $key => $value) {
					$data['tongchuahoanthanhdung'][$value['PK_iMaPhongCT']] = $value['tong'];
				}
			}
			
			// tổng việc chưa hoàn thành quá hạn
			$tongchuahoanthanhqua  = $chuahoanthanh['tongchuahoanthanhqua'];
			if(!empty($tongchuahoanthanhqua))
			{
				foreach ($tongchuahoanthanhqua as $key => $value) {
					$data['tongchuahoanthanhqua'][$value['PK_iMaPhongCT']] = $value['tong'];
				}
			}
			$data['chuahoanthanh']	= $chuahoanthanh['chuahoanthanh'];
			// lấy file văn bản
			if(!empty($data['chuahoanthanh']))
			{
				foreach ($data['chuahoanthanh'] as $key => $value) {
					$data['chuahoanthanh'][$key]['ketqua'] = $this->Mtonghop_moi->layFileKetQua($value['PK_iMaVBDen'],$value['PK_iMaPhongCT']);
					$data['chuahoanthanh'][$key]['file']   = $this->Mtonghop_moi->layFileCuoi($value['PK_iMaVBDen']);
				}
			}
			if(_get('xuat_excel'))
			{
				$this->export_excel($data['dsphongban'],$data['phongviettat'],$data['ngaykt'],$data['hoanthanh'],$data['tonghoanthanh'],$data['tonghoanthanhdung'],$data['tonghoanthanhqua'],$data['chuahoanthanh'],$data['tongchuahoanthanh'],$data['tongchuahoanthanhdung'],$data['tongchuahoanthanhqua']);
			}
		}
		$temp['data']         = $data;
		$temp['template']     = 'thongke/Vtonghop_moi';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function viecdahoanthanh()
	{
		$data['ngaybd']				= _get('ngaybd');
		$data['ngaykt']				= _get('ngaykt');
		$data['phongban']			= _get('phongban');
		$data['vbtt']				= _get('vbtt');
		$data['vbstc']				= _get('vbstc');
		$data['nam']				= _get('nam');
		$data['hoanthanh']			= $this->Mtonghop_moi->thongke_hoanthanh(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		$data['tonghoanthanh']		= $this->Mtonghop_moi->dem_thongke_hoanthanh(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		$data['tonghoanthanhdung']	= $this->Mtonghop_moi->dem_thongke_hoanthanh_dung(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		$data['tonghoanthanhqua']	= $this->Mtonghop_moi->dem_thongke_hoanthanh_sai(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		return $data;
	}
	public function viecchuahoanthanh()
	{
		$data['ngaybd']					= _get('ngaybd');
		$data['ngaykt']					= _get('ngaykt');
		$data['phongban']				= _get('phongban');
		$data['vbtt']					= _get('vbtt');
		$data['vbstc']					= _get('vbstc');
		$data['nam']				    = _get('nam');
		$data['chuahoanthanh']			= $this->Mtonghop_moi->thongke_chuahoanthanh(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		$data['tongchuahoanthanh']		= $this->Mtonghop_moi->dem_thongke_chuahoanthanh(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		$data['tongchuahoanthanhdung']	= $this->Mtonghop_moi->dem_thongke_chuahoanthanh_dung(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		$data['tongchuahoanthanhqua']	= $this->Mtonghop_moi->dem_thongke_chuahoanthanh_sai(date_insert($data['ngaybd']),date_insert($data['ngaykt']),$data['phongban'],$data['vbtt'],$data['vbstc'],$data['nam']);
		return $data;
	}
	public function khoiphuc(){
		$mavanban			= _post('khoiphuc');
		$laythongtinvanban	= $this->Mtonghop_moi->laythongtinvanban($mavanban);
		$tuan				= $this->Mtonghop_moi->laytuanmoinhat($mavanban);
		$xoabangkehoach		= $this->Mtonghop_moi->xoabangkehoach($mavanban,$tuan['tuan']);
		$mangcapnhat = array(
			'sNgayGiaiQuyet' => $laythongtinvanban['sNgayHoanThanhCu']
		);
		$this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$mavanban,'tbl_vanbanden',$mangcapnhat);
	}
	public function Reject(){
		$taikhoan		= $this->_session['PK_iMaCB'];
		$mavanban		= $this->input->post('mavanban');
		$lydotralai     = _post('noidungtuchoi');
		$thongtinchuyen = $this->Mvanbanphoihop->layCV_CT($mavanban);
		$tuanhientai	= date('W');
		$tuan			= $this->Mtonghop_moi->laytuanmoinhat($mavanban);
		$kehoach		= $this->Mtonghop_moi->laythongtinkehoach($mavanban,$tuan['tuan']);
		if(!empty($kehoach))
		{
			foreach ($kehoach as $key => $value) {
				$mangchuyen[] =array(
					'kh_id_sub'		=> $value['kh_id'],
					'vanban_id'		=> $value['vanban_id'],
					'kh_noidung'	=> $value['kh_noidung'],
					'date_nhap'		=> $value['date_nhap'],
					'vanban_skh'	=> $value['vanban_skh'],
					'tuan'			=> $tuanhientai,
					'ngay_nhan'		=> $value['ngay_nhan'],
					'ngay_han'		=> $value['ngay_han'],
					'canbo_id'		=> $value['canbo_id'],
					'thuc_hien'		=> $value['thuc_hien'],
					'loai_kh'		=> $value['loai_kh'],
					'chucvu'		=> $value['chucvu'],
					'lanhdao_id'	=> $value['lanhdao_id'],
					'user_input'	=> $value['user_input'],
					'lanhdao_so'	=> $value['lanhdao_so'],
					'phong_id'		=> $value['phong_id']
				);
			}
			$this->Mdanhmuc->themNhieuDuLieu('kehoach',$mangchuyen);
			$mangthemdulieu = array(
	            'FK_iMaVBDen'     => $mavanban,
	            'FK_iMaCB_Chuyen' => $taikhoan,
	            'FK_iMaCB_Nhan'   => $thongtinchuyen['PK_iMaCVCT'],
	            'sNoiDung'        => $lydotralai,
	            'sThoiGian'       => date('Y-m-d H:i:s')
	        );
	        $this->Mdanhmuc->themDuLieu('tbl_luuvet_chuyennhan',$mangthemdulieu);
	        $mangcapnhat=array(
				'iTrangThai'		=> 0,
				'PK_iMaCBDuyet'		=> $thongtinchuyen['PK_iMaCBChuyen'],
				'iTraLai_BPTH'		=> 2,
				'sNgayGiaiQuyet'	=> NULL,
				'sNgayHoanThanhCu'	=> $kehoach[0]['ngay_hoanthanh']
	        );
	        $this->Mdanhmuc->capnhatDuLieu('PK_iMaVBDen',$mavanban,'tbl_vanbanden',$mangcapnhat);
		}
    }
	public function export_excel($dsphong,$phongviettat,$ngaykt,$hoanthanh,$tonghoanthanh,$tonghoanthanhdung,$tonghoanthanhqua,$chuahoanthanh,$tongchuahoanthanh,$tongchuahoanthanhdung,$tongchuahoanthanhqua)
	{
		$objPHPExcel  =new PHPExcel();
		$filename     ='DSVB'.' - '.date('d-m-Y',time());
        $objPHPExcel->getProperties()->setCreator("Administrator")
                                     ->setLastModifiedBy("Administrator")
                                     ->setTitle("Danh sách các văn bản")
                                     ->setSubject("Danh sách các văn bản")
                                     ->setDescription("Danh sách các văn bản")
                                     ->setKeywords("office 2010 openxml php")
                                     ->setCategory("Sở Tài Chính");
        //Căn dòng chữ
        $dinhdang = array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
         $dinhdang2 = array(
            // 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
        // chỉnh kích cỡ cột
        $array_column = array(
                'A' => 5,
                'B' => 9,
                'C' => 10,
                'D' => 13,
                'E' => 40,
                'F' => 13,
                'G' => 26,
                'H' => 14,
                'I' => 11,
                'J' => 20,
                'K' => 11,
                'L' => 11,
                'M' => 11,
                'N' => 11,
                'O' => 11,
                'P' => 11,
                'Q' => 11,
                'R' => 11
            );
            foreach($array_column as $key => $value){
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setAutoSize(false);
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setWidth($value);
            }

        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(13);
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();

        //Phần tiêu đề (nếu có)
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:R1')->setCellValue('A1','Báo cáo tổng hợp ngày '.date('d/m/Y'));
        //end sét chiều vào
        $array_content2 = array(
                'A1');
        // định dạng và in đậm
        foreach($array_content2 as $key => $val){
            $objPHPExcel->getActiveSheet()->getStyle($val)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setSize(16);
        }
        //sét chiều cao
        $array_row = array(
				'1' => 35,
				'2' => 25,
				'3' => 58,
				'4' => 25
            );
        // nhiệm vụ hoàn thành trong kỳ
        $objPHPExcel->getActiveSheet()->mergeCells('A4:R4')->setCellValue('A4','NHIỆM VỤ HOÀN THÀNH TRONG KỲ')->getStyle('A4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray($dinhdang2);
        foreach($array_row as $key => $value){
            $objPHPExcel->getActiveSheet()->getRowDimension($key)->setRowHeight($value);
        }
        //end sét chiều vào
        $array_content0 = array(
            'F2' => 'Lãnh đạo Sở giao',
            'K2' => 'Tiến độ'
            );
        // định dạng và in đậm
        $objPHPExcel->getActiveSheet()->mergeCells('F2:H2')->setCellValue('F2','Lãnh đạo Sở giao');
        $objPHPExcel->getActiveSheet()->mergeCells('K2:O2')->setCellValue('K2','Tiến độ');
        foreach($array_content0 as $key => $value){
            $objPHPExcel->getActiveSheet()->getStyle($key)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getFont()->setBold(true);
        }
        $array_content = array(
            'A' => 'STT',
            'B' => 'Số đến',
            'C' => 'Số SKH',
            'D' => 'Ngày ban hành',
            'E' => 'Nội dung văn bản',
            'I' => 'Thời hạn hoàn thành',
            'J' => 'Tình hình triển khai và kết quả đạt được',
            'P' => 'Khó khăn, vướng mắc',
            'Q' => 'Đề xuất hướng xử lý',
            'R' => 'Ghi Chú'
            );
        // định dạng và in đậm
        foreach($array_content as $key => $value){
            $objPHPExcel->getActiveSheet()->mergeCells($key.'2:'.$key.'3')->setCellValue($key.'2',$value);
            $objPHPExcel->getActiveSheet()->getStyle($key.'2')->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key.'2')->getFont()->setBold(true);
        }
        $array_content1 = array(
			'F3' => 'Ngày giao',
			'G3' => 'Phòng, ban, đơn vị chủ trì',
			'H3' => 'Phòng, ban, đơn vị phối hợp',
			'K3'  => 'Tổng số',
			'L3'  => 'Đang triển khai (Chưa đến hạn)',
			'M3'  => 'Đang triển khai (Đã quá hạn)',
			'N3'  => 'Hoàn thành (Trong hạn)',
			'O3'  => 'Hoàn thành (Quá hạn)'
            );
        // định dạng và in đậm
        foreach($array_content1 as $key => $value){
            $objPHPExcel->getActiveSheet()->setCellValue($key,$value);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getFont()->setBold(true);
        }
        $startRow = 5;
        $stt=1;
        if(!empty($dsphong)){
        	foreach ($dsphong as $key => $pb) {
        		$objPHPExcel->getActiveSheet()->mergeCells('A'.$startRow.':J'.$startRow)->setCellValue('A'.$startRow,$pb['sTenPB'])->getStyle('A'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
        		$objPHPExcel->getActiveSheet()->setCellValue('K'.$startRow,(isset($tonghoanthanh[$pb['PK_iMaPB']])?$tonghoanthanh[$pb['PK_iMaPB']]:0))->getStyle('K'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('L'.$startRow,0)->getStyle('L'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('L'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('M'.$startRow,0)->getStyle('M'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('M'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('N'.$startRow,(isset($tonghoanthanhdung[$pb['PK_iMaPB']])?$tonghoanthanhdung[$pb['PK_iMaPB']]:0))->getStyle('N'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('N'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('O'.$startRow,(isset($tonghoanthanhqua[$pb['PK_iMaPB']])?$tonghoanthanhqua[$pb['PK_iMaPB']]:0))->getStyle('O'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('O'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(25);
        		$stt2=1;
        		$startRow++;
        		if(!empty($hoanthanh)){
        			foreach ($hoanthanh as $key => $value) {
        				if($value['PK_iMaPhongCT']==$pb['PK_iMaPB']){
        					$phongph='';
        					if(!empty($value['PK_iMaPhongPH'])){
        						$dem=0;
        						$mangten = explode(',',$value['PK_iMaPhongPH']);
        						foreach ($mangten as $ph) {
        							$dem++;
        							($dem>1)? $a=', ':$a='';
        							$phongph= $phongph.$a.$phongviettat[$ph];
        						}
        					}
        					if($value['hanvb'] == 1){
        						$hanvb ='Hạn VB: ';
        					}else{
        						$hanvb ='';
        					}
        					$han = ($value['sHanGiaiQuyet'] >'1970-01-01' && !empty($value['sHanGiaiQuyet']))?
			        			$hanvb.date_select($value['sHanGiaiQuyet']):'';
			        		$hantk = ($value['sHanGiaiQuyet'] < $value['sHanThongKe'] && $value['sHanThongKe'] >'1970-01-01')
			        			?'(Gia hạn: '.date_select($value['sHanThongKe']).')':'';
	        				$objPHPExcel->getActiveSheet()->setCellValue('A'.$startRow,$stt2)->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('B'.$startRow,$value['iSoDen'])->getStyle('B'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('C'.$startRow,$value['sKyHieu'])->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('D'.$startRow,date_select($value['sNgayKy']))->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('E'.$startRow,$value['sMoTa'])->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('F'.$startRow,date_select($value['sThoiGian']))->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('G'.$startRow,$value['sVietTat'])->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('H'.$startRow,$phongph)->getStyle('H'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('I'.$startRow,$han.' '.$hantk)->getStyle('I'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('J'.$startRow,$value['ketqua']['sMoTa'])->getStyle('J'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('K'.$startRow,'')->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('L'.$startRow,'')->getStyle('L'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('M'.$startRow,'')->getStyle('M'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('N'.$startRow,($value['sNgayGiaiQuyet'] <= $value['sHanThongKe'] || $value['sHanThongKe'] <= "1970-01-01")?'x':'')->getStyle('N'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('O'.$startRow,($value['sNgayGiaiQuyet'] > $value['sHanThongKe'] && $value['sHanThongKe'] > '1970-01-01')?'x':'')->getStyle('O'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('P'.$startRow,'')->getStyle('P'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$startRow,'')->getStyle('Q'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('R'.$startRow,'')->getStyle('R'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$startRow++;
			        		$stt2++;
			        	}
        			}
        		}
        		$stt++;
        	}
        }
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow()+1;
        // nhiệm vụ chưa hoàn thành
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$maxR.':R'.$maxR)->setCellValue('A'.$maxR,'NHIỆM VỤ ĐANG DỞ DANG')->getStyle('A'.$maxR)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$maxR)->getAlignment()->applyFromArray($dinhdang2);
        $objPHPExcel->getActiveSheet()->getRowDimension($maxR)->setRowHeight(25);
        $startRow = $maxR+1;
        $stt=1;
        if(!empty($dsphong)){
        	foreach ($dsphong as $key => $pb) {
        		$objPHPExcel->getActiveSheet()->mergeCells('A'.$startRow.':J'.$startRow)->setCellValue('A'.$startRow,$pb['sTenPB'])->getStyle('A'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
        		$objPHPExcel->getActiveSheet()->setCellValue('K'.$startRow,(isset($tongchuahoanthanh[$pb['PK_iMaPB']])?$tongchuahoanthanh[$pb['PK_iMaPB']]:0))->getStyle('K'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('L'.$startRow,(isset($tongchuahoanthanhdung[$pb['PK_iMaPB']])?$tongchuahoanthanhdung[$pb['PK_iMaPB']]:0))->getStyle('L'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('L'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('M'.$startRow,(isset($tongchuahoanthanhqua[$pb['PK_iMaPB']])?$tongchuahoanthanhqua[$pb['PK_iMaPB']]:0))->getStyle('M'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('M'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('N'.$startRow,0)->getStyle('N'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('N'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('O'.$startRow,0)->getStyle('O'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('O'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(25);
        		$stt2=1;
        		$startRow++;
        		if(!empty($chuahoanthanh)){
        			foreach ($chuahoanthanh as $key => $value) {
        				if($value['PK_iMaPhongCT']==$pb['PK_iMaPB']){
        					$phongph='';
        					if(!empty($value['PK_iMaPhongPH'])){
        						$dem=0;
        						$mangten = explode(',',$value['PK_iMaPhongPH']);
        						foreach ($mangten as $ph) {
        							$dem++;
        							($dem>1)? $a=', ':$a='';
        							$phongph= $phongph.$a.$phongviettat[$ph];
        						}
        					}
	        				$objPHPExcel->getActiveSheet()->setCellValue('A'.$startRow,$stt2)->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('B'.$startRow,$value['iSoDen'])->getStyle('B'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('C'.$startRow,$value['sKyHieu'])->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('D'.$startRow,date_select($value['sNgayKy']))->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('E'.$startRow,$value['sMoTa'])->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('F'.$startRow,date_select($value['sThoiGian']))->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('G'.$startRow,$value['sVietTat'])->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('H'.$startRow,$phongph)->getStyle('H'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('I'.$startRow,($value['sHanGiaiQuyet'] >'1970-01-01' && !empty($value['sHanGiaiQuyet']))?date_select($value['sHanGiaiQuyet']):'')->getStyle('I'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('J'.$startRow,$value['ketqua']['sMoTa'])->getStyle('J'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('K'.$startRow,'')->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('L'.$startRow,'')->getStyle('L'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('M'.$startRow,'')->getStyle('M'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('N'.$startRow,($value['sNgayGiaiQuyet'] <= $value['sHanThongKe'] || $value['sHanThongKe'] <= "1970-01-01")?'x':'')->getStyle('N'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('O'.$startRow,($value['sNgayGiaiQuyet'] > $value['sHanThongKe'] && $value['sHanThongKe'] > '1970-01-01')?'x':'')->getStyle('O'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('P'.$startRow,'')->getStyle('P'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$startRow,'')->getStyle('Q'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('R'.$startRow,'')->getStyle('R'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$startRow++;
			        		$stt2++;
			        	}
        			}
        		}
        		$stt++;
        	}
        }
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();
     //    $objPHPExcel->getActiveSheet()
	    // ->getStyle('A2:R3')
	    // ->getFill()
	    // ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
	    // ->getStartColor()
	    // ->setRGB('191239255');
        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A2".":"."R".$maxR)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=".$filename.".xls");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
	}

}

/* End of file Ctonghop_moi.php */
/* Location: ./application/controllers/thongke/Ctonghop_moi.php */