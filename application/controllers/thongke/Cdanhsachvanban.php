<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachvanban extends MY_Controller {

	protected $_thongtin;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('thongke/Mthongkevanbantheolanhdao');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['title']		= 'Danh sách văn bản';
		$vanthu				= $this->Mthongkevanbantheolanhdao->layLD_So(array(9));
		$data['hotenvanthu']= array_column($vanthu,'sHoTen','PK_iMaCB');
		$dulieu				= $this->layDanhSach();
		$data['danhsach']	= $dulieu['danhsach'];
		$data['phantrang']	= $dulieu['phantrang'];
		$temp['data']		= $data;
		$temp['template']	= 'thongke/Vdanhsachvanban';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function layDanhSach()
	{
		$phongban		= _get('phongban');
		$phogiamdoc		= _get('phogiamdoc');
		$giamdoc		= _get('giamdoc');
		$chucnang		= _get('chucnang');
		$chucnangdem	= _get('chucnangdem');
		$config['base_url']             = base_url().'danhsachvanban?phongban='.$phongban.'&phogiamdoc='.$phogiamdoc.'&giamdoc='.$giamdoc.'&chucnang='.$chucnang.'&chucnangdem='.$chucnangdem;
		$config['total_rows']           = $this->Mthongkevanbantheolanhdao->$chucnangdem($phongban,$phogiamdoc,$giamdoc);
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'insoluutru_di');
      	}
		$data['danhsach']        = $this->Mthongkevanbantheolanhdao->$chucnang($phongban,$phogiamdoc,$giamdoc,$config['per_page'], $data['page']);
		$data['phantrang']       = $this->pagination->create_links();
		return $data;
	}

}

/* End of file Cdanhsachvanban.php */
/* Location: ./application/controllers/thongke/Cdanhsachvanban.php */