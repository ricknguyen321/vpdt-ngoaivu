<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cinsoluutru_di extends MY_Controller {
	protected $_thongtin;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('thongke/Mthongke');
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['title']      = 'In sổ lưu trữ';
		$data['dsphongban'] = $this->Mvanbandi->layPhongBanDuThao();
		$this->_thongtin = $this->layDanhSach();
		$data['thongtin'] = $this->_thongtin;
		$data['danhsach'] = $this->_thongtin['danhsach'];
		// if(!empty($data['danhsach']))
		// {
		// 	foreach ($data['danhsach'] as $key => $value) {
		// 		$hoten = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$value['FK_iMaCB_Nhap'],'tbl_canbo');
		// 		$data['danhsach'][$key]['nguoinhap'] = $hoten[0]['sHoTen'];
		// 	}
		// }
	if(isset($_GET['ketxuatword']) or isset($_GET['ketxuatexcel'])){
		if(isset($_GET['ketxuatword'])){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=SoLuuTruVBDi_".$_SESSION['nam'].".doc");

            $soditu     = _get('soditu');
			$sodiden    = _get('sodiden');
			$ngaytu     = _get('ngaytu');
			$ngayden    = _get('ngayden');
			$loaiso     = _get('loaiso');
			$giaymoitu  = _get('giaymoitu');
			$giaymoiden = _get('giaymoiden');
			$phongban   = _get('phongban');
			$quyen = $this->_session['iQuyenHan_DHNB'];
			if($quyen==6 || $quyen==7 || $quyen==3 || $quyen==8)
			{
				$phongban = $this->_session['FK_iMaPhongHD'];
			}
            $data['danhsach'] = $this->Mthongke->insoluutru_di($soditu,$sodiden,$ngaytu,$ngayden,$loaiso,$giaymoitu,$giaymoiden,$phongban,NULL, NULL);
            $temp['data']     = $data;//pr($data);
            $temp['template'] = 'thongke/Vinsoluutru_di_exp';
        	$this->load->view('layout_admin/layout_word',$temp);
        }else{
            header("Content-Type: application/vnd.ms-excel");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=SoLuuTruVBDi_".$_SESSION['nam'].".xls"); 

            $soditu     = _get('soditu');
			$sodiden    = _get('sodiden');
			$ngaytu     = _get('ngaytu');
			$ngayden    = _get('ngayden');
			$loaiso     = _get('loaiso');
			$giaymoitu  = _get('giaymoitu');
			$giaymoiden = _get('giaymoiden');
			$phongban   = _get('phongban');
			$quyen = $this->_session['iQuyenHan_DHNB'];
			if($quyen==6 || $quyen==7 || $quyen==3 || $quyen==8)
			{
				$phongban = $this->_session['FK_iMaPhongHD'];
			}
            $data['danhsach'] = $this->Mthongke->insoluutru_di($soditu,$sodiden,$ngaytu,$ngayden,$loaiso,$giaymoitu,$giaymoiden,$phongban,NULL, NULL);
            $temp['data']     = $data;//pr($data);
            $temp['template'] = 'thongke/Vinsoluutru_di_exp';
        	$this->load->view('layout_admin/layout_word',$temp);
        }
    }else{
    	$temp['data']     = $data;//pr($data);
    	$temp['template'] = 'thongke/Vinsoluutru_di';
		$this->load->view('layout_admin/layout',$temp);
    }
        
	}
	public function layDanhSach()
	{
		$soditu     = _get('soditu');
		$sodiden    = _get('sodiden');
		$ngaytu     = _get('ngaytu');
		$ngayden    = _get('ngayden');
		$loaiso     = _get('loaiso');
		$giaymoitu  = _get('giaymoitu');
		$giaymoiden = _get('giaymoiden');
		$phongban   = _get('phongban');
		$quyen = $this->_session['iQuyenHan_DHNB'];
		if($quyen==6 || $quyen==7 || $quyen==3 || $quyen==8)
		{
			$phongban = $this->_session['FK_iMaPhongHD'];
		}
		$config['base_url']             = base_url().'insoluutru_di?soditu='.$soditu.'&sodiden='.$sodiden.'&ngaytu='.$ngaytu.'&ngayden='.$ngayden.'&loaiso='.$loaiso.'&giaymoitu='.$giaymoitu.'&giaymoiden='.$giaymoiden.'&phongban='.$phongban;
		$config['total_rows']           = $this->Mthongke->dem_insoluutru_di($soditu,$sodiden,$ngaytu,$ngayden,$loaiso,$giaymoitu,$giaymoiden,$phongban);
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'insoluutru_di');
      	}
		$danhsach        = $this->Mthongke->insoluutru_di($soditu,$sodiden,$ngaytu,$ngayden,$loaiso,$giaymoitu,$giaymoiden,$phongban,$config['per_page'], $data['page']);
		$phantrang       = $this->pagination->create_links();
		$data=array(
			'soditu'     => $soditu,
			'sodiden'    => $sodiden,
			'ngaytu'     => $ngaytu,
			'ngayden'    => $ngayden,
			'loaiso'     => $loaiso,
			'giaymoitu'  => $giaymoitu,
			'giaymoiden' => $giaymoiden,
			'phongban'   => $phongban,
			'danhsach'   => $danhsach,
			'phantrang'  => $phantrang
			);
		return $data;
	}

}

/* End of file Cinsoluutru_di.php */
/* Location: ./application/controllers/thongke/Cinsoluutru_di.php */