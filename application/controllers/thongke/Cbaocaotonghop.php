<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cbaocaotonghop extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('Vanbanden/Mvanbanden');
		$this->load->model('thongke/Mthongke');
		$this->load->library('Excel');
	}

	public function index()
	{
		$layVBDicoTL = $this->Mthongke->layVBDicoTL();
		foreach ($layVBDicoTL as $key => $value) {
			$mangVBDI[trim($value['iSoDen'])][] = $value['iSoVBDi'];
			$mangVBDI[trim($value['iSoDen'])][] = $value['sDuongDan'];
		}
		$data['mangVBDI']     = $mangVBDI;
		$data['title']        = 'Báo cáo tổng hợp';
		$quyen = $this->_session['iQuyenHan_DHNB'];
		$ma    = $this->_session['sTenPB'];
		$ten   = $this->_session['FK_iMaPhongHD'];
		if($quyen==2 || $quyen==9 || $quyen==3 || $quyen==4||$quyen==5)
		{
			$data['dsphongban']   = $this->Mthongke->layPhongBanDuThao();
			$data['tenluachon']   = 'Chọn tất cả các phòng ban';
		}
		else
		{
			$data['dsphongban']   = [['sTenPB'=> $ma,'PK_iMaPB'=> $ten]];
			$data['tenluachon']   = 'Chọn phòng ban';
		}
		$danhsach = '';
		$thongtin = '';
		if(_post('loc'))
		{
			$thongtin = $this->baocaotonghop();
			$danhsach = $thongtin['danhsach'];
		}
		foreach ($data['dsphongban'] as $k => $vl) {$tong=0;$cgq_cdh=0;$cgq_dqh=0;$dgq_dth=0;$dgq_qth=0;
			if(!empty($danhsach))
			{
				foreach ($danhsach as $key => $val) {
					if($val['PK_iMaPhongCT']==$vl['PK_iMaPB'])
					{
						$tong++;
						$data['dsphongban'][$k]['tong'] = $tong;
						if($val['iTrangThai']==0 && $val['sHanGiaiQuyet'] >= date('Y-m-d'))// đang giải quyết trong hạn
						{
							$cgq_cdh++;
							$data['dsphongban'][$k]['danggiaiquyet_tronghan'] = $cgq_cdh;
						}
						if($val['iTrangThai']==0 && $val['sHanGiaiQuyet'] < date('Y-m-d'))// đang giải quyết quá hạn
						{
							$cgq_dqh++;
							$data['dsphongban'][$k]['danggiaiquyet_quahan'] = $cgq_dqh;
						}
						if($val['iTrangThai']==1 && ($val['sHanGiaiQuyet'] >= $val['sNgayGiaiQuyet'] || $val['sHanGiaiQuyet'] <= '1970-01-01'))// đã giải quyết trong hạn
						{
							$dgq_dth++;
							$data['dsphongban'][$k]['dagiaiquyet_tronghan'] = $dgq_dth;
						}
						if($val['iTrangThai']==1 && $val['sHanGiaiQuyet'] > '1970-01-01' && $val['sHanGiaiQuyet'] < $val['sNgayGiaiQuyet'])// đã giải quyết quá hạn
						{
							$dgq_qth++;
							$data['dsphongban'][$k]['dagiaiquyet_quahan'] = $dgq_qth;
						}
					}
					else{
						$data['dsphongban'][$k]['tong'] = $tong;
					}
				}
			}else{
				$data['dsphongban'][$k]['tong'] = $tong;
			}
		}
		if(_post('xuat_excel'))
		{
			$thongtin = $this->baocaotonghop();
			$this->export($data['dsphongban'],$thongtin['danhsach']);
		}
		if(!empty($danhsach))
		{
			foreach ($danhsach as $key => $value) {
				$duongdan = $this->Mvanbanden->layFileLast($value['PK_iMaVBDen']);
                if(!empty($duongdan))
                {
                    $danhsach[$key]['sDuongDan'] = $duongdan['sDuongDan'];
                }else{
                    $danhsach[$key]['sDuongDan'] = '';
                }
				if(!empty($value['PK_iMaPhongPH']))
				{
					$phongphoihop = $this->Mthongke->layPhongPhoiHop(explode(',',$value['PK_iMaPhongPH']));
					$danhsach[$key]['phongphoihop'] = $phongphoihop;
				}else{
					$danhsach[$key]['phongphoihop'] = '';
				}
				
			}
		}
		$data['thongtin']    = $thongtin;
		$data['dsdulieu']	  = $danhsach;
		$temp['data']         = $data;
		$temp['template']     = 'thongke/Vbaocaotonghop';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function baocaotonghop()
	{
		$all          = _post('all');
		$vbtt         = _post('vbtt');
		$vbstc        = _post('vbstc');
		$vbtt_tbkt    = _post('vbtt_tbkt');
		$vbtt_khac    = _post('vbtt_khac');
		$vbstc_tbkt   = _post('vbstc_tbkt');
		$vbstc_khac   = _post('vbstc_khac');
		$khongthoihan = _post('khongthoihan');
		$cothoihan    = _post('cothoihan');
		$ngay_ngaybd  = _post('ngay_ngaybd');
		$ngay_ngaykt  = _post('ngay_ngaykt');
		$soden_tu 	  = _post('soden_tu');
		$soden_den    = _post('soden_den');
		$cgq          = _post('cgq');
		$cgq_cdh      = _post('cgq_cdh');
		$cgq_sdh      = _post('cgq_sdh');
		$cgq_dqh      = _post('cgq_dqh');
		$dgq          = _post('dgq');
		$dgq_dth      = _post('dgq_dth');
		$dgq_qth      = _post('dgq_qth');
		$hiensoden      = _post('hiensoden');
		$quyen = $this->_session['iQuyenHan_DHNB'];
		if($quyen==2 || $quyen==9 || $quyen==3 || $quyen==4||$quyen==5)
		{
			$phongban     = _post('phongban');
		}
		else{
			$phongban = $this->_session['FK_iMaPhongHD'];
		}
		$danhsach = $this->Mthongke->baocaotonghop($all,$vbtt,$vbstc,$vbtt_tbkt,$vbtt_khac,$vbstc_tbkt,$vbstc_khac,$khongthoihan,$cothoihan,$ngay_ngaybd,$ngay_ngaykt,$soden_tu,$soden_den,$cgq,$cgq_cdh,$cgq_sdh,$cgq_dqh,$dgq,$dgq_dth,$dgq_qth,$phongban,$quyen);
		// pr($danhsach);
		$thongtin = array(
			'all'          =>$all,
			'vbtt'         =>$vbtt,
			'vbstc'        =>$vbstc,
			'vbtt_tbkt'    =>$vbtt_tbkt,
			'vbtt_khac'    =>$vbtt_khac,
			'vbstc_tbkt'   =>$vbstc_tbkt,
			'vbstc_khac'   =>$vbstc_khac,
			'khongthoihan' =>$khongthoihan,
			'cothoihan'    =>$cothoihan,
			'ngay_ngaybd'  =>$ngay_ngaybd,
			'ngay_ngaykt'  =>$ngay_ngaykt,
			'soden_tu' 	   =>$soden_tu,
			'soden_den'    =>$soden_den,
			'cgq'          =>$cgq,
			'cgq_cdh'      =>$cgq_cdh,
			'cgq_sdh'      =>$cgq_sdh,
			'cgq_dqh'      =>$cgq_dqh,
			'dgq'          =>$dgq,
			'dgq_dth'      =>$dgq_dth,
			'dgq_qth'      =>$dgq_qth,
			'quyen' 	   =>$quyen,
			'phongban'     =>$phongban,
			'hiensoden'	   =>$hiensoden,
			'danhsach'     =>$danhsach
			);
		return $thongtin;
	}
	public function export($dsphongban,$thongtin)
	{
		foreach ($dsphongban as $k => $vl) {$tong=0;$cgq_cdh=0;$cgq_dqh=0;$dgq_dth=0;$dgq_qth=0;
			if(!empty($thongtin))
			{
				foreach ($thongtin as $key => $val) {
					if($val['PK_iMaPhongCT']==$vl['PK_iMaPB'])
					{
						$tong++;
						$dsphongban[$k]['tong'] = $tong;
						if($val['iTrangThai']==0 && $val['sHanGiaiQuyet'] >= date('Y-m-d'))// đang giải quyết trong hạn
						{
							$cgq_cdh++;
							$dsphongban[$k]['danggiaiquyet_tronghan'] = $cgq_cdh;
						}
						if($val['iTrangThai']==0 && $val['sHanGiaiQuyet'] < date('Y-m-d'))// đang giải quyết quá hạn
						{
							$cgq_dqh++;
							$dsphongban[$k]['danggiaiquyet_quahan'] = $cgq_dqh;
						}
						if($val['iTrangThai']==1 && ($val['sHanGiaiQuyet'] >= $val['sNgayGiaiQuyet'] || $val['sHanGiaiQuyet'] <='1970-01-01'))// đã giải quyết trong hạn
						{
							$dgq_dth++;
							$dsphongban[$k]['dagiaiquyet_tronghan'] = $dgq_dth;
						}
						if($val['iTrangThai']==1 && $val['sHanGiaiQuyet'] > '1970-01-01' && $val['sHanGiaiQuyet'] < $val['sNgayGiaiQuyet'])// đã giải quyết quá hạn
						{
							$dgq_qth++;
							$dsphongban[$k]['dagiaiquyet_quahan'] = $dgq_qth;
						}
					}
					else{
						$dsphongban[$k]['tong'] = $tong;
					}
				}
			}else{
				$dsphongban[$k]['tong'] = $tong;
			}
		}
		// pr($dsphongban);
		if(!empty($thongtin))
		{
			foreach ($thongtin as $key => $value) {
				if(!empty($value['PK_iMaPhongPH']))
				{
					$phongphoihop = $this->Mthongke->layPhongPhoiHop(explode(',',$value['PK_iMaPhongPH']));
					$thongtin[$key]['phongphoihop'] = $phongphoihop;
				}else{
					$thongtin[$key]['phongphoihop'] = '';
				}
				
			}
		}
		// pr($thongtin);
		$objPHPExcel  =new PHPExcel();
		$filename     ='DSVB'.' - '.date('d-m-Y',time());
        $objPHPExcel->getProperties()->setCreator("Administrator")
                                     ->setLastModifiedBy("Administrator")
                                     ->setTitle("Danh sách các văn bản")
                                     ->setSubject("Danh sách các văn bản")
                                     ->setDescription("Danh sách các văn bản")
                                     ->setKeywords("office 2010 openxml php")
                                     ->setCategory("Sở Tài Chính");
        //Căn dòng chữ
        $dinhdang = array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
         $dinhdang2 = array(
            // 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
        // chỉnh kích cỡ cột
        $array_column = array(
                'A' => 5,
                'B' => 9,
                'C' => 10,
                'D' => 13,
                'E' => 40,
                'F' => 13,
                'G' => 26,
                'H' => 14,
                'I' => 11,
                'J' => 20,
                'K' => 11,
                'L' => 11,
                'M' => 11,
                'N' => 11,
                'O' => 11,
                'P' => 11,
                'Q' => 11,
                'R' => 11
            );
            foreach($array_column as $key => $value){
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setAutoSize(false);
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setWidth($value);
            }

        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(13);
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();

        //Phần tiêu đề (nếu có)
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:R1')->setCellValue('A1','Báo cáo tổng hợp ngày '.date('d/m/Y'));
        //end sét chiều vào
        $array_content2 = array(
                'A1');
        // định dạng và in đậm
        foreach($array_content2 as $key => $val){
            $objPHPExcel->getActiveSheet()->getStyle($val)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setSize(16);
        }
        //sét chiều cao
        $array_row = array(
				'1' => 35,
				'2' => 25,
				'3' => 58
            );

        foreach($array_row as $key => $value){
            $objPHPExcel->getActiveSheet()->getRowDimension($key)->setRowHeight($value);
        }
        //end sét chiều vào
        $array_content0 = array(
            'F2' => 'Lãnh đạo Sở giao',
            'K2' => 'Tiến độ'
            );
        // định dạng và in đậm
        $objPHPExcel->getActiveSheet()->mergeCells('F2:H2')->setCellValue('F2','Lãnh đạo Sở giao');
        $objPHPExcel->getActiveSheet()->mergeCells('K2:O2')->setCellValue('K2','Tiến độ');
        foreach($array_content0 as $key => $value){
            $objPHPExcel->getActiveSheet()->getStyle($key)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getFont()->setBold(true);
        }
        $array_content = array(
            'A' => 'STT',
            'B' => 'Số đến',
            'C' => 'Số SKH',
            'D' => 'Ngày ban hành',
            'E' => 'Nội dung văn bản',
            'I' => 'Thời hạn hoàn thành',
            'J' => 'Tình hình triển khai và kết quả đạt được',
            'P' => 'Khó khăn, vướng mắc',
            'Q' => 'Đề xuất hướng xử lý',
            'R' => 'Ghi Chú'
            );
        // định dạng và in đậm
        foreach($array_content as $key => $value){
            $objPHPExcel->getActiveSheet()->mergeCells($key.'2:'.$key.'3')->setCellValue($key.'2',$value);
            $objPHPExcel->getActiveSheet()->getStyle($key.'2')->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key.'2')->getFont()->setBold(true);
        }
        $array_content1 = array(
			'F3' => 'Ngày giao',
			'G3' => 'Phòng, ban, đơn vị chủ trì',
			'H3' => 'Phòng, ban, đơn vị phối hợp',
			'K3'  => 'Tổng số',
			'L3'  => 'Đang triển khai (Chưa đến hạn)',
			'M3'  => 'Đang triển khai (Đã quá hạn)',
			'N3'  => 'Hoàn thành (Trong hạn)',
			'O3'  => 'Hoàn thành (Quá hạn)'
            );
        // định dạng và in đậm
        foreach($array_content1 as $key => $value){
            $objPHPExcel->getActiveSheet()->setCellValue($key,$value);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getFont()->setBold(true);
        }
        $dsphongsl='';
        $startRow = 4;
        $stt=1;
        if(!empty($dsphongban)){
        	foreach ($dsphongban as $key => $pb) {
        		$objPHPExcel->getActiveSheet()->mergeCells('A'.$startRow.':J'.$startRow)->setCellValue('A'.$startRow,$pb['sTenPB'])->getStyle('A'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
        		$objPHPExcel->getActiveSheet()->setCellValue('K'.$startRow,$pb['tong'])->getStyle('K'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('L'.$startRow,(isset($pb['danggiaiquyet_tronghan'])?$pb['danggiaiquyet_tronghan']:0))->getStyle('L'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('L'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('M'.$startRow,(isset($pb['danggiaiquyet_quahan'])?$pb['danggiaiquyet_quahan']:0))->getStyle('M'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('M'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
        		$objPHPExcel->getActiveSheet()->setCellValue('N'.$startRow,(isset($pb['dagiaiquyet_tronghan'])?$pb['dagiaiquyet_tronghan']:0))->getStyle('N'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('N'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->setCellValue('O'.$startRow,(isset($pb['dagiaiquyet_quahan'])?$pb['dagiaiquyet_quahan']:0))->getStyle('O'.$startRow)->getFont()->setBold(true);
        		$objPHPExcel->getActiveSheet()->getStyle('O'.$startRow)->getAlignment()->applyFromArray($dinhdang);
        		$objPHPExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(25);
        		$stt2=1;
        		$startRow++;
        		if(!empty($thongtin)){
        			foreach ($thongtin as $key => $value) {
        				if($value['PK_iMaPhongCT']==$pb['PK_iMaPB']){$phongph='';
        					if (!empty($value['phongphoihop'])){
        						$dem=0;
        						foreach ($value['phongphoihop'] as $ph) {
        							$dem++;
        							($dem>1)?',':'';
        							$phongph= $phongph.$ph['sVietTat'];
        						}
        					}
	        				$objPHPExcel->getActiveSheet()->setCellValue('A'.$startRow,$stt2)->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('B'.$startRow,$value['iSoDen'])->getStyle('B'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('C'.$startRow,$value['sKyHieu'])->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('D'.$startRow,date_select($value['sNgayKy']))->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('E'.$startRow,$value['sMoTa'])->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('F'.$startRow,date_select($value['sThoiGian']))->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('G'.$startRow,$value['sVietTat'])->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('H'.$startRow,$phongph)->getStyle('H'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('I'.$startRow,($value['sHanGiaiQuyet'] >'1970-01-01' && !empty($value['sHanGiaiQuyet']))?date_select($value['sHanGiaiQuyet']):'-')->getStyle('I'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('J'.$startRow,$value['sGhiChu'])->getStyle('J'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('K'.$startRow,'')->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('L'.$startRow,($value['iTrangThai']==0 && $value['sHanGiaiQuyet'] >= date('Y-m-d'))?'x':'')->getStyle('L'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('M'.$startRow,($value['iTrangThai']==0 && $value['sHanGiaiQuyet'] < date('Y-m-d'))?'x':'')->getStyle('M'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('N'.$startRow,($value['iTrangThai']==1 && ($value['sHanGiaiQuyet'] >= $value['sNgayGiaiQuyet'] || $value['sHanGiaiQuyet'] <= '1970-01-01'))?'x':'')->getStyle('N'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('O'.$startRow,($value['iTrangThai']==1 && $value['sHanGiaiQuyet'] > '1970-01-01' && $value['sHanGiaiQuyet'] < $value['sNgayGiaiQuyet'])?'x':'')->getStyle('O'.$startRow)->getAlignment()->applyFromArray($dinhdang);
			        		$objPHPExcel->getActiveSheet()->setCellValue('P'.$startRow,'')->getStyle('P'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$startRow,'')->getStyle('Q'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$objPHPExcel->getActiveSheet()->setCellValue('R'.$startRow,'')->getStyle('R'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
			        		$startRow++;
			        		$stt2++;
			        	}
        			}
        		}
        		$stt++;
        	}
        }
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();
     //    $objPHPExcel->getActiveSheet()
	    // ->getStyle('A2:R3')
	    // ->getFill()
	    // ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
	    // ->getStartColor()
	    // ->setRGB('191239255');
        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A2".":"."R".$maxR)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=".$filename.".xls");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
	}

}

/* End of file Cbaocaotonghop.php */
/* Location: ./application/controllers/thongke/Cbaocaotonghop.php */