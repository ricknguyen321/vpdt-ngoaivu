<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongkevanbantheolanhdao extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongke/Mtonghop_moi');
		$this->load->model('thongke/Mthongkevanbantheolanhdao');
	}
	public function index()
	{
		$data['title']		= 'Văn bản đã chỉ đạo';
		$data['dsphongban']	= $this->Mtonghop_moi->layPhongBanDuThao();
		$data['malanhdao']  = _get('malanhdao');
		if(!empty($data['malanhdao']))
		{
			$taikhoan = $data['malanhdao'];
		}
		else
		{
			$taikhoan			= $this->_session['PK_iMaCB'];
		}
		$mang_phongban	= array_column($data['dsphongban'],'PK_iMaPB');
		$quyen			= $this->_session['iQuyenHan_DHNB'];
		$giamdoc		= $this->Mthongkevanbantheolanhdao->layLD_So(array(4));
		if($giamdoc[0]['PK_iMaCB']==$taikhoan)
		{
			$data['chucvu']				= 'giamdoc';
			$data['vanbanphogiamdoc']	= $this->Mthongkevanbantheolanhdao->layVanBan_GD_ChiDao($taikhoan);
		}
		else
		{
			$data['chucvu']				= 'phogiamdoc';
			$data['vanbanphogiamdoc']	= '';
		}
		$data['vanbanphongban']		= $this->Mthongkevanbantheolanhdao->layVanBan_PGD_ChiDao($taikhoan,$mang_phongban);
		$data['phogiamdoc']			= $this->Mthongkevanbantheolanhdao->layLD_So(array(5));
		$data['taikhoan']			= $taikhoan;
		$temp['data']				= $data;
		$temp['template']			= 'thongke/Vthongkevanbantheolanhdao';
		$this->load->view('layout_admin/layout',$temp);
	}

}

/* End of file Cthongkevanbantheolanhdao.php */
/* Location: ./application/controllers/thongke/Cthongkevanbantheolanhdao.php */