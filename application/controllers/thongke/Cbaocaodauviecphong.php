<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cbaocaodauviecphong extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('thongke/Mthongke','Mthongke');
        $this->Mthongke = new Mthongke();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->library('Excel');
	}
	public function index()
	{
		if(!empty($this->input->get('ngaynhap'))){
            $ngaynhap      = ($this->input->get('ngaynhap') > '01/01/2017') ? date_insert($this->input->get('ngaynhap')) : NULL;
//            pr($ngaynhap);
            $data['ngaynhap'] = $ngaynhap;
            $ngayden       = ($this->input->get('ngayden') > '01/01/2017') ? date_insert($this->input->get('ngayden')) : NULL;
            $data['ngayden'] = $ngayden;
            $phongban    = $this->Mthongke->getDepartment('0');
            $mangMaPB = array();
            if(!empty($phongban))
            {
                foreach ($phongban as $key => $value) {
                    $mangMaPB[] = $value['PK_iMaPB'];
                }
            }
            $id = NULL;
            if(empty($id)){
                $id = $this->input->get('id');
                $data['id']    = $id;
            }
            foreach($mangMaPB as $keypb => $valuepb){
                $chuagiaiquyet_tronghan = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,1,NULL,$ngaynhap,$ngayden,$valuepb,$id);
                $chuagiaiquyet_quahan   = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,2,NULL,$ngaynhap,$ngayden,$valuepb,$id);
//        pr($chuagiaiquyet_tronghan);
                $dagiaiquyet_dunghan    = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,1,$ngaynhap,$ngayden,$valuepb,$id);
//		pr($dagiaiquyet_dunghan);
                $dagiaiquyet_quahan     = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,2,$ngaynhap,$ngayden,$valuepb,$id);
//		pr($dagiaiquyet_quahan);
                $sangtao                = $this->Mthongke->layDauViecTheoPhong($valuepb,1,1,NULL,NULL,$ngaynhap,$ngayden,$valuepb,$id);
                $tongdauviec			= $this->Mthongke->tongDauViecTheoPhong($valuepb,$ngaynhap,$ngayden,$valuepb,$id);


                $data['mang_tongdauviec'][$valuepb] = count($tongdauviec);
                $data['mang_chuagiaiquyet_tronghan'][$valuepb] =count($chuagiaiquyet_tronghan);
                $data['mang_chuagiaiquyet_quahan'][$valuepb]   =count($chuagiaiquyet_quahan);
                $data['mang_dagiaiquyet_dunghan'][$valuepb]    =count($dagiaiquyet_dunghan);
                $data['mang_dagiaiquyet_quahan'][$valuepb]    =count($dagiaiquyet_quahan);
                $data['mang_sangtao'][$valuepb]                =count($sangtao);

//            pr($tongdauviec);
            }
            $data['phongban']    = $phongban;
        }else{
			$phongban    = $this->Mthongke->getDepartment('0');
			$mangMaPB = array();
			if(!empty($phongban))
			{
				foreach ($phongban as $key => $value) {
					$mangMaPB[] = $value['PK_iMaPB'];
				}
			}
			$idLanhDao = $this->input->get('id');
			$data['id']    = $idLanhDao;
			foreach($mangMaPB as $keypb => $valuepb){
				$chuagiaiquyet_tronghan = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,1,NULL,NULL,NULL,$valuepb,$idLanhDao);
				$chuagiaiquyet_quahan   = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,2,NULL,NULL,NULL,$valuepb,$idLanhDao);
	//        pr($chuagiaiquyet_tronghan);
				$dagiaiquyet_dunghan    = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,1,NULL,NULL,$valuepb,$idLanhDao);
	//		pr($dagiaiquyet_dunghan);
				$dagiaiquyet_quahan     = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,2,NULL,NULL,$valuepb,$idLanhDao);
	//		pr($dagiaiquyet_quahan);
				$sangtao                = $this->Mthongke->layDauViecTheoPhong($valuepb,1,1,NULL,NULL,NULL,NULL,$valuepb,$idLanhDao);
				$tongdauviec			= $this->Mthongke->tongDauViecTheoPhong($valuepb,NULL,NULL,$valuepb,$idLanhDao);


				$data['mang_tongdauviec'][$valuepb] = count($tongdauviec);
				$data['mang_chuagiaiquyet_tronghan'][$valuepb] =count($chuagiaiquyet_tronghan);
				$data['mang_chuagiaiquyet_quahan'][$valuepb]   =count($chuagiaiquyet_quahan);
				$data['mang_dagiaiquyet_dunghan'][$valuepb]    =count($dagiaiquyet_dunghan);
				$data['mang_dagiaiquyet_quahan'][$valuepb]    =count($dagiaiquyet_quahan);
				$data['mang_sangtao'][$valuepb]                =count($sangtao);

	//            pr($tongdauviec);
			}
			$data['phongban']    = $phongban;
		}
		if(_get('xuat_excel'))
		{
			$this->export_excel();
		}
			
		$data['title']    = 'Thống kê văn bản đến theo phòng';
		$temp['data']     = $data;
		$temp['template'] = 'thongke/Vbaocaodauviecphong';
		$this->load->view('layout_admin/layout',$temp);
	}
	
	public function export_excel()
	{
		$tongdauviecTP = 0;
		$chuagiaiquyet_tronghan_TP = 0;
		$chuagiaiquyet_quahan_TP = 0;
		$dagiaiquyet_dunghan_TP = 0;
		$dagiaiquyet_quahan_TP = 0;
		if(!empty($this->input->get('ngaynhap'))){
            $ngaynhap      = ($this->input->get('ngaynhap') > '01/01/2017') ? date_insert($this->input->get('ngaynhap')) : NULL;
//            pr($ngaynhap);
            $data['ngaynhap'] = $ngaynhap;
            $ngayden       = ($this->input->get('ngayden') > '01/01/2017') ? date_insert($this->input->get('ngayden')) : NULL;
            $data['ngayden'] = $ngayden;
            $phongban    = $this->Mthongke->getDepartment('0');
            $mangMaPB = array();
            if(!empty($phongban))
            {
                foreach ($phongban as $key => $value) {
                    $mangMaPB[] = $value['PK_iMaPB'];
                }
            }
            $id = NULL;
            if(empty($id)){
                $id = $this->input->get('id');
                $data['id']    = $id;
            }
            foreach($mangMaPB as $keypb => $valuepb){
                $chuagiaiquyet_tronghan = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,1,NULL,$ngaynhap,$ngayden,$valuepb,$id);
                $chuagiaiquyet_quahan   = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,2,NULL,$ngaynhap,$ngayden,$valuepb,$id);
//        pr($chuagiaiquyet_tronghan);
                $dagiaiquyet_dunghan    = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,1,$ngaynhap,$ngayden,$valuepb,$id);
//		pr($dagiaiquyet_dunghan);
                $dagiaiquyet_quahan     = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,2,$ngaynhap,$ngayden,$valuepb,$id);
//		pr($dagiaiquyet_quahan);
                $sangtao                = $this->Mthongke->layDauViecTheoPhong($valuepb,1,1,NULL,NULL,$ngaynhap,$ngayden,$valuepb,$id);
                $tongdauviec			= $this->Mthongke->tongDauViecTheoPhong($valuepb,$ngaynhap,$ngayden,$valuepb,$id);


                $mang_tongdauviec[$valuepb] = count($tongdauviec);
                $mang_chuagiaiquyet_tronghan[$valuepb] =count($chuagiaiquyet_tronghan);
                $mang_chuagiaiquyet_quahan[$valuepb]   =count($chuagiaiquyet_quahan);
                $mang_dagiaiquyet_dunghan[$valuepb]    =count($dagiaiquyet_dunghan);
                $mang_dagiaiquyet_quahan[$valuepb]    =count($dagiaiquyet_quahan);
                $mang_sangtao[$valuepb]                =count($sangtao);
				
				$tongdauviecTP = $tongdauviecTP + count($tongdauviec);
				$chuagiaiquyet_tronghan_TP = $chuagiaiquyet_tronghan_TP + count($chuagiaiquyet_tronghan);
				$chuagiaiquyet_quahan_TP = $chuagiaiquyet_quahan_TP + count($chuagiaiquyet_quahan);
				$dagiaiquyet_dunghan_TP = $dagiaiquyet_dunghan_TP + count($dagiaiquyet_dunghan);
				$dagiaiquyet_quahan_TP = $dagiaiquyet_quahan_TP + count($dagiaiquyet_quahan);

            }
        }else{
			$phongban    = $this->Mthongke->getDepartment('0');
			$mangMaPB = array();
			if(!empty($phongban))
			{
				foreach ($phongban as $key => $value) {
					$mangMaPB[] = $value['PK_iMaPB'];
				}
			}
			$idLanhDao = $this->input->get('id');
			$data['id']    = $idLanhDao;
			foreach($mangMaPB as $keypb => $valuepb){
				$chuagiaiquyet_tronghan = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,1,NULL,NULL,NULL,$valuepb,$idLanhDao);
				$chuagiaiquyet_quahan   = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,2,NULL,NULL,NULL,$valuepb,$idLanhDao);
	//        pr($chuagiaiquyet_tronghan);
				$dagiaiquyet_dunghan    = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,1,NULL,NULL,$valuepb,$idLanhDao);
	//		pr($dagiaiquyet_dunghan);
				$dagiaiquyet_quahan     = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,2,NULL,NULL,$valuepb,$idLanhDao);
	//		pr($dagiaiquyet_quahan);
				$sangtao                = $this->Mthongke->layDauViecTheoPhong($valuepb,1,1,NULL,NULL,NULL,NULL,$valuepb,$idLanhDao);
				$tongdauviec			= $this->Mthongke->tongDauViecTheoPhong($valuepb,NULL,NULL,$valuepb,$idLanhDao);


				$mang_tongdauviec[$valuepb] = count($tongdauviec);
				$mang_chuagiaiquyet_tronghan[$valuepb] =count($chuagiaiquyet_tronghan);
				$mang_chuagiaiquyet_quahan[$valuepb]   =count($chuagiaiquyet_quahan);
				$mang_dagiaiquyet_dunghan[$valuepb]    =count($dagiaiquyet_dunghan);
				$mang_dagiaiquyet_quahan[$valuepb]    =count($dagiaiquyet_quahan);
				$mang_sangtao[$valuepb]                =count($sangtao);
				
				$tongdauviecTP = $tongdauviecTP + count($tongdauviec);
				$chuagiaiquyet_tronghan_TP = $chuagiaiquyet_tronghan_TP + count($chuagiaiquyet_tronghan);
				$chuagiaiquyet_quahan_TP = $chuagiaiquyet_quahan_TP + count($chuagiaiquyet_quahan);
				$dagiaiquyet_dunghan_TP = $dagiaiquyet_dunghan_TP + count($dagiaiquyet_dunghan);
				$dagiaiquyet_quahan_TP = $dagiaiquyet_quahan_TP + count($dagiaiquyet_quahan);

			}
		}
		
		
		
		
		// xu ly file excel
		$objPHPExcel  =new PHPExcel();
		$filename     ='Báo cáo tổng hợp văn bản đến'.' - '.date('d-m-Y',time());
        $objPHPExcel->getProperties()->setCreator("Administrator")
                                     ->setLastModifiedBy("Administrator")
                                     ->setTitle("Danh sách các văn bản")
                                     ->setSubject("Danh sách các văn bản")
                                     ->setDescription("Danh sách các văn bản")
                                     ->setKeywords("office 2010 openxml php")
                                     ->setCategory("Sở Ngoại vụ");
        //Căn dòng chữ
        $dinhdang = array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
         $dinhdang2 = array(
            // 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, //giua theo chieu ngang
            'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER, //giua theo chieu doc
            'rotation'   => 0, //khong quay
            'wrap'       => true //tu dong xuong dong
            );
        // chỉnh kích cỡ cột
        $array_column = array(
                'A' => 5,
                'B' => 22,
                'C' => 10,
                'D' => 10,
                'E' => 10,
                'F' => 10,
                'G' => 10,
                'H' => 10,
                'I' => 10,
                'J' => 10
            );
            foreach($array_column as $key => $value){
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setAutoSize(false);
                $objPHPExcel->getActiveSheet()->getColumnDimension($key)->setWidth($value);
            }
		
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(13);
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();

        //Phần tiêu đề (nếu có)
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:J1')->setCellValue('A1','Báo cáo tổng hợp văn bản đến');
        //end sét chiều vào
        $array_content2 = array(
                'A1');
        // định dạng và in đậm
        foreach($array_content2 as $key => $val){
            $objPHPExcel->getActiveSheet()->getStyle($val)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($val)->getFont()->setSize(16);
        }
        //sét chiều cao
        $array_row = array(
				'1' => 25,
				'2' => 25,
				'3' => 25,
				'4' => 25
            );
        // nhiệm vụ hoàn thành trong kỳ

        foreach($array_row as $key => $value){
            $objPHPExcel->getActiveSheet()->getRowDimension($key)->setRowHeight($value);
        }
        //end sét chiều vào
        $array_content0 = array(
            'D2' => '2. Đã giải quyết',
            'H2' => '3. Chưa giải quyết'
            );
        // định dạng và in đậm
        $objPHPExcel->getActiveSheet()->mergeCells('D2:G2')->setCellValue('D2','2. Đã giải quyết');
		$objPHPExcel->getActiveSheet()->mergeCells('H2:J2')->setCellValue('H2','3. Chưa giải quyết');
      
        foreach($array_content0 as $key => $value){
            $objPHPExcel->getActiveSheet()->getStyle($key)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getFont()->setBold(true);
        }
       
        // định dạng và in đậm
		$objPHPExcel->getActiveSheet()->mergeCells('A2:A3')->setCellValue('A2','STT');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->mergeCells('B2:B3')->setCellValue('B2','Họ và tên');
            $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->mergeCells('C2:C3')->setCellValue('C2','1. Tổng số VB đến');
            $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
        $array_content1 = array(
			'D3' => 'Tổng',
			'E3' => 'Đúng hạn',
			'F3' => 'Quá hạn',
			'G3' => 'Sáng tạo',
			'H3' => 'Tổng',
			'I3' => 'Trong hạn',
			'J3' => 'Quá hạn'			
            );
        // định dạng và in đậm
        foreach($array_content1 as $key => $value){
            $objPHPExcel->getActiveSheet()->setCellValue($key,$value);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getAlignment()->applyFromArray($dinhdang);
            $objPHPExcel->getActiveSheet()->getStyle($key)->getFont()->setBold(true);
        }
        $startRow = 4;
        $stt2=1;
		
		
		
		foreach ($phongban as $key => $value) {
				
				$objPHPExcel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('J'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				
				// CHÈN DỮ LIỆU
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$startRow,$stt2)->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$startRow,$value['sTenPB'])->getStyle('B'.$startRow);
				if (isset($mang_tongdauviec[$value['PK_iMaPB']])) {
					$valc = $mang_dagiaiquyet_quahan[$value['PK_iMaPB']]+$mang_dagiaiquyet_dunghan[$value['PK_iMaPB']]+$mang_chuagiaiquyet_tronghan[$value['PK_iMaPB']]+$mang_chuagiaiquyet_quahan[$value['PK_iMaPB']];
					} else {
						$valc = 0;
					};				
        		$objPHPExcel->getActiveSheet()->setCellValue('C'.$startRow,$valc)->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($mang_dagiaiquyet_dunghan[$value['PK_iMaPB']]) && isset($mang_dagiaiquyet_quahan[$value['PK_iMaPB']])){
					$vald = $mang_dagiaiquyet_quahan[$value['PK_iMaPB']]+$mang_dagiaiquyet_dunghan[$value['PK_iMaPB']];
					} elseif (isset($mang_dagiaiquyet_quahan[$value['PK_iMaPB']])){
						$vald = $mang_dagiaiquyet_quahan[$value['PK_iMaPB']];
						} elseif (isset($mang_dagiaiquyet_dunghan[$value['PK_iMaPB']])){
							$mang_dagiaiquyet_dunghan[$value['PK_iMaPB']];
							} else $vald = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$startRow,$vald)->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($mang_dagiaiquyet_dunghan[$value['PK_iMaPB']])) $vale = $mang_dagiaiquyet_dunghan[$value['PK_iMaPB']]; else $vale = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$startRow,$vale)->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($mang_dagiaiquyet_quahan[$value['PK_iMaPB']])) $valf = $mang_dagiaiquyet_quahan[$value['PK_iMaPB']]; else $valf = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$startRow,$valf)->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($mang_sangtao[$value['PK_iMaPB']])) $valg = $mang_sangtao[$value['PK_iMaPB']]; else $valg = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$startRow,$valg)->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($mang_chuagiaiquyet_tronghan[$value['PK_iMaPB']]) && isset($mang_chuagiaiquyet_quahan[$value['PK_iMaPB']])){
					$valh = $mang_chuagiaiquyet_tronghan[$value['PK_iMaPB']] + $mang_chuagiaiquyet_quahan[$value['PK_iMaPB']];
					} elseif (isset($mang_chuagiaiquyet_tronghan[$value['PK_iMaPB']])){
						$valh = $mang_chuagiaiquyet_tronghan[$value['PK_iMaPB']];
						} elseif (isset($mang_chuagiaiquyet_quahan[$value['PK_iMaPB']])){
							$valh = $mang_chuagiaiquyet_quahan[$value['PK_iMaPB']];
							} else $valh = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$startRow,$valh)->getStyle('H'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($mang_chuagiaiquyet_tronghan[$value['PK_iMaPB']])) {
					$vali = $mang_chuagiaiquyet_tronghan[$value['PK_iMaPB']];
					} else {
						$vali = 0;}
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$startRow,$vali)->getStyle('I'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($mang_chuagiaiquyet_quahan[$value['PK_iMaPB']])) {
					$valj = $mang_chuagiaiquyet_quahan[$value['PK_iMaPB']];
					} else {
						$valj = 0;}
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$startRow,$valj)->getStyle('J'.$startRow)->getAlignment()->applyFromArray($dinhdang);

        		$stt2++;
        		$startRow++;
				
            }
			
			// TỔNG
				$objPHPExcel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('J'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('K'.$startRow)->getAlignment()->applyFromArray($dinhdang2);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('J'.$startRow)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('K'.$startRow)->getFont()->setBold(true);

				
				// CHÈN DỮ LIỆU
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$startRow,'Tổng')->getStyle('B'.$startRow);
				if (isset($tongdauviecTP)){
					$valc = $dagiaiquyet_dunghan_TP + $dagiaiquyet_quahan_TP + $chuagiaiquyet_tronghan_TP + $chuagiaiquyet_quahan_TP;
					} else $valc = 0;		
        		$objPHPExcel->getActiveSheet()->setCellValue('C'.$startRow,$valc)->getStyle('C'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($dagiaiquyet_dunghan_TP) && isset($dagiaiquyet_quahan_TP)){ 
					$vald = $dagiaiquyet_dunghan_TP + $dagiaiquyet_quahan_TP;
					} elseif (isset($dagiaiquyet_dunghan_TP)){
						$vald = $dagiaiquyet_dunghan_TP;
						} elseif (isset($dagiaiquyet_quahan_TP)){ 
							$vald = $dagiaiquyet_quahan_TP;
							} else $vald = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$startRow,$vald)->getStyle('D'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($dagiaiquyet_dunghan_TP)) $vale = $dagiaiquyet_dunghan_TP; else $vale = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$startRow,$vale)->getStyle('E'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($dagiaiquyet_quahan_TP)) $valf = $dagiaiquyet_quahan_TP; else $valf = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$startRow,$valf)->getStyle('F'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				$valg = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$startRow,$valg)->getStyle('G'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($chuagiaiquyet_tronghan_TP) && isset($chuagiaiquyet_quahan_TP)){$valh = $chuagiaiquyet_tronghan_TP + $chuagiaiquyet_quahan_TP;} elseif (isset($chuagiaiquyet_tronghan_TP)){$valh = $chuagiaiquyet_tronghan_TP;} elseif (isset($chuagiaiquyet_quahan_TP)){$valh = $chuagiaiquyet_quahan_TP;} else $valh = 0;
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$startRow,$valh)->getStyle('H'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($chuagiaiquyet_tronghan_TP)) {
					$vali = $chuagiaiquyet_tronghan_TP;
					} else {
						$vali = 0;}
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$startRow,$vali)->getStyle('I'.$startRow)->getAlignment()->applyFromArray($dinhdang);
				
				if (isset($chuagiaiquyet_quahan_TP)) {
					$valj = $chuagiaiquyet_quahan_TP;
					} else {
						$valj = 0;}
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$startRow,$valj)->getStyle('J'.$startRow)->getAlignment()->applyFromArray($dinhdang);

       
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow()+1;
       
       
        $maxR=$objPHPExcel->getActiveSheet()->getHighestRow();
        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A2".":"."J".$maxR)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=".$filename.".xls");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
	}

}

/* End of file Cbaocaodauviecphong.php */
/* Location: ./application/controllers/thongke/Cbaocaodauviecphong.php */