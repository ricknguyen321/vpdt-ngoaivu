<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongkevanbantheophong extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongke/Mtonghop_moi');
		$this->load->model('thongke/Mthongkevanbantheolanhdao');
	}
	public function index()
	{
		$data['title']				= 'Văn bản đã chỉ đạo';
		$phongban					= _get('phongban');
		$phogiamdoc					= _get('phogiamdoc');
		$giamdoc					= _get('giamdoc');
		$data['dl_phongban']		= $phongban;
		$data['dl_phogiamdoc']		= $phogiamdoc;
		$data['dl_giamdoc']			= $giamdoc;
		
		$data['chualam_quahan']		= $this->Mthongkevanbantheolanhdao->demVB_ChuaLam_QuaHan($phongban,$phogiamdoc,$giamdoc);
		$data['chualam_tronghan']	= $this->Mthongkevanbantheolanhdao->demVB_ChuaLam_TrongHan($phongban,$phogiamdoc,$giamdoc);
		$data['dalam_quahan']		= $this->Mthongkevanbantheolanhdao->demVB_DaLam_QuaHan($phongban,$phogiamdoc,$giamdoc);
		$data['dalam_tronghan']		= $this->Mthongkevanbantheolanhdao->demVB_DaLam_TrongHan($phongban,$phogiamdoc,$giamdoc);
		if(!empty($giamdoc)&&!empty($phogiamdoc))
		{
			$data['phogiamdoc']		    = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$phogiamdoc,'tbl_canbo');
		}
		else{
			$data['phogiamdoc']		    = '';
		}
		if(!empty($phongban))
		{
			$data['phongban']		    = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$phongban,'tbl_phongban');
		}
		else{
			$data['phongban']		    = '';
		}
		$temp['data']				= $data;
		$temp['template']			= 'thongke/Vthongkevanbantheophong3';
		$this->load->view('layout_admin/layout',$temp);
	}
}

/* End of file Cthongkevanbantheophong.php */
/* Location: ./application/controllers/thongke/Cthongkevanbantheophong.php */