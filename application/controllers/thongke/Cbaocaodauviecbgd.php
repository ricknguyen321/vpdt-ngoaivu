<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cbaocaodauviecbgd extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('thongke/Mthongke','Mthongke');
        $this->Mthongke = new Mthongke();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
    }
    public function index()
    {
        if(!empty($this->input->get('ngaynhap'))){
            $ngaynhap      = ($this->input->get('ngaynhap') > '01/01/2017') ? date_insert($this->input->get('ngaynhap')) : NULL;
//            pr($ngaynhap);
            $data['ngaynhap'] = $ngaynhap;
            $ngayden       = ($this->input->get('ngayden') > '01/01/2017') ? date_insert($this->input->get('ngayden')) : NULL;
            $data['ngayden'] = $ngayden;
            $phongban    = $this->Mthongke->getDepartment('0');
            $bangiamdoc    = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',73,'tbl_canbo');
//            pr($bangiamdoc);
            $mangMaPB = array();
            if(!empty($phongban))
            {
                foreach ($phongban as $key => $value) {
                    $mangMaPB[] = $value['PK_iMaPB'];
                }
            }
            $idLanhDao = $this->input->get('id');
            $data['id']    = $idLanhDao;
            foreach($mangMaPB as $keypb => $valuepb){
                $chuagiaiquyet_tronghan = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,1,NULL,$ngaynhap,$ngayden,$valuepb,$idLanhDao);
                $chuagiaiquyet_quahan   = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,2,NULL,$ngaynhap,$ngayden,$valuepb,$idLanhDao);
//        pr($chuagiaiquyet_tronghan);
                $dagiaiquyet_dunghan    = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,1,$ngaynhap,$ngayden,$valuepb,$idLanhDao);
//		pr($dagiaiquyet_dunghan);
                $dagiaiquyet_quahan     = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,2,$ngaynhap,$ngayden,$valuepb,$idLanhDao);
//		pr($dagiaiquyet_quahan);
                $sangtao                = $this->Mthongke->layDauViecTheoPhong($valuepb,1,1,NULL,NULL,$ngaynhap,$ngayden,$valuepb,$idLanhDao);
                $tongdauviec			= $this->Mthongke->tongDauViecTheoPhong($valuepb,$ngaynhap,$ngayden,$valuepb,$idLanhDao);


                $data['mang_tongdauviec'][$valuepb] = count($tongdauviec);
                $data['mang_chuagiaiquyet_tronghan'][$valuepb] =count($chuagiaiquyet_tronghan);
                $data['mang_chuagiaiquyet_quahan'][$valuepb]   =count($chuagiaiquyet_quahan);
                $data['mang_dagiaiquyet_dunghan'][$valuepb]    =count($dagiaiquyet_dunghan);
                $data['mang_dagiaiquyet_quahan'][$valuepb]    =count($dagiaiquyet_quahan);
                $data['mang_sangtao'][$valuepb]                =count($sangtao);

//            pr($tongdauviec);
            }
            $data['phongban']    = $phongban;
        }else{
            $phongban    = $this->Mthongke->getDepartment('0');
            $bangiamdoc    = $this->Mdanhmuc->layDuLieu3where('FK_iMaPhongHD',73,'iTrangThai !=',1,'FK_iMaCV',2,'tbl_canbo');
//            pr($bangiamdoc);
            $mangMaPB = array();
            if(!empty($phongban))
            {
                foreach ($phongban as $key => $value) {
                    $mangMaPB[] = $value['PK_iMaPB'];
                }
            }
            $mangPGD= array();
            if(!empty($bangiamdoc))
            {
                foreach ($bangiamdoc as $key1 => $value1) {
                    $mangPGD[] = $value1['PK_iMaCB'];
                }
            }
//            pr($mangPGD);

            foreach($mangPGD as $keybg => $valuebg){
                $chuagiaiquyet_tronghan_bgd = $this->Mthongke->layDauViecTheoBGD($valuebg,array(0,2),NULL,1,NULL,NULL,NULL);
                $chuagiaiquyet_quahan_bgd   = $this->Mthongke->layDauViecTheoBGD($valuebg,array(0,2),NULL,2,NULL,NULL,NULL);
//        pr($chuagiaiquyet_quahan_bgd);
                $dagiaiquyet_dunghan_bgd    = $this->Mthongke->layDauViecTheoBGD($valuebg,1,NULL,NULL,1,NULL,NULL);
//		pr($dagiaiquyet_dunghan_bgd);
                $dagiaiquyet_quahan_bgd     = $this->Mthongke->layDauViecTheoBGD($valuebg,1,NULL,NULL,2,NULL,NULL);
//		pr($dagiaiquyet_quahan);
                $sangtao_bgd                = $this->Mthongke->layDauViecTheoBGD($valuebg,1,1,NULL,NULL,NULL,NULL);
                $tongdauviec_bgd			= $this->Mthongke->tongDauViecTheoBGD($valuebg,NULL,NULL);


                $data['mang_tongdauviec_bgd'][$valuebg] = count($tongdauviec_bgd);
                $data['mang_chuagiaiquyet_tronghan_bgd'][$valuebg] =count($chuagiaiquyet_tronghan_bgd);
                $data['mang_chuagiaiquyet_quahan_bgd'][$valuebg]   =count($chuagiaiquyet_quahan_bgd);
                $data['mang_dagiaiquyet_dunghan_bgd'][$valuebg]    =count($dagiaiquyet_dunghan_bgd);
                $data['mang_dagiaiquyet_quahan_bgd'][$valuebg]    =count($dagiaiquyet_quahan_bgd);
                $data['mang_sangtao_bgd'][$valuebg]                =count($sangtao_bgd);

//            pr($tongdauviec_bgd);
            }
//        pr($bangiamdoc);
            $data['bangiamdoc'] = $bangiamdoc;
            foreach($mangMaPB as $keypb => $valuepb){
                $chuagiaiquyet_tronghan = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,1,NULL,NULL,NULL,$valuepb);
                $chuagiaiquyet_quahan   = $this->Mthongke->layDauViecTheoPhong($valuepb,array(0,2),NULL,2,NULL,NULL,NULL,$valuepb);
//        pr($chuagiaiquyet_tronghan);
                $dagiaiquyet_dunghan    = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,1,NULL,NULL,$valuepb);
//		pr($dagiaiquyet_dunghan);
                $dagiaiquyet_quahan     = $this->Mthongke->layDauViecTheoPhong($valuepb,1,NULL,NULL,2,NULL,NULL,$valuepb);
//		pr($dagiaiquyet_quahan);
                $sangtao                = $this->Mthongke->layDauViecTheoPhong($valuepb,1,1,NULL,NULL,NULL,NULL,$valuepb);
                $tongdauviec			= $this->Mthongke->tongDauViecTheoPhong($valuepb,NULL,NULL,$valuepb);


                $data['mang_tongdauviec'][$valuepb] = count($tongdauviec);
                $data['mang_chuagiaiquyet_tronghan'][$valuepb] =count($chuagiaiquyet_tronghan);
                $data['mang_chuagiaiquyet_quahan'][$valuepb]   =count($chuagiaiquyet_quahan);
                $data['mang_dagiaiquyet_dunghan'][$valuepb]    =count($dagiaiquyet_dunghan);
                $data['mang_dagiaiquyet_quahan'][$valuepb]    =count($dagiaiquyet_quahan);
                $data['mang_sangtao'][$valuepb]                =count($sangtao);

//            pr($tongdauviec);
            }
            $data['phongban']    = $phongban;
        }
        $data['title']    = 'Báo cáo theo đầu công việc theo phòng';
        $temp['data']     = $data;
        $temp['template'] = 'thongke/Vbaocaodauviecbgd';
        $this->load->view('layout_admin/layout',$temp);
    }

}

/* End of file Cbaocaodauviecbgd.php */
/* Location: ./application/controllers/thongke/Cbaocaodauviecbgd.php */