<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cinsoluutru_den extends MY_Controller {
	protected $_thongtin;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('thongke/Mthongke');
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['title']      = 'In sổ lưu trữ';
		$data['dsphongban'] = $this->Mvanbandi->layPhongBanDuThao();
		$data['nguoiky']    = $this->Mvanbandi->layNguoiKy();
		$data['loaivanban']    = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaivanban');
		$data['noiguiden']    = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_donvi');
		$this->_thongtin = $this->layDanhSach();
		if(!empty($this->_thongtin))
		{
			$data['danhsach'] = $this->_thongtin['info'];
			foreach ($data['danhsach'] as $key => $value) {
				$nguoigiaiquyet = $this->Mthongke->layNguoiGiaiQuyetCuoi($value['PK_iMaVBDen']);
				if(!empty($nguoigiaiquyet))
				{
					$data['danhsach'][$key]['nguoigiaiquyet'] = $nguoigiaiquyet['sHoTen'];
				}
				else{
					$data['danhsach'][$key]['nguoigiaiquyet'] = '';
				}
			}
		}
		else{
			$data['danhsach'] = '';
		}
		// pr($data['danhsach']);
		$data['thongtin'] = $this->_thongtin;
		$temp['data']     = $data;
		if(_get('download')){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=luutru_".$_SESSION['nam'].".doc");
            $temp['template'] = 'thongke/Vinsoluutru_d';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
			$temp['template'] = 'thongke/Vinsoluutru_den';
			$this->load->view('layout_admin/layout',$temp);
		}
	}
	public function layDanhSach()
	{
		$soditu      = _get('soditu');
		$sodiden     = _get('sodiden');
		$ngaynhaptu  = _get('ngaynhaptu');
		$ngaynhapden = _get('ngaynhapden');
		$noiguiden   = _get('noiguiden');
		$loaivanban  = _get('loaivanban');
		$ngaymoitu   = _get('ngaymoitu');
		$ngaymoiden  = _get('ngaymoiden');
		$phongban    = _get('phongban');
		$lanhdao     = _get('lanhdao');
		$download    = _get('download');
		$quyen = $this->_session['iQuyenHan_DHNB'];
		if($quyen!=2 && $quyen!=9)
		{
			$phongban = $this->_session['FK_iMaPhongHD'];
		}
		if($quyen==8)
		{	
			$canbo = $this->_session['PK_iMaCB'];
		}
		else
		{
			$canbo = '';
		}
		if(!empty($soditu)||!empty($sodiden)||!empty($ngaynhaptu)||!empty($ngaynhapden)||!empty($noiguiden)||!empty($loaivanban)||!empty($ngaymoitu)||!empty($ngaymoiden)||!empty($phongban)||!empty($lanhdao)){
		$config['base_url']             = base_url().'insoluutru_den?ngaynhaptu='.$ngaynhaptu.'&ngaynhapden='.$ngaynhapden.'&soditu='.$soditu.'&sodiden='.$sodiden.'&sodiden='.$sodiden.'&noiguiden='.$noiguiden.'&loaivanban='.$loaivanban.'&ngaymoitu='.$ngaymoitu.'&ngaymoiden='.$ngaymoiden.'&lanhdao='.$lanhdao.'&phongban='.$phongban;
		$config['total_rows']           = $this->Mthongke->dem_insoluutru_den($soditu,$sodiden,$ngaynhaptu,$ngaynhapden,$noiguiden,$loaivanban,$ngaymoitu,$ngaymoiden,$phongban,$lanhdao,$canbo);
		$config['per_page']             = ($download)?100000000:100;
		// pr($config['per_page']);
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'insoluutru_den');
      	}
		$data['soditu']      = $soditu;
		$data['sodiden']     = $sodiden;
		$data['ngaynhaptu']  = $ngaynhaptu;
		$data['ngaynhapden'] = $ngaynhapden;
		$data['noiguiden']   = $noiguiden;
		$data['loaivanban']  = $loaivanban;
		$data['ngaymoitu']   = $ngaymoitu;
		$data['ngaymoiden']  = $ngaymoiden;
		$data['phongban']    = $phongban;
		$data['lanhdao']     = $lanhdao;
		$data['info']        = $this->Mthongke->insoluutru_den($soditu,$sodiden,$ngaynhaptu,$ngaynhapden,$noiguiden,$loaivanban,$ngaymoitu,$ngaymoiden,$phongban,$lanhdao,$canbo,$config['per_page'], $data['page']);
		$data['phantrang']   = $this->pagination->create_links();
		return $data;
		}
	}

}

/* End of file Cinsoluutru_den.php */
/* Location: ./application/controllers/thongke/Cinsoluutru_den.php */