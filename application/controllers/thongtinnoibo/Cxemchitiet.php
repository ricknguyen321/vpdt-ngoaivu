<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemchitiet extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		
		$data['thongtin'] = $this->_thongtin;
		$mavanban         = _get('id');
		$taikhoan 		  = $this->_session['PK_iMaCB'];
		$time			  = date('Y-m-d H:i:s',time());
		if(_post('luudulieu'))
		{
			
			// ten file, duong dan
			$time2 = time();
			$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$arrsTenFile = array();
				$arrsDuongDan = array();
				$this->upload('doc_ttnb_'.date('Y'), $time2);
				foreach ($name as $key => $value) {
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}						
					$tmp = str_replace('$', '-', $tmp);					
					$tmp = $tmp.'.'.$a[$c];
						
					array_push($arrsTenFile,$value);
					array_push($arrsDuongDan, 'doc_ttnb_'.date('Y').'/ttnb_'.$time2.'_'.$tmp);
				}
				
				$sTenFile = implode(';',$arrsTenFile);
				$sDuongDan = implode(';',$arrsDuongDan);
			}
			
			$data=array(
				'FK_iMaVB'  => $mavanban,
				'sNoiDung'  => nl2br(_post('noidunggopy')),
				'sThoiGian' => $time,
				'FK_iMaCB'  => $taikhoan,
				'sTenFile'  => $sTenFile,
				'sDuongDan' => $sDuongDan
				);
			if (!empty(_post('noidunggopy'))) {
				$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_gopy',$data);
				echo $kiemtra;
				if($kiemtra>0){
					redirect(base_url().'xemchitiet?id='.$mavanban);
					$data['content'] = messagebox('Thêm góp ý thành công','info');
				}
			} else {
				$data['content'] = messagebox('Chưa nhập nội dung!','danger');
			}
			
		}
		$data['dsfile']   = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$mavanban,'tbl_files_vb');
		$data['dsgopy']   = $this->Msoanthaothongtin->layGopY($mavanban);
		
			//cập nhật thời gian đọc
			$nn   			= $this->Mdanhmuc->layDuLieu('FK_iMaVB',$mavanban,'tlb_nguoinhanvanban');
			$k=0;
			foreach ($nn as $key => $value) {				
				if ($value['FK_iMaNguoiNhan'] == $taikhoan ) {
					$k = 1;
				}
			}
			if ($k == 1) {				
				$data['content']  = $this->Msoanthaothongtin->capnhatThoiGianXem($mavanban,$taikhoan,$time);
			} else {
				$datac = array(
					'FK_iMaNguoiNhan' => $taikhoan,
					'FK_iMaVB'        => $mavanban,
					'sThoiGianGui'    => date('Y-m-d H:i:s')
				);
				$this->Mdanhmuc->themDuLieu('tlb_nguoinhanvanban',$datac);
				$data['content']  = $this->Msoanthaothongtin->capnhatThoiGianXem($mavanban,$taikhoan,$time);
			}
			
		$data['dulieu']   = $this->Msoanthaothongtin->layThongTinVanBan($mavanban);
		$data['dsphong']  = $this->Msoanthaothongtin->layPB($mavanban);
		$data['dscanbo']  = $this->Msoanthaothongtin->layCB($mavanban);
		$data['mavanban'] = $mavanban;
		$data['title']    = 'Xem chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'thongtinnoibo/Vxemchitiet';
		$this->load->view('layout_admin/layout', $temp);
	}	
	
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'ttnb_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cxemchitiet.php */
/* Location: ./application/controllers/thongtinnoibo/Cxemchitiet.php */