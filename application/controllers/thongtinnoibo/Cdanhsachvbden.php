<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachvbden extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
		$this->load->library('pagination');
	}

	public function index()
	{
		$vanban               = $this->DSVanBan();
		$data['vanbanden']    = $vanban['info'];
		$data['kyhieu']       = $vanban['kyhieu'];
		$data['soden']        = $vanban['soden'];
		$data['phantrang']    = $vanban['pagination'];
		$data['title']        = 'Danh sách văn bản';
		$temp['data']         = $data;
		$temp['template']     = 'thongtinnoibo/Vdanhsachvbden';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function DSVanBan() 
	{
		$mavanbanden = $this->Msoanthaothongtin->layMaVBDenDi(1);
		$mang=array();
		foreach ($mavanbanden as $key => $value) {
			$mang[] = $value['FK_iMaVBDen'];
		}
		$soden  = _get('soden');
		$kyhieu = _get('kyhieu');
		$config['base_url']             = base_url().'danhsachvbden?soden='.$soden.'&kyhieu='.$kyhieu;
		$config['total_rows']           = $this->Msoanthaothongtin->demDSVBDen($mang,$soden,$kyhieu);
		$config['per_page']             = 50;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'danhsachvbden');
      	}
		$data['soden'] 		= $soden;
		$data['kyhieu']     = $kyhieu;
		$data['info']       = $this->Msoanthaothongtin->layDSVBDen($mang,$soden,$kyhieu,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdanhsachvbden.php */
/* Location: ./application/controllers/thongtinnoibo/Cdanhsachvbden.php */