<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmocham extends MY_Controller {

	protected $_thongtin;
	protected $_nodung;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		if($this->_session['PK_iMaCB']!=615){
			redirect("dangnhap");
		}

 /*$id = strtotime("now");//echo $id;die;
 $wsdl_url = 'http://gateway.vpmedia.vn/Gateway.asmx?WSDL';
 $client = new SOAPClient($wsdl_url);
 $params = array(
  'src' => 'VSMS',
  'dest' => '84919133688',
  'msgbody' => 'STC-HN Quyết định đ/c Nguyễn Trần Tuấn Anh giữ chức Trưởng phòng tin học tạm quyền từ ngày 18/07/2018',
  'requestid' => $id,
  'username' => 'sotaichinhhn',
  'password' => 'am7wbq27as62'
  
 );
 $result = $client->SendMT($params);
 echo "aaaaaaa: ".$result->SendMTResult;
*/

		$data['title']   = 'Mở chấm điểm';
		if($this->input->post('luudulieu')){
			$data['content'] = $this->themDuLieu();
		}
		if(_get('id') > 0){
			$id = _get('id');
			$this->Mdanhmuc->xoaDuLieu('id',$id,'tbl_mocham');
			redirect("mocham");
		}
		if(_post('xoatat'))
		{
			$this->Mdanhmuc->xoaDuLieu(NULL,NULL,'tbl_mocham');
		}
		$mangpb =array();
		$mangqh =array();
		$mangtenphong =array();
		$data['dsphongban'] = $this->Msoanthaothongtin->layPhongBan(1);
		if(!empty($data['dsphongban']))
		{
			foreach ($data['dsphongban'] as $key => $value) {
				$mangpb[]=$value['PK_iMaPB'];
				$mangtenphong[$value['PK_iMaPB']] = $value['sTenPB'];
			}
		}
		$data['dsnguoinhanpb'] = $this->Msoanthaothongtin->layNguoiNhan($mangpb);


		$year = date('Y');		
		$year = $_SESSION['nam'];

        $arr=array();
        for($i=0;$i<53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            
        }
        $tuan = date('W')-1;

        $where_mocham = " 1=1 and ketthuc >= '".date('Y-m-d H:i:s')."'";
        $data['mocham'] = $this->Mdanhmuc->get_kehoach3($where_mocham,'tbl_mocham','');

        $canbo = $this->Mdanhmuc->get_kehoach3('iTrangThai = 0','tbl_canbo');

        $arr_canbo =array();
        foreach ($canbo as $key => $ma_cb) {
            $arr_canbo[$ma_cb['PK_iMaCB']] =  $ma_cb['sHoTen'];
        }

        $data['arr_canbo']  = $arr_canbo;
        $data['mangtenphong']  = $mangtenphong;
		$data['arr']  = $arr;
		$data['tuan']  = $tuan;

		$temp['data']     = $data;
		$temp['template'] = 'thongtinnoibo/Vmocham';
		$this->load->view('layout_admin/layout', $temp);
	}
	
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$nguoinhan = _post('nguoinhan');
			$phongban  = _post('phongban');
			$tuan = _post('tuan');
			$thoigian = _post('thoigian');
			$arr_tg  =explode ('-' , $thoigian);
			$batdau = date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $arr_tg[0])));
			$ketthuc = date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $arr_tg[1])));

			if(!empty($phongban))
			{
				foreach ($phongban as $key => $value) {
					if(!empty($nguoinhan[$value]))
					{$mangthem=array();
						foreach ($nguoinhan[$value] as $k => $val) {
							$mangthem[] = 
								array(
									'tuan'      => $tuan,
									'phong'     => $value,
									'batdau'    => $batdau,
									'ketthuc'   => $ketthuc,
									'canbo'     => $val,
									'date_nhap' => date('Y-m-d H:i:s')
								);
						}
						$this->Mdanhmuc->themNhieuDuLieu('tbl_mocham',$mangthem);
					}
					else{
						$dscanbophong = $this->Msoanthaothongtin->layNguoiNhan($value);
						if(!empty($dscanbophong))
						{$mangthem2=array();
							foreach ($dscanbophong as $key => $val) {
								$mangthem2[] =
									array(
										'tuan' => $tuan,
										'phong' => $value,
										'batdau' => $batdau,
										'ketthuc' => $ketthuc,
										'canbo'  => $val['PK_iMaCB'],
										'date_nhap' => date('Y-m-d H:i:s')
									);
							}
							$this->Mdanhmuc->themNhieuDuLieu('tbl_mocham',$mangthem2);
						}

					}	
				}
			}
			
			
			
			//redirect('xemchitietguidi?id='.$id_insert);
		}
	}
	
	
}

/* End of file Csoanthaothongtin.php */
/* Location: ./application/controllers/thongtinnoibo/Csoanthaothongtin.php */