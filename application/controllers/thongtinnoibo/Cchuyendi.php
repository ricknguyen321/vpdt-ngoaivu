<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchuyendi extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		$data['title']    = 'Chuyển văn bản đi';
		
		$mavanban         = _get('id');
		
		$canbo = $this->Msoanthaothongtin->layCB($mavanban);
		$mangcb=array();
		if(!empty($canbo)){
			foreach ($canbo as $key => $value) {
				$mangcb[]=$value['PK_iMaCB'];
			}
		}
		$this->themDuLieu($mavanban);
		$data['dsfile']		= $this->Mdanhmuc->layDuLieu('FK_iMaVB',$mavanban,'tbl_files_vb');
		// $data['dsphongban'] = $this->Msoanthaothongtin->layPBChuaChon();
		// $data['dscanbo']    = $this->Msoanthaothongtin->layCBChuaChon();
		$mangpb =array();
		$mangqh =array();
		$data['dsphongban'] = $this->Msoanthaothongtin->layPhongBan(1);
		$data['dsquanhyen'] = $this->Msoanthaothongtin->layPhongBan(2);
		if(!empty($data['dsphongban']))
		{
			foreach ($data['dsphongban'] as $key => $value) {
				$mangpb[]=$value['PK_iMaPB'];
			}
		}
		if(!empty($data['dsquanhyen']))
		{
			foreach ($data['dsquanhyen'] as $key => $value) {
				$mangqh[]=$value['PK_iMaPB'];
			}
		}
		
		$data['dsnguoinhanpb'] = $this->Msoanthaothongtin->layNguoiNhan($mangpb);
		
		//$data['dsnguoinhanqh'] = $this->Msoanthaothongtin->layNguoiNhan($mangqh);
		
		$data['dulieu']     = $this->Msoanthaothongtin->layThongTinVanBan($mavanban);
		$data['thongtin']   = '';
		
		$temp['data']       = $data;
		$temp['template']   = 'thongtinnoibo/Vchuyendi';
		
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu($id_insert)
	{
		$laytenphong = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_phongban');
		if(_post('luudulieu'))
		{
			$canbodanhan = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$id_insert,'tlb_nguoinhanvanban');
			$mangnguoinhan =array();
			if(!empty($canbodanhan))
			{
				foreach ($canbodanhan as $key => $value) {
					$mangnguoinhan[] = $value['FK_iMaNguoiNhan'];
				}
			}
			$dulieu = $this->Mdanhmuc->layDuLieu('PK_iMaVB',$id_insert,'tbl_vanban');
			$noidungchidao = $dulieu[0]['sNoiDungChiDao'];
			$ykien = '<p>'.nl2br(_post('ykien')).' <br> <b>'.$this->_session['sHoTen'].'</b> - '.date('Y-m-d H:i:s',time()).'</p>';
			$data=array(
				'sNoiDungChiDao'         => $noidungchidao.$ykien,
				);
			$kiemtra = _post('ykien');
			if(!empty($kiemtra))
			{
				$this->Mdanhmuc->capnhatDuLieu('PK_iMaVB',$id_insert,'tbl_vanban',$data);
			}
			$phongban  = _post('phongban');
			$nguoinhan = _post('nguoinhan');
			$ghichu    = _post('ghichu');
			
			
			if(!empty($phongban))
			{
				foreach ($phongban as $key => $value) {
					
						$dscanbophong = $this->Msoanthaothongtin->layNguoiNhan($value);
						if(!empty($dscanbophong))
						{	
							$mangthem2=array();
							foreach ($dscanbophong as $key => $val) {
								$mangthem2[] = array(
									'FK_iMaNguoiNhan' => $val['PK_iMaCB'],
									'FK_iMaVB'        => $id_insert,
									'sThoiGianGui'    => date('Y-m-d H:i:s'),
									'sGhiChu'         => $ghichu[$value][0]
									);
							}
							$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem2);
						}
				}
			}
			
			if(!empty($nguoinhan))
					{
						$mangthem=array();
						foreach ($nguoinhan as $k => $val) {
							$kt = $this->Msoanthaothongtin->kiemtratontai($id_insert,$val);
							echo $kt;
							if ($kt < 1){
								$mangthem[] = array(
									'FK_iMaNguoiNhan' => $val,
									'FK_iMaVB'        => $id_insert,
									'sThoiGianGui'    => date('Y-m-d H:i:s')
									);
							}
						}
						$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem);
					}
			
			redirect('thongtinguidi');
		}
	}

}

/* End of file Cchuyendi.php */
/* Location: ./application/controllers/thongtinnoibo/Cchuyendi.php */