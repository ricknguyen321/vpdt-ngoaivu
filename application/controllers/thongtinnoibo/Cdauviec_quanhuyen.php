<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdauviec_quanhuyen extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->library('pagination');
		$this->load->model('thongtinnoibo/Mdauviec_quanhuyen');
	}
	public function index()
	{
		$data['title']		= 'Đầu việc quận huyện';
		$dulieu				= $this->DSVanBan();
		$data['dulieu']		= $dulieu['info'];
		$data['phantrang']	= $dulieu['pagination'];
		$temp['data']		= $data;
		$temp['template']	= 'thongtinnoibo/Vdauviec_quanhuyen1';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function DSVanBan() 
	{
		$phongban = _get('phongban');
		$config['base_url']             = base_url().'dauviec_quanhuyen?phongban='.$phongban;
		$config['total_rows']           = $this->Mdauviec_quanhuyen->demDauViec_QuanHuyen($phongban);
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'thongtinguidi');
      	}
		$data['info']       = $this->Mdauviec_quanhuyen->layDauViec_QuanHuyen($phongban,$config['per_page'], $data['page']);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}
}

/* End of file Cdauviec_quanhuyen.php */
/* Location: ./application/controllers/thongtinnoibo/Cdauviec_quanhuyen.php */