<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckiemtracongvu extends MY_Controller {

	protected $_thongtin;
	protected $_nodung;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		/*if($this->_session['PK_iMaCB']!=615){
			redirect("dangnhap");
		}*/

		$data['title']   = 'Kiểm tra công vụ';
		if($this->input->post('luudulieu')){
			$data['content'] = $this->themDuLieu();
		}
		if(_get('id') > 0){
			$id = _get('id');
			$this->Mdanhmuc->xoaDuLieu('id',$id,'tbl_kiemtracongvu');
			redirect("kiemtracongvu?tuan="._get('tuan')."&phongbankt="._get('phongbankt'));
		}
		$mangpb =array();
		$mangqh =array();
		$mangtenphong =array();
		$data['dsphongban'] = $this->Msoanthaothongtin->layPhongBan(1);
		if(!empty($data['dsphongban']))
		{
			foreach ($data['dsphongban'] as $key => $value) {
				$mangpb[]=$value['PK_iMaPB'];
				$mangtenphong[$value['PK_iMaPB']] = $value['sTenPB'];
			}
		}

		$data['dsnguoinhanpb'] = array();
		$data['phongbankt'] = 0;
		if(_get('phongbankt') >0){
			$data['dsnguoinhanpb'] = $this->Msoanthaothongtin->layNguoiNhan(_get('phongbankt'));

			$data['danhsachvipham'] = $this->Mdanhmuc->layDuLieu2('tuan',_get('tuan'),'phong',_get('phongbankt'),'tbl_kiemtracongvu');

			$data['phongbankt'] = _get('phongbankt');
		}
		


		$year = date('Y');
        $arr=array();
        for($i=0;$i<53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            
        }
        $tuan = date('W');

        $where_mocham = " 1=1 and ketthuc >= '".date('Y-m-d H:i:s')."'";
        $data['mocham'] = $this->Mdanhmuc->get_kehoach3($where_mocham,'tbl_mocham','');

        $canbo = $this->Mdanhmuc->get_kehoach3('iTrangThai = 0','tbl_canbo');

        $arr_canbo =array();
        foreach ($canbo as $key => $ma_cb) {
            $arr_canbo[$ma_cb['PK_iMaCB']] =  $ma_cb['sHoTen'];
        }

        $data['arr_canbo']  = $arr_canbo;
        $data['mangtenphong']  = $mangtenphong;
		$data['arr']  = $arr;
		$data['tuan']  = $tuan;

		$temp['data']     = $data;
		$temp['template'] = 'thongtinnoibo/Vkiemtracongvu';
		$this->load->view('layout_admin/layout', $temp);
	}
	
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$tuan = _get('tuan');
			$ngaynhap = date('Y-m-d H:i:s');
			$phong = _get('phongbankt');

			$Doc_array= array();
	        $i = 0;
	        foreach($this->input->post('macanbo') as $key => $value ){
	        	if(_post('nhomvipham_'.$value) >0 ){
	        		$Doc_array[$i]['macanbo'] = $value;
		            $Doc_array[$i]['phong'] = $phong;
		            $Doc_array[$i]['tuan'] = $tuan;
		            $Doc_array[$i]['ngaynhap'] = $ngaynhap;
		            $Doc_array[$i]['nhomvipham'] =_post('nhomvipham_'.$value);
		            $Doc_array[$i]['noidungvipham'] =_post('noidungvipham_'.$value);
		            $Doc_array[$i]['ngaykiemtra'] = date_insert(_post('ngaykiemtra_'.$value));
		            $Doc_array[$i]['canbonhap'] = $this->_session['PK_iMaCB'];
		            $i++;
	        	}    
	            
	        }
	     	$this->Mdanhmuc->themNhieuDuLieu('tbl_kiemtracongvu',$Doc_array);		
			redirect('kiemtracongvu?tuan='._get('tuan').'&phongbankt='._get('phongbankt'));
		}
	}
	
	
}

/* End of file Csoanthaothongtin.php */
/* Location: ./application/controllers/thongtinnoibo/Csoanthaothongtin.php */