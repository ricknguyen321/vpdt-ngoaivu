<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongtindaxem extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
		$this->load->library('pagination');
	}
	public function index()
	{
		$data['title']     = 'Thông tin đã xem';
		$data['thongtin']  = $this->_thongtin;
		$data['ds_phongban']= $this->Msoanthaothongtin->layPB_STC();
		$vanban            = $this->DSVanBan();
		$data['danhsach']  = $vanban['info'];
		$data['phantrang'] = $vanban['phantrang'];
		$data['phongban']  = $vanban['phongban'];
		$data['noidung']   = $vanban['noidung'];
		$data['canbo']     = $vanban['canbo'];
		$data['tungay']    = $vanban['tungay'];
		$data['denngay']   = $vanban['denngay'];
		$data['count']   = $vanban['count'];
		if(!empty($data['phongban']))
		{
			$data['ds_canbo']  = $this->Msoanthaothongtin->layCB_Phong($data['phongban']);
		}
		else{
			$data['ds_canbo']  = '';	
		}
		if(_post('markxoa'))
		{
			$ma         = _post('markxoa');
			$datac = array(
				'iDel'    => 1
			);
			$this->Mdanhmuc->capnhatDuLieu('PK_iMaNguoiNhanVB',$ma,'tlb_nguoinhanvanban',$datac);
			redirect('thongtindaxem');
		}
		$temp['data']      = $data;
		$temp['template']  = 'thongtinnoibo/Vthongtindaxem';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function DSVanBan() 
	{
		$taikhoan = $this->_session['PK_iMaCB'];
		$phongban = _get('phongban');
		$noidung  = _get('noidung');
		$canbo    = _get('canbo');
		$tungay   = _get('tungay');
		$denngay  = _get('denngay');
		if(!empty($canbo)){
			$thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$canbo,'tbl_canbo');
			$phongbanhd = $thongtin[0]['FK_iMaPhongHD'];
			if($phongban!=$phongbanhd){
				$canbo   = '';
			}
		}
		$config['base_url']             = base_url().'thongtindaxem';
		$config['total_rows']           = $this->Msoanthaothongtin->TT_DaXem($taikhoan,$phongban,$noidung,$canbo,$tungay,$denngay);
		$config['per_page']             = 30;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 9;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'thongtindaxem');
      	}
      	$data['phongban']   = $phongban;
      	$data['noidung']    = $noidung;
      	$data['canbo']      = $canbo;
      	$data['tungay']     = $tungay;
      	$data['denngay']    = $denngay;
		$data['count']    = $config['total_rows'];
		$data['info']       = $this->Msoanthaothongtin->layTT_DaXem($taikhoan,$phongban,$noidung,$canbo,$tungay,$denngay,$config['per_page'], $data['page']);
		$data['phantrang']  = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cthongtindaxem.php */
/* Location: ./application/controllers/thongtinnoibo/Cthongtindaxem.php */