<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchitietvbdendi extends MY_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		$mavb = _get('id');
		$vb   = _get('vb');
		//kiểm tra có tồn tại biến vb trên url k? nếu có là văn bản đến
		if(!empty($vb))
		{
			$data['thongtin'] = $this->Msoanthaothongtin->layChiTietVBDen($mavb);
			$data['dsfile']	  = $this->Mdanhmuc->layDuLieu('FK_iMaVBDen',$mavb,'tbl_files_vbden');
		}
		else{
			$data['thongtin'] = $this->Mvanbandi->layVBChoSo($mavb,1,1);
			$data['dsfile']	  = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$mavb,'tbl_files_vbdi');
		}
		$data['dokhan']   = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_dokhan');
		$data['phongban'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_phongban');
		if(_post('luudulieu'))
		{
			if(!empty($vb))
			{
				$this->themDuLieu($mavb,'FK_iMaVBDen',$data['dsfile']);
			}
			else{
				$this->themDuLieu($mavb,'FK_iMaVBDi',$data['dsfile']);
			}
		}
		$data['title']    = 'Xem chi tiết';
		$temp['data']     = $data;
		$temp['template'] = 'thongtinnoibo/Vchitietvbdendi';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu($mavb,$vanban,$dsfile)
	{
		$data=array(
			$vanban     	 => $mavb,
			'sTenLVB'        => _post('loaivanban'),
			'sTenPB'         => _post('phongban'),
			'sKyHieu'        => _post('kyhieu'),
			'sMoTa'          => _post('trichyeu'),
			'sNoiDungChiDao' => nl2br(_post('ykienchidao')),
			'sNgayNhap'      => date('Y-m-d H:i:s'),
			'FK_iMaCB_Nhap'  => $this->_session['PK_iMaCB'],
			'sNguoiKy'       => _post('nguoiky'),
			'sNgayKy'        => date_insert(_post('ngayky'))
			);
		$kiemtra = $this->Mdanhmuc->themDuLieu2('tbl_vanban',$data);
		if(!empty($dsfile))
		{
			foreach ($dsfile as $key => $value) {
				$data_file[]=array(
				'FK_iMaVB'  => $kiemtra,
				'sTenFile'  => $value['sTenFile'],
				'sDuongDan' => $value['sDuongDan'],
				'sThoiGian' => date('Y-m-d H:i:s'),
				'FK_iMaCB'  => $this->_session['PK_iMaCB']
				);
			}
			$this->Mdanhmuc->themNhieuDuLieu('tbl_files_vb',$data_file);
		}
		if($kiemtra>0)
		{
			redirect('chuyendi?id='.$kiemtra);
		}
	}

}

/* End of file Cchitietvbddendi.php */
/* Location: ./application/controllers/thongtinnoibo/Cchitietvbdi.php */