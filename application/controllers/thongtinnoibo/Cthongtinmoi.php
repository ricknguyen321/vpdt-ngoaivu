<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongtinmoi extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		if(_post('daxem'))
		{
			$mavanban         = _post('daxem');
			$taikhoan 		  = $this->_session['PK_iMaCB'];
			$time			  = date('Y-m-d H:i:s',time());
			$nn   			= $this->Mdanhmuc->layDuLieu('FK_iMaVB',$mavanban,'tlb_nguoinhanvanban');
			$k=0;
			foreach ($nn as $key => $value) {				
				if ($value['FK_iMaNguoiNhan'] == $taikhoan ) {
					$k = 1;
				}
			}
			if ($k == 1) {				
				$data['content']  = $this->Msoanthaothongtin->capnhatThoiGianXem($mavanban,$taikhoan,$time);
			} else {
				$datac = array(
					'FK_iMaNguoiNhan' => $taikhoan,
					'FK_iMaVB'        => $mavanban,
					'sThoiGianGui'    => date('Y-m-d H:i:s')
				);
				$this->Mdanhmuc->themDuLieu('tlb_nguoinhanvanban',$datac);
				$data['content']  = $this->Msoanthaothongtin->capnhatThoiGianXem($mavanban,$taikhoan,$time);
			}
			redirect('thongtinmoi');
		}
		$data['title']   = 'Thông tin mới';
		$data['thongtin'] = $this->_thongtin;
		$data['dsthongtin'] = $this->Msoanthaothongtin->layVanBanMoiNhan2($this->_session['PK_iMaCB'],0);
		$temp['data']     = $data;
		$temp['template'] = 'thongtinnoibo/Vthongtinmoi';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cthongtinmoi.php */
/* Location: ./application/controllers/thongtinnoibo/Cthongtinmoi.php */