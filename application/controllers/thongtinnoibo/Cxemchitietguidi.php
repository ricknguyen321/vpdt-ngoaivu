<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemchitietguidi extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		$data['title']   = 'Xem chi tiết thông tin đã gửi đi';
		$data['thongtin'] = $this->_thongtin;
		$mavanban = _get('id');
		$data['dsgopy']   = $this->Msoanthaothongtin->layGopY($mavanban);
		if(_post('xoa'))
		{
			$ma = _post('xoa');
			$this->Mdanhmuc->xoaDuLieu('PK_iMaFileVB',$ma,'tbl_files_vb');
		}
		$data['dsfile']   = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$mavanban,'tbl_files_vb');
		$data['dulieu']   = $this->Msoanthaothongtin->layThongTinVanBan($mavanban);
		$data['dsphong']  = $this->Msoanthaothongtin->layPB($mavanban);
		$data['dscanbo']  = $this->Msoanthaothongtin->layCB($mavanban);
		$data['mavanban'] = $mavanban;
		$temp['data']     = $data;
		$temp['template'] = 'thongtinnoibo/Vxemchitietguidi';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cxemchitietguidi.php */
/* Location: ./application/controllers/thongtinnoibo/Cxemchitietguidi.php */