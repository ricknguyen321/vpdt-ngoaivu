<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csoanthaothongtin extends MY_Controller {

	protected $_thongtin;
	protected $_nodung;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('thongtinnoibo/Msoanthaothongtin');
	}
	public function index()
	{
		$data['title']   = 'Soạn thảo thông tin';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
			$data['content'] = $this->xoaDuLieu();
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['content'] = $this->xoaDuLieu();
		$mangpb =array();
		$mangqh =array();
		$data['dsphongban'] = $this->Msoanthaothongtin->layPhongBan(1);
		$data['dsquanhyen'] = $this->Msoanthaothongtin->layPhongBan(2);
		if(!empty($data['dsphongban']))
		{
			foreach ($data['dsphongban'] as $key => $value) {
				$mangpb[]=$value['PK_iMaPB'];
			}
		}
		if(!empty($data['dsquanhyen']))
		{
			foreach ($data['dsquanhyen'] as $key => $value) {
				$mangqh[]=$value['PK_iMaPB'];
			}
		}
		$data['mota'] =$this->_nodung;
		if(_post('ganteptin'))
		{
			$this->themFile($ma);
			$data['mota'] = _post('noidung');
		}
		$data['dsnguoinhanpb'] = $this->Msoanthaothongtin->layNguoiNhan($mangpb);
		//$data['dsnguoinhanqh'] = $this->Msoanthaothongtin->layNguoiNhan($mangqh);
		$data['thongtin'] = $this->_thongtin;
		$mavanban         = _get('id');
		$data['dsphong']  = $this->Msoanthaothongtin->layPB($mavanban);
		$data['dscanbo']  = $this->Msoanthaothongtin->layCB($mavanban);
		if(!empty($mavanban))
		{
			$data['dsfile']   = $this->Mdanhmuc->layDuLieu('FK_iMaVB',$mavanban,'tbl_files_vb');
		}
		else{
			$data['dsfile']   = $this->Msoanthaothongtin->layfilevuatai($this->_session['PK_iMaCB']);
		}
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_dokhan','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thongtinnoibo/Vsoanthaothongtin';
		$this->load->view('layout_admin/layout', $temp);
	}
	
	public function xoaDuLieu()
	{
		if(_post('xoadulieu'))
		{
			$duongdan = _post('xoadulieu');
			$this->_nodung = _post('noidung');
			$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_vb');
			if(file_exists($duongdan))
			{
				unlink($duongdan);
			}
			if($kiemtra>0)
			{
				return messagebox('Xóa file thành công','info');
			}
		}
	}

	public function themDuLieu()
	{
		$laytenphong = $this->Mdanhmuc->layDuLieu('PK_iMaPB',$this->_session['FK_iMaPhongHD'],'tbl_phongban');
		if(_post('luudulieu'))
		{
			$data=array(
				'sMoTa'         => nl2br(_post('noidung')),
				'FK_iMaCB_Nhap' => $this->_session['PK_iMaCB'],
				'sTenPB'		=> $laytenphong[0]['sTenPB'],
				'FK_iMaPB'      => $this->_session['FK_iMaPhongHD'],
				'sNgayNhap'		=> date('Y-m-d H:i:s')
				);
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_vanban',$data);
			if(!empty($id_insert))
			{
				$this->Msoanthaothongtin->capnhatMaVB($id_insert,$this->_session['PK_iMaCB']);
			}
			$phongban  = _post('phongban');
			$nguoinhan = _post('nguoinhan');
			$ghichu    = _post('ghichu');
			// nếu tồn tại phòng ban thì kiểm tra có người nhận k, không có thì là cả phòng
			// nếu k tồn tại phòng ban thì mặc định là phòng ban giám đốc
			if(!empty($phongban))
			{
				foreach ($phongban as $key => $value) {
					
						$dscanbophong = $this->Msoanthaothongtin->layNguoiNhan($value);
						if(!empty($dscanbophong))
						{$mangthem2=array();
							foreach ($dscanbophong as $key => $val) {
								$mangthem2[] = array(
									'FK_iMaNguoiNhan' => $val['PK_iMaCB'],
									'FK_iMaVB'        => $id_insert,
									'sThoiGianGui'    => date('Y-m-d H:i:s'),
									'sGhiChu'         => $ghichu[$value][0]
									);
							}
							$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem2);
						}
				}
			}
			
			if(!empty($nguoinhan))
					{
						$mangthem=array();
						foreach ($nguoinhan as $k => $val) {
							$kt = $this->Msoanthaothongtin->kiemtratontai($id_insert,$val);
							echo $kt;
							if ($kt < 1){
								$mangthem[] = array(
									'FK_iMaNguoiNhan' => $val,
									'FK_iMaVB'        => $id_insert,
									'sThoiGianGui'    => date('Y-m-d H:i:s')
									);
							}
						}
						$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem);
					}
			// else{
				// $mangthem3=array();
				// $dscanbophong = $this->Msoanthaothongtin->layNguoiNhan(73);
				// if(!empty($dscanbophong))
				// {
				// 	foreach ($dscanbophong as $key => $val) {
				// 		$mangthem3[] = array(
				// 			'FK_iMaNguoiNhan' => $val['PK_iMaCB'],
				// 			'FK_iMaVB'        => $id_insert,
				// 			'sThoiGianGui'    => date('Y-m-d H:i:s')
				// 			);
				// 	}
				// 	$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem3);
				// }
			// }
			$this->themFile($id_insert);
			$data['mota'] = _post('noidung');
			
			redirect('xemchitietguidi?id='.$id_insert);
		}
	}
	
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaVB',$ma,'tbl_vanban');
		if(_post('luudulieu'))
		{
			$nguoinhanvanban = $this->Msoanthaothongtin->layNguoiNhanVB($ma);
			if(!empty($nguoinhanvanban))
			{
				foreach ($nguoinhanvanban as $key => $nn) {
					$mangnguoinhanvanban[$key]=$nn['FK_iMaNguoiNhan'];
				}
			}
			else{
				$mangnguoinhanvanban[0]=0;
			}
			$data=array(
				'sMoTa'         => nl2br(_post('noidung'))
				);
			$kiemtra   = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVB',$ma,'tbl_vanban',$data);
			if($kiemtra>0)
			{
				$this->Msoanthaothongtin->capnhatMaVB($ma,$this->_session['PK_iMaCB']);
			}
			$phongban  = _post('phongban');
			$nguoinhan = _post('nguoinhan');
			$ghichu    = _post('ghichu');
			// nếu tồn tại phòng ban thì kiểm tra có người nhận k, không có thì là cả phòng
			// nếu k tồn tại phòng ban thì mặc định là phòng ban giám đốc
			if(!empty($phongban))
			{
				foreach ($phongban as $key => $value) {
					if(!empty($nguoinhan[$value]))
					{$mangthem=array();
						foreach ($nguoinhan[$value] as $k => $val) {
							if (!in_array($val, $mangnguoinhanvanban))
							{
								$mangthem[] = array(
								'FK_iMaNguoiNhan' => $val,
								'FK_iMaVB'        => $ma,
								'sThoiGianGui'    => date('Y-m-d H:i:s'),
								'sGhiChu'         => $ghichu[$value][0]
								);
							}
							
						}
						if(!empty($mangthem))
						{
							$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem);
						}
					}
					else{
						$dscanbophong = $this->Msoanthaothongtin->layNguoiNhan($value);
						if(!empty($dscanbophong))
						{$mangthem2=array();
							foreach ($dscanbophong as $key => $val) {
								if (!in_array($val['PK_iMaCB'], $mangnguoinhanvanban))
								{
									$mangthem2[] = array(
										'FK_iMaNguoiNhan' => $val['PK_iMaCB'],
										'FK_iMaVB'        => $ma,
										'sThoiGianGui'    => date('Y-m-d H:i:s'),
										'sGhiChu'         => $ghichu[$value][0]
										);
								}
							}
							if(!empty($mangthem2))
							{
								$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem2);
							}
						}

					}	
				}
			}
			// else{
			// 	$mangthem3=array();
			// 	$dscanbophong = $this->Msoanthaothongtin->layNguoiNhan(73);
			// 	if(!empty($dscanbophong))
			// 	{
			// 		foreach ($dscanbophong as $key => $val) {
			// 			$mangthem3[] = array(
			// 				'FK_iMaNguoiNhan' => $val['PK_iMaCB'],
			// 				'FK_iMaVB'        => $ma,
			// 				'sThoiGianGui'    => date('Y-m-d H:i:s')
			// 				);
			// 		}
			// 		$this->Mdanhmuc->themNhieuDuLieu('tlb_nguoinhanvanban',$mangthem3);
			// 	}
			// }
		redirect('xemchitietguidi?id='.$ma);
		}
	}
	
	public function themFile($ma)
	{
		$time = time();
		$name = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$this->upload('doc_ttnb_'.date('Y'), $time);
				$files = array();
				foreach ($name as $key => $value) {
					
					
					$val = clear($value);
					$a = explode('.', $val);
					$c = count($a) - 1;
					$tmp = '';
					for ($i = 0; $i < $c; $i++){
						if ($i ==0) {
							$tmp = $tmp.$a[$i];
						} else {
							$tmp = $tmp.'-'.$a[$i];
						}
					}						
					$tmp = str_replace('$', '-', $tmp);					
					$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaVB'  => ($ma)?$ma:0,
						'sTenFile'  => $value,
						'sDuongDan' => 'doc_ttnb_'.date('Y').'/ttnb_'.$time.'_'.$tmp,
						'sThoiGian' => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'  => $this->_session['PK_iMaCB']
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_vb',$files);
			}
	}
	
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'ttnb_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Csoanthaothongtin.php */
/* Location: ./application/controllers/thongtinnoibo/Csoanthaothongtin.php */