<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cwelcomead extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
        $data['title']       = 'Chào mừng '.$this->_session['sHoTen'];
		$temp['data']        = $data;
		$temp['template']    = 'Vwelcomead';
		$this->load->view('layout_admin/layout', $temp);
	}

}

/* End of file Cwelcomead.php */
/* Location: ./application/controllers/Cwelcomead.php */