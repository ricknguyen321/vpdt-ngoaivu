<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clanhdaonhanxetthang extends MY_Controller {
    protected $_giatri;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
         $this->Mkehoachcongtac = new Mkehoachcongtac();        
    }
    public function index()
    {
        if(_post('action') == 'getmaphong'){
            $this->getmaphong();
        }
        if(_post('thongke') == 'thongkebaocaothang'){
            $thangbd = _post('thangbd');
            $thangkt = _post('thangkt');
            $phong = _post('phongban');
            $canbo = _post('canbo');
            $loai = _post('danhgia');
            $where =' 1=1 ';
            $where_ld =' 1=1 ';
            if($thangbd > 0){
                $where .= " and  thang = ".$thangbd;
                $where_get ="?thang=".$thangbd;
            }
            
            if($phong > 0){
                $where .= " and  id_phong = ".$phong;
                $where_get .="&phong=".$phong;
                $data['danhsachcbphong'] = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 2 and FK_iMaPhongHD='.$phong,'tbl_canbo');

            }else{
                $data['danhsachcbphong'] = array();
            }
            if($canbo > 0){
                $where .= " and  id_cb = ".$canbo;
                $where_get .="&canbo=".$canbo;
            }
            $quyen = $this->_session['iQuyenHan_DHNB'];
            if($quyen==4){
                $where .= " and  acitve >= 3";
                $where_get .=" and  acitve >= 3";
            } 
            $where .=" and id_cb != 626 ";
            $data['kehoach']    = $this->Mdanhmuc->get_kehoach3($where,'danhgiathang','id_phong,id_cb,thang');

           

           
            $data['thangbd']     = $thangbd;
            $data['phong']      = $phong;
            $data['canbo']      = $canbo;
            $data['loai']       = $loai;
            $data['where']       = $where_get;
        }else{
            $where_get = "";$where_get1 = " 1 = 1 ";
            if(_get('thang')>0){
                $data['thangbd']     = _get('thang');
                $where_get .="?thang="._get('thang');
                $where_get1 .= " and  thang = "._get('thang');
            }else{
                $data['thangbd']     = date('m')-1;
                $thang_get = date('m')-1;
                $where_get .="?thang=".$thang_get;
                $where_get1 .= " and  thang = ".$thang_get;
            } 

            if(_get('phong')>0){
                $data['phong']      = _get('phong');
                $phong_get  = _get('phong');
                $where_get .="&phong=".$phong_get;
                $where_get1 .= " and  id_phong = ".$phong_get;
            }else{
                $data['phong']     = 0;
                //$where_get .=" and id_phong = 0 ";
            } 

            if(_get('canbo')>0){
                $data['canbo']     = _get('canbo');
                $canbo_get  = _get('canbo');
                $where_get .="&canbo=".$canbo_get;
                $where_get1 .= " and  id_cb = ".$canbo_get;
            }else{
                $data['canbo']     = 0;
                //$where_get .=" and id_cb = 0 ";
            } 
            $quyen = $this->_session['iQuyenHan_DHNB'];
            if($quyen==4){
                $where .= " and  acitve >= 3";
                $where_get .=" and  acitve >= 3";
                $where_get1 .= " and  acitve >= 3";
            } 

            $where_get1 .=" and id_cb != 626 ";
            $data['where']       = $where_get;
            $data['kehoach']    = $this->Mdanhmuc->get_kehoach3($where_get1,'danhgiathang','id_phong,id_cb,thang');
        }

        $thang_ld = $data['thangbd'];
        $where_ld =" thang = ".$thang_ld." AND (FK_iMaCB_CV = 519 or FK_iMaCB_CV = 673) ";
        $data['danhgia_ld']    = $this->Mdanhmuc->get_kehoach3($where_ld,'nhanxet_phophong','PK_iMaNX');
        $where_gd =" thang = ".$thang_ld." AND (canbo_id = 519 or canbo_id = 673)";
        $data['danhgia_gd']    = $this->Mdanhmuc->get_kehoach3($where_gd,'danhgialanhdao','dg_id')[0];

    
        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('(PK_iMaPB < 22 or PK_iMaPB = 68) and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        $year = date('Y');

        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 2 ','tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
                $data['tencanbo'][$value['PK_iMaCB']]=$value['sHoTen'];
        }  
// chánh văn phòng tham mưu dánh giá của ban giám đốc
        if(_post('danhgiabgd') == 'danhgiabangiamdoc'){
            if($quyen==3) $active =3;
            if($quyen==4) $active =5;
            $dg_id = $this->input->post('danhgia_id');
            $BGD_array = array(
                    'nhanxet_bgd' => $this->input->post('nhanxet_bgd'), 
                    'danhgia_bgd' => $this->input->post('danhgia_bgd'), 
                    'danhgia_bgd_date' => date("Y-m-d H:i:s"),
                    'active'       => $active
                );
            $this->Mdanhmuc->capnhatDuLieu('dg_id',$dg_id,'danhgialanhdao',$BGD_array);
        }

// giám đốc đánh giá duyệt cán bộ chuyên viên trong toàn sở:
        if($this->input->post('duyetkequathang')){
            $quyen = $this->_session['iQuyenHan_DHNB'];
            if($quyen==7) $acitve =2;
            if($quyen==3) $acitve =3;
            if($quyen==4) $acitve =5;
            $arrayDuyet = array();
            foreach ($this->input->post('duyet') as $key5 => $value5){
                if(!empty($key5)){
                    $danhgiathang         = $this->input->post('danhgiaGD')[$key5];
                    $nhanxet              = $this->input->post('nhanxetGD')[$key5];
                    $key_id               = $this->input->post('key_id')[$key5];
                }
                $DG_array = array(
                    'danhgia_gd' => $danhgiathang, 
                    'nhanxet_gd' => $nhanxet,
                    'date_giamdoc' => date("Y-m-d H:i:s"),
                    'acitve'       => $acitve
                );

                $this->Mdanhmuc->capnhatDuLieu('id',$key_id,'danhgiathang',$DG_array);
            }
            $where=$this->input->post('where');
            redirect("lanhdaonhanxetthang".$where);
        }
        
// xác định ngày tháng kết thúc việc chỉnh sửa 
        $check = (int) date('m') - 2;
        if($data['thangbd']<$check){
            $data['kiemtra'] = 0;
        }else{$data['kiemtra'] = 1;}

        $data['quyen']    = $this->_session['iQuyenHan_DHNB'];
        $data['title']    = 'Giám đốc sở đánh giá';
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=ThongKeBaoCaoThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=ThongKeBaoCaoThang_".date('d-m-Y').".xls"); 
            }
            //$temp['template'] = 'kehoachcongtac/Vtongkebaocao_thang';
            //$this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vlanhdaonhanxetthang1';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
    }    
    public function getmaphong()
    {
        $phong_id = _post('maphong');
        $datacanbo = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB != 4 and FK_iMaPhongHD='.$phong_id,'tbl_canbo');
        echo json_encode($datacanbo); exit();
    }
}

/* End of file Cthongkebaocaothang.php */
