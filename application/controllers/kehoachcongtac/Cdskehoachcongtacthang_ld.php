<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdskehoachcongtacthang_ld extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{    
        if(date('d')<15)$month = date('m')-1;
        else $month = date('m');
        if(isset($_POST['month']))$month = $_POST['month'];
        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);
        $week = (int)date("W", strtotime(date('Y-m-d')));
        if($week < $end) $end = $week;

        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3(' iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        
        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB >= 3 and iQuyenHan_DHNB <=6 and PK_iMaCB != 617','tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            if($value['iQuyenHan_DHNB']==3 or $value['iQuyenHan_DHNB']==6){
                $chuyenvien[$value['PK_iMaCB']]=$value['sHoTen'];
                $phong[$value['PK_iMaCB']]= $value['FK_iMaPhongHD'];
            }else{
                if($value['iQuyenHan_DHNB']==4 || $value['iQuyenHan_DHNB']==5)$lanhdao[$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        $year = date('Y');
        
        //lay tổng gia tri cac cong viec cua lãnh đạo sở
        $key_cb=0;
        foreach ($lanhdao as $id_cb => $tencb) {
            $count_arr[$key_cb][0] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb);
            $count_arr[$key_cb][1] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5");
            $count_arr[$key_cb][2] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[$key_cb][3] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cb][4] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and chatluong =1 ");
            $count_arr[$key_cb][5] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and chatluong =2 ");
            $count_arr[$key_cb][6] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and sangtao =2 ");
            $count_arr[$key_cb][7] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$end.") and lanhdao_so = ".$id_cb." and active < 5 ");
            $count_arr[$key_cb][8] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$end.") and lanhdao_so = ".$id_cb." and active < 5 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[$key_cb][9] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$end.") and lanhdao_so = ".$id_cb." and active < 5 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");

            $check = $count_arr[$key_cb][0]['tong'] - $count_arr[$key_cb][8]['tong'];
            if($check >0){
                $count_arr[$key_cb][10] = round($count_arr[$key_cb][1]['tong']/$check*100,1);
            }else{
                $count_arr[$key_cb][10] ='0';
            }
            $count_arr[$key_cb][11] = $tencb;
            $count_arr[$key_cb][12] = $id_cb;
            $count_arr[$key_cb][13] = $this->Mdanhmuc->countTP_thang('diem_tong','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][14] = $this->Mdanhmuc->countTP_thang('danhgia_ld','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $key_cb++;
        }
        $key_cv=11;
        foreach ($chuyenvien as $id_cb => $tencb) {
            /*$count_arr[$key_cv][0] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]);*/
            $count_arr[$key_cv][1] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5");
            $count_arr[$key_cv][2] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01'  or ngay_han < '2017-01-01' or ngay_han IS NULL or  ngay_hoanthanh IS NULL )");
            $count_arr[$key_cv][3] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cv][4] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =1 ");
            $count_arr[$key_cv][5] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =2 ");
            $count_arr[$key_cv][6] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and sangtao =2 ");
            $count_arr[$key_cv][7] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$end.") and phong_id = ".$phong[$id_cb]." and active < 5 ");
            $count_arr[$key_cv][8] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$end.") and phong_id = ".$phong[$id_cb]." and active < 5 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[$key_cv][9] = $this->Mdanhmuc->countLD_thang("kh_id","(thuc_hien = 1) AND tuan in (".$end.") and phong_id = ".$phong[$id_cb]." and active < 5 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");
            $count_arr[$key_cv][0]['tong'] = $count_arr[$key_cv][1]['tong'] + $count_arr[$key_cv][7]['tong'];
            $check = $count_arr[$key_cv][0]['tong'] - $count_arr[$key_cv][8]['tong'];
            if($check >0){
                $count_arr[$key_cv][10] = round($count_arr[$key_cv][1]['tong']/$check*100,1);
            }else{
                $count_arr[$key_cv][10] ='0';
            }
            $count_arr[$key_cv][11] = $tencb;
            $count_arr[$key_cv][12] = $id_cb;
            $count_arr[$key_cv][13] = $this->Mdanhmuc->countTP_thang('diem_tong_tp','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][14] = $this->Mdanhmuc->countTP_thang('danhgia_ld','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][15] = $phong[$id_cb];
            $key_cv++;
        }
        unset($count_arr[11]);       

        $data['title']    = 'Đánh giá kế hoạch công tác tháng';
        $data['phong_id'] = $this->_session['FK_iMaPhongHD'];
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['count_arr']   = $count_arr;
        //$data['count_arr1']   = $count_arr1;
        $data['month'] = $month;
        $data['list_user'] = $list_user;
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/VdskehoachcongtacthangLD_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtacthang_ld1';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}
   
}

/* End of file Cdskehoachcongtac.php */
/* Location: ./application/controllers/vanban/Cdskehoachcongtac.php */