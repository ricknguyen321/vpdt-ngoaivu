<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemkehoachcongtacquy extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->danhgiaquy();
        }   

        //$tuan = $this->uri->segment(2);
        $month = date('m');
        if($month == 1 || $month == 2 || $month == 3)$quy = 1;
        if($month == 4 || $month == 5 || $month == 6)$quy = 2;
        if($month == 7 || $month == 8 || $month == 9)$quy = 3;
        if($month == 10 || $month == 11 || $month == 12)$quy = 4;

        if(isset($_POST['quy']))$quy = $_POST['quy'];
        $tempt = $this->uri->segment(3);
        if(isset($tempt) && $tempt >0 ) $quy = $tempt;
        $year = date('Y');

        if($quy==1){
            $month1 = 1;$month2 = 3;$arr_thang = array(1,2,3);

            $beg0 = (int) date('W',strtotime("first thursday of $year-$arr_thang[0]"));
            $end0 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[0]"));
            $tuan0 = (join(', ', range($beg0, $end0)));
            $arr_tuan[1] = explode(',',$tuan0);

            $beg1 = (int) date('W',strtotime("first thursday of $year-$arr_thang[1]"));
            $end1 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[1]"));
            $tuan1 = (join(', ', range($beg1, $end1)));
            $arr_tuan[2] = explode(',',$tuan1);

            $beg2 = (int) date('W',strtotime("first thursday of $year-$arr_thang[2]"));
            $end2 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[2]"));
            $tuan2 = (join(', ', range($beg2, $end2)));
            $arr_tuan[3] = explode(',',$tuan2);
             
        }else if($quy==2){
            $month1 = 4;$month2 = 6;$arr_thang = array(4,5,6);

            $beg0 = (int) date('W',strtotime("first thursday of $year-$arr_thang[0]"));
            $end0 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[0]"));
            $tuan0 = (join(', ', range($beg0, $end0)));
            $arr_tuan[4] = explode(',',$tuan0);

            $beg1 = (int) date('W',strtotime("first thursday of $year-$arr_thang[1]"));
            $end1 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[1]"));
            $tuan1 = (join(', ', range($beg1, $end1)));
            $arr_tuan[5] = explode(',',$tuan1);

            $beg2 = (int) date('W',strtotime("first thursday of $year-$arr_thang[2]"));
            $end2 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[2]"));
            $tuan2 = (join(', ', range($beg2, $end2)));
            $arr_tuan[6] = explode(',',$tuan2);

        }else if($quy==3){
            $month1 = 7;$month2 = 9;$arr_thang = array(7,8,9);

            $beg0 = (int) date('W',strtotime("first thursday of $year-$arr_thang[0]"));
            $end0 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[0]"));
            $tuan0 = (join(', ', range($beg0, $end0)));
            $arr_tuan[7] = explode(',',$tuan0);

            $beg1 = (int) date('W',strtotime("first thursday of $year-$arr_thang[1]"));
            $end1 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[1]"));
            $tuan1 = (join(', ', range($beg1, $end1)));
            $arr_tuan[8] = explode(',',$tuan1);

            $beg2 = (int) date('W',strtotime("first thursday of $year-$arr_thang[2]"));
            $end2 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[2]"));
            $tuan2 = (join(', ', range($beg2, $end2)));
            $arr_tuan[9] = explode(',',$tuan2);

        }else{
            $month1 = 10;$month2 = 12;$arr_thang = array(10,11,12);

            $beg0 = (int) date('W',strtotime("first thursday of $year-$arr_thang[0]"));
            $end0 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[0]"));
            $tuan0 = (join(', ', range($beg0, $end0)));
            $arr_tuan[10] = explode(',',$tuan0);

            $beg1 = (int) date('W',strtotime("first thursday of $year-$arr_thang[1]"));
            $end1 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[1]"));
            $tuan1 = (join(', ', range($beg1, $end1)));
            $arr_tuan[11] = explode(',',$tuan1);

            $beg2 = (int) date('W',strtotime("first thursday of $year-$arr_thang[2]"));
            $end2 = (int) date('W',strtotime("last  thursday of $year-$arr_thang[2]"));
            $tuan2 = (join(', ', range($beg2, $end2)));
            $arr_tuan[12] = explode(',',$tuan2);
        }
        $beg = (int) date('W', strtotime("first thursday of $year-$month1"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month2"));

        $week = (int)date("W", strtotime(date('Y-m-d')));
        if($week < $end) $end = $week;

        $tuan = (join(', ', range($beg, $end)));//pr($tuan);
//lay ma can bo
        $check = $this->uri->segment(2);
        if(!empty($check)){
            $canbo_id=$check;
        }else{
            $canbo_id=$this->_session['PK_iMaCB'];
        } 
//gan gia tri sang view
        $arrData=array();
        $arrData = $this->Mdanhmuc->layDuLieu2('cb_id',$canbo_id,'quy',$quy,'danhgiaquy');            
        if(!empty($arrData)){
            $data['giatri']=$arrData[0];
        }

        $count_arr = array();
        if($quy > 0){
            for($i=0;$i<count($arr_thang);$i++){
                $list_data[$i][0] = $this->Mdanhmuc->count_kehoach(" tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id." and kh_id_sub is NULL");
                $list_data[$i][1] = $this->Mdanhmuc->count_kehoach(" tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id." and active = 3");
                $list_data[$i][2] = $this->Mdanhmuc->count_kehoach(" tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id." and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
                $list_data[$i][3] = $this->Mdanhmuc->count_kehoach(" tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id." and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
                $list_data[$i][4] = $this->Mdanhmuc->count_kehoach(" tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id." and active = 3 and chatluong =1 ");
                $list_data[$i][5] = $this->Mdanhmuc->count_kehoach(" tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id." and active = 3 and chatluong =2 ");
                $list_data[$i][6] = $this->Mdanhmuc->count_kehoach(" tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id." and active = 3 and sangtao =2 ");
                $list_data[$i][7] = $this->Mdanhmuc->count_kehoach(" tuan in(".$end.") and canbo_id=".$canbo_id." and active < 3 ");
                $list_data[$i][8] = $this->Mdanhmuc->count_kehoach(" tuan in(".$end.") and canbo_id=".$canbo_id." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
                $list_data[$i][9] = $this->Mdanhmuc->count_kehoach(" tuan in(".$end.") and canbo_id=".$canbo_id." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");
                $check = $list_data[$i][0] - $list_data[$i][8];
                if($check >0){
                    $list_data[$i][10] = round($list_data[$i][1]/$check*100,1);
                }else{
                    $list_data[$i][10] ='0';
                } 
            }
            //pr($list_data);
            // ý thức tổ chức kỷ luật
            $trungbinhthang1=0;
            $trungbinhthang2=0;
            $trungbinhthang3=0;
            $trungbinhthang4=0;
            for($i=0;$i<count($arr_thang);$i++){
                $kyluat[$i][0] = $this->Mdanhmuc->count_diem("diem_1_tp"," tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id);
                $trungbinhthang1 = (float)$trungbinhthang1 + (float)$kyluat[$i][0]['diem_1_tp'];

                $kyluat[$i][1] = $this->Mdanhmuc->count_diem("diem_2_tp"," tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id);
                $trungbinhthang2 = (float)$trungbinhthang2 + (float)$kyluat[$i][1]['diem_2_tp'];

                $kyluat[$i][2] = $this->Mdanhmuc->count_diem("diem_10_tp"," tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id);
                $trungbinhthang3 = (float)$trungbinhthang3 + (float)$kyluat[$i][2]['diem_10_tp'];

                $kyluat[$i][3] = $this->Mdanhmuc->count_diem("diem_19_tp"," tuan in(".implode(',',$arr_tuan[$arr_thang[$i]]).") and canbo_id=".$canbo_id);
                $trungbinhthang4 = (float)$trungbinhthang4 + (float)$kyluat[$i][3]['diem_19_tp'];
            }
            $trungbinhthang1 = round($trungbinhthang1/count($arr_thang),1);
            $trungbinhthang2 = round($trungbinhthang2/count($arr_thang),1);
            $trungbinhthang3 = round($trungbinhthang3/count($arr_thang),1);
            $trungbinhthang4 = round($trungbinhthang4/count($arr_thang),1);

            $count_arr[0] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and kh_id_sub IS NULL");
            $count_arr[1] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 3");
            $count_arr[2] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[3] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[4] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 3 and chatluong =1 ");
            $count_arr[5] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 3 and chatluong =2 ");
            $count_arr[6] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 3 and sangtao =2 ");
            $count_arr[7] = $this->Mdanhmuc->count_kehoach(" tuan in(".$end.") and canbo_id=".$canbo_id." and active < 3 ");
            $count_arr[8] = $this->Mdanhmuc->count_kehoach(" tuan in(".$end.") and canbo_id=".$canbo_id." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[9] = $this->Mdanhmuc->count_kehoach(" tuan in(".$end.") and canbo_id=".$canbo_id." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");
            $check = $count_arr[0] - $count_arr[8];
            if($check >0){
                $count_arr[10] = round($count_arr[1]/$check*100,1);
            }else{
                $count_arr[10] ='0';
            }
            
        }
        $phong_id = $this->Mdanhmuc->get_phong($canbo_id);//pr($phong_id);

        $list_user = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',$phong_id['FK_iMaPhongHD'],'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
        }
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }

       
        $data['title']    = 'Xem kế hoạch công tác quý';
        $data['count_arr']   = $count_arr;
        $data['arr_thang']   = $arr_thang;
        $data['quy'] = $quy;//pr($quy);
        $data['list_data']   = $list_data;
        $data['kyluat']   = $kyluat;
        //$data['list_diem']   = $list_diem;
        $data['trungbinhthang1']   = $trungbinhthang1;
        $data['trungbinhthang2']   = $trungbinhthang2;
        $data['trungbinhthang3']   = $trungbinhthang3;
        $data['trungbinhthang4']   = $trungbinhthang4;
        $data['tongdiemthang'] = $trungbinhthang4+$trungbinhthang3+$trungbinhthang2+$trungbinhthang1;
        $data['month'] = $month;
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['PK_iMaCB'] = $this->_session['PK_iMaCB'];
        $data['phong_id'] = $phong_id['FK_iMaPhongHD'];
        $data['cb_id'] = $canbo_id;
        $temp['data']     = $data;//pr($data);

        if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vxemkehoachcongtacquy_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vxemkehoachcongtacquy';
            $this->load->view('layout_admin/layout',$temp);
            /*$temp['template'] = 'kehoachcongtac/VkehoachcongtacThang_word';
            $this->load->view('layout_admin/layout_word',$temp);*/
        }
        
	}

    // cá nhân, lãnh đạo đánh giá tháng
    public function danhgiaquy()
    {
        if ($this->input->post('luudulieu')) {
            if(_post('quyenhan') < 7){
                $danhgia = array(
                    'cb_id' => _post('canbo_id'),
                    'phong_id' => $this->_session['FK_iMaPhongHD'],
                    'diem' => _post('diem'),
                    'danhgiaquy_ld' => _post('danhgiaquy_ld'),
                    'lanhdao_nhanxet' => _post('lanhdao_nhanxet'),
                    'quy' => _post('quy'),
                    'date_danhgia' => date('Y-m-d H:i:s'),
                    'active' => 2,
                    'lanhdao_id' =>   $this->_session['PK_iMaCB'] 
                );
            }else{
                $danhgia = array(
                    'cb_id' => _post('canbo_id'),
                    'phong_id' => $this->_session['FK_iMaPhongHD'],
                    'diem' => _post('diem'),
                    'danhgiaquy' => _post('danhgiaquy'),
                    'quy' => _post('quy'),
                    'date_nhap'=> date('Y-m-d H:i:s'),
                    'active' => 1,
                    'lanhdao_id' =>   $this->_session['PK_iMaCB'] 
                );
            }
            
            
            //kiem tra du lieu ton tai chua
            $arrData = $this->Mdanhmuc->layDuLieu2('cb_id',_post('canbo_id'),'quy',_post('quy'),'danhgiaquy');            
            if(empty($arrData)){
                $check_insert = $this->Mdanhmuc->themDuLieu('danhgiaquy',$danhgia);
                if(_post('quyenhan') == 4 || _post('quyenhan') == 5){
                	redirect("dskehoachcongtacquytp");	
                }else{
                	redirect("dskehoachcongtacquy/"._post('canbo_id')."/"._post('quy'));
                }
            }else{
                $check_insert = $this->Mdanhmuc->capnhatDuLieu('quy_id',$arrData[0]['quy_id'],'danhgiaquy',$danhgia);
                if(_post('quyenhan') == 4 || _post('quyenhan') == 5){
                	redirect("dskehoachcongtacquytp");	
                }else{
                	redirect("dskehoachcongtacquy/"._post('canbo_id')."/"._post('quy'));
                }
            }
            
            if ($check_insert > 0) {
                $this->_giatri ='';
                return messagebox('Thêm thành công ', 'info');
            } else {
                return messagebox('Thêm thất bại', 'danger');
            } 
            
        }
    }
 
}
    