<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csuakhctphong extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
       $id = $this->uri->segment(2);
       $id_del = $this->uri->segment(3);
       if($id_del > 0){
            $sl = $this->Mdanhmuc->battrungDuLieu('id_sub',$id,'tbl_kehoachphong');
            if($sl>0){
                $this->Mdanhmuc->xoaDuLieu('id_sub',$id,'tbl_kehoachphong');
            }
            $this->Mdanhmuc->xoaDuLieu('id',$id,'tbl_kehoachphong');
             redirect("viewkhctphong");
       }
        //kiểm tra dữu liệu đã tồn tại chưa
        $check_data = $this->Mdanhmuc->layDuLieu('id',$id,'tbl_kehoachphong');
        if(isset($check_data[0]['id'])) $this->_giatri = $check_data[0];

        $list_lanhdao = $this->Mdanhmuc->layDuLieu3('iQuyenHan_DHNB',4,'iQuyenHan_DHNB',5,'tbl_canbo');
       
        $list_user = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'tbl_canbo');
       
        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->suaKeHoach();
            redirect("viewkhctphong");
        }	
      
        $data['giatri']   = $this->_giatri;    
        $data['title']    = 'Sửa kế hoạch công tác tháng';
        $data['lanhdaoso']    = $list_lanhdao;
        $data['canbophong']    = $list_user;
        $temp['data']     = $data;
       
        $temp['template'] = 'kehoachcongtac/Vsuakhctphong';
        $this->load->view('layout_admin/layout',$temp);
	}
    // sửa kế hoạch công tác tháng
    public function suaKeHoach()
    {
        if ($this->input->post('luudulieu')) {
            $id = _post('id');
            $thang=_post('thang');
            $data_kehoachthang = array(
                'id_sub' => _post('id_sub'),
                'department_id' => _post('department_id'),
                'thang' => $thang,
                'noidung' => _post('noidung'),
                'ld_so' => _post('ld_so'),
                'ld_phong' => implode(",",_post('ld_phong')),
                'user_id' => implode(",",_post('user_id')),
                'date_start' => date('Y-m-d',strtotime(str_replace('/', '-', _post('date_start')))),
                'date_end' => date('Y-m-d',strtotime(str_replace('/', '-', _post('date_end')))),
                'date_nhap' => date('Y-m-d H:i:s')        
            );
           // pr($data_kehoachthang);
           //thêm mới dữ liệu
            $id_sub = $this->Mdanhmuc->capnhatDuLieu('id',$id,'tbl_kehoachphong',$data_kehoachthang);
            
            
            if ($id_sub > 0) {
                return messagebox('Sửa thành công kế hoạch công tác tháng ' .$thang, 'info');
            } else {
                return messagebox('Sửa kế hoạch công tác tuần thất bại', 'danger');
            } 
            
            
        }
    }

}

/* End of file Ckehoachcongtac.php */
/* Location: ./application/controllers/vanban/Ckehoachcongtac.php */