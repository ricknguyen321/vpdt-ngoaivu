<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongkebaocao_thang extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        if(_post('action') == 'getmaphong'){
            $this->getmaphong();
        }
        if(_post('thongke') == 'thongkebaocaothang'){
            $thangbd = _post('thangbd');
            $thangkt = _post('thangkt');
            $phong = _post('phongban');
            $canbo = _post('canbo');
            $loai = _post('danhgia');
            $where =' 1=1 ';
            $where_ld =' 1=1 and (canbo_id!=519  or canbo_id != 673)';
            $where_gd = '1=1 and (canbo_id = 519  or canbo_id = 673)';
            if($thangbd > 0){
                $where .= " and  thang >= ".$thangbd;
                $where_ld .= " and  thang >= ".$thangbd;
                $where_gd .= " and  thang >= ".$thangbd;
            }
            if($thangkt > 0){
                $where .= " and  thang <= ".$thangkt;
                $where_ld .= " and  thang <= ".$thangkt;
                $where_gd .= " and  thang <= ".$thangkt;
            }
            if($phong > 0){
                $where .= " and  id_phong = ".$phong;
                $where_ld .= " and phong_id = ".$phong;
                $where_gd .= " and phong_id = ".$phong;

                $data['danhsachcbphong'] = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 2 and FK_iMaPhongHD='.$phong,'tbl_canbo','iQuyenHan_DHNB');

            }else{
                $data['danhsachcbphong'] = array();
            }
            if($canbo > 0){
                $where .= " and  id_cb = ".$canbo;
                $where_ld .= " and  canbo_id = ".$canbo;
                $where_gd .= " and  canbo_id = ".$canbo;
            }
            if($loai == 1){
                $where .= " and danhgia_gd = 6 ";
                $where_ld .= " and  danhgia_lds = 6";
                $where_gd .= " and  danhgia_lds = 6";
            }
            if($loai == 2){
                $where .= " and danhgia_gd = 5 ";
                $where_ld .= " and  danhgia_lds = 5";
                $where_gd .= " and  danhgia_lds = 5";
            }
            if($loai == 3){
                 $where .= " and danhgia_gd = 4 ";
                 $where_ld .= " and  danhgia_lds = 4";
                 $where_gd .= " and  danhgia_lds = 4";
            }
            if($loai == 4){
                $where .= " and danhgia_gd = 8 ";
                $where_ld .= " and  danhgia_lds = 8";
                $where_gd .= " and  danhgia_lds = 8";
            }
            if($loai == 5){
                 $where .= " and danhgia_gd = 2 ";
                 $where_ld .= " and  danhgia_lds = 2";
                 $where_gd .= " and  danhgia_lds = 2";
            }

            $data['kehoach']    = $this->Mdanhmuc->get_kehoach3($where,'danhgiathang','id_phong,id_cb,thang');

            $data['kehoach_ld']    = $this->Mdanhmuc->get_kehoach3($where_ld,'danhgialanhdao','phong_id,canbo_id,thang');

            $data['kehoach_gd']    = $this->Mdanhmuc->get_kehoach3($where_gd,'danhgialanhdao','phong_id,canbo_id,thang');

           
            $data['thangbd']     = $thangbd;
            $data['thangkt']     = $thangkt;
            $data['phong']      = $phong;
            $data['canbo']      = $canbo;
            $data['loai']       = $loai;
        }else{
            $data['kehoach'] = array();
            $data['kehoach_ld']  = array();
            $data['thangbd']     = 0;
            $data['thangkt']     = 0;
            $data['phong']      = 0;
            $data['canbo']      = 0;
            $data['loai']       = 0;
        }
    
        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('(PK_iMaPB < 22 or PK_iMaPB = 68) and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        $year = date('Y');

        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 2 ','tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
                $data['tencanbo'][$value['PK_iMaCB']]=$value['sHoTen'];
        }  
        

        $data['title']    = 'Thống kê báo cáo tháng';
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=ThongKeBaoCaoThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=ThongKeBaoCaoThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vthongkebaocao_thang_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vthongkebaocao_thang';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}    
    public function getmaphong()
    {
        $phong_id = _post('maphong');
        $datacanbo = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB != 4 and FK_iMaPhongHD='.$phong_id,'tbl_canbo');
        echo json_encode($datacanbo); exit();
    }
}

/* End of file Cthongkebaocaothang.php */
