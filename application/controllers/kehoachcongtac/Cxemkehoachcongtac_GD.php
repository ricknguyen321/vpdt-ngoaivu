<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemkehoachcongtac_GD extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}

    public function getStartAndEndDate($week, $year) {
      $dateTime = new DateTime();
      $dateTime->setISODate($year, $week);
      $dateTime->modify('+6 days');
      $result = $dateTime->format('Y-m-d');
      return $result;
    }
	public function index()
	{
        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->chamdiem();
        }   
        $id_cb = $this->_session['PK_iMaCB'];
        $tam = $this->uri->segment(2);
        if(isset($tam) && $tam >0 ) $id_cb = $tam;
		
		//$tuan_hientai = (int)date("W", strtotime(date('Y-m-d')));
        $month = date('m');
        
        $tempt = $this->uri->segment(3);
        if(isset($tempt) && $tempt >0 ) $month = $tempt;

        if(isset($_POST['month'])){$month = $_POST['month'];}

        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);//echo $id_cb;
        $count_arr = array();
		
		/*if($tuan_hientai < end($arr_tuan))$tuan_cuoi = $tuan_hientai;
        else $tuan_cuoi = end($arr_tuan);*/

        $tuan_cuoi = $end;
        
        // lấy danh sách lãnh đạo
        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0  and PK_iMaCB = '.$id_cb,'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbo'][$value['PK_iMaCB']]=$value['sHoTen'];
            $quyen=$value['iQuyenHan_DHNB'];
            $phong= $value['FK_iMaPhongHD'];
        }

        // lấy danh sách lãnh đạo
        $list_pgd_nhaxet = $this->Mdanhmuc->get_kehoach3(' thang='.$month.'  and FK_iMaCB_CV = '.$id_cb,'nhanxet_phophong');
         // lấy danh sách lãnh đạo
        $list_user1 = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0  and iQuyenHan_DHNB = 5','tbl_canbo');
        foreach ($list_user1 as $key1 => $value1) {
            $data['canbo1'][$value1['PK_iMaCB']]=$value1['sHoTen'];
        }

        /*if($month == 12){
            $year = (int)date('Y') + 1;
            $ngaythangnamhan = $year."-01-01";
        }else{
            $month1 = $month + 1;
            if($month1 < 10 ) $month1 ="0".$month1;
            $ngaythangnamhan = date('Y') .'-'.$month1.'-01';
        }*/
        $year = date('Y');

        $ngaythangnamhan = $this->getStartAndEndDate($tuan_cuoi,$year);
        if($month == date('m')){
            $ngaythangnamhan = date('Y-m-d');
        }

        if($quyen==4 or $quyen==5){
            /*$count_arr[1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5     ");
            $count_arr[2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and (CAST(kehoach.ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01')     ");
            $count_arr[3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and  CAST(kehoach.ngay_hoanthanh AS DATE) > ngay_han and ngay_han > '2017-01-01'     ");
            $count_arr[4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and chatluong =1     ");
            $count_arr[5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and chatluong =2      ");
            $count_arr[6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and sangtao =2      ");
            $count_arr[7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan_cuoi.") and active < 5      ");
            $count_arr[8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan_cuoi.") and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')     ");
            $count_arr[9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan_cuoi.") and active < 5 and ngay_han < '".$ngaythangnamhan."'  and ngay_han >'2017-01-01'     ");*/
            $count_arr[1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5     ");
            $count_arr[2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01')     ");
            $count_arr[3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01'      ");
            $count_arr[4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and chatluong =1      ");
            $count_arr[5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and chatluong =2      ");
            $count_arr[6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and sangtao =2      ");
            $count_arr[7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan_cuoi.") and active < 5      ");
            $count_arr[8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan_cuoi.")  and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')     ");
            $count_arr[9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan_cuoi.") and active < 5 and ngay_han < '".$ngaythangnamhan."' and ngay_han >'2017-01-01'      ");
			
			$count_arr[0]['tong'] = $count_arr[1]['tong'] + $count_arr[7]['tong'] ;

            $check = $count_arr[0]['tong'] - $count_arr[8]['tong'];
            if($check >0){
                $count_arr[10] = round($count_arr[1]['tong']/$check*100,1);
            }else{
                $count_arr[10] ='0';
            }
            $count_arr[11] = $data['canbo'];
            $count_arr[12] = $id_cb;
        }

        //điểm thưởng máy tự động chấm
         // mỗi một công viêc có sáng tạo được công thêm 2 điểm
        if($count_arr[6]['tong'] >0) $diemthuongmaycham = ($count_arr[6]['tong']*2)/count($arr_tuan);
        else $diemthuongmaycham = 0;
        if($diemthuongmaycham > 10)$diemthuongmaycham=10;
        $check = $count_arr[0]['tong'] - $count_arr[8]['tong'];
        if($check >0){
            $phantram = round($count_arr[10],2);
            $diemtru = ($count_arr[3]['tong']/$count_arr[1]['tong'])*20;
            $arr_chamtudong=0.6*$phantram - ($count_arr[3]['tong']*0.02); //round($diemtru,1);
            
        }else{
            $count_arr[10] ='100';
            $phantram = 0;
            $arr_chamtudong=60;
        }

        $list_diem = '';
        $list_diem = $this->Mdanhmuc->get_kehoach3('canbo_id='.$id_cb.' and thang='.$month,'danhgialanhdao');
            
        $data['title']    = 'Xem kế hoạch công tác tháng';
        $data['count_arr']   = $count_arr;
        $data['list_diem']   = $list_diem;
        $data['arr_chamtudong']   = $arr_chamtudong;
        $data['diemthuongmaycham']   = $diemthuongmaycham;
        $data['list_pgd_nhaxet']   = $list_pgd_nhaxet;//pr($list_pgd_nhaxet);
        $taikhoan = $this->_session['PK_iMaCB'];
        $truongphong = $this->uri->segment(2);
        $thang = $month;//$this->uri->segment(3);
        $data['nhanxet_danhgia'] = $this->Mkehoachcongtac->laythang($taikhoan,$truongphong,$thang);
        // pr($data['list_diem']);
        $data['month'] = $month;
        $data['quyen'] = $quyen;//pr($quyen);
        $data['quyen_lds'] = $this->_session['iQuyenHan_DHNB'];
        $data['cb_id'] = $id_cb;
        $data['PK_iMaCB'] = $this->_session['PK_iMaCB'];
        $data['phong']= $phong;
        $temp['data']     = $data;//pr($data);

        if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacTuan".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacTuan_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vkehoachcongtac_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vxemkehoachcongtac_GD';
            $this->load->view('layout_admin/layout',$temp);
        }
        
	}

    // cá nhân, lãnh đạo chấm điểm
    public function chamdiem()
    {
        if ($this->input->post('luudulieu')) {
            $cb_idd = $this->uri->segment(2);
            if(empty($cb_idd)) $cb_id = $this->_session['PK_iMaCB'];
            else $cb_id = $cb_idd;

            $thang = _post('month');
            $diem_2_tp = (float)_post('diem_3_tp')+(float)_post('diem_4_tp')+(float)_post('diem_5_tp')+(float)_post('diem_6_tp')+(float)_post('diem_7_tp')+(float)_post('diem_8_tp')+(float)_post('diem_9_tp');
             $diem_10_tp = (float)_post('diem_11_tp')+(float)_post('diem_12_tp')+(float)_post('diem_13_tp')+(float)_post('diem_14_tp')+(float)_post('diem_15_tp')+(float)_post('diem_16_tp')+(float)_post('diem_17_tp')+(float)_post('diem_18_tp');
             $diem_tong_tp = (float)$diem_2_tp + (float)$diem_10_tp + (float)_post('diem_1_tp') + (float)_post('diem_19_tp');
                
            $check_data = $this->Mdanhmuc->layDuLieu2('thang',$thang,'canbo_id',$cb_id,'danhgialanhdao');
            if($this->_session['iQuyenHan_DHNB']==4 ){
                $diem_cv = array(
                'canbo_id' => $cb_id,
                'phong_id' => _post('phong'),
                'diem_1_tp' => _post('diem_1_tp'),
                'diem_2_tp' => $diem_2_tp,
                'diem_3_tp' => _post('diem_3_tp'),
                'diem_4_tp' => _post('diem_4_tp'),
                'diem_5_tp' => _post('diem_5_tp'),
                'diem_6_tp' => _post('diem_6_tp'),
                'diem_7_tp' => _post('diem_7_tp'),
                'diem_8_tp' => _post('diem_8_tp'),
                'diem_9_tp' => _post('diem_9_tp'),
                'diem_10_tp' => $diem_10_tp,
                'diem_11_tp' => _post('diem_11_tp'),
                'diem_12_tp' => _post('diem_12_tp'),
                'diem_13_tp' => _post('diem_13_tp'),
                'diem_14_tp' => _post('diem_14_tp'),
                'diem_15_tp' => _post('diem_15_tp'),
                'diem_16_tp' => _post('diem_16_tp'),
                'diem_17_tp' => _post('diem_17_tp'),
                'diem_18_tp' => _post('diem_18_tp'),
                'diem_19_tp' => _post('diem_19_tp'),
                'diem_tong_tp' =>$diem_tong_tp,// $diem_tong_tp,
                'active'  => 2,
                'thang'   => _post('month'),
                'ngay_ld' => date('Y-m-d H:i:s'),
                'danhgia_ld' => (_post('danhgia_ld'))?_post('danhgia_ld'):(($check_data)?$check_data[0]['danhgia_ld']:''),                
                'danhgia_lds' => (_post('danhgia_lds'))?_post('danhgia_lds'):(($check_data)?$check_data[0]['danhgia_lds']:'')                        
            ); 
            }
            
            $this->list_diem = $diem_cv;
            
            $check_data = $this->Mdanhmuc->layDuLieu2('thang',$thang,'canbo_id',$cb_id,'danhgialanhdao');
            $taikhoan= $this->_session['PK_iMaCB'];
            $hoten   = $this->_session['sHoTen'];
            $quyen   = $this->_session['iQuyenHan_DHNB'];
            $time    = date('Y-m-d H:i:s');
            $date_time = date_time($time);
            $thang   = $this->uri->segment(3);
            if($quyen==5)
            {
                $nhanxet  = _post('danhgia_pgd');
                $noidung  = "Nhận xét của PGĐ <b>$hoten</b> <i>($date_time)</i>: <br/> $nhanxet ";
                $mangcapnhat1 = array('canbo_id' => $cb_id,
                                     'phong_id' => _post('phong'),
                                     'thang'   => _post('month'),
                                     'danhgia_pgd'=> $noidung);

                $mangcapnhat = array('danhgia_pgd'=> $noidung);
            }            
            if(empty($check_data)){
                if($quyen==4)
                    $check_insert = $this->Mdanhmuc->themDuLieu2('danhgialanhdao',$diem_cv);
                if($quyen==5)
                    $check_insert = $this->Mdanhmuc->themDuLieu2('danhgialanhdao',$mangcapnhat1);
                if($check_insert>0)
                {
                    if($quyen==5)
                    {
                        $mangthem = array(
                            'FK_iMaCB_PP' => $taikhoan,
                            'FK_iMaCB_CV' => $cb_id,
                            'tuan'        => 0,
                            'thang'       => $thang,
                            'id_danhgia'  => $check_insert,
                            'sNhanXet'    => $nhanxet,
                            'sThoiGian'   => $time
                        );
                        
                        $kiemtrathem = $this->Mdanhmuc->themDuLieu2('nhanxet_phophong',$mangthem);                        
                    }
                }
                redirect("dskehoachcongtacthangtp");
            }else{
                if($quyen == 4){
                    $check_insert = $this->Mdanhmuc->capnhatDuLieu('dg_id',$check_data[0]['dg_id'],'danhgialanhdao',$diem_cv);
                }
                $kiemtratontaichua = $this->Mkehoachcongtac->laythang($taikhoan,$cb_id,$thang);
                
                if($quyen==5){
                        $nhanxetcu  = "Nhận xét của PGĐ <b>".$hoten."</b> <i>(".date_time($kiemtratontaichua['sThoiGian']).")</i>: <br/> ".$kiemtratontaichua['sNhanXet']." ";
                        $noidungdachinhsua = trim(str_replace($nhanxetcu,'',$check_data[0]['danhgia_pgd']));
                        $mangcapnhatt=array(
                            'danhgia_pgd' => $noidungdachinhsua.'<br/>'.$noidung
                        );
                        $mangthem = array(
                            'FK_iMaCB_PP' => $taikhoan,
                            'FK_iMaCB_CV' => $cb_id,
                            'tuan'        => 0,
                            'thang'       => $thang,
                            'id_danhgia'  => $check_data[0]['dg_id'],
                            'sNhanXet'    => $nhanxet,
                            'sThoiGian'   => $time
                        );
                        if(!empty($kiemtratontaichua)){
                            if($taikhoan!=$cb_id){
                                $this->Mdanhmuc->capnhatDuLieu('dg_id',$check_data[0]['dg_id'],'danhgialanhdao',$mangcapnhatt);
                                $this->Mkehoachcongtac->capnhatthang($taikhoan,$cb_id,$thang,$mangthem);
                            }
                        
                        }
                        else
                        {   
                            if($taikhoan!=$cb_id){
                                $this->Mdanhmuc->capnhatDuLieu('dg_id',$check_data[0]['dg_id'],'danhgialanhdao',$mangcapnhatt);
                                $this->Mdanhmuc->themDuLieu2('nhanxet_phophong',$mangthem);
                            }
                        }
                    }
                
                redirect("dskehoachcongtacthangtp");
            }
            
            if ($check_insert > 0) {
                $this->_giatri ='';
                return messagebox('Thêm thành công ', 'info');
            } else {
                return messagebox('Thêm thất bại', 'danger');
            } 
            
        }
    }
}
 

    