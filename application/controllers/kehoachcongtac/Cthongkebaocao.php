<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongkebaocao extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        if(_post('action') == 'getmaphong'){
            $this->getmaphong();
        }
        if(_post('thongke') == 'thongkebaocao'){
            $tuanbd = _post('tuanbd');
            $tuankt = _post('tuankt');
            $phong = _post('phongban');
            $canbo = _post('canbo');
            $loai = _post('danhgia');
            $where =' 1=1 ';

            if($tuanbd > 0){
                $where .= " and  tuan >= ".$tuanbd;
            }
            if($tuankt > 0){
                $where .= " and  tuan <= ".$tuankt;
            }
            if($phong > 0){
                $where .= " and  phong_id = ".$phong;
                $data['danhsachcbphong'] = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 6 and FK_iMaPhongHD='.$phong,'tbl_canbo');

            }else{
                $data['danhsachcbphong'] = array();
            }
            if($canbo > 0){
                $where .= " and  canbo_id = ".$canbo;
            }
            /*if($loai == 1){
                $where .= " and  diem_tong_tp >= 90 and diem_19_tp > 0 ";
            }
            if($loai == 2){
                $where .= " and diem_tong_tp >= 80  and ( diem_19_tp = 0 or diem_tong_tp < 90)";
            }
            if($loai == 3){
                $where .= " and diem_tong_tp >= 70 and diem_tong_tp < 80";
            }
            if($loai == 4){
                $where .= " and  diem_tong_tp >= 60 and and diem_tong_tp < 70";
            }
            if($loai == 5){
                $where .= " and  diem_tong_tp < 60  ";
            }*/

            $data['kehoach']    = $this->Mdanhmuc->get_kehoach3($where,'danhgia','phong_id,canbo_id,tuan');
            $data['tuanbd']     = $tuanbd;
            $data['tuankt']     = $tuankt;
            $data['phong']      = $phong;
            $data['canbo']      = $canbo;
            $data['loai']       = $loai;
        }else{
            $data['kehoach'] = array();
            $data['tuanbd']     = 0;
            $data['tuankt']     = 0;
            $data['phong']      = 0;
            $data['canbo']      = 0;
            $data['loai']       = 0;
        }
    
        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('(PK_iMaPB < 22 or PK_iMaPB = 68) and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        $year = date('Y');
        $arr=array();
        for($i=0;$i<=53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            
        }

        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 6 ','tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
                $data['tencanbo'][$value['PK_iMaCB']]=$value['sHoTen'];
        }  
        

        $data['title']    = 'Thống kê báo cáo';
        $data['arr']   = $arr;//pr($arr);
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=ThongKeBaoCao_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=ThongKeBaoCao_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vthongkebaocao_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vthongkebaocao';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}    
    public function getmaphong()
    {
        $phong_id = _post('maphong');
        $datacanbo = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 6 and FK_iMaPhongHD='.$phong_id,'tbl_canbo');
        echo json_encode($datacanbo); exit();
    }
}

/* End of file Cthongkebaocao.php */
