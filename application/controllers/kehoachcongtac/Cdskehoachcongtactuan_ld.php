<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdskehoachcongtactuan_ld extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{    
        /*$month = date('m');
        if(isset($_POST['month']))$month = $_POST['month'];
        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);*/
        $tuan = (int)date("W", strtotime(date('Y-m-d')));
        if($this->uri->segment(2) >0) $tuan = $this->uri->segment(2);
        if(_post('tuan')>0) $tuan = _post('tuan');

        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3(' iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        
        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB >= 3 and iQuyenHan_DHNB <=6 and PK_iMaCB != 617','tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            if($value['iQuyenHan_DHNB']==3 or $value['iQuyenHan_DHNB']==6){
                $chuyenvien[$value['PK_iMaCB']]=$value['sHoTen'];
                $phong[$value['PK_iMaCB']]= $value['FK_iMaPhongHD'];
            }else{
                if($value['iQuyenHan_DHNB']==4 || $value['iQuyenHan_DHNB']==5)$lanhdao[$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        $year = date('Y');
        $arr=array();unset($arr[0]);
        for($i=0;$i<53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            $j= $i+1;
            if($tuan == $j){
            	$ngaycuoituan = $week_end = date('Y-m-d', strtotime('this week sunday', $custom_date));
            }  
            
        }  
       //echo $ngaycuoituan;
        $key_cv=11;
        foreach ($chuyenvien as $id_cb => $tencb) {
            /*$tong1[$key_cv][0] = $this->Mdanhmuc->countLD_thang("kh_id"," tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]);
            $tong2[$key_cv][0] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]);
            $count_arr[$key_cv][0] = $tong1[$key_cv][0]+$tong2[$key_cv][0];*/

            /*$tong1[$key_cv][1] = $this->Mdanhmuc->countLD_thang("kh_id"," tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5");
            $tong2[$key_cv][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and  tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5");
            $count_arr[$key_cv][1] = $tong1[$key_cv][1]+$tong2[$key_cv][1];*/

            $tong1[$key_cv][2] = $this->Mdanhmuc->countLD_thang("kh_id"," (vanban_id IS NULL OR vanban_id ='') AND  tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and (CAST(ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01')");
            $tong2[$key_cv][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and   tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and (CAST(ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[$key_cv][2]['tong'] = $tong1[$key_cv][2]['tong']+$tong2[$key_cv][2]['tong'];

            $tong1[$key_cv][3] = $this->Mdanhmuc->countLD_thang("kh_id"," (vanban_id IS NULL OR vanban_id ='') AND  tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and  CAST(ngay_hoanthanh AS DATE) > ngay_han and ngay_han > '2017-01-01' ");
            $tong2[$key_cv][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and   tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and  CAST(ngay_hoanthanh AS DATE) > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cv][3]['tong'] = $tong1[$key_cv][3]['tong'] + $tong2[$key_cv][3]['tong']; 

            $tong1[$key_cv][4] = $this->Mdanhmuc->countLD_thang("kh_id"," (vanban_id IS NULL OR vanban_id ='') AND  tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =1 ");
            $tong2[$key_cv][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =1 ");
            $count_arr[$key_cv][4]['tong']= $tong1[$key_cv][4]['tong'] + $tong2[$key_cv][4]['tong'] ;

            $tong1[$key_cv][5] = $this->Mdanhmuc->countLD_thang("kh_id"," (vanban_id IS NULL OR vanban_id ='') AND  tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =2 ");
            $tong2[$key_cv][5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =2 ");
            $count_arr[$key_cv][5]['tong'] = $tong1[$key_cv][5]['tong'] + $tong2[$key_cv][5]['tong'];

            $tong1[$key_cv][6] = $this->Mdanhmuc->countLD_thang("kh_id"," (vanban_id IS NULL OR vanban_id ='') AND  tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and sangtao =2 ");
            $tong2[$key_cv][6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active = 5 and sangtao =2 ");
            $count_arr[$key_cv][6]['tong'] = $tong1[$key_cv][6]['tong'] + $tong2[$key_cv][6]['tong'] ;

            /*$tong1[$key_cv][7] = $this->Mdanhmuc->countLD_thang("kh_id"," tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active < 5 ");
            $tong2[$key_cv][7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active < 5 ");
            $count_arr[$key_cv][7] = $tong1[$key_cv][7] + $tong2[$key_cv][7] ;*/

            $tong1[$key_cv][8] = $this->Mdanhmuc->countLD_thang("kh_id"," (vanban_id IS NULL OR vanban_id ='') AND   tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active < 5 and (ngay_han >='".$ngaycuoituan."' or ngay_han < '2017-01-01')");
            $tong2[$key_cv][8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active < 5 and (ngay_han >='".$ngaycuoituan."' or ngay_han < '2017-01-01')");
            $count_arr[$key_cv][8]['tong'] = $tong1[$key_cv][8]['tong'] + $tong2[$key_cv][8]['tong'];

            $tong1[$key_cv][9] = $this->Mdanhmuc->countLD_thang("kh_id"," (vanban_id IS NULL OR vanban_id ='') AND  tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active < 5 and ngay_han < '".$ngaycuoituan."'  and ngay_han >'2017-01-01'");
            $tong2[$key_cv][9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 and tuan = ".$tuan."  and phong_id = ".$phong[$id_cb]." and active < 5 and ngay_han < '".$ngaycuoituan."'  and ngay_han >'2017-01-01'");
            $count_arr[$key_cv][9]['tong'] = $tong1[$key_cv][9]['tong'] + $tong2[$key_cv][9]['tong'];

            $count_arr[$key_cv][7]['tong'] = $count_arr[$key_cv][8]['tong'] + $count_arr[$key_cv][9]['tong'];
            $count_arr[$key_cv][1]['tong'] = $count_arr[$key_cv][2]['tong'] + $count_arr[$key_cv][3]['tong'];
            $count_arr[$key_cv][0]['tong'] = $count_arr[$key_cv][7]['tong'] + $count_arr[$key_cv][1]['tong'];

            $check = $count_arr[$key_cv][0]['tong'] - $count_arr[$key_cv][8]['tong'];
            if($check >0){
                $count_arr[$key_cv][10] = round($count_arr[$key_cv][1]['tong']/$check*100,1);
            }else{
                $count_arr[$key_cv][10] ='0';
            }
            $count_arr[$key_cv][11] = $tencb;
            $count_arr[$key_cv][12] = $id_cb;
            $count_arr[$key_cv][15] = $phong[$id_cb];
            $key_cv++;
        }//pr($count_arr);
        //unset($count_arr[11]);      

        $data['title']    = ' đánh giá kế hoạch công tác tuần';
        $data['phong_id'] = $this->_session['FK_iMaPhongHD'];
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['count_arr']   = $count_arr;
        $data['arr']   = $arr;
        $data['tuan'] = $tuan;
        $data['list_user'] = $list_user;
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtactuanld_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtactuan_ld';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}
   
}

/* End of file Cdskehoachcongtac.php */
/* Location: ./application/controllers/vanban/Cdskehoachcongtac.php */