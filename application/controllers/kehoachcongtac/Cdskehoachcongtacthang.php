<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdskehoachcongtacthang extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{  
		$tuan_hientai = (int)date("W", strtotime(date('Y-m-d')));
        if(date('d')<15)$month = date('m')-1;
        else $month = date('m');
        if($month == 0)$month = 12;
        $tam_thang = $this->uri->segment(3);
        if(isset($tam_thang) && $tam_thang >0 ) $month = $tam_thang;
        if(isset($_POST['month']))$month = $_POST['month'];
        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);
        if($month == 2)$arr_tuan = array(7,8,9);
        
        if($tuan_hientai < end($arr_tuan))$tuan_cuoi = $tuan_hientai;
        else $tuan_cuoi = end($arr_tuan);
        $phong_id = $this->_session['FK_iMaPhongHD'];
        $tam = $this->uri->segment(2);
        if(isset($tam) && $tam >0 ) $phong_id = $tam;

        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        
        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and FK_iMaPhongHD='.$phong_id,'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            if($value['iQuyenHan_DHNB']==8){
                $chuyenvien[$value['PK_iMaCB']]=$value['sHoTen'];
            }else{
                if($value['iQuyenHan_DHNB']>6 && $value['iQuyenHan_DHNB']!=9)$lanhdao[$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        $year = date('Y');
        
        //lay tổng gia tri cac cong viec cua tung chuyen vien
        $key_cb=0;
        foreach ($lanhdao as $id_cb => $tencb) {
            
            $count_arr[$key_cb][1] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5");
            $count_arr[$key_cb][2] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01' or ngay_han IS NULL or  ngay_hoanthanh IS NULL)");
            $count_arr[$key_cb][3] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cb][4] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and chatluong =1 ");
            $count_arr[$key_cb][5] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and chatluong =2 ");
            $count_arr[$key_cb][6] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and sangtao =2 ");
            $count_arr[$key_cb][7] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and canbo_id=".$id_cb." and active < 5 ");
            
            $count_arr[$key_cb][0] = $count_arr[$key_cb][1] + $count_arr[$key_cb][7];
            
            $count_arr[$key_cb][8] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and canbo_id=".$id_cb." and active < 5 and qua_han = 1 ");
            
            $count_arr[$key_cb][9] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and canbo_id=".$id_cb." and active < 5 and qua_han = 2 ");
            $quyen_canbo = $this->Mdanhmuc->get_quyen($id_cb)['iQuyenHan_DHNB'];
            
            if($quyen_canbo==10 or $quyen_canbo==11){
	            $count_arr[$key_cb][1] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and lanhdao_id=".$id_cb." and active = 5");
	            $count_arr[$key_cb][2] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and lanhdao_id=".$id_cb." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01' or ngay_han IS NULL or  ngay_hoanthanh IS NULL)");
	            $count_arr[$key_cb][3] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and lanhdao_id=".$id_cb." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
	            $count_arr[$key_cb][4] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and lanhdao_id=".$id_cb." and active = 5 and chatluong =1 ");
	            $count_arr[$key_cb][5] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and lanhdao_id=".$id_cb." and active = 5 and chatluong =2 ");
	            $count_arr[$key_cb][6] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and lanhdao_id=".$id_cb." and active = 5 and sangtao =2 ");
	            $count_arr[$key_cb][7] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and lanhdao_id=".$id_cb." and active < 5 ");
	            
	            $count_arr[$key_cb][0] = $count_arr[$key_cb][1] + $count_arr[$key_cb][7];
	            $count_arr[$key_cb][8] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and lanhdao_id=".$id_cb." and active < 5 and qua_han = 1 ");
	            $count_arr[$key_cb][9] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and lanhdao_id=".$id_cb." and active < 5 and qua_han = 2 ");            	
            }
            
            $check = $count_arr[$key_cb][0] - $count_arr[$key_cb][8];
            if($check >0 && $count_arr[$key_cb][1]>0){
                $count_arr[$key_cb][10] = round($count_arr[$key_cb][1]/$check*100,1);
            }else{
                $count_arr[$key_cb][10] ='0';
            }
            $count_arr[$key_cb][11] = $tencb;
            $count_arr[$key_cb][12] = $id_cb;
            $count_arr[$key_cb][13] = $this->Mdanhmuc->count_thang('diem','id_cb ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][14] = $this->Mdanhmuc->count_thang('danhgia_tp','id_cb ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][15] = $this->Mdanhmuc->count_thang('nhanxet,danhgia_cv,nhanxet_cv,date_cv,date_tp','id_cb ='.$id_cb.' and thang = '.$month.'');
            $key_cb++;
        }
        $key_cv=11;
        foreach ($chuyenvien as $id_cb => $tencb) {
            
            $count_arr[$key_cv][1] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5");
            $count_arr[$key_cv][2] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01'  or ngay_han IS NULL or  ngay_hoanthanh IS NULL )");
            $count_arr[$key_cv][3] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cv][4] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and chatluong =1 ");
            $count_arr[$key_cv][5] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and chatluong =2 ");
            $count_arr[$key_cv][6] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 5 and sangtao =2 ");
            $count_arr[$key_cv][7] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and canbo_id=".$id_cb." and active < 5 ");
            
            $count_arr[$key_cv][0] = $count_arr[$key_cv][1]+$count_arr[$key_cv][7];
            
            $count_arr[$key_cv][8] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and canbo_id=".$id_cb." and active < 5 and qua_han = 1 ");
 
            $count_arr[$key_cv][9] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and canbo_id=".$id_cb." and active < 5 and qua_han = 2 ");
            $check = $count_arr[$key_cv][0] - $count_arr[$key_cv][8];
            if($check >0 && $count_arr[$key_cv][1]>0){
                $count_arr[$key_cv][10] = round($count_arr[$key_cv][1]/$check*100,1);
            }else{
                $count_arr[$key_cv][10] ='0';
            }
            $count_arr[$key_cv][11] = $tencb;
            $count_arr[$key_cv][12] = $id_cb;
            $count_arr[$key_cv][13] = $this->Mdanhmuc->count_thang('diem','id_cb ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][14] = $this->Mdanhmuc->count_thang('danhgia_tp','id_cb ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][15] = $this->Mdanhmuc->count_thang('nhanxet,date_tp,danhgia_cv,nhanxet_cv,date_cv,nhanxet_pp','id_cb ='.$id_cb.' and thang = '.$month.'');
            $key_cv++;
        }//pr($count_arr);
         
        
            $count_arr1[1] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 5 and thuc_hien = 1");
            $count_arr1[2] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 5 and thuc_hien = 1 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01' or ngay_han IS NULL or  ngay_hoanthanh IS NULL )");
            $count_arr1[3] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 5 and thuc_hien = 1 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr1[4] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 5 and thuc_hien = 1 and chatluong =1 ");
            $count_arr1[5] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 5 and thuc_hien = 1 and chatluong =2 ");
            $count_arr1[6] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 5 and thuc_hien = 1 and sangtao =2 ");
            $count_arr1[7] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and phong_id = ".$phong_id." and active < 5  and thuc_hien = 1");
            
            $count_arr1[0] = $count_arr1[1] + $count_arr1[7];
            $count_arr1[8] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and phong_id = ".$phong_id." and active < 5 and thuc_hien = 1  and qua_han = 1 ");
            $count_arr1[9] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan_cuoi.") and phong_id = ".$phong_id." and active < 5 and thuc_hien = 1 and qua_han = 2 ");
            $check = $count_arr1[0] - $count_arr1[8];
            if($check >0 && $count_arr1[1]>0){
                $count_arr1[10] = round($count_arr1[1]/$check*100,1);
            }else{
                $count_arr1[10] ='0';
            }
               

        $data['title']    = 'Đánh giá kế hoạch công tác tháng';
        $data['phong_id'] = $phong_id;
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $quyenhan = $this->_session['iQuyenHan_DHNB'];
        $chucvu   = $this->_session['FK_iMaCV'];
        $phongban = $this->_session['FK_iMaPhongHD'];
        $taikhoan = $this->_session['PK_iMaCB'];
        $data['taikhoan'] = $taikhoan;
        $data['quyenhan'] = $quyenhan;
        $data['chucvu']   = $chucvu;
        $data['phongban'] = $phongban;
        $data['count_arr']   = $count_arr;
        $data['count_arr1']   = $count_arr1;
        $data['month'] = $month;
        //$data['week'] = $tuan;
        $data['list_user'] = $list_user;
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtacthang_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtacthang';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}
   
}

/* End of file Cdskehoachcongtac.php */
/* Location: ./application/controllers/vanban/Cdskehoachcongtac.php */