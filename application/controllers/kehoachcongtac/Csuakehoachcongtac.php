<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csuakehoachcongtac extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        $tuan = $this->uri->segment(2);
        if($tuan > 0){
            $check_data = $this->Mdanhmuc->layDuLieu2('tuan',$tuan,'user_id',$this->_session['PK_iMaCB'],'tbl_kehoach');
            $this->_giatri = $check_data[0];
        }

        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->suaKeHoach();
            redirect("dskehoachcongtac");
        }

        $start = date('d/m/Y',strtotime($check_data[0]['date_start']));
        $end = date('d/m/Y',strtotime($check_data[0]['date_end']));
 
        $data['giatri'] = $this->_giatri;    
        $data['title']    = 'Sửa kế hoạch công tác';
        $data['arr']   = $tuan." (".$start." - ".$end.")";
        $data['tuan'] = $tuan;
        $temp['data']     = $data;
       
        $temp['template'] = 'kehoachcongtac/Vsuakehoachcongtac';
        $this->load->view('layout_admin/layout',$temp);
	}
    // thêm kế hoạch công tác tuần
    public function suaKeHoach()
    {
        $id = _post('id');
        if(_post('date_phatsinh2')=="")$date_phatsinh2 = date('Y-m-d H:i:s');
        else $date_phatsinh2 = _post('date_phatsinh2');
        if(_post('date_phatsinh3')=="")$date_phatsinh3 = date('Y-m-d H:i:s');
        else $date_phatsinh3 = _post('date_phatsinh3');
        if(_post('date_phatsinh4')=="")$date_phatsinh4 = date('Y-m-d H:i:s');
        else $date_phatsinh4 = _post('date_phatsinh4');
        if(_post('date_phatsinh5')=="")$date_phatsinh5 = date('Y-m-d H:i:s');
        else $date_phatsinh5 = _post('date_phatsinh5');
        if(_post('date_phatsinh6')=="")$date_phatsinh6 = date('Y-m-d H:i:s');
        else $date_phatsinh6 = _post('date_phatsinh6');
        if(_post('date_phatsinh7')=="")$date_phatsinh7= date('Y-m-d H:i:s');
        else $date_phatsinh7 = _post('date_phatsinh7');


        if ($this->input->post('luudulieu')) {
            $data_kehoachtuan = array(
                'user_id' => $this->_session['PK_iMaCB'],
                'department_id' => $this->_session['FK_iMaPhongHD'],
                'viec2s' => _post('viec2s'),
                'viec2c' => _post('viec2c'),
                'phatsinh2s' => _post('phatsinh2s'),
                'phatsinh2c' => _post('phatsinh2c'),
                'date_phatsinh2' => $date_phatsinh2,
                'viec3s' => _post('viec3s'),
                'viec3c' => _post('viec3c'),
                'phatsinh3s' => _post('phatsinh3s'),
                'phatsinh3c' => _post('phatsinh3c'),
                'date_phatsinh3' => $date_phatsinh3,
                'viec4s' => _post('viec4s'),
                'viec4c' => _post('viec4c'),
                'phatsinh4s' => _post('phatsinh4s'),
                'phatsinh4c' => _post('phatsinh4c'),
                'date_phatsinh4' => $date_phatsinh4,
                'viec5s' => _post('viec5s'),
                'viec5c' => _post('viec5c'),
                'phatsinh5s' => _post('phatsinh5s'),
                'phatsinh5c' => _post('phatsinh5c'),
                'date_phatsinh5' => $date_phatsinh5,
                'viec6s' => _post('viec6s'),
                'viec6c' => _post('viec6c'),
                'phatsinh6s' => _post('phatsinh6s'),
                'phatsinh6c' => _post('phatsinh6c'),
                'date_phatsinh6' => $date_phatsinh6,
                'viec7s' => _post('viec7s'),
                'viec7c' => _post('viec7c'),
                'phatsinh7s' => _post('phatsinh7s'),
                'phatsinh7c' => _post('phatsinh7c'),
                'date_phatsinh7' => $date_phatsinh7,
                'tuan'   => _post('tuan'),
                'date_nhap' => _post('date_nhap'),
                'date_start' => _post('date_start'),
                'date_end' => _post('date_end')           
            );
            $this->_giatri = $data_kehoachtuan;
           //thêm mới dữ liệu
            $check_update = $this->Mdanhmuc->capnhatDuLieu('id',$id,'tbl_kehoach',$data_kehoachtuan);
            if ($check_update > 0) {
                return messagebox('Cập nhật thành công kế hoạch công tác tuần ' ._post('tuan'), 'info');
            } else {
                return messagebox('Cập nhật kế hoạch tuần thất bại', 'danger');
            }             
        }
    }
}
    