<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemkehoachcongtac extends MY_Controller {
    protected $_giatri;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
         $this->Mkehoachcongtac = new Mkehoachcongtac();        
    }
    public function index()
    {

// khi chấm điểm xong: sẽ tự chuyển việc chưa hoàn thành sang tuần tới
/*if($this->_session['iQuyenHan_DHNB'] > 6 ){
            $week = date('W');$cb_id=596;
            //if(date('l')=='Monday'){$week = $tuan;}
            $week_1 = $week-1;
            $check_data ='';
            $check_data = $this->Mdanhmuc->layDuLieu2('tuan',$week,'canbo_id',$cb_id,'kehoach');//echo $week;
            //lấy chuỗi kh_id_sub trong tuần tiếp theo
            $str_subID='0';
            foreach ($check_data as $key => $value) {
                if($value['kh_id_sub'] >0){
                    $str_subID .= ','.$value['kh_id_sub'];
                }
            }
            $giatri = $this->Mdanhmuc->get_kehoach3('tuan = '.$week_1.' and (active < 5  or  loai_kh = 3) and canbo_id ='.$cb_id.' and kh_id not in('.$str_subID.')','kehoach');//pr($giatri);
            foreach ($giatri as $key => $value) {
                $value['kh_id_sub'] = $value['kh_id'];
                $kh_id = $value['kh_id'];
                unset($value['kh_id']);
                $value['tuan'] = $week;
                if($value['loai_kh'] == 3){
                    $value['active'] = 1;
                    $value['ykien_tp'] = '';
                    $value['ngay_ykien'] = '0000-00-00';
                    $value['ykien_pp'] = '';
                    $value['ket_qua_hoanthanh'] = '';
                    $value['ngay_hoanthanh'] = '0000-00-00';
                    $value['ngay_danhgia'] = '0000-00-00';
                     $value['danh_gia'] = '';

                }
                $this->Mdanhmuc->themDuLieu('kehoach',$value);
                // update lại trường qua_han
                if($value['ngay_han'] < date('Y-m-d') && $value['ngay_han'] >'2017-01-01'){
                    $this->Mdanhmuc->quahan($kh_id,$week_1);
                }
            }
}*/
        $week = date('W');
        $sql ="UPDATE kehoach k, tbl_vanbanden v SET k.active= 5,k.ngay_hoanthanh = v.sNgayGiaiQuyet WHERE k.vanban_id = v.PK_iMaVBDen AND  v.iTrangThai = 1 AND k.tuan = ".$week." AND k.canbo_id =".$this->_session['PK_iMaCB'];
        $this->Mdanhmuc->capnhattuan($sql);

        $where_mocham = " 1=1 and ketthuc >= '".date('Y-m-d H:i:s')."' and canbo = ".$this->_session['PK_iMaCB'];
        $data['mocham'] = $this->Mdanhmuc->get_kehoach3($where_mocham,'tbl_mocham','');
        //pr($data['mocham']);


        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->chamdiem();
        } 
        $tuanhientai = $week;// (int)date("W", strtotime(date('Y-m-d')));  
        $taikhoan   = $this->_session['PK_iMaCB'];
        $tuan       = $this->uri->segment(2);
        $chuyenvien = $this->uri->segment(3);
        $id_danhgia = $this->uri->segment(4);
        $data['nhanxetphophong'] = $this->Mkehoachcongtac->lay($taikhoan,$chuyenvien,$tuan,$id_danhgia);
        $macanbo = $this->uri->segment(3);
        if($macanbo > 0){}else{$macanbo = $this->_session['PK_iMaCB'];}
        $count_arr = array();

        $data['danhsachvipham'] = $this->Mdanhmuc->layDuLieu2('tuan',$tuan,'macanbo',$macanbo,'tbl_kiemtracongvu');
        if(isset($data['danhsachvipham'])){}else{
           $data['danhsachvipham']=array(0 => Array('nhomvipham' => 0));
        }

        $phong_id =  $this->Mkehoachcongtac->lay_phong_id($id_danhgia)['phong_id'];//pr($phong_id);
        if($phong_id > 0){

        }else{
           $phong_id = $this->_session['FK_iMaPhongHD'];
        }
        $canbo = $this->Mdanhmuc->get_kehoach3('FK_iMaPhongHD = '.$phong_id.' and iTrangThai = 0','tbl_canbo');

        $dscanbo ='';
        foreach ($canbo as $key => $ma_cb) {
            $dscanbo .= $ma_cb['PK_iMaCB'].',';
        }
        $dscanbo = rtrim($dscanbo, ",");
        if($this->_session['iQuyenHan_DHNB']>=7){ //if($this->_session['iQuyenHan_DHNB']==8){
            $dscanbo = $this->_session['PK_iMaCB'];
        }
        if($chuyenvien > 0)$dscanbo = $chuyenvien;
        //lấy quyền của người đang bị xem
        $quyen_canbo = $this->Mdanhmuc->get_quyen($dscanbo)['iQuyenHan_DHNB'];//pr($quyen_canbo);
        $year = date ('Y');
        $week_current = $tuan - 1;
        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week_current * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
        $date_for_friday = date( 'Y-m-d', $timestamp_for_monday + 5 * 24 * 60 * 60 );
        $date_for_sunday = date( 'Y-m-d', $timestamp_for_monday + 7 * 24 * 60 * 60 );
        //echo $date_for_monday." - ".$date_for_sunday;
        if($this->_session['iQuyenHan_DHNB']==6 or $this->_session['iQuyenHan_DHNB']==3 or $this->_session['iQuyenHan_DHNB']==8 or $this->_session['iQuyenHan_DHNB']==4 or $this->_session['iQuyenHan_DHNB']==5 or $this->_session['iQuyenHan_DHNB']==7 or $this->_session['iQuyenHan_DHNB']==10 or $this->_session['iQuyenHan_DHNB']==11 ){
            $vbct_dh = $this->Mdanhmuc->get_kehoach3('PK_iMaCBHoanThanh in ('.$dscanbo.') and sNgayGiaiQuyet >= "'.$date_for_monday.'" and sNgayGiaiQuyet < "'.$date_for_sunday.'" and (sNgayGiaiQuyet <= sHanThongKe or sHanThongKe < "1999-12-12" or sHanThongKe is null ) and vbdaura_cv !=1','tbl_vanbanden','PK_iMaVBDen');

            $ds_idctdh ='0,';
            foreach ($vbct_dh as $key => $ds_ctdh) {
                $ds_idctdh = $ds_idctdh.$ds_ctdh['PK_iMaVBDen'].',';
            }
            if($ds_idctdh!='')$ds_idctdh = rtrim($ds_idctdh, ",");

            $vbct_qh = $this->Mdanhmuc->get_kehoach3('PK_iMaCBHoanThanh in ('.$dscanbo.') and sNgayGiaiQuyet >= "'.$date_for_monday.'" and sNgayGiaiQuyet < "'.$date_for_sunday.'" and (sNgayGiaiQuyet > sHanThongKe and sHanThongKe > "1999-12-12") and vbdaura_cv !=1','tbl_vanbanden','PK_iMaVBDen');

            $ds_idctqh ='0,';
            foreach ($vbct_qh as $key => $ds_ctqh) {
                $ds_idctqh = $ds_idctqh.$ds_ctqh['PK_iMaVBDen'].',';
            }
            if($ds_idctqh!='')$ds_idctqh = rtrim($ds_idctqh, ",");

            if(count($vbct_dh)>0) $tong_vbct_dh = count($vbct_dh);
            else $tong_vbct_dh = 0;
            if(count($vbct_qh)>0) $tong_vbct_qh = count($vbct_qh);
            else $tong_vbct_qh = 0;

            $vbph_dh = $this->Mdanhmuc->get_kehoach3('PK_iMaCB in ('.$dscanbo.') and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and (sThoiGianHoanThanh <= sThoiGianHetHan or sThoiGianHetHan < "1999-12-12" or sThoiGianHetHan is null )','tbl_phongphoihop','PK_iMaVBDen');

            $ds_idphdh ='0,';
            foreach ($vbph_dh as $key => $ds_phdh) {
                $ds_idphdh = $ds_idphdh.$ds_phdh['PK_iMaVBDen'].',';
            }
            if($ds_idphdh!='')$ds_idphdh = rtrim($ds_idphdh, ",");


            $vbph_qh = $this->Mdanhmuc->get_kehoach33('PK_iMaCB in ('.$dscanbo.') and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and sThoiGianHoanThanh > sThoiGianHetHan and sThoiGianHetHan > "1999-12-12" ','tbl_phongphoihop','PK_iMaVBDen');

            $ds_idphqh ='0,';
            foreach ($vbph_qh as $key => $ds_phqh) {
                $ds_idphqh = $ds_idphqh.$ds_phqh['PK_iMaVBDen'].',';
            }
            if($ds_idphqh!='')$ds_idphqh = rtrim($ds_idphqh, ",");

            if(count($vbph_dh)>0) $tong_vbph_dh = count($vbph_dh);
            else $tong_vbph_dh = 0;
            if(count($vbph_qh)>0) $tong_vbph_qh = count($vbph_qh);
            else $tong_vbph_qh = 0;

        }/*else if($this->_session['iQuyenHan_DHNB']==7 or $this->_session['iQuyenHan_DHNB']==10 or $this->_session['iQuyenHan_DHNB']==11 ){
            if($chuyenvien > 0){
                $macanbo_1 = $chuyenvien;
            }else{
                $macanbo_1 = $this->_session['PK_iMaCB'];
            }
            
            $tong_vbct_dh = 0;
            $tong_vbct_qh = 0;
            $str_idvd = '0,';
            $dsvbct = $this->Mdanhmuc->get_kehoach3('sNgayGiaiQuyet >= "'.$date_for_monday.'" and sNgayGiaiQuyet < "'.$date_for_sunday.'" and vbdaura_cv !=1','tbl_vanbanden','PK_iMaVBDen');
            foreach ($dsvbct as $key => $idvbd) {
                $str_idvd = $str_idvd.$idvbd['PK_iMaVBDen'].',';
            }
            if($str_idvd != ''){
                $str_idvd = rtrim($str_idvd, ",");
                $list_ppvbct = $this->Mdanhmuc->get_kehoach3('PK_iMaVBDen in('.$str_idvd.') and PK_iMaCVCT = '.$macanbo_1.'','tbl_chuyennhanvanban','PK_iMaVBDen');
                $str_pp_vbd ='0,';
                foreach ($list_ppvbct as $key => $idvbdpp) {
                    $str_pp_vbd = $str_pp_vbd.$idvbdpp['PK_iMaVBDen'].',';
                }
                $str_pp_vbd = rtrim($str_pp_vbd, ",");

                $vbct_dh = $this->Mdanhmuc->get_kehoach3('PK_iMaVBDen in ('.$str_pp_vbd.') and (sNgayGiaiQuyet <= sHanThongKe or sHanThongKe < "1999-12-12" or sHanThongKe is null ) and vbdaura_cv !=1','tbl_vanbanden','PK_iMaVBDen');

                $ds_idctdh ='0,';
                foreach ($vbct_dh as $key => $ds_ctdh) {
                    $ds_idctdh = $ds_idctdh.$ds_ctdh['PK_iMaVBDen'].',';
                }
                if($ds_idctdh!='')$ds_idctdh = rtrim($ds_idctdh, ",");

                $vbct_qh = $this->Mdanhmuc->get_kehoach3('PK_iMaVBDen in ('.$str_pp_vbd.') and (sNgayGiaiQuyet > sHanThongKe and sHanThongKe > "1999-12-12")','tbl_vanbanden','PK_iMaVBDen');

                $ds_idctqh ='0,';
                foreach ($vbct_qh as $key => $ds_ctqh) {
                    $ds_idctqh = $ds_idctqh.$ds_ctqh['PK_iMaVBDen'].',';
                }
                if($ds_idctqh!='')$ds_idctqh = rtrim($ds_idctqh, ",");

                $tong_vbct_dh = count($vbct_dh);
                $tong_vbct_qh = count($vbct_qh);
            }


            $vbph_dh = $this->Mdanhmuc->get_kehoach3('input_per = '.$macanbo_1.'  and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and (sThoiGianHoanThanh <= sThoiGianHetHan or sThoiGianHetHan < "1999-12-12" or sThoiGianHetHan is null )','tbl_phongphoihop','PK_iMaVBDen');

            $ds_idphdh ='0,';
            foreach ($vbph_dh as $key => $ds_phdh) {
                $ds_idphdh = $ds_idphdh.$ds_phdh['PK_iMaVBDen'].',';
            }
            if($ds_idphdh!='')$ds_idphdh = rtrim($ds_idphdh, ",");

            $vbph_qh = $this->Mdanhmuc->get_kehoach3('input_per = '.$macanbo_1.'  and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and sThoiGianHoanThanh > sThoiGianHetHan and sThoiGianHetHan > "1999-12-12" ','tbl_phongphoihop','PK_iMaVBDen');

            $ds_idphqh ='0,';
            foreach ($vbph_qh as $key => $ds_phqh) {
                $ds_idphqh = $ds_idphqh.$ds_phqh['PK_iMaVBDen'].',';
            }
            if($ds_idphqh!='')$ds_idphqh = rtrim($ds_idphqh, ",");
    
            if(count($vbph_dh) > 0)$tong_vbph_dh = count($vbph_dh);
            else $tong_vbph_dh = 0;
            if(count($vbph_qh) > 0)$tong_vbph_qh = count($vbph_qh);
            else $tong_vbph_qh = 0;
        }*/


if($tuanhientai==$tuan){
    $hantuan = date('Y-m-d');
}else{
    $hantuan = $date_for_friday;
}
if($hantuan == '2019-12-28')$hantuan =date('Y-m-d');

        if($tuan > 0){
            if($this->_session['iQuyenHan_DHNB']>=7){
                $list_data1 = $this->Mdanhmuc->get_kehoach3('(canbo_id ='.$macanbo.' or user_input = '.$macanbo.')and tuan='.$tuan.' and loai_kh >1','kehoach');
                $list_data = $this->Mdanhmuc->get_kehoach3('canbo_id ='.$macanbo.' and tuan='.$tuan.' and loai_kh =1','kehoach');
                $count_arr[0] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo);
                $count_arr[1] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active = 5");
                $count_arr[2] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active = 5 and (CAST(kehoach.ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01'  OR ngay_han IS NULL OR ngay_hoanthanh IS NULL)");
                $count_arr[3] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active = 5 and  CAST(kehoach.ngay_hoanthanh AS DATE) > ngay_han and ngay_han > '2017-01-01' ");
                $count_arr[4] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active = 5 and chatluong =1 ");
                $count_arr[5] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active = 5 and chatluong =2 ");
                $count_arr[6] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active = 5 and sangtao =2 ");
                $count_arr[7] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active < 5 ");
                $count_arr[8] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active < 5 and (ngay_han >='".$hantuan."' or ngay_han < '2017-01-01' or ngay_han is null or ngay_han='')");
                $count_arr[9] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and canbo_id=".$macanbo." and active < 5 and ngay_han < '".$hantuan."'  and ngay_han >'2017-01-01'");
                
                if($this->_session['iQuyenHan_DHNB']==10 or $this->_session['iQuyenHan_DHNB']==11){

                    $list_data1 = $this->Mdanhmuc->get_kehoach3("((lanhdao_id=".$macanbo." and loai_kh >= 2) or (canbo_id=".$macanbo." and loai_kh > 1)) and tuan=".$tuan.' and loai_kh >1','kehoach');//echo "((lanhdao_id=".$macanbo." and loai_kh = 1) or (canbo_id=".$macanbo." and loai_kh > 1)) and tuan=".$tuan.' and loai_kh >1';
                    $list_data = $this->Mdanhmuc->get_kehoach3("((lanhdao_id=".$macanbo." and loai_kh = 1) or (canbo_id=".$macanbo." and loai_kh > 1)) and tuan=".$tuan.' and loai_kh =1','kehoach');

                    $count_arr[0] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1))");
                    $count_arr[1] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active = 5");
                    $count_arr[2] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active = 5 and (CAST(kehoach.ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01')");
                    $count_arr[3] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active = 5 and  CAST(kehoach.ngay_hoanthanh AS DATE) > ngay_han and ngay_han > '2017-01-01' ");
                    $count_arr[4] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active = 5 and chatluong =1 ");
                    $count_arr[5] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active = 5 and chatluong =2 ");
                    $count_arr[6] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active = 5 and sangtao =2 ");
                    $count_arr[7] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active < 5 ");
                    $count_arr[8] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active < 5 and (ngay_han >='".$hantuan."' or ngay_han < '2017-01-01'  or ngay_han is null or ngay_han='')");
                    $count_arr[9] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and ((lanhdao_id=".$macanbo." and loai_kh  >= 2) or (canbo_id=".$macanbo." and loai_kh >= 1)) and active < 5 and ngay_han < '".$hantuan."'  and ngay_han >'2017-01-01'");                
                }
//điểm thưởng máy tự động chấm
                //nếu quyền cán bộ =8 sẽ bị trừ 1 điểm khi văn bản hoàn thành quá hạn trái lại trừ 0.5 điểm:
                if($quyen_canbo ==8){
                    $hesodiemtru =1;
                }else{
                    $hesodiemtru =0.5;
                }
                // mỗi một công viêc có sáng tạo được công thêm 2 điểm
                if($count_arr[6] >0) $diemthuongmaycham = $count_arr[6]*2;
                else $diemthuongmaycham = 0;
                $check = $count_arr[0] - $count_arr[8];
                if($check >0){
                    $count_arr[10] = round($count_arr[1]/$check*100,2);
                    $phantram = round($count_arr[10]);
                    $arr_chamtudong=(0.6*$phantram) - ($count_arr[3]*$hesodiemtru);
                    
                }else{
                    $count_arr[10] ='100';
                    $phantram = 0;
                    $arr_chamtudong=60;
                }
            }else{
                $cb_idd = $this->uri->segment(3);
                if(empty($cb_idd)) $str_cb_id = '';
                //else $str_cb_id = ' and ( canbo_id ='.$cb_idd.' or user_input = '.$macanbo.' )';
                else $str_cb_id =  ' and ((canbo_id='.$cb_idd.'  OR lanhdao_id = '.$cb_idd.') AND vanban_id IS NULL or (canbo_id='.$cb_idd.' and vanban_id > 0 ))';
               
                $list_data1 = $this->Mdanhmuc->get_kehoach3('phong_id='.$phong_id.' and tuan='.$tuan.'  and loai_kh > 1'.$str_cb_id,'kehoach');
                $list_data = $this->Mdanhmuc->get_kehoach3('phong_id='.$phong_id.' and tuan='.$tuan.'  and loai_kh = 1'.$str_cb_id,'kehoach');
                $count_arr[0] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id.$str_cb_id);
                $count_arr[1] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active = 5".$str_cb_id);
                $count_arr[2] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active = 5 and (CAST(kehoach.ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01' OR ngay_han IS NULL OR ngay_hoanthanh IS NULL)".$str_cb_id);
                $count_arr[3] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active = 5 and  CAST(kehoach.ngay_hoanthanh AS DATE) > ngay_han  and ngay_han > '2017-01-01' ".$str_cb_id);
                $count_arr[4] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active = 5 and chatluong =1 ".$str_cb_id);
                $count_arr[5] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active = 5 and chatluong =2 ".$str_cb_id);
                $count_arr[6] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active = 5 and sangtao =2 ".$str_cb_id);
                $count_arr[7] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active < 5 ".$str_cb_id);
                $count_arr[8] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active < 5 and (ngay_han >='".$hantuan."' or ngay_han < '2017-01-01'  or ngay_han is null or ngay_han='')".$str_cb_id);
                $count_arr[9] = $this->Mdanhmuc->count_kehoach(" tuan=".$tuan." and phong_id=".$phong_id." and active < 5 and ngay_han <'".$hantuan."' and ngay_han >'2017-01-01'".$str_cb_id);
//điểm thưởng máy tự động chấm
                 // mỗi một công viêc có sáng tạo được công thêm 2 điểm
                if($count_arr[6] >0) $diemthuongmaycham = $count_arr[6]*2;
                else $diemthuongmaycham = 0;
                $check = $count_arr[0] - $count_arr[8];
                if($check >0){
                    $count_arr[10] = round($count_arr[1]/$check*100,2);
                    $phantram = round($count_arr[10]);
                    $arr_chamtudong=0.6*$phantram - $count_arr[3];
                    
                }else{
                    $count_arr[10] ='100';
                    $phantram = 0;
                    $arr_chamtudong=60;
                }
            }
            $cb_idd = $this->uri->segment(3);
            if(empty($cb_idd)) $cb_id = $this->_session['PK_iMaCB'];
            else $cb_id = $cb_idd;

            $list_diem = '';
            $list_diem = $this->Mdanhmuc->layDuLieu2('canbo_id',$cb_id,'tuan',$tuan,'danhgia');
            
        }
        
        if($chuyenvien > 0){
            $list_user = $this->Mdanhmuc->layDuLieu('PK_iMaCB',$chuyenvien,'tbl_canbo');
        }else{
            $list_user = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',$phong_id,'tbl_canbo');
        }
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
        }
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }

        $year = date('Y');
        $arr=array();
        for($i=0;$i<53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            
        }  
        $quyen_cb= $this->Mdanhmuc->get_quyen($cb_id);
        $data['tong_vbph_dh'] = $tong_vbph_dh;
        $data['tong_vbph_qh'] = $tong_vbph_qh;
        $data['tong_vbct_dh'] = $tong_vbct_dh;
        $data['tong_vbct_qh'] = $tong_vbct_qh;
        $data['tong_diemtru'] = ($tong_vbct_qh + $tong_vbph_qh)*0.5;

        $data['ds_idctdh'] = $ds_idctdh;
        $data['ds_idctqh'] = $ds_idctqh;
        $data['ds_idphdh'] = $ds_idphdh;
        $data['ds_idphqh'] = $ds_idphqh;

        $data['date_for_monday'] = $date_for_monday;
        $data['date_for_friday'] = $date_for_friday;
        $data['date_for_sunday'] = $date_for_sunday;

        $data['hantuan'] = $hantuan;//pr($hantuan);

        $data['title']    = 'Xem kế hoạch công tác tuần';
        $data['arr']   = $arr; //$arr[$tuan-1];
        $data['count_arr']   = $count_arr;
        $data['list_data']   = $list_data;
        $data['list_data1']   = $list_data1;
        $data['list_diem']   = $list_diem;
        $data['arr_chamtudong']   = $arr_chamtudong;
        $data['diemthuongmaycham']   = $diemthuongmaycham;
        $data['phantram']   = $phantram;
        // pr($list_diem);
        $data['tuan'] = $tuan;
        $data['tuanthemmot'] = $tuan + 1;
        $data['tuan_hientai'] =(int)date("W", strtotime(date('Y-m-d')));
        $data['thu_hientai'] = date('l');
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['cb_id'] = $this->_session['PK_iMaCB'];
        $data['FK_iMaPhongHD'] = $this->_session['FK_iMaPhongHD'];
        $temp['data']     = $data;//pr($data);

        if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacTuan".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacTuan_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vkehoachcongtac_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            if($quyen_cb['iQuyenHan_DHNB']==7){
                $temp['template'] = 'kehoachcongtac/Vxemkehoachcongtac';
            }else{
                $temp['template'] = 'kehoachcongtac/VxemkehoachcongtacCV';
                //$temp['template'] = 'kehoachcongtac/Vkehoachcongtac_word';
            }
            $this->load->view('layout_admin/layout',$temp);
        }
        
    }

    // cá nhân, lãnh đạo chấm điểm
    public function chamdiem()
    {
        if ($this->input->post('luudulieu')) {
            $cb_idd = $this->uri->segment(3);
            if(empty($cb_idd)) $cb_id = $this->_session['PK_iMaCB'];
            else $cb_id = $cb_idd;

            $tuan = $this->uri->segment(2);
            if(date('l')=='Monday' or date('l')=='Tuesday' or date('l')=='Wednesday' or date('l')=='Thursday'){
                $tuan = date('W') - 1 ;
            }else{
                $tuan = date('W');
            }
            if($this->input->post('tuanchamdiem')> 0) $tuan = $this->input->post('tuanchamdiem');
            $diem2 = (float)_post('diem3')+(float)_post('diem4')+(float)_post('diem5')+(float)_post('diem6')+(float)_post('diem7')+(float)_post('diem8')+(float)_post('diem9');

            $diem_2 = (float)_post('diem_3')+(float)_post('diem_4')+(float)_post('diem_5')+(float)_post('diem_6')+(float)_post('diem_7')+(float)_post('diem_8');

            $diem10 = (float)_post('diem11')+(float)_post('diem12')+(float)_post('diem13')+(float)_post('diem14')+(float)_post('diem15')+(float)_post('diem16')+(float)_post('diem17')+(float)_post('diem18');
            
            if(_post('diem_18') > 0)$diem_18 =(float)_post('diem_18');
            else $diem_18 =0.0;
            $diem_10 = (float)_post('diem_11')+(float)_post('diem_12')+(float)_post('diem_13')+(float)_post('diem_14')+(float)_post('diem_15')+(float)_post('diem_16')+(float)_post('diem_17') +(float)$diem_18;//+(float)_post('diem_18');


             $diemtong = (float)$diem2 + (float)$diem10 + (float)_post('diem1') + (float)_post('diem19');
             $diem_tong = (float)$diem_2 + (float)$diem_10 + (float)_post('diem_1') + (float)_post('diem_19');

             $diem_2_tp = (float)_post('diem_3_tp')+(float)_post('diem_4_tp')+(float)_post('diem_5_tp')+(float)_post('diem_6_tp')+(float)_post('diem_7_tp')+(float)_post('diem_8_tp');

             if(_post('diem_18_tp') > 0)$diem_18_tp = (float)_post('diem_18_tp');
             else $diem_18_tp = 0.0;
             $diem_10_tp = (float)_post('diem_11_tp')+(float)_post('diem_12_tp')+(float)_post('diem_13_tp')+(float)_post('diem_14_tp')+(float)_post('diem_15_tp')+(float)_post('diem_16_tp')+(float)_post('diem_17_tp')+(float)$diem_18_tp;

             $diem_tong_tp = (float)$diem_2_tp + (float)$diem_10_tp + (float)_post('diem_1_tp') + (float)_post('diem_19_tp');

            /*if(date('l')=='Monday'){
                $tuan1 = (int)date("W") - 1;
            }else{
                $tuan1 = (int)date("W");
            }*/

            $diem_cv = array(
                'canbo_id' => $cb_id,
                'phong_id' => $this->_session['FK_iMaPhongHD'],
                'diem_1' => _post('diem_1'),
                'diem_2' => $diem_2,
                'diem_3' => _post('diem_3'),
                'diem_4' => _post('diem_4'),
                'diem_5' => _post('diem_5'),
                'diem_6' => _post('diem_6'),
                'diem_7' => _post('diem_7'),
                'diem_8' => _post('diem_8'),
                'diem_9' => 0,
                'diem_10' => $diem_10,
                'diem_11' => _post('diem_11'),
                'diem_12' => _post('diem_12'),
                'diem_13' => _post('diem_13'),
                'diem_14' => _post('diem_14'),
                'diem_15' => _post('diem_15'),
                'diem_16' => _post('diem_16'),
                'diem_17' => _post('diem_17'),
                'diem_18' => _post('diem_18'),
                'diem_19' => _post('diem_19'),
                'diem_tong' => $diem_tong,
                'diem1' => _post('diem1'),
                'diem2' => $diem2,
                'diem3' => _post('diem3'),
                'diem4' => _post('diem4'),
                'diem5' => _post('diem5'),
                'diem6' => _post('diem6'),
                'diem7' => _post('diem7'),
                'diem8' => _post('diem8'),
                'diem9' => _post('diem9'),
                'diem10' => $diem10,
                'diem11' => _post('diem11'),
                'diem12' => _post('diem12'),
                'diem13' => _post('diem13'),
                'diem14' => _post('diem14'),
                'diem15' => _post('diem15'),
                'diem16' => _post('diem16'),
                'diem17' => _post('diem17'),
                'diem18' => $diem18,//_post('diem18'),
                'diem19' => _post('diem19'),
                'diemtong' => $diemtong,
                'active'  => 1,
                'tuan'   => $tuan,
                'thang'   => date('m'),
                'ngay_cham' => date('Y-m-d H:i:s'),
                'ngay_cv_cham'=> date('Y-m-d H:i:s')
                        
            );
            if($this->_session['iQuyenHan_DHNB']<7){
               $diem_cv = array(
                'canbo_id' => $cb_id,
                'phong_id' => $this->_session['FK_iMaPhongHD'],
                'diem_1_tp' => _post('diem_1_tp'),
                'diem_2_tp' => $diem_2_tp,
                'diem_3_tp' => _post('diem_3_tp'),
                'diem_4_tp' => _post('diem_4_tp'),
                'diem_5_tp' => _post('diem_5_tp'),
                'diem_6_tp' => _post('diem_6_tp'),
                'diem_7_tp' => _post('diem_7_tp'),
                'diem_8_tp' => _post('diem_8_tp'),
                'diem_9_tp' => 0.0,
                'diem_10_tp' => $diem_10_tp,
                'diem_11_tp' => _post('diem_11_tp'),
                'diem_12_tp' => _post('diem_12_tp'),
                'diem_13_tp' => _post('diem_13_tp'),
                'diem_14_tp' => _post('diem_14_tp'),
                'diem_15_tp' => _post('diem_15_tp'),
                'diem_16_tp' => _post('diem_16_tp'),
                'diem_17_tp' => _post('diem_17_tp'),
                'diem_18_tp' => $diem_18_tp,
                'diem_19_tp' => _post('diem_19_tp'),
                'diem_tong_tp' => $diem_tong_tp,
                'active'  => 2,
                'tuan'   => $tuan,
                'thang'   => date('m'),
                'ngay_cham' => date('Y-m-d H:i:s'),
                'nhan_xet' => _post('nhan_xet')
                        
            ); 
            }
            if($this->_session['iQuyenHan_DHNB']==7 && $cb_id != $this->_session['PK_iMaCB']){
                $diem_cv = array(
                'phophong_id' => $this->_session['PK_iMaCB'],
                'ngay_nhanxet' => date('Y-m-d H:i:s'),
                'nhan_xet_1' => _post('nhan_xet_1'));
            }

// khi chấm điểm xong: sẽ tự chuyển việc chưa hoàn thành sang tuần tới
if($this->_session['iQuyenHan_DHNB'] > 6 && $cb_id == $this->_session['PK_iMaCB']){
            $week = $tuan+1;
            /*if(date('l')=='Monday'){$week = $tuan;}
            if(date('l')=='Sunday'){$week = $tuan;}*/
            $week_1 = $week-1;
            $check_data ='';
            $check_data = $this->Mdanhmuc->layDuLieu2('tuan',$week,'canbo_id',$cb_id,'kehoach');//echo $week;
            //lấy chuỗi kh_id_sub trong tuần tiếp theo
            $str_subID='0';
            foreach ($check_data as $key => $value) {
                if($value['kh_id_sub'] >0){
                    $str_subID .= ','.$value['kh_id_sub'];
                }
            }
//echo 'tuan = '.$week_1.' and (active < 5  or  loai_kh = 3) and canbo_id ='.$cb_id.' and kh_id not in('.$str_subID.')';
            $giatri = $this->Mdanhmuc->get_kehoach3('tuan = '.$week_1.' and (active < 5  or  loai_kh = 3) and canbo_id ='.$cb_id.' and kh_id not in('.$str_subID.')','kehoach');//pr($giatri);
            foreach ($giatri as $key => $value) {
                $value['kh_id_sub'] = $value['kh_id'];
                $kh_id = $value['kh_id'];
                unset($value['kh_id']);
                $value['tuan'] = $week;
                if($value['loai_kh'] == 3){
                    $value['active'] = 1;
                    $value['ykien_tp'] = '';
                    $value['ngay_ykien'] = '0000-00-00';
                    $value['ykien_pp'] = '';
                    $value['ket_qua_hoanthanh'] = '';
                    $value['ngay_hoanthanh'] = '0000-00-00';
                    $value['ngay_danhgia'] = '0000-00-00';
                     $value['danh_gia'] = '';

                }
                $this->Mdanhmuc->themDuLieu('kehoach',$value);
                // update lại trường qua_han
                if($value['ngay_han'] < date('Y-m-d') && $value['ngay_han'] >'2017-01-01'){
                    $this->Mdanhmuc->quahan($kh_id,$week_1);
                }
            }
}
            
            $this->list_diem = $diem_cv;
            //thêm mới dữ liệu
            $danhgia_id = $this->uri->segment(4);
            $check_data = $this->Mdanhmuc->layDuLieu2('tuan',$tuan,'canbo_id',$cb_id,'danhgia');
            // pr($week);
            if(empty($check_data)){
                $check_insert = $this->Mdanhmuc->themDuLieu('danhgia',$diem_cv);
                redirect("dskehoachcongtac/".$tuan);
            }else{
                $danhgia_id = $check_data[0]['danhgia_id'];
                if($this->_session['iQuyenHan_DHNB']==7 && (_post('nhan_xet_1')) != ''){
                    $taikhoan   = $this->_session['PK_iMaCB'];
                    $tuan       = $this->uri->segment(2);
                    $chuyenvien = $this->uri->segment(3);
                    $id_danhgia = $this->uri->segment(4);
                    $hoten      = $this->_session['sHoTen'];
                    $thoigan    = date('Y-m-d H:i:s');
                    $nhanxet    = _post('nhan_xet_1');
                    $mangnhanxet = array(
                        'FK_iMaCB_PP' => $taikhoan,
                        'FK_iMaCB_CV' => $chuyenvien,
                        'tuan'        => $tuan,
                        'id_danhgia'  => $id_danhgia,
                        'sNhanXet'    => $nhanxet,
                        'sThoiGian'   => $thoigan
                    );
                    $noidungnhanxet = $check_data[0]['nhan_xet_1'];
                    $kiemtra =  $this->Mkehoachcongtac->kiemtra($taikhoan,$chuyenvien,$tuan,$id_danhgia);
                    if($kiemtra>0)
                    {
                        $noidungnhanxet_cu = $this->Mkehoachcongtac->lay($taikhoan,$chuyenvien,$tuan,$id_danhgia);
                        $nhanxetcu = '<b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.$noidungnhanxet_cu['sThoiGian'].')</i><br/>'.$noidungnhanxet_cu['sNhanXet'].'';
                        $noidungdachinhsua = trim(str_replace($nhanxetcu,'',$noidungnhanxet));
                        $nhanxet_danhgia=array(
                            'nhan_xet_1' => $noidungdachinhsua.'<br/><b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.$thoigan.')</i><br/>'.$nhanxet.''
                        );
                        $check_insert = $this->Mdanhmuc->capnhatDuLieu('danhgia_id',$danhgia_id,'danhgia',$nhanxet_danhgia);
                        $this->Mkehoachcongtac->capnhat($taikhoan,$chuyenvien,$tuan,$id_danhgia,$mangnhanxet);
                    }
                    else{
                        $this->Mkehoachcongtac->them($taikhoan,$chuyenvien,$tuan,$id_danhgia,$mangnhanxet);
                        if(!empty($noidungnhanxet))
                        {
                            $nhanxet_danhgia=array(
                                'nhan_xet_1' => $noidungnhanxet.'<br/><b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.$thoigan.')</i><br/>'.$nhanxet.''
                            );
                        }
                        else{
                             $nhanxet_danhgia=array(
                                'nhan_xet_1' => $noidungnhanxet.'<b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.$thoigan.')</i><br/>'.$nhanxet.''
                            );
                        }
                        $check_insert = $this->Mdanhmuc->capnhatDuLieu('danhgia_id',$danhgia_id,'danhgia',$nhanxet_danhgia);
                    }
                }
                else{
                    $check_insert = $this->Mdanhmuc->capnhatDuLieu('danhgia_id',$danhgia_id,'danhgia',$diem_cv);
                }
                // tự động cập nhật điểm trung bình tháng khi TP chấm điểm tuần
                if($this->_session['iQuyenHan_DHNB']==6 || $this->_session['iQuyenHan_DHNB']==3){
                    $month = $check_data[0]['thang'];
                    $year = date('Y');
                    $beg = (int) date('W', strtotime("first thursday of $year-$month"));
                    $end = (int) date('W', strtotime("last  thursday of $year-$month"));
                        
                    $canbo_id =$check_data[0]['canbo_id'];
                    $arrData = $this->Mdanhmuc->layDuLieu2('id_cb',$canbo_id,'thang',$month,'danhgiathang');            
                    if(!empty($arrData)){
                        $diemtuan = array();
                        $j=0;$tongdiem=0;
                        for($i=$beg;$i<=$end;$i++){
                            $diemtuan[$i] = $this->Mdanhmuc->count_diem("diem_tong_tp"," tuan =".$i." and canbo_id=".$canbo_id);
                            $tongdiem += (float)$diemtuan[$i]['diem_tong_tp'];
                            $j++;
                        }
                        $tong_diem_thang = round($tongdiem/$j,1);
                        $mangcapnhatdiem = array("diem" => $tong_diem_thang);

                        $check_insert = $this->Mdanhmuc->capnhatDuLieu('id',$arrData[0]['id'],'danhgiathang',$mangcapnhatdiem);

                    }
                }
                redirect("dskehoachcongtac/".$tuan);
            }
            
            if ($check_insert > 0) {
                $this->_giatri ='';
                return messagebox('Thêm thành công ', 'info');
            } else {
                return messagebox('Thêm thất bại', 'danger');
            } 
            
        }
    }
 
}
    