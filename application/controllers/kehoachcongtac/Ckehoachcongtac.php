<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckehoachcongtac extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		$this->Mkehoachcongtac = new Mkehoachcongtac();        
	    $this->load->model('vanbandi/Mtruyennhan');      
    }
    public function index()
    {     
        $canbo           = $this->Mtruyennhan->layDSCB();
        $data['dscanbo'] = array_column($canbo, 'sHoTen','PK_iMaCB');
        $loccanbo =''; $idcbloc=0; 
        $action = _post('action');
        if($action=='capnhat')
        {
            $this->capnhat();
        }
        if($action =='danhgia')
        {
            $this->danhgia();
        }
        if(_post('luuppchidao')){
            $data['content'] = $this->ppchidao();
        }
        if(_post('luuchuahoanthanh')){
            $data['content'] = $this->chuahoanthanh();
        }
        if(_post('luutruongphongdanhgia')){
            $data['content'] = $this->tpdanhgia();
        }
        if(_post('tpduyet')){
            $data['content'] = $this->tpduyet();
        }
        if(_post('tpduyetrahan')){
            $data['content'] = $this->tpduyetrahan();
        }
        if(_post('ppduyet')){
            $data['content'] = $this->ppduyet();
        }
        if(_post('xoabanghi') >0){
            $makh=_post('xoabanghi');
            $this->Mdanhmuc->xoaDuLieu('kh_id',$makh,'kehoach');
        }

        //duyệt nhiều kế hoạch
        if(_post('string_kh_id') != ''){
            $string_kh_id=_post('string_kh_id');
            $string_active=_post('string_active');

            $arr_kh_id = explode(',',$string_kh_id);
            $arr_active = explode(',',$string_active);
            for($i=1;$i<count($arr_kh_id);$i++){
                if($arr_active[$i]==4){
                    $data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 5,"chatluong"=>1,"sangtao"=>1,'danh_gia'=>'Hoàn thành công việc');
                    $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$arr_kh_id[$i],"kehoach",$data_update);
                }else{
                    $data_update = array("ngay_danhgia"=>date('Y-m-d H:i:s'),"active" => 3,'ykien_tp'=>'Đồng ý với đề xuất');
                    $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$arr_kh_id[$i],"kehoach",$data_update);
                }
            }
           // redirect("kehoachcongtac");
           // header("Refresh:0");
        }

        if($this->uri->segment(3) >0) $phong_idkh = $this->uri->segment(3);
        else $phong_idkh = $this->_session['FK_iMaPhongHD'];
        if($this->uri->segment(2) >0) $week = $this->uri->segment(2);

        $week = (int)date("W", strtotime(date('Y-m-d'))); if($week == 1 and date('Y') < '2020') $week = 53;
        if(_post('tuan')>0 && !isset($_POST['luudulieu'])) $week = _post('tuan');
        
         // so sánh nếu tuần hiện tại lớn hơn tuần post dữ liệu
        if($week < (int)date("W", strtotime(date('Y-m-d'))))$kiemtratuan = 1;
        else $kiemtratuan = 2;


        
        $canbo = $this->Mdanhmuc->get_kehoach3('FK_iMaPhongHD = '.$phong_idkh.' and iTrangThai = 0 and PK_iMaCB != 678 and PK_iMaCB != 617','tbl_canbo');

        $dscanbo ='';
        foreach ($canbo as $key => $ma_cb) {
            $dscanbo .= $ma_cb['PK_iMaCB'].',';
        }
        $dscanbo = rtrim($dscanbo, ",");
        if($this->_session['iQuyenHan_DHNB']>=7){
            $dscanbo = $this->_session['PK_iMaCB'];
        }
//echo $dscanbo;
        if($this->input->post('tencanbo')){
            $week = ($this->input->post('tuanpost'));
            $idcbloc=$this->input->post('tencanbo');
            $loccanbo = ' and canbo_id = '.$idcbloc;
            $dscanbo = $idcbloc;
        }
        if(_get('canbo_id')>0 && empty($_POST)){
            $idcbloc=_get('canbo_id');
            $week = date('W');
            $loccanbo = ' and canbo_id = '.$idcbloc;
            $dscanbo = $idcbloc;
        }

        if($this->input->post('trangthaitp') >0 || _get('trangthai')>0 ){
            $week = ($this->input->post('tuanpost'));
            if(empty($week))$week= date('W');
            $active ='1,2,4';
            $trangthai = ' and active in('.$active.')';
            $active_tt = 1;
        }else{
            $trangthai = '';
            $active_tt = 0;
        }

        $year = date ('Y');
        $year = $_SESSION['nam'];
        $week_current = $week - 1;
        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week_current * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
        $date_for_friday = date( 'Y-m-d', $timestamp_for_monday + 5 * 24 * 60 * 60 );
        $date_for_sunday = date( 'Y-m-d', $timestamp_for_monday + 7 * 24 * 60 * 60 );
        //echo $date_for_monday." - ".$date_for_sunday;
        if($this->_session['iQuyenHan_DHNB']==6 or $this->_session['iQuyenHan_DHNB']==3 or $this->_session['iQuyenHan_DHNB']==8 or $this->_session['iQuyenHan_DHNB']==4 or $this->_session['iQuyenHan_DHNB']==5 or $this->_session['iQuyenHan_DHNB']==7 or $this->_session['iQuyenHan_DHNB']==10 or $this->_session['iQuyenHan_DHNB']==11){
            $vbct_dh = $this->Mdanhmuc->get_kehoach33('PK_iMaCBHoanThanh in ('.$dscanbo.') and sNgayGiaiQuyet >= "'.$date_for_monday.'" and sNgayGiaiQuyet < "'.$date_for_sunday.'" and (sNgayGiaiQuyet <= sHanThongKe or sHanThongKe < "1999-12-12" or sHanThongKe is null ) and (vbdaura_tp < 1 or vbdaura_tp IS NULL )','tbl_vanbanden','PK_iMaVBDen');

            $ds_idctdh ='0,';
            foreach ($vbct_dh as $key => $ds_ctdh) {
                $ds_idctdh = $ds_idctdh.$ds_ctdh['PK_iMaVBDen'].',';
            }
            if($ds_idctdh!='')$ds_idctdh = rtrim($ds_idctdh, ",");

            $vbct_qh = $this->Mdanhmuc->get_kehoach33('PK_iMaCBHoanThanh in ('.$dscanbo.') and sNgayGiaiQuyet >= "'.$date_for_monday.'" and sNgayGiaiQuyet < "'.$date_for_sunday.'" and (sNgayGiaiQuyet > sHanThongKe and sHanThongKe > "1999-12-12") and  (vbdaura_tp < 1 or vbdaura_tp IS NULL )','tbl_vanbanden','PK_iMaVBDen');

            $ds_idctqh ='0,';
            foreach ($vbct_qh as $key => $ds_ctqh) {
                $ds_idctqh = $ds_idctqh.$ds_ctqh['PK_iMaVBDen'].',';
            }
            if($ds_idctqh!='')$ds_idctqh = rtrim($ds_idctqh, ",");

            if(count($vbct_dh)>0) $tong_vbct_dh = count($vbct_dh);
            else $tong_vbct_dh = 0;
            if(count($vbct_qh)>0) $tong_vbct_qh = count($vbct_qh);
            else $tong_vbct_qh = 0;

            $vbph_dh = $this->Mdanhmuc->get_kehoach33('PK_iMaCB in ('.$dscanbo.') and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and (sThoiGianHoanThanh <= sThoiGianHetHan or sThoiGianHetHan < "1999-12-12" or sThoiGianHetHan is null )','tbl_phongphoihop','PK_iMaVBDen');

            $ds_idphdh ='0,';
            foreach ($vbph_dh as $key => $ds_phdh) {
                $ds_idphdh = $ds_idphdh.$ds_phdh['PK_iMaVBDen'].',';
            }
            if($ds_idphdh!='')$ds_idphdh = rtrim($ds_idphdh, ",");


            $vbph_qh = $this->Mdanhmuc->get_kehoach33('PK_iMaCB in ('.$dscanbo.') and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and sThoiGianHoanThanh > sThoiGianHetHan and sThoiGianHetHan > "1999-12-12" ','tbl_phongphoihop','PK_iMaVBDen');
//pr($vbph_qh);
            $ds_idphqh ='0,';
            foreach ($vbph_qh as $key => $ds_phqh) {
                $ds_idphqh = $ds_idphqh.$ds_phqh['PK_iMaVBDen'].',';
            }
            if($ds_idphqh!='')$ds_idphqh = rtrim($ds_idphqh, ",");

            if(count($vbph_dh)>0) $tong_vbph_dh = count($vbph_dh);
            else $tong_vbph_dh = 0;
            if(count($vbph_qh)>0) $tong_vbph_qh = count($vbph_qh);
            else $tong_vbph_qh = 0;

        }/*else if($this->_session['iQuyenHan_DHNB']==7 or $this->_session['iQuyenHan_DHNB']==10 or $this->_session['iQuyenHan_DHNB']==11 ){
            $tong_vbct_dh = 0;
            $tong_vbct_qh = 0;
            $str_idvd = '0,';
            $dsvbct = $this->Mdanhmuc->get_kehoach33('sNgayGiaiQuyet >= "'.$date_for_monday.'" and sNgayGiaiQuyet < "'.$date_for_sunday.'"  and (vbdaura_tp < 1 or vbdaura_tp IS NULL )','tbl_vanbanden','PK_iMaVBDen');
            foreach ($dsvbct as $key => $idvbd) {
                $str_idvd = $str_idvd.$idvbd['PK_iMaVBDen'].',';
            }
            if($str_idvd != ''){
                $str_idvd = rtrim($str_idvd, ",");
                $list_ppvbct = $this->Mdanhmuc->get_kehoach33('PK_iMaVBDen in('.$str_idvd.') and PK_iMaCVCT = '.$dscanbo.'','tbl_chuyennhanvanban','PK_iMaVBDen');
                $str_pp_vbd ='0,';
                foreach ($list_ppvbct as $key => $idvbdpp) {
                    $str_pp_vbd = $str_pp_vbd.$idvbdpp['PK_iMaVBDen'].',';
                }
                $str_pp_vbd = rtrim($str_pp_vbd, ",");

                $vbct_dh = $this->Mdanhmuc->get_kehoach33('PK_iMaVBDen in ('.$str_pp_vbd.') and (sNgayGiaiQuyet <= sHanThongKe or sHanThongKe < "1999-12-12" or sHanThongKe is null ) and  (vbdaura_tp < 1 or vbdaura_tp IS NULL )','tbl_vanbanden','PK_iMaVBDen');

                $ds_idctdh ='0,';
                foreach ($vbct_dh as $key => $ds_ctdh) {
                    $ds_idctdh = $ds_idctdh.$ds_ctdh['PK_iMaVBDen'].',';
                }
                if($ds_idctdh!='')$ds_idctdh = rtrim($ds_idctdh, ",");

                $vbct_qh = $this->Mdanhmuc->get_kehoach33('PK_iMaVBDen in ('.$str_pp_vbd.') and (sNgayGiaiQuyet > sHanThongKe and sHanThongKe > "1999-12-12")','tbl_vanbanden','PK_iMaVBDen');

                $ds_idctqh ='0,';
                foreach ($vbct_qh as $key => $ds_ctqh) {
                    $ds_idctqh = $ds_idctqh.$ds_ctqh['PK_iMaVBDen'].',';
                }
                if($ds_idctqh!='')$ds_idctqh = rtrim($ds_idctqh, ",");

                $tong_vbct_dh = count($vbct_dh);
                $tong_vbct_qh = count($vbct_qh);
            }


            $vbph_dh = $this->Mdanhmuc->get_kehoach33('input_per = '.$dscanbo.'  and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and (sThoiGianHoanThanh <= sThoiGianHetHan or sThoiGianHetHan < "1999-12-12" or sThoiGianHetHan is null )','tbl_phongphoihop','PK_iMaVBDen');

            $ds_idphdh ='';
            foreach ($vbph_dh as $key => $ds_phdh) {
                $ds_idphdh = $ds_idphdh.$ds_phdh['PK_iMaVBDen'].',';
            }
            if($ds_idphdh!='')$ds_idphdh = rtrim($ds_idphdh, ",");

            $vbph_qh = $this->Mdanhmuc->get_kehoach33('input_per = '.$dscanbo.'  and sThoiGianHoanThanh >= "'.$date_for_monday.'" and sThoiGianHoanThanh < "'.$date_for_sunday.'"  and sThoiGianHoanThanh > sThoiGianHetHan and sThoiGianHetHan > "1999-12-12" ','tbl_phongphoihop','PK_iMaVBDen');

            $ds_idphqh ='0,';
            foreach ($vbph_qh as $key => $ds_phqh) {
                $ds_idphqh = $ds_idphqh.$ds_phqh['PK_iMaVBDen'].',';
            }
            if($ds_idphqh!='')$ds_idphqh = rtrim($ds_idphqh, ",");
    
            if(count($vbph_dh) > 0)$tong_vbph_dh = count($vbph_dh);
            else $tong_vbph_dh = 0;
            if(count($vbph_qh) > 0)$tong_vbph_qh = count($vbph_qh);
            else $tong_vbph_qh = 0;
        }*/

//echo $phong_idkh;
        $list_kh = $this->Mdanhmuc->get_kehoach3('phong_id ='.$phong_idkh.' and tuan='.$week.' and loai_kh > 1   and vanban_id is null  '.$loccanbo.$trangthai,'kehoach','canbo_id,active');

        $list_kh1 = $this->Mdanhmuc->get_kehoach3('phong_id ='.$phong_idkh.' and tuan='.$week.' and loai_kh = 1'.$loccanbo,'kehoach','canbo_id');

        if($this->_session['iQuyenHan_DHNB']==7 or $this->_session['iQuyenHan_DHNB']==10 or $this->_session['iQuyenHan_DHNB']==11){
    		if($this->_session['iQuyenHan_DHNB']==7){
            	$list_kh = $this->Mdanhmuc->get_kehoach3('(canbo_id = '.$dscanbo.' or lanhdao_id ='.$dscanbo.' or phoihop like "%['.$dscanbo.']%") and tuan='.$week.' and loai_kh > 1   and vanban_id is null '.$loccanbo.$trangthai,'kehoach');

                if($this->_session['PK_iMaCB']==156 )
                    $list_kh = $this->Mdanhmuc->get_kehoach3('(canbo_id = '.$dscanbo.' or lanhdao_id ='.$dscanbo.' or phoihop like "%['.$dscanbo.']%") and tuan='.$week.' and loai_kh > 1   and vanban_id is null  and (active in(0,1,2,4) or user_input = 156 )'.$loccanbo.$trangthai,'kehoach');

            	$list_kh1 = $this->Mdanhmuc->get_kehoach3('canbo_id ='.$dscanbo.' and tuan='.$week.' and loai_kh = 1'.$loccanbo,'kehoach');//echo 'canbo_id ='.$dscanbo.' and tuan='.$week.' and loai_kh = 1';
    		}
    		if( $this->_session['iQuyenHan_DHNB']==10 or $this->_session['iQuyenHan_DHNB']==11){
    			$list_kh = $this->Mdanhmuc->get_kehoach3('(canbo_id = '.$dscanbo.' or lanhdao_id ='.$dscanbo.' or phoihop like "%['.$dscanbo.']%") and tuan='.$week.' and loai_kh > 1  and vanban_id is null '.$loccanbo.$trangthai,'kehoach');
            	//$list_kh1 = $this->Mdanhmuc->get_kehoach3('(canbo_id ='.$dscanbo.' OR lanhdao_id='.$dscanbo.') and tuan='.$week.' and loai_kh = 1'.$loccanbo,'kehoach');
                $list_kh1 = $this->Mdanhmuc->get_kehoach3(' canbo_id ='.$dscanbo.' and tuan='.$week.' and loai_kh = 1'.$loccanbo,'kehoach');
    		}
        }

        if($this->_session['iQuyenHan_DHNB']==8){
            //$canbo = $this->Mdanhmuc->layDuLieu2('PK_iMaCB',$dscanbo,'iTrangThai',0,'tbl_canbo');
            $list_kh = $this->Mdanhmuc->get_kehoach3('(canbo_id ='.$dscanbo.' or phoihop like "%['.$dscanbo.']%") and tuan='.$week.' and loai_kh > 1  and vanban_id is null and active >= 0 ','kehoach');
            $list_kh1 = $this->Mdanhmuc->get_kehoach3('canbo_id ='.$dscanbo.' and tuan='.$week.' and loai_kh = 1','kehoach');
        }

        foreach ($list_kh as $key => $value) {
            
            if($value['phoihop']!=''){
                $file_phoihop = $this->Mdanhmuc->getfile1($value['kh_id'],'kehoach_file');
                $list_kh[$key]['file_phoihop']= $file_phoihop; 
                
            }

            $file_name='';
            $file_name = $this->Mdanhmuc->getfile($value['kh_id'],'kehoach_file');
            if($file_name !=''){
                $array_file = explode(';',$file_name['file_upload']);
                $arr_temp  = array();
                foreach ($array_file as $key1 => $value1) {
                    array_push($arr_temp,  $value1);
                }
                $list_kh[$key]['filename']=$arr_temp;unset($arr_temp);
                
            }

            if($list_kh[$key]['canbo_id']==$this->_session['PK_iMaCB']){
                $list_kh[$key]['chutri']= 1;
            }else{
                $list_kh[$key]['chutri']= 2;
            }
            
        }

        $array_cb= array();
        $truongdonvi ='';
        foreach ($canbo as $key_cb => $value_cb) {
           $array_cb[$value_cb['PK_iMaCB']]=$value_cb['sHoTen'];
           if($value_cb['iQuyenHan_DHNB']==6 or $value_cb['iQuyenHan_DHNB']==3 )$truongdonvi=$value_cb['PK_iMaCB'];
        }

        $giatri ='';
        if(_get('id') > 0){
            $kehoach_ma = _get('id');
            $giatri = $this->Mdanhmuc->layDuLieu('kh_id',$kehoach_ma,'kehoach');
        }else{
            $kehoach_ma =0;
        }

        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->themKeHoach();
        }	
        $year = date('Y');
        $arr=array();
        for($i=0;$i<53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            
        }
        $data['tong_vbph_dh'] = $tong_vbph_dh;
        $data['tong_vbph_qh'] = $tong_vbph_qh;
        $data['tong_vbct_dh'] = $tong_vbct_dh;
        $data['tong_vbct_qh'] = $tong_vbct_qh;

        $data['ds_idctdh'] = $ds_idctdh;
        $data['ds_idctqh'] = $ds_idctqh;
        $data['ds_idphdh'] = $ds_idphdh;
        $data['ds_idphqh'] = $ds_idphqh;

        $data['date_for_monday'] = $date_for_monday;
        $data['date_for_friday'] = ($week > (int)date("W", strtotime(date('Y-m-d'))))?$date_for_monday:$date_for_friday;
        if($week == (int)date("W", strtotime(date('Y-m-d')))){
            $data['date_for_friday'] = date('Y-m-d');
        }
        $data['date_for_sunday'] = $date_for_sunday;

        $data['kiemtratuan'] = $kiemtratuan;

        $data['array_cb'] = $array_cb;
        $data['giatri'] = $giatri;
        $data['title']    = 'Nhập kế hoạch công tác';
        $data['arr']   = $arr;
        $data['tuan_ct']   = $arr[$week-1];
        $data['week'] = $week;
        $data['week_hientai'] = (int)date("W", strtotime(date('Y-m-d')));
        $data['thu_hientai'] = date('l');
        $data['canbo'] = $canbo;//pr($canbo);
        $data['idcbloc'] = $idcbloc;
        $data['active_tt'] = $active_tt;
        $data['truongdonvi'] = $truongdonvi;
        $data['list_kh'] = $list_kh;//pr($list_kh);
        $data['list_kh1'] = $list_kh1;
        $data['kehoach_ma'] = $kehoach_ma;
        $data['iQuyenHan_DHNB'] = $this->_session['iQuyenHan_DHNB'];
        $data['PK_iMaCB'] = $this->_session['PK_iMaCB'];
        $data['FK_iMaPhongHD'] = $this->_session['FK_iMaPhongHD'];
        $temp['data']     = $data;
        
        
       
        $temp['template'] = 'kehoachcongtac/Vkehoachcongtac';
        $this->load->view('layout_admin/layout',$temp);
	}

    public function ppchidao(){
        $kehoach_id      = _post('kehoach_id');
        $canbo_id        = _post('pp_canbo_id');
        $phoihop         = _post('list_cvph');
        $pp_chidao       = _post('pp_chidao');
        $pp_ngaygiao     = date('Y-m-d H:i:s');
        $arrayUpdate = array('kh_id' => $kehoach_id,'pp_chidao' => $pp_chidao, 'canbo_id' =>$canbo_id,'lanhdao_id' => $this->_session['PK_iMaCB'],'pp_ngaygiao' => $pp_ngaygiao);
        $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$kehoach_id,"kehoach",$arrayUpdate);
        return messagebox('Thêm thành công ', 'info');
    }
    
    public function tpduyet(){
        $id = _post('kehoach_id');
        $giatri = $this->Mdanhmuc->layDuLieu('kh_id',$id,'kehoach')[0];//pr($giatri['lanhdao_id']);
        $trangthai = _post('trangthai');
        $lydo = _post('lydo');
        if($this->_session['iQuyenHan_DHNB'] == 7 or $this->_session['iQuyenHan_DHNB'] == 11){
            if($trangthai == 2)$active=0;
            else $active=2;
            $data_update = array("ngay_ykien"=>date('Y-m-d H:i:s'),"active" => $active,'ykien_pp'=>$lydo);
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
        }
        if($this->_session['iQuyenHan_DHNB'] == 6 or $this->_session['iQuyenHan_DHNB'] == 3){
            if($trangthai == 2){
                if($giatri['lanhdao_id'] != $this->_session['PK_iMaCB'] && $giatri['lanhdao_id'] == $giatri['user_input'])$active=2;
                else $active=0;
            }else $active=3;
            $data_update = array("ngay_danhgia"=>date('Y-m-d H:i:s'),"active" => $active,'ykien_tp'=>$lydo);
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
        } 

        $tencanbo_id = _post('tencanbo');
        $trangthai_active = _post('trangthai_active');            
        if($tencanbo_id > 0){
            $tencanbo ="?canbo_id=".$tencanbo_id;
        }else{
            $tencanbo ="";
        }
        if($trangthai_active > 0){
            $trangthai = "?trangthai=".$trangthai_active;
            if($tencanbo !='')$trangthai = "&trangthai=".$trangthai_active;
        }else{
            $trangthai = "";
        }
        redirect("kehoachcongtac".$tencanbo.$trangthai);
    }

     public function tpduyetrahan(){
        $id = _post('kehoach_id');
        $giatri = $this->Mdanhmuc->layDuLieu('kh_id',$id,'kehoach')[0];//pr($giatri['lanhdao_id']);
        $trangthai = _post('trangthai');
        $lydorahan = _post('lydorahan');
        $var = _post('ngayhanmoi');
        $date = str_replace('/', '-', $var);
        $ngayhanmoi = date('Y-m-d', strtotime($date));
        if($ngayhanmoi > '2018-01-01'){
            $data_update = array("ngayrahanmoi"=>date('Y-m-d H:i:s'),"lydorahan" => $lydorahan,'ngayhanmoi'=>$ngayhanmoi,'ngay_han'=>$ngayhanmoi);
        }else{
            $data_update = array("ngayrahanmoi"=>date('Y-m-d H:i:s'),"lydorahan" => $lydorahan);
        }      
        
        $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);

        $tencanbo_id = _post('tencanbo');
        $trangthai_active = _post('trangthai_active');            
        if($tencanbo_id > 0){
            $tencanbo ="?canbo_id=".$tencanbo_id;
        }else{
            $tencanbo ="";
        }
        if($trangthai_active > 0){
            $trangthai = "?trangthai=".$trangthai_active;
            if($tencanbo !='')$trangthai = "&trangthai=".$trangthai_active;
        }else{
            $trangthai = "";
        }
        redirect("kehoachcongtac".$tencanbo.$trangthai);
    }

    public function ppduyet(){
        $id = _post('kehoach_id');
        $giatri = $this->Mdanhmuc->layDuLieu('kh_id',$id,'kehoach')[0];//;
        $ketluan_pp = _post('ketluan_pp') .' - '.date('d/m/Y H:i:s');
        
        $data_update = array('ketluan_pp'=>$ketluan_pp);
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
          
        $tencanbo_id = _post('tencanbo');           
        if($tencanbo_id > 0){
            $tencanbo ="?canbo_id=".$tencanbo_id;
        }else{
            $tencanbo ="";
        }
        redirect("kehoachcongtac".$tencanbo);
    }

    public function tpdanhgia(){
        $id = _post('kehoach_id');
        $canbo_id = _post('canbo_id');
        $lydo = _post('lydo');
        if(_post('chatluong')){
            $chatluong= _post('chatluong');
            if($chatluong == 3 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 5,"chatluong"=>1,"sangtao"=>2,'danh_gia'=>$lydo);
            if($chatluong == 2 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 5,"chatluong"=>2,"sangtao"=>1,'danh_gia'=>$lydo);
            if($chatluong == 1 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 5,"chatluong"=>1,"sangtao"=>1,'danh_gia'=>$lydo);
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
            // xóa công việc nếu trưởng phòng duyệt công việc vào cuối tuần
            $this->Mdanhmuc->xoa_kehoach_file("kh_id_sub=".$id." and loai_kh !=3 ","kehoach");
        }else{
            $data_update = array("ngay_danhgia"=>date('Y-m-d H:i:s'),"active" => 3,'danh_gia'=>$lydo,"chatluong"=>0,"sangtao"=>0);
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
            $update_ketqua = $this->Mdanhmuc->capNhatKetQua($id);
        }
         $tencanbo_id = _post('tencanbo');            
        if($tencanbo_id > 0){
            $tencanbo ="?canbo_id=".$tencanbo_id;
        }else{
            $tencanbo ="";
        }
        $trangthai_active = _post('trangthai_active');            
        if($trangthai_active > 0){
            $trangthai = "?trangthai=".$trangthai_active;
            if($tencanbo !='')$trangthai = "&trangthai=".$trangthai_active;
        }else{
            $trangthai = "";
        }
        redirect("kehoachcongtac".$tencanbo.$trangthai);
    }

    public function chuahoanthanh(){
        $kehoach_id      = _post('kehoach_id');
        $canbo_id        = _post('canbo_id');
        $lydo            = _post('lydo');
        $viecthuongxuyen = _post('viecthuongxuyen');
        $name            = $_FILES['files']['name'];
        $ngay_han        = date('Y-m-d',strtotime(str_replace('/', '-', _post('ngay_han'))));
        $time            = time();
        $thoigianluu     = date('Y-m-d H:i:s');
        $ngay_nhap       = date('Y-m-d');
        $trangthai       = _post('trangthai');
        $mangfile        ='';
        if(!empty($name[0]))
        { 
            foreach ($name as $key => $value) {
                $dauphay=($key>0)?';':'';
                $mangfile = $mangfile.$dauphay.$time."_".clear($value);
            }
            $this->upload('kehoach_'.date('Y'),$time);
        }

        $phoi_hop = 2;//mặc định nhiệm vụ phòng giao phối hợp là 2

        if($canbo_id == $this->_session['PK_iMaCB']){
            if($trangthai == 1){
                if($viecthuongxuyen==4)
                {
                     $arrayUpdate = array(
                        'ket_qua_hoanthanh' => $lydo, 
                        'ngay_hoanthanh'    => $thoigianluu, 
                        'active'            => 4,
                        'loai_kh'           => 4,
                        'ketluan_pp'        => '' );
                }
                else{
                    $arrayUpdate = array(
                        'ket_qua_hoanthanh' => $lydo, 
                        'ngay_hoanthanh'    => $thoigianluu, 
                        'active'            => 4,
                        'ketluan_pp'        => '' );
                }
               
                $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$kehoach_id,"kehoach",$arrayUpdate);
            }else{
                $arrayUpdate = array(
                    'khokhan_vuongmac' => $lydo, 
                    'ngay_hoanthanh'   => $thoigianluu, 
                    'active'           => 3 );
                $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$kehoach_id,"kehoach",$arrayUpdate);
            }
            $phoi_hop = 1;
        }
        
        $arrayName = array('kehoach_id' => $kehoach_id,'lydo' => $lydo,'file_upload' => $mangfile, 'thoigianluu' => $thoigianluu, 'active' => $trangthai, 'canbo_id' => $this->_session['PK_iMaCB'], 'phong_id' => $this->_session['FK_iMaPhongHD'], 'chutri' => $phoi_hop,'ngay_han' => $ngay_han,'ngay_nhap' => $ngay_nhap); 

        $delete_kehoachFile = $this->Mdanhmuc->xoa_kehoach_file(' kehoach_id='.$kehoach_id.' and canbo_id = '.$this->_session['PK_iMaCB'],'kehoach_file');
        $check_insert = $this->Mdanhmuc->themDuLieu('kehoach_file',$arrayName);
        
        $tencanbo_id = _post('tencanbo');            
        if($tencanbo_id > 0){
            $tencanbo ="?canbo_id=".$tencanbo_id;
        }else{
            $tencanbo ="";
        }
        $trangthai_active = _post('trangthai_active');            
        if($trangthai_active > 0){
            $trangthai = "?trangthai=".$trangthai_active;
            if($tencanbo !='')$trangthai = "&trangthai=".$trangthai_active;
        }else{
            $trangthai = "";
        }
        redirect("kehoachcongtac".$tencanbo.$trangthai);
    }
    public function upload($dir,$time)
    {
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
                    $_FILES['files']['name']     = $file['files']['name'][$i];
                    $_FILES['files']['type']     = $file['files']['type'][$i];
                    $_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
                    $_FILES['files']['error']    = $file['files']['error'][$i];
                    $_FILES['files']['size']     = $file['files']['size'][$i];
                    if(is_dir($dir)==false){
                        mkdir($dir);        // Create directory if it does not exist
                    }
                    $config['upload_path']   = $dir.'/';
                    $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
                    $config['overwrite']     = true;
                    $config['file_name']     = $time."_".clear($_FILES['files']['name']);
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
    }
    // chuyên viên tự kết thúc công việc của mình
    public function capnhat()
    {
        $id = _post('id');
        $chatluong = _post('chatluong');
        if($chatluong >0)$data_update = array("ngay_hoanthanh" => date('Y-m-d H:i:s'),"active" => 4,'loai_kh' => $chatluong);
        else $data_update = array("ngay_hoanthanh" => date('Y-m-d H:i:s'),"active" => 3);
        $check_update = $this->Mdanhmuc->capnhatDuLieuKHCT('kh_id',$id,"kehoach",$data_update);
        echo json_encode($check_update);exit();
    }
    //lãnh đạo phòng đánh giá mức hoàn thành của chuyên viên
    public function danhgia()
    {
        $id = _post('id');
        $chatluong= _post('chatluong');
        if($chatluong == 3 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 5,"chatluong"=>1,"sangtao"=>2);
        if($chatluong == 2 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 5,"chatluong"=>2,"sangtao"=>1);
        if($chatluong == 1 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 5,"chatluong"=>1,"sangtao"=>1);
        $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
        echo json_encode($check_update);exit();
    }

    // thêm kế hoạch công tác tuần
    public function themKeHoach()
    {
        if ($this->input->post('luudulieu')) {
            if (_post('loai_kh') >= 3)$loai_kh = _post('loai_kh');
            else $loai_kh = 2; 
            $kehoach_ma = _post('kehoach_ma');
            $ngay_han = str_replace('/', '-',_post('ngay_han'));
            $active = 0;
            $ykien_pp='';
            $ykien_tp='';
            if(_post('lanhdao_id') > 0)  $lanhdao_id=_post('lanhdao_id');
            else  $lanhdao_id=$this->_session['PK_iMaCB'];

            $quyen_cbphutrach = $this->Mdanhmuc->get_quyen($lanhdao_id)['iQuyenHan_DHNB'];//pr($quyen_cbphutrach);
            if($quyen_cbphutrach == 3 or $quyen_cbphutrach == 6)$active = 1;

            if($this->_session['iQuyenHan_DHNB'] ==7 or $this->_session['iQuyenHan_DHNB'] ==11 or $this->_session['iQuyenHan_DHNB'] ==10){
                $active = 2; 
            }
            if($this->_session['iQuyenHan_DHNB'] ==6 || $this->_session['iQuyenHan_DHNB'] ==3){
                $active = 3;
            }  
           
            /*$tuan_chamdiem =  $this->Mdanhmuc->getTuanMax($this->_session['PK_iMaCB']);
            if($tuan_chamdiem['tuan'] == _post('tuan')) {$tuan = (int)_post('tuan')+1;} 
            else $tuan = (int)date("W", strtotime(date('Y-m-d')));//(int)_post('tuan'); */

            $tuan = _post('tuan');
            if($tuan < (int)date("W", strtotime(date('Y-m-d')))){
                $tuan = (int)date("W", strtotime(date('Y-m-d')));
            }
            //nếu chấm điểm rồi người dùng nhập kế hoạch tuần nó sẽ tự động sang tuần tiếp theo
            $munber_week = $this->Mdanhmuc->getTuanMax(_post('canbo_id'))['tuan'];
            $tuan_hientai = (int)date("W");
            if($munber_week == $tuan ){
                $cong = 1;
            }else{
                $cong = 0;
            }

            $data_kehoachtuan = array(
                'lanhdao_id' => $lanhdao_id,
                'phong_id' => $this->_session['FK_iMaPhongHD'],
                'kh_noidung' => _post('kh_noidung'),
                'canbo_id' => _post('canbo_id'),
                'tuan'   => $tuan + $cong,
                'date_nhap' => date('Y-m-d H:i:s'),
                'ngay_han' =>  date('Y-m-d',strtotime($ngay_han)),
                'ngay_nhan' => date('Y-m-d'),
                'loai_kh' => $loai_kh,
                'user_input' =>  $this->_session['PK_iMaCB'],
                'phoihop' =>   _post('chuyenvien_ph'),
                'active' => $active,
                'ykien_pp'  => $ykien_pp,
                'ykien_tp'  => $ykien_tp    
            );
            $this->_giatri = $data_kehoachtuan;
            //thêm mới dữ liệu
            if( _post('canbo_id') >0 && _post('kh_noidung')!=''){
                if($kehoach_ma == 0){
                    $check_insert = $this->Mdanhmuc->themDuLieu('kehoach',$data_kehoachtuan);
                    redirect("kehoachcongtac");
                }else{
                    $check_insert = $this->Mdanhmuc->capnhatDuLieu('kh_id',$kehoach_ma,'kehoach',$data_kehoachtuan);
                    redirect("kehoachcongtac");
                }
            }else{
                return messagebox('Bạn phải nhập nội dung và tên cán bộ thụ lý', 'danger');
            }
            
            if ($check_insert > 0) {
                $this->_giatri ='';
                return messagebox('Thêm thành công ', 'info');
            } else {
                return messagebox('Thêm thất bại', 'danger');
            } 
            
        }
    }

}

/* End of file Ckehoachcongtac.php */
/* Location: ./application/controllers/vanban/Ckehoachcongtac.php */