<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cviewkehoachcongtac extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        $id = $this->uri->segment(2);
        if($id > 0){
            $check_data = $this->Mdanhmuc->layDuLieu('id',$id,'tbl_kehoach');
            $this->_giatri = $check_data[0];
        }

        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->ldDanhGia();
            redirect("dskehoachcongtac");
        }

        $start = date('d/m/Y',strtotime($check_data[0]['date_start']));
        $end = date('d/m/Y',strtotime($check_data[0]['date_end']));
 
        $data['giatri'] = $this->_giatri;    
        $data['title']    = 'Xem kế hoạch công tác tuần';
        $data['arr']   = $check_data[0]['tuan']." (".$start." - ".$end.")";
        $data['tuan'] = $check_data[0]['tuan'];
        $temp['data']     = $data;//pr($data);
       
        $temp['template'] = 'kehoachcongtac/Vviewkehoachcongtac';
        $this->load->view('layout_admin/layout',$temp);
	}
    // thêm kế hoạch công tác tuần
    public function ldDanhGia()
    {
        $id = _post('id');
        /*if(_post('date_tp_danhgia')=="")$date_danhgia2 = date('Y-m-d H:i:s'); 
        else $date_tp_danhgia = _post('date_tp_danhgia');*/ 

        $date_tp_danhgia = date('Y-m-d H:i:s');
        
        if ($this->input->post('luudulieu')) {
            $id = _post('id');
            $data_kehoachtuan = array(
                'tp_danhgia'    => _post('tp_danhgia'),
                'date_tp_danhgia' =>  $date_tp_danhgia,
                'tp_diem'       =>  _post('tp_diem'),
                'user_tp'       =>  $this->_session['PK_iMaCB']                   
            );
            $this->_giatri = $data_kehoachtuan;
           //thêm mới dữ liệu
            $check_update = $this->Mdanhmuc->capnhatDuLieu('id',$id,'tbl_kehoach',$data_kehoachtuan);
            if ($check_update > 0) {
                return messagebox('Cập nhật đánh giá kết quả thành công kế hoạch công tác tuần ' ._post('tuan'), 'info');
            } else {
                return messagebox('Cập nhật đánh giá kết quả kế hoạch tuần thất bại', 'danger');
            }             
        }
    }
}
    