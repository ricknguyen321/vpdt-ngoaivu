<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdskehoachcongtac extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        $action = _post('action');
        if($action =='chamdiem')
        {
            $this->chamdiem();
        }
        $tuan = (int)date("W", strtotime(date('Y-m-d')));
        if($this->uri->segment(2) >0) $tuan = $this->uri->segment(2);
        if(_post('tuan')>0) $tuan = _post('tuan');

        $phong_id = $this->_session['FK_iMaPhongHD'];
        if($this->uri->segment(3) >0) $phong_id = $this->uri->segment(3);
        //kiểm tra dữu liệu đã tồn tại chưa
        $check_data = $this->Mdanhmuc->layDuLieu2('tuan',$tuan,'phong_id',$phong_id,'danhgia','diem_tong');
        $this->_giatri = $check_data;
        $tongdiem=0;
        foreach ($check_data as $value) {
            $data['diem'][$value['canbo_id']]=$value['diem_tong_tp'];
            $tongdiem= (float)$tongdiem+(float)$value['diem_tong_tp'];
        }
        if(count($check_data)==0)$data['diem_tb'] = 0;
        else $data['diem_tb'] = round($tongdiem/count($check_data),1);
        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        
        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and FK_iMaPhongHD='.$phong_id,'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            if($value['iQuyenHan_DHNB']==8){
                $chuyenvien[$value['PK_iMaCB']]=$value['sHoTen'];
            }else{
                if($value['iQuyenHan_DHNB']>6 && $value['iQuyenHan_DHNB']!=9)$lanhdao[$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        $year = date('Y');
        $year = $_SESSION['nam'];
        $arr=array();unset($arr[0]);
        for($i=0;$i<=53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            
        }  
        //lay tổng gia tri cac cong viec cua tung chuyen vien
        $id_lanhdao = array();
        foreach ($lanhdao as $id_cb => $tencb) {
            array_push($id_lanhdao, $id_cb);
        }

        $data['giatri'] = $check_data;   
        $data['title']    = 'Danh sách công tác tuần';
        $data['arr']   = $arr;//pr($arr);
        $data['phong_id'] = $phong_id;
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['FK_iMaPhongHD'] = $this->_session['FK_iMaPhongHD'];
       
        $data['id_lanhdao']   = $id_lanhdao;
        $data['tuan'] = $tuan;
        //$data['week'] = $tuan;
        $data['list_user'] = $list_user;
        $temp['data']     = $data;//pr($data);
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacTuan_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacTuan_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtac_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtac1';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}
    public function chamdiem(){
        $danhgia_id = _post('danhgia_id');
        $data_diem_tp = array(
            'diem_1_tp'  => _post('diem_1_tp'),
            'diem_2_tp'  => _post('diem_2_tp'),
            'diem_3_tp'  => _post('diem_3_tp'),
            'diem_4_tp'  => _post('diem_4_tp'),
            'diem_5_tp'  => _post('diem_5_tp'),
            'diem_6_tp'  => _post('diem_6_tp'),
            'diem_7_tp'  => _post('diem_7_tp'),
            'diem_8_tp'  => _post('diem_8_tp'),
            'diem_9_tp'  => _post('diem_9_tp'),
            'diem_10_tp'  => _post('diem_10_tp'),
            'diem_11_tp'  => _post('diem_11_tp'),
            'diem_12_tp'  => _post('diem_12_tp'),
            'diem_13_tp'  => _post('diem_13_tp'),
            'diem_14_tp'  => _post('diem_14_tp'),
            'diem_15_tp'  => _post('diem_15_tp'),
            'diem_16_tp'  => _post('diem_16_tp'),
            'diem_18_tp'  => _post('diem_18_tp'),
            'diem_19_tp'  => _post('diem_19_tp'),
            'diem_tong_tp'  => _post('diem_tong_tp'),
            'active' => 2,
            'ngay_danhgia'  => date('Y-m-d H:i:s'),
            'nhan_xet' => _post('nhan_xet')
        );
        $check_update = $this->Mdanhmuc->capnhatDuLieu('danhgia_id',$danhgia_id,'danhgia',$data_diem_tp);
        echo json_encode($check_update);exit();
    }
}

/* End of file Cdskehoachcongtac.php */
/* Location: ./application/controllers/vanban/Cdskehoachcongtac.php */