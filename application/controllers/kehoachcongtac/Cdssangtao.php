<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdssangtao extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
      
        $month = $this->uri->segment(2);
        if(empty($month) and date('d') < 12 )$month = date('m') - 1;
        if(empty($month) and date('d') >= 12 )$month = date('m');
        if(isset($_POST['month']))$month = $_POST['month'];
        
        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);

        $count_arr = array();
        if($month > 0){
			$list_data = $this->Mdanhmuc->sangtaotuan(" tuan in (".$tuan.") and sangtao=2");
        }    
        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('(PK_iMaPB < 22 or PK_iMaPB = 68) and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }

        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 2 ','tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['tencanbo'][$value['PK_iMaCB']]=$value['sHoTen'];
        }  


        $data['title']    = 'Xem danh sách sáng tạo đổi mới của tháng';
        
        $data['arr_tuan']   = $arr_tuan;
        $data['list_data']   = $list_data;
        $data['month'] = $month;
        $data['sotuan'] = count($arr_tuan);

        
        $temp['data']     = $data;//pr($data);

        $temp['template'] = 'kehoachcongtac/Vdssangtao';
        $this->load->view('layout_admin/layout',$temp);        
	}

}