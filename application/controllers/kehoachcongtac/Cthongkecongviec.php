<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongkecongviec extends MY_Controller {
    protected $_giatri;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
        $this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
         $this->Mkehoachcongtac = new Mkehoachcongtac();        
    }
    public function index()
    {       
        $month = $this->uri->segment(2);
        $canbo = $this->uri->segment(3);
        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);
        $tuanbd = $beg;
        $tuankt = $end;
        if(isset($_POST['tuanbd'])){
            $tuanbd = $_POST['tuanbd'];
            $tuankt = $_POST['tuankt'];
            $sotuan = $tuankt - $tuanbd;
            $tuan = "0";
            for($i=0;$i<=$sotuan;$i++){
                $tuan_tam = $tuanbd + $i;
                $arr_tuan[$i] = $tuan_tam;
                $tuan .= ",".$tuan_tam;
            }
        }

        $year = date('Y');
        $arr=array();
        for($i=0;$i<53;$i++){
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            
        }
// lấy tên canbo
        $hoten = $this->Mdanhmuc->layDuLieu('PK_iMaCB=',$canbo,'tbl_canbo')[0]['sHoTen'];            
        $data['tuankt']   = $tuankt;
        $data['tuanbd']   = $tuanbd;

        $data['arr']   = $arr;
        $data['hoten']   = $hoten;
        $data['dsviechoanthanh'] = $this->Mdanhmuc->dsviechoanthanh(" tuan in (".$tuan.") and canbo_id=".$canbo." and active = 5");

        if($tuankt > date('W'))$tuankt=date('W');//pr($tuankt);
        $data['dsviecdangthuchien'] = $this->Mdanhmuc->dsviechoanthanh(" tuan = ".$tuankt." and canbo_id=".$canbo." and active < 5");


        $data['title']    = 'Thống kê công việc';        
        $temp['data']     = $data;            
        $temp['template'] = 'kehoachcongtac/Vthongkecongviec';
        $this->load->view('layout_admin/layout',$temp);

        
    }

   
 
}
    