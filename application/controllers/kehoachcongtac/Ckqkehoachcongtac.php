<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckqkehoachcongtac extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        $tuan = $this->uri->segment(2);
        if($tuan > 0){
            $check_data = $this->Mdanhmuc->layDuLieu2('tuan',$tuan,'user_id',$this->_session['PK_iMaCB'],'tbl_kehoach');
            $this->_giatri = $check_data[0];
        }

        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->kqKeHoach();
            redirect("dskehoachcongtac");
        }

        $start = date('d/m/Y',strtotime($check_data[0]['date_start']));
        $end = date('d/m/Y',strtotime($check_data[0]['date_end']));
 
        $data['giatri'] = $this->_giatri;    
        $data['title']    = 'Nhập kết quả kế hoạch công tác';
        $data['arr']   = $tuan." (".$start." - ".$end.")";
        $data['tuan'] = $tuan;
        $temp['data']     = $data;
       
        $temp['template'] = 'kehoachcongtac/Vkqkehoachcongtac';
        $this->load->view('layout_admin/layout',$temp);
	}
    // thêm kế hoạch công tác tuần
    public function kqKeHoach()
    {
        $id = _post('id');
        if(_post('danhgia2')=="")$date_danhgia2 = '';
        else if(_post('danhgia2')!="" && _post('date_danhgia2')=="")$date_danhgia2 = date('Y-m-d H:i:s'); 
        else $date_danhgia2 = _post('date_danhgia2');

        if(_post('danhgia3')=="")$date_danhgia3 = '';
        else if(_post('danhgia3')!="" && _post('date_danhgia3')=="")$date_danhgia3 = date('Y-m-d H:i:s'); 
        else $date_danhgia3 = _post('date_danhgia3');

        if(_post('danhgia4')=="")$date_danhgia4 = '';
        else if(_post('danhgia4')!="" && _post('date_danhgia4')=="")$date_danhgia4 = date('Y-m-d H:i:s'); 
        else $date_danhgia4 = _post('date_danhgia4');

        if(_post('danhgia5')=="")$date_danhgia5 = '';
        else if(_post('danhgia5')!="" && _post('date_danhgia5')=="")$date_danhgia5 = date('Y-m-d H:i:s'); 
        else $date_danhgia5 = _post('date_danhgia5');

        if(_post('danhgia6')=="")$date_danhgia6 = '';
        else if(_post('danhgia6')!="" && _post('date_danhgia6')=="")$date_danhgia6 = date('Y-m-d H:i:s'); 
        else $date_danhgia6 = _post('date_danhgia6');

        if(_post('danhgia7')=="")$date_danhgia7 = '';
        else if(_post('danhgia7')!="" && _post('date_danhgia7')=="")$date_danhgia7 = date('Y-m-d H:i:s'); 
        else $date_danhgia7 = _post('date_danhgia7');

        

        if ($this->input->post('luudulieu')) {
            $id = _post('id');
            $data_kehoachtuan = array(
                'danhgia2' => _post('danhgia2'),
                'date_danhgia2' => $date_danhgia2,

                'danhgia3' => _post('danhgia3'),
                'date_danhgia3' => $date_danhgia3,

                'danhgia4' => _post('danhgia4'),
                'date_danhgia4' => $date_danhgia4,

                'danhgia5' => _post('danhgia5'),
                'date_danhgia5' => $date_danhgia5,

                'danhgia6' => _post('danhgia6'),
                'date_danhgia6' => $date_danhgia6,

                'danhgia7' => _post('danhgia7'),
                'date_danhgia7' => $date_danhgia7       
            );
            $this->_giatri = $data_kehoachtuan;
           //thêm mới dữ liệu
            $check_update = $this->Mdanhmuc->capnhatDuLieu('id',$id,'tbl_kehoach',$data_kehoachtuan);
            if ($check_update > 0) {
                return messagebox('Cập nhật đánh giá kết quả thành công kế hoạch công tác tuần ' ._post('tuan'), 'info');
            } else {
                return messagebox('Cập nhật đánh giá kết quả kế hoạch tuần thất bại', 'danger');
            }             
        }
    }
}
    