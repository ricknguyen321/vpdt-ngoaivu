<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cviewkhctphong extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        $month = (int)date("m");
        if(_post('month')>0 && !isset($_POST['luudulieu'])) $month = _post('month');
        //kiểm tra dữu liệu đã tồn tại chưa
        $check_data = $this->Mdanhmuc->layDuLieu2('thang',$month,'department_id',$this->_session['FK_iMaPhongHD'],'tbl_kehoachphong');
        if(isset($check_data[0]['id'])) $this->_giatri = $check_data;

        $list_lanhdao = $this->Mdanhmuc->layDuLieu3('iQuyenHan_DHNB',4,'iQuyenHan_DHNB',5,'tbl_canbo');
        foreach ($list_lanhdao as $key => $value) {
            $data['lanhdaoso'][$value['PK_iMaCB']]=$value['sHoTen'];
        }
        $list_user = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
        }
        
        $data['giatri']   = $this->_giatri;    
        $data['title']    = 'Kế hoạch công tác tháng';
        $data['month']    = $month;
        $temp['data']     = $data;
       
        $temp['template'] = 'kehoachcongtac/Vviewkhctphong';
        $this->load->view('layout_admin/layout',$temp);
	}
   

}

/* End of file Ckehoachcongtac.php */
/* Location: ./application/controllers/vanban/Ckehoachcongtac.php */