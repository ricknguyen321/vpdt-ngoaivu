<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckhctphong extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        $month = (int)date("m");
        if(_post('month')>0 && !isset($_POST['luudulieu'])) $month = _post('month');

        //kiểm tra dữu liệu đã tồn tại chưa
        $check_data = $this->Mdanhmuc->layDuLieu2('thang',$month,'department_id',$this->_session['FK_iMaPhongHD'],'tbl_kehoachphong');
        if(isset($check_data[0]['id'])) $this->_giatri = $check_data[0];

        $list_lanhdao = $this->Mdanhmuc->layDuLieu3('iQuyenHan_DHNB',4,'iQuyenHan_DHNB',5,'tbl_canbo');
        /*foreach ($list_lanhdao as $key => $value) {
            $data['lanhdaoso'][$value['PK_iMaCB']]=$value['sHoTen'];
        }*/
        $list_user = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',$this->_session['FK_iMaPhongHD'],'tbl_canbo');
        /*foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
        }
*/
        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->themKeHoach();
        }	
        $year = date('Y');
        $data['giatri']   = $this->_giatri;    
        $data['title']    = 'Nhập kế hoạch công tác tháng';
        $data['month']    = $month;
        $data['lanhdaoso']    = $list_lanhdao;
        $data['canbophong']    = $list_user;
        $temp['data']     = $data;
       
        $temp['template'] = 'kehoachcongtac/Vkhctphong';
        $this->load->view('layout_admin/layout',$temp);
	}
    // thêm kế hoạch công tác tuần
    public function themKeHoach()
    {
        if ($this->input->post('luudulieu')) {//pr($_POST);
            $thang=_post('month');
            $kiemtra=1;
            $data_kehoachthang = array(
                'id_sub' => 0,
                'department_id' => $this->_session['FK_iMaPhongHD'],
                'thang' => $thang,
                'noidung' => _post('noidung'),
                'ld_so' => _post('ld_so'),
                'ld_phong' => implode(",",_post('ld_phong')),
                'user_id' => implode(",",_post('user_id')),
                'date_start' => date('Y-m-d',strtotime(str_replace('/', '-', _post('date_start')))),
                'date_end' => date('Y-m-d',strtotime(str_replace('/', '-', _post('date_end')))),
                'date_nhap' => date('Y-m-d H:i:s')        
            );
            
           //thêm mới dữ liệu
            $id_sub = $this->Mdanhmuc->themDuLieu2('tbl_kehoachphong',$data_kehoachthang);
            if($id_sub>0){
                for($i=1;$i<20;$i++){
                    if(_post("noidung$i") == "")break;
                    $data_kehoachthang = array(
                    'id_sub' => $id_sub,
                    'department_id' => $this->_session['FK_iMaPhongHD'],
                    'thang' => $thang,
                    'noidung' => _post("noidung$i"),
                    'ld_so' => _post("ld_so$i"),
                    'ld_phong' => implode(",",_post("ld_phong$i")),
                    'user_id' => implode(",",_post("user_id$i")),
                    'date_start' => date('Y-m-d',strtotime(str_replace('/', '-', _post("date_start$i")))),
                    'date_end' => date('Y-m-d',strtotime(str_replace('/', '-', _post("date_end$i")))),
                    'date_nhap' => date('Y-m-d H:i:s')         
                    );
                    $this->Mdanhmuc->themDuLieu('tbl_kehoachphong',$data_kehoachthang);
                    $kiemtra ++;
                }
            }
            
            if ($kiemtra == _post('soluong')) {
                return messagebox('Thêm thành công kế hoạch công tác tháng ' .$thang, 'info');
            } else {
                return messagebox('Thêm kế hoạch công tác tuần thất bại', 'danger');
            } 
            
            
        }
    }

}

/* End of file Ckehoachcongtac.php */
/* Location: ./application/controllers/vanban/Ckehoachcongtac.php */