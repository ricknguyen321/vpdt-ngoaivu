<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdskehoachcongtacquy_tp extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{    
        $phong_id = $this->_session['FK_iMaPhongHD'];
        $month = date('m');
        $year = date('Y');
        if($month == 1 || $month == 2 || $month == 3)$quy = 1;
        if($month == 4 || $month == 5 || $month == 6)$quy = 2;
        if($month == 7 || $month == 8 || $month == 9)$quy = 3;
        if($month == 10 || $month == 11 || $month == 12)$quy = 4;

        if($this->uri->segment(3)>0)$quy = $this->uri->segment(3);
        if(isset($_POST['quy']))$quy = $_POST['quy'];

        if($quy==1){
            $month1 = 1;$month2 = 3;
        }else if($quy==2){
            $month1 = 4;$month2 = 6;
        }else if($quy==3){
            $month1 = 7;$month2 = 9;
        }else{
            $month1 = 10;$month2 = 12;
        }

        $beg = (int) date('W', strtotime("first thursday of $year-$month1"));
        $end = (int) date('W', strtotime("last thursday of $year-$month2"));
        $tuan = (join(', ', range($beg, $end)));
        $week = (int)date("W", strtotime(date('Y-m-d')));
        if($week < $end) $end = $week;

        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        
        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB >= 3 and iQuyenHan_DHNB <=6 and PK_iMaCB != 617','tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            if($value['iQuyenHan_DHNB']==3 or $value['iQuyenHan_DHNB']==6){
                $chuyenvien[$value['PK_iMaCB']]=$value['sHoTen'];
                $phong[$value['PK_iMaCB']]= $value['FK_iMaPhongHD'];
                $data['phongdonvi'][$value['PK_iMaCB']]= $value['FK_iMaPhongHD'];
            }else{
                if($value['iQuyenHan_DHNB']==5)$lanhdao[$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        $year = date('Y');
        //lay tong toan sơ
        $count_arr[0][0] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.")");
            $count_arr[0][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 3");
            $count_arr[0][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[0][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[0][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 3 and chatluong =1 ");
            $count_arr[0][5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 3 and chatluong =2 ");
            $count_arr[0][6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 3 and sangtao =2 ");
            $count_arr[0][7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$end.") and active < 3 ");
            $count_arr[0][8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$end.")  and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[0][9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$end.") and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01' and vanban_id not in(select distinct(vanban_id) from kehoach where active =3 AND tuan in (".$tuan.") )");

            $check = $count_arr[0][0]['tong'] - $count_arr[0][8]['tong'];
            if($check >0){
                $count_arr[0][10] = round($count_arr[0][1]['tong']/$check*100,1);
            }else{
                $count_arr[0][10] ='0';
            }
            $count_arr[0][11] = '';
            $count_arr[0][12] = '';
            $count_arr[0][13]['diem_tong_tp']= '';
            $count_arr[0][14]['danhgia_ld'] = '';
        
        //lay tổng gia tri cac cong viec cua lãnh đạo sở
        $key_cb=1;
        foreach ($lanhdao as $id_cb => $tencb) {
            $count_arr[$key_cb][0] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb);
            $count_arr[$key_cb][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 3");
            $count_arr[$key_cb][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[$key_cb][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cb][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 3 and chatluong =1 ");
            $count_arr[$key_cb][5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 3 and chatluong =2 ");
            $count_arr[$key_cb][6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 3 and sangtao =2 ");
            $count_arr[$key_cb][7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$end.") and lanhdao_so = ".$id_cb." and active < 3 ");
            $count_arr[$key_cb][8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$end.") and lanhdao_so = ".$id_cb." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[$key_cb][9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$end.") and lanhdao_so = ".$id_cb." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01' and vanban_id not in(select distinct(vanban_id) from kehoach where active =3 AND tuan in (".$tuan.") )");

            $check = $count_arr[$key_cb][0]['tong'] - $count_arr[$key_cb][8]['tong'];
            if($check >0){
                $count_arr[$key_cb][10] = round($count_arr[$key_cb][1]['tong']/$check*100,1);
            }else{
                $count_arr[$key_cb][10] ='0';
            }
            $count_arr[$key_cb][11] = $tencb;
            $count_arr[$key_cb][12] = $id_cb;
            $count_arr[$key_cb][13] = $this->Mdanhmuc->countTP_quy('diem','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $count_arr[$key_cb][14] = $this->Mdanhmuc->countTP_quy('danhgiaquy_ld','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $key_cb++;
        }
        $key_cv=11;
        foreach ($chuyenvien as $id_cb => $tencb) {
            $count_arr[$key_cv][0] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]);
            $count_arr[$key_cv][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 3");
            $count_arr[$key_cv][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[$key_cv][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cv][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 3 and chatluong =1 ");
            $count_arr[$key_cv][5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$id_cb." and active = 3 and chatluong =2 ");
            $count_arr[$key_cv][6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 3 and sangtao =2 ");
            $count_arr[$key_cv][7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$id_cb." and active < 3 ");
            $count_arr[$key_cv][8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[$key_cv][9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND (thuc_hien = 3) AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01' and vanban_id not in(select distinct(vanban_id) from kehoach where active =3 AND tuan in (".$tuan.") )");
            $check = $count_arr[$key_cv][0]['tong'] - $count_arr[$key_cv][8]['tong'];
            if($check >0){
                $count_arr[$key_cv][10] = round($count_arr[$key_cv][1]['tong']/$check*100,1);
            }else{
                $count_arr[$key_cv][10] ='0';
            }
            $count_arr[$key_cv][11] = $tencb;
            $count_arr[$key_cv][12] = $id_cb;
            $count_arr[$key_cv][13] = $this->Mdanhmuc->countTP_quy('diem','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $count_arr[$key_cv][14] = $this->Mdanhmuc->countTP_quy('danhgiaquy_ld','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $key_cv++;
        }//pr($count_arr);
         
       

        $data['title']    = 'Đánh giá kế hoạch công tác tháng';
        $data['phong_id'] = $this->_session['FK_iMaPhongHD'];
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['count_arr']   = $count_arr;
        //$data['count_arr1']   = $count_arr1;
        $data['month'] = $month;
        $data['quy'] = $quy;
        $data['list_user'] = $list_user;
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/VdskehoachcongtacquyTP_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtacquy_tp';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}
   
}

/* End of file Cdskehoachcongtac.php */
/* Location: ./application/controllers/vanban/Cdskehoachcongtac.php */