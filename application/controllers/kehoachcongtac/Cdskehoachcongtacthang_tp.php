<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdskehoachcongtacthang_tp extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
    public function getStartAndEndDate($week, $year) {
      $dateTime = new DateTime();
      $dateTime->setISODate($year, $week);
      //$result['start_date'] = $dateTime->format('d-M-Y');
      $dateTime->modify('+6 days');
      //$result['end_date'] = $dateTime->format('Y-m-d');
      $result = $dateTime->format('Y-m-d');
      return $result;
    }

	public function index()
	{    
        if(date('d')<15)$month = date('m')-1;
        else $month = date('m');
        if($month == 0)$month = 12;
        if(isset($_POST['month']))$month = $_POST['month'];
        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);
        $tuancuoi = end($arr_tuan);

        $tuan_hientai = (int)date("W", strtotime(date('Y-m-d')));
        if($tuan_hientai < $tuancuoi)$tuancuoi = $tuan_hientai;
        /*$tuan1 =  (int)date("W", strtotime(date('Y-m-d')));
        $end = count($arr_tuan)-1;
        if($arr_tuan[0]>$tuan1) $tuan = $arr_tuan[0];   
        if($arr_tuan[$end]<$tuan1) $tuan = $arr_tuan[$end];*/

        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        if(date('Y-'.$month) > '2018-10'){
            $str = ' and PK_iMaCB != 56 ';
        }else{
            $str = ' and PK_iMaCB != 655 ';
        }

        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB >= 3 and iQuyenHan_DHNB <=6 and (PK_iMaCB != 617 and PK_iMaCB !=678 )'.$str." or (iTrangThai=1 and  PK_iMaCB = 534 )",'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            if($value['iQuyenHan_DHNB']==3 or $value['iQuyenHan_DHNB']==6){
                $chuyenvien[$value['PK_iMaCB']]=$value['sHoTen'];
                $phong[$value['PK_iMaCB']]= $value['FK_iMaPhongHD'];
                $data['phongdonvi'][$value['PK_iMaCB']]= $value['FK_iMaPhongHD'];
            }else{
                if($value['iQuyenHan_DHNB']==5)$lanhdao[$value['PK_iMaCB']]=$value['sHoTen'];
                if($value['iQuyenHan_DHNB']==4)$GDS=$value['PK_iMaCB'];
            }
        }
        /*if($month == 12){
            $year = (int)date('Y') + 1;
            $ngaythangnamhan = $year."-01-01";//pr($ngaythangnamhan);
        }else{
            $month1 = $month + 1;
            if($month1 < 10 ) $month1 ="0".$month1;
            $ngaythangnamhan = date('Y') .'-'.$month1.'-01';
        }*/
        
        
        $year = date('Y');

        $ngaythangnamhan = $this->getStartAndEndDate($tuancuoi,$year);
        //$ngaythangnamhan = '2018-12-31';
        //echo $ngaythangnamhan;
        if($month == date('m')){
            $ngaythangnamhan = date('Y-m-d');
        }

        //lay tong toan sơ
        	//$count_arr[0][0] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.")     ");
            $count_arr[0][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5     ");
            $count_arr[0][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01')     ");
            $count_arr[0][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01'      ");
            $count_arr[0][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and chatluong =1      ");
            $count_arr[0][5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and chatluong =2      ");
            $count_arr[0][6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and active = 5 and sangtao =2      ");
            $count_arr[0][7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and active < 5      ");
            $count_arr[0][8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.")  and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')     ");
            $count_arr[0][9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and active < 5 and ngay_han < '".$ngaythangnamhan."' and ngay_han >'2017-01-01'      ");

            $count_arr[0][0]['tong'] = $count_arr[0][1]['tong'] + $count_arr[0][7]['tong'];
                //and vanban_id not in(select distinct(vanban_id) from kehoach where active = 5 AND thuc_hien = 1 AND tuan in (".$tuan.") )");

            $check = $count_arr[0][0]['tong'] - $count_arr[0][8]['tong'];
            if($check >0){
                $count_arr[0][10] = round($count_arr[0][1]['tong']/$check*100,1);
            }else{
                $count_arr[0][10] ='0';
            }
            $count_arr[0][11] = '';
            $count_arr[0][12] = '';
            $count_arr[0][13]['diem_tong']= $this->Mdanhmuc->countTP_thang('diem_tong_tp','canbo_id = '.$GDS.' and thang = '.$month.'')['diem_tong_tp'];
            $count_arr[0][14]['danhgia_ld'] = '';
            $count_arr[0][15]['danhgia']= $this->Mdanhmuc->countTP_thang('danhgia_ld','canbo_id = '.$GDS.' and thang = '.$month.'')['danhgia_ld'];


            $count_arr[0][25]['danhgia_pgd']= $this->Mdanhmuc->countTP_thang('danhgia_pgd','canbo_id ='.$GDS.' and thang = '.$month.'')['danhgia_pgd'];
            $count_arr[0][16]['diem_tong_tp']= '';

            $count_arr[0][17]['danhgia_lds'] = $this->Mdanhmuc->countTP_thang('danhgia_lds','canbo_id ='.$GDS.' and thang = '.$month.'')['danhgia_lds'];
            $count_arr[0][18] = '';
            $count_arr[0][19] = '';
            $count_arr[0][20] = round($count_arr[0][2]['tong']/$count_arr[0][1]['tong']*100,1);
            $count_arr[0][21] = round($count_arr[0][3]['tong']/$count_arr[0][1]['tong']*100,1);
            $count_arr[0][22]['diemtudong'] =$count_arr[0][10]*0.6 +30 - $count_arr[0][3]['tong']*0.02;
            
        
        //lay tổng gia tri cac cong viec cua lãnh đạo sở
        $key_cb=1;
        foreach ($lanhdao as $id_cb => $tencb) {
            //$count_arr[$key_cb][0] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb)."     ";

            //echo " vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb."<br>";
        if($id_cb == 534 and $month >= 9) continue;

        if($id_cb == 486){
            
            $count_arr[$key_cb][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5     ");
            $tam1 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuan.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active = 5     ");
            $count_arr[$key_cb][1]['tong']=$count_arr[$key_cb][1]['tong'] + $tam1['tong'];
            
            $count_arr[$key_cb][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and (CAST(kehoach.ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01')     ");

            $tam2 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuan.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active = 5 and (CAST(kehoach.ngay_hoanthanh AS DATE) <= ngay_han or ngay_han < '2017-01-01')     ");
            $count_arr[$key_cb][2]['tong']=$count_arr[$key_cb][2]['tong'] + $tam2['tong'];

            $count_arr[$key_cb][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and  CAST(kehoach.ngay_hoanthanh AS DATE) > ngay_han and ngay_han > '2017-01-01'     ");

            $tam3 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuan.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active = 5 and  CAST(kehoach.ngay_hoanthanh AS DATE) > ngay_han and ngay_han > '2017-01-01'");
            $count_arr[$key_cb][3]['tong']=$count_arr[$key_cb][3]['tong'] + $tam3['tong'];

            $count_arr[$key_cb][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and chatluong =1     ");

            $tam4 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuan.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active = 5 and chatluong =1 ");
            $count_arr[$key_cb][4]['tong']=$count_arr[$key_cb][4]['tong'] + $tam4['tong'];

            $count_arr[$key_cb][5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and chatluong =2      ");

            $tam5 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuan.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active = 5 and chatluong =2 ");
            $count_arr[$key_cb][5]['tong']=$count_arr[$key_cb][5]['tong'] + $tam5['tong'];

            $count_arr[$key_cb][6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and sangtao =2      ");

            $tam6 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuan.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active = 5 and sangtao =2 ");
            $tam6['tong']=0;
            $count_arr[$key_cb][6]['tong']=$count_arr[$key_cb][6]['tong'] + $tam6['tong'];

            $count_arr[$key_cb][7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5      ");

            $tam7 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active < 5 ");
            $count_arr[$key_cb][7]['tong']=$count_arr[$key_cb][7]['tong'] + $tam7['tong'];

            $count_arr[$key_cb][8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')     ");

            $tam8 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active < 5  and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')   ");
            $count_arr[$key_cb][8]['tong']=$count_arr[$key_cb][8]['tong'] + $tam8['tong'];

            $count_arr[$key_cb][9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5 and ngay_han < '".$ngaythangnamhan."'  and ngay_han >'2017-01-01'     ");

            $tam9 = $this->Mdanhmuc->countLD_thang("kh_noidung"," vanban_id is null AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and (phong_id = 19 or canbo_id = 156 OR lanhdao_id = 156) and active < 5 and ngay_han < '".$ngaythangnamhan."'  and ngay_han >'2017-01-01'  ");
            $count_arr[$key_cb][9]['tong']=$count_arr[$key_cb][9]['tong'] + $tam9['tong'];
            
        }else{
            $count_arr[$key_cb][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5     ");
            $count_arr[$key_cb][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01')     ");
            $count_arr[$key_cb][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01'      ");
            $count_arr[$key_cb][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and chatluong =1      ");
            $count_arr[$key_cb][5] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and chatluong =2      ");
            $count_arr[$key_cb][6] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and sangtao =2      ");
            $count_arr[$key_cb][7] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5      ");
            $count_arr[$key_cb][8] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01'   or ngay_han IS NULL  or ngay_han ='' ) ");
            $count_arr[$key_cb][9] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5 and ngay_han < '".$ngaythangnamhan."' and ngay_han >'2017-01-01'     ");

        }    


            $count_arr[$key_cb][0]['tong'] = $count_arr[$key_cb][1]['tong'] + $count_arr[$key_cb][7]['tong'];

            $check = $count_arr[$key_cb][0]['tong'] - $count_arr[$key_cb][8]['tong'];

            $count_arr[$key_cb][20] = round($count_arr[$key_cb][2]['tong']/$count_arr[$key_cb][1]['tong']*100,1);
            
            if($check >0){
                $count_arr[$key_cb][10] = round($count_arr[$key_cb][1]['tong']/$check*100,1);
                $count_arr[$key_cb][22]['diemtudong'] = $count_arr[$key_cb][10] * 0.6 + 30 - ((100-$count_arr[$key_cb][20])*0.2);
            }else{
                $count_arr[$key_cb][10] ='0';
                $count_arr[$key_cb][22]['diemtudong'] = 90- ((100-$count_arr[$key_cb][20])*0.2);
            }
            $count_arr[$key_cb][21] = round($count_arr[$key_cb][3]['tong']/$count_arr[$key_cb][1]['tong']*100,1);



            $count_arr[$key_cb][11] = $tencb;
            $count_arr[$key_cb][12] = $id_cb;
            $count_arr[$key_cb][13] = $this->Mdanhmuc->countTP_thang('diem_tong','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][14] = $this->Mdanhmuc->countTP_thang('danhgia_ld','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][15] = $this->Mdanhmuc->countTP_thang('danhgia','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][25] = $this->Mdanhmuc->countTP_thang('danhgia_pgd','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][16] = $this->Mdanhmuc->countTP_thang('diem_tong_tp','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][17] = $this->Mdanhmuc->countTP_thang('danhgia_lds','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cb][18]['danhgia_canhan'] = $this->Mdanhmuc->countTP_thang('danhgia_canhan','canbo_id ='.$id_cb.' and thang = '.$month.'')['danhgia_canhan'];
            $count_arr[$key_cb][19] = '';
            
            $key_cb++;
        }
        $key_cv=11;
        foreach ($chuyenvien as $id_cb => $tencb) {
            //$count_arr[$key_cv][0] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]."     ");
            $count_arr[$key_cv][1] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5  ");
            //echo "thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5     "."<br>";
            $count_arr[$key_cv][2] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01')   ");
           $tong_333=$this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' and vanban_id is null    ");
           $tong_334=$this->Mdanhmuc->countLD_thang("vanban_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' and vanban_id >0    ");
            $count_arr[$key_cv][3]['tong'] =  $tong_333['tong'] +  $tong_334['tong'];

            $arr_vbhtqh =   $this->Mdanhmuc->layVBQuaHanThang(" thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01'      ");
            //pr($arr_vbhtqh);
            $count_arr[$key_cv][23] = '0';
            if(count($arr_vbhtqh)>0){
                for($i=0;$i<count($arr_vbhtqh);$i++){
                    if($arr_vbhtqh[$i]['kh_id'] >0)$count_arr[$key_cv][23] .= ','.$arr_vbhtqh[$i]['kh_id'];
                }
            }
            



            $count_arr[$key_cv][4] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =1      ");
            $count_arr[$key_cv][5] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and chatluong =2      ");
            $count_arr[$key_cv][6] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$phong[$id_cb]." and active = 5 and sangtao =2      ");
            $count_arr[$key_cv][7] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$phong[$id_cb]." and active < 5      ");

            

            //echo " thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$phong[$id_cb]." and active < 5      "."<br>";

            $count_arr[$key_cv][8] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$phong[$id_cb]." and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')     ");
            $count_arr[$key_cv][9] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$phong[$id_cb]." and active < 5 and ngay_han < '".$ngaythangnamhan."' and ngay_han >'2017-01-01'      ");//" and vanban_id not in(select distinct(vanban_id) from kehoach where active = 1 AND (thuc_hien = 1) AND tuan in (".$tuan.") )");
            //echo "kh_id"," thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$phong[$id_cb]." and active < 5 and ngay_han < '".$ngaythangnamhan."' and ngay_han >'2017-01-01'      ";
             $arr_vbchtqh =   $this->Mdanhmuc->layVBQuaHanThang(" thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$phong[$id_cb]." and active < 5 and ngay_han < '".$ngaythangnamhan."' and ngay_han >'2017-01-01'   ");
            $count_arr[$key_cv][24] = '0';
            if(count($arr_vbchtqh)>0){
                for($i=0;$i<count($arr_vbchtqh);$i++){
                    if($arr_vbchtqh[$i]['kh_id'] >0)$count_arr[$key_cv][24] .= ','.$arr_vbchtqh[$i]['kh_id'];
                }
            }
            $count_arr[$key_cv][0]['tong'] = $count_arr[$key_cv][1]['tong'] + $count_arr[$key_cv][7]['tong'];

            $check = $count_arr[$key_cv][0]['tong'] - $count_arr[$key_cv][8]['tong'];
            if($check >0){
                $tongdiemthuong1 =  ($count_arr[$key_cv][6]['tong'] * 2)/count($arr_tuan );
                if($tongdiemthuong1>10)$tongdiemthuong1 = 10;
                $count_arr[$key_cv][10] = round($count_arr[$key_cv][1]['tong']/$check*100,1);
                $count_arr[$key_cv][22]['diemtudong'] = $count_arr[$key_cv][10] * 0.6 + 30 - ($count_arr[$key_cv][3]['tong']*0.2) +$tongdiemthuong1 ;
            }else{
                $count_arr[$key_cv][10] ='0';
                $count_arr[$key_cv][22]['diemtudong'] = 90 - ($count_arr[$key_cv][3]['tong']*0.5);
            }
            $count_arr[$key_cv][11] = $tencb;
            $count_arr[$key_cv][12] = $id_cb;
            $count_arr[$key_cv][13] = $this->Mdanhmuc->countTP_thang('diem_tong','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][14] = $this->Mdanhmuc->countTP_thang('danhgia_ld','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][15] = $this->Mdanhmuc->countTP_thang('danhgia','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][16] = $this->Mdanhmuc->countTP_thang('diem_tong_tp','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][17] = $this->Mdanhmuc->countTP_thang('danhgia_lds','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][18] = $this->Mdanhmuc->countTP_thang('danhgia_cvp','canbo_id ='.$id_cb.' and thang = '.$month.'');
            if($count_arr[$key_cv][18] !=''){
                $count_arr[$key_cv][18]['danhgia_cvp']= substr($count_arr[$key_cv][18]['danhgia_cvp'],89);
            }
            $count_arr[$key_cv][19] = $this->Mdanhmuc->countTP_thang('danhgia_pgd','canbo_id ='.$id_cb.' and thang = '.$month.'');
            $count_arr[$key_cv][20] = round($count_arr[$key_cv][2]['tong']/$count_arr[$key_cv][1]['tong']*100,1);
            $count_arr[$key_cv][21] = round($count_arr[$key_cv][3]['tong']/$count_arr[$key_cv][1]['tong']*100,1);

            $count_arr[$key_cv][25]['danhgia_canhan'] = $this->Mdanhmuc->countTP_thang('danhgia_canhan','canbo_id ='.$id_cb.' and thang = '.$month.'')['danhgia_canhan'];

            $key_cv++;
        }//pr($count_arr);
         //unset($count_arr[11]);
       

        $data['title']    = 'Đánh giá kế hoạch công tác tháng';
        $data['phong_id'] = $this->_session['FK_iMaPhongHD'];
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['PK_iMaCB'] = $this->_session['PK_iMaCB'];

        $data['count_arr']   = $count_arr;
        //$data['count_arr1']   = $count_arr1;
        $data['month'] = $month;
        $data['list_user'] = $list_user;
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/VdskehoachcongtacthangTP_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtacthang_tp';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}
   
}

/* End of file Cdskehoachcongtac.php */
/* Location: ./application/controllers/vanban/Cdskehoachcongtac.php */