<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctrinhlanhdao extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{    
        $action = _post('action');
        if($action=='capnhat')
        {
            $this->capnhat();
        }
        if($action =='danhgia')
        {
            $this->danhgia();
        }
        if(_post('luuchuahoanthanh')){
            $data['content'] = $this->chuahoanthanh();
        }
        if(_post('LDdanhgia')){
            $data['content'] = $this->LDdanhgia();
        }
        if(_post('ldduyet')){
            $data['content'] = $this->ldduyet();
        }


        $week = (int)date("W", strtotime(date('Y-m-d')));
        if(_post('tuan')>0 && !isset($_POST['luudulieu'])) $week = _post('tuan'); 

        $year = date('Y');
        $arr=array();
        for($i=0;$i<53;$i++){
            $arr_week_start_end[$i+1] = array();
            $custom_date = strtotime( date('d-m-Y', strtotime('01-01-'.$year)+($i*7*86400)) ); 
            $week_start = date('d/m/Y', strtotime('this week monday', $custom_date));
            $week_end = date('d/m/Y', strtotime('this week sunday', $custom_date));

            $week_start1 = date('Y-m-d', strtotime('this week monday', $custom_date));
            $week_end1 = date('Y-m-d', strtotime('this week sunday', $custom_date));
            
            array_push($arr,$week_start." - ".$week_end);
            array_push($arr_week_start_end[$i+1],$week_start1);
            array_push($arr_week_start_end[$i+1],$week_end1);
        }//pr($arr_week_start_end);       
        
        $canbo = $this->Mdanhmuc->get_kehoach3('FK_iMaPhongHD = 73 and iTrangThai =0','tbl_canbo','iQuyenHan_DHNB');

        $phong_ph = $this->Mdanhmuc->get_kehoach5('(PK_iMaPB < 30 or PK_iMaPB = 68 ) and iTrangThai = 0','tbl_phongban','PK_iMaPB');

        $array_pb= array();
        foreach ($phong_ph as $key_pb => $value_pb) {
           $array_pb[$value_pb['PK_iMaPB']]=$value_pb['sTenPB'];
        }

        if( $this->_session['iQuyenHan_DHNB'] < 4 or  $this->_session['iQuyenHan_DHNB'] > 5){
            $list_kh = $this->Mdanhmuc->get_kehoach5('(phong_id ='.$this->_session['FK_iMaPhongHD'].'  or phong_ph like "%'.$this->_session['FK_iMaPhongHD'].'%" ) ','kehoachtrinhld','kh_id');
        }else{
            $list_kh = $this->Mdanhmuc->get_kehoach5('lanhdao_id ='.$this->_session['PK_iMaCB'].'  ','kehoachtrinhld','kh_id');
        }
               
        foreach ($list_kh as $key => $value) {
            $list_kh[$key]['phong_phoihop'] ='';
            $list_kh[$key]['arr_phoihop'] ='';
            $list_kh[$key]['file_phoihop']='';
            $list_kh[$key]['file_chutri']='';
            $list_kh[$key]['active_phoihop']='';
            $arr_phoihop = array();
            if($value['phong_ph']!=''){
                $phong_phoihop = $this->Mdanhmuc->get_kehoach5('PK_iMaPB in('.$value['phong_ph'].')','tbl_phongban','PK_iMaPB');
                $dsphong_ph='';
                foreach ($phong_phoihop as $ph => $tenphong) {
                    if($dsphong_ph == ''){
                        $dsphong_ph = $tenphong['sTenPB'];
                    }else{
                        $dsphong_ph .= ", ".$tenphong['sTenPB'];
                    }
                    array_push($arr_phoihop,$tenphong['PK_iMaPB']);
                }
                $list_kh[$key]['phong_phoihop']=  $dsphong_ph;  
                $list_kh[$key]['arr_phoihop'] = $arr_phoihop;   

                $file_phoihop = $this->Mdanhmuc->getFileLD("kehoach_id = ".$value['kh_id']." and chutri=2",'ketqua_ldgiao');
                if(count($file_phoihop) > 0){
                    $list_kh[$key]['file_phoihop']= $file_phoihop;
                }
                $file_phoihop1 = $this->Mdanhmuc->getFileLD("kehoach_id = ".$value['kh_id']." and chutri=2 and phong_id =".$this->_session['FK_iMaPhongHD'],'ketqua_ldgiao');
                if(count($file_phoihop1) > 0){
                    $list_kh[$key]['active_phoihop']= $file_phoihop1[0]['active'];
                }

                $file_chutri = $this->Mdanhmuc->getFileLD_One("kehoach_id = ".$value['kh_id']." and chutri=1",'ketqua_ldgiao');
                if(count($file_chutri) > 0){
                    $list_kh[$key]['file_chutri']= $file_chutri[0]['file_upload'];
                }
                            
            }       
        }

        $giatri ='';
        if($this->uri->segment(2) > 0){
            $kh_id = $this->uri->segment(2);
            $giatri = $this->Mdanhmuc->layDuLieu('kh_id',$kh_id,'kehoachtrinhld');
        }else{
            $kh_id =0;
        }

        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->themKeHoach($kh_id);
        }	
           
        $data['kiemtra'] = 1;
        $data['giatri'] = $giatri;
        $data['title']    = 'Nhập kế hoạch công tác';
        $data['arr']   = $arr;
        $data['tuan_ct']   = $arr[$week-1];
        $data['week'] = $week;
        $data['canbo'] = $canbo;
        $data['phong_ph'] = $phong_ph;
        $data['list_kh'] = $list_kh;//pr($list_kh);
        $data['kh_id'] = $kh_id;
        $data['array_pb'] = $array_pb;
        $data['iQuyenHan_DHNB'] = $this->_session['iQuyenHan_DHNB'];
        $data['PK_iMaCB'] = $this->_session['PK_iMaCB'];
        $data['PK_iMaPB'] = $this->_session['PK_iMaPB'];
        $temp['data']     = $data;     
       
        $temp['template'] = 'kehoachcongtac/Vtrinhlanhdao';
        $this->load->view('layout_admin/layout',$temp);
	}
    
    public function ldduyet(){
        $id = _post('kehoach_id');
        $trangthai = _post('trangthai');
        $lydo = _post('lydo');
        if($trangthai == 2){
            $active=2;
            $data_update = array("ngay_ld_khongduyet"=>date('Y-m-d H:i:s'),"active" => $active,'lydo_khongduyet'=>$lydo);
        }else{
            $active=3;
            $data_update = array("ngay_ld_duyet"=>date('Y-m-d H:i:s'),"active" => $active,'lydo_duyet'=>$lydo);
        } 
        $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoachtrinhld",$data_update);
        redirect("trinhlanhdao");
    }

    public function LDdanhgia(){
        $id = _post('kehoach_id');
        $lydo = _post('lydo');
        if(_post('chatluong')){
            $chatluong= _post('chatluong');
            $data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"chat_luong" =>_post('chatluong'),'lanhdao_danhgia'=>$lydo);
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoachtrinhld",$data_update);
        
         redirect("trinhlanhdao");
        }
    }

    public function chuahoanthanh(){
        $kehoach_id = _post('kehoach_id');
        $canbo_id = _post('canbo_id');
        $lydo = _post('lydo');
        $phoi_hop = _post('phongphoihop');
        $name = $_FILES['files']['name'];
        $ngay_han = date('Y-m-d',strtotime(str_replace('/', '-', _post('ngay_han'))));
        $time = time();
        $thoigianluu = date('Y-m-d H:i:s');
        $ngay_nhap = date('Y-m-d');
        $trangthai = _post('trangthai');
        $mangfile='';
        if(!empty($name[0]))
        { 
            foreach ($name as $key => $value) {
                $dauphay=($key>0)?';':'';
                $mangfile = $mangfile.$dauphay.$time."_".clear($value);
            }
            $this->upload('kehoach_'.date('Y'),$time);
        }

        if($phoi_hop == 1 && $trangthai == 1){
            $arrayUpdate = array('ketqua_hoanthanh' => $lydo, 'ngay_hoanthanh' => $thoigianluu, 'active' => 5 );
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$kehoach_id,"kehoachtrinhld",$arrayUpdate);

        }
        if($phoi_hop == 1 && $trangthai == 2){
            $arrayUpdate = array('khokhan_vuongmac' => $lydo, 'ngay_hoanthanh' => $thoigianluu);
            $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$kehoach_id,"kehoachtrinhld",$arrayUpdate);
        }
        
        $arrayName = array('kehoach_id' => $kehoach_id,'lydo' => $lydo,'file_upload' => $mangfile, 'thoigianluu' => $thoigianluu, 'active' => $trangthai, 'canbo_id' => $this->_session['PK_iMaCB'], 'phong_id' => $this->_session['FK_iMaPhongHD'], 'chutri' => $phoi_hop,'ngay_han' => $ngay_han,'ngay_nhap' => $ngay_nhap); 

        $delete_kehoachFile = $this->Mdanhmuc->xoa_kehoach_file(' kehoach_id='.$kehoach_id.' and canbo_id = '.$this->_session['PK_iMaCB'],'ketqua_ldgiao');
        $check_insert = $this->Mdanhmuc->themDuLieu('ketqua_ldgiao',$arrayName);
        redirect("trinhlanhdao");
    }
    public function upload($dir,$time)
    {
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
                    $_FILES['files']['name']     = $file['files']['name'][$i];
                    $_FILES['files']['type']     = $file['files']['type'][$i];
                    $_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
                    $_FILES['files']['error']    = $file['files']['error'][$i];
                    $_FILES['files']['size']     = $file['files']['size'][$i];
                    if(is_dir($dir)==false){
                        mkdir($dir);        // Create directory if it does not exist
                    }
                    $config['upload_path']   = $dir.'/';
                    $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
                    $config['overwrite']     = true;
                    $config['file_name']     = $time."_".clear($_FILES['files']['name']);
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
    }
    // chuyên viên tự kết thúc công việc của mình
    public function capnhat()
    {
        $id = _post('id');
        $chatluong = _post('chatluong');
        if($chatluong >0)$data_update = array("ngay_hoanthanh" => date('Y-m-d H:i:s'),"active" => 2,'loai_kh' => $chatluong);
        else $data_update = array("ngay_hoanthanh" => date('Y-m-d H:i:s'),"active" => 2);
        $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
        echo json_encode($check_update);exit();
    }
    //lãnh đạo phòng đánh giá mức hoàn thành của chuyên viên
    public function danhgia()
    {
        $id = _post('id');
        $chatluong= _post('chatluong');
        if($chatluong == 3 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 3,"chatluong"=>1,"sangtao"=>2);
        if($chatluong == 2 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 3,"chatluong"=>2,"sangtao"=>1);
        if($chatluong == 1 )$data_update = array("ngay_danhgia" => date('Y-m-d H:i:s'),"active" => 3,"chatluong"=>1,"sangtao"=>1);
        $check_update = $this->Mdanhmuc->capnhatDuLieu('kh_id',$id,"kehoach",$data_update);
        echo json_encode($check_update);exit();
    }

    // thêm kế hoạch công tác tuần
    public function themKeHoach($kh_id)
    {
        if ($this->input->post('luudulieu')) {
            $ngay_han = str_replace('/', '-',_post('ngay_han'));
            $active = 1;              
            $tuan = (int)_post('tuan');
            if( $this->_session['iQuyenHan_DHNB'] < 4 or  $this->_session['iQuyenHan_DHNB'] > 5){
                $lanhdaoduyet = array(
                    'lanhdao_id' => _post('lanhdao_id'),
                    'phong_id' => $this->_session['FK_iMaPhongHD'],
                    'kh_noidung' => _post('kh_noidung'),
                    'tuan'   => $tuan,
                    'date_nhap' => date('Y-m-d H:i:s'),
                    'ngay_han' =>  date('Y-m-d',strtotime($ngay_han)),
                    'user_input' =>  $this->_session['PK_iMaCB'],
                    'phong_ph' =>   _post('phongban_ph'),
                    'active' => $active      
                );
            }else{
                $lanhdaoduyet = array(
                    'lanhdao_id' => $this->_session['PK_iMaCB'],
                    'phong_id' =>_post('phong_id'),
                    'kh_noidung' =>_post('kh_noidung'),
                    'tuan'   => $tuan,
                    'date_nhap' => date('Y-m-d H:i:s'),
                    'ngay_han' =>  date('Y-m-d',strtotime($ngay_han)),
                    'user_input' =>  $this->_session['PK_iMaCB'],
                    'phong_ph' =>   _post('phongban_ph'),
                    'active' => 3      
                );
            }
            $this->_giatri = $lanhdaoduyet;
           //thêm mới dữ liệu
            if($kh_id == 0){
                $check_insert = $this->Mdanhmuc->themDuLieu('kehoachtrinhld',$lanhdaoduyet);
                redirect("trinhlanhdao");
            }else{
                $check_insert = $this->Mdanhmuc->capnhatDuLieu('kh_id',$kh_id,'kehoachtrinhld',$lanhdaoduyet);
                redirect("trinhlanhdao");
            }
            
            if ($check_insert > 0) {
                $this->_giatri ='';
                return messagebox('Thêm thành công ', 'info');
            } else {
                return messagebox('Thêm thất bại', 'danger');
            } 
            
        }
    }

}

/* End of file Ckehoachcongtac.php */
/* Location: ./application/controllers/vanban/Ckehoachcongtac.php */