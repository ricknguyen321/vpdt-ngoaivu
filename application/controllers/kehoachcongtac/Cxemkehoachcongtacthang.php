<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cxemkehoachcongtacthang extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{
        if(isset($_POST['luudulieu'])){
            $data['content'] = $this->danhgiathanh();
        }

        if(isset($_POST['luudulieugd'])){
            $DG_array = array(
                    'danhgia_gd' => _post('danhgia_gd'), 
                    'nhanxet_gd' => _post('nhanxet_gd'),
                    'date_giamdoc' => date("Y-m-d H:i:s")
                );
            $key_id = _post('key_id');
            $id_cb = _post('id_cb');
            $this->Mdanhmuc->capnhatDuLieu('id',$key_id,'danhgiathang',$DG_array);
             redirect("lanhdaonhanxetthang?canbo=".$id_cb);
        } 

        // mở chấm tháng
        $where_mocham = " 1=1 and ketthuc >= '".date('Y-m-d H:i:s')."' and canbo = ".$this->_session['PK_iMaCB'];
        $data['mocham'] = $this->Mdanhmuc->get_kehoach3($where_mocham,'tbl_mochamthang','');


        $macanbo  = $this->uri->segment(2);
        $thang    = $this->uri->segment(3);
        $quyenhan = $this->_session['iQuyenHan_DHNB'];
        $chucvu   = $this->_session['FK_iMaCV'];
        $phongban = $this->_session['FK_iMaPhongHD'];
        $taikhoan = $this->_session['PK_iMaCB'];
        $data['macanbo']  = $macanbo;
        $data['taikhoan'] = $taikhoan;
        $data['quyenhan'] = $quyenhan;
        $data['chucvu']   = $chucvu;
        $data['phongban'] = $phongban;
        $data['nhanxet_phophong'] = $this->Mkehoachcongtac->kiemtranhanxetthang($taikhoan,$macanbo,$thang);
        //$tuan = $this->uri->segment(2);
        
        $month = $this->uri->segment(3);
        if(empty($month))$month = date('m') - 1;
        if($month ==0)$month =1;
        if(isset($_POST['month']))$month = $_POST['month'];
        //$tempt = $this->uri->segment(3);
        //if(isset($tempt) && $tempt >0 ) $month = $tempt;
        //echo $month;
        $year = date('Y');
        $beg = (int) date('W', strtotime("first thursday of $year-$month"));
        $end = (int) date('W', strtotime("last  thursday of $year-$month"));

        $tuan = (join(', ', range($beg, $end)));
        $arr_tuan = explode(',',$tuan);
        
        if($month == 1)$arr_tuan = array(1,2,3,5);

//lay ma can bo
//      if($this->_session['iQuyenHan_DHNB']<7){
        $check = $this->uri->segment(2);
        if(!empty($check)){
            $canbo_id=$check;
        }else{
            $canbo_id=$this->_session['PK_iMaCB'];
        } 
    


        $quyen_DHNB = $this->Mdanhmuc->get_quyen($canbo_id)['iQuyenHan_DHNB'];
        $ten_cb = $this->Mdanhmuc->get_ten($canbo_id)['sHoTen'];

        $phong_nguoidung = $this->Mdanhmuc->get_phong($canbo_id)['FK_iMaPhongHD'];
        //pr($phong_nguoidung);

//gan gia tri sang view
        $arrData = $this->Mdanhmuc->layDuLieu2('id_cb',$canbo_id,'thang',$month,'danhgiathang');            
        if(!empty($arrData)){
            $data['giatri']=$arrData[0];
        }

        $count_arr = array();//pr($arr_tuan);
        if($month > 0){
            for($i=0;$i<count($arr_tuan);$i++){
                $list_data[$i][0] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id);
                $list_data[$i][1] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active = 5");
                $list_data[$i][2] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01' or  ngay_hoanthanh IS NULL )");
                $list_data[$i][3] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
                $list_data[$i][4] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active = 5 and chatluong =1 ");
                $list_data[$i][5] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active = 5 and chatluong =2 ");
                $list_data[$i][6] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active = 5 and sangtao =2 ");
                $list_data[$i][7] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active < 5 ");
                //$list_data[$i][8] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");

                $list_data[$i][8] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active < 5 and qua_han = 1");
                //$list_data[$i][9] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");

                $list_data[$i][9] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id." and active < 5 and qua_han = 2");
				
				if( $quyen_DHNB ==10 or $quyen_DHNB ==11){
					$list_data[$i][0] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id);
	                $list_data[$i][1] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active = 5");
	                $list_data[$i][2] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01' or  ngay_hoanthanh IS NULL )");
	                $list_data[$i][3] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
	                $list_data[$i][4] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active = 5 and chatluong =1 ");
	                $list_data[$i][5] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active = 5 and chatluong =2 ");
	                $list_data[$i][6] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active = 5 and sangtao =2 ");
	                $list_data[$i][7] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active < 5 ");	
	                $list_data[$i][8] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active < 5 and qua_han = 1");	
	                $list_data[$i][9] = $this->Mdanhmuc->count_kehoach(" tuan =".$arr_tuan[$i]." and lanhdao_id=".$canbo_id." and active < 5 and qua_han = 2");	
				}
				
                $check = $list_data[$i][0] - $list_data[$i][8];
                if($check >0 && $list_data[$i][1] >0){
                    $list_data[$i][10] = round($list_data[$i][1]/$check*100,1);
                }else{
                    $list_data[$i][10] ='100';
                } 
            }
            // ý thức tổ chức kỷ luật
            $trungbinhthang1=0;
            $trungbinhthang2=0;
            $trungbinhthang3=0;
            $trungbinhthang4=0;
            for($i=0;$i<count($arr_tuan);$i++){
                $kyluat[$i][0] = $this->Mdanhmuc->count_diem("diem_1_tp"," tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id);

                //lấy nhận xét của trưởng phó phòng theo tuần
                $nhanxettp[$i] =  $this->Mdanhmuc->count_diem("nhan_xet"," tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id);
                $nhanxetpp[$i] =  $this->Mdanhmuc->count_diem("nhan_xet_1"," tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id);

                $trungbinhthang1 = (float)$trungbinhthang1 + (float)$kyluat[$i][0]['diem_1_tp'];

                $kyluat[$i][1] = $this->Mdanhmuc->count_diem("diem_2_tp"," tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id);
                $trungbinhthang2 = (float)$trungbinhthang2 + (float)$kyluat[$i][1]['diem_2_tp'];

                $kyluat[$i][2] = $this->Mdanhmuc->count_diem("diem_10_tp"," tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id);
                $trungbinhthang3 = (float)$trungbinhthang3 + (float)$kyluat[$i][2]['diem_10_tp'];

                $kyluat[$i][3] = $this->Mdanhmuc->count_diem("diem_19_tp"," tuan =".$arr_tuan[$i]." and canbo_id=".$canbo_id);
                $trungbinhthang4 = (float)$trungbinhthang4 + (float)$kyluat[$i][3]['diem_19_tp'];
            }
            $trungbinhthang1 = round($trungbinhthang1/count($arr_tuan),1);
            $trungbinhthang2 = round($trungbinhthang2/count($arr_tuan),1);
            $trungbinhthang3 = round($trungbinhthang3/count($arr_tuan),1);
            $trungbinhthang4 = round($trungbinhthang4/count($arr_tuan),1);

            $count_arr[0] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id);
            $count_arr[1] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 5");
            $count_arr[2] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01' or ngay_han IS NULL or  ngay_hoanthanh IS NULL )");
            $count_arr[3] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[4] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 5 and chatluong =1 ");
            $count_arr[5] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 5 and chatluong =2 ");
            $count_arr[6] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active = 5 and sangtao =2 ");
            $count_arr[7] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active < 5 ");
            //$count_arr[8] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");

            $count_arr[8] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active < 5 and qua_han = 1 ");
            

            //$count_arr[9] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");

            $count_arr[9] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and canbo_id=".$canbo_id." and active < 5 and qua_han = 2 ");
			
			if($quyen_DHNB ==10 or $quyen_DHNB ==11){
				$count_arr[0] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id);
	            $count_arr[1] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active = 5");
	            $count_arr[2] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01' or ngay_han IS NULL or  ngay_hoanthanh IS NULL )");
	            $count_arr[3] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01' ");
	            $count_arr[4] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active = 5 and chatluong =1 ");
	            $count_arr[5] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active = 5 and chatluong =2 ");
	            $count_arr[6] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active = 5 and sangtao =2 ");
	            $count_arr[7] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active < 5 ");
	            $count_arr[8] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active < 5 and qua_han = 1 ");
	            $count_arr[9] = $this->Mdanhmuc->count_kehoach(" tuan in(".$tuan.") and lanhdao_id=".$canbo_id." and active < 5 and qua_han = 2 ");
			}

            $check = $count_arr[0] - $count_arr[8];
            if($check >0 && $count_arr[2] > 0){
                $count_arr[10] = round($count_arr[1]/$check*100,1);
            }else{
                $count_arr[10] ='100';
            }
            
        }
        $list_user = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',$phong_nguoidung,'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
        }
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }


        $data['title']    = 'Xem kế hoạch công tác tháng';
        $data['count_arr']   = $count_arr;
        $data['arr_tuan']   = $arr_tuan;
        $data['list_data']   = $list_data;
        $data['kyluat']   = $kyluat;
        $data['nhanxetpp']   = $nhanxetpp;//pr($nhanxetpp);
        $data['nhanxettp']   = $nhanxettp;
        $data['phong_nguoidung']   = $phong_nguoidung;
        //$data['list_diem']   = $list_diem;
        $data['trungbinhthang1']   = $trungbinhthang1;
        $data['trungbinhthang2']   = $trungbinhthang2;
        $data['trungbinhthang3']   = $trungbinhthang3;
        $data['trungbinhthang4']   = $trungbinhthang4;
        $data['tongdiemthang'] = $trungbinhthang4+$trungbinhthang3+$trungbinhthang2+$trungbinhthang1;
        $data['month'] = $month;
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['PK_iMaCB'] = $this->_session['PK_iMaCB'];
        $data['phong_id'] = $this->_session['FK_iMaPhongHD'];
        $data['cb_id'] = $canbo_id;
        $data['handanhgiathang'] = date('Y-m-d', strtotime("first wednesday of this month"));
        $data['ngayhientai'] = date('Y-m-d');
        $data['ten_cb'] = $ten_cb;
        $temp['data']     = $data;//pr($data);

        if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacThang_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacThang_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vkehoachcongtacthang_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vxemkehoachcongtacthang1';
            $this->load->view('layout_admin/layout',$temp);
            /*$temp['template'] = 'kehoachcongtac/VkehoachcongtacThang_word';
            $this->load->view('layout_admin/layout_word',$temp);*/
        }
        
	}

    // cá nhân, lãnh đạo đánh giá tháng
    public function danhgiathanh()
    {
        $macanbo     = $this->uri->segment(2);
        $quyenhan    = $this->_session['iQuyenHan_DHNB'];
        $chucvu      = $this->_session['FK_iMaCV'];
        $phongban    = $this->_session['FK_iMaPhongHD'];
        $taikhoan    = $this->_session['PK_iMaCB'];
        $hoten       = $this->_session['sHoTen'];
        $phong_canbo = $this->Mdanhmuc->get_phong($macanbo)['FK_iMaPhongHD'];
        if ($this->input->post('luudulieu')) {
            if($phong_canbo==$phongban)
            {
                if($quyenhan==6 || ($quyenhan==3 && $chucvu == 6)){
                    $danhgia = array(
                        'id_cb'      => _post('canbo_id'),
                        'id_phong'   => $phongban,
                        'diem'       => _post('diem'),
                        'danhgia_tp' => _post('danhgiathang'),
                        'nhanxet'    => _post('noidungnhanxet'),
                        'thang'      => _post('thang'),
                        'date_tp'    => date('Y-m-d H:i:s'),
                        'acitve'     => 2                        
                    );
                }elseif(($phongban==12&&$quyenhan==11&&$macanbo!=$taikhoan)||($phongban!=12&&$quyenhan==7&&$macanbo!=$taikhoan))
                {
                    // kiểm tra đã nhận xét của ai chưa
                    $kiemtra = $this->Mdanhmuc->layDuLieu2('id_cb',_post('canbo_id'),'thang',_post('thang'),'danhgiathang');
                    if(!empty($kiemtra))
                    {
                        $kiemtrathang = $this->Mkehoachcongtac->kiemtranhanxetthang($taikhoan,_post('canbo_id'),_post('thang'));
                        if(!empty($kiemtrathang))
                        {
                            $nhanxetcu = '<b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.$kiemtrathang['sThoiGian'].')</i><br/>'.$kiemtrathang['sNhanXet'].'';
                            $mangnhanxet = array(
                                'sNhanXet'    => _post('noidungnhanxet'),
                                'sThoiGian'   => date('Y-m-d H:i:s')
                            );
                            $this->Mkehoachcongtac->capnhatnhanxetthang($taikhoan,_post('canbo_id'),_post('thang'),$mangnhanxet);
                        }
                        else{
                            $mangnhanxet = array(
                                'FK_iMaCB_PP' => $taikhoan,
                                'FK_iMaCB_CV' => _post('canbo_id'),
                                'tuan'        => '',
                                'thang'       => _post('thang'),
                                'id_danhgia'  => 0,
                                'sNhanXet'    => _post('noidungnhanxet'),
                                'sThoiGian'   => date('Y-m-d H:i:s')
                            );
                            $this->Mdanhmuc->themDuLieu('nhanxet_phophong',$mangnhanxet);
                            $nhanxetcu = '';
                        }
                        // kiểm tra phó phòng ai nhận xét chưa
                        $nhanxetphophong = $kiemtra[0]['nhanxet_pp'];
                        $nhanxet         = _post('noidungnhanxet');
                        $thoigan         = date('Y-m-d H:i:s');
                        if(!empty($nhanxetphophong))
                        {
                            $noidungdachinhsua = trim(str_replace($nhanxetcu,'',$nhanxetphophong));
                            $danhgia = array(
                                'id_cb'      => _post('canbo_id'),
                                'id_phong'   => $phongban,
                                'diem'       => _post('diem'),
                                'nhanxet_pp' => $noidungdachinhsua.'<br/><b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.$thoigan.')</i><br/>'.$nhanxet.'',
                                'thang'      => _post('thang'),
                                'acitve'     => 1       
                            );
                        }
                        else{
                            $danhgia = array(
                                'id_cb'      => _post('canbo_id'),
                                'id_phong'   => $phongban,
                                'diem'       => _post('diem'),
                                'nhanxet_pp' => $nhanxetphophong.'<b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.$thoigan.')</i><br/>'.$nhanxet.'',
                                'thang'      => _post('thang'),
                                'acitve'     => 1        
                            );
                        }
                    }
                    else{
                        $mangnhanxet = array(
                            'FK_iMaCB_PP' => $taikhoan,
                            'FK_iMaCB_CV' => _post('canbo_id'),
                            'tuan'        => '',
                            'thang'       => _post('thang'),
                            'id_danhgia'  => 0,
                            'sNhanXet'    => _post('noidungnhanxet'),
                            'sThoiGian'   => date('Y-m-d H:i:s')
                        );
                        $this->Mdanhmuc->themDuLieu('nhanxet_phophong',$mangnhanxet);
                        $danhgia = array(
                            'id_cb'      => _post('canbo_id'),
                            'id_phong'   => $phongban,
                            'diem'       => _post('diem'),
                            'nhanxet_pp' => '<b>Nhận xét của phó phòng '.$hoten.'</b> <i>('.date('Y-m-d H:i:s').')</i><br/>'._post('noidungnhanxet').'',
                            'thang'      => _post('thang'),     
                            'acitve'     => 1     
                        );
                    }
                    
                }
                elseif($macanbo==$taikhoan){
                    $danhgia = array(
                        'id_cb'      => _post('canbo_id'),
                        'id_phong'   => $this->_session['FK_iMaPhongHD'],
                        'diem'       => _post('diem'),
                        'danhgia_cv' =>  _post('danhgiathang'),
                        'nhanxet_cv' => _post('noidungnhanxet'),
                        'thang'      => _post('thang'),
                        'date_cv'    => date('Y-m-d H:i:s'),
                        'acitve'     => 1                     
                    );
                }
                //kiem tra du lieu ton tai chua
                $arrData = $this->Mdanhmuc->layDuLieu2('id_cb',_post('canbo_id'),'thang',_post('thang'),'danhgiathang');            
                if(empty($arrData)){
                    $check_insert = $this->Mdanhmuc->themDuLieu('danhgiathang',$danhgia);
                    redirect("dskehoachcongtacthang/0/"._post('thang'));
                }else{
                    $check_insert = $this->Mdanhmuc->capnhatDuLieu('id',$arrData[0]['id'],'danhgiathang',$danhgia);
                    redirect("dskehoachcongtacthang/0/"._post('thang'));
                }
            }            
        }
    }
 
}
    