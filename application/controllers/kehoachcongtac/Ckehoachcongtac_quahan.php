<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckehoachcongtac_quahan  extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();		      
	}
	public function index()
	{     
        $list_kh_id = _get('listkh'); 
        if(empty($list_kh_id)){
            $list_kh_id = '0';
        }  
        $canbo = $this->Mdanhmuc->get_kehoach3(' iTrangThai = 0','tbl_canbo');
        $list_kh = $this->Mdanhmuc->get_kehoach3('loai_kh > 1 and kh_id in ('.$list_kh_id.' )','kehoach');
        $list_kh1 = $this->Mdanhmuc->get_kehoach3('loai_kh = 1 and kh_id in ('.$list_kh_id.' )','kehoach');
        $data['title']    = 'Danh sách việc quá hạn';
        $data['canbo'] = $canbo;
        $data['list_kh'] = $list_kh;
        $data['list_kh1'] = $list_kh1;
        $temp['data']     = $data;
       
        $temp['template'] = 'kehoachcongtac/Vkehoachcongtac_quahan';
        $this->load->view('layout_admin/layout',$temp);
	}
    
   
}

/* End of file Ckehoachcongtac_quahan .php */
/* Location: ./application/controllers/vanban/Ckehoachcongtac_quahan .php */