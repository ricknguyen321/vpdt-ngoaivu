<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongkebaocao_thangIn extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
    public function getStartAndEndDate($week, $year) {
      $dateTime = new DateTime();
      $dateTime->setISODate($year, $week);
      //$result['start_date'] = $dateTime->format('d-M-Y');
      $dateTime->modify('+6 days');
      //$result['end_date'] = $dateTime->format('Y-m-d');
      $result = $dateTime->format('Y-m-d');
      return $result;
    }
	public function index()
	{
$thangbd = _get('thangbd');
if($thangbd >0){
            $where =' 1=1 and thang = '.$thangbd;
        $where_ld =' 1=1 and thang = '.$thangbd;            

        $data['kehoach']    = $this->Mdanhmuc->get_kehoach3aa($where,'danhgiathang','id_phong','danhgia_tp');

        $data['kehoach_ld']    = $this->Mdanhmuc->get_kehoach3aaa($where_ld,'danhgialanhdao','','diem_tong_tp,danhgia_lds');

        $year = date('Y');
        $data['thangbd']     = $thangbd;
        $data['nam']       = $year;
        $tuanbd = (int) date('W', strtotime("first thursday of $year-$thangbd"));
        $tuankt = (int) date('W', strtotime("last thursday of $year-$thangbd"));
        $tuan = (join(', ', range($tuanbd, $tuankt)));
        $arr_tuan = explode(',',$tuan);
        $tuancuoi = $tuankt;
        $year = date('Y');
        $ngaythangnamhan = $this->getStartAndEndDate($tuancuoi,$year);
        $data['tuanbd']       = $tuanbd;
        $data['tuankt']       = $tuankt;

        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('(PK_iMaPB < 22 or PK_iMaPB = 68) and iTrangThai =0','tbl_phongban','sepxep');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }

        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 2 ','tbl_canbo','sapxep');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            $data['tencanbo'][$value['PK_iMaCB']]=$value['sHoTen'];
            $data['chucvu'][$value['PK_iMaCB']]=$value['tendinhdanh'];

            $key_cb = $value['PK_iMaCB'];
            $id_cb = $value['PK_iMaCB'];
            if($value['iQuyenHan_DHNB']==5){
                $count_arr[$key_cb][1] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5     ");
                $count_arr[$key_cb][2] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuan.") and lanhdao_so = ".$id_cb." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01')     ");
                
                $count_arr[$key_cb][3] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5      ");
                $count_arr[$key_cb][4] = $this->Mdanhmuc->countLD_thang("vanban_id"," vanban_id > 0 AND thuc_hien = 1 AND tuan in (".$tuancuoi.") and lanhdao_so = ".$id_cb." and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')     ");
                

                $count_arr[$key_cb][0]['tong'] = $count_arr[$key_cb][1]['tong'] + $count_arr[$key_cb][3]['tong'];

                $check = $count_arr[$key_cb][0]['tong'] - $count_arr[$key_cb][4]['tong'];
                if($count_arr[$key_cb][1]['tong'] >0){
                    $count_arr[$key_cb][5] = round($count_arr[$key_cb][2]['tong']/$count_arr[$key_cb][1]['tong']*100,1);
                }else{
                    $count_arr[$key_cb][5] =0;
                }
                
                
                if($check >0){
                    $count_arr[$key_cb][6] = round($count_arr[$key_cb][1]['tong']/$check*100,1);
                    $count_arr[$key_cb][7]['diemtudong'] = $count_arr[$key_cb][6] * 0.6 + 30 - ((100-$count_arr[$key_cb][5])*0.2);
                }else{
                    $count_arr[$key_cb][6] ='0';
                    $count_arr[$key_cb][7]['diemtudong'] = 90- ((100-$count_arr[$key_cb][5])*0.2);
                }
            }
            if($value['iQuyenHan_DHNB']==6 or $value['iQuyenHan_DHNB']==3){
                $key_cv = $value['PK_iMaCB'];
                $count_arr[$key_cv][1] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$value['FK_iMaPhongHD']." and active = 5  ");
            
                $count_arr[$key_cv][2] = $this->Mdanhmuc->countLD_thang("kh_id","thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$value['FK_iMaPhongHD']." and active = 5 and (DATE(ngay_hoanthanh) <= ngay_han or ngay_han < '2017-01-01')     ");
                $count_arr[$key_cv][3] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$value['FK_iMaPhongHD']." and active = 5 and  DATE(ngay_hoanthanh) > ngay_han and ngay_han > '2017-01-01'      ");

                $count_arr[$key_cv][6] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuan.") and phong_id = ".$value['FK_iMaPhongHD']." and active = 5 and sangtao =2      ");
                $count_arr[$key_cv][7] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$value['FK_iMaPhongHD']." and active < 5      ");            
                $count_arr[$key_cv][8] = $this->Mdanhmuc->countLD_thang("kh_id"," thuc_hien = 1 AND tuan in (".$tuancuoi.") and phong_id = ".$value['FK_iMaPhongHD']." and active < 5 and (ngay_han >='".$ngaythangnamhan."' or ngay_han < '2017-01-01')     ");       
            
                $count_arr[$key_cv][0]['tong'] = $count_arr[$key_cv][1]['tong'] + $count_arr[$key_cv][7]['tong'];

                $check = $count_arr[$key_cv][0]['tong'] - $count_arr[$key_cv][8]['tong'];
                if($check >0){
                    $tongdiemthuong1 =  ($count_arr[$key_cv][6]['tong'] * 2)/count($arr_tuan);
                    if($tongdiemthuong1>10)$tongdiemthuong1 = 10;
                    $count_arr[$key_cv][10] = round($count_arr[$key_cv][1]['tong']/$check*100,1);
                    $count_arr[$key_cv][22]['diemtudong'] = $count_arr[$key_cv][10] * 0.6 + 30 - ($count_arr[$key_cv][3]['tong']*0.2) +$tongdiemthuong1 ;
                }else{
                    $count_arr[$key_cv][10] ='0';
                    $count_arr[$key_cv][22]['diemtudong'] = 90 - ($count_arr[$key_cv][3]['tong']*0.5);
                }
            }
         
        }  
        
        $data['count_arr']   = $count_arr;
        $data['title']    = 'Thống kê báo cáo tháng';
        $temp['data']     = $data;
       
        if(isset($_GET['ketxuatword'])){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=ThongKeBaoCaoThang_".date('d-m-Y').".doc");
        }else{
           header("Content-Type: application/vnd.ms-excel");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=ThongKeBaoCaoThang_".date('d-m-Y').".xls"); 
        }
        /*header("Content-Type: application/vnd.ms-word");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header("Content-disposition: attachment; filename=ThongKeBaoCaoThang_".date('d-m-Y').".doc");*/
        $temp['template'] = 'kehoachcongtac/Vthongkebaocao_thangIn_word';
        $this->load->view('layout_admin/layout_word',$temp);
    }
else{   
        $data['thangbd']     = (int)date('m')-1;
        $data['title']    = 'Thống kê báo cáo tháng';
        $temp['data']     = $data;
        $temp['template'] = 'kehoachcongtac/Vthongkebaocao_thangIn';
        $this->load->view('layout_admin/layout',$temp);            
    }           
}
}
/* End of file Cthongkebaocaothang.php */
