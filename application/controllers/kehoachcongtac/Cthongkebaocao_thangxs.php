<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cthongkebaocao_thangxs extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
    
	public function index()
	{
        $thangbd = _get('thangbd');
        $thangkt = _get('thangkt');
    if($thangbd >0){
        /*$list_depart = $this->Mdanhmuc->get_kehoach3('(PK_iMaPB < 22 or PK_iMaPB = 68) and iTrangThai =0','tbl_phongban','sepxep');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }*/

        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and iQuyenHan_DHNB > 2 and (FK_iMaPhongHD < 22 or FK_iMaPhongHD = 68 or FK_iMaPhongHD = 73)','tbl_canbo','FK_iMaPhongHD,iQuyenHan_DHNB');
        foreach ($list_user as $key => $value) {
            $data['tencanbo'][$value['PK_iMaCB']]=$value['sHoTen'];
            $data['chucvu'][$value['PK_iMaCB']]=$value['tendinhdanh'];
            if($value['PK_iMaCB'] == 676 or $value['PK_iMaCB'] == 81 or $value['PK_iMaCB'] == 136 or $value['PK_iMaCB'] == 577 or $value['PK_iMaCB'] == 627  or $value['PK_iMaCB'] == 629  or $value['PK_iMaCB'] == 617 ) continue;
            $key_cb = $value['PK_iMaCB'];
            $key_pb = $value['FK_iMaPhongHD'];
            if($value['iQuyenHan_DHNB']==5){
                for($i=$thangbd;$i<=$thangkt;$i++){
                    $count_arrld[$key_cb][$i] = $this->Mdanhmuc->danhgialdso("danhgia_lds"," thang =".$i." and canbo_id = ".$key_cb." and active >= 2 ","danhgialanhdao");
                }
            }
            /*if( $value['iQuyenHan_DHNB']==6 or $value['iQuyenHan_DHNB']==3){
                for($i=$thangbd;$i<=$thangkt;$i++){
                    $count_arr[$key_cb][$i] = $this->Mdanhmuc->danhgialdso("danhgia_lds"," thang =".$i." and canbo_id = ".$key_cb." and active >= 2 ","danhgialanhdao");
                }
            }*/
            if($value['iQuyenHan_DHNB']>=6 or  $value['iQuyenHan_DHNB']==3){
                if( $value['iQuyenHan_DHNB']==6 or $value['iQuyenHan_DHNB']==3){
                    $bangdulieu = "danhgialanhdao";
                    $canbo_id = " and canbo_id ";
                    $danhgia_gd ="danhgia_lds";
                    $active = " and active >= 2 ";
                }else{ 
                    $bangdulieu = "danhgiathang";
                    $canbo_id = " and id_cb ";
                    $danhgia_gd ="danhgia_gd";
                    $active = " and acitve >= 2 ";}

                for($i=$thangbd;$i<=$thangkt;$i++){
                    if($key_pb ==11)
                        $count_arr1[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==12)
                        $count_arr2[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==13)
                        $count_arr3[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==14)
                        $count_arr4[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==15)
                        $count_arr5[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==17)
                        $count_arr6[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==19)
                        $count_arr7[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==20)
                        $count_arr8[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb ==21)
                        $count_arr9[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                    if($key_pb == 68)
                        $count_arr10[$key_cb][$i] = $this->Mdanhmuc->danhgialdso($danhgia_gd," thang =".$i. $canbo_id ."= ".$key_cb.$active,$bangdulieu);
                }
            }
         
        } 

        $arr_thang = array();
        for($i=$thangbd;$i<=$thangkt;$i++){
            array_push($arr_thang,$i);
        } 
        
        $year = $_SESSION['nam'];

        $data['arr_thang']   = $arr_thang;
        $data['thangbd']     = $thangbd;
        $data['thangkt']     = $thangkt;
        $data['socot']       = $thangkt-$thangbd +7;
        $data['nam']       = $year;
        $data['count_arrld']   = $count_arrld;
        $data['count_arr']   = $count_arr;
        $data['count_arr1']   = $count_arr1;
        $data['count_arr2']   = $count_arr2;
        $data['count_arr3']   = $count_arr3;
        $data['count_arr4']   = $count_arr4;
        $data['count_arr5']   = $count_arr5;
        $data['count_arr6']   = $count_arr6;
        $data['count_arr7']   = $count_arr7;
        $data['count_arr8']   = $count_arr8;
        $data['count_arr9']   = $count_arr9;
        $data['count_arr10']   = $count_arr10;
        $data['title']    = 'Thống kê báo cáo tháng';
        $temp['data']     = $data;
       
        if(isset($_GET['ketxuatword'])){
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=ThongKeBaoCaoThang_".date('d-m-Y').".doc");
        }else{
           header("Content-Type: application/vnd.ms-excel");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=ThongKeBaoCaoThang_".date('d-m-Y').".xls"); 
        }

        $temp['template'] = 'kehoachcongtac/Vthongkebaocao_thangxs1';
        $this->load->view('layout_admin/layout_word',$temp);

       
    }
else{   
        $data['thangbd']     = 1;
        $data['thangkt']     = 12;

        if(_get('thangbd')>0)$data['thangbd']     = _get('thangbd');
        if(_get('thangkt')>0)$data['thangkt']     = _get('thangkt');

        $data['title']    = 'Thống kê báo cáo tháng';
        $temp['data']     = $data;
        $temp['template'] = 'kehoachcongtac/Vthongkebaocao_thangIn1';
        $this->load->view('layout_admin/layout',$temp);            
    }           
}
}
/* End of file Cthongkebaocaothang.php */
