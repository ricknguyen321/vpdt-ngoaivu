<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdskehoachcongtacquy extends MY_Controller {
    protected $_giatri;
	public function __construct()
	{
		parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc','Mdanhmuc');
        $this->Mdanhmuc = new Mdanhmuc();
		$this->load->model('kehoachcongtac/Mkehoachcongtac','Mkehoachcongtac');
		 $this->Mkehoachcongtac = new Mkehoachcongtac();        
	}
	public function index()
	{   
        $phong_id = $this->_session['FK_iMaPhongHD'];
        
        $month = date('m');
        $year = date('Y');
        if($month == 1 || $month == 2 || $month == 3)$quy = 1;
        if($month == 4 || $month == 5 || $month == 6)$quy = 2;
        if($month == 7 || $month == 8 || $month == 9)$quy = 3;
        if($month == 10 || $month == 11 || $month == 12)$quy = 4;

        if(isset($_POST['quy']))$quy = $_POST['quy'];
        $tempt = $this->uri->segment(3);
        if(isset($tempt) && $tempt >0 ) $quy = $tempt;
        $tempt1 = $this->uri->segment(2);
        if(isset($tempt1) && $tempt1 >0 ) $phong_id = $tempt1;

        if($quy==1){
            $month1 = 1;$month2 = 3;
        }else if($quy==2){
            $month1 = 4;$month2 = 6;
        }else if($quy==3){
            $month1 = 7;$month2 = 9;
        }else{
            $month1 = 10;$month2 = 12;
        }
        $beg = (int) date('W', strtotime("first thursday of $year-$month1"));
        $end = (int) date('W', strtotime("last thursday of $year-$month2"));
        $week = (int)date("W", strtotime(date('Y-m-d')));
        if($week < $end) $end = $week;
        $tuan = (join(', ', range($beg, $end)));
//lay ma can bo
        /*$check = $this->uri->segment(2);
        if(!empty($check)){
            $canbo_id=$check;
        }else{
            $canbo_id=$this->_session['PK_iMaCB'];
        }*/ 

        //phong ban
        $list_depart = $this->Mdanhmuc->get_kehoach3('PK_iMaPB < 22 and iTrangThai =0','tbl_phongban');
        foreach ($list_depart as $key => $value) {
            $data['phongban'][$value['PK_iMaPB']]=$value['sTenPB'];
        }
        
        $list_user = $this->Mdanhmuc->get_kehoach3(' iTrangThai=0 and FK_iMaPhongHD='.$phong_id,'tbl_canbo');
        foreach ($list_user as $key => $value) {
            $data['canbophong'][$value['PK_iMaCB']]=$value['sHoTen'];
            if($value['iQuyenHan_DHNB']==8){
                $chuyenvien[$value['PK_iMaCB']]=$value['sHoTen'];
            }else{
                if($value['iQuyenHan_DHNB']>6 && $value['iQuyenHan_DHNB']!=9)$lanhdao[$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        
        //lay tổng gia tri cac cong viec cua tung chuyen vien
        $key_cb=0;
        foreach ($lanhdao as $id_cb => $tencb) {
            $count_arr[$key_cb][0] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and kh_id_sub is NULL");
            $count_arr[$key_cb][1] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3");
            $count_arr[$key_cb][2] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[$key_cb][3] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cb][4] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and chatluong =1 ");
            $count_arr[$key_cb][5] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and chatluong =2 ");
            $count_arr[$key_cb][6] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and sangtao =2 ");
            $count_arr[$key_cb][7] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and canbo_id=".$id_cb." and active < 3 ");
            $count_arr[$key_cb][8] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and canbo_id=".$id_cb." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[$key_cb][9] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and canbo_id=".$id_cb." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");
            $check = $count_arr[$key_cb][0] - $count_arr[$key_cb][8];
            if($check >0){
                $count_arr[$key_cb][10] = round($count_arr[$key_cb][1]/$check*100,1);
            }else{
                $count_arr[$key_cb][10] ='0';
            }
            $count_arr[$key_cb][11] = $tencb;
            $count_arr[$key_cb][12] = $id_cb;
            $count_arr[$key_cb][13] = $this->Mdanhmuc->count_quy('diem','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $count_arr[$key_cb][14] = $this->Mdanhmuc->count_quy('danhgiaquy_ld','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $key_cb++;
        }
        $key_cv=11;
        foreach ($chuyenvien as $id_cb => $tencb) {
            $count_arr[$key_cv][0] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb);
            $count_arr[$key_cv][1] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3");
            $count_arr[$key_cv][2] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr[$key_cv][3] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr[$key_cv][4] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and chatluong =1 ");
            $count_arr[$key_cv][5] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and chatluong =2 ");
            $count_arr[$key_cv][6] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and canbo_id=".$id_cb." and active = 3 and sangtao =2 ");
            $count_arr[$key_cv][7] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and canbo_id=".$id_cb." and active < 3 ");
            $count_arr[$key_cv][8] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and canbo_id=".$id_cb." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr[$key_cv][9] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and canbo_id=".$id_cb." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");
            $check = $count_arr[$key_cv][0] - $count_arr[$key_cv][8];
            if($check >0){
                $count_arr[$key_cv][10] = round($count_arr[$key_cv][1]/$check*100,1);
            }else{
                $count_arr[$key_cv][10] ='0';
            }
            $count_arr[$key_cv][11] = $tencb;
            $count_arr[$key_cv][12] = $id_cb;
            $count_arr[$key_cv][13] = $this->Mdanhmuc->count_quy('diem','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $count_arr[$key_cv][14] = $this->Mdanhmuc->count_quy('danhgiaquy_ld','cb_id ='.$id_cb.' and quy = '.$quy.'');
            $key_cv++;
        }//pr($count_arr);
         
        // tổng số trung bình của phòng
        $count_arr1[0] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id);
            $count_arr1[1] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 3");
            $count_arr1[2] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 3 and (ngay_hoanthanh <= ngay_han or ngay_han < '2017-01-01')");
            $count_arr1[3] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 3 and  ngay_hoanthanh > ngay_han and ngay_han > '2017-01-01' ");
            $count_arr1[4] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 3 and chatluong =1 ");
            $count_arr1[5] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 3 and chatluong =2 ");
            $count_arr1[6] = $this->Mdanhmuc->count_kehoach(" tuan in (".$tuan.") and phong_id = ".$phong_id." and active = 3 and sangtao =2 ");
            $count_arr1[7] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and phong_id = ".$phong_id." and active < 3 ");
            $count_arr1[8] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and phong_id = ".$phong_id." and active < 3 and (ngay_han >='".date('Y-m-d')."' or ngay_han < '2017-01-01')");
            $count_arr1[9] = $this->Mdanhmuc->count_kehoach(" tuan in (".$end.") and phong_id = ".$phong_id." and active < 3 and ngay_han < '".date('Y-m-d')."'  and ngay_han >'2017-01-01'");
            $check = $count_arr1[0] - $count_arr1[8];
            if($check >0){
                $count_arr1[10] = round($count_arr1[1]/$check*100,1);
            }else{
                $count_arr1[10] ='0';
            }
               

        $data['title']    = 'Đánh giá kế hoạch công tác tháng';
        $data['phong_id'] = $phong_id;
        $data['quyen'] = $this->_session['iQuyenHan_DHNB'];
        $data['count_arr']   = $count_arr;
        $data['count_arr1']   = $count_arr1;
        $data['quy'] = $quy;
        //$data['week'] = $tuan;
        $data['list_user'] = $list_user;
        $temp['data']     = $data;
       
       if(isset($_POST['ketxuatword']) || isset($_POST['ketxuatexcel'])){
            if(isset($_POST['ketxuatword'])){
                header("Content-Type: application/vnd.ms-word");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("Content-disposition: attachment; filename=DanhGiaCongTacQuy_".date('d-m-Y').".doc");
            }else{
               header("Content-Type: application/vnd.ms-excel");
                header("Expires: 0");
                header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
                header("content-disposition: attachment;filename=DanhGiaCongTacQuy_".date('d-m-Y').".xls"); 
            }
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtacquy_word';
            $this->load->view('layout_admin/layout_word',$temp);
        }else{
            $temp['template'] = 'kehoachcongtac/Vdskehoachcongtacquy';
            $this->load->view('layout_admin/layout',$temp);            
        }
        
	}
   
}

/* End of file Cdskehoachcongtac.php */
/* Location: ./application/controllers/vanban/Cdskehoachcongtac.php */