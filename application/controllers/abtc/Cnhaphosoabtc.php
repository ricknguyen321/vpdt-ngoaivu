<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhaphosoabtc extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		$ma = _get('id');
		if ($ma > 0) {
			
			$data['tailieu'] = $this->Mdanhmuc->layDuLieu('FK_iMaABTC',$ma,'tbl_files_abtc');

			if(_post('xoa'))
			{
				$data['content'] = $this->xoaDuLieu($ma);
			}
		}
		if(!empty($ma))
		{			
			$data['content'] = $this->capnhatDuLieu($ma);						
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin']	  = $this->_thongtin;
		
		$data['phongban'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB','','tbl_phongban');		
		
		$data['dscbls']	  = $this->Mdanhmuc->layDuLieu('FK_iMaPhongHD',76,'tbl_canbo');
		
		$giomoi = $this->_thongtin[0]['sGioMoi'];
	  
		$data['title']        = 'Nhập mới hồ sơ ABTC';
		$temp['data']         = $data;
		$temp['template']     = 'abtc/Vnhaphosoabtc';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_hoso_abtc');
		if(_post('luudulieu'))
		{
			$data=array(
				'don_vi'		   		=> _post('don_vi'),
				'so_hieu'        		=> _post('so_hieu'),
				'ngay_den'     			=> date_insert(_post('ngay_den')),
				'so_luong_the'          => _post('so_luong_the'),
				'nguoi_duoc_cap'      	=> _post('nguoi_duoc_cap'),
				'cb_xu_ly'    			=> _post('cb_xu_ly'),
				'mst'    				=> _post('mst'),
				'diachi'    			=> _post('diachi'),
				'vb_den'    			=> _post('vb_den'),
				'vb_di'    				=> _post('vb_di'),
				'vb_dn'    				=> _post('vb_dn'),
				'vb_ca'    				=> _post('vb_ca'),				
				'vb_thue'    			=> _post('vb_thue'),
				'vb_bhxh'    			=> _post('vb_bhxh'),
				'vb_ubnd'    			=> _post('vb_ubnd'),
				'vb_ca_tl'    			=> _post('vb_ca_tl'),
				'vb_thue_tl'    		=> _post('vb_thue_tl'),
				'vb_bhxh_tl'    		=> _post('vb_bhxh_tl'),
				'vb_kq'    				=> _post('vb_kq'),
				'trang_thai'    		=> _post('trang_thai'),				
				'note'    				=> _post('note')
			);
				
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_hoso_abtc',$data);
			$name    = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('doc_abtc_'.date('Y'), $time);
				$files = array();
				foreach ($name as $key => $value) {
					$val = clear($value);
					$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaABTC' => $ma,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_abtc_'.date('Y').'/tl_abtc_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
					);
				}
				$kiemtrafile=$this->Mdanhmuc->themNhieuDuLieu('tbl_files_abtc',$files);
				
			}
			if(empty(_post('don_vi')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập đơn vị!','danger');
			}
						
			redirect('hosoabtc?id='.$ma);
		}
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_abtc');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		redirect('nhaphosoabtc?id='.$mavanban);
		
	}
	public function themDuLieu()
	{
		$time = time();
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if(_post('luudulieu'))
		{
			$data=array(
				'don_vi'		   		=> _post('don_vi'),
				'so_hieu'        		=> _post('so_hieu'),
				'ngay_den'     			=> date_insert(_post('ngay_den')),
				'so_luong_the'          => _post('so_luong_the'),
				'nguoi_duoc_cap'      	=> _post('nguoi_duoc_cap'),
				'cb_xu_ly'    			=> _post('cb_xu_ly'),
				'mst'    				=> _post('mst'),
				'diachi'    			=> _post('diachi'),
				'vb_den'    			=> _post('vb_den'),
				'vb_di'    				=> _post('vb_di'),
				'vb_dn'    				=> _post('vb_dn'),
				'vb_ca'    				=> _post('vb_ca'),
				'vb_thue'    			=> _post('vb_thue'),
				'vb_bhxh'    			=> _post('vb_bhxh'),
				'vb_ubnd'    			=> _post('vb_ubnd'),				
				'vb_ca_tl'    			=> _post('vb_ca_tl'),
				'vb_thue_tl'    		=> _post('vb_thue_tl'),
				'vb_bhxh_tl'    		=> _post('vb_bhxh_tl'),
				'vb_kq'    				=> _post('vb_kq'),
				'FK_iMaCBNhap'    		=> $this->_session['PK_iMaCB'],
				'trang_thai'    		=> _post('trang_thai'),				
				'note'    				=> _post('note'),
				'ngay_nhap'        		=> date('Y-m-d H:i:s', time())
			);
			
			
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_hoso_abtc',$data);
			
			$name = $_FILES['files']['name'];
			
			
			if($id_insert>0)
			{
				
				$name = $_FILES['files']['name'];
				if(!empty($name[0]))
				{
					$this->upload('doc_abtc_'.date('Y'), $time);
					$files = array();
					foreach ($name as $key => $value) {
						$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaABTC' => $id_insert,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_abtc_'.date('Y').'/tl_abtc_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB'],
						);
					}
					$kiemtrafile = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_abtc',$files);
				
				}
				echo '<script language="javascript">';
				echo 'alert(Thêm hồ sơ ABTC THÀNH CÔNG)';  //not showing an alert box.
				echo '</script>';
				redirect('dshosoabtc');	
			}
			else{
				return messagebox('Thêm thẻ ABTC thất bại','danger');
			}
		}
	}
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'tl_abtc_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */