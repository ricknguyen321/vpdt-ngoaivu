<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhaptheabtc extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		$ma = _get('id');
		if ($ma > 0) {
			
			$data['tailieu'] = $this->Mdanhmuc->layDuLieu('FK_iMaABTC',$ma,'tbl_files_abtc');

			if(_post('xoa'))
			{
				$data['content'] = $this->xoaDuLieu($ma);
			}
		}
		if(!empty($ma))
		{			
			$data['content'] = $this->capnhatDuLieu($ma);						
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin']	  = $this->_thongtin;
		
		$data['phongban'] = $this->Mdanhmuc->layDuLieu('PK_iMaPB','','tbl_phongban');		
		
		$giomoi = $this->_thongtin[0]['sGioMoi'];
	  
		$data['title']        = 'Nhập mới hồ sơ ABTC';
		$temp['data']         = $data;
		$temp['template']     = 'abtc/Vnhaptheabtc';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_abtc');
		if(_post('luudulieu'))
		{
			$data=array(
				'name'		   		=> _post('name'),
				'sex'        		=> _post('sex'),
				'cmt_cccd_hc'       => _post('cmt_cccd_hc'),
				'economy'          	=> _post('economy'),
				'date_of_birth'     => date_insert(_post('date_of_birth')),
				'doc_no'            => _post('doc_no'),
				'country'         	=> _post('country'),
				'ngaycap'         	=> date_insert(_post('ngaycap')),
				'expire_date'       => date_insert(_post('expire_date')),
				'chucvu'      		=> _post('chucvu'),
				'congty'    		=> _post('congty'),
				'mst'    			=> _post('mst'),
				'diachi'    		=> _post('diachi'),
				'FK_iMaCBNhap'    	=> $this->_session['PK_iMaCB'],
				'trang_thai'    	=> _post('trang_thai'),
				'vbden'    			=> _post('vbden'),
				'vbdi'    			=> _post('vbdi'),
				'note'    			=> _post('note'),
				'ngay_nhap'        	=> date('Y-m-d H:i:s', time())
			);
				
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('id',$ma,'tbl_abtc',$data);
			$name    = $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('doc_abtc_'.date('Y'), $time);
				$files = array();
				foreach ($name as $key => $value) {
					$val = clear($value);
					$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaABTC' => $ma,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_abtc_'.date('Y').'/tl_abtc_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
					);
				}
				$kiemtrafile=$this->Mdanhmuc->themNhieuDuLieu('tbl_files_abtc',$files);
				
			}
			if(empty(_post('chucvu')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập chức vụ!','danger');
			}
			if(empty(_post('congty')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập công ty!','danger');
			}
			if(empty(_post('diachi')))
			{
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập địa chỉ!','danger');
			}
						
			redirect('nhaptheabtc?id='.$ma);
		}
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_abtc');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		redirect('nhaptheabtc?id='.$mavanban);
		
	}
	public function themDuLieu()
	{
		$time = time();
		$quyen      = $this->_session['iQuyenHan_DHNB'];
		if(_post('luudulieu'))
		{
			$data=array(
				'name'		   		=> _post('name'),
				'sex'        		=> _post('sex'),
				'cmt_cccd_hc'       => _post('cmt_cccd_hc'),
				'economy'          	=> _post('economy'),
				'date_of_birth'     => date_insert(_post('date_of_birth')),
				'doc_no'            => _post('doc_no'),
				'country'         	=> _post('country'),
				'ngaycap'         	=> date_insert(_post('ngaycap')),
				'expire_date'       => date_insert(_post('expire_date')),
				'chucvu'      		=> _post('chucvu'),
				'congty'    		=> _post('congty'),
				'mst'    			=> _post('mst'),
				'diachi'    		=> _post('diachi'),
				'FK_iMaCBNhap'    	=> $this->_session['PK_iMaCB'],
				'trang_thai'    	=> _post('trang_thai'),
				'vbden'    			=> _post('vbden'),
				'vbdi'    			=> _post('vbdi'),
				'note'    			=> _post('note'),
				'ngay_nhap'        	=> date('Y-m-d H:i:s', time())
			);
			
			
			if(empty(date_insert(_post('date_of_birth'))) || date_insert(_post('date_of_birth')) == '1970-01-01')
			{
				
				$data=array(
				'name'		   		=> _post('name'),
				'sex'        		=> _post('sex'),
				'cmt_cccd_hc'       => _post('cmt_cccd_hc'),
				'economy'          	=> _post('economy'),
				'doc_no'            => _post('doc_no'),
				'country'         	=> _post('country'),
				'ngaycap'         	=> _post('ngaycap')?date_insert(_post('ngaycap')):NULL,
				'expire_date'       => _post('expire_date')?date_insert(_post('expire_date')):NULL,
				'chucvu'      		=> _post('chucvu'),
				'congty'    		=> _post('congty'),
				'mst'    			=> _post('mst'),
				'diachi'    		=> _post('diachi'),
				'FK_iMaCBNhap'    	=> $this->_session['PK_iMaCB'],
				'trang_thai'    	=> _post('trang_thai'),
				'note'    			=> _post('note'),
				'ngay_nhap'        	=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa nhập ngày sinh','danger');
			}
			if(empty(date_insert(_post('ngaycap'))) || date_insert(_post('ngaycap')) == '1970-01-01')
			{
				
				$data=array(
				'name'		   		=> _post('name'),
				'sex'        		=> _post('sex'),
				'cmt_cccd_hc'       => _post('cmt_cccd_hc'),
				'economy'          	=> _post('economy'),
				'date_of_birth'     => _post('date_of_birth')?date_insert(_post('date_of_birth')):NULL,
				'doc_no'            => _post('doc_no'),
				'country'         	=> _post('country'),
				'expire_date'       => _post('expire_date')?date_insert(_post('expire_date')):NULL,
				'chucvu'      		=> _post('chucvu'),
				'congty'    		=> _post('congty'),
				'mst'    			=> _post('mst'),
				'diachi'    		=> _post('diachi'),
				'FK_iMaCBNhap'    	=> $this->_session['PK_iMaCB'],
				'trang_thai'    	=> _post('trang_thai'),
				'note'    			=> _post('note'),
				'ngay_nhap'        	=> date('Y-m-d H:i:s', time())
				);
				$this->_thongtin = array($data);
				return messagebox('Bạn chưa chọn ngày cấp','danger');
			}
			
			$id_insert = $this->Mdanhmuc->themDuLieu2('tbl_abtc',$data);
			
			$name = $_FILES['files']['name'];
			
			
			if($id_insert>0)
			{
				
				$name = $_FILES['files']['name'];
				if(!empty($name[0]))
				{
					$this->upload('doc_abtc_'.date('Y'), $time);
					$files = array();
					foreach ($name as $key => $value) {
						$val = clear($value);
						$a = explode('.', $val);
						$c = count($a) - 1;
						$tmp = '';
						for ($i = 0; $i < $c; $i++){
							if ($i ==0) {
								$tmp = $tmp.$a[$i];
							} else {
								$tmp = $tmp.'-'.$a[$i];
							}
						}				
						$tmp = str_replace('$', '-', $tmp);						
						$tmp = $tmp.'.'.$a[$c];
						
					$files[] = array(
						'FK_iMaABTC' => $id_insert,
						'sTenFile'   => $value,
						'sDuongDan'  => 'doc_abtc_'.date('Y').'/tl_abtc_'.$time.'_'.$tmp,
						'sThoiGian'  => date('Y-m-d H:i:s',time()),
						'FK_iMaCB'   => $this->_session['PK_iMaCB'],
						);
					}
					$kiemtrafile = $this->Mdanhmuc->themNhieuDuLieu('tbl_files_abtc',$files);
				
				}
				echo '<script language="javascript">';
				echo 'alert(Thêm thẻ ABTC THÀNH CÔNG)';  //not showing an alert box.
				echo '</script>';
				redirect('dstheabtc');	
			}
			else{
				return messagebox('Thêm thẻ ABTC thất bại','danger');
			}
		}
	}
	public function upload($dir, $time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = 'tl_abtc_'.$time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */