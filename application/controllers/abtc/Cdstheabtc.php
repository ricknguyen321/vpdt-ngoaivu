<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdstheabtc extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('abtc/Mabtc');
		$this->load->library('pagination');
	}
	public function index()
	{
		//ricknguyen321 yêu cầu toàn bộ cbcc thay đổi mật khâu theo đúng quy tắc.
		$pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*\W).{8,16}$/";
		$result = preg_match ($pattern, $this->_session['sBanRo']);
			
		if ( !$result ) {
			echo '<script language="javascript">';
			echo 'alert("Weak password. Please change your password!")';
			echo '</script>';
			redirect(base_url().'doimatkhaucn');
		}
		$data['dsloaivanban'] = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_loaivanban');
		$data['dslinhvuc']	  = $this->Mdanhmuc->layDuLieu('iTrangThai',0,'tbl_linhvuc');
		
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu();
		}
		$vanban             = $this->DSVanBan();
		$data['dsabtc']	= $vanban['info'];
		$data['count'] = $vanban['count'];		
		$data['tonghop'] 	   = $vanban['tonghop'];		
		$data['phantrang']  = $vanban['pagination'];
		$data['title']    = 'Danh sách hồ sơ cấp thẻ ABTC';
		$data['page']    = _get('page');
		$temp['data']     = $data;
		$temp['template'] = 'abtc/Vdstheabtc';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu()
	{
		$ma = _post('xoa');
		$files = $this->Mdanhmuc->layDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		if(!empty($files))
		{
			foreach ($files as $key => $value) {
				if(file_exists($value['sDuongDan']))
				{
					unlink($value['sDuongDan']);
				}
			}
			$this->Mdanhmuc->xoaDuLieu('FK_iMaVBDi',$ma,'tbl_files_vbdi');
		}
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaVBDi',$ma,'tbl_vanbandi');
		if($kiemtra>0)
		{
			return messagebox('Xóa văn bản thành công!','info');
		}
	}
	
	public function DSVanBan() 
	{
		
		$tonghop	 = _get('tonghop');
		
		$config['base_url']             = base_url().'dstheabtc?tonghop='.$tonghop;
		
		$config['total_rows']           = $this->Mabtc->demDSABTC($tonghop);
	
		$config['per_page']             = 100;
		$config['page_query_string']    = TRUE;
		$config['query_string_segment'] = 'page';
		$config['num_links']            = 10;
		$config['use_page_numbers']     = false;
		$config['full_tag_open']        = '<ul class="pagination">';
		$config['full_tag_close']       = '</ul>';
		$config['first_link']           = '&laquo';
		$config['last_link']            = '&raquo';
		$config['first_tag_open']       = '<li>';
		$config['first_tag_close']      = '</li>';
		$config['prev_link']            = '&lt';
		$config['prev_tag_open']        = '<li class="prev">';
		$config['prev_tag_close']       = '</li>';
		$config['next_link']            = '&gt;';
		$config['next_tag_open']        = '<li>';
		$config['next_tag_close']       = '</li>';
		$config['last_tag_open']        = '<li>';
		$config['last_tag_close']       = '</li>';
		$config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
		$config['cur_tag_close']        = '</a></li>';
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';

		$this->pagination->initialize($config);

		$data['page']   = _get('page') ? _get('page') : 0;
		
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
      		redirect(base_url().'dsvanban');
      	}
		
		$data['tonghop']    = $tonghop;
		$data['count']      = $config['total_rows'];
		$data['info']       = $this->Mabtc->layDSABTC($config['per_page'], $data['page'],$tonghop);
		$data['pagination'] = $this->pagination->create_links();
        return $data;
	}

}

/* End of file Cdsvanban.php */
/* Location: ./application/controllers/vanban/Cdsvanban.php */