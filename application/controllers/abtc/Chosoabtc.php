<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chosoabtc extends MY_Controller {
	protected $_thongtin;
	protected $_files;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		$ma = _get('id');
		if ($ma > 0) {			
			$data['tailieu'] = $this->Mdanhmuc->layDuLieu('FK_iMaABTC',$ma,'tbl_files_abtc');
			if(_post('xoa'))
			{
				$data['content'] = $this->xoaDuLieu($ma);
			}
		}
		
		$this->_thongtin 	= $this->Mdanhmuc->layDuLieu('id',$ma,'tbl_hoso_abtc');
		$data['thongtin']	= $this->_thongtin;
		
		$data['title']        = 'Thông tin hồ sơ ABTC';
		$temp['data']         = $data;
		$temp['template']     = 'abtc/Vhosoabtc';
		$this->load->view('layout_admin/layout',$temp);
	}
	
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_files_abtc');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		redirect('hosoabtc?id='.$mavanban);
		
	}
	
	
}

/* End of file Cvanban.php */
/* Location: ./application/controllers/vanban/Cvanban.php */