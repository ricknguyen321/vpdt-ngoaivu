<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdsvanbanphongphoihop extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mvanban_caphai');
        $this->load->library('pagination');
    }
    public function index()
    {
        $donvi         = $this->_session['FK_iMaPhongHD'];
        $phongban      = $this->_session['phong_caphai'];
        $quyen         = $this->_session['iQuyenHan_DHNB'];
        $taikhoan      = $this->_session['PK_iMaCB'];
        $dulieu        = $this->layDS($donvi,$phongban);
        $data['dulieu']      = $dulieu['dulieu'];
        if(!empty($data['dulieu']))
        {
            foreach ($data['dulieu'] as $key => $value) {
                $file = $this->Mvanban_caphai->layFielVB_caphai($value['PK_iMaVB'],NULL,1);
                $data['dulieu'][$key]['sDuongDan'] = ($file)?$file[0]['sDuongDan']:'';
                $data['dulieu'][$key]['chuyennhan']= $this->Mvanban_caphai->layChuyenNhan($value['PK_iMaVB'],array(1,2),array(0,1));
            }
        }
        $data['phantrang']   = $dulieu['phantrang'];
        $data['loaivanban']  = $dulieu['loaivanban'];
        $data['noiden']      = $dulieu['noiden'];
        $data['kyhieu']      = $dulieu['kyhieu'];
        $data['ngayky']      = $dulieu['ngayky'];
        $data['ngaynhaptu']  = $dulieu['ngaynhaptu'];
        $data['ngaynhapden'] = $dulieu['ngaynhapden'];
        $data['trichyeu']    = $dulieu['trichyeu'];
        $data['ngaymoitu']   = $dulieu['ngaymoitu'];
        $data['ngaymoiden']  = $dulieu['ngaymoiden'];
        $data['nguoiky']     = $dulieu['nguoiky'];
        $data['chucvu']      = $dulieu['chucvu'];
        $data['soden']       = $dulieu['soden'];
        $data['dsloaivanban']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaivanban');
        $data['dsnoiguiden']   = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_donvi');
        $canbo         = $this->Mvanban_caphai->layCB($donvi,NULL,NULL);
        $data['dscanbo'] = '';
        if(!empty($canbo))
        {
            foreach ($canbo as $key => $value) {
                $data['dscanbo'][$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        $data['title']     = 'Danh sách văn bản của phòng';
        $temp['data']      = $data;
        $temp['template']  = 'vanbanden_caphai/Vdsvanbanphongphoihop';
        $this->load->view('layout_admin/layout',$temp);
    }
    public function layDS($donvi,$phongban)
    {
        $data['loaivanban']  = _get('loaivanban');
        $data['noiden']      = _get('noiden');
        $data['kyhieu']      = _get('kyhieu');
        $data['ngayky']      = _get('ngayky');
        $data['ngaynhaptu']  = (_get('ngaynhaptu'))?date_insert(_get('ngaynhaptu')):'';
        $data['ngaynhapden'] = (_get('ngaynhapden'))?date_insert(_get('ngaynhapden')):'';
        $data['trichyeu']    = _get('trichyeu');
        $data['ngaymoitu']   = (_get('ngaymoitu'))?date_insert(_get('ngaymoitu')):'';
        $data['ngaymoiden']  = (_get('ngaymoiden'))?date_insert(_get('ngaymoiden')):'';
        $data['nguoiky']     = _get('nguoiky');
        $data['chucvu']      = _get('chucvu');
        $data['soden']       = _get('soden');

        $config['base_url']             = base_url().'dsvanbanphongphoihop?loaivanban='.$data['loaivanban'].'&noiden='.$data['noiden'].'&kyhieu='.$data['kyhieu'].'&ngayky='.$data['ngayky'].'&ngaynhaptu='.$data['ngaynhaptu'].'&ngaynhapden='.$data['ngaynhapden'].'&noiden='.$data['trichyeu'].'&ngaymoitu='.$data['ngaymoitu'].'&ngaymoiden='.$data['ngaymoiden'].'&nguoiky='.$data['nguoiky'].'&chucvu='.$data['chucvu'].'&soden='.$data['soden'];
        $config['total_rows']           = $this->Mvanban_caphai->demDSVB_Phong($donvi,$phongban,3,$data['loaivanban'],$data['noiden'],$data['kyhieu'],$data['ngayky'],$data['ngaynhaptu'],$data['ngaynhapden'],$data['trichyeu'],$data['ngaymoitu'],$data['ngaymoiden'],$data['nguoiky'],$data['chucvu'],$data['soden']);
        $config['per_page']             = 50;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links']            = 10;
        $config['use_page_numbers']     = false;
        $config['full_tag_open']        = '<ul class="pagination">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '&laquo';
        $config['last_link']            = '&raquo';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['prev_link']            = '&lt';
        $config['prev_tag_open']        = '<li class="prev">';
        $config['prev_tag_close']       = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['cur_tag_open']         = "<li class='active'><a style='cursor: not-allowed;' href='javasctipt:void(0);'>";
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';

        $this->pagination->initialize($config);

        $data['page']   = _get('page') ? _get('page') : 0;
        
        if((is_numeric($data['page']) == FALSE) || $data['page']  % $config['per_page'] != 0 ||($data['page']) > $config['total_rows']) {
            redirect(base_url().'dsvanbanphongphoihop');
        }
        $data['dulieu']     = $this->Mvanban_caphai->layDSVB_Phong($donvi,$phongban,3,$data['loaivanban'],$data['noiden'],$data['kyhieu'],$data['ngayky'],$data['ngaynhaptu'],$data['ngaynhapden'],$data['trichyeu'],$data['ngaymoitu'],$data['ngaymoiden'],$data['nguoiky'],$data['chucvu'],$data['soden'],$config['per_page'],$data['page']);
        $data['phantrang']  = $this->pagination->create_links();
        return $data;

    }

}

/* End of file Cdsvanbanphongphoihop.php */
/* Location: ./application/controllers/vanbanden_caphai/Cdsvanbanphongphoihop.php */