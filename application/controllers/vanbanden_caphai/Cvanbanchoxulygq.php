<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanchoxulygq extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mvanbanchoxuly');
        $this->load->model('vanbanden_caphai/Mvanban_caphai');
    }
    public function index()
    {
		$donvi				= $this->_session['FK_iMaPhongHD'];
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		$phongban			= $this->_session['phong_caphai'];
		$canbo				= $this->Mvanban_caphai->layCB($donvi,NULL,NULL);
		$data['ma']			= _get('ma');
		if(!is_numeric($data['ma'])){
			redirect('welcome.html');
		}
		$kiemtradonvi		= $this->Mvanbanchoxuly->kiemtraDV($data['ma'],$donvi);
		if(empty($kiemtradonvi))
		{
			redirect('welcome.html');
		}
		$thongtinchuyennhan = $this->Mvanban_caphai->layTT_ChuyenNhan($data['ma'],$taikhoan);
		// pr($thongtinchuyennhan);
		if(empty($thongtinchuyennhan))
		{
			$ct_pt			= array(1,2);
			$nguoinhan		= array(0,1);
			$nguoiduyet     = 0;
		}
		if(!empty($thongtinchuyennhan))
		{
			if($thongtinchuyennhan['iTrangThai']==1)
			{
				$data['anhien'] = '';
			}
			else{
				$data['anhien'] = 'hide';
			}
		}
		else{
			$data['anhien'] = 'hide';
		}
		if($thongtinchuyennhan['CT_PH']==1)
		{
			$data['anhien'] = 'hide';
		}
		if($thongtinchuyennhan['CT_PH']==1)
		{
			$ct_pt		= array(1,2);
			$nguoinhan	= array(0,1);
		}
		if($thongtinchuyennhan['CT_PH']==2) // phòng chủ trì
		{
			if($thongtinchuyennhan['nguoinhan']==0) // văn bản người chủ trì
			{
				$ct_pt			= array(1,2);
				$nguoinhan		= array(0,1);
				$nguoiduyet		= $thongtinchuyennhan['FK_iMaCB_Nhan'];
				$trangthaiduyet	= 2;
				$trangthaifile  = 2;
			}
			elseif($thongtinchuyennhan['nguoinhan']==1){ // văn bản người chủ trì
				$ct_pt			= array(1,2);
				$nguoinhan		= array(0,1);
				$nguoiduyet		= $thongtinchuyennhan['FK_iMaCB_Gui'];
				$trangthaiduyet	= 1;
				$trangthaifile  = 2;
			}
			else{ // // văn bản người phối hợp
				$ct_pt			= array(1,2);
				$nguoinhan		= array(0,1,2);
				$nguoiduyet		= $thongtinchuyennhan['FK_iMaCB_Gui'];
				$trangthaiduyet	= 1;
				$trangthaifile  = 3;
			}
		}
		if($thongtinchuyennhan['CT_PH']==3) // phòng phối hợp
		{
			if($thongtinchuyennhan['nguoinhan']==0) // văn bản người chủ trì
			{
				$ct_pt			= array(1,3);
				$nguoinhan		= array(0,1);
				$nguoiduyet		= $thongtinchuyennhan['FK_iMaCB_Nhan'];
				$trangthaiduyet	= 2;
				$trangthaifile  = 2;
			}
			elseif($thongtinchuyennhan['nguoinhan']==1){ // văn bản người chủ trì
				$ct_pt			= array(1,3);
				$nguoinhan		= array(0,1);
				$nguoiduyet		= $thongtinchuyennhan['FK_iMaCB_Gui'];
				$trangthaiduyet	= 2;
				$trangthaifile  = 2;
			}
			else{ // // văn bản người phối hợp
				$ct_pt			= array(1,3);
				$nguoinhan		= array(0,1,2);
				$nguoiduyet		= $thongtinchuyennhan['FK_iMaCB_Gui'];
				$trangthaiduyet	= 2;
				$trangthaifile  = 3;
			}
		}
		if(_post('luulaiketqua'))
		{
			$time= time();
			$name = clear($_FILES['file']['name']);
			if(!empty($name))
			{
				$this->upload('caphai_'.date('Y'),$name,'file',$time);
			}
			$data_ketqua=array(
				'FK_iMaVB'			=> $data['ma'],
				'FK_iMaDV'			=> $donvi,
				'phong'				=> $phongban,
				'CT_PH'				=> $thongtinchuyennhan['CT_PH'],
				'sDuongDan'			=> ($name)?'caphai_'.date('Y').'/'.$time.'_'.$name:'',
				'FK_iMaCB'			=> $taikhoan,
				'sThoiGian'			=> date('Y-m-d H:i:s'),
				'iTrangThai'		=> $trangthaifile,
				'sNoiDung'			=> _post('ketqua'),
				'FK_iMaCB_Duyet'	=> $nguoiduyet,
				'iTrangThai_Duyet'	=> $trangthaiduyet
			);
			$ketqua = $this->Mdanhmuc->themDuLieu('tbl_file_caphai',$data_ketqua);
			if($ketqua>0)
			{
				// cập nhật trạng thái chuyền nhận
				$data_trangthai=array(
					'iTrangThai' => 2
				);
				$this->Mvanbanchoxuly->capnhatTrangThai($taikhoan,$data['ma'],$data_trangthai);
				// cập nhật trạng thái văn bản
				$data_mang_capnhat=array(
					'sNgayGiaiQuyet'	=> date('Y-m-d H:i:s'),
					'iTrangThai'		=> $trangthaiduyet+1,
					'sNoiDungGiaiQuyet'	=> _post('ketqua')
				);
				if($thongtinchuyennhan['CT_PH']==2)
				{
					if($thongtinchuyennhan['nguoinhan']==1 || $thongtinchuyennhan['nguoinhan']==0) //phòng chủ trì
					{
						$this->Mdanhmuc->capnhatDuLieu('PK_iMaVB',$data['ma'],'tbl_vanbanden_caphai',$data_mang_capnhat);
					}
				}
				else{
					if($thongtinchuyennhan['nguoinhan']==1 || $thongtinchuyennhan['nguoinhan']==0) //phòng chủ trì
					{
					$this->Mdanhmuc->setDuLieu('FK_iMaVB',$data['ma'],'tbl_vanbanden_caphai_ph','sNgayGiaiQuyet',date('Y-m-d H:i:s'));
					}
				}
				
				$data['content'] = messagebox('Cập nhật kết quả thành công','info');
			}
		}
		if(_post('luulaichammuon'))
		{
			$data_ketqua=array(
				'FK_iMaVB'			=> $data['ma'],
				'FK_iMaDV'			=> $donvi,
				'phong'				=> $phongban,
				'CT_PH'				=> $thongtinchuyennhan['CT_PH'],
				'FK_iMaCB'			=> $taikhoan,
				'sThoiGian'			=> date('Y-m-d H:i:s'),
				'iTrangThai'		=> 4,
				'sNoiDung'			=> _post('ketqua')
			);
			$ketqua = $this->Mdanhmuc->themDuLieu('tbl_file_caphai',$data_ketqua);
			if($ketqua>0)
			{
				$data['content'] = messagebox('Ghi lý do chậm muộn thành công','info');
			}
		}
		$thongtinchuyennhan = $this->Mvanban_caphai->layTT_ChuyenNhan($data['ma'],$taikhoan);
		if(!empty($thongtinchuyennhan))
		{
			if($thongtinchuyennhan['iTrangThai']==1)
			{
				$data['anhien'] = '';
			}
			else{
				$data['anhien'] = 'hide';
			}
		}
		else{
			$data['anhien'] = 'hide';
		}
		if($thongtinchuyennhan['CT_PH']==1)
		{
			$data['anhien'] = 'hide';
		}
		if(is_numeric($data['ma']))
		{
			$data['thongtin']			= $this->Mvanban_caphai->layChiTietVB($data['ma']);
			$data['trinhtu']			= $this->Mvanban_caphai->layQuaTrinhChuyenNhan($data['ma'],$ct_pt,$nguoinhan,array(0,$phongban));
			if(empty($thongtinchuyennhan)){
				$data['trinhtu']			= $this->Mvanban_caphai->layQuaTrinhChuyenNhan($data['ma'],$ct_pt,$nguoinhan,NULL);
			}
			$data['file']				= $this->Mvanban_caphai->layFielVB_caphai($data['ma'],NULL,1);
			$data['filekqchutri']		= $this->Mvanban_caphai->layFielVB_caphai($data['ma'],2,2);
			$data['filekqphoihopct']	= $this->Mvanban_caphai->layFielVB_caphai($data['ma'],2,3);
			$data['filekqphoihopph']	= $this->Mvanban_caphai->layFielVB_caphai($data['ma'],3,NULL);
			$data['chammuon']			= $this->Mvanban_caphai->layFielVB_caphai($data['ma'],NULL,4);
			if(empty($data['thongtin']))
			{
				redirect('welcome.html');
			}
		}
		else{
			redirect('welcome.html');
		}
		// pr($data['thongtin']);
		$data['canbo']		= '';
        if(!empty($canbo))
        {
            foreach ($canbo as $key => $value) {
                $data['canbo'][$value['PK_iMaCB']]=$value['sHoTen'];
            }
        }
        $data['title']     = 'Văn bản chờ xử lý giải quyết';
        $temp['data']      = $data;
        $temp['template']  = 'vanbanden_caphai/Vvanbanchoxulygq';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function upload($dir,$name,$filename,$time)
	{
		if(is_dir($dir)==false){
			mkdir($dir);		// Create directory if it does not exist
		}
		$config['upload_path']   = $dir;
		$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
		$config['overwrite']     = true;	
		$config['file_name']     = $time.'_'.$name;
		$this->load->library('upload');
		$this->upload->initialize($config);
		$this->upload->do_upload($filename);
	}

}

/* End of file Cvanbanchoxulygq.php */
/* Location: ./application/controllers/vanbanden_caphai/Cvanbanchoxulygq.php */