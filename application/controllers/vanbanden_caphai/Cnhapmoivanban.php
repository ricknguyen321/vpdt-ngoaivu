<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhapmoivanban extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mnhapmoi');
    }
    public function index()
    {
		$data['khuvuc']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_khuvuc');
        $donvi = $this->_session['FK_iMaPhongHD'];
        $nam   = date('Y');
        if(_post('luulai'))
        {
            $loaivanban = _post('loaivanban');
            $sKyHieu    = _post('sokyhieu');
            $noigui     = _post('noiguiden');
            $kiemtra = $this->Mdanhmuc->kiemtraTrung($sKyHieu,$loaivanban,$noigui);
            if($kiemtra==0)
            {
                $this->themDuLieu();
            }
            else{
                $data['content'] = messagebox('Văn bản này đã tồn tại trong hệ thống!','danger');
            }
        }
        $soden = $this->Mnhapmoi->laySoDen($donvi,$nam);
        if(!empty($soden))
        {
            $data['soden'] = $soden['iSoDen']+1;
        }
        else{
            $data['soden'] = 1;
        }
        $data['loaivanban'] = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaivanban');
		$data['title']      = 'Nhập mới văn bản';
		$temp['data']       = $data;
		$temp['template']   = 'vanbanden_caphai/Vnhapmoivanban';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function themDuLieu()
    {
    	$donvi = $this->_session['FK_iMaPhongHD'];
        $nam   = date('Y');
        $soden = $this->Mnhapmoi->laySoDen($donvi,$nam);
        if(!empty($soden))
        {
        	$soden =  $soden['iSoDen']+1;
        }
        else{
        	$soden =  1;
        }
    	$noidunghop = _post('noidunghop');
        $hannoidung = _post('hannoidung');
        $hanxuly    = _post('hangiaiquyet');
        $hannoidung = _post('hannoidung');
        if(empty($hanxuly))
        {
            $hanxuly   = '0000-00-00';
        }
        foreach ($noidunghop as $key => $value) {
            $hangiaiquyet =($hannoidung[$key])?date_insert($hannoidung[$key]):date_insert($hanxuly);
            $data = array(
                'iVanBanQPPL'     => (_post('iVanBanQPPL'))?_post('iVanBanQPPL'):1,
                'iSTCChuTri'      => (_post('iSTCChuTri'))?_post('iSTCChuTri'):1,
                'iVanBanTBKL'     => (_post('iVanBanTBKL'))?_post('iVanBanTBKL'):1,
                'iVanBanMat'      => (_post('iVanBanMat'))?_post('iVanBanMat'):1,
                'sTenKV'          => _post('khuvuc'),
                'sNoiGuiDen'      => _post('donvi'),
                'FK_iMaDV'        => $this->_session['FK_iMaPhongHD'],
                'iSoDen'          => $soden,
                'iGiayMoi'        => 1,
                'sKyHieu'         => _post('sokyhieu'),
                'sTenLVB'         => _post('loaivanban'),
                'sTenLV'          => _post('linhvuc'),
                'sMoTa'           => _post('trichyeu'),
                'sNguoiKy'        => _post('nguoiky'),
                'sChucVu'         => _post('chucvu'),
                'sNgayKy'		  => _post('ngayky'),
                'sHanGiaiQuyet'   => $hangiaiquyet,
                'sHan_ThongKe'    => $hangiaiquyet,
                'sNoiDung'        => $value,
                'FK_iMaCB'        => $this->_session['PK_iMaCB'],
                'sNgayNhap'       => date('Y-m-d H:i:s'),
                'sNgayNhan'       => date_insert(_post('ngaynhan')),
                'iTrangThai'	  => 1,
                'iNam'			  => date('Y'),
                'iSoTrang'        => _post('sotrang')
            );
            $id = $this->Mdanhmuc->themDuLieu2('tbl_vanbanden_caphai',$data);
            if($id>0)
            {
                $this->themChuyenNhan($id,$hangiaiquyet);
            }
        }
    }

    // thêm chuyền nhận
    public function themChuyenNhan($id,$hangiaiquyet)
    {
        $donvi   = $this->_session['FK_iMaPhongHD'];
        $lanhdao = $this->Mnhapmoi->layLanhDao($donvi);
        $data=array(
            'FK_iMaVB'      => $id,
            'FK_iMaDV'      => $this->_session['FK_iMaPhongHD'],
            'FK_iMaCB_Gui'  => $this->_session['PK_iMaCB'],
            'FK_iMaCB_Nhan' => $lanhdao['PK_iMaCB'],
            'sNoiDung'      => '',
            'sHanGiaiQuyet' => $hangiaiquyet,
            'sThoiGian'     => date('Y-m-d H:i:s'),
            'CT_PH'         => 1,
            'iTrangThai'    => 1
        );
        $this->Mdanhmuc->themDuLieu('tbl_chuyennhan_caphai',$data);
    }

}

/* End of file Cnhapmoivanban.php */
/* Location: ./application/controllers/vanbanden_caphai/Cnhapmoivanban.php */