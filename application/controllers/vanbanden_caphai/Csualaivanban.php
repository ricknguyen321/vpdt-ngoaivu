<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csualaivanban extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mnhapmoi');
    }
    public function index()
    {
		$data['khuvuc']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_khuvuc');
        $donvi = $this->_session['FK_iMaPhongHD'];
        $canbo = $this->Mnhapmoi->layCB($donvi,6);
        foreach ($canbo as $key => $cb) {
        	$data['dscanbo'][$cb['PK_iMaCB']] = $cb['sHoTen'];
        }
        $nam   = date('Y');
        $ma    = _get('id');
        if(is_numeric($ma))
        {
        	$data['thongtin'] = $this->Mdanhmuc->layDuLieu('PK_iMaVB',$ma,'tbl_vanbanden_caphai');
        }
        else{
        	redirect('danhsachtongthe_caphai');
        }
        if(_post('luulai'))
        {
            $this->capnhatDuLieu($ma);
        }
        $data['loaivanban'] = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaivanban');
		$data['title']      = 'Sửa lại văn bản';
		$temp['data']       = $data;
		$temp['template']   = 'vanbanden_caphai/Vsualaivanban';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function capnhatDuLieu($ma)
    {
    	$donvi = $this->_session['FK_iMaPhongHD'];
        $nam   = date('Y');
        $data = array(
            'iVanBanQPPL'     => (_post('iVanBanQPPL'))?_post('iVanBanQPPL'):1,
            'iSTCChuTri'      => (_post('iSTCChuTri'))?_post('iSTCChuTri'):1,
            'iVanBanTBKL'     => (_post('iVanBanTBKL'))?_post('iVanBanTBKL'):1,
            'iVanBanMat'      => (_post('iVanBanMat'))?_post('iVanBanMat'):1,
            'sTenKV'          => _post('khuvuc'),
            'sNoiGuiDen'      => _post('donvi'),
            'FK_iMaDV'        => $donvi,
            'iSoDen'          => _post('soden'),
            'iGiayMoi'        => 1,
            'sKyHieu'         => _post('sokyhieu'),
            'sTenLVB'         => _post('loaivanban'),
            'sTenLV'          => _post('linhvuc'),
            'sMoTa'           => _post('trichyeu'),
            'sNguoiKy'        => _post('nguoiky'),
            'sChucVu'         => _post('chucvu'),
            'sNgayKy'		  => date_insert(_post('ngayky')),
            'sHanGiaiQuyet'   => _post('hangiaiquyet'),
            'sNoiDung'        => _post('noidunghop'),
            'FK_iMaCB'        => $this->_session['PK_iMaCB'],
            'sNgayNhap'       => date('Y-m-d H:i:s'),
            'sNgayNhan'       => date_insert(_post('ngaynhan')),
            'iTrangThai'	  => 1,
            'iNam'			  => date('Y'),
            'iSoTrang'        => _post('sotrang')
        );
        $kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVB',$ma,'tbl_vanbanden_caphai',$data);
        if($kiemtra>0)
        {
        	redirect('danhsachtongthe_caphai');
        }
    }

}

/* End of file Csualaivanban.php */
/* Location: ./application/controllers/vanbanden_caphai/Csualaivanban.php */