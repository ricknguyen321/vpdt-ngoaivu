<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cduyetketqua_ch extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mvanbanchoxuly');
        $this->load->model('vanbanden_caphai/Mvanban_caphai');
    }
    public function index()
    {
        $donvi             = $this->_session['FK_iMaPhongHD'];
        $quyen             = $this->_session['iQuyenHan_DHNB'];
        $taikhoan          = $this->_session['PK_iMaCB'];
        $phongban          = $this->_session['phong_caphai'];
        $dscanbo  = $this->Mvanban_caphai->layCB($donvi,NULL,NULL);
        if(!empty($dscanbo))
        {
        	foreach ($dscanbo as $key => $cb) {
        		$data['dscanbo'][$cb['PK_iMaCB']] = $cb['sHoTen'];
        	}
        }

        if(_post('duyet'))
        {
            $mavanban = _post('mavanban');
            if(!empty($mavanban))
            {
                $data=array(
                    'iTrangThai_Duyet' => 2,
                    'sThoiGian_Duyet'  => date('Y-m-d H:i:s')
                );
                foreach ($mavanban as $key => $value) {
                    $kiemtra = $this->Mvanban_caphai->capnhatTrangThai_KQ($value,$taikhoan,2,2,$data);
                    if($kiemtra>0)
                    {
                        $this->Mdanhmuc->setDuLieu('PK_iMaVB',$value,'tbl_vanbanden_caphai','iTrangThai',3);
                    }
                }
                $data['content'] = messagebox('Duyệt kết quả thành công!','info');
            }
        }
		$data['dsvanban']	= $this->Mvanban_caphai->layDSVB_ChoDuyet($taikhoan);
        if(!empty($data['dsvanban']))
        {
            foreach ($data['dsvanban'] as $key => $value) {
                $file = $this->Mvanbanchoxuly->layFile($value['PK_iMaVB']);
                $data['dsvanban'][$key]['sDuongDan'] = ($file)?$file[0]['sDuongDan']:'';
                $data['dsvanban'][$key]['chuyennhan']= $this->Mvanban_caphai->layChuyenNhan($value['PK_iMaVB'],array(1,2),array(0,1));
            }
        }
        $data['title']     = 'Văn bản chờ duyệt';
        $temp['data']      = $data;
        $temp['template']  = 'vanbanden_caphai/Vduyetketqua_ch';
		$this->load->view('layout_admin/layout',$temp);
    }

}

/* End of file Cduyetketqua_ch.php */
/* Location: ./application/controllers/vanbanden_caphai/Cduyetketqua_ch.php */