<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupload_files_caphai extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbanden_caphai/Mvanban_caphai');
	}
	public function index()
	{
		$data['content'] = '';
		if(_post('upload'))
		{
			$data['content'] = $this->upload();
		}
		$data['title']    = 'Upload Files Văn Bản Cấp Hai';
		$data['url']      = base_url();
		$this->parser->parse('vanbanden_caphai/Vupload_files_caphai',$data);
	}
	public function upload()
	{
		$donvi = $this->_session['FK_iMaPhongHD'];
		$phongban = $this->_session['iQuyenDB'];

		$dir  = 'caphai_'.date('Y');
        $name = $_FILES['files']['name'];
        $tong = count($name);
		if(!empty($name[0]))
		{
            $file = $_FILES;
            for ($i = 0; $i < $tong; $i++) {
                #initialization new file data
                $time = time();
				$_FILES['files']['name']     = $file['files']['name'][$i];
				$_FILES['files']['type']     = $file['files']['type'][$i];
				$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
				$_FILES['files']['error']    = $file['files']['error'][$i];
				$_FILES['files']['size']     = $file['files']['size'][$i];

				if(is_dir($dir)==false){
					mkdir($dir);		// Create directory if it does not exist
				}
				$file_name = $_FILES['files']['name'];
				$tentep    = substr($file_name,0,(strlen($file_name)-4)); 
				$thongtin  = $this->Mvanban_caphai->layTT_soDen($donvi,$tentep);
				if(!empty($thongtin))
				{
					foreach ($thongtin as $key => $val) {
						$data=array(
							'FK_iMaVB'	=> $val['PK_iMaVB'],
							'FK_iMaDV'	=> $donvi,
							'sTenFile'	=> clear($file_name),
							'sDuongDan'	=> $dir.'/'.$time.'_'.clear($file_name),
							'sThoiGian'	=> date('Y-m-d H:i:s'),
							'FK_iMaCB'	=> $this->_session['PK_iMaCB'],
							'iTrangThai'=> 1
							);
						$this->Mdanhmuc->themDuLieu('tbl_file_caphai',$data);
						$this->Mdanhmuc->setDuLieu('PK_iMaVB',$val['PK_iMaVB'],'tbl_vanbanden_caphai','iFile',1);
					}
				}
				$config['upload_path']   = $dir;
				$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
				$config['overwrite']     = true;
				$config['file_name']     = $time.'_'.clear($file_name);
				$this->load->library('upload');
             	$this->upload->initialize($config);
                $checkResult = $this->upload->do_upload('files');
                $fileData = $this->upload->data();
            }
            return messagebox('Upload Files thành công','info');
        }
        else
        {
        	return messagebox('Bạn chưa chọn File','danger');
        }
	}

}

/* End of file Cupload_files_caphai.php */
/* Location: ./application/controllers/vanbanden_caphai/Cupload_files_caphai.php */