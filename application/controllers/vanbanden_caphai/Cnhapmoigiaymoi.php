<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhapmoigiaymoi extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mnhapmoi');
    }
    public function index()
    {
        $quyen = $this->_session['iQuyenHan_DHNB'];
		$data['khuvuc']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_khuvuc');
        $donvi = $this->_session['FK_iMaPhongHD'];
        $nam   = date('Y');
        if(_post('luulai'))
        {
            $loaivanban = _post('loaivanban');
            $sKyHieu    = _post('sokyhieu');
            $noigui     = _post('noiguiden');
            $kiemtra = $this->Mdanhmuc->kiemtraTrung($sKyHieu,$loaivanban,$noigui);
            if($kiemtra==0)
            {
                $this->themDuLieu();
            }
            else{
                $data['content'] = messagebox('Văn bản này đã tồn tại trong hệ thống!','danger');
            }
        }
        $soden = $this->Mnhapmoi->laySoDen($donvi,$nam);
        if(!empty($soden))
        {
            $data['soden'] = $soden['iSoDen']+1;
        }
        else{
            $data['soden'] = 1;
        }
		$data['title']      = 'Nhập mới giấy mời';
		$temp['data']       = $data;
		$temp['template']   = 'vanbanden_caphai/Vnhapmoigiaymoi';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function themDuLieu()
    {
    	$donvi = $this->_session['FK_iMaPhongHD'];
        $nam   = date('Y');
        $soden = $this->Mnhapmoi->laySoDen($donvi,$nam);
        if(!empty($soden))
        {
        	$soden =  $soden['iSoDen']+1;
        }
        else{
        	$soden =  1;
        }
		$giohopgm	= _post('giohopgm');
		$ngayhopgm	= _post('ngayhopgm');
		$diadiem	= _post('diadiem');

		$noidunghop	= _post('noidunghop');
		$diadiemhop	= _post('diadiemhop');
		$giohop		= _post('giohop');
		$ngayhop	= _post('ngayhop');
        if(empty($hanxuly))
        {
            $hanxuly   = '0000-00-00';
        }
        foreach ($noidunghop as $key => $value) {
            $data = array(
				'FK_iMaDV'		=> $this->_session['FK_iMaPhongHD'],
				'iSoDen'		=> $soden,
				'sNoiGuiDen'	=> _post('noiguiden'),
				'sKyHieu'		=> _post('sokyhieu'),
				'sTenLVB'		=> 'Giấy mời',
				'sMoTa'			=> _post('noidung'),
				'sGioMoi'		=> ($giohop[$key])?$giohop[$key]:$giohopgm,
				'sNgayMoi'		=> ($ngayhop[$key])?date_insert($ngayhop[$key]):date_insert($ngayhopgm),
				'sDiaDiem'		=> ($diadiemhop[$key])?$diadiemhop[$key]:$diadiem,
				'sNoiDung'		=> $value,
                'iGiayMoi'      => 2,
				'sNguoiKy'		=> _post('nguoiky'),
				'sChucVu'		=> _post('chucvu'),
				'sNgayKy'		=> _post('ngayky'),
				'sNguoiChuTri'  => _post('nguoichutri'),
				'sHanGiaiQuyet'	=> '0000-00-00',
                'sHan_ThongKe'  => '0000-00-00',
				'FK_iMaCB'		=> $this->_session['PK_iMaCB'],
				'sNgayNhap'		=> date('Y-m-d H:i:s'),
				'iTrangThai'	=> 1,
				'iNam'			=> date('Y'),
				'iSoTrang'		=> _post('sotrang'),
				'sNgayNhan'	    => date_insert(_post('ngaynhan'))
            );
            $id = $this->Mdanhmuc->themDuLieu2('tbl_vanbanden_caphai',$data);
            if($id>0)
            {
                $this->themChuyenNhan($id);
            }
        }
    }

    // thêm chuyền nhận
    public function themChuyenNhan($id)
    {
        $donvi   = $this->_session['FK_iMaPhongHD'];
        $lanhdao = $this->Mnhapmoi->layLanhDao($donvi);
        $data=array(
            'FK_iMaVB'      => $id,
            'FK_iMaDV'      => $this->_session['FK_iMaPhongHD'],
            'FK_iMaCB_Gui'  => $this->_session['PK_iMaCB'],
            'FK_iMaCB_Nhan' => $lanhdao['PK_iMaCB'],
            'sNoiDung'      => '',
            'sHanGiaiQuyet' => '0000-00-00',
            'sThoiGian'     => date('Y-m-d H:i:s'),
            'CT_PH'         => 1,
            'iTrangThai'    => 1
        );
        $this->Mdanhmuc->themDuLieu('tbl_chuyennhan_caphai',$data);
    }

}

/* End of file Cnhapmoigiaymoi.php */
/* Location: ./application/controllers/vanbanden_caphai/Cnhapmoigiaymoi.php */