<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanchoxuly_pp_ch extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mvanbanchoxuly');
        $this->load->model('vanbanden_caphai/Mvanban_caphai');
    }
    public function index()
    {
        $donvi             = $this->_session['FK_iMaPhongHD'];
        $quyen             = $this->_session['iQuyenHan_DHNB'];
        $taikhoan          = $this->_session['PK_iMaCB'];
        $phongban          = $this->_session['phong_caphai'];
        if(_post('duyet'))
        {
            $data['content'] = $this->duyetDauViec();
        }
        $dscanbo  = $this->Mvanban_caphai->layCB($donvi,NULL,NULL);
        if(!empty($dscanbo))
        {
        	foreach ($dscanbo as $key => $cb) {
        		$data['dscanbo'][$cb['PK_iMaCB']] = $cb['sHoTen'];
        	}
        }
		$data['chuyenvien']	= $this->Mvanban_caphai->layCB_Phong($taikhoan,$donvi,5,$phongban);
		$data['dsvanban']	= $this->Mvanban_caphai->layDSVB_Phong_CT($donvi,$taikhoan,2,1,1,NULL,1,NULL,NULL);
        if(!empty($data['dsvanban']))
        {
            foreach ($data['dsvanban'] as $key => $value) {
                $file = $this->Mvanban_caphai->layFielVB_caphai($value['PK_iMaVB'],NULL,1);
                $data['dsvanban'][$key]['sDuongDan'] = ($file)?$file[0]['sDuongDan']:'';
            }
        }
        $data['title']     = 'Văn bản chờ xử lý';
        $temp['data']      = $data;
        $temp['template']  = 'vanbanden_caphai/Vvanbanchoxuly_pp_ch';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function duyetDauViec()
	{
		$donvi    = $this->_session['FK_iMaPhongHD'];
		$taikhoan = $this->_session['PK_iMaCB'];
		$mavanban = _post('mavanban');
		$phongban = $this->_session['phong_caphai'];
		if(!empty($mavanban))
		{
			foreach ($mavanban as $key => $value) {
				$chuyenvien			= _post('chuyenvien_'.$value);
				$tenchuyenvien		= _post('tenchuyenvien_'.$value);
				$chuyenvienph		= _post('chuyenvienph_'.$value);
				$tenchuyenvienph	= _post('tenchuyenvienph_'.$value);
				$hanxuly			= _post('hanxuly_'.$value);
				if(!empty($chuyenvien))
				{
					$mangchuyennhan = array(
						'FK_iMaVB'		=> $value,
						'FK_iMaDV'		=> $donvi,
						'FK_iMaCB_Gui'	=> $taikhoan,
						'sNoiDung'		=> $tenchuyenvien.$tenchuyenvienph,
						'FK_iMaCB_Nhan'	=> $chuyenvien,
						'sThoiGian'		=> date('Y-m-d H:i:s'),
						'CT_PH'			=> 2,
						'phongban'		=> $phongban,
						'sHanGiaiQuyet'	=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
						'iTrangThai'	=> 1,
						'tralai'		=> 1,
						'nguoinhan'		=> 1
					);
					$this->Mdanhmuc->themDuLieu('tbl_chuyennhan_caphai',$mangchuyennhan);
					if(!empty($chuyenvienph))
					{
						$phoihop = explode(',',$chuyenvienph);
						foreach ($phoihop as $key => $val) {
							$mangphoihop[] = array(
								'FK_iMaVB'		=> $value,
								'FK_iMaDV'		=> $donvi,
								'FK_iMaCB_Gui'	=> $taikhoan,
								'sNoiDung'		=> $tenchuyenvien.$tenchuyenvienph,
								'FK_iMaCB_Nhan'	=> $val,
								'sThoiGian'		=> date('Y-m-d H:i:s'),
								'CT_PH'			=> 2,
								'phongban'		=> $phongban,
								'sHanGiaiQuyet'	=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
								'iTrangThai'	=> 1,
								'tralai'		=> 1,
								'nguoinhan'		=> 2
							);
						}
						$this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_caphai',$mangphoihop);
						$mangphoihop=array();
					}
				}
				$this->Mvanban_caphai->capnhatTrangThaiNguoiChuyen($value,$taikhoan);
				$mangcapnhat = array(
					'FK_iMaCB_CV_CT'	=> $chuyenvien,
					'FK_iMaCB_CV_PH'	=> $chuyenvienph,
					'sGoiY_CV_CT'		=> $tenchuyenvien,
					'sGoiY_CV_PH'		=> $tenchuyenvienph
				);
				$this->Mdanhmuc->capnhatDuLieu('PK_iMaVB',$value,'tbl_vanbanden_caphai',$mangcapnhat);
			}
			return messagebox('Duyệt văn bản thành công!','info');
		}
	}

}

/* End of file Cvanbanchoxuly_pp_ch.php */
/* Location: ./application/controllers/vanbanden_caphai/Cvanbanchoxuly_pp_ch.php */