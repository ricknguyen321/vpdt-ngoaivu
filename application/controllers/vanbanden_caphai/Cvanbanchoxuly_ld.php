<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanchoxuly_ld extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mvanbanchoxuly');
        $this->load->model('vanbanden_caphai/Mvanban_caphai');
    }
    public function index()
    {
        $donvi             = $this->_session['FK_iMaPhongHD'];
        $quyen             = $this->_session['iQuyenHan_DHNB'];
        $taikhoan          = $this->_session['PK_iMaCB'];
        if(_post('duyet'))
        {
            $data['content'] = $this->duyetDauViec();
        }
        $data['lanhdaopho']= $this->Mvanban_caphai->layCB($donvi,2,NULL);
        $data['phongban']  = $this->Mdanhmuc->layDuLieu('FK_iMaDV',$donvi,'tbl_phongban_caphai');
        $data['dsvanban']  = $this->Mvanban_caphai->layDSVB_Phong_CT($donvi,$taikhoan,1,1,1,NULL,NULL,NULL,NULL);
        if(!empty($data['dsvanban']))
        {
            foreach ($data['dsvanban'] as $key => $value) {
                $file = $this->Mvanban_caphai->layFielVB_caphai($value['PK_iMaVB'],NULL,1);
                $data['dsvanban'][$key]['sDuongDan'] = ($file)?$file[0]['sDuongDan']:'';
            }
        }
        $data['title']     = 'Văn bản chờ xử lý';
        $temp['data']      = $data;
        $temp['template']  = 'vanbanden_caphai/Vvanbanchoxuly_ld';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function duyetDauViec()
	{
		$donvi    = $this->_session['FK_iMaPhongHD'];
		$taikhoan = $this->_session['PK_iMaCB'];
		$mavanban = _post('mavanban');
		if(!empty($mavanban))
		{
			foreach ($mavanban as $key => $value) {
				$lanhdaopho			= _post('lanhdaopho_'.$value);
				$tenlanhdaopho		= _post('tenlanhdaopho_'.$value);
				$phongchutri		= _post('phongchutri_'.$value);
				$tenchutri 			= _post('chutri_'.$value);
				$phongphoihop		= _post('phongph_'.$value);
				$tenphongphoihop	= _post('tenphongph_'.$value);
				$hanxuly			= _post('hanxuly_'.$value);
				if(!empty($lanhdaopho))
				{
					$mangchuyennhan = array(
						'FK_iMaVB'			=> $value,
						'FK_iMaDV'			=> $donvi,
						'FK_iMaCB_Gui'		=> $taikhoan,
						'sNoiDung'	        => $tenlanhdaopho,
						'FK_iMaCB_Nhan'		=> $lanhdaopho,
						'sThoiGian'			=> date('Y-m-d H:i:s'),
						'CT_PH'				=> 1,
						'sHanGiaiQuyet'		=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
						'iTrangThai'		=> 1,
						'tralai'			=> 1
					);
					$this->Mdanhmuc->themDuLieu('tbl_chuyennhan_caphai',$mangchuyennhan);
				}
				if(!empty($phongchutri)&&empty($lanhdaopho))
				{
                    $laytruongphong = $this->Mvanban_caphai->layCB($donvi,3,$phongchutri);
                    $truongphong    = $laytruongphong[0]['PK_iMaCB'];
					$mangchuyennhan = array(
						'FK_iMaVB'			=> $value,
						'FK_iMaDV'			=> $donvi,
						'FK_iMaCB_Gui'		=> $taikhoan,
						'sNoiDung'	        => $tenchutri.$tenphongphoihop,
						'FK_iMaCB_Nhan'		=> $truongphong,
						'sThoiGian'			=> date('Y-m-d H:i:s'),
						'CT_PH'				=> 2,
						'phongban'			=> $phongchutri,
						'sHanGiaiQuyet'		=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
						'iTrangThai'		=> 1,
						'tralai'			=> 1
					);
					$this->Mdanhmuc->themDuLieu('tbl_chuyennhan_caphai',$mangchuyennhan);
					if(!empty($phongphoihop))
					{
						$phoihop = explode(',',$phongphoihop);
						foreach ($phoihop as $key => $val) {
	                    	$laytruongphong = $this->Mvanban_caphai->layCB($donvi,3,$val);
                			$truongphong    = $laytruongphong[0]['PK_iMaCB'];
							$mangphoihop[] = array(
								'FK_iMaVB'			=> $value,
								'FK_iMaDV'			=> $donvi,
								'FK_iMaCB_Gui'		=> $taikhoan,
								'sNoiDung'	        => $tenchutri.$tenphongphoihop,
								'FK_iMaCB_Nhan'		=> $truongphong,
								'sThoiGian'			=> date('Y-m-d H:i:s'),
								'CT_PH'				=> 3,
								'phongban'			=> $val,
								'sHanGiaiQuyet'		=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
								'iTrangThai'		=> 1,
								'tralai'			=> 1
							);
							$them_mangphoihop[] = array(
								'FK_iMaDV'		=> $donvi,
								'FK_iMaPB'		=> $val,
								'FK_iMaVB'		=> $value,
								'FK_iMaCB_TP'	=> $truongphong
							);
						}
						$this->Mdanhmuc->themNhieuDuLieu('tbl_vanbanden_caphai_ph',$them_mangphoihop);
						$this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_caphai',$mangphoihop);
						$mangphoihop=array();
						$them_mangphoihop=array();
					}
				}
				$this->Mvanban_caphai->capnhatTrangThaiNguoiChuyen($value,$taikhoan);
				$mangcapnhat = array(
					'FK_iMaCB_LanhDao_Pho'	=> $lanhdaopho,
					'sGoiY_LanhDao_Pho'		=> $tenlanhdaopho,
					'FK_iMaPhong_CT'		=> $phongchutri,
					'FK_iMaPhong_PH'		=> $phongphoihop,
					'sGoiY_Phong_CT'		=> $tenchutri,
					'sGoiY_Phong_PH'		=> $tenphongphoihop,
					'sHan_ThongKe'			=> ($hanxuly)?date_insert($hanxuly):'0000-00-00'
				);
				$this->Mdanhmuc->capnhatDuLieu('PK_iMaVB',$value,'tbl_vanbanden_caphai',$mangcapnhat);
			}
			return messagebox('Duyệt văn bản thành công!','info');
		}
	}
}

/* End of file Cvanbanchoxuly_ld.php */
/* Location: ./application/controllers/vanbanden_caphai/Cvanbanchoxuly_ld.php */