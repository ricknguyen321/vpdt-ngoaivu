<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbanchoxuly_cv_ch extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mvanbanchoxuly');
        $this->load->model('vanbanden_caphai/Mvanban_caphai');
    }
    public function index()
    {
        $donvi             = $this->_session['FK_iMaPhongHD'];
        $quyen             = $this->_session['iQuyenHan_DHNB'];
        $taikhoan          = $this->_session['PK_iMaCB'];
        $phongban          = $this->_session['phong_caphai'];
        $dscanbo  = $this->Mvanban_caphai->layCB($donvi,NULL,NULL);
        if(!empty($dscanbo))
        {
        	foreach ($dscanbo as $key => $cb) {
        		$data['dscanbo'][$cb['PK_iMaCB']] = $cb['sHoTen'];
        	}
        }
		$data['dsvanban']	= $this->Mvanban_caphai->layDSVB_Phong_CT($donvi,$taikhoan,2,1,1,NULL,1,NULL,NULL);
        if(!empty($data['dsvanban']))
        {
            foreach ($data['dsvanban'] as $key => $value) {
                $file = $this->Mvanban_caphai->layFielVB_caphai($value['PK_iMaVB'],NULL,1);
                $data['dsvanban'][$key]['sDuongDan'] = ($file)?$file[0]['sDuongDan']:'';
                $data['dsvanban'][$key]['chuyennhan']= $this->Mvanban_caphai->layChuyenNhan($value['PK_iMaVB'],array(1,2),array(0,1));
            }
        }
        $data['title']     = 'Văn bản chờ xử lý';
        $temp['data']      = $data;
        $temp['template']  = 'vanbanden_caphai/Vvanbanchoxuly_cv_ch';
		$this->load->view('layout_admin/layout',$temp);
    }

}

/* End of file Cvanbanchoxuly_cv_ch.php */
/* Location: ./application/controllers/vanbanden_caphai/Cvanbanchoxuly_cv_ch.php */