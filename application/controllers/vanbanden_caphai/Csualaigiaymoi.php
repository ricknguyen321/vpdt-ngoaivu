<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csualaigiaymoi extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mnhapmoi');
    }
    public function index()
    {
        $data['khuvuc']     = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_khuvuc');
        $donvi = $this->_session['FK_iMaPhongHD'];
        $canbo = $this->Mnhapmoi->layCB($donvi,6);
        foreach ($canbo as $key => $cb) {
            $data['dscanbo'][$cb['PK_iMaCB']] = $cb['sHoTen'];
        }
        $nam   = date('Y');
        $ma    = _get('id');
        if(is_numeric($ma))
        {
            $data['thongtin'] = $this->Mdanhmuc->layDuLieu('PK_iMaVB',$ma,'tbl_vanbanden_caphai');
        }
        else{
            redirect('danhsachtongthe_caphai');
        }
        if(_post('luulai'))
        {
            $this->capnhatDuLieu($ma);
        }
		$data['title']      = 'Sửa lại giấy mời';
		$temp['data']       = $data;
		$temp['template']   = 'vanbanden_caphai/Vsualaigiaymoi';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function capnhatDuLieu($ma)
    {
    	$donvi = $this->_session['FK_iMaPhongHD'];
        
		$giohopgm	= _post('giohopgm');
		$ngayhopgm	= _post('ngayhopgm');
		$diadiem	= _post('diadiem');
		$ngayhop	= _post('ngayhop');
        $data = array(
			'FK_iMaDV'		=> $donvi,
			'iSoDen'		=> _post('soden'),
			'sNoiGuiDen'	=> _post('noiguiden'),
			'sKyHieu'		=> _post('sokyhieu'),
			'sTenLVB'		=> 'Giấy mời',
			'sMoTa'			=> _post('noidung'),
			'sGioMoi'		=> $giohopgm,
			'sNgayMoi'		=> date_insert($ngayhopgm),
			'sDiaDiem'		=> $diadiem,
			'sNoiDung'		=> _post('noidunghop'),
            'iGiayMoi'      => 2,
			'sNguoiKy'		=> _post('nguoiky'),
			'sChucVu'		=> _post('chucvu'),
			'sNgayKy'		=> date_insert(_post('ngayky')),
			'sNguoiChuTri'  => _post('nguoichutri'),
			'sHanGiaiQuyet'	=> '0000-00-00',
            'sHan_ThongKe'  => '0000-00-00',
			'FK_iMaCB'		=> $this->_session['PK_iMaCB'],
			'sNgayNhap'		=> date('Y-m-d H:i:s'),
			'iTrangThai'	=> 1,
			'iSoTrang'		=> _post('sotrang'),
			'sNgayNhan'	    => date_insert(_post('ngaynhan'))
        );
        $kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaVB',$ma,'tbl_vanbanden_caphai',$data);
        if($kiemtra>0)
        {
            redirect('danhsachtongthe_caphai');
        }
    }

}

/* End of file Csualaigiaymoi.php */
/* Location: ./application/controllers/vanbanden_caphai/Csualaigiaymoi.php */