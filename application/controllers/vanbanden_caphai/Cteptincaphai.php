<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cteptincaphai extends MY_Controller {

	protected $_thongtin;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbanden_caphai/Mvanbanchoxuly');
		$this->load->model('vanbanden_caphai/Mvanban_caphai');
	}
	public function index()
	{

		$quyen_caphai     = $this->_session['quyenhan_caphai'];
		if($quyen_caphai!=6)
		{
			redirect('danhsachtongthe_caphai');
		}
		$mavanban         = _get('id');
		if(empty($mavanban))
		{
			redirect('danhsachtongthe_caphai');
		}
		$mafile           = _get('mafile');
		if(!empty($mafile))
		{
			$data['content'] = $this->capnhatDuLieu($mavanban,$mafile);
		}
		else{
			$data['content'] = $this->themDuLieu($mavanban);
		}
		if(_post('xoa'))
		{
			$data['content'] = $this->xoaDuLieu($mavanban);
		}
		$data['mavanban'] = $mavanban;
		$data['thongtin'] = $this->_thongtin;
		$data['dsfile']	  = $this->Mvanban_caphai->layFielVB_caphai($mavanban,NULL,1);
		$data['title']    = 'Tệp tin văn bản đi';
		$temp['data']     = $data;
		$temp['template'] = 'vanbanden_caphai/Vteptincaphai';
		$this->load->view('layout_admin/layout',$temp);
	}
	public function xoaDuLieu($mavanban)
	{
		$duongdan = _post('xoa');
		if(file_exists($duongdan))
		{
			unlink($duongdan);
		}
		$kiemtra = $this->Mdanhmuc->xoaDuLieu('sDuongDan',$duongdan,'tbl_file_caphai');
		if($kiemtra>0)
		{
			redirect('teptincaphai?id='.$mavanban);
		}
	}
	public function capnhatDuLieu($mavanban,$mafile)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaFile',$mafile,'tbl_file_caphai');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenFile' => _post('tenteptin')
				);
			$kiemtra = $this->Mdanhmuc->capnhatDuLieu('PK_iMaFile',$mafile,'tbl_file_caphai',$data);
				if($kiemtra>0)
				{
					redirect('teptincaphai?id='.$mavanban);
				}
				else{
					return messagebox('Cập nhật tệp tin thất bại','danger');
				}
		}
	}
	public function themDuLieu($mavanban)
	{
		if(_post('luudulieu'))
		{
			$donvi	= $this->_session['FK_iMaPhongHD'];
			$name	= $_FILES['files']['name'];
			if(!empty($name[0]))
			{
				$time = time();
				$this->upload('caphai_'.date('Y'),$time);
				$files = array();
				foreach ($name as $key => $value) {
					$files[] = array(
						'FK_iMaVB'	=> $mavanban,
						'FK_iMaDV'	=> $donvi,
						'sTenFile'	=> _post('tenteptin'),
						'sDuongDan'	=> 'caphai_'.date('Y').'/'.$time.'_'.clear($value),
						'sThoiGian'	=> date('Y-m-d H:i:s'),
						'FK_iMaCB'	=> $this->_session['PK_iMaCB'],
						'iTrangThai'=> 1
						);
				}
				$kiemtra = $this->Mdanhmuc->themNhieuDuLieu('tbl_file_caphai',$files);
				if($kiemtra>0)
				{
					$this->Mdanhmuc->setDuLieu('PK_iMaVB',$mavanban,'tbl_vanbanden_caphai','iFile',1);
					return messagebox('Thêm tệp tin thành công','info');
				}
				else{
					return messagebox('Thêm tệp tin thất bại','danger');
				}
			}
		}
	}
	public function upload($dir,$time)
	{
        $fileNumber = count($_FILES["files"]["name"]);
        if ($fileNumber > 0) {
            $file = $_FILES;
            for ($i = 0; $i < $fileNumber; $i++) {
                if ($_FILES['files']['name'][$i]) {
                    #initialization new file data
					$_FILES['files']['name']     = $file['files']['name'][$i];
					$_FILES['files']['type']     = $file['files']['type'][$i];
					$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
					$_FILES['files']['error']    = $file['files']['error'][$i];
					$_FILES['files']['size']     = $file['files']['size'][$i];
					if(is_dir($dir)==false){
						mkdir($dir);		// Create directory if it does not exist
					}
					$config['upload_path']   = $dir.'/';
					$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|png|jpg|jpeg';
					$config['overwrite']     = true;
					$config['file_name']     = $time.'_'.clear($_FILES['files']['name']);
					$this->load->library('upload');
                 	$this->upload->initialize($config);
                    $checkResult = $this->upload->do_upload('files');
                    $fileData = $this->upload->data();
                }
            }
        }
	}

}

/* End of file Cteptincaphai.php */
/* Location: ./application/controllers/vanbanden_caphai/Cteptincaphai.php */