<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvanbandaxuly_tp_ch_ph extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('vanbanden_caphai/Mvanbanchoxuly');
        $this->load->model('vanbanden_caphai/Mvanban_caphai');
    }
    public function index()
    {
        $donvi             = $this->_session['FK_iMaPhongHD'];
        $quyen             = $this->_session['iQuyenHan_DHNB'];
        $taikhoan          = $this->_session['PK_iMaCB'];
        $phongban          = $this->_session['phong_caphai'];
        if(_post('duyet'))
        {
            $data['content'] = $this->duyetDauViec();
        }
        $dscanbo  = $this->Mvanban_caphai->layCB($donvi,NULL,NULL);
        if(!empty($dscanbo))
        {
        	foreach ($dscanbo as $key => $cb) {
        		$data['dscanbo'][$cb['PK_iMaCB']] = $cb['sHoTen'];
        	}
        }
		$data['phophong']	= $this->Mvanban_caphai->layCB_Phong($taikhoan,$donvi,4,$phongban);
		$data['chuyenvien']	= $this->Mvanban_caphai->layCB_Phong($taikhoan,$donvi,5,$phongban);
		$data['dsvanban']	= $this->Mvanban_caphai->layDSVB_Phong_PH_ChuaGiaiQuyet($donvi,$taikhoan,NULL,NULL,NULL);
        if(!empty($data['dsvanban']))
        {
            foreach ($data['dsvanban'] as $key => $value) {
                $file = $this->Mvanban_caphai->layFielVB_caphai($value['PK_iMaVB'],NULL,1);
                $data['dsvanban'][$key]['sDuongDan'] = ($file)?$file[0]['sDuongDan']:'';
            }
        }
        $data['title']     = 'Văn bản đã xử lý';
        $temp['data']      = $data;
        $temp['template']  = 'vanbanden_caphai/Vvanbandaxuly_tp_ch_ph';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function duyetDauViec()
	{
		$donvi    = $this->_session['FK_iMaPhongHD'];
		$taikhoan = $this->_session['PK_iMaCB'];
		$mavanban = _post('mavanban');
		$phongban = $this->_session['phong_caphai'];
		if(!empty($mavanban))
		{
			foreach ($mavanban as $key => $value) {
				$mangchuyennhan = $this->Mvanban_caphai->layMaChuyenNhan($value,$taikhoan);
				$this->Mvanban_caphai->xoaChuyenNhan($value,$mangchuyennhan['PK_iMaCN'],$phongban,NULL);
				$phophong			= _post('phophong_'.$value);
				$tenphophong		= _post('tenphophong_'.$value);
				$phophongph			= _post('phophongph_'.$value);
				$tenphophongph		= _post('tenphophongph_'.$value);
				$chuyenvien			= _post('chuyenvien_'.$value);
				$tenchuyenvien		= _post('tenchuyenvien_'.$value);
				$chuyenvienph		= _post('chuyenvienph_'.$value);
				$tenchuyenvienph	= _post('tenchuyenvienph_'.$value);
				$hanxuly			= _post('hanxuly_'.$value);
				if(!empty($phophong))
				{
					$mangchuyennhan = array(
						'FK_iMaVB'		=> $value,
						'FK_iMaDV'		=> $donvi,
						'FK_iMaCB_Gui'	=> $taikhoan,
						'sNoiDung'		=> $tenphophong.$tenphophongph,
						'FK_iMaCB_Nhan'	=> $phophong,
						'sThoiGian'		=> date('Y-m-d H:i:s'),
						'CT_PH'			=> 3,
						'phongban'		=> $phongban,
						'sHanGiaiQuyet'	=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
						'iTrangThai'	=> 1,
						'tralai'		=> 1,
						'nguoinhan'		=> 1
					);
					$this->Mdanhmuc->themDuLieu('tbl_chuyennhan_caphai',$mangchuyennhan);
					if(!empty($phophongph))
					{
						$phophongphoihop = explode(',',$phophongph);
						foreach ($phophongphoihop as $key => $val) {
							$mangphophoihop[] = array(
								'FK_iMaVB'		=> $value,
								'FK_iMaDV'		=> $donvi,
								'FK_iMaCB_Gui'	=> $taikhoan,
								'sNoiDung'		=> $tenphophong.$tenphophongph,
								'FK_iMaCB_Nhan'	=> $val,
								'sThoiGian'		=> date('Y-m-d H:i:s'),
								'CT_PH'			=> 3,
								'phongban'		=> $phongban,
								'sHanGiaiQuyet'	=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
								'iTrangThai'	=> 1,
								'tralai'		=> 1,
								'nguoinhan'		=> 2
							);
						}
						$this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_caphai',$mangphophoihop);
						$mangphophoihop= array();
					}
				}
				if(!empty($chuyenvien)&&empty($phophong))
				{
					$mangchuyennhan = array(
						'FK_iMaVB'		=> $value,
						'FK_iMaDV'		=> $donvi,
						'FK_iMaCB_Gui'	=> $taikhoan,
						'sNoiDung'		=> $tenchuyenvien.$tenchuyenvienph,
						'FK_iMaCB_Nhan'	=> $chuyenvien,
						'sThoiGian'		=> date('Y-m-d H:i:s'),
						'CT_PH'			=> 3,
						'phongban'		=> $phongban,
						'sHanGiaiQuyet'	=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
						'iTrangThai'	=> 1,
						'tralai'		=> 1,
						'nguoinhan'		=> 1
					);
					$this->Mdanhmuc->themDuLieu('tbl_chuyennhan_caphai',$mangchuyennhan);
					if(!empty($chuyenvienph))
					{
						$phoihop = explode(',',$chuyenvienph);
						foreach ($phoihop as $key => $val) {
							$mangphoihop[] = array(
								'FK_iMaVB'		=> $value,
								'FK_iMaDV'		=> $donvi,
								'FK_iMaCB_Gui'	=> $taikhoan,
								'sNoiDung'		=> $tenchuyenvien.$tenchuyenvienph,
								'FK_iMaCB_Nhan'	=> $val,
								'sThoiGian'		=> date('Y-m-d H:i:s'),
								'CT_PH'			=> 3,
								'phongban'		=> $phongban,
								'sHanGiaiQuyet'	=> ($hanxuly)?date_insert($hanxuly):'0000-00-00',
								'iTrangThai'	=> 1,
								'tralai'		=> 1,
								'nguoinhan'		=> 2
							);
						}
						$this->Mdanhmuc->themNhieuDuLieu('tbl_chuyennhan_caphai',$mangphoihop);
						$mangphoihop=array();
					}
				}
				$this->Mvanban_caphai->capnhatTrangThaiNguoiChuyen($value,$taikhoan);
				$mangcapnhat = array(
					'FK_iMaCB_PP_CT'	=> $phophong,
					'FK_iMaCB_PP_PH'	=> $phophongph,
					'sGoiY_PP_CT'		=> $tenphophong,
					'sGoiY_PP_PH'		=> $tenphophongph,
					'FK_iMaCB_CV_CT'	=> $chuyenvien,
					'FK_iMaCB_CV_PH'	=> $chuyenvienph,
					'sGoiY_CV_CT'		=> $tenchuyenvien,
					'sGoiY_CV_PH'		=> $tenchuyenvienph
				);
				$this->Mvanban_caphai->capnhatVB_PH($value,$phongban,$mangcapnhat);
			}
			return messagebox('Duyệt văn bản thành công!','info');
		}
	}

}

/* End of file Cvanbandaxuly_tp_ch_ph.php */
/* Location: ./application/controllers/vanbanden_caphai/Cvanbandaxuly_tp_ch_ph.php */