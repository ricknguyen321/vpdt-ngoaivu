<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cdangnhap extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Mdangnhap');
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$dateb = getdate();
		$data['giaythu'] = $dateb['seconds'];
		$data['ngaythu'] = $dateb['mday'];
		$data['thangthu'] = $dateb['mon'];
		$is_mobile = '0';
		if(preg_match('/(android|iphone|ipad|up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
			$is_mobile=1;
		if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')>0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']))))
			$is_mobile=1;
		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
		$mobile_agents = array('w3c ','acs-','alav','alca','amoi','andr','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','oper','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda','xda-');
	 
		if(in_array($mobile_ua,$mobile_agents))
			$is_mobile=1;
	 
		if (isset($_SERVER['ALL_HTTP'])) {
			if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini')>0)
				$is_mobile=1;
		}
		if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows')>0)
			$is_mobile=0;
		$data['is_mobile'] = $is_mobile;
		
		$data['url'] = base_url();
		$data['ketqua']='';
		if($this->input->post('dangnhap'))
		{
			$ketqua         =$this->DangNhap();
			$data['ketqua'] = $ketqua; 
		}
		$this->parser->parse('Vdangnhap', $data, FALSE);
	}

	// �    g nhập
	public function DangNhap()
	{
		$taikhoan = $this->input->post('taikhoan');
		$matkhau  = md5($this->input->post('matkhau'));
		//$nam 	  = _post('db_year');
		$nam 	  = date('Y');
		if($nam > 2017){
            $CI = &get_instance();
            $this->db = $CI->load->database('default_'.$nam, TRUE);
        }
		$vanban =$this->Mdangnhap->kiemtraDangNhap($taikhoan,$matkhau);
		if(!empty($vanban))
		{				
			if($taikhoan=='admin'){
				$loaiphanmem = array('iLoaiPhanMem'=>3);
				$vanban= array_merge($vanban,$loaiphanmem);
			}
			$_SESSION['nam'] = $nam;
			setcookie('kiemtra','ok',time()+7200,'/');
			$session_data=array('vanban'=>$vanban,'nam'=> $nam);//pr($session_data);
			setcookie('FK_iMaCB',$vanban['FK_iMaCB'],time()+7200,'/');
			$this->session->set_userdata($session_data);
			// khi ch    i   xong: s  t  chuy   vi   ch a ho�n th�nh sang tu   t  
			/*if(date('l')=='Monday'){
	            $week = (int)date("W", strtotime(date('Y-m-d')));
	            $week_1 = $week-1;
	            //$check_data ='';
	            $check_data = $this->Mdanhmuc->layDuLieu2('tuan',$week,'canbo_id',$vanban['PK_iMaCB'],'kehoach');
	            if(empty($check_data)){
	                $giatri = $this->Mdanhmuc->getData_kehoach('tuan',$week_1,'active',3,'loai_kh',3,'canbo_id',$vanban['PK_iMaCB'],'kehoach');
	                foreach ($giatri as $key => $value) {
	                    $value['kh_id_sub'] = $value['kh_id'];
	                    $kh_id = $value['kh_id'];
	                    unset($value['kh_id']);
	                    $value['tuan'] = $week;
	                    $this->Mdanhmuc->themDuLieu('kehoach',$value);
	                    if($value['ngay_han'] < date('Y-m-d') && $value['ngay_han'] >'2017-01-01'){
	                        $this->Mdanhmuc->quahan($kh_id,$week_1);
	                	}
	                }
	            }
			}*/
			
			$pattern = "/^(?=.*\d)(?=.*\W).{8,16}$/";
			$result = preg_match ($pattern, $this->input->post('matkhau'));
			
			if ( !$result ) {
				echo '<script language="javascript">';
				echo 'alert("Weak password. Please change your password!")';
				echo '</script>';
				redirect(base_url().'doimatkhaucn');
			}
			
			if($vanban['iQuyenHan_DHNB']==9 || $vanban['iQuyenHan_DHNB']==2)
			{
				redirect(base_url().'vanbanden');
			}
			elseif($vanban['iQuyenHan_DHNB']==1)
			{
				redirect(base_url().'welcomead');
			}
			elseif($vanban['iLoaiPhanMem']==2){
				redirect(base_url().'thongtinmoi');
			}
			else{
				redirect(base_url().'welcome.html');
			}
		}
		else{
			return 'failed';
		}
	}
	// �    g xuất
	public function DangXuat() {
		$this->session->userdata = array();
		$this->session->sess_destroy();
		$this->input->set_cookie('', '', time()-5400);
		redirect(base_url().'dangnhap');
		exit();
	}

}

/* End of file Cdangnhap.php */
/* Location: ./application/controllers/dangnhap/Cdangnhap.php */