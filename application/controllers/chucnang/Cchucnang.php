<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchucnang extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']     = 'chức năng';
		$ketqua            = ($this->uri->segment(2))?$this->capnhatDuLieu():$this->themDuLieu();
		$data['content']   = $ketqua;
		$data['thongtin']  = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_chucnang');
		$data['dstheloai'] = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_nhomchucnang');
		$temp['data']      = $data;
		$temp['template']  = 'chucnang/Vchucnang';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if($this->input->post('luudulieu'))
		{	
			$data=array(
				'sTenCN'       => $this->input->post('dulieu'),
				'FK_iMaNhomCN' => $this->input->post('nhomchucnang'),
				'sLink'        => $this->input->post('link')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_chucnang',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm chức năng thành công','info');
			}
			else{
				return messagebox('Thêm chức năng thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu()
	{
		$id = $this->uri->segment(2);
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCN',$id,'tbl_chucnang');
		
		if($this->input->post('luudulieu'))
		{
			$data=array(
				'sTenCN'       => $this->input->post('dulieu'),
				'FK_iMaNhomCN' => $this->input->post('nhomchucnang'),
				'sLink'        => $this->input->post('link')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaCN',$id,'tbl_chucnang',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCN',$id,'tbl_chucnang');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật chức năng thành công','info');
			}
			else{
				return messagebox('Cập nhật chức năng thất bại','danger');
			}
		}
	}

}

/* End of file Cchucnang.php */
/* Location: ./application/controllers/chucnang/Cchucnang.php */