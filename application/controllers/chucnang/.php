<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupload_files_vbdi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('danhmuc/Mdanhmuc');
		$this->load->model('vanbandi/Mvanbandi');
	}
	public function index()
	{
		//ini_set('memory_limit', '2048M'); //2GB
		$data['content'] = '';
		$mavbdi = _get('ma');
		$isodi = _get('isodi');
		$loai = _get('loai');		
		$data['isodi'] = $isodi;
		if(_post('upload'))
		{
			$data['content'] = $this->upload($mavbdi, $isodi, $loai);
		}
		$data['title']    = 'Phát hành văn bản đi - Sở Ngoại vụ Hà Nội';
		$data['url']      = base_url();
		$this->parser->parse('vanbandi/Vupload_files_vbdi',$data);
	}
	public function upload($mavbdi, $isodi, $loai)
	{
		$dir  = 'doc_uploads_'.date('Y');
        $name = $_FILES['files']['name'];
        $tong = count($name);
		
		if(!empty($name[0]))
		{
            $file = $_FILES;
            for ($i = 0; $i < $tong; $i++) {
				$_FILES['files']['name']     = $file['files']['name'][$i];
				$_FILES['files']['type']     = $file['files']['type'][$i];
				$_FILES['files']['tmp_name'] = $file['files']['tmp_name'][$i];
				$_FILES['files']['error']    = $file['files']['error'][$i];
				$_FILES['files']['size']     = $file['files']['size'][$i];

				if(is_dir($dir)==false){
					mkdir($dir,0777, true);		// Create directory if it does not exist
				}
				$time= time();
				$file_name = $_FILES['files']['name'];
				$dinhdang = substr($file_name,-3);
				$tentep    = explode('_', $file_name);
				$sodi      = explode('.', $tentep[0]);				
				
				if ($sodi[0] != $isodi) {					
					echo '<script language="javascript">';
					echo 'alert("Tên file và số văn bản trên hệ thống chưa khớp!\nVui lòng chọn lại file.")';
					echo '</script>';
					return messagebox('Gửi email thất bại! ','danger');
				}
				
				if ($dinhdang != 'pdf') {					
					echo '<script language="javascript">';
					echo 'alert("Định dạng file không đúng!\nVui lòng chọn lại file pdf.")';
					echo '</script>';
					return messagebox('Gửi email thất bại! ','danger');
				}
				
				if(!strstr($sodi[0],'-')){
					$thongtin  = $this->Mdanhmuc->layDuLieu_sendMail($sodi[0],$loai);
				}
				else{
					$thongtin  = '';
				}
				
				if ($thongtin[0]['PK_iMaVBDi'] == '' || $thongtin[0]['PK_iMaVBDi'] != $mavbdi) {					
					echo '<script language="javascript">';
					echo 'alert("Chọn file chưa đúng!\nVui lòng chọn lại file và nhấn gửi email.")';
					echo '</script>';
					return messagebox('Gửi email thất bại! ','danger');
				}
				
				if(!empty($thongtin))
				{					
					foreach ($thongtin as $key => $value) {
						$data=array(
						'FK_iMaVBDi' => $thongtin[$key]['PK_iMaVBDi'],
						'sTenFile'   => $i.$time.'_vbdi_'.clear($file_name),
						'sDuongDan'  => $dir.'/'.$i.$time.'_vbdi_'.clear($file_name),
						'sThoiGian'  => date('Y-m-d H:i:s'),
						'FK_iMaCB'   => $this->_session['PK_iMaCB']
						);
					$this->Mdanhmuc->themDuLieu('tbl_files_vbdi',$data);
					$this->Mdanhmuc->setDuLieu('PK_iMaVBDi',$thongtin[$key]['PK_iMaVBDi'],'tbl_vanbandi','iFile',1);
					}
				}
				$config['upload_path']   = $dir;
				$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
				$config['overwrite']     = true;
				$config['file_name']     = $i.$time.'_vbdi_'.clear($file_name);
				$this->load->library('upload');
             	$this->upload->initialize($config);
                $checkResult = $this->upload->do_upload('files');
                $fileData = $this->upload->data();
                if(!empty($thongtin))
				{
					foreach ($thongtin as $key => $value) {
						$this->guiMail($thongtin[$key]['PK_iMaVBDi']);// truyền mã văn bản lấy từ số xuống
					}
				}
            }
            return messagebox('Gửi email THÀNH CÔNG','info');
        }
        else
        {
        	return messagebox('Bạn chưa chọn File!','danger');
        }
	}
	public function guiMail($ma)
	{
		$thongtin = $this->Mvanbandi->layThongTinTheoMaVB($ma);
		$this->createXML($ma);
		$this->sendMail($ma);
		if(!empty($thongtin['FK_iMaDV_Ngoai']))
		{
			$this->createXML_N($ma);
			$this->sendMail_N($ma);
		}
	}
	

}

/* End of file Cupload_files_vbdi.php */
/* Location: ./application/controllers/vanbandi/Cupload_files_vbdi.php */