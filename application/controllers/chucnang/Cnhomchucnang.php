<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhomchucnang extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']       = 'Nhóm chức năng';
		$ketqua              = ($this->uri->segment(2))?$this->capnhatDuLieu():$this->themDuLieu();
		$data['content']     = $ketqua;
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_nhomchucnang');
		
		$temp['data']     = $data;
		$temp['template'] = 'chucnang/Vnhomchucnang';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if($this->input->post('luudulieu'))
		{
			$data=array(
				'sTenNhomCN' => $this->input->post('dulieu'),
				'iCon'       => $this->input->post('icon'),
				'sMaMau'     => $this->input->post('mau')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_nhomchucnang',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm nhóm chức năng thành công','info');
			}
			else{
				return messagebox('Thêm nhóm chức năng thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu()
	{
		$id = $this->uri->segment(2);
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNhomCN',$id,'tbl_nhomchucnang'	);
		if($this->input->post('luudulieu'))
		{
			$data=array(
				'sTenNhomCN' => $this->input->post('dulieu'),
				'iCon'       => $this->input->post('icon'),
				'sMaMau'     => $this->input->post('mau')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaNhomCN',$id,'tbl_nhomchucnang',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNhomCN',$id,'tbl_nhomchucnang');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật nhóm chức năng thành công','info');
			}
			else{
				return messagebox('Cập nhật nhóm chức năng thất bại','danger');
			}
		}
	}

}

/* End of file Cnhomchucnang.php */
/* Location: ./application/controllers/chucnang/Cnhomchucnang.php */