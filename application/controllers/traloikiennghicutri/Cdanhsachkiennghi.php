<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdanhsachkiennghi extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('traloikiennghicutri/Mkiennghicutri');
        $this->load->model('tailieuphucvuhop/Mtailieuphucvuhop');
    }
    public function index()
    {
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		if(_post('luudulieu'))
		{
			$data['content'] = $this->themDuLieu($taikhoan);
		}
		$action = _post('action');
		if(!empty($action))
		{
			$this->layLinhVuc();
		}
		$dulieu					= $this->dsKienNghi();
		$data['dulieu']			= $dulieu['dulieu'];
		$data['khoa']			= $dulieu['khoa'];
		$data['nhiemky']		= $dulieu['nhiemky'];
		$data['kyhop']			= $dulieu['kyhop'];
		$data['trangthai']		= $dulieu['trangthai'];
		$data['cauhoi']			= $dulieu['cauhoi'];
		$data['cutri']			= $dulieu['cutri'];
		$data['noidungcauhoi']	= $dulieu['noidungcauhoi'];
		$data['linhvuccha']		= $dulieu['linhvuccha'];
		$data['linhvuccon']		= $dulieu['linhvuccon'];
		$data['lanhdaoo']		= $dulieu['lanhdao'];
		$data['phongchutri']	= $dulieu['phongchutri'];
		$data['linhvuc']		= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_linhvuccha');
		$data['linhvuccon']		= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_linhvuccon');
		$data['lanhdao']		= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		foreach ($data['lanhdao'] as $key => $value) {
			$data['manglanhdao'][$value['PK_iMaCB']] = $value['sHoTen'];
		}
		$data['phongban']		= $this->Mtailieuphucvuhop->layPhongBanDuThao();
		foreach ($data['phongban'] as $key => $value) {
			$data['mangphong'][$value['PK_iMaPB']] = $value['sVietTat'];
		}
		$data['thongtin']		= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_kiennghicutri');
		$data['title']			= 'Danh sách kiến nghị';
		$temp['data']			= $data;
		$temp['template']		= 'traloikiennghicutri/Vdanhsachkiennghi';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function layLinhVuc()
    {
    	$macha = _post('macha');
    	$ketqua=$this->Mkiennghicutri->layLinhVucCon($macha);
    	echo json_encode($ketqua); exit();
    }
    public function dsKienNghi()
    {
		$data['khoa']			= _get('khoa');
		$data['nhiemky']		= _get('nhiemky');
		$data['kyhop']			= _get('kyhop');
		$data['trangthai']		= _get('trangthai');
		$data['cauhoi']			= _get('cauhoi');
		$data['cutri']			= _get('cutri');
		$data['noidungcauhoi']	= _get('noidungcauhoi');
		$data['linhvuccha']		= _get('linhvuccha');
		$data['linhvuccon']		= _get('linhvuccon');
		$data['lanhdao']		= _get('lanhdao');
		$data['phongchutri']	= _get('phongchutri');
		$data['dulieu'] = $this->Mkiennghicutri->layDSKienNghi(
			$data['khoa'],
			$data['nhiemky'],
			$data['kyhop'],
			$data['trangthai'],
			$data['cauhoi'],
			$data['cutri'],
			$data['noidungcauhoi'],
			$data['linhvuccha'],
			$data['linhvuccon'],
			$data['lanhdao'],
			$data['phongchutri']
		);
    	return $data;
    }

}

/* End of file Cdanhsachkiennghi.php */
/* Location: ./application/controllers/traloikiennghicutri/Cdanhsachkiennghi.php */