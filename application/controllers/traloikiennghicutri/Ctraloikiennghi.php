<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctraloikiennghi extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('traloikiennghicutri/Mkiennghicutri');
        $this->load->model('tailieuphucvuhop/Mtailieuphucvuhop');
    }
    public function index()
    {
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		$ma					= $this->uri->segment(2);
		$data['thongtin']	= $this->Mdanhmuc->layDuLieu('PK_iMaKienNghi',$ma,'tbl_kiennghicutri');
		$maphong			= $data['thongtin'][0]['FK_iMaPhong_CT'];
		$data['lienquan']	= $this->Mkiennghicutri->layKienNghitheoPhong($maphong,$ma);
		$data['phongban']	= $this->Mtailieuphucvuhop->layPhongBanDuThao();
		foreach ($data['phongban'] as $key => $value) {
			$data['mangphong'][$value['PK_iMaPB']] = $value['sTenPB'];
		}
		$data['title']		= 'Trả lời kiến nghị';
		$temp['data']		= $data;
		$temp['template']	= 'traloikiennghicutri/Vtraloikiennghi';
		$this->load->view('layout_admin/layout',$temp);
    }

}

/* End of file Ctraloikiennghi.php */
/* Location: ./application/controllers/traloikiennghicutri/Ctraloikiennghi.php */