<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnhapmoikiennghi extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('danhmuc/Mdanhmuc');
        $this->load->model('traloikiennghicutri/Mkiennghicutri');
    }
    public function index()
    {
		$quyen				= $this->_session['iQuyenHan_DHNB'];
		$taikhoan			= $this->_session['PK_iMaCB'];
		if(_post('luudulieu'))
		{
			$data['content'] = $this->themDuLieu($taikhoan);
		}
		$action = _post('action');
		if(!empty($action))
		{
			$this->layLinhVuc();
		}
		$data['linhvuc']	= $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_linhvuccha');
		$data['lanhdao']	= $this->Mdanhmuc->getWhereIn('iQuyenHan_DHNB',array(4,5),'iQuyenHan_DHNB','tbl_canbo');
		$data['phongban']	= $this->Mdanhmuc->getDepartment('tbl_phongban');
		$data['thongtin']	= $this->Mkiennghicutri->layKienNghiCuoi();
		$data['title']		= 'Nhập mới kiến nghị cử tri';
		$temp['data']		= $data;
		$temp['template']	= 'traloikiennghicutri/Vnhapmoikiennghi';
		$this->load->view('layout_admin/layout',$temp);
    }
    public function layLinhVuc()
    {
    	$macha = _post('macha');
    	$ketqua=$this->Mkiennghicutri->layLinhVucCon($macha);
    	echo json_encode($ketqua); exit();
    }
    public function themDuLieu($taikhoan)
    {
    	$phoihop = _post('phongphoihop');
    	$data=array(
			'sKhoa'				=> _post('khoa'),
			'sNhiemKy'			=> _post('nhiemky'),
			'sKyHop'			=> _post('kyhop'),
			'sCauHoi'			=> _post('cauhoi'),
			'sNoiDung'			=> _post('noidung'),
			'sTenCuTri'			=> _post('cutri'),
			'FK_iMaLV_Cha'		=> _post('linhvuccha'),
			'FK_iMaLV_Con'		=> _post('linhvuccon'),
			'FK_iMaPhong_CT'	=> _post('phongchutri'),
			'FK_iMaPhong_PH'	=> ($phoihop)?implode(',',$phoihop):'',
			'FK_iMaCB_LD'		=> _post('lanhdao'),
			'FK_iMaCB_Nhap'		=> $taikhoan,
			'sNgayNhap'			=> date('Y-m-d H:i:s'),
			'iTrangThai'		=> _post('stcchutri'),
			'sNoiDungTraLoi'	=> $this->input->post('noidungtraloi',false)
    	);
    	$kiemtra = $this->Mdanhmuc->themDuLieu('tbl_kiennghicutri',$data);
    	if($kiemtra>0)
    	{
    		return messagebox('Thêm kiến nghị thành công!','info');
    	}
    }

}

/* End of file Cnhapmoikiennghi.php */
/* Location: ./application/controllers/traloikiennghicutri/Cnhapmoikiennghi.php */