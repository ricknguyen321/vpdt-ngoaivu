<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinhvuccha extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Lĩnh vực cha trả lời kiến nghị cử tri';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_linhvuccha','desc');
		$temp['data']     = $data;
		$temp['template'] = 'traloikiennghicutri/Vlinhvuccha';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenLV'      => _post('ten'),
				'iTrangThai'  => _post('trangthai'),
				'sNgayNhap'   => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_linhvuccha',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm lĩnh vực thành công','info');
			}
			else{
				return messagebox('Thêm lĩnh vực thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaLV',$ma,'tbl_linhvuccha');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenLV'      => _post('ten'),
				'iTrangThai'  => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaLV',$ma,'tbl_linhvuccha',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaLV',$ma,'tbl_linhvuccha');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật lĩnh vực thành công','info');
			}
			else{
				return messagebox('Cập nhật lĩnh vực thất bại','danger');
			}
		}
	}

}

/* End of file Clinhvuccha.php */
/* Location: ./application/controllers/traloikiennghicutri/Clinhvuccha.php */