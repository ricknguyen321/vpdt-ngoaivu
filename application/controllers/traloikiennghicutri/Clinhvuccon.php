<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinhvuccon extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Lĩnh vực con trả lời kiến nghị cử tri';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['linhvuccha']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_linhvuccha');
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_linhvuccon','desc');
		$temp['data']     = $data;
		$temp['template'] = 'traloikiennghicutri/Vlinhvuccon';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenLV'      => _post('ten'),
				'iTrangThai'  => _post('trangthai'),
				'sNgayNhap'   => date('Y-m-d',time()),
				'FK_iMaLV_Cha'=> _post('linhvuccha')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_linhvuccon',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm lĩnh vực thành công','info');
			}
			else{
				return messagebox('Thêm lĩnh vực thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaLV',$ma,'tbl_linhvuccon');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenLV'      => _post('ten'),
				'iTrangThai'  => _post('trangthai'),
				'FK_iMaLV_Cha'=> _post('linhvuccha')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaLV',$ma,'tbl_linhvuccon',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaLV',$ma,'tbl_linhvuccon');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật lĩnh vực thành công','info');
			}
			else{
				return messagebox('Cập nhật lĩnh vực thất bại','danger');
			}
		}
	}

}

/* End of file Clinhvuccon.php */
/* Location: ./application/controllers/traloikiennghicutri/Clinhvuccon.php */