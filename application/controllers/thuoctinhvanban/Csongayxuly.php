<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csongayxuly extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý số ngày xử lý';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_songayxuly','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vsongayxuly';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sNoiDung'   => _post('noidung'),
				'sVietTat'   => _post('viettat'),
				'fSoNgay'    => _post('songay'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_songayxuly',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm số ngày xử lý thành công','info');
			}
			else{
				return messagebox('Thêm số ngày xử lý thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaSoNgay',$ma,'tbl_songayxuly');
		if(_post('luudulieu'))
		{
			$data=array(
				'sNoiDung'   => _post('noidung'),
				'sVietTat'   => _post('viettat'),
				'fSoNgay'    => _post('songay'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaSoNgay',$ma,'tbl_songayxuly',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaSoNgay',$ma,'tbl_songayxuly');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật số ngày xử lý thành công','info');
			}
			else{
				return messagebox('Cập nhật số ngày xử lý thất bại','danger');
			}
		}
	}

}

/* End of file Csongayxuly.php */
/* Location: ./application/controllers/thuoctinhvanban/Csongayxuly.php */