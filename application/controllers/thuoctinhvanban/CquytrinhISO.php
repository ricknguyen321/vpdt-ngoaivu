<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CquytrinhISO extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quy trình ISO';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_quytrinhiso	','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/VquytrinhISO';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenQuyTrinh' => _post('tenquytrinh'),
				'iSoNgay'      => _post('songay'),
				'iTrangThai'   => _post('trangthai'),
				'sNgayNhap'    => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_quytrinhiso',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm quy trình ISO thành công','info');
			}
			else{
				return messagebox('Thêm quy trình ISO thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaQuyTrinh',$ma,'tbl_quytrinhiso');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenQuyTrinh' => _post('tenquytrinh'),
				'iSoNgay'      => _post('songay'),
				'iTrangThai'   => _post('trangthai'),
				'sNgayNhap'    => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaQuyTrinh',$ma,'tbl_quytrinhiso',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaQuyTrinh',$ma,'tbl_quytrinhiso');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật quy trình ISO thành công','info');
			}
			else{
				return messagebox('Cập nhật quy trình ISO thất bại','danger');
			}
		}
	}

}

/* End of file CquytrinhISO.php */
/* Location: ./application/controllers/thuoctinhvanban/CquytrinhISO.php */