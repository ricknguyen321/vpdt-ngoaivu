<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnoiguiden extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý nơi gửi đến';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_donvi','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vnoiguiden';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenDV'     => _post('ten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_donvi',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm đơn vị thành công','info');
			}
			else{
				return messagebox('Thêm đơn vị thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaDV',$ma,'tbl_donvi');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenDV'     => _post('ten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaDV',$ma,'tbl_donvi',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaDV',$ma,'tbl_donvi');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật đơn vị thành công','info');
			}
			else{
				return messagebox('Cập nhật đơn vị thất bại','danger');
			}
		}
	}

}

/* End of file Cnoiguiden.php */
/* Location: ./application/controllers/thuoctinhvanban/Cnoiguiden.php */