<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckhuvuc extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý khu vực';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_khuvuc','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vkhuvuc';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenKV'      => _post('ten'),
				'sTenVietTat' => _post('tenviettat'),
				'iTrangThai'  => _post('trangthai'),
				'sNgayNhap'   => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_khuvuc',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm khu vực thành công','info');
			}
			else{
				return messagebox('Thêm khu vực thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaKV',$ma,'tbl_khuvuc');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenKV'      => _post('ten'),
				'sTenVietTat' => _post('tenviettat'),
				'iTrangThai'  => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaKV',$ma,'tbl_khuvuc',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaKV',$ma,'tbl_khuvuc');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật khu vực thành công','info');
			}
			else{
				return messagebox('Cập nhật khu vực thất bại','danger');
			}
		}
	}

}

/* End of file Ckhuvuc.php */
/* Location: ./application/controllers/thuoctinhvanban/Ckhuvuc.php */