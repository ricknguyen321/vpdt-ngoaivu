<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinhvuc_qldv extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý lĩnh vực';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin']   = $this->_thongtin;
		$data['linhvuccha'] = $this->Mdanhmuc->layLinhVucCha();
		$data['dsdulieu']   = $this->Mdanhmuc->sapxepDuLieu('linhVuc_date','tbl_linhvuc_qldv','desc');
		$temp['data']       = $data;
		$temp['template']   = 'thuoctinhvanban/Vlinhvuc_qldv';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'id_parent'      => _post('linhvuccha'),
				'linhVuc_name'   => _post('linhvuc'),
				'linhVuc_desc'   => _post('mota'),
				'linhVuc_active' => _post('trangthai'),
				'linhVuc_date'   => date('Y-m-d H:i:s',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_linhvuc_qldv',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm lĩnh vực thành công','info');
			}
			else{
				return messagebox('Thêm lĩnh vực thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('linhVuc_id',$ma,'tbl_linhvuc_qldv');
		if(_post('luudulieu'))
		{
			$data=array(
				'id_parent'      => _post('linhvuccha'),
				'linhVuc_name'   => _post('linhvuc'),
				'linhVuc_desc'   => _post('mota'),
				'linhVuc_active' => _post('trangthai'),
				'linhVuc_date'   => date('Y-m-d H:i:s',time())
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('linhVuc_id',$ma,'tbl_linhvuc_qldv',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('linhVuc_id',$ma,'tbl_linhvuc_qldv');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật lĩnh vực thành công','info');
			}
			else{
				return messagebox('Cập nhật lĩnh vực thất bại','danger');
			}
		}
	}

}

/* End of file Clinhvuc_qldv.php */
/* Location: ./application/controllers/thuoctinhvanban/Clinhvuc_qldv.php */