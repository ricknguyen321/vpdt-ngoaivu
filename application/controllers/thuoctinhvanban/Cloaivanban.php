<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cloaivanban extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý loại văn bản';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['active']  = 1;
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['active']  = '';
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_loaivanban');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vloaivanban';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenLVB'      => _post('ten'),
				'sTenVietTat'  => _post('tenviettat'),
				'iChuyenDenGD' => _post('giamdoc'),
				'iTrangThai'   => _post('trangthai'),
				'iVanBanDi'    => _post('vanbandi'),
				'iTenPBSKH'    => _post('pb'),
				'iTenLVBSKH'   => _post('lvb'),
				'iSoDiRieng'   => _post('sorieng'),
				'sNgayNhap'    => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_loaivanban',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm loại văn bản thành công','info');
			}
			else{
				return messagebox('Thêm loại văn bản thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaLVB',$ma,'tbl_loaivanban');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenLVB'      => _post('ten'),
				'sTenVietTat'  => _post('tenviettat'),
				'iChuyenDenGD' => _post('giamdoc'),
				'iTrangThai'   => _post('trangthai'),
				'iVanBanDi'    => _post('vanbandi'),
				'iTenPBSKH'    => _post('pb'),
				'iTenLVBSKH'   => _post('lvb'),
				'iSoDiRieng'   => _post('sorieng')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaLVB',$ma,'tbl_loaivanban',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaLVB',$ma,'tbl_loaivanban');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật loại văn bản thành công','info');
			}
			else{
				return messagebox('Cập nhật loại văn bản thất bại','danger');
			}
		}
	}

}

/* End of file Cloaivanban.php */
/* Location: ./application/controllers/thuoctinhvanban/Cloaivanban.php */