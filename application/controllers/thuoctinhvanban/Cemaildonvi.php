<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cemaildonvi extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Email đơn vị';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['mailgroup']= [['ma'=>2,'ten'=>'Group quận huyện'],['ma'=>1,'ten'=>'Group Sở ban ngành'],['ma'=>3,'ten'=>'Group các đơn vị']];
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu'] = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_emaildonvi','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vemaildonvi';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sEmail'     => _post('email'),
				'sTenDV'     => _post('donvi'),
				'sSDT'       => _post('sdt'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time()),
				'mail_group' => _post('mailgroup')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_emaildonvi',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm email đơn vị thành công','info');
			}
			else{
				return messagebox('Thêm email đơn vị thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaEmailDV',$ma,'tbl_emaildonvi');
		if(_post('luudulieu'))
		{
			$data=array(
				'sEmail'     => _post('email'),
				'sSDT'       => _post('sdt'),
				'sTenDV'     => _post('donvi'),
				'iTrangThai' => _post('trangthai'),
				'mail_group' => _post('mailgroup')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaEmailDV',$ma,'tbl_emaildonvi',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaEmailDV',$ma,'tbl_emaildonvi');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật email đơn vị thành công','info');
			}
			else{
				return messagebox('Cập nhật email đơn vị thất bại','danger');
			}
		}
	}

}

/* End of file Cemaildonvi.php */
/* Location: ./application/controllers/thuoctinhvanban/Cemaildonvi.php */