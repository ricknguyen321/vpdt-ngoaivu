<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnguoiky extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý người ký';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_nguoiky','desc');
        if($this->input->post('xoafile')){
            $kiemtra = $this->Mdanhmuc->xoaDuLieu('PK_iMaNK',$this->input->post('xoafile'),'tbl_nguoiky');
            return redirect(base_url().'nguoiky');

        }
		
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vnguoiky';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sHoTen'     => _post('hoten'),
				'sChucVu'    => _post('chucvu'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_nguoiky',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm người ký thành công','info');
			}
			else{
				return messagebox('Thêm người ký thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNK',$ma,'tbl_nguoiky');
		if(_post('luudulieu'))
		{
			$data=array(
				'sHoTen'     => _post('hoten'),
				'sChucVu'    => _post('chucvu'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaNK',$ma,'tbl_nguoiky',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNK',$ma,'tbl_nguoiky');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật người ký thành công','info');
			}
			else{
				return messagebox('Cập nhật người ký thất bại','danger');
			}
		}
	}

}

/* End of file Cnguoiky.php */
/* Location: ./application/controllers/thuoctinhvanban/Cnguoiky.php */