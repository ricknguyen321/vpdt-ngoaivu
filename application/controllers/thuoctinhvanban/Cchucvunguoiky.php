<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cchucvunguoiky extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Chức vụ người ký';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_chucvunguoiky','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vchucvunguoiky';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sChucVu'     => _post('chucvu'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_chucvunguoiky',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm chức vụ người ký thành công','info');
			}
			else{
				return messagebox('Thêm chức vụ người ký thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCVNK',$ma,'tbl_chucvunguoiky');
		if(_post('luudulieu'))
		{
			$data=array(
				'sChucVu'     => _post('chucvu'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaCVNK',$ma,'tbl_chucvunguoiky',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaCVNK',$ma,'tbl_chucvunguoiky');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật chức vụ người ký thành công','info');
			}
			else{
				return messagebox('Cập nhật chức vụ người ký thất bại','danger');
			}
		}
	}

}

/* End of file Cchucvunguoiky.php */
/* Location: ./application/controllers/thuoctinhvanban/Cchucvunguoiky.php */