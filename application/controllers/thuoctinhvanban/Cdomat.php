<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdomat extends CI_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý độ mật';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_domat','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vdomat';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenDoMat'     => _post('ten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_domat',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm độ mật thành công','info');
			}
			else{
				return messagebox('Thêm độ mật thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaDM',$ma,'tbl_domat');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenDoMat'     => _post('ten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaDM',$ma,'tbl_domat',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaDM',$ma,'tbl_domat');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật độ mật thành công','info');
			}
			else{
				return messagebox('Cập nhật độ mật thất bại','danger');
			}
		}
	}

}

/* End of file Cdomat.php */
/* Location: ./application/controllers/thuoctinhvanban/Cdomat.php */