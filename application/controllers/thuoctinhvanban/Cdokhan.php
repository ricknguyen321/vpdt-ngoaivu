<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdokhan extends CI_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Quản lý độ khẩn';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_dokhan','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vdokhan';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenDoKhan'     => _post('ten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_dokhan',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm độ khẩn thành công','info');
			}
			else{
				return messagebox('Thêm độ khẩn thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaDK',$ma,'tbl_dokhan');
		if(_post('luudulieu'))
		{
			$data=array(
				'sTenDoKhan'     => _post('ten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaDK',$ma,'tbl_dokhan',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaDK',$ma,'tbl_dokhan');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật độ khẩn thành công','info');
			}
			else{
				return messagebox('Cập nhật độ khẩn thất bại','danger');
			}
		}
	}

}

/* End of file Cdokhan.php */
/* Location: ./application/controllers/thuoctinhvanban/Cdokhan.php */