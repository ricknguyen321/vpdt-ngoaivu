<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnguoikyliennganh extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Người ký liên ngành';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->sapxepDuLieu('sNgayNhap','tbl_nguoikyliennganh','desc');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vnguoikyliennganh';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sHoTen'     => _post('hoten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai'),
				'sNgayNhap'  => date('Y-m-d',time())
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_nguoikyliennganh',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm người ký liên ngành thành công','info');
			}
			else{
				return messagebox('Thêm người ký liên ngành thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNKLN',$ma,'tbl_nguoikyliennganh');
		if(_post('luudulieu'))
		{
			$data=array(
				'sHoTen'     => _post('hoten'),
				'sMota'      => _post('mota'),
				'iTrangThai' => _post('trangthai')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaNKLN',$ma,'tbl_nguoikyliennganh',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaNKLN',$ma,'tbl_nguoikyliennganh');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật người ký liên ngành thành công','info');
			}
			else{
				return messagebox('Cập nhật người ký liên ngành thất bại','danger');
			}
		}
	}

}

/* End of file Cnguoikyliennganh.php */
/* Location: ./application/controllers/thuoctinhvanban/Cnguoikyliennganh.php */