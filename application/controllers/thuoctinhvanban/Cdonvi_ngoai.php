<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdonvi_ngoai extends MY_Controller {

	protected $_thongtin;
	public function __construct() {
		parent:: __construct();
		$this->load->model('danhmuc/Mdanhmuc');
	}
	public function index()
	{
		$data['title']   = 'Mã định danh';
		$ma              = _get('id');
		if(!empty($ma))
		{
			$data['content'] = $this->capnhatDuLieu($ma);
		}
		else{
			$data['content'] = $this->themDuLieu();
		}
		$data['thongtin'] = $this->_thongtin;
		$data['dsdulieu']  = $this->Mdanhmuc->layDuLieu(NULL,NULL,'tbl_thongtin_donvi');
		$temp['data']     = $data;
		$temp['template'] = 'thuoctinhvanban/Vdonvi_ngoai';
		$this->load->view('layout_admin/layout', $temp);
	}
	public function themDuLieu()
	{
		if(_post('luudulieu'))
		{
			$data=array(
				'sMaDinhDanh' => _post('madinhdanh'),
				'sTenDV'      => _post('donvi')
				);
			$kiemtra=$this->Mdanhmuc->themDuLieu('tbl_thongtin_donvi',$data);
			if($kiemtra>0)
			{
			return messagebox('Thêm mã định danh thành công','info');
			}
			else{
				return messagebox('Thêm mã định danh thất bại','danger');
			}
		}
	}
	public function capnhatDuLieu($ma)
	{
		$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaTT',$ma,'tbl_thongtin_donvi');
		if(_post('luudulieu'))
		{
			$data=array(
				'sMaDinhDanh' => _post('madinhdanh'),
				'sTenDV'      => _post('donvi')
				);
			$kiemtra=$this->Mdanhmuc->capnhatDuLieu('PK_iMaTT',$ma,'tbl_thongtin_donvi',$data);
			$this->_thongtin = $this->Mdanhmuc->layDuLieu('PK_iMaTT',$ma,'tbl_thongtin_donvi');
			if($kiemtra>0)
			{
			return messagebox('Cập nhật mã định danh thành công','info');
			}
			else{
				return messagebox('Cập nhật mã định danh thất bại','danger');
			}
		}
	}

}

/* End of file Cdonvi_ngoai.php */
/* Location: ./application/controllers/thuoctinhvanban/Cdonvi_ngoai.php */