$(document).ready(function() {
	var url = window.location.href;
	// lấy thông tin văn bản đến dựa trên số ký hiệu
	$(document).on('change','input[name=qlv_code]',function(){
		var kyhieu = $(this).val();
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'getTTVBDen',
				kyhieu:kyhieu
			},
			success:function(repon){
				var result = JSON.parse(repon);
				var tong=result.length;
				if(tong==0)
				{
					$('.giaymoi').html('');
					$('.reset-form')[0].reset();
					$('input[name=qlv_code]').val(kyhieu);
				}
				if(tong==1)
				{
					$('input[name=qlv_date]').val(result[0]['ngayvanban']);
					$('textarea[name=qlv_noidung]').val(result[0]['sMoTa']);
					$('input[name=qlv_ld_chutri]').val(result[0]['sGiayMoiChuTri']);
					$('input[name=qlv_ld_ky]').val(result[0]['sTenNguoiKy']);
					var ma =$('#qlv_loai option:contains("'+result[0]['sTenLVB']+'")').val();
					$("#qlv_loai").val(ma).trigger("change");
                	$('#qlv_loai').select2();
					$('.giaymoi').html('');
				}
				if(tong>1)
				{
					var html='';
					var so=1;
					for(var i=0;i<tong;i++)
					{
						if(result[i]['iGiayMoi']==1 && result[i]['sNoiDung']!= null)
						{
							html+='<div class="col-md-12"><div class="form-group"><button type="button" class="btn btn-default btn-sm mavanban" nam="giaymoi" value="'+result[i]['PK_iMaVBDen']+'">'+so+'. '+result[i]['sNoiDung']+'</button></div></div>';
						}
						else{
							html+='<div class="col-md-12"><div class="form-group"><button type="button" class="btn btn-default btn-sm mavanban" nam="giaymoi" value="'+result[i]['PK_iMaVBDen']+'">'+so+'. '+result[i]['sMoTa']+'</button></div></div>';
						}
						so++;
					}
					$('.giaymoi').html(html);
				}
			}
		});
	});
	$(document).on('click','.mavanban',function(){
		var mavanban = $(this).val();
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'click_getTTVBDen',
				mavanban:mavanban
			},
			success:function(repon){
				var result = JSON.parse(repon);
				$('input[name=qlv_date]').val(result['ngayvanban']);
				if(result['sNoiDung']!= null)
				{
					$('textarea[name=qlv_noidung]').val(result['sNoiDung']);
				}
				else{
					$('textarea[name=qlv_noidung]').val(result['sMoTa']);
				}
				$('input[name=qlv_ld_chutri]').val(result['sGiayMoiChuTri']);
				$('input[name=qlv_ld_ky]').val(result['sTenNguoiKy']);
				$("#qlv_loai option:selected").text(result['sTenLVB']);
            	$('#qlv_loai').select2();
			}
		});
	});
	// chọn người ký để lấy chức vụ
		$(document).on('change','.linhvuc_cha',function(){
		var ma = $(this).val();
		var k = $(this).attr("id");
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'getLinhvucSub',
				ma:ma
			},
			success:function(repon){
				var result = JSON.parse(repon);
				var tong= result.length;
				if(tong>0)
				{ 
					var html='';
					html+="<option value='0'>-- Lĩnh vực chi tiết --</option>";
					for(var i=0;i<tong;i++)
					{
						html+="<option value='"+result[i]['linhVuc_id']+"'>"+result[i]['linhVuc_name']+"</option>";
					}
					$('select[name=linhvuc_sub_'+k+']').html(html);
					//$(".select2").select2();
				}
			}
		});
	});
	
	$(document).on('change','select[name=linhVuc_id]',function(){
		var ma = $(this).val();
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'getLinhvucSub',
				ma:ma
			},
			success:function(repon){
				var result = JSON.parse(repon);
				var tong= result.length;
				if(tong>0)
				{ 
					var html='';
					html+="<option value='0'>-- Lĩnh vực chi tiết --</option>";
					for(var i=0;i<tong;i++)
					{
						html+="<option value='"+result[i]['linhVuc_id']+"'>"+result[i]['linhVuc_name']+"</option>";
					}
					$('select[name=linhvuc_sub]').html(html);
					$(".select2").select2();
				}
			}
		});
	});
});
