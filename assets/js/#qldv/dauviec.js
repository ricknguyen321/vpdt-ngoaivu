$(document).ready(function() {
	$('.select2-selection__choice__remove').addClass('hide');
	$(document).on('change','.phogiamdoc',function(){
		var PGD = $(this).val();
		var MaVB = $(this).attr('id');
		if(PGD.length>0)
		{
			var ten_PGD = $(this).find("option:selected").text();
			$('.namenoidungchuyen_PGD_'+MaVB).val('Chuyển đồng chí '+ten_PGD);
		}
		else
		{
			$('.namenoidungchuyen_PGD_'+MaVB).val('');
		}
		
	});
	$(document).on('change','.phongchutri',function(){
		var CT = $(this).val();
		var MaVB = $(this).attr('id');
		if(CT.length>0)
		{
			var ten_CT = $(this).find("option:selected").text();
			var phoihop = $('.phongphoihop_'+MaVB).val();
			$(this).parent().parent().find('.phongphoihop').removeAttr("disabled");
			if(phoihop.length>0)
			{
				$('.noidungchuyen_phongban_'+MaVB).val('Chuyển phòng '+ten_CT+' phụ trách giải quyết. '+phoihop);
			}
			else{
				$('.noidungchuyen_phongban_'+MaVB).val('Chuyển phòng '+ten_CT+' phụ trách giải quyết');
			}
		}
		else
		{
			var ten_CT = '';
			$(this).parent().parent().find('.phongphoihop').val(null).trigger("change");
			$(this).parent().parent().find('.phongphoihop').attr("disabled", 'disabled');
			$('.noidungchuyen_phongban_'+MaVB).val('');
		}
		$('.phongchutri_'+MaVB).val('Chuyển phòng '+ten_CT+' phụ trách giải quyết');
		
	});
	$(document).on('change','.phongphoihop',function(){
		var PH = $(this).val();
		var MaVB = $(this).attr('id');
		var phoihop = "";
		var i = 0;
    	$(this).find("option:selected").each(function(){
    		i++;
    		var a = '';
    		var b = '';
    		if(i>1)
    		{
    			var a = ', ';
    		}
    		if(i>0)
    		{
    			var b = ' phối hợp';
    		}
        	phoihop += a+$(this).text()+b;
    	});
    	$('.phongphoihop_'+MaVB).val(phoihop);
    	var chutri = $('.phongchutri_'+MaVB).val();
    	$('.noidungchuyen_phongban_'+MaVB).val(chutri+'. '+phoihop);
    	$('.select2-selection__choice__remove').addClass('hide');
	});
	$(document).on('change','.photruongphong',function(){
		var PTP = $(this).val();
		var MaVB = $(this).attr('id');
		if(PTP.length>0)
		{
			var ten_PTP = $(this).find("option:selected").text();
			$('.noidungchuyen_PTP_'+MaVB).val('Chuyển đồng chí '+ten_PTP);
		}
		else
		{
			$('.noidungchuyen_PTP_'+MaVB).val('');
		}
		
	});
	$(document).on('change','.chuyenvien',function(){
		var CV = $(this).val();
		var MaVB = $(this).attr('id');
		if(CV.length>0)
		{
			var ten_CV = $(this).find("option:selected").text();
			$('.noidungchuyen_CV_'+MaVB).val('Chuyển đồng chí '+ten_CV);
			$(this).parent().parent().find('.chuyenvienPH').removeAttr("disabled");
		}
		else
		{
			$('.noidungchuyen_CV_'+MaVB).val('');
			$(this).parent().parent().find('.chuyenvienPH').val(null).trigger("change");
			$(this).parent().parent().find('.chuyenvienPH').attr("disabled", 'disabled');
		}
		
	});
	$(document).on('change','.chuyenvienPH',function(){
		var PH = $(this).val();
		var MaVB = $(this).attr('id');
		var phoihop = "";
		var i = 0;
    	$(this).find("option:selected").each(function(){
    		i++;
    		var a = '';
    		if(i>1)
    		{
    			var a = ', ';
    		}
        	phoihop += a+$(this).text();
    	});
    	if(phoihop.length>0)
    	{
    		var b =' phối hợp';
    	}
    	else{
    		var b ='';
    	}
    	$('.noidungchuyen_CVPH_'+MaVB).val(phoihop+b);
    	$('.select2-selection__choice__remove').addClass('hide');
	});
	$(document).on('change','.chicucpho',function(){
		var PGD = $(this).val();
		var MaVB = $(this).attr('id');
		if(PGD.length>0)
		{
			var ten_PGD = $(this).find("option:selected").text();
			$('.noidungchuyen_CCP_'+MaVB).val('Chuyển đồng chí '+ten_PGD);
		}
		else
		{
			$('.noidungchuyen_CCP_'+MaVB).val('');
		}
		
	});
	$(document).on('change','.phongchutriCC',function(){
		var CT = $(this).val();
		var MaVB = $(this).attr('id');
		if(CT.length>0)
		{
			var ten_CT = $(this).find("option:selected").text();
			$(this).parent().parent().find('.phongphoihopCC').removeAttr("disabled");
			$('.noidungchuyen_CCCT_'+MaVB).val(ten_CT+' phụ trách giải quyết');
		}
		else
		{
			var ten_CT = '';
			$(this).parent().parent().find('.phongphoihopCC').val(null).trigger("change");
			$(this).parent().parent().find('.phongphoihopCC').attr("disabled", 'disabled');
			$('.noidungchuyen_CCCT_'+MaVB).val('');
			$('.noidungchuyen_CCPH_'+MaVB).val('');
		}
		
	});
	$(document).on('change','.phongphoihopCC',function(){
		var PH = $(this).val();
		var MaVB = $(this).attr('id');
		var phoihop = "";
		var i = 0;
    	$(this).find("option:selected").each(function(){
    		i++;
    		var a = '';
    		if(i>1)
    		{
    			var a = ', ';
    		}
        	phoihop += a+$(this).text();
    	});
    	if(phoihop.length>0)
    	{
    		var b =' phối hợp';
    	}
    	else{
    		var b ='';
    	}
    	$('.noidungchuyen_CCPH_'+MaVB).val(phoihop+b);
    	$('.select2-selection__choice__remove').addClass('hide');
	});
});