/**
 * Created by Minh Duy on 6/21/2017.
 */
$(document).ready(function() {
    $("button[name=chonphoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phongchutri1]').val($phongchutri);
            // alert($(this).attr('data-id'));
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphongban]').val();
                var chutri = $('input[name=phongchutri1]').val();
                // console.log(ma);
                $('#'+id).val('');
                $('#'+id).val(ma);
                $('#'+id+'-'+id).val('Giao '+chutri+' chủ trì tham mưu. '+valuess+' phối hợp.');
                document.getElementById("Q_A").reset();
            });
        });
    });
    $(document).on('change','.giamdoc1',function () {
        var nameDirec = $(this).find(':selected').attr("data-id");
		
        if($(this).val()){
			$(this).parent().parent().parent().find('.chidao').find('.giamdoc').val('Kính chuyển GĐ '+ nameDirec);
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.giamdoc').val('');
        }
    });
    $(document).on('change','.phogiamdoc1',function () {
        var nameDeDirec = $(this).find(':selected').attr('data-id');
		var quyen = $(this).find(':selected').attr('quyen');
        if($(this).val()){
			if(quyen == 4){
				$(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Giám đốc '+ nameDeDirec +' trực tiếp chỉ đạo./.');
				$(this).parent().parent().parent().find('input[id=dondoc]').attr("checked","");
			} else {
				$(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Kính chuyển PGĐ '+ nameDeDirec+' chỉ đạo');
				$(this).parent().parent().parent().find('input[id=dondoc]').removeAttr("checked","");
			}
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Giám đốc trực tiếp chỉ đạo');
        }
//                console.log(command);
    });
    $(document).on('change','.phongchutri1',function () {
        var nameChuTri = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('Giao '+ nameChuTri + ' chủ trì tham mưu.');
            var abc = $(this).parent().next().find('button[name=chonphoihop]').attr("data-id",nameChuTri);
            $(this).parent().next().find('button[name=chonphoihop]').removeAttr("disabled");
            $(this).parent().next().next().find('.mangphoihop1').val('');
            $(this).parent().next().next().val('');
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('');
            $(this).parent().next().find('button[name=chonphoihop]').attr("disabled","disabled");
            $(this).parent().next().next().val('');
        }
    });
    // $('.ykien').each(function () {
    //     var giamdoc = $(this).find('.giamdoc1').find(':selected').attr('data-id');
    //     var chidaogd = $(this).find('.giamdoc1').find(':selected').attr('data-num');
    //     $(this).parent().parent().parent().find('.chidao').find('#'+chidaogd).val('Chuyển đ/c '+ giamdoc + ' - Giám giám đốc');
    //     // alert(chidaogd);
    //     var phogiamdoc = $(this).find('.phogiamdoc1').find(':selected').attr('data-id');
    //     var chidaopgd = $(this).find('.phogiamdoc1').find(':selected').attr('data-num');
    //     $(this).parent().parent().parent().find('.chidao').find('#'+chidaopgd).val('Chuyển đ/c '+ phogiamdoc + ' - Phó giám đốc');
    //     // alert(phogiamdoc);
    // });

    $(document).on('click','#select_all',function () {
        var checked_status = this.checked;
        $(".duyet").each(function(){
            this.checked = checked_status;
        });
    });
	
	

});
