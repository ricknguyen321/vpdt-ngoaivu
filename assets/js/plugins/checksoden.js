/**
 * Created by Minh Duy on 8/4/2017.
 */
// create a module
var myApp = angular.module('myModule',[]);
// create controller
// $scope biến cục bộ
// $rootscope biến toàn cục
myApp.controller('mtController',function ($scope,$http) {
    // click
    $scope.checkSoDen = function () {
        var name = angular.element($('#soden')).val();
        // alert(name+1);
        var url = "/dieuhanhnoibo/vanbanden/Cvanbanden/checkSoDen";
        $http({
            method: 'POST',
            url: url
        }).then(function (response){
            var data = response.data;
            $scope.user = data;
            for(var i=0; i<data.length; i++){
                if(data[i]['iSoDen'] == name){
                    //alert("Số đên "+name+" đã tồn tại hệ thống sẽ tự động nhảy số mới nhất");
                    angular.element($('#soden')).val(parseInt(data[0]['iSoDen'])+1);
                }
            }
        },function (error){
            // log error
        });
    }
});
$(document).ready(function() {
    var url = window.location.href;
    // alert('1');
    $(document).on('change', 'textarea[name=trichyeu]', function () {
        var name = $(this).val();
        // alert(name);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                'action': 'getData',
                'trichyeu' : name
            },
            success: function (response) {
                var result = JSON.parse(response);
                   if(result.length > 0){
                       $('#baoloi').html('Văn bản: '+result[0]['sMoTa']+' - <span style="color:red">đã tồn tại trên hệ thống, vui lòng kiểm tra lại!</span>');	 
                       // console.log(result[0]['sMoTa']);
                   }else{
                       // console.log(1);
                       $('#baoloi').text('');
                   }
               }
            });// end ajax
    });
});


