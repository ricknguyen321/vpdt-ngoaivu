/**
 * Created by Minh Duy on 7/12/2017.
 */
/**
 * Created by Minh Duy on 6/21/2017.
 */
$(document).ready(function() {
    $("button[name=chonphoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phongchutri1]').val($phongchutri);
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphongban]').val();
                var chutri = $('input[name=phongchutri1]').val();
                // console.log(ma);
                $('#'+id).val('');
                $('#'+id).val(ma);
                $('#'+id+'-'+id).val(chutri+' chủ trì. '+valuess+' phối hợp.');
                document.getElementById("Q_A").reset();
            });
        });
    });
    // phó giám đốc phối hợp
    $("button[name=pgdphoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=maphogiamdoc]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phongchutri2]').val($phongchutri);
            $('button[name=ghilai1]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=maphogiamdoc]').val();
                var chutri = $('input[name=phongchutri2]').val();
                // console.log(ma);
                $('#1'+id).val('');
                $('#1'+id).val(ma);
                $('#2'+id).val('Chuyển '+chutri+' chủ trì. Chuyển đ/c '+valuess+' phối hợp.');
                document.getElementById("Q_B").reset();
            });
        });
    });
    $(document).on('change','.giamdoc1',function () {
        var nameDirec = $(this).find(':selected').attr("data-id");
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.giamdoc').val('Chuyển đ/c '+ nameDirec);
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.giamdoc').val('');
        }
    });
    $(document).on('change','.phogiamdoc1',function () {
        var nameDeDirec = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Chuyển đ/c '+ nameDeDirec);
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('');
        }
//                console.log(command);
    });
    $(document).on('change','.phongchutri1',function () {
        var nameChuTri = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('Chuyển '+ nameChuTri + ' chủ trì.');
            var abc = $(this).parent().next().find('button[name=chonphoihop]').attr("data-id",nameChuTri);
            $(this).parent().next().find('button[name=chonphoihop]').removeAttr("disabled");
            $(this).parent().next().next().find('.mangphoihop1').val('');
            $(this).parent().next().next().val('');
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('');
            $(this).parent().next().find('button[name=chonphoihop]').attr("disabled","disabled");
            $(this).parent().next().next().val('');
        }
    });
    // $('.ykien').each(function () {
    //     var giamdoc = $(this).find('.giamdoc1').find(':selected').attr('data-id');
    //     var chidaogd = $(this).find('.giamdoc1').find(':selected').attr('data-num');
    //     $(this).parent().parent().parent().find('.chidao').find('#'+chidaogd).val('Chuyển đồng chí '+ giamdoc + ' - Giám giám đốc');
    //     // alert(chidaogd);
    //     var phogiamdoc = $(this).find('.phogiamdoc1').find(':selected').attr('data-id');
    //     var chidaopgd = $(this).find('.phogiamdoc1').find(':selected').attr('data-num');
    //     $(this).parent().parent().parent().find('.chidao').find('#2'+chidaopgd).val('Chuyển đồng chí '+ phogiamdoc + ' - Phó giám đốc');
    //     // alert(phogiamdoc);
    // });

    $("#select_all").click(function(){
        var checked_status = this.checked;
        $(".duyet").each(function(){
            this.checked = checked_status;
        });
    });

});
