/**
 * Created by Minh Duy on 9/1/2017.
 */
var dbRef = new Firebase("https://message-d1a52.firebaseio.com/");
var notificationsRef = dbRef.child('notifications');
var notificationsLastQuery = notificationsRef.orderByKey();
//        console.log(notificationsRef);
notificationsLastQuery.on("child_added", function(snap) {
    var notifier = snap.val();
    var id_user1 = document.getElementById("id_user1").innerHTML;
    var id_cb1 = document.getElementById("id_cb").innerHTML;
//            console.log(id_phong);
    // Kiểm tra node đã hiển thi chưa
    if(notifier.is_notification==false && notifier.id_cb==id_cb1){
        notifyBrowser('Văn bản đến mới',notifier.notification, notifier.base_url);
        var audio = new Audio('https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/assets/img/123.mp3');
//                alert(audio);
        audio.play();
        // Cập nhật node đã hiển thị thông báo
        notificationsRef.child(snap.key()).remove();
    }
    //gd
    if(notifier.is_notification==false && notifier.id_cb1==id_cb1){
        notifyBrowser('Văn bản đến mới','Có '+notifier.notification+' văn bản đến mới', notifier.base_url);
        var audio = new Audio('https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/assets/img/123.mp3');
//                alert(audio);
        audio.play();
        // Cập nhật node đã hiển thị thông báo
        notificationsRef.child(snap.key()).remove();
    }
    if(notifier.is_notification==false && notifier.id_user==id_user1){
        notifyBrowser('Văn bản đến mới','Có số đến là: '+notifier.notification, notifier.base_url);
        var audio = new Audio('https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/assets/img/123.mp3');
//                alert(audio);
        audio.play();
        // Cập nhật node đã hiển thị thông báo
        notificationsRef.child(snap.key()).remove();
    }
});
function notifyBrowser(title,desc,url){
    if (!Notification) {
        console.log('Desktop notifications not available in your browser..');
        return;
    }
    if (Notification.permission !== "granted"){
        Notification.requestPermission();
    }else {
        var notification = new Notification(title, {
            icon:'https://vpdt.sotaichinh.hanoi.gov.vn/dieuhanhnoibo/assets/img/iconvanban.png',
            body: desc,
        });
        // Remove the notification from Notification Center when clicked.
        notification.onclick = function () {
            window.open(url);
        };
        // Callback function when the notification is closed.
        notification.onclose = function () {
            console.log('Notification closed');
        };
    }
}
