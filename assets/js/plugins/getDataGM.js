/**
 * Created by Minh Duy on 10/2/2017.
 */
$(document).ready(function() {
    var url = window.location.href;
    // alert('1');
    $(document).on('click', 'button[name=themnoidung]', function () {
        var name = $(this).val();
        // alert(name);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                'action': 'getDataDocGo',
                'idDoc' : name
            },
            success: function (response) {
                var result = JSON.parse(response);
                if(result.length > 0){
                    $('input[name=khuvuc]').val(result[0]['sTenKV']);
                    $('input[name=sokyhieu]').val(result[0]['sKyHieu']);
                    $('input[name=loaivanban]').val(result[0]['sTenLVB']);
                    $('input[name=donvi]').val(result[0]['sTenDV']);
                    $('input[name=ngayky]').val(result[0]['sNgayKy']);
                    $('input[name=linhvuc]').val(result[0]['sTenLV']);
                    $('input[name=trichyeu]').val(result[0]['sMoTa']);
                    $('input[name=nguoiky]').val(result[0]['sTenNguoiKy']);
                    $('input[name=ngaynhan]').val(result[0]['sNgayNhan']);
                    $('input[name=soden]').val(result[0]['iSoDen']);
                    $('input[name=chucvu]').val(result[0]['sChucVu']);
                    $('input[name=hangiaiquyet1]').val(result[0]['sHanGiaiQuyet']);
                    $('input[name=sotrang]').val(result[0]['iSoTrang']);
                    $('input[name=giohopcu]').val(result[0]['sGiayMoiGio']);
                    $('input[name=ngayhopcu]').val(result[0]['sGiayMoiNgay']);
                    $('input[name=diachihopcu]').val(result[0]['sGiayMoiDiaDiem']);
                }
            }
        });// end ajax
    });
    // $(".modal").on("hidden.bs.modal", function() {
    //     $(".modal-body").html("");
    // });
});