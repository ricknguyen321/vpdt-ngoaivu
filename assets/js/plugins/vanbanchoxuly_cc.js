/**
 * Created by Minh Duy on 8/1/2017.
 */
$(document).ready(function() {
    $("button[name=chonphoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phongchutri1]').val($phongchutri);
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphongban]').val();
                var chutri = $('input[name=phongchutri1]').val();
                // console.log(ma);
                $('#'+id).val('');
                $('#'+id).val(ma);
                $('#'+id+'-'+id).val('Chuyển đ/c '+chutri+' chủ trì. '+valuess+' phối hợp.');
                document.getElementById("Q_A").reset();
            });
        });
    });
    $(document).on('change','.phochicuc',function () {
        var nameDeDirec = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Chuyển đ/c '+ nameDeDirec);
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('');
        }
//                console.log(command);
    });
    $(document).on('change','.truongphong',function () {
        var nameChuTri = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('Chuyển đ/c '+ nameChuTri + ' tham mưu giải quyết.');
            var abc = $(this).parent().next().find('button[name=chonphoihop]').attr("data-id",nameChuTri);
            $(this).parent().next().find('button[name=chonphoihop]').removeAttr("disabled");
            $(this).parent().next().next().find('input[name="mangphoihop[]"]').val('');
            $(this).parent().next().next().val('');
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('');
            $(this).parent().next().find('button[name=chonphoihop]').attr("disabled","disabled");
            $(this).parent().next().next().val('');
        }
    });
    // $('.ykien').each(function () {
    //     var phogiamdoc = $(this).find('select[name="phogiamdoc[]"]').find(':selected').attr('data-id');
    //     var chidaopgd = $(this).find('select[name="phogiamdoc[]"]').find(':selected').attr('data-num');
    //     $(this).parent().parent().parent().find('.chidao').find('#'+chidaopgd).val('Chuyển đ/c '+ phogiamdoc + ' - Phó giám đốc');
    //     // alert(phogiamdoc);
    // });


});
/**
 * Created by Minh Duy on 8/1/2017.
 */
