/**
 * Created by Minh Duy on 6/28/2017.
 */
/**
 * Created by Minh Duy on 6/21/2017.
 */
$(document).ready(function() {
    $("button[name=chonphoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phongchutri1]').val($phongchutri);
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphongban]').val();
                var chutri = $('input[name=phongchutri1]').val();
                // console.log(ma);
                $('#'+id).val('');
                $('#'+id).val(ma);
                $('#'+id+'-'+id).val('Chuyển đ/c '+valuess+' phối hợp giải quyết.');
                document.getElementById("Q_A").reset();
            });
        });
    });
    $(document).on('change','.phogiamdoc1',function () {
        var nameDeDirec = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Chuyển PP '+ nameDeDirec);
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('');
        }
    });
    $("button[name=chonphophoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphophongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phochutri1]').val($phongchutri);
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphophongban]').val();
                var chutri = $('input[name=phochutri1]').val();
                // console.log(ma);
                $('#'+id+'-1').val('');
                $('#'+id+'-1').val(ma);
                $('#'+id+'-'+id+'-1').val('Chuyển PP '+chutri+' chủ trì. '+valuess+' phối hợp.');
                document.getElementById("Q_B").reset();
            });
        });
    });

    $(document).on('change','.phogiamdoc2',function () {
        // alert(12345678);
        var nameDeDirec = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Chuyển TP '+ nameDeDirec);
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('');
        }
    });

    $("button[name=chonphophoihop_cc]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphophongban1]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phochutri2]').val($phongchutri);
            $('button[name=ghilai1]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-num');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphophongban1]').val();
                var chutri = $('input[name=phochutri2]').val();
                // console.log(ma);
                $('#'+id+'-1').val('');
                $('#'+id+'-1').val(ma);
                $('#'+id+'-'+id+'-1').val('Chuyển TP '+chutri+' chủ trì. '+valuess+' phối hợp.');
                document.getElementById("BHN").reset();
            });
        });
    });


});