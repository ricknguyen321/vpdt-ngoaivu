/**
 * Created by Minh Duy on 7/3/2017.
 */
$(document).ready(function() {
	
    $("button[name=chonphoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phongchutri1]').val($phongchutri);
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphongban]').val();
                var chutri = $('input[name=phongchutri1]').val();
                // console.log(ma);
                $('#'+id).val('');
                $('#'+id).val(ma);
                $('#'+id+'-'+id).val('Kính chuyển '+chutri+' chủ trì tham mưu. '+valuess+' phối hợp.');
                document.getElementById("Q_A").reset();
            });
        });
    });
    $(document).on('change','select[name="giamdoc[]"]',function () {
        var nameDirec = $(this).find(':selected').attr("data-id");
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.giamdoc').val('Kính chuyển GĐ '+ nameDirec+' chỉ đạo');
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.giamdoc').val('');
        }
    });
    $(document).on('change','select[name="phogiamdoc[]"]',function () {
        var nameDeDirec = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Kính chuyển PGĐ '+ nameDeDirec+' chỉ đạo');
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('');
        }
//                console.log(command);
    });
    $(document).on('change','select[name="phongchutri[]"]',function () {
        var nameChuTri = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('Giao '+ nameChuTri + ' chủ trì tham mưu.');
            var abc = $(this).parent().next().find('button[name=chonphoihop]').attr("data-id",nameChuTri);
            $(this).parent().next().find('button[name=chonphoihop]').removeAttr("disabled");
            $(this).parent().next().next().find('input[name="mangphoihop[]"]').val('');
            $(this).parent().next().next().val('');
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('');
            $(this).parent().next().find('button[name=chonphoihop]').attr("disabled","disabled");
            $(this).parent().next().next().val('');
        }
    });
    $('.ykien').each(function () {
        var phogiamdoc = $(this).find('select[name="phogiamdoc[]"]').find(':selected').attr('data-id');
        var chidaopgd = $(this).find('select[name="phogiamdoc[]"]').find(':selected').attr('data-num');
        $(this).parent().parent().parent().find('.chidao').find('#'+chidaopgd).val('Kính chuyển đ/c '+ phogiamdoc);
        // alert(phogiamdoc);
    });
	
	
	
});
