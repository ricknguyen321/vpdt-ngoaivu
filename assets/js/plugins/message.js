/**
 * Created by Minh Duy on 8/25/2017.
 */
$(document).on('click','button[name=duyetvanban]',function(){
    $('#duyet').click();
});
document.addEventListener('DOMContentLoaded', function () {
    document.querySelector("#duyet").addEventListener("click", function(e){
        var dem = 0;
        $(".thongbao").each(function () {
            var giamdoc = $(this).find('.ykien').find('.giamdoc1 :selected').val();
            var phogiamdoc = $(this).find('.ykien').find('.phogiamdoc1 :selected').val();
            var tieudepgd = $(this).find('.ykien').find('.phogiamdoc1 :selected').attr('data-toggle');
            var phongchutri = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-num');
            var tieude = $(this).find('.ykien').find('.phongchutri1 :selected').attr('data-collapse');
            if (giamdoc != "") {
                // console.log(giamdoc);
                dem++;
            }
            if (giamdoc == "") {
                if (phogiamdoc != "") {
                    var notification1 = {
                        'is_notification': false,
                        'id_cb': phogiamdoc,
                        'base_url': 'vanbanchoxuly_pgd',
                        'notification': tieudepgd
                    };
                    notificationsRef.push(notification1);
                }
            }
            if ((giamdoc == "") && (phogiamdoc == "")) {
                if (phongchutri != undefined) {
                    var notification2 = {
                        'is_notification': false,
                        'id_cb': phongchutri,
                        'base_url': 'vanbanchoxuly_tp',
                        'notification': tieude
                    };
                    notificationsRef.push(notification2);
                }
            }
        });

        // if (dem > 0 || dempgd > 0 || dempct > 0) {
            if (Notification.permission !== "granted") {
                Notification.requestPermission();
            }
            e.preventDefault();
            // Save notification
            if(dem > 0){
                var notification = {
                    'is_notification': false,
                    'id_cb1': $('#idhmh').text(),
                    'base_url': 'vanbanchoxuly_gd',
                    'notification': dem
                };
                notificationsRef.push(notification);
            }
//            $('#soden').val('');
            return false;
        // }
    });
});
