/**
 * Created by Minh Duy on 6/21/2017.
 */
$(document).ready(function() {
    $("button[name=chonphoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phongchutri1]').val($phongchutri);
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphongban]').val();
                var chutri = $('input[name=phongchutri1]').val();
                // console.log(ma);
                $('#'+id).val('');
                $('#'+id).val(ma);
                $('#'+id+'-'+id).val('Chuyển đ/c '+chutri+' chủ trì giải quyết. '+valuess+' phối hợp.');
                document.getElementById("Q_A").reset();
            });
        });
    });
    $(document).on('change','.phogiamdoc1',function () {
        var nameDeDirec = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('Chuyển PP '+ nameDeDirec);
            $(this).parent().next().next().next().next().next().val('')
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phogiamdoc').val('');
            $(this).parent().next().next().next().next().next().val('')
        }
//                console.log(command);
    });
    $(document).on('change','.phongchutri1',function () {
        var nameChuTri = $(this).find(':selected').attr('data-id');
        if($(this).val()){
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('Chuyển đ/c '+ nameChuTri + ' chủ trì giải quyết.');
            var abc = $(this).parent().next().find('button[name=chonphoihop]').attr("data-id",nameChuTri);
            $(this).parent().next().find('button[name=chonphoihop]').removeAttr("disabled");
            $(this).parent().next().next().find('.mangphoihop1').val('');
            $(this).parent().next().next().val('');
        }
        else{
            $(this).parent().parent().parent().find('.chidao').find('.phongchutri').val('');
            $(this).parent().next().find('button[name=chonphoihop]').attr("disabled","disabled");
            $(this).parent().next().next().val('');
        }
    });

    $("#select_all").click(function(){
        var checked_status = this.checked;
        $(".duyet").each(function(){
            this.checked = checked_status;
        });
    });
    checkCBHop();

});
function checkCBHop() {
    var url = window.location.href;
    $(document).on('change','.cvct',function () {
       var idDoc = $(this).find(':selected').attr('data-id');
        var idCB = $(this).val();
        var giohop = $(this).find(':selected').attr('data-fruit');
        var ngayhop = $(this).find(':selected').attr('data-leaves');
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                'action' : 'checkCBHop',
                'idCB' : idCB,
                'giohop' : giohop,
                'ngayhop' : ngayhop
            },
            success: function(response){
                var result = JSON.parse(response);
                // alert(result);
                if(result != ""){
                    alert('đ/c '+idDoc+' đã có cuộc họp vào hồi '+giohop+' ngày '+ngayhop);
                }
            }
        });// end ajax
       // alert(ngayhop);
    });
    // check phó phòng có họp hay không
    $(document).on('change','.phophong',function () {
        var idDoc = $(this).find(':selected').attr('data-id');
        var idCB = $(this).val();
        var giohop = $(this).find(':selected').attr('data-fruit');
        var ngayhop = $(this).find(':selected').attr('data-leaves');
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                'action' : 'checkCBHop',
                'idCB' : idCB,
                'giohop' : giohop,
                'ngayhop' : ngayhop
            },
            success: function(response){
                var result = JSON.parse(response);
                // alert(result);
                if(result != ""){
                    alert('đ/c '+idDoc+' đã có cuộc họp vào hồi '+giohop+' ngày '+ngayhop);
                }
            }
        });// end ajax
        // alert(ngayhop);
    });

    $("button[name=chonphophoihop]").each(function(i,e){
        $(this).click(function () {
            $('input[name=mangphophongban]').val($(this).val());
            $phongchutri = $(this).parent().prev().find(':selected').html();
            $('input[name=phochutri1]').val($phongchutri);
            $('button[name=ghilai]').click(function() {
                var valuess = $('.Checkbox:checked').map(function() {
                    return $(this).attr('data-id');
                }).get().join(',');
                var ma = $('.Checkbox:checked').map(function() {
                    return this.value;
                }).get().join(',');
                var id = $('input[name=mangphophongban]').val();
                var chutri = $('input[name=phochutri1]').val();
                // console.log(ma);
                $('#'+id+'--1').val('');
                $('#'+id+'--1').val(ma);
                $('#'+id+'-'+id+'-1').val('Chuyển PP '+chutri+' chủ trì. '+valuess+' phối hợp.');
                document.getElementById("Q_B").reset();
            });
        });
    });
}
