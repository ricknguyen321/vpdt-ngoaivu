$(document).ready(function() {
	var url = window.location.href;
	// chọn người ký để lấy chức vụ
	$(document).on('change','select[name=nguoiky]',function(){
		var ma = $('select[name=nguoiky]').val();
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'laychucvu',
				ma:ma
			},
			success:function(repon){
				var result = JSON.parse(repon);
				$('input[name=chucvu]').val(result);
			}
		});
	});
	// chọn loại văn bản để lấy ra số ký hiệu
	$(document).on('change','.laykyhieu',function(){
		var maloai  = $('select[name=loaivanban]').val();
		var maphong = $('select[name=duthao]').val();
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'laykyhieu',
				maloai:maloai,
				maphong:maphong
			},
			success:function(repon){
				var result = JSON.parse(repon);
				$('input[name=sodi]').val(result[0]);
				$('input[name=kyhieu]').val(result[1]);

			}
		});
	});
});