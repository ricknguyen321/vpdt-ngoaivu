$(document).ready(function() {
	var url = window.location.href;
	// chọn người ký để lấy chức vụ
	$(document).on('change','select[name=nguoiky]',function(){
		var ma = $('select[name=nguoiky]').val();
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'laychucvu',
				ma:ma
			},
			success:function(repon){
				var result = JSON.parse(repon);
				$('input[name=chucvu]').val(result);
			}
		});
	});
	// chọn loại văn bản để lấy ra số ký hiệu
	$(document).on('change','.kyhieu',function(){
		var maloai  = $('select[name=loaivanban]').val();
		var maphong = $('select[name=noiduthao]').val();
		var so      = $('input[name=sovanban]').val();
		if(maloai==10)
		{
			$('.an').removeClass('hide');
			$('.req').attr("required", "true");
		}
		else{
			$('.an').addClass('hide');
			$('.req').removeAttr('required');
			$('.req').val('');
		}
		$.ajax({
			url:url,
			type:'post',
			data:{
				action:'laykyhieu',
				maloai:maloai,
				maphong:maphong,
				so:so
			},
			success:function(repon){
				var result = JSON.parse(repon);
				$('input[name=sokyhieu]').val(result);
			}
		});
	});
});